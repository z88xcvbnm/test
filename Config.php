<?php

class Config{

    /** @var bool */
//    public  static $is_debug            =true;
    public  static $is_debug            =false;

    /** @var bool */
    public  static $is_query_debug      =false;

    /** @var bool */
    public  static $is_geo_debug        =false;

    /** @var bool */
    public  static $is_geo_determine    =false;

    /** @var bool */
    public  static $is_need_recaptcha   =true;

    /** @var string */
    public  static $project_name        ='Shluham.net';

    /** @var string */
    public  static $db_name_server      ='whore';

    /** @var string */
    public  static $db_name_local       ='whore';

    /** @var array */
    public  static $email_log           =array(
        'solveigsaab@outlook.com'
    );

    /** @var string */
    public  static $email_no_replay     ='no-reply@shluham.net';

    /** @var string */
//    public  static $email_default       ='serg.silinin@gmail.com';
    public  static $email_default       ='no-reply@shluham.net';

    /** @var string */
    public  static $email_payment       ='serg.silinin@gmail.com';
//    public  static $email_payment       ='no-reply@shluham.net';

    /** @var string */
    public  static $email_feedback      ='serg.silinin@gmail.com';

    /** @var string */
    public  static $http_type           ='https';

    /**
     * @return boolget_trigger
     */
    public  static function init(){

        self::$is_debug=false;

        return true;

    }

}