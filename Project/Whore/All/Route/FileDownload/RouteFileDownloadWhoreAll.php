<?php

namespace Project\Whore\All\Route\FileDownload;

use Core\Module\User\User;

class RouteFileDownloadWhoreAll{

    /**
     * @return mixed
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(User::is_login())
            return RouteAuthorizedUserFileDownloadWhoreApi::init();
        else
            return RouteUnAuthorizedUserFileDownloadWhoreApi::init();

    }

    /**
     * @return mixed
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}