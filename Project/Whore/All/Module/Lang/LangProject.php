<?php

namespace Project\Whore\All\Module\Lang;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;

class LangProject{

    /** @var int */
    public  static $lang_ID;

    /** @var string */
    public  static $lang_key;

    /**
     * @param int|NULL $lang_ID
     * @return null|string
     * @throws ParametersException
     */
    public  static function get_lang_name(int $lang_ID=NULL){

        if(empty($lang_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'    =>array(
                'name'
            ),
            'table'     =>'lang',
            'where'     =>array(
                'id'        =>$lang_ID,
                'type'      =>0
            ),
            'limit'     =>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param int|NULL $lang_ID
     * @return null|string
     * @throws ParametersException
     */
    public  static function get_lang_key(int $lang_ID=NULL){

        if(empty($lang_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'    =>array(
                'key'
            ),
            'table'     =>'lang',
            'where'     =>array(
                'id'        =>$lang_ID,
                'type'      =>0
            ),
            'limit'     =>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['key'];

    }

    /**
     * @param string|NULL $lang_key
     * @return null|int
     * @throws ParametersException
     */
    public  static function get_lang_ID(string $lang_key=NULL){

        if(empty($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'    =>array(
                'id'
            ),
            'table'     =>'lang',
            'where'     =>array(
                'key'       =>$lang_key,
                'type'      =>0
            ),
            'limit'     =>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param string|NULL $lang_key
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_lang_key(string $lang_key=NULL){

        if(empty($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'key'=>$lang_key
        );

        return Db::isset_row('lang',0,$where_list);

    }

    /**
     * @param string|NULL $lang_key
     * @return array|null
     * @throws ParametersException
     */
    public  static function get_lang_data(string $lang_key=NULL){

        if(empty($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'    =>array(
                'id',
                'key',
                'name'
            ),
            'table'     =>'lang',
            'where'     =>array(
                'key'       =>$lang_key,
                'type'      =>0
            ),
            'limit'     =>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'    =>$r[0]['id'],
            'key'   =>$r[0]['key'],
            'name'  =>$r[0]['name']
        );

    }

    /**
     * @param int|NULL $lang_ID
     * @return array|null
     * @throws ParametersException
     */
    public  static function get_lang_ID_data(int $lang_ID=NULL){

        if(empty($lang_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'    =>array(
                'id',
                'key',
                'name'
            ),
            'table'     =>'lang',
            'where'     =>array(
                'id'        =>$lang_ID,
                'type'      =>0
            ),
            'limit'     =>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'    =>$r[0]['id'],
            'key'   =>$r[0]['key'],
            'name'  =>$r[0]['name']
        );

    }

    /**
     * @return array
     */
    public  static function get_lang_list(array $lang_ID_list=[]){

        $q=array(
            'select'    =>array(
                'id',
                'key',
                'name'
            ),
            'table'     =>'lang',
            'where'     =>array(
                'type'=>0
            )
        );

        if(count($lang_ID_list)>0)
            $q['where']['id']=$lang_ID_list;

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=array(
                'ID'        =>$row['id'],
                'key'       =>$row['key'],
                'name'      =>$row['name']
            );

        return $list;

    }

    /**
     * @return array
     */
    public  static function get_lang_list_all(){

        return self::get_lang_list();

    }

    /**
     * @param array|NULL $lang_ID_list
     * @param int|NULL $timestamp_from
     * @param int|NULL $timestamp_to
     * @return array
     */
    public  static function get_lang_full_data_list(array $lang_ID_list=NULL,int $timestamp_from=NULL,int $timestamp_to=NULL,int $limit=NULL,int $len=NULL,bool $is_get_count=NULL){

        $q=array(
            'select'=>array(
                'id',
                'lang_id',
                'key',
                'name',
                'order',
                'hide',
                'type'
            ),
            'table'=>'lang',
            'where'=>array(
                array(
                    'column'    =>'public',
                    'value'     =>1
                )
            )
        );

        if(!is_null($lang_ID_list))
            $q['where'][]=array(
                'column'    =>'id',
                'value'     =>$lang_ID_list
            );

        if(!is_null($limit))
            $q['limit']=$limit;

        if(!is_null($timestamp_from)||!is_null($timestamp_to)){

            $q['where'][]=array(
                'column'    =>'hide',
                'value'     =>array(0,1)
            );
            $q['where'][]=array(
                'column'    =>'type',
                'value'     =>array(0,1)
            );

            if(!is_null($timestamp_from)&&!is_null($timestamp_to))
                $q['where'][]=array(
                    'where'=>array(
                        array(
                            'column'    =>'date_update',
                            'method'    =>'>',
                            'value'     =>Date::get_date_time_full($timestamp_from)
                        ),
                        array(
                            'column'    =>'date_update',
                            'method'    =>'<=',
                            'value'     =>Date::get_date_time_full($timestamp_to)
                        )
                    )
                );
            else if(!is_null($timestamp_from))
                $q['where'][]=array(
                    'column'    =>'date_update',
                    'method'    =>'>',
                    'value'     =>Date::get_date_time_full($timestamp_from)
                );
            else if(!is_null($timestamp_to))
                $q['where'][]=array(
                    'column'    =>'date_update',
                    'method'    =>'<=',
                    'value'     =>Date::get_date_time_full($timestamp_to)
                );

        }
        else{

            $q['where'][]=array(
                'column'    =>'hide',
                'value'     =>0
            );
            $q['where'][]=array(
                'column'    =>'type',
                'value'     =>0
            );

        }

        if(!is_null($is_get_count))
            if($is_get_count)
                return Db::get_row_len($q['table'],NULL,$q['where']);

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=array(
                'ID'            =>$row['id'],
                'lang_ID'       =>$row['lang_id'],
                'key'           =>$row['key'],
                'name'          =>$row['name'],
                'order'         =>$row['order'],
                'is_hide'       =>(bool)$row['hide'],
                'type'          =>$row['type']
            );

        return $list;

    }

    /**
     * @param int|NULL $lang_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_lang_ID(int $lang_ID=NULL){

        if(empty($lang_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($lang_ID,'lang',0);

    }

}