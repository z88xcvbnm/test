<?php

namespace Project\Whore\All\Module\Lang;

class LangConfigProject{

    /** @var int */
    public  static $lang_ID_default     =1;

    /** @var int */
    public  static $lang_ID_spare       =2;

    /** $var string */
    public  static $lang_key_default    ='ru';

    /** $var string */
    public  static $lang_key_spare      ='en';

}