<?php

namespace Project\Whore\All\Module\Lang;

use Core\Module\Db\Db;
use Core\Module\Lang\Lang;

class LangDetermineProject{

    /**
     * @return bool
     */
    private static function set_lang_ID(){

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'lang',
            'where'=>array(
                'lang_id'   =>Lang::$lang_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0){

            LangProject::$lang_ID=LangConfigProject::$lang_ID_default;

            return true;

        }

        LangProject::$lang_ID=$r[0]['id'];

        return true;

    }

    /**
     * Preare
     */
    private static function set(){

        self::set_lang_ID();

    }

    /**
     * Init
     */
    public  static function init(){

        self::set();

    }

}