<?php

namespace Project\Whore\All\Module\Info;

use Core\Module\Db\Db;

class Info{

    /** @var string */
    public  static $table_name='info';

    /**
     * @param string|NULL $group
     * @param string|NULL $key
     * @return array|null
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_info_list(string $group=NULL,string $key=NULL){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'group',
                'key',
                'value'
            ],
            'where'=>[
                'type'=>0
            ]
        ];

        if(!empty($group))
            $q['where']['group']=$group;

        if(!empty($key))
            $q['where']['key']=$key;

        $r=Db::select($q);

        if(empty($r))
            return NULL;

        $list=[];

        foreach($r as $row){

            if(!isset($list[$row['group']]))
                $list[$row['group']]=[];

            $list[$row['group']][$row['key']]=$row['value'];

        }

        return $list;

    }

    /**
     * @param string|NULL $group
     * @param string|NULL $key
     * @param bool|null $value
     * @return array|bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_info_data(string $group=NULL,string $key=NULL,$value=NULL){

        if(empty($group))
            return false;

        if(empty($key))
            return false;

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'value'         =>$value,
                'date_update'   =>'NOW()'
            ],
            'where'=>[
                'group'     =>$group,
                'key'       =>$key,
                'type'      =>0
            ]
        ];

        return Db::update($q);

    }

}