<?php

namespace Project\Whore\All\Module\Wallet;

use Core\Module\Wallet\WalletAddress;
use Core\Module\Wallet\WalletName;

class WalletConfig{

    /**
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_wallet_list(){

        $wallet_address_list    =WalletAddress::get_wallet_address_list(false);
        $wallet_name_list       =WalletName::get_wallet_name_list(false);

        if(count($wallet_address_list)==0)
            return[];

        $list=[];

        foreach($wallet_address_list as $row)
            if(isset($wallet_name_list[$row['wallet_name_ID']]))
                $list[$wallet_name_list[$row['wallet_name_ID']]]=$row['address'];
            else
                $list[$wallet_name_list[$row['wallet_name_ID']]]=NULL;

        return $list;

    }

}