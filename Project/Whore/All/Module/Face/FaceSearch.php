<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Json\Json;
use Core\Module\User\User;

class FaceSearch{

    /** @var string */
    public  static $table_name='face_search';

    /**
     * @param int|NULL $face_search_multi_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_list(int $face_search_multi_ID=NULL){

        if(empty($face_search_multi_ID))
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'image_id'
            ],
            'where'     =>[
                'face_search_multi_id'  =>$face_search_multi_ID,
                'type'                  =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['id']]=$row['image_id'];

        return $list;

    }

    /**
     * @param int|NULL $face_search_history_ID
     * @param int $start
     * @param int $len
     * @param int|NULL $user_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_data_list(int $face_search_history_ID=NULL,int $start=0,int $len=30,int $user_ID=NULL){

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'image_id',
                'face_id',
                'search_count',
                'percent',
                'price',
                'date_create'
            ],
            'where'     =>[
                'user_id'       =>User::$user_ID,
                'type'          =>0
            ],
            'order'=>[
                [
                    'column'        =>'id',
                    'direction'     =>'desc'
                ]
            ],
            'limit'=>[
                $start,
                $len
            ]
        ];

        if(!empty($face_search_history_ID))
            $q['where']['id']=[
                'method'        =>'>',
                'value'         =>$face_search_history_ID
            ];

        if(!empty($user_ID))
            $q['where']['user_id']=$user_ID;

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'ID'                =>$row['id'],
                'image_ID'          =>$row['image_id'],
                'face_ID'           =>$row['face_id'],
                'search_count'      =>$row['search_count'],
                'percent'           =>floor($row['percent']*100),
                'price'             =>floor($row['price']),
                'date_create'       =>strtotime($row['date_create'])
            ];

        return $list;

    }

    /**
     * @param int|NULL $user_ID
     * @return float|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_data_last(int $user_ID=NULL){

        if(empty($user_ID))
            return NULL;

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'percent'
            ],
            'where'     =>[
                'user_id'       =>$user_ID,
                'type'          =>0
            ],
            'order'=>[
                [
                    'column'        =>'percent',
                    'direction'     =>'desc'
                ]
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['percent'];

    }

    /**
     * @param int|NULL $face_search_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_data(int $face_search_ID=NULL){

        if(empty($face_search_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face search ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'image_id'
            ],
            'where'     =>[
                'id'        =>$face_search_ID,
                'type'      =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'image_ID'      =>$r[0]['image_id']
        ];

    }

    /**
     * @param int|NULL $face_search_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_percent(int $face_search_ID=NULL){

        if(empty($face_search_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face search ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'percent'
            ],
            'where'     =>[
                'id'        =>$face_search_ID,
                'type'      =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['percent'];

    }

    /**
     * @param string|NULL $face_search_token
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_data_from_face_search_token(string $face_search_token=NULL){

        if(empty($face_search_token)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face search token is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'user_id',
                'file_id',
                'image_id',
                'face_pp_face_token',
                'face_coords',
                'faceset_index',
                'faceset_len'
            ],
            'where'     =>[
                'token'     =>$face_search_token,
                'type'      =>[0,2]
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'ID'                =>$r[0]['id'],
            'user_ID'           =>$r[0]['user_id'],
            'file_ID'           =>$r[0]['file_id'],
            'image_ID'          =>$r[0]['image_id'],
            'face_token'        =>$r[0]['face_pp_face_token'],
            'face_coords'       =>$r[0]['face_coords'],
            'faceset_index'     =>$r[0]['faceset_index'],
            'faceset_len'       =>$r[0]['faceset_len']
        ];

    }

    /**
     * @param string|NULL $face_search_token
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_data_from_face_search_token_with_multi_ID(int $face_search_multi_ID=NULL,string $face_search_token=NULL){

        $error_info_list=[];

        if(empty($face_search_multi_ID))
            $error_info_list[]='Face search multi ID is empty';

        if(empty($face_search_token))
            $error_info_list[]='Face search token is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'user_id',
                'file_id',
                'image_id',
                'face_pp_face_token',
                'face_coords',
                'faceset_index',
                'faceset_len'
            ],
            'where'     =>[
                'face_search_multi_id'      =>$face_search_multi_ID,
                'token'                     =>$face_search_token,
                'type'                      =>[0,2]
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'ID'                =>$r[0]['id'],
            'user_ID'           =>$r[0]['user_id'],
            'file_ID'           =>$r[0]['file_id'],
            'image_ID'          =>$r[0]['image_id'],
            'face_token'        =>$r[0]['face_pp_face_token'],
            'face_coords'       =>$r[0]['face_coords'],
            'faceset_index'     =>$r[0]['faceset_index'],
            'faceset_len'       =>$r[0]['faceset_len']
        ];

    }

    /**
     * @param int|NULL $face_search_ID
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_face_search_with_user(int $face_search_ID=NULL,int $user_ID=NULL){

        $error_info_list=[];

        if(empty($face_search_ID))
            $error_info_list[]='Face search ID is empty';

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'id'        =>$face_search_ID,
            'user_id'   =>$user_ID
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $face_token
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_ID(int $face_token=NULL){

        if(empty($face_token)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face token is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_token'=>$face_token
        ];

        return Db::get_row_ID(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $file_ID
     * @param int|NULL $image_ID
     * @param int|NULL $face_ID
     * @param int|NULL $user_balance_transaction_ID
     * @param array|NULL $face_coords_list
     * @param string|NULL $face_token
     * @param int|NULL $face_token_count
     * @param float|NULL $percent
     * @param float|NULL $price
     * @param string|NULL $face_search_token
     * @param int $faceset_index
     * @param int $faceset_len
     * @param int $type
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_search(int $file_ID=NULL,int $image_ID=NULL,int $face_ID=NULL,int $user_balance_transaction_ID=NULL,array $face_coords_list=NULL,string $face_token=NULL,int $face_token_count=NULL,float $percent=NULL,float $price=NULL,string $face_search_token=NULL,int $faceset_index=0,int $faceset_len=0,int $type=0,int $face_search_multi_ID=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'                           =>User::$user_ID,
                'file_id'                           =>$file_ID,
                'image_id'                          =>$image_ID,
                'face_id'                           =>$face_ID,
                'user_balance_transaction_id'       =>$user_balance_transaction_ID,
                'face_search_multi_id'              =>$face_search_multi_ID,
                'face_coords'                       =>empty($face_coords_list)?NULL:Json::encode($face_coords_list),
                'face_count'                        =>empty($face_coords_list)?NULL:count($face_coords_list),
                'face_pp_face_token'                =>$face_token,
                'search_count'                      =>empty($face_token_count)?0:$face_token_count,
                'percent'                           =>empty($percent)?0:$percent,
                'price'                             =>empty($price)?0:$percent,
                'token'                             =>$face_search_token,
                'faceset_index'                     =>$faceset_index,
                'faceset_len'                       =>$faceset_len,
                'date_create'                       =>'NOW()',
                'date_update'                       =>'NOW()',
                'type'                              =>$type
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face search was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_search_ID
     * @param int|NULL $face_ID
     * @param int|NULL $user_balance_transaction_ID
     * @param array|NULL $face_coords_list
     * @param string|NULL $face_token
     * @param int|NULL $face_token_count
     * @param float|NULL $percent
     * @param float|NULL $price
     * @param string|NULL $face_search_token
     * @param int $faceset_index
     * @param int $faceset_len
     * @param int $type
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_search(int $face_search_ID=NULL,int $face_ID=NULL,int $user_balance_transaction_ID=NULL,array $face_coords_list=NULL,string $face_token=NULL,int $face_token_count=NULL,float $percent=NULL,float $price=NULL,string $face_search_token=NULL,int $faceset_index=NULL,int $faceset_len=NULL,int $type=NULL){

        $error_info_list=[];

        if(empty($face_search_ID))
            $error_info_list[]='File search ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'date_update'=>'NOW()'
            ],
            'where'=>[
                'id'        =>$face_search_ID
            ]
        ];

        if(!is_null($face_ID))
            $q['set']['face_id']=$face_ID;

        if(!is_null($user_balance_transaction_ID))
            $q['set']['user_balance_transaction_id']=$user_balance_transaction_ID;

        if(!is_null($face_coords_list)){

            $q['set']['face_coords']    =Json::encode($face_coords_list);
            $q['set']['face_count']     =count($face_coords_list);

        }

        if(!is_null($face_token))
            $q['set']['face_pp_face_token']=$face_token;

        if(!is_null($face_token_count))
            $q['set']['search_count']=$face_token_count;

        if(!is_null($percent))
            $q['set']['percent']=$percent;

        if(!is_null($price))
            $q['set']['price']=$price;

        if(!is_null($face_search_token))
            $q['set']['token']=$face_search_token;

        if(!is_null($faceset_index))
            $q['set']['faceset_index']=$faceset_index;

        if(!is_null($faceset_len))
            $q['set']['faceset_len']=$faceset_len;

        if(!is_null($type))
            $q['set']['type']=$type;

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face search was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_search_ID
     * @param int $faceset_index
     * @param int|NULL $type
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_search_face_index(int $face_search_ID=NULL,int $faceset_index=0,int $type=NULL){

        if(empty($face_search_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face search ID is empty'
            ];

            throw new ParametersValidationException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'faceset_index'     =>$faceset_index,
                'date_update'       =>'NOW()'
            ],
            'where'=>[
                'id'        =>$face_search_ID
            ]
        ];

        if(!is_null($type))
            $q['set']['type']=$type;

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face search was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

}