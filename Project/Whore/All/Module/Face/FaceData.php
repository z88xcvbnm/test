<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;

class FaceData{

    /** @var string */
    public  static $table_name='face_data';

    /**
     * @param int|NULL $face_data_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_face_data_ID(int $face_data_ID=NULL){

        if(empty($face_data_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face data ID is empty'
            ];

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($face_data_ID,self::$table_name,0);

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_face_data(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'=>$face_ID
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int $day_len
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_reg_count_face_data_list(int $day_len=10){

        $date_time=Date::get_date_for_db(Date::get_timestamp()-$day_len*24*3600).' 00:00:00';

        $q=[
            'select'=>[
                [
                    'function'=>'COUNT(id) as profit'
                ],
                [
                    'function'=>"DATE(date_create AT TIME ZONE 'UTC+3') as date_add"
                ]
            ],
            'table'=>Face::$table_name,
            'where'=>[
                'date_create'       =>[
                    'method'    =>'>=',
                    'value'     =>$date_time
                ],
                'type'          =>0
            ],
            'group'=>[
                [
                    'function'=>"DATE(date_create AT TIME ZONE 'UTC+3')"
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        $list=[];

        foreach($r as $row)
            $list[$row['date_add']]=(int)$row['profit'];

        return $list;

    }

    /**
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_parsing_date_stats(){

        $q=[
            'select'=>[
                [
                    'column'=>'source_id'
                ],
                [
                    'function'=>'COUNT(source_id) as len'
                ]
            ],
            'table'=>self::$table_name,
            'where'=>[
                [
                    'function'=>"DATE(date_create AT TIME ZONE 'UTC+3')=DATE(NOW())"
                ],
                [
                    'column'    =>'type',
                    'value'     =>0
                ]
            ],
            'group'=>[
                [
                    'function'=>"source_id"
                ]
            ],
            'order'=>[
                [
                    'column'        =>'len',
                    'direction'     =>'desc'
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['source_id']]=(int)$row['len'];

        return $list;

    }

    /**
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_parsing_all_stats(){

        $q=[
            'select'=>[
                [
                    'column'=>'source_id'
                ],
                [
                    'function'=>'COUNT(source_id) as len'
                ]
            ],
            'table'=>self::$table_name,
            'where'=>[
                [
                    'column'    =>'type',
                    'value'     =>0
                ]
            ],
            'group'=>[
                [
                    'function'=>"source_id"
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['source_id']]=(int)$row['len'];

        return $list;

    }

    /**
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_parsing_date_stats_in_one_hour(){

        $q=[
            'select'=>[
                [
                    'column'=>'source_id'
                ],
                [
                    'function'=>'COUNT(source_id) as len'
                ]
            ],
            'table'=>self::$table_name,
            'where'=>[
                [
                    'function'=>"date_create AT TIME ZONE 'UTC+3'>=NOW() AT TIME ZONE 'UTC-2'"
                ],
                [
                    'column'    =>'type',
                    'value'     =>0
                ]
            ],
            'group'=>[
                [
                    'function'=>"source_id"
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['source_id']]=(int)$row['len'];

        return $list;

    }

    /**
     * @param int $len
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_city_len_list(int $len=3){

        $q=[
            'select'=>[
                [
                    'column'=>'city_id'
                ],
                [
                    'function'=>'COUNT(*) as len'
                ]
            ],
            'table'=>self::$table_name,
            'where'=>[
                'city_id'       =>[
                    'method'        =>'!=',
                    'value'         =>NULL
                ],
                'type'          =>0
            ],
            'group'=>[
                [
                    'column'=>'city_id'
                ]
            ],
            'order'=>[
                [
                    'column'        =>'len',
                    'direction'     =>'desc'
                ]
            ],
            'limit'=>$len
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['city_id']]=(int)$row['len'];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_ID(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'=>$face_ID
        ];

        return Db::get_row_ID(self::$table_name,0,$where_list);

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_city_stats_list(){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                [
                    'column'=>'city_id'
                ],
                [
                    'function'=>'COUNT(*) as "face_len"'
                ],
                [
                    'function'=>'SUM(image_len) as "image_len"'
                ]
            ],
            'where'=>[
                'type'=>0
            ],
            'group'=>[
                [
                    'column'=>'city_id'
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['city_id']]=[
                'city_ID'       =>$row['city_id'],
                'face_len'      =>$row['face_len'],
                'image_len'     =>$row['image_len']
            ];

        return $list;

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_stats_len(){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                [
                    'function'=>'COUNT(id) as "face_len"'
                ],
                [
                    'function'=>'SUM(image_len) as "image_len"'
                ]
            ],
            'where'=>[
                'type'=>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        return[
            'face_len'  =>$r[0]['face_len'],
            'image_len' =>$r[0]['image_len']
        ];

    }

    /**
     * @return false|int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_last_date_update(){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'date_update'
            ],
            'where'=>[
                'type'=>0
            ],
            'order'=>[
                [
                    'column'    =>'date_update',
                    'direction' =>'desc'
                ]
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return empty($r[0]['date_update'])?NULL:strtotime($r[0]['date_update']);

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_ID_list(){

        $q=[
            'select'=>[
                'face_id'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'type'=>0
            ],
            'order'=>[
                [
                    'column'        =>'id',
                    'direction'     =>'asc'
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['face_id'];

        return $list;

    }

    /**
     * @param array $face_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_list_for_root(array $face_ID_list=[]){

        if(count($face_ID_list)==0)
            return[];

        $q=[
            'select'=>[
                'face_id',
                'name',
                'age',
                'city'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'face_id'   =>$face_ID_list,
                'type'      =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[Hash::get_sha1_encode($row['face_id'])]=$row['name'].(empty($row['age'])?'':', '.$row['age']).(empty($row['city'])?'':', '.$row['city']);

        return $list;

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_ID_list_with_zero_image_facekit_len(){

        $q=[
            'select'=>[
                'face_id'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'image_facekit_len'     =>0,
                'type'                  =>0
            ],
            'order'=>[
                [
                    'column'        =>'face_id',
                    'direction'     =>'asc'
                ]
            ]
        ];

        if(!empty($_POST['face_ID']))
            $q['where']['face_id']=[
                'method'        =>'>=',
                'value'         =>(int)$_POST['face_ID']
            ];

        if(!empty($_POST['len']))
            $q['limit']=(int)$_POST['len'];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['face_id'];

        return $list;

    }

    /**
     * @param int|NULL $source_ID
     * @param int $start
     * @param int $len
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_ID_list_with_zero_image_facekit_len_with_source_ID(int $source_ID=NULL,int $start=0,int $len=100){

        $q=[
            'select'=>[
                'face_id'
            ],
            'table'=>self::$table_name,
            'where'=>[
//                'image_facekit_len'     =>0,
                'is_send_to_fpp'        =>0,
                'type'                  =>0
            ],
            'order'=>[
                [
                    'column'        =>'face_id',
                    'direction'     =>'asc'
                ]
            ],
            'limit'=>[
                $start,
                $len
            ]
        ];

        if(!empty($_POST['face_ID']))
            $q['where']['face_id']=[
                'method'        =>'>=',
                'value'         =>(int)$_POST['face_ID']
            ];

        if(!empty($source_ID))
            $q['where']['source_id']=[
                'method'        =>'=',
                'value'         =>$source_ID
            ];

        if(!empty($_POST['len']))
            $q['limit']=(int)$_POST['len'];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['face_id'];

        return $list;

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_source_account_link_list_with_one_image(){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'face_id',
                'source_account_link',
                'image_len'
            ],
            'where'=>[
                'image_len'=>[
                    'method'    =>'<=',
                    'value'     =>2
                ],
                'type'=>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'face_ID'               =>$row['face_id'],
                'source_account_link'   =>$row['source_account_link'],
                'image_len'             =>$row['image_len']
            ];

        return $list;

    }

    /**
     * @param array $source_ID_list
     * @param int|NULL $offset
     * @param int|NULL $len
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_info_list(array $source_ID_list=[],int $offset=1,int $len=100){

        if(count($source_ID_list)==0)
            return[];

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'face_id',
                'info'
            ],
            'where'=>[
                'source_id'     =>$source_ID_list,
                'type'          =>0
            ],
            'limit'=>[$offset,$len]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['face_id']]=$row['info'];

        return $list;

    }

    /**
     * @param int $offset
     * @param int $len
     * @param int|NULL $face_ID
     * @param int|NULL $source_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_info_list_with_not_check_info(int $offset=0,int $len=100,int $face_ID=NULL,int $source_ID=NULL){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'face_id',
                'info'
            ],
            'where'=>[
                'is_check_info'     =>0,
                'type'              =>0
            ],
            'order'=>[
                [
                    'column'    =>'face_id',
                    'direction' =>'asc'
                ]
            ],
            'limit'=>[$offset,$len]
        ];

        if(!empty($face_ID))
            $q['where']['face_id']=$face_ID;

        if(!empty($source_ID))
            $q['where']['source_id']=$source_ID;

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['face_id']]=$row['info'];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @param string|NULL $info_dev
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_info_dev(int $face_ID=NULL,string $info_dev=NULL){

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'info_dev'      =>$info_dev,
                'is_check_info' =>1
            ],
            'where'=>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data dev info was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_is_check_info(int $face_ID=NULL){

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'is_check_info' =>1
            ],
            'where'=>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data dev info was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data(int $face_ID=NULL){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id',
                'face_id',
                'source_account_link',
                'image_len'
            ],
            'where'=>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'ID'                        =>$r[0]['id'],
            'face_ID'                   =>$r[0]['face_id'],
            'source_account_link'       =>$r[0]['source_account_link'],
            'image_len'                 =>$r[0]['image_len']
        ];

    }

    /**
     * @param int $len
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_with_empty_city_ID(int $len=100){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id',
                'face_id',
                'city'
            ],
            'where'=>[
                'is_check_city'     =>0,
                'city'              =>[
                    'method'    =>'!=',
                    'value'     =>NULL
                ],
                'type'          =>0
            ],
            'limit'=>$len
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'ID'        =>$row['id'],
                'face_ID'   =>$row['face_id'],
                'city'      =>$row['city'],
            ];

        return $list;

    }

    /**
     * @param int|NULL $face_data_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_from_face_data_ID(int $face_data_ID=NULL){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'name',
                'age',
                'info',
                'city',
                'source_account_link',
                'face_data_type_id'
            ],
            'where'=>[
                'id'=>$face_data_ID
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'face_data_type_ID'         =>empty($r[0]['face_data_type_id'])?NULL:$r[0]['face_data_type_id'],
            'name'                      =>empty($r[0]['name'])?NULL:$r[0]['name'],
            'age'                       =>empty($r[0]['age'])?NULL:$r[0]['age'],
            'info'                      =>empty($r[0]['info'])?NULL:$r[0]['info'],
            'city'                      =>empty($r[0]['city'])?NULL:$r[0]['city'],
            'source_account_link'       =>empty($r[0]['source_account_link'])?NULL:$r[0]['source_account_link']
        ];

    }

    /**
     * @param array $face_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_list_from_face_ID_list(array $face_ID_list=[]){

        if(count($face_ID_list)==0)
            return[];

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id',
                'face_id',
                'source_id',
                'name',
                'age',
                'city',
                'info',
                'source_account_link',
                'image_len',
                'video_len',
                'face_data_type_id'
            ],
            'where'=>[
                'face_id'       =>$face_ID_list,
                'type'          =>0
            ],
            'order'=>[
                [
                    'column'        =>'image_len',
                    'direction'     =>'desc'
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['face_id']]=[
                'ID'                        =>$row['id'],
                'face_ID'                   =>$row['face_id'],
                'source_ID'                 =>$row['source_id'],
                'face_data_type_ID'         =>$row['face_data_type_id'],
                'name'                      =>$row['name'],
                'age'                       =>$row['age'],
                'city'                      =>$row['city'],
                'info'                      =>$row['info'],
                'source_account_link'       =>$row['source_account_link'],
                'image_len'                 =>$row['image_len'],
                'video_len'                 =>$row['video_len']
            ];

        return $list;

    }

    /**
     * @param array $face_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_source_link_list_from_face_ID_list(array $face_ID_list=[]){

        if(count($face_ID_list)==0)
            return[];

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'face_id',
                'source_account_link'
            ],
            'where'=>[
                'face_id'       =>$face_ID_list,
                'type'          =>0
            ],
            'order'=>[
                [
                    'column'        =>'image_len',
                    'direction'     =>'desc'
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['face_id']]=$row['source_account_link'];

        return $list;

    }

    /**
     * @param int|NULL $model_zone_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_ID_from_model_zone_ID(int $model_zone_ID=NULL){

        if(empty($model_zone_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Model zone ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'select'=>[
                'id',
                'face_id',
                'source_account_link'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'source_account_link'=>[
                    'function'=>"LIKE '%/".$model_zone_ID."'"
                ],
                'type'=>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(empty($r))
            return NULL;

        return $r[0]['face_id'];

    }

    /**
     * @param int|NULL $face_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_and_date_create(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'image_id',
                'date_create'
            ],
            'where'=>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(empty($r))
            return NULL;

        return[
            'image_ID'      =>$r[0]['image_id'],
            'date_create'   =>$r[0]['date_create']
        ];

    }

    /**
     * @param int|NULL $face_ID
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'image_id'
            ],
            'where'=>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(empty($r))
            return NULL;

        return $r[0]['image_id'];

    }

    /**
     * @param int|NULL $image_ID
     * @param int $limit
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_list_from_min_image_ID_new(int $image_ID=NULL,int $limit=100){

        $q=[
            'select'    =>[
                'image_id',
                'face_id'
            ],
            'table'     =>self::$table_name,
            'where'     =>[
                [
                    'column'        =>'image_id',
                    'method'        =>'>=',
                    'value'         =>$image_ID
                ],
                [
                    'column'        =>'face_id',
                    'method'        =>'!=',
                    'value'         =>NULL
                ],
                [
                    'column'        =>'type',
                    'value'         =>0
                ]
            ],
            'limit'=>$limit
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['image_id']]=$row['face_id'];

        return $list;

    }


    /**
     * @param string|NULL $source_account_link
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_source_account_link(string $source_account_link=NULL,int $face_ID=NULL){

        if(empty($source_account_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face data ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'source_account_link'=>$source_account_link
        ];

        if(!empty($face_ID))
            $where_list['face_id']=[
                'method'        =>'!=',
                'value'         =>$face_ID
            ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $source_ID
     * @param string|NULL $link_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_link_ID(int $source_ID=NULL,string $link_ID=NULL){

        $error_info_list=[];

        if(empty($source_ID))
            $error_info_list[]='Source ID is empty';

        if(empty($link_ID))
            $error_info_list[]='Link ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'source_id'     =>$source_ID,
            'link_id'       =>$link_ID
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param string|NULL $source_account_link
     * @return string|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_ID_from_source_account_link(string $source_account_link=NULL){

        if(empty($source_account_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face data ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'select'=>[
                'face_id'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'source_account_link'   =>$source_account_link,
                'type'                  =>[0,1,2]
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['face_id'];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_image_ID(int $face_ID=NULL,int $image_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $image_len              =FaceImage::get_image_len($face_ID);
        $image_facekit_len      =FaceImage::get_facekit_image_len($face_ID);

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'image_id'              =>$image_ID,
                'image_len'             =>$image_len,
                'image_facekit_len'     =>$image_facekit_len,
                'date_update'           =>'NOW()'
            ],
            'where'=>[
                'face_id'   =>$face_ID,
                'type'      =>[0,2]
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_image_ID_face_plus_plus(int $face_ID=NULL,int $image_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $image_len              =FaceImage::get_image_len($face_ID);
        $image_facekit_len      =FaceImage::get_face_plus_plus_image_len($face_ID);

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'image_id'              =>$image_ID,
                'image_len'             =>$image_len,
                'image_facekit_len'     =>$image_facekit_len,
                'date_update'           =>'NOW()'
            ],
            'where'=>[
                'face_id'   =>$face_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $country_ID
     * @param int|NULL $city_ID
     * @param string|NULL $city
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_city_and_country(int $face_ID=NULL,int $country_ID=NULL,int $city_ID=NULL,string $city=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'country_id'            =>$country_ID,
                'city_id'               =>$city_ID,
                'city'                  =>$city,
                'is_check_city'         =>1,
                'date_update'           =>'NOW()'
            ],
            'where'=>[
                'face_id'   =>$face_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_check_city(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'is_check_city'         =>1,
                'date_update'           =>'NOW()'
            ],
            'where'=>[
                'face_id'   =>$face_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $source_ID
     * @param string|NULL $link_ID
     * @param int|NULL $face_data_type_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_type(int $source_ID=NULL,string $link_ID=NULL,int $face_data_type_ID=NULL){

        $error_info_list=[];

        if(empty($source_ID))
            $error_info_list[]='Source ID is empty';

        if(empty($link_ID))
            $error_info_list[]='Link ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'face_data_type_id'     =>$face_data_type_ID,
                'date_update'           =>'NOW()',
            ],
            'where'=>[
                'source_id'     =>$source_ID,
                'link_id'       =>$link_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param bool $is_check
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_check_goal(int $face_ID=NULL,bool $is_check=true){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'is_check_goal'     =>(int)$is_check,
                'date_update'       =>'NOW()',
            ],
            'where'=>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param bool $is_check
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_check_video(int $face_ID=NULL,bool $is_check=true){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'is_check_video'    =>(int)$is_check,
                'date_update'       =>'NOW()',
            ],
            'where'=>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param bool $is_check
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_check_duplicate_face(int $face_ID=NULL,bool $is_check=true){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'is_check_duplicate_face'   =>(int)$is_check,
                'date_update'               =>'NOW()',
            ],
            'where'=>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_image_len(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $image_len              =FaceImage::get_image_len($face_ID);
        $image_facekit_len      =FaceImage::get_facekit_image_len($face_ID);

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'image_len'             =>$image_len,
                'image_facekit_len'     =>$image_facekit_len,
                'date_update'           =>'NOW()'
            ],
            'where'=>[
                'face_id'=>$face_ID
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_video_len(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $video_len=FaceVideo::get_video_len($face_ID);

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'video_len'     =>$video_len,
                'date_update'   =>'NOW()'
            ],
            'where'=>[
                'face_id'=>$face_ID
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_face_plus_plus_image_len(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $image_len                  =FaceImage::get_image_len($face_ID);
        $image_face_plus_plus_len   =FaceImage::get_face_plus_plus_image_len($face_ID);

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'image_len'             =>$image_len,
                'image_facekit_len'     =>$image_face_plus_plus_len,
                'is_send_to_fpp'        =>1,
                'date_update'           =>'NOW()'
            ],
            'where'=>[
                'face_id'=>$face_ID
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param int $start
     * @param int $len
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_list(int $face_ID=NULL,int $start=0,int $len=10){

        $face_ID_list=[];

        if(!empty($_POST['is_not_one_face'])){

            if(empty($_POST['source_ID'])){

                $q=[
                    'table'=>FaceImage::$table_name,
                    'select'=>[
                        'face_id'
                    ],
                    'where'=>[
                        'face_count'    =>[
                            'method'        =>'>',
                            'value'         =>1
                        ],
                        'type'=>0
                    ],
                    'group'=>[
                        [
                            'column'=>'face_id'
                        ]
                    ],
                    'order'=>[
                        [
                            'column'        =>'face_id',
                            'direction'     =>'desc'
                        ]
                    ],
                    'limit'=>[
                        $start,
                        $len
                    ]
                ];

                if(!empty($face_ID))
                    $q['where']['face_id']=[
                        'method'    =>'<=',
                        'value'     =>$face_ID
                    ];

            }
            else{

                \Config::$is_debug=true;

                $q=[
                    'table'=>FaceImage::$table_name,
                    'select'=>[
                        [
                            'table'         =>FaceImage::$table_name,
                            'column'        =>'face_id',
                        ]
                    ],
                    'join'=>[
                        [
                            'table'=>FaceData::$table_name,
                            'where'=>[
                                [
                                    'table'         =>FaceImage::$table_name,
                                    'table_join'    =>FaceData::$table_name,
                                    'method'        =>'=',
                                    'column'        =>'face_id',
                                    'column_join'   =>'face_id'
                                ],
                                [
                                    'table'=>FaceData::$table_name,
                                    'column'=>'source_id',
                                    'value'=>(int)$_POST['source_ID']
                                ],
                                [
                                    'table'=>FaceData::$table_name,
                                    'column'=>'type',
                                    'value'=>0
                                ]
                            ]
                        ]
                    ],
                    'where'=>[
                        [
                            'table'         =>FaceImage::$table_name,
                            'column'        =>'face_count',
                            'method'        =>'>',
                            'value'         =>1
                        ],
                        [
                            'table'         =>FaceImage::$table_name,
                            'column'        =>'type',
                            'value'         =>0
                        ],
                    ],
                    'group'=>[
                        [
                            'table'         =>FaceImage::$table_name,
                            'column'        =>'face_id',
                        ]
                    ],
                    'order'=>[
                        [
                            'table'         =>FaceImage::$table_name,
                            'column'        =>'face_id',
                            'direction'     =>'desc'
                        ]
                    ],
                    'limit'=>[
                        $start,
                        $len
                    ]
                ];

                if(!empty($face_ID))
                    $q['where'][]=[
                        'table'         =>FaceImage::$table_name,
                        'column'        =>'face_id',
                        'method'        =>'<=',
                        'value'         =>$face_ID
                    ];

            }

            $r=Db::select($q);

//            print_r($r);
//            echo Db::$query_last;

            if(count($r)==0)
                return[];

            foreach($r as $row)
                $face_ID_list[]=$row['face_id'];

            $start=0;

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'city_id',
                'face_id',
                'image_id',
                'source_id',
                'link_id',
                'face_data_type_id',
                'source_account_link',
                'name',
                'age',
                'city',
                'info',
                'info_dev',
                'image_len',
                'image_facekit_len',
                'video_len',
                'date_create'
            ],
            'where'     =>[
                [
                    'column'    =>'type',
                    'value'     =>0
                ]
            ],
            'order'     =>[],
            'limit'     =>[
                $start,
                $len
            ]
        ];

        if(count($face_ID_list)>0)
            $q['where'][]=[
                'column'    =>'face_id',
                'value'     =>$face_ID_list
            ];

        if(!empty($face_ID))
            $q['where'][]=[
                'column'    =>'face_id',
                'method'    =>'<=',
                'value'     =>$face_ID
            ];

        if(!empty($_POST['source_ID']))
            $q['where'][]=[
                'column'    =>'source_id',
                'method'    =>'=',
                'value'     =>(int)$_POST['source_ID']
            ];

        if(!empty($_POST['level']))
            $q['where'][]=[
                'column'    =>'face_data_type_id',
                'method'    =>'=',
                'value'     =>(int)$_POST['level']
            ];

        if(!empty($_POST['is_not_city']))
            $q['where'][]=[
                'column'    =>'city',
                'method'    =>'=',
                'value'     =>NULL
            ];

        if(!empty($_POST['is_not_age']))
            $q['where'][]=[
                'column'    =>'age',
                'method'    =>'=',
                'value'     =>NULL
            ];

        if(!empty($_POST['sort_by'])){

            switch($_POST['sort_by']){

                case'image_count':{

                    $q['order'][]=[
                        'column'    =>'image_len',
                        'direction' =>isset($_POST['sort_direction'])?($_POST['sort_direction']=='asc'?'asc':'desc'):'asc'
                    ];

                    $q['order'][]=[
                        'column'    =>'face_id',
                        'direction' =>'desc'
                    ];

//                    print_r($q);

                    break;

                }

                case'image_uploaded_count':{

                    $q['order'][]=[
                        'column'    =>'image_facekit_len',
                        'direction' =>isset($_POST['sort_direction'])?($_POST['sort_direction']=='asc'?'asc':'desc'):'asc'
                    ];

                    $q['order'][]=[
                        'column'    =>'face_id',
                        'direction' =>'desc'
                    ];

                    break;

                }

            }

        }
        else
            $q['order'][]=[
                'column'    =>'face_id',
                'direction' =>'desc'
            ];

        if(!empty($_POST['source_link']))
            $q['where'][]=[
                'column'    =>'source_account_link',
                'method'    =>'like',
                'value'     =>$_POST['source_link']
            ];

        if(isset($_POST['is_uploaded_image']))
            $q['where'][]=[
                'column'    =>'is_send_to_fpp',
                'method'    =>'=',
                'value'     =>(int)$_POST['is_uploaded_image']
            ];

        if(!empty($_POST['city']))
            $q['where'][]=[
                'function'=>"lower(city) like '%".mb_strtolower($_POST['city'],'utf-8')."%'"
            ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'ID'                        =>$row['id'],
                'city_ID'                   =>$row['city_id'],
                'face_ID'                   =>$row['face_id'],
                'image_ID'                  =>$row['image_id'],
                'source_ID'                 =>$row['source_id'],
                'face_data_type_ID'         =>$row['face_data_type_id'],
                'link_ID'                   =>$row['link_id'],
                'source_account_link'       =>$row['source_account_link'],
                'name'                      =>$row['name'],
                'age'                       =>$row['age'],
                'city'                      =>$row['city'],
                'info'                      =>$row['info'],
                'info_dev'                  =>$row['info_dev'],
                'image_len'                 =>$row['image_len'],
                'image_facekit_len'         =>$row['image_facekit_len'],
                'video_len'                 =>$row['video_len'],
                'date_create'               =>Date::get_timestamp($row['date_create'])
            ];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $source_ID
     * @param int $start
     * @param int $len
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_source_link_list(int $face_ID=NULL,int $source_ID=NULL,int $start=0,int $len=10){

        if(empty($source_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'face_id',
                'source_account_link'
            ],
            'where'     =>[
                [
                    'column'    =>'age',
                    'value'     =>NULL
                ],
                [
                    'column'    =>'city',
                    'value'     =>NULL
                ],
                [
                    'column'    =>'info',
                    'value'     =>NULL
                ],
                [
                    'column'    =>'source_account_link',
                    'method'    =>'!=',
                    'value'     =>NULL
                ],
                [
                    'column'    =>'source_id',
                    'value'     =>$source_ID
                ],
                [
                    'column'    =>'type',
                    'value'     =>0
                ]
            ],
            'order'=>[
                [
                    'column'    =>'face_id',
                    'direction' =>'asc'
                ]
            ],
            'limit'=>[
                $start,
                $len
            ]
        ];

        if(!empty($face_ID))
            $q['where'][]=[
                'column'    =>'face_id',
                'method'    =>'>=',
                'value'     =>$face_ID
            ];


        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'ID'                        =>$row['id'],
                'face_ID'                   =>$row['face_id'],
                'source_account_link'       =>$row['source_account_link']
            ];

        return $list;

    }

    /**
     * @param array $source_ID_list
     * @param int $offset
     * @param int $len
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_link_from_source_IDs(array $source_ID_list=[],int $offset=1,int $len=100){

        if(empty($source_ID_list)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'face_id',
                'source_account_link'
            ],
            'where'     =>[
                'source_id'     =>$source_ID_list,
                'type'          =>0
            ],
            'order'=>[
                [
                    'column'    =>'face_id',
                    'direction' =>'asc'
                ]
            ],
            'limit'=>[
                $offset,
                $len
            ]
        ];

        if(!empty($face_ID))
            $q['where'][]=[
                'column'    =>'face_id',
                'method'    =>'>=',
                'value'     =>$face_ID
            ];


        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['face_id']]=$row['source_account_link'];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $city_ID
     * @param int|NULL $image_ID
     * @param int|NULL $source_ID
     * @param string|NULL $source_account_link
     * @param string|NULL $name
     * @param int|NULL $age
     * @param string|NULL $city
     * @param string|NULL $info
     * @param int $image_len
     * @param int $image_facekit_len
     * @param string|NULL $date_create
     * @param int $type
     * @param string|NULL $link_ID
     * @param int|NULL $face_data_type_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_data(int $face_ID=NULL,int $city_ID=NULL,int $image_ID=NULL,int $source_ID=NULL,string $source_account_link=NULL,string $name=NULL,int $age=NULL,string $city=NULL,string $info=NULL,int $image_len=0,int $image_facekit_len=0,string $date_create=NULL,int $type=2,string $link_ID=NULL,int $face_data_type_ID=NULL){

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'                           =>User::$user_ID,
                'city_id'                           =>empty($city_ID)?NULL:$city_ID,
                'face_id'                           =>$face_ID,
                'image_id'                          =>empty($image_ID)?NULL:$image_ID,
                'source_id'                         =>$source_ID,
                'source_account_link'               =>empty($source_account_link)?NULL:$source_account_link,
                'name'                              =>$name,
                'age'                               =>$age,
                'city'                              =>empty($city)?NULL:$city,
                'info'                              =>empty($info)?NULL:$info,
                'info_dev'                          =>empty($info)?NULL:$info,
                'image_len'                         =>$image_len,
                'image_facekit_len'                 =>$image_facekit_len,
                'link_id'                           =>empty($link_ID)?NULL:$link_ID,
                'face_data_type_id'                 =>empty($face_data_type_ID)?NULL:$face_data_type_ID,
                'date_create'                       =>empty($date_create)?'NOW()':$date_create,
                'date_update'                       =>'NOW()',
                'type'                              =>$type
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not add'
            ];

            throw new PhpException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_data_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_to_public(int $face_data_ID=NULL){

        if(empty($face_data_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face data ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'date_update'   =>'NOW()',
                'type'          =>0
            ],
            'where'=>[
                'id'        =>$face_data_ID,
                'type'      =>[0,2]
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $country_ID
     * @param int|NULL $city_ID
     * @param string|NULL $name
     * @param int|NULL $age
     * @param string|NULL $city
     * @param string|NULL $info
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data(int $face_ID=NULL,int $country_ID=NULL,int $city_ID=NULL,string $name=NULL,int $age=NULL,string $city=NULL,string $info=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'country_id'    =>empty($country_ID)?NULL:$country_ID,
                'city_id'       =>empty($city_ID)?NULL:$city_ID,
                'name'          =>empty($name)?NULL:$name,
                'age'           =>empty($age)?NULL:$age,
                'city'          =>empty($city)?NULL:$city,
                'info'          =>empty($info)?NULL:$info,
                'date_update'   =>'NOW()',
                'type'          =>0
            ],
            'where'=>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param string|NULL $info
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_data_info(int $face_ID=NULL,string $info=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'info'          =>empty($info)?NULL:$info
            ],
            'where'=>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_data_ID
     * @param bool $is_need_remove_child
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_data_ID(int $face_data_ID=NULL,bool $is_need_remove_child=true){

        if(empty($face_data_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face data ID is empty'
            ];

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($face_data_ID,self::$table_name,0)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_data_from_face_ID(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'=>$face_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param array $face_ID_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_data_from_face_ID_list(array $face_ID_list=[]){

        if(count($face_ID_list)==0)
            return true;

        $where_list=[
            'face_id'=>$face_ID_list
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

}