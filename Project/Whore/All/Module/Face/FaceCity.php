<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;

class FaceCity{

    /** @var string */
    public  static $table_name='face_city';

    /**
     * @param int|NULL $city_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_face_city(int $city_ID=NULL){

        if(empty($city_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'City ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'city_id'=>$city_ID
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int $len
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_city_stats_list(int $len=4){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'city_name',
                'face_len',
//                'image_len'
            ],
            'where'=>[
                'type'=>0
            ],
            'order'=>[
                [
                    'column'        =>'face_len',
                    'direction'     =>'desc'
                ]
            ],
            'limit'=>$len
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'name'          =>$row['city_name'],
                'len'           =>$row['face_len'],
//                'image_len'     =>$row['image_len'],
            ];

        return $list;

    }

    /**
     * @param int|NULL $city_ID
     * @param int|NULL $face_len
     * @param int|NULL $image_len
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_len(int $city_ID=NULL,int $face_len=NULL,int $image_len=NULL){

        if(empty($city_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'City ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'set'=>[
                'face_len'      =>(int)$face_len,
                'image_len'     =>(int)$image_len,
                'date_update'   =>'NOW()'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'city_id'   =>$city_ID,
                'type'      =>0
            ],
            'limit'=>1
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face city len was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $country_ID
     * @param int|NULL $city_ID
     * @param string|NULL $country_name
     * @param string|NULL $city_name
     * @param int|NULL $face_len
     * @param null $image_len
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_city(int $country_ID=NULL,int $city_ID=NULL,string $country_name=NULL,string $city_name=NULL,int $face_len=NULL,$image_len=NULL){

        if(empty($city_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'City ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'       =>User::$user_ID,
                'country_id'    =>$country_ID,
                'city_id'       =>$city_ID,
                'country_name'  =>$country_name,
                'city_name'     =>$city_name,
                'face_len'      =>(int)$face_len,
                'image_len'     =>(int)$image_len,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>0
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face city len was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

}