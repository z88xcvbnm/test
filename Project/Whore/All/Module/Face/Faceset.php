<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class Faceset{

    /** @var string */
    public  static $table_name='faceset';

    /**
     * @param string|NULL $faceset_token
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_faceset_ID(string $faceset_token=NULL){

        if(empty($faceset_token)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Faceset token is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_pp_faceset_token'=>$faceset_token
        ];

        return Db::get_row_ID(self::$table_name,0,$where_list);

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_faceset_list(){

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'face_pp_faceset_token'
            ],
            'where'     =>[
                'type'=>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'faceset_ID'        =>$row['id'],
                'faceset_token'     =>$row['face_pp_faceset_token']
            ];

        return $list;

    }

    /**
     * @param int|NULL $index
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_faceset_data_from_index(int $index=NULL){

        if(empty($index)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Index is empty'
            ];

            throw new ParametersException($error);

        }

        $index--;

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'face_pp_faceset_token'
            ],
            'where'     =>[
                'type'=>0
            ],
            'order'=>[
                [
                    'column'        =>'id',
                    'direction'     =>'asc'
                ]
            ],
            'limit'=>[$index,1]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'faceset_ID'        =>$r[0]['id'],
            'faceset_token'     =>$r[0]['face_pp_faceset_token']
        ];

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_faceset_len(){

        return Db::get_row_len(self::$table_name,0);

    }

    /**
     * @param int|NULL $need_add_face_len
     * @return array|null
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_last_faceset_data(int $need_add_face_len=NULL){

        if(empty($need_add_face_len))
            $need_add_face_len=1;

        $face_max_len=FacePlusPlusConfig::$face_token_max_len-$need_add_face_len;

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id',
                'name',
                'face_pp_faceset_token',
                'face_len'
            ],
            'where'=>[
                'face_len'=>[
                    'method'    =>'<=',
                    'value'     =>$face_max_len
                ],
                'type'=>0
            ],
            'order'=>[
                [
                    'column'    =>'id',
                    'direction' =>'desc'
                ]
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'faceset_ID'        =>$r[0]['id'],
            'faceset_token'     =>$r[0]['face_pp_faceset_token'],
            'face_len'          =>$r[0]['face_len']
        ];

    }

    /**
     * @param string|NULL $name
     * @param string|NULL $faceset_token
     * @param int|NULL $face_len
     * @param int $type
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_faceset(string $name=NULL,string $faceset_token=NULL,int $face_len=NULL,int $type=0){

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'                   =>User::$user_ID,
                'name'                      =>empty($name)?NULL:$name,
                'face_pp_faceset_token'     =>empty($faceset_token)?NULL:$faceset_token,
                'face_len'                  =>empty($face_len)?0:$face_len,
                'date_create'               =>'NOW()',
                'date_update'               =>'NOW()',
                'type'                      =>$type
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Faceset was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $faceset_ID
     * @param bool $is_plus
     * @param int|NULL $face_len
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_len(int $faceset_ID=NULL,bool $is_plus=true,int $face_len=NULL){

        if(empty($face_len))
            return true;

        if(empty($faceset_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Faceset ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table' =>self::$table_name,
            'set'   =>[
                [
                    'function'=>'face_len=face_len'.($is_plus?'+':'-').$face_len
                ],
                [
                    'column'    =>'date_update',
                    'value'     =>'NOW()'
                ]
            ],
            'where' =>[
                'id'        =>$faceset_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Faceset was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $faceset_ID
     * @param int|NULL $face_len
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_faceset_face_len(int $faceset_ID=NULL,int $face_len=NULL){

        if(empty($faceset_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Faceset ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table' =>self::$table_name,
            'set'   =>[
                'face_len'      =>$face_len,
                'date_update'   =>'NOW()'
            ],
            'where' =>[
                'id'        =>$faceset_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Faceset was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $faceset_ID
     * @param string|NULL $faceset_token
     * @param int $type
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_faceset_token(int $faceset_ID=NULL,string $faceset_token=NULL,int $type=0){

        $error_info_list=[];

        if(empty($faceset_ID))
            $error_info_list[]='Faceset ID is empty';

        if(empty($faceset_token))
            $error_info_list[]='Faceset token is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table' =>self::$table_name,
            'set'   =>[
                'face_pp_faceset_token'     =>$faceset_token,
                'date_update'               =>'NOW()',
                'type'                      =>$type
            ],
            'where' =>[
                'id'        =>$faceset_ID
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Faceset was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $faceset_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_faceset(int $faceset_ID=NULL){

        if(empty($faceset_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Faceset ID is empty'
            ];

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($faceset_ID,self::$table_name,0)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Faceset was not remove'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param array $except_faceset_ID_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_faceset_except_faceset_ID_list(array $except_faceset_ID_list=[]){

        $where_list=NULL;

        if(count($except_faceset_ID_list)!=0)
            $where_list=[
                'id'=>[
                    'method'    =>'!=',
                    'value'     =>$except_faceset_ID_list
                ]
            ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Facesets were not remove'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

}