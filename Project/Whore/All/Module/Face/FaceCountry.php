<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;

class FaceCountry{

    /** @var string */
    public  static $table_name='face_country';

    /**
     * @param int|NULL $country_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_country_ID(int $country_ID=NULL){

        if(empty($country_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Country ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'country_id'=>$country_ID
        ];

        return Db::get_row_ID(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $country_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_len(int $country_ID=NULL){

        if(empty($country_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Country ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'select'=>[
                'face_len'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'country_id'    =>$country_ID,
                'type'          =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(empty($r))
            return NULL;

        return $r[0]['face_len'];

    }

    /**
     * @param int|NULL $country_ID
     * @param bool $need_plus
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_len(int $country_ID=NULL,bool $need_plus=true){

        if(empty($country_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Country ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'set'=>[
                'face_len'=>[
                    'function'=>'face_len'.($need_plus?'+':'-').'1'
                ],
                'date_update'=>'NOW()'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'country_id'    =>$country_ID,
                'type'          =>0
            ],
            'limit'=>1
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face len was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $country_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_country(int $country_ID=NULL){

        if(empty($country_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Country ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'       =>User::$user_ID,
                'country_id'    =>$country_ID,
                'face_len'      =>0,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>0
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face len was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

}