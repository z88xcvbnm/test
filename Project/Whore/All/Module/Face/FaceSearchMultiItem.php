<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;
use Core\Module\User\User;

class FaceSearchMultiItem{

    /** @var string */
    public  static $table_name='face_search_multi_item';

    /**
     * @param array|NULL $face_search_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_item_list(array $face_search_ID_list=NULL){

        if(empty($face_search_ID_list))
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'face_search_id',
                'face_search_multi_id',
                'face_id',
                'image_id',
                'token',
                'percent'
            ],
            'where'     =>[
                'face_search_multi_id'  =>$face_search_ID_list,
                'type'                  =>0
            ],
            'order'=>[
                [
                    'column'    =>'percent',
                    'direction' =>'desc'
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row){

            if(empty($list[$row['face_search_multi_id']]))
                $list[$row['face_search_multi_id']]=[
                    'face_search_multi_ID'  =>$row['face_search_multi_id'],
                    'face_list'             =>[]
                ];

            if(!isset($list[$row['face_search_multi_id']]['face_list'][$row['face_id']]))
                $list[$row['face_search_multi_id']]['face_list'][$row['face_id']]=[
                    'ID'            =>$row['id'],
                    'face_ID'       =>$row['face_id'],
                    'image_ID'      =>$row['image_id'],
                    'face_token'    =>$row['token'],
                    'percent'       =>ceil($row['percent'])
                ];
            else{

                $percent=ceil($row['percent']);

                if($list[$row['face_search_multi_id']]['face_list'][$row['face_id']]['percent']<$percent){

                    $list[$row['face_search_multi_id']]['face_list'][$row['face_id']]['image_ID']     =$row['image_id'];
                    $list[$row['face_search_multi_id']]['face_list'][$row['face_id']]['percent']      =$percent;

                }

            }

        }

        return $list;

    }

    /**
     * @param string|NULL $token
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_multi_item_data(string $token=NULL){

        if(empty($token)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Token is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id',
                'face_id',
                'image_id',
                'face_image_id',
                'percent',
                'source_image_id',
                'percent_mode'
            ],
            'where'=>[
                'token'     =>$token,
                'type'      =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'ID'                =>$r[0]['id'],
            'face_ID'           =>$r[0]['face_id'],
            'image_ID'          =>$r[0]['image_id'],
            'source_image_ID'   =>$r[0]['source_image_id'],
            'face_image_ID'     =>$r[0]['face_image_id'],
            'percent'           =>$r[0]['percent'],
            'percent_mode'      =>$r[0]['percent_mode']
        ];

    }

    /**
     * @param int|NULL $face_search_multi_ID
     * @param int|NULL $face_search_ID
     * @param int|NULL $face_search_item_ID
     * @param int|NULL $source_image_ID
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @param int|NULL $face_image_ID
     * @param float|NULL $percent
     * @param float|NULL $percent_mode
     * @param int $image_len
     * @param string|NULL $token
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_search_multi_item(
        int $face_search_multi_ID                   =NULL,
        int $face_search_ID                         =NULL,
        int $face_search_item_ID                    =NULL,
        int $source_image_ID                        =NULL,
        int $face_ID                                =NULL,
        int $image_ID                               =NULL,
        int $face_image_ID                          =NULL,
        float $percent                              =NULL,
        float $percent_mode                         =NULL,
        int $image_len                              =0,
        string $token                               =NULL
    ){

        $error_info_list=[];

        if(empty($face_search_multi_ID))
            $error_info_list[]='Face search multi ID is empty';

        if(empty($face_search_ID))
            $error_info_list[]='Face search ID is empty';

        if(empty($face_search_item_ID))
            $error_info_list[]='Face search item ID is empty';

        if(empty($source_image_ID))
            $error_info_list[]='Source image ID is empty';

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($face_image_ID))
            $error_info_list[]='Face image ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'values'=>[
                'user_id'                       =>User::$user_ID,
                'face_search_multi_id'          =>$face_search_multi_ID,
                'face_search_id'                =>$face_search_ID,
                'face_search_item_id'           =>$face_search_item_ID,
                'face_image_id'                 =>$face_image_ID,
                'source_image_id'               =>$source_image_ID,
                'face_id'                       =>$face_ID,
                'image_id'                      =>$image_ID,
                'percent'                       =>$percent,
                'percent_mode'                  =>$percent_mode,
                'image_len'                     =>$image_len,
                'token'                         =>$token,
                'date_create'                   =>'NOW()',
                'date_update'                   =>'NOW()',
                'type'                          =>0
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face search multi was not add'
            ];

            throw new ParametersException($error);

        }

        return $r[0]['id'];

    }

}