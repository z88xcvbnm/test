<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Core\Module\User\User;

class FaceVideo{

    /** @var string */
    public  static $table_name='face_video';

    /**
     * @param int|NULL $video_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_ID(int $video_ID=NULL){

        if(empty($video_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Video ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'face_id'
            ],
            'where'     =>[
                'video_id'      =>$video_ID,
                'type'          =>0
            ],
            'limit'     =>1
        ];

        $r=Db::select($q);

        if(empty($r))
            return NULL;

        return $r[0]['face_id'];

    }

    /**
     * @return false|int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_last_date_update(){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'date_update'
            ],
            'where'=>[
                'type'=>0
            ],
            'order'=>[
                [
                    'column'    =>'date_update',
                    'direction' =>'desc'
                ]
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return empty($r[0]['date_update'])?NULL:strtotime($r[0]['date_update']);

    }

    /**
     * @param array $face_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_ID_list_from_face_ID_list(array $face_ID_list=[]){

        if(count($face_ID_list)==0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID list is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'face_id',
                'video_id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID_list,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            if(!empty($row['video_id']))
                $list[$row['video_id']]=[
                    'face_ID'       =>$row['face_id'],
                    'video_ID'      =>$row['video_id']
                ];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @param array $video_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_ID_list_from_video_ID_list(int $face_ID=NULL,array $video_ID_list=[]){

        if(count($video_ID_list)==0)
            return[];

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'video_id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'video_id'      =>$video_ID_list,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            if(!empty($row['video_id']))
                $list[]=$row['video_id'];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_ID_list(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'video_id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            if(!empty($row['video_id']))
                $list[]=$row['video_id'];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_data_list(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'video_id',
                'date_create'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'video_ID'          =>$row['video_id'],
                'date_create'       =>empty($row['date_create'])?NULL:strtotime($row['date_create'])
            ];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @param array $video_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_except_video_ID_list(int $face_ID=NULL,array $video_ID_list=[]){

        if(count($video_ID_list)==0)
            return[];

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'video_id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'video_id'      =>[
                    'method'        =>'not in',
                    'value'         =>$video_ID_list
                ],
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['video_id'];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_len(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'=>$face_ID
        ];

        return Db::get_row_len(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $file_ID
     * @param int|NULL $video_ID
     * @param int|NULL $faceset_ID
     * @param int|NULL $face_kit_video_ID
     * @param array|NULL $face_coords
     * @param string|NULL $faceset_token
     * @param string|NULL $face_token
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_video(int $face_ID=NULL,int $file_ID=NULL,int $video_ID=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($video_ID))
            $error_info_list[]='Video ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'                       =>User::$user_ID,
                'face_id'                       =>$face_ID,
                'file_id'                       =>empty($file_ID)?NULL:$file_ID,
                'video_id'                      =>$video_ID,
                'date_create'                   =>'NOW()',
                'date_update'                   =>'NOW()'
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face video was not add'
            ];

            throw new PhpException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_video(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'=>$face_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face video was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param array $video_ID_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_video_from_video_ID_list(int $face_ID=NULL,array $video_ID_list=[]){

        if(count($video_ID_list)==0)
            return true;

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'       =>$face_ID,
            'video_id'      =>$video_ID_list
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face video was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param array $video_ID_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_video_except_video_ID_list(int $face_ID=NULL,array $video_ID_list=[]){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'=>$face_ID
        ];

        if(count($video_ID_list)>0)
            $where_list['video_id']=[
                'method'        =>'not in',
                'value'         =>$video_ID_list
            ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face video was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

}