<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;
use Core\Module\User\User;
use Core\Module\User\UserAccess;

class FaceSearchMulti{

    /** @var string */
    public  static $table_name='face_search_multi';

    /**
     * @param int|NULL $face_search_multi_ID
     * @param int $start
     * @param int $len
     * @param int|NULL $user_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_multi_data_history_list(int $face_search_multi_ID=NULL,int $start=0,int $len=30,int $user_ID=NULL){

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'image_id',
                'source_image_id',
                'face_id',
                'percent',
                'price',
                'token',
//                'source_image',
                'date_create'
            ],
            'where'     =>[
                'user_id'       =>User::$user_ID,
                'type'          =>0
            ],
            'order'=>[
                [
                    'column'        =>'id',
                    'direction'     =>'desc'
                ]
            ],
            'limit'=>[
                $start,
                $len
            ]
        ];

        if(!empty($face_search_multi_ID))
            $q['where']['id']=[
                'method'        =>'>',
                'value'         =>$face_search_multi_ID
            ];

        if(!empty($user_ID))
            $q['where']['user_id']=$user_ID;

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'ID'                        =>$row['id'],
                'image_ID'                  =>$row['image_id'],
                'source_image_ID'           =>$row['source_image_id'],
                'face_ID'                   =>$row['face_id'],
                'percent'                   =>floor($row['percent']),
                'price'                     =>floor($row['price']),
                'face_search_multi_token'   =>$row['token'],
                'date_create'               =>strtotime($row['date_create'])
            ];

        return $list;

    }

    /**
     * @param string|NULL $token
     * @return int|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_multi_ID(string $token=NULL){

        if(empty($token)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Token is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'select'=>[
                'id'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'token'         =>$token,
                'type'          =>[0,2]
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_search_multi_ID
     * @return float|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_multi_percent(int $face_search_multi_ID=NULL){

        if(empty($face_search_multi_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face search multi ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'select'=>[
                'percent',
                'source_image'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'id'    =>$face_search_multi_ID,
                'type'  =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $token
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_face_search_multi_ID_with_user_ID(string $token=NULL,int $user_ID=NULL){

        $error_info_list=[];

        if(empty($token))
            $error_info_list[]='Token is empty';

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'       =>$user_ID,
            'token'         =>$token,
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $token
     * @param array $type
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_multi_ID_with_user_ID(int $user_ID=NULL,string $token=NULL,array $type=[0]){

        if(empty($token)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Token is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'select'=>[
                'id'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'user_id'       =>$user_ID,
                'token'         =>$token,
                'type'          =>$type
            ],
            'limit'=>1
        ];

        if(
              UserAccess::$is_root
            ||UserAccess::$is_admin
        )
            unset($q['where']['user_id']);

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param string|NULL $token
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_multi_data(string $token=NULL){

        if(empty($token)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Token is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'select'=>[
                'id',
                'user_id',
                'user_balance_transaction_id',
                'search_count',
                'price'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'token'=>$token,
                'type'=>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'user_ID'                       =>$r[0]['user_id'],
            'user_balance_transaction_ID'   =>$r[0]['user_balance_transaction_id'],
            'price'                         =>$r[0]['price'],
            'search_count'                  =>$r[0]['search_count']
        ];

    }

    /**
     * @param int|NULL $user_ID
     * @param int $start
     * @param int $len
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_multi_data_list(int $user_ID=NULL,int $start=0,int $len=10){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[

            'select'=>[
                'id',
                'user_id',
                'user_balance_transaction_id',
                'search_count',
                'price',
                'date_create'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'user_id'   =>$user_ID,
                'type'      =>0
            ],
            'limit'=>[$start,$len]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'user_ID'                       =>$row['user_id'],
                'user_balance_transaction_ID'   =>$row['user_balance_transaction_id'],
                'price'                         =>$row['price'],
                'search_count'                  =>$row['search_count'],
                'date_create'                   =>$row['date_create']
            ];

        return $list;

    }

    /**
     * @param int|NULL $user_balance_transaction_ID
     * @param int|NULL $image_len
     * @param float|NULL $price
     * @param string|NULL $token
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_search_multi(int $user_balance_transaction_ID=NULL,int $image_len=NULL,float $price=NULL,string $token=NULL){

        $error_info_list=[];

        if(empty($user_balance_transaction_ID))
            $error_info_list[]='User balance transaction ID is empty';

        if(empty($image_len))
            $error_info_list[]='Search count is empty';

        if(empty($price))
            $error_info_list[]='Price is empty';

        if(empty($token))
            $error_info_list[]='Token is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'values'=>[
                'user_id'                       =>User::$user_ID,
                'user_balance_transaction_id'   =>$user_balance_transaction_ID,
                'image_len'                     =>$image_len,
                'price'                         =>$price,
                'token'                         =>$token,
                'date_create'                   =>'NOW()',
                'date_update'                   =>'NOW()',
                'type'                          =>2
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face search multi was not add'
            ];

            throw new ParametersException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_search_multi_ID
     * @param int|NULL $source_image_ID
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @param array $source_image_list
     * @param float|NULL $percent
     * @param float|NULL $percet_mode
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_result(int $face_search_multi_ID=NULL,int $source_image_ID=NULL,int $face_ID=NULL,int $image_ID=NULL,array $source_image_list=[],float $percent=NULL,float $percet_mode=NULL){

        if(empty($face_search_multi_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face search multi ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'source_image_id'           =>$source_image_ID,
                'source_image'              =>empty($source_image_list)?NULL:Json::encode($source_image_list),
                'face_id'                   =>$face_ID,
                'image_id'                  =>$image_ID,
                'percent'                   =>$percent,
                'percent_mode'              =>$percet_mode,
                'date_update'               =>'NOW()',
                'type'                      =>0
            ],
            'where'     =>[
                'id'=>$face_search_multi_ID
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face search multi was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

}