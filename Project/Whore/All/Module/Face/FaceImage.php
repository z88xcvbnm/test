<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Image\ImageItem;
use Core\Module\Json\Json;
use Core\Module\User\User;

class FaceImage{

    /** @var string */
    public  static $table_name='face_image';

    /**
     * @param int|NULL $image_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_ID(int $image_ID=NULL){

        if(empty($image_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Image ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'face_id'
            ],
            'where'     =>[
                'image_id'      =>$image_ID,
                'type'          =>0
            ],
            'limit'     =>1
        ];

        $r=Db::select($q);

        if(empty($r))
            return NULL;

        return $r[0]['face_id'];

    }

    /**
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_min(){

        $q=[
            'select'=>[
                [
                    'function'=>'MIN(image_id) as min_image_id'
                ]
            ],
            'table'=>self::$table_name,
            'where'=>[
                'type'=>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(empty($r))
            return NULL;

        return $r[0]['min_image_id'];

    }

    /**
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_max(){

        $q=[
            'select'=>[
                [
                    'function'=>'MAX(image_id) as max_image_id'
                ]
            ],
            'table'=>self::$table_name,
            'where'=>[
                'type'=>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(empty($r))
            return NULL;

        return $r[0]['max_image_id'];

    }

    /**
     * @param int $limit
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_random_image_ID_list(int $limit=100){

        if($limit>1000)
            $limit=100;

        $q=[
            'select'=>[
                'image_id'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'type'=>0
            ],
            'order'=>[
                [
                    'function'=>'random()'
                ]
            ],
            'limit'=>$limit
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['image_id'];

        return $list;

    }

    /**
     * @param int $image_ID
     * @param array $not_eq_image_ID_list
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_with_start_image_ID(int $image_ID=1,array $not_eq_image_ID_list=[]){

        $q=[
            'select'=>[
                'image_id'
            ],
            'table'=>self::$table_name,
            'where'=>[
                [
                    'column'        =>'image_id',
                    'method'        =>'>=',
                    'value'         =>$image_ID
                ],
                [
                    'column'        =>'type',
                    'value'         =>0
                ]
            ],
            'limit'=>1
        ];

        if(count($not_eq_image_ID_list)>0)
            $q['where'][]=[
                'column'        =>'image_id',
                'method'        =>'not in',
                'value'         =>$not_eq_image_ID_list
            ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['image_id'];

    }

    /**
     * @return false|int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_last_date_update(){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'date_update'
            ],
            'where'=>[
                'type'=>0
            ],
            'order'=>[
                [
                    'column'    =>'date_update',
                    'direction' =>'desc'
                ]
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return empty($r[0]['date_update'])?NULL:strtotime($r[0]['date_update']);

    }

    /**
     * @param array $face_token_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_from_face_token_list(array $face_token_list=[]){

        if(count($face_token_list)==0)
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'file_id',
                'image_id',
                'face_id',
                'faceset_id',
                'face_pp_faceset_token',
                'face_pp_face_token'
            ],
            'where'     =>[
                'face_pp_face_token'        =>$face_token_list,
                'type'                      =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['face_pp_face_token']]=[
                'face_image_ID'         =>$row['id'],
                'file_ID'               =>$row['file_id'],
                'image_ID'              =>$row['image_id'],
                'face_ID'               =>$row['face_id'],
                'faceset_ID'            =>$row['faceset_id'],
                'faceset_token'         =>$row['face_pp_faceset_token'],
                'face_token'            =>$row['face_pp_face_token']
            ];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_list(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'image_id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            if(!empty($row['image_id']))
                $list[]=$row['image_id'];

        return $list;

    }

    /**
     * @param array $face_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_unical_image_ID_list_from_face_ID_list(array $face_ID_list=[]){

        if(empty($face_ID_list))
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'image_id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID_list,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $image_ID_list=[];

        foreach($r as $row)
            $image_ID_list[]=$row['image_id'];

        if(count($image_ID_list)==0)
            return[];

        $r=ImageItem::get_unical_image_ID_list($image_ID_list);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $image_ID)
            if(array_search($image_ID,$list)===false)
                $list[]=$image_ID;

        return $list;

    }

    /**
     * @param array $image_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_ID_list_from_image_ID_list(array $image_ID_list=[]){

        if(empty($image_ID_list)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Image ID list is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'face_id'
            ],
            'where'     =>[
                'image_id'      =>$image_ID_list,
                'type'          =>0
            ],
            'group'=>[
                [
                    'column'=>'face_id'
                ]
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['face_id'];

        return $list;

    }

    /**
     * @param array $face_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_len_list(array $face_ID_list=[]){

        if(count($face_ID_list)==0)
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                [
                    'column'=>'face_id'
                ],
                [
                    'function'=>'COUNT(*) as len'
                ]
            ],
            'where'     =>[
                'face_id'       =>$face_ID_list,
                'type'          =>0
            ],
            'group'=>[
                [
                    'column'=>'face_id'
                ]
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['face_id']]=$row['len'];

        return $list;

    }

    /**
     * @param int|NULL $image_ID
     * @param int $limit
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_list_from_min_image_ID(int $image_ID=NULL,int $limit=100){

        $q=[
            'select'    =>[
                [
                    'table'     =>self::$table_name,
                    'column'    =>'image_id'
                ],
                [
                    'table'     =>self::$table_name,
                    'column'    =>'face_id'
                ]
            ],
            'table'     =>self::$table_name,
            'join'      =>[
                [
                    'table'=>FaceData::$table_name,
                    'where'=>[
                        [
                            'table'         =>self::$table_name,
                            'table_join'    =>FaceData::$table_name,
                            'column'        =>'face_id',
                            'column_join'   =>'face_id'
                        ],
                        [
                            'table'         =>FaceData::$table_name,
                            'column'        =>'type',
                            'value'         =>0
                        ]
                    ]
                ],
                [
                    'table'=>Face::$table_name,
                    'where'=>[
                        [
                            'table'         =>self::$table_name,
                            'table_join'    =>Face::$table_name,
                            'column'        =>'face_id',
                            'column_join'   =>'id'
                        ],
                        [
                            'table'         =>Face::$table_name,
                            'column'        =>'type',
                            'value'         =>0
                        ]
                    ]
                ]
            ],
            'where'     =>[
                [
                    'table'         =>self::$table_name,
                    'column'        =>'image_id',
                    'method'        =>'>=',
                    'value'         =>$image_ID
                ],
                [
                    'table'         =>self::$table_name,
                    'column'        =>'face_id',
                    'method'        =>'!=',
                    'value'         =>NULL
                ],
                [
                    'table'         =>self::$table_name,
                    'column'        =>'type',
                    'value'         =>0
                ]
            ],
//            'group'=>[
//                [
//                    'table'=>self::$table_name,
//                    'column'=>'face_id'
//                ]
//            ],
            'limit'=>$limit
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['image_id']]=$row['face_id'];

        return $list;

    }

    /**
     * @param int|NULL $image_ID
     * @param int $limit
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_list_from_min_image_ID_new(int $image_ID=NULL,int $limit=100){

        $q=[
            'select'    =>[
                'image_id',
                'face_id'
            ],
            'table'     =>self::$table_name,
            'where'     =>[
                [
                    'column'        =>'image_id',
                    'method'        =>'>=',
                    'value'         =>$image_ID
                ],
                [
                    'column'        =>'face_id',
                    'method'        =>'!=',
                    'value'         =>NULL
                ],
                [
                    'column'        =>'type',
                    'value'         =>0
                ]
            ],
            'group'=>[
                [
                    'column'=>'face_id'
                ]
            ],
            'limit'=>$limit
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['image_id']]=$row['face_id'];

        return $list;

    }

    /**
     * @param array $face_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_list_from_face_ID_list(array $face_ID_list=[]){

        if(count($face_ID_list)==0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID list is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'face_id',
                'image_id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID_list,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            if(!empty($row['image_id']))
                $list[$row['image_id']]=[
                    'face_ID'       =>$row['face_id'],
                    'image_ID'      =>$row['image_id']
                ];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @param array|NULL $image_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_plus_plus_data(int $face_ID=NULL,array $image_ID_list=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'image_id',
                'faceset_id',
                'face_pp_faceset_token',
                'face_pp_face_token'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'faceset_id'    =>[
                    'method'        =>'!=',
                    'value'         =>NULL
                ],
                'type'          =>0
            ]
        ];

        if(!empty($image_ID_list))
            $q['where']['image_id']=$image_ID_list;

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            if(!empty($row['face_pp_faceset_token']))
                $list[]=[
                    'ID'                    =>$row['id'],
                    'image_ID'              =>$row['image_id'],
                    'faceset_ID'            =>$row['faceset_id'],
                    'faceset_token'         =>$row['face_pp_faceset_token'],
                    'face_token'            =>$row['face_pp_face_token']
                ];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @param array|NULL $image_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_plus_plus_data_without_faceset_ID(int $face_ID=NULL,array $image_ID_list=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'file_id',
                'image_id',
                'faceset_id',
                'face_pp_faceset_token',
                'face_pp_face_token'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'faceset_id'    =>NULL,
                'type'          =>0
            ]
        ];

        if(!empty($image_ID_list))
            $q['where']['image_id']=$image_ID_list;

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'ID'                    =>$row['id'],
                'file_ID'               =>$row['file_id'],
                'image_ID'              =>$row['image_id'],
                'faceset_ID'            =>$row['faceset_id'],
                'faceset_token'         =>$row['face_pp_faceset_token'],
                'face_token'            =>$row['face_pp_face_token']
            ];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @param array $image_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_list_from_image_ID_list(int $face_ID=NULL,array $image_ID_list=[]){

        if(count($image_ID_list)==0)
            return[];

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'image_id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'image_id'      =>$image_ID_list,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            if(!empty($row['image_id']))
                $list[]=$row['image_id'];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_data_list(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'image_id',
                'faceset_id',
                'facekit_image_id',
                'face_count',
                'date_create'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'image_ID'              =>$row['image_id'],
                'faceset_ID'            =>$row['faceset_id'],
                'facekit_image_ID'      =>$row['facekit_image_id'],
                'face_count'            =>$row['face_count'],
                'date_create'           =>empty($row['date_create'])?NULL:strtotime($row['date_create'])
            ];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_hash_list(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'image_id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ],
            'order'=>[
                [
                    'column'        =>'image_id',
                    'direction'     =>'asc'
                ]
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'image_ID'=>$row['image_id']
            ];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @param array $image_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_except_image_ID_list(int $face_ID=NULL,array $image_ID_list=[]){

        if(count($image_ID_list)==0)
            return[];

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'image_id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'image_id'      =>[
                    'method'        =>'not in',
                    'value'         =>$image_ID_list
                ],
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['image_id'];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_len(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'=>$face_ID
        ];

        return Db::get_row_len_id(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $face_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_facekit_image_len(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'               =>$face_ID,
            'facekit_image_id'      =>[
                'method'        =>'!=',
                'value'         =>NULL
            ]
        ];

        return Db::get_row_len_id(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $face_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_plus_plus_image_len(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'               =>$face_ID,
            'face_pp_face_token'    =>[
                'method'        =>'!=',
                'value'         =>NULL
            ]
        ];

        return Db::get_row_len_id(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $face_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_facekit_image_ID_list(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'facekit_image_id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return[];

        $list=[];

        foreach($r as $row)
            if(!empty($row['facekit_image_id']))
                $list[]=$row['facekit_image_id'];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_facekit_image_ID(int $face_ID=NULL,int $image_ID=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id'
            ],
            'where'     =>[
                'face_id'       =>$face_ID,
                'image_id'      =>$image_ID,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(empty($r))
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_global_facekit_image_len(){

        $where_list=[
            'facekit_image_id'      =>[
                'method'        =>'!=',
                'value'         =>NULL
            ]
        ];

        return Db::get_row_len_id(self::$table_name,0,$where_list);

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_global_face_plus_plus_image_len(){

        $where_list=[
            'faceset_id'      =>[
                'method'        =>'!=',
                'value'         =>NULL
            ]
        ];

        return Db::get_row_len_id(self::$table_name,0,$where_list);

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_month_facekit_image_len(){

        $year_month         =Date::get_date(NULL,"Y-m");
        $last_day_in_month  =Date::get_last_day_in_month();

        $where_list=[
            [
                'column'        =>'facekit_image_id',
                'method'        =>'!=',
                'value'         =>NULL
            ],
            [
                'column'        =>'date_update',
                'method'        =>'>=',
                'value'         =>$year_month.'-01 00:00:00'
            ],
            [
                'column'        =>'date_update',
                'method'        =>'<=',
                'value'         =>$year_month.'-'.$last_day_in_month.' 23:59:59'
            ]
        ];

        return Db::get_row_len_id(self::$table_name,0,$where_list);

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_month_face_plus_plus_image_len(){

        $year_month         =Date::get_date(NULL,"Y-m");
        $last_day_in_month  =Date::get_last_day_in_month();

        $where_list=[
            [
                'column'        =>'faceset_id',
                'method'        =>'!=',
                'value'         =>NULL
            ],
            [
                'column'        =>'date_update',
                'method'        =>'>=',
                'value'         =>$year_month.'-01 00:00:00'
            ],
            [
                'column'        =>'date_update',
                'method'        =>'<=',
                'value'         =>$year_month.'-'.$last_day_in_month.' 23:59:59'
            ]
        ];

        return Db::get_row_len_id(self::$table_name,0,$where_list);

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_30_days_face_plus_plus_image_len(){

        $timestamp_to           =Date::get_timestamp();
        $timestamp_from         =$timestamp_to-30*24*3600;

        $where_list=[
            [
                'column'        =>'faceset_id',
                'method'        =>'!=',
                'value'         =>NULL
            ],
            [
                'column'        =>'date_update',
                'method'        =>'>=',
                'value'         =>Date::get_date_time_full($timestamp_from)
            ],
            [
                'column'        =>'date_update',
                'method'        =>'<=',
                'value'         =>Date::get_date_time_full($timestamp_to)
            ]
        ];

        return Db::get_row_len_id(self::$table_name,0,$where_list);

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_global_face_image_len(){

        return Db::get_row_len_id(self::$table_name,0);

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_month_face_image_len(){

        $year_month         =Date::get_date(NULL,"Y-m");
        $last_day_in_month  =Date::get_last_day_in_month();

        $where_list=[
            [
                'column'        =>'date_create',
                'method'        =>'>=',
                'value'         =>$year_month.'-01 00:00:00'
            ],
            [
                'column'        =>'date_create',
                'method'        =>'<=',
                'value'         =>$year_month.'-'.$last_day_in_month.' 23:59:59'
            ]
        ];

        return Db::get_row_len_id(self::$table_name,0,$where_list);

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_30_days_face_image_len(){

        $timestamp_to           =Date::get_timestamp();
        $timestamp_from         =$timestamp_to-30*24*3600;

        $where_list=[
            [
                'column'        =>'date_create',
                'method'        =>'>=',
                'value'         =>Date::get_date_time_full($timestamp_from)
            ],
            [
                'column'        =>'date_create',
                'method'        =>'<=',
                'value'         =>Date::get_date_time_full($timestamp_to)
            ]
        ];

        return Db::get_row_len_id(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $file_ID
     * @param int|NULL $image_ID
     * @param int|NULL $faceset_ID
     * @param int|NULL $face_kit_image_ID
     * @param array|NULL $face_coords
     * @param string|NULL $faceset_token
     * @param string|NULL $face_token
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_image(int $face_ID=NULL,int $file_ID=NULL,int $image_ID=NULL,int $faceset_ID=NULL,int $face_kit_image_ID=NULL,array $face_coords=NULL,string $faceset_token=NULL,string $face_token=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'                       =>User::$user_ID,
                'face_id'                       =>$face_ID,
                'file_id'                       =>empty($file_ID)?NULL:$file_ID,
                'image_id'                      =>$image_ID,
                'faceset_id'                    =>empty($faceset_ID)?NULL:$faceset_ID,
                'facekit_image_id'              =>empty($face_kit_image_ID)?NULL:$face_kit_image_ID,
                'face_coords'                   =>empty($face_coords)?NULL:Json::encode($face_coords),
                'face_count'                    =>empty($face_coords)?0:count($face_coords),
                'face_pp_faceset_token'         =>empty($faceset_token)?NULL:$faceset_token,
                'face_pp_face_token'            =>empty($face_token)?NULL:$faceset_token,
                'date_create'                   =>'NOW()',
                'date_update'                   =>'NOW()'
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face image was not add'
            ];

            throw new PhpException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @param array|NULL $face_coords
     * @param string|NULL $face_token
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_image_face_token(int $face_ID=NULL,int $image_ID=NULL,array $face_coords=NULL,string $face_token=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table' =>self::$table_name,
            'set'   =>[
                'face_coords'           =>empty($face_coords)?NULL:Json::encode($face_coords),
                'face_count'            =>empty($face_coords)?0:count($face_coords),
                'face_pp_face_token'    =>empty($face_token)?NULL:$face_token,
                'date_update'           =>'NOW()'
            ],
            'where' =>[
                'face_id'       =>$face_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face image was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_image(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'=>$face_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face image was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param array $face_ID_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_image_from_face_ID_list(array $face_ID_list=[]){

        if(count($face_ID_list)==0)
            return true;

        $where_list=[
            'face_id'=>$face_ID_list
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face image was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param array $image_ID_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_image_from_image_ID_list(int $face_ID=NULL,array $image_ID_list=[]){

        if(count($image_ID_list)==0)
            return true;

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'       =>$face_ID,
            'image_id'      =>$image_ID_list
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face image was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param array $image_ID_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_image_except_image_ID_list(int $face_ID=NULL,array $image_ID_list=[]){

        if(count($image_ID_list)==0)
            return true;

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'       =>$face_ID,
            'image_id'      =>[
                'method'        =>'not in',
                'value'         =>$image_ID_list
            ]
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face image was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

}