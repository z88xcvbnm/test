<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;

class FaceSearchItemParts{

    /** @var string */
    public  static $table_name='face_search_item_parts';

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_face_ID_for_user_ID(int $face_ID=NULL,int $user_ID=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'       =>$user_ID,
            'face_id'       =>$face_ID
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $face_search_ID
     * @param int|NULL $face_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_item_ID(int $face_search_ID=NULL,int $face_ID=NULL){

        $error_info_list=[];

        if(empty($face_search_ID))
            $error_info_list[]='Face search ID is empty';

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_search_id'        =>$face_search_ID,
            'face_id'               =>$face_ID
        ];

        return Db::get_row_ID(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $face_search_ID
     * @param int|NULL $face_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_item_data(int $face_search_ID=NULL,int $face_ID=NULL){

        $error_info_list=[];

        if(empty($face_search_ID))
            $error_info_list[]='Face search ID is empty';

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'face_id',
                'percent'
            ],
            'where'     =>[
                'face_search_id'        =>$face_search_ID,
                'face_id'               =>$face_ID,
                'type'                  =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'face_search_item_ID'       =>$r[0]['id'],
            'face_ID'                   =>$r[0]['face_id'],
            'percent'                   =>$r[0]['percent']
        ];

    }

    /**
     * @param array|NULL $face_search_ID_list
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_item_list(array $face_search_ID_list=NULL){

        if(empty($face_search_ID_list))
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'face_search_id',
                'face_id',
                'image_id',
                'percent'
            ],
            'where'     =>[
                'face_search_id'    =>$face_search_ID_list,
                'type'              =>0
            ],
            'order'=>[
                [
                    'column'    =>'percent',
                    'direction' =>'desc'
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row){

            if(empty($list[$row['face_search_id']]))
                $list[$row['face_search_id']]=[
                    'face_search_ID'    =>$row['face_search_id'],
                    'face_list'         =>[]
                ];

            if(!isset($list[$row['face_search_id']]['face_list'][$row['face_id']]))
                $list[$row['face_search_id']]['face_list'][$row['face_id']]=[
                    'ID'            =>$row['id'],
                    'face_ID'       =>$row['face_id'],
                    'image_ID'      =>$row['image_id'],
                    'percent'       =>ceil($row['percent'])
                ];
            else{

                $percent=ceil($row['percent']);

                if($list[$row['face_search_id']]['face_list'][$row['face_id']]['percent']<$percent){

                    $list[$row['face_search_id']]['face_list'][$row['face_id']]['image_ID']     =$row['image_id'];
                    $list[$row['face_search_id']]['face_list'][$row['face_id']]['percent']      =$percent;

                }

            }

        }

        return $list;

    }

    /**
     * @param int|NULL $face_search_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_item_data_list_from_face_search_ID(int $face_search_ID=NULL){

        if(empty($face_search_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face search ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'face_id',
                'file_id',
                'image_id',
                'face_image_id',
                'face_search_id',
                'faceset_id',
                'face_pp_faceset_token',
                'face_pp_face_token',
                'face_pp_request_id',
                'percent'

            ],
            'where'     =>[
                'face_search_id'    =>$face_search_ID,
                'type'              =>0
            ],
            'order'=>[
                [
                    'column'    =>'percent',
                    'direction' =>'desc'
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];
//                (
//                    [request_ID] => 1553797718,3c642253-aaf2-48b6-9dd3-e6d5c2d449db
//                    [faceset_ID] => 1
//                    [faceset_token] => bb5b21a348bf4b16e5da5dc534ae5ac1
//                    [face_token] => bf2700ede19cbaa2519e96f5bae48cde
//                    [percent] => 9706
//                    [face_image_ID] => 19332
//                    [file_ID] => 19369
//                    [image_ID] => 19367
//                    [face_ID] => 252
//                )

        foreach($r as $row){

            $list[]=[
                'ID'                =>$row['id'],
                'request_ID'        =>$row['face_pp_request_id'],
                'faceset_ID'        =>$row['faceset_id'],
                'faceset_token'     =>$row['face_pp_faceset_token'],
                'face_token'        =>$row['face_pp_face_token'],
                'percent'           =>$row['percent'],
                'face_image_ID'     =>$row['face_image_id'],
                'file_ID'           =>$row['file_id'],
                'image_ID'          =>$row['image_id'],
                'face_ID'           =>$row['face_id'],
                'face_ID_list'      =>[]
            ];

        }

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $face_image_ID
     * @param int|NULL $face_search_ID
     * @param string|NULL $face_token
     * @param float|NULL $percent
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_search_item(int $face_ID=NULL,int $face_image_ID=NULL,int $face_search_ID=NULL,string $face_token=NULL,float $percent=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($face_image_ID))
            $error_info_list[]='Face image ID is empty';

        if(empty($face_search_ID))
            $error_info_list[]='Face search ID is empty';

        if(empty($face_token))
            $error_info_list[]='Face token is empty';

        if(empty($percent))
            $error_info_list[]='Percent is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'               =>User::$user_ID,
                'face_id'               =>$face_ID,
                'face_image_id'         =>$face_image_ID,
                'face_search_id'        =>$face_search_ID,
                'face_pp_face_token'    =>$face_token,
                'percent'               =>$percent,
                'date_create'           =>'NOW()',
                'date_update'           =>'NOW()',
                'type'                  =>0
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face search item was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_search_ID
     * @param int $faceset_index
     * @param array $face_list
     * @param int|NULL $face_search_multi_ID
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_search_item_list(int $face_search_ID=NULL,int $faceset_index=0,array $face_list=[],int $face_search_multi_ID=NULL){

        if(empty($face_search_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face search ID is empty'
            ];

            throw new ParametersException($error);

        }

        if(count($face_list)==0)
            return true;

        $value_list=[];

        foreach($face_list as $row){

            $error_info_list=[];

            if(empty($row['face_ID']))
                $error_info_list[]='Face ID is empty';

            if(empty($row['face_image_ID']))
                $error_info_list[]='Face image ID is empty';

            if(empty($row['face_token']))
                $error_info_list[]='Face token is empty';

            if(empty($row['percent']))
                $error_info_list[]='Percent is empty';

            if(count($error_info_list)>0){

                $error=[
                    'title'     =>ParametersException::$title,
                    'info'      =>$error_info_list
                ];

                throw new ParametersException($error);

            }

            $value_list[]=[
                'user_id'                   =>User::$user_ID,
                'face_id'                   =>$row['face_ID'],
                'file_id'                   =>$row['file_ID'],
                'image_id'                  =>$row['image_ID'],
                'face_image_id'             =>$row['face_image_ID'],
                'face_search_id'            =>$face_search_ID,
                'faceset_id'                =>$row['faceset_ID'],
                'face_search_multi_id'      =>$face_search_multi_ID,
                'faceset_index'             =>$faceset_index,
                'face_pp_faceset_token'     =>$row['faceset_token'],
                'face_pp_face_token'        =>$row['face_token'],
                'face_pp_request_id'        =>$row['request_ID'],
                'percent'                   =>$row['percent'],
                'date_create'               =>'NOW()',
                'date_update'               =>'NOW()',
                'type'                      =>0
            ];

        }

        if(count($value_list)==0)
            return true;

        $q=[
            'table'     =>self::$table_name,
            'values'    =>$value_list
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face search item was not add'
            ];

            throw new DbQueryException($error);

        }

        $r_ID_list=[];

        foreach($r as $row)
            $r_ID_list[]=$row['id'];

        return $r_ID_list;

    }

}