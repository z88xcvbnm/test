<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;

class FaceSearchPdf{

    /** @var string */
    public  static $table_name='face_search_pdf';

    /**
     * @param string|NULL $token
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_pdf_ID_from_token(string $token=NULL){

        if(empty($token)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Token is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'token'     =>$token
        ];

        return Db::get_row_ID(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $face_search_ID
     * @param int|NULL $face_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_search_pdf_ID(int $face_search_ID=NULL,int $face_ID=NULL){

        $error_info_list=[];

        if(empty($face_search_ID))
            $error_info_list[]='Face search ID is empty';

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'           =>User::$user_ID,
            'face_search_id'    =>$face_search_ID,
            'face_id'           =>$face_ID
        ];

        return Db::get_row_ID(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $face_search_ID
     * @param int|NULL $face_search_item_ID
     * @param int|NULL $face_ID
     * @param int|NULL $face_data_ID
     * @param string|NULL $token
     * @param int $type
     * @param int|NULL $face_search_multi_ID
     * @param int|NULL $face_search_multi_item_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_search_pdf(
        int $face_search_ID                 =NULL,
        int $face_search_item_ID            =NULL,
        int $face_ID                        =NULL,
        int $face_data_ID                   =NULL,
        string $token                       =NULL,
        int $type                           =2,
        int $face_search_multi_ID           =NULL,
        int $face_search_multi_item_ID      =NULL
    ){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($face_data_ID))
            $error_info_list[]='Face data ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'                       =>User::$user_ID,
                'face_search_id'                =>$face_search_ID,
                'face_search_item_id'           =>$face_search_item_ID,
                'face_search_multi_id'          =>$face_search_ID,
                'face_search_multi_item_id'     =>$face_search_item_ID,
                'face_id'                       =>$face_ID,
                'face_data_id'                  =>$face_data_ID,
                'token'                         =>empty($token)?NULL:$token,
                'date_create'                   =>'NOW()',
                'date_update'                   =>'NOW()',
                'type'                          =>$type
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_search_pdf_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_search_pdf_to_public(int $face_search_pdf_ID=NULL){

        if(empty($face_search_pdf_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face search pdf ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'date_update'       =>'NOW()',
                'type'              =>0
            ],
            'where'     =>[
                'id'        =>$face_search_pdf_ID,
                'type'      =>2
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face search pdf was not create'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

}