<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;

class FaceTrigItem{

    /** @var string */
    public  static $table_name='face_trig_item';

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_face_trig(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_id'=>$face_ID
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $face_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_trig_ID(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'select'=>[
                'face_trig_id'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'face_id'   =>$face_ID,
                'type'      =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['face_trig_id'];

    }

    /**
     * @param array $face_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_ID_list_from_face_ID_list(array $face_ID_list=[]){

        if(empty($face_ID_list))
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'face_trig_id',
                'face_id'
            ],
            'where'=>[
                'face_id'   =>$face_ID_list,
                'type'      =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $face_ID_list=[];

        foreach($r as $row){

            if(!isset($face_ID_list[$row['face_trig_id']]))
                $face_ID_list[$row['face_trig_id']]=$row['face_id'];

        }

        if(count($face_ID_list)==0)
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'face_trig_id',
                'face_id'
            ],
            'where'=>[
                'face_trig_id'   =>array_keys($face_ID_list),
                'type'           =>0
            ]
        ];

        $r      =Db::select($q);
        $list   =[];

        foreach($r as $row){

            if(!isset($list[$row['face_trig_id']]))
                $list[$row['face_trig_id']]=[];

            $list[$row['face_trig_id']][]=$row['face_id'];

        }

        return $list;

    }

    /**
     * @param int|NULL $face_trig_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_ID_list_from_face_trig_ID(int $face_trig_ID=NULL){

        if(empty($face_trig_ID))
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'face_id'
            ],
            'where'=>[
                'face_trig_id'  =>$face_trig_ID,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face trig item have problem'
            ];

            throw new DbQueryException($error);

        }

        $face_ID_list=[];

        foreach($r as $row)
            $face_ID_list[]=$row['face_id'];

        return $face_ID_list;

    }

    /**
     * @param int|NULL $face_trig_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_trig_len_list(int $face_trig_ID=NULL){

        if(empty($face_trig_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face trig ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                [
                    'function'=>'SUM("image_len") as "result_image_len"'
                ],
                [
                    'function'=>'SUM("video_len") as "result_video_len"'
                ],
                [
                    'function'=>'COUNT("id") as "result_face_len"'
                ]
            ],
            'where'=>[
                'face_trig_id'      =>$face_trig_ID,
                'type'              =>0
            ],
            'group'=>[
                [
                    'column'=>'face_trig_id'
                ]
            ]
        ];

        $r=Db::select($q);

        echo Db::$query_last."\n";
        print_r(Db::$value_last);

        print_r($r);

        if(empty($r))
            return NULL;

        return[
            'image_len'     =>(int)$r[0]['result_image_len'],
            'video_len'     =>(int)$r[0]['result_video_len'],
            'face_len'      =>(int)$r[0]['result_face_len']
        ];

    }

    /**
     * @param int|NULL $face_trig_ID
     * @param int|NULL $face_ID
     * @param int|NULL $image_len
     * @param int|NULL $video_len
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_trig_item(int $face_trig_ID=NULL,int $face_ID=NULL,int $image_len=NULL,int $video_len=NULL){

        $error_info_list=[];

        if(empty($face_trig_ID))
            $error_info_list[]='Face trig ID is empty';

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'           =>User::$user_ID,
                'face_trig_id'      =>$face_trig_ID,
                'face_id'           =>$face_ID,
                'image_len'         =>empty($image_len)?0:$image_len,
                'video_len'         =>empty($video_len)?0:$video_len,
                'date_create'       =>'NOW()',
                'date_update'       =>'NOW()',
                'type'              =>0
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face trig was not add'
            ];

            throw new PhpException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_trig_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_trig_item(int $face_trig_ID=NULL){

        if(empty($face_trig_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face trig ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'face_trig_id'=>$face_trig_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face trig item was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

}