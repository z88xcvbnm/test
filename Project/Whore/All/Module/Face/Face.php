<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;

class Face{

    /** @var string */
    public  static $table_name='face';

    /**
     * @param int|NULL $face_ID
     * @param int $type
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_face_ID(int $face_ID=NULL,int $type=0){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face kit ID is empty'
            ];

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($face_ID,self::$table_name,$type);

    }

    /**
     * @param int|NULL $face_ID
     * @param array $type
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_face_ID_with_type_array(int $face_ID=NULL,array $type=[0]){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face kit ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'id'        =>$face_ID,
            'type'      =>$type
        ];

        return Db::isset_row(self::$table_name,NULL,$where_list);

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_count(){

        return Db::get_row_len(self::$table_name,0);

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_month_count(){

        $year_month         =Date::get_date(NULL,"Y-m");
        $last_day_in_month  =Date::get_last_day_in_month();

        $where_list=[
            [
                'column'        =>'date_update',
                'method'        =>'>=',
                'value'         =>$year_month.'-01 00:00:00'
            ],
            [
                'column'        =>'date_update',
                'method'        =>'<=',
                'value'         =>$year_month.'-'.$last_day_in_month.' 23:59:59'
            ]
        ];

        return Db::get_row_len(self::$table_name,0,$where_list);

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_30_days_count(){

        $timestamp_to           =Date::get_timestamp();
        $timestamp_from         =$timestamp_to-30*24*3600;

        $where_list=[
            [
                'column'        =>'date_update',
                'method'        =>'>=',
                'value'         =>Date::get_date_time_full($timestamp_from)
            ],
            [
                'column'        =>'date_update',
                'method'        =>'<=',
                'value'         =>Date::get_date_time_full($timestamp_to)
            ]
        ];

        return Db::get_row_len(self::$table_name,0,$where_list);

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_ID_list(){

        $q=[
            'select'=>[
                'id'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'type'=>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['id'];

        return $list;

    }

    /**
     * @return int
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face(){

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'       =>User::$user_ID,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>2
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Kit was not add'
            ];

            throw new PhpException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_ID
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_ID(int $face_ID=NULL){

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'id'            =>$face_ID,
                'user_id'       =>User::$user_ID,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>0
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Kit was not add'
            ];

            throw new PhpException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face(int $face_ID=NULL){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'date_update'       =>'NOW()',
                'type'              =>0
            ],
            'where'     =>[
                'id'        =>$face_ID
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param bool $is_need_remove_child
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_ID(int $face_ID=NULL,bool $is_need_remove_child=true){

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face kit ID is empty'
            ];

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($face_ID,self::$table_name,0)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Kit was not remove'
            ];

            throw new PhpException($error);

        }

        if($is_need_remove_child){

            $error_info_list=[];

            if(!FaceImage::remove_face_image($face_ID))
                $error_info_list[]='Face image was not remove';

            if(!FaceData::remove_face_data_from_face_ID($face_ID))
                $error_info_list[]='Face data was not remove';

            if(count($error_info_list)>0){

                $error=[
                    'title'     =>PhpException::$title,
                    'info'      =>$error_info_list
                ];

                throw new PhpException($error);

            }

        }

        return true;

    }

    /**
     * @param array $face_ID_list
     * @param bool $is_need_remove_child
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_ID_list(array $face_ID_list=[],bool $is_need_remove_child=true){

        if(count($face_ID_list)==0)
            return true;

        $where_list=[
            'id'=>$face_ID_list
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Kit was not remove'
            ];

            throw new PhpException($error);

        }

        if($is_need_remove_child){

            $error_info_list=[];

            if(!FaceImage::remove_face_image_from_face_ID_list($face_ID_list))
                $error_info_list[]='Face image was not remove';

            if(!FaceData::remove_face_data_from_face_ID_list($face_ID_list))
                $error_info_list[]='Face data was not remove';

            if(count($error_info_list)>0){

                $error=[
                    'title'     =>PhpException::$title,
                    'info'      =>$error_info_list
                ];

                throw new PhpException($error);

            }

        }

        return true;

    }

}