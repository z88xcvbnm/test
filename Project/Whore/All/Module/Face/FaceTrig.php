<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;

class FaceTrig{

    /** @var string */
    public  static $table_name='face_trig';

    /**
     * @param int|NULL $face_trig_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_face_trig_ID(int $face_trig_ID=NULL){

        if(empty($face_trig_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face trig ID is empty'
            ];

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($face_trig_ID,self::$table_name,0);

    }

    /**
     * @param int|NULL $face_len
     * @param int|NULL $image_len
     * @param int|NULL $video_len
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_trig(int $face_len=NULL,int $image_len=NULL,int $video_len=NULL){

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'       =>User::$user_ID,
                'face_len'      =>empty($face_len)?0:$face_len,
                'image_len'     =>empty($image_len)?0:$image_len,
                'video_len'     =>empty($video_len)?0:$video_len,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>0
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face trig was not add'
            ];

            throw new PhpException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_trig_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_trig_image_len(int $face_trig_ID=NULL){

        if(empty($face_trig_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face trig ID is empty'
            ];

            throw new ParametersException($error);

        }

        $data           =FaceTrigItem::get_face_trig_len_list($face_trig_ID);
        $image_len      =0;
        $video_len      =0;
        $face_len       =0;

        if(!empty($data)){

            $image_len      =$data['image_len'];
            $video_len      =$data['video_len'];
            $face_len       =$data['face_len'];

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'face_len'      =>$face_len,
                'image_len'     =>$image_len,
                'video_len'     =>$video_len,
                'date_update'   =>'NOW()'
            ],
            'where'     =>[
                'id'        =>$face_trig_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face trig was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_trig_ID
     * @param bool $is_need_remove_child
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_face_trig_ID(int $face_trig_ID=NULL,bool $is_need_remove_child=true){

        if(empty($face_trig_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face trig ID is empty'
            ];

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($face_trig_ID,self::$table_name,0)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face trig was not remove'
            ];

            throw new PhpException($error);

        }

        if($is_need_remove_child)
            if(!FaceTrigItem::remove_face_trig_item($face_trig_ID)){

                $error=[
                    'title'     =>PhpException::$title,
                    'info'      =>'Face trig item was not remove'
                ];

                throw new PhpException($error);

            }

        return true;

    }

}