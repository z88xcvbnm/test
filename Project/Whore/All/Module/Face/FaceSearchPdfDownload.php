<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;

class FaceSearchPdfDownload{

    /** @var string */
    public  static $table_name='face_search_pdf_download';

    /**
     * @param int|NULL $face_search_pdf_ID
     * @param bool $is_show
     * @param bool $is_download
     * @return int|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_face_search_pdf_download(int $face_search_pdf_ID=NULL,bool $is_show=false,bool $is_download=false){

        if(empty($face_search_pdf_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face search PDF ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'                   =>User::$user_ID,
                'face_search_pdf_id'        =>$face_search_pdf_ID,
                'is_show'                   =>(int)$is_show,
                'is_download'               =>(int)$is_download,
                'date_create'               =>'NOW()',
                'date_update'               =>'NOW()',
                'type'                      =>2
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face search PDF download was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $face_search_pdf_download_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_face_search_pdf_download_to_downloaded(int $face_search_pdf_download_ID=NULL){

        if(empty($face_search_pdf_download_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face search PDF download ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'date_update'       =>'NOW()',
                'type'              =>0
            ],
            'where'     =>[
                'id'        =>$face_search_pdf_download_ID
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Face search PDF download was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

}