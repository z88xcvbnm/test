<?php

namespace Project\Whore\All\Module\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class FaceDataType{

    /** @var string */
    public  static $table_name='face_data_type';

    /**
     * @param int|NULL $face_data_type_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_face_data_type_ID(int $face_data_type_ID=NULL){

        if(empty($face_data_type_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face data type is empty'
            ];

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($face_data_type_ID,self::$table_name,0);

    }

    /**
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_face_data_type_list(){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id',
                'name'
            ],
            'where'=>[
                'type'=>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'ID'        =>$row['id'],
                'name'      =>$row['name']
            ];

        return $list;

    }

}