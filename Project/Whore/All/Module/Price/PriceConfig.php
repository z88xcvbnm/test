<?php

namespace Project\Whore\All\Module\Price;

use Core\Module\Date\Date;
use Core\Module\Exception\ParametersException;

class PriceConfig{

    /** @var int */
    public  static $search_black_friday_price_1     =100;

    /** @var int */
    public  static $search_black_friday_price_2_3   =500;

    /** @var int */
    public  static $search_price                    =500;

    /** @var int */
    public  static $search_price_combo              =500;

    /**
     * @return string
     */
    public  static function get_black_friday_date_limit(){

        if(Date::get_timestamp("2019-12-02 03:00:00")>=time())
            return'00:00:00 02.12.2019';

        return NULL;

    }

    /**
     * @param int $face_len
     * @return float|int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_black_friday_price(int $face_len=1){

        switch($face_len){

            case 0:
                return 0;

            case 1:
                return self::$search_black_friday_price_1;

            case 2:
            case 3:
                return self::$search_black_friday_price_2_3;

            default:{

                $error=[
                    'title'     =>ParametersException::$title,
                    'info'      =>'Faces length are not valid'
                ];

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @param int $face_len
     * @return float|int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_price(int $face_len=0){

        if(Date::get_timestamp("2019-12-02 03:00:00")>=time())
            return self::get_black_friday_price($face_len);
        
        switch($face_len){

            case 0:
                return 0;

            case 1:
                return self::$search_price_combo;

            case 2:
            case 3:
                return 2*self::$search_price_combo;

            default:{

                $error=[
                    'title'     =>ParametersException::$title,
                    'info'      =>'Faces length are not valid'
                ];

                throw new ParametersException($error);

            }

        }

    }

}