<?php

namespace Project\Whore\All\Module\Dir;

class DirConfigProject{

    /** @var string */
    public  static $dir_pdf                             ='Resource/Pdf';

    /** @var string */
    public  static $dir_face_temp                       ='Temp/Face';

    /** @var string */
    public  static $dir_facekit_faces_temp              ='Temp/FaceKitFaces';

    /** @var string */
    public  static $dir_parsing_log                     ='Temp/Parsing';

}