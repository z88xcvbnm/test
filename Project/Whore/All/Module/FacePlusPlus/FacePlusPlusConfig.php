<?php

namespace Project\Whore\All\Module\FacePlusPlus;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Os\Os;
use Core\Module\OsServer\OsServer;

class FacePlusPlusConfig{

    /** @var bool */
    public  static $is_free                     =true;

    /** @var string */
    public  static $app_name                    ='shluham.net_free';

    /** @var string */
    public  static $app_name_free               ='prod_shluham.net';

    /** @var string */
    public  static $api_key_free                ='SYUtC0QW-HYRZ-w3OloMvGA2-9iXdRRO';

    /** @var string */
    public  static $api_secret_key_free         ='rWTEEju6xGbHjk3KagK6DJd9KjpduoVM';

    /** @var string */
    public  static $api_key                     ='qLyYmiCj4kZr1VZ3eBWvK5vN3Ex8YHIF';

    /** @var string */
    public  static $api_secret_key              ='aW1wQAYmVqIjP1GN-zX4_9KZpArSqjGZ';

    /** @var int */
    public  static $face_landmark               =2;

    /** @var int */
    public  static $sleep_timeout               =10;

    /** @var int */
    public  static $sleep_timeout_delta         =40;

    /** @var int */
    public  static $face_token_max_len          =9900;

    /** @var int */
    public  static $face_token_part_len         =5;

    /** @var string */
    public  static $url                         ='https://api-us.faceplusplus.com/facepp/v3';

    /** @var string */
    public  static $detect_face_action          ='detect';

    /** @var string */
    public  static $search_face_action          ='search';

    /** @var string */
    public  static $get_faceset_list_action     ='faceset/getfacesets';

    /** @var string */
    public  static $get_faceset_data_action     ='faceset/getdetail';

    /** @var string */
    public  static $create_faceset_action       ='faceset/create';

    /** @var string */
    public  static $remove_faceset_action       ='faceset/delete';

    /** @var string */
    public  static $add_face_action             ='faceset/addface';

    /** @var string */
    public  static $remove_face_action          ='faceset/removeface';

    /** @var int */
    public  static $face_calculate_all          =0;

    /** @var int */
    public  static $force_merge                 =1;

    /** @var int */
    public  static $search_result_count         =5;

    /** @var array */
    public  static $face_attribute_list         =[
        'gender',
        'age',
        'smiling',
        'headpose',
        'facequality',
        'blur',
        'eyestatus',
        'emotion',
        'ethnicity',
        'beauty',
        'mouthstatus',
        'eyegaze',
        'skinstatus'
    ];

    /**
     * @return int
     */
    public  static function get_random_sleep_timeout(){

        $rand_delta=rand(0,self::$sleep_timeout_delta);

        return self::$sleep_timeout+$rand_delta;

    }

    /**
     * @return array
     */
    public  static function get_key_data(){

        if(OsServer::$is_windows)
            return[
                'api_key'           =>self::$api_key_free,
                'api_secret_key'    =>self::$api_secret_key_free
            ];

        return[
            'api_key'           =>self::$api_key,
            'api_secret_key'    =>self::$api_secret_key
        ];

    }

    /**
     * @param string|NULL $action
     * @return string
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_url_api(string $action=NULL){

        if(empty($action)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Action is empty'
            ];

            throw new ParametersException($error);

        }

        switch($action){

            case'detect_faces':{

                $action_method=self::$detect_face_action;

                break;

            }

            case'search_faces':{

                $action_method=self::$search_face_action;

                break;

            }

            case'get_faceset_list':{

                $action_method=self::$get_faceset_list_action;

                break;

            }

            case'get_faceset_data':{

                $action_method=self::$get_faceset_data_action;

                break;

            }

            case'add_faceset':{

                $action_method=self::$create_faceset_action;

                break;

            }

            case'remove_faceset':{

                $action_method=self::$remove_faceset_action;

                break;

            }

            case'add_face':{

                $action_method=self::$add_face_action;

                break;

            }

            case'remove_face':{

                $action_method=self::$remove_face_action;

                break;

            }

            default:{

                $error=[
                    'title'     =>ParametersValidationException::$title,
                    'info'      =>'Action is not valid'
                ];

                throw new ParametersValidationException($error);

                break;

            }

        }

        return self::$url.'/'.$action_method;

    }

    /**
     * @return string
     */
    public  static function get_face_attribute(){

        return implode(',',self::$face_attribute_list);

    }

}