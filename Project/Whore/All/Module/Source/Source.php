<?php

namespace Project\Whore\All\Module\Source;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;

class Source{

    /** @var string */
    public  static $table_name='source';

    /**
     * @param int|NULL $source_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_source_ID(int $source_ID=NULL){

        if(empty($source_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source ID is empty'
            ];

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($source_ID,self::$table_name,0);

    }

    /**
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_source_list(){

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'name',
                'rating'
            ],
            'where'     =>[
                'type'=>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'ID'        =>$row['id'],
                'name'      =>$row['name'],
                'rang'      =>empty($row['rating'])?NULL:(int)$row['rating']
            ];

        return $list;

    }

    /**
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_source_rang_list(){

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'rating'
            ],
            'where'     =>[
                'type'=>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['id']]=[
                'ID'        =>$row['id'],
                'rang'      =>$row['rating']
            ];

        return $list;

    }

    /**
     * @param int|NULL $source_ID
     * @return int|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_source_rang(int $source_ID=NULL){

        if(empty($source_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'rating'
            ],
            'where'     =>[
                'id'        =>$source_ID,
                'type'      =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['rating'];

    }

    /**
     * @param string|NULL $name
     * @param string|NULL $link
     * @param float|NULL $rating
     * @param string|NULL $info
     * @return mixed
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_source(string $name=NULL,string $link=NULL,float $rating=NULL,string $info=NULL){

        $error_info_list=[];

        if(empty($name))
            $error_info_list[]='Name is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'       =>User::$user_ID,
                'name'          =>$name,
                'link'          =>empty($link)?NULL:$link,
                'rating'        =>empty($rating)?0:$rating,
                'info'          =>empty($info)?NULL:$info,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()'
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Source was not add'
            ];

            throw new PhpException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $source_ID
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_source_ID(int $source_ID=NULL){

        if(empty($source_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source ID is empty'
            ];

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($source_ID,self::$table_name,0)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Source ID was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

}