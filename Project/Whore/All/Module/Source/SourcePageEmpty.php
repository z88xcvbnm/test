<?php

namespace Project\Whore\All\Module\Source;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;

class SourcePageEmpty{

    /** @var string */
    public  static $table_name='source_page_empty';

    /**
     * @param int|NULL $source_ID
     * @param string|NULL $link_ID
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_source_page_empty(int $source_ID=NULL,string $link_ID=NULL){

        if(empty($source_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'source_id'     =>$source_ID,
            'link_id'       =>$link_ID
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $source_ID
     * @param string|NULL $source_account_link
     * @param string|NULL $link_ID
     * @return mixed
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_source_page_empty(int $source_ID=NULL,string $source_account_link=NULL,string $link_ID=NULL,int $status=NULL){

        $error_info_list=[];

        if(empty($source_ID))
            $error_info_list[]='Source ID is empty';

        if(empty($source_account_link))
            $error_info_list[]='Source account link ID is empty';

        if(empty($link_ID))
            $error_info_list[]='Link ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'               =>User::$user_ID,
                'source_id'             =>$source_ID,
                'source_account_link'   =>$source_account_link,
                'link_id'               =>$link_ID,
                'status'                =>$status,
                'date_create'           =>'NOW()',
                'date_update'           =>'NOW()'
            ]
        ];

        $r=Db::insert($q);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Source was not add'
            ];

            throw new PhpException($error);

        }

        return $r[0]['id'];

    }

}