<?php

namespace Project\Whore\All\Module\FaceKit;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;

class FaceKitConfig{

    /** @var string */
    public  static $face_kit_api_key            ='27623342-4de4-471c-a374-410a218e2bdd';

    /** @var string */
    public  static $url                         ='https://api.facekit.io/api/v1';

    /** @var string */
    public  static $add_image_action            ='reference-photo';

    /** @var string */
    public  static $detect_faces_action         ='detect-faces';

    /** @var string */
    public  static $recognise_faces_action      ='recognize-faces';

    /** @var string */
    public  static $remove_image_action         ='reference-photo';

    /**
     * @param string|NULL $action
     * @return string
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_url_api(string $action=NULL){

        if(empty($action)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Action is empty'
            ];

            throw new ParametersException($error);

        }

        switch($action){

            case'add_image':{

                $action_method=self::$add_image_action;

                break;

            }

            case'detect_faces':{

                $action_method=self::$detect_faces_action;

                break;

            }

            case'recognise_faces':{

                $action_method=self::$recognise_faces_action;

                break;

            }

            case'remove_image':{

                $action_method=self::$remove_image_action;

                break;

            }

            default:{

                $error=[
                    'title'     =>ParametersValidationException::$title,
                    'info'      =>'Action is not valid'
                ];

                throw new ParametersValidationException($error);

                break;

            }

        }

        return self::$url.'/'.$action_method;

    }

}