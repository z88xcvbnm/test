<?php

namespace Project\Whore\All\Module\Mailing;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\UserEmail;

class Mailing{

    /** @var string */
    public  static $table_name='mailing';

    /**
     * @param int|NULL $mailing_ID
     * @return array|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_data(int $mailing_ID=NULL){

        if(empty($mailing_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Mailing ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id',
                'title',
                'message',
                'user_send_len',
                'user_all_len'
            ],
            'where'=>[
                'id'        =>$mailing_ID,
                'type'      =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'ID'                =>$mailing_ID,
            'title'             =>$r[0]['title'],
            'message'           =>$r[0]['message'],
            'user_send_len'     =>$r[0]['user_send_len'],
            'user_all_len'      =>$r[0]['user_all_len'],
        ];

    }

    /**
     * @param int|NULL $mailing_ID
     * @return array|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_len(int $mailing_ID=NULL){

        if(empty($mailing_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Mailing ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'user_send_len',
                'user_all_len'
            ],
            'where'=>[
                'id'        =>$mailing_ID,
                'type'      =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'user_send_len'     =>$r[0]['user_send_len'],
            'user_all_len'      =>$r[0]['user_all_len'],
        ];

    }

    /**
     * @param int|NULL $mailing_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function user_send_len_plus(int $mailing_ID=NULL){

        if(empty($mailing_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Mailing ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'user_send_len'=>[
                    'function'=>'user_send_len + 1'
                ],
                'date_update'=>'NOW()'
            ],
            'where'=>[
                'id'        =>$mailing_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Mailing was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $mailing_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_all_len(int $mailing_ID=NULL){

        if(empty($mailing_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Mailing ID is empty'
            ];

            throw new ParametersException($error);

        }

        $user_all_len=UserEmail::get_user_len();

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'user_all_len'  =>$user_all_len,
                'date_update'   =>'NOW()'
            ],
            'where'=>[
                'id'        =>$mailing_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Mailing was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

}