<?php

namespace Project\Whore\All\Module\Mailing;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\UserEmail;

class MailingItem{

    /** @var string */
    public  static $table_name='mailing_item';

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $mailing_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_ID(int $user_ID=NULL,int $mailing_ID=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($mailing_ID))
            $error_info_list[]='Mailing ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'       =>$user_ID,
            'mailing_id'    =>$mailing_ID
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $mailing_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_mailing_item(int $user_ID=NULL,int $mailing_ID=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($mailing_ID))
            $error_info_list[]='Mailing ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'values'=>[
                'user_id'       =>$user_ID,
                'mailing_id'    =>$mailing_ID,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>0
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Mailing item was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

}