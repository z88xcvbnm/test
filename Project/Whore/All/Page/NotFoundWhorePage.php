<?php

namespace Project\Page\Whore\All;

use Core\Module\Header\HeaderCode;
use Templates\Dom\Whore\All\NotFoundWhoreDom;

class NotFoundWhorePage{

    /**
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function set_header(){

        HeaderCode::init(404);

    }

    /**
     * Init page
     */
    private static function init_page(){

        NotFoundWhoreDom::init();

    }

    /**
     * Prepare
     */
    private static function set(){

        self::set_header();
        self::init_page();

    }

    /**
     * Init page not found
     */
    public  static function init(){

        self::set();

    }

}