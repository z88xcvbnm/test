<?php

namespace Project\Whore\All\Api\Face;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Image\Image;
use Core\Module\Response\ResponseSuccess;

class GetRecognizedFaceApi{

    /** @var int */
    private static $image_ID;

    /** @var array */
    private static $face_list=[];

    /** @var array */
    private static $coords;

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_ID(){

        if(!Image::isset_image_ID(self::$image_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'face_list'         =>self::$face_list
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_image_ID()){

            return self::set_return();

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(empty($_POST['image_ID'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Image ID is empty'
            ];

            throw new ParametersException($error);

        }

        self::$image_ID=(int)$_POST['image_ID'];

        return self::set();

    }

}