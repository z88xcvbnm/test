<?php

namespace Project\Whore\All\Api\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;

class GetFaceIDListApi{

    /** @var array */
    private static $source_account_link_list    =[];

    /** @var array */
    private static $face_ID_list                =[];

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_ID_list(){

        if(count(self::$source_account_link_list)==0)
            return true;

        $q=[
            'select'=>[
                'face_id',
                'source_account_link'
            ],
            'table'=>'face_data',
            'where'=>[
                'source_account_link'   =>self::$source_account_link_list,
                'type'                  =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return true;

        foreach($r as $row)
            self::$face_ID_list[$row['source_account_link']]=$row['face_id'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'face_ID_list'=>self::$face_ID_list
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_face_ID_list();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(empty($_POST['source_account_link_list'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source account link list is empty'
            ];

            throw new ParametersException($error);

        }

        self::$source_account_link_list=Json::decode($_POST['source_account_link_list']);

        return self::set();

    }

}