<?php

namespace Project\Whore\All\Api\Face;

use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\File\File;
use Core\Module\Image\Image;
use Core\Module\Image\ImageUploadedPrepare;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;

class AddFaceImageApi{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $image_ID;

    /** @var int */
    private static $face_image_ID;

    /** @var int */
    private static $facekit_image_ID;

    /** @var string */
    private static $file_mime_type;

    /** @var string */
    private static $file_content_type;

    /** @var string */
    private static $file_extension;

    /** @var int */
    private static $file_size;

    /** @var array */
    private static $face_coords;

    /** @var string */
    private static $image_base64;

    /** @var string */
    private static $file_path;

    /** @var array */
    private static $image_data;

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_ID(){

        if(!Face::isset_face_ID_with_type_array(self::$face_ID,[0,2])){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_file(){

        $hash_list=[
            User::$user_ID,
            self::$face_ID,
            time(),
            rand(0,time())
        ];
        $file_hash=Hash::get_sha1_encode(implode(':',$hash_list));

        self::$file_ID=File::add_file($file_hash,1,1,self::$file_size,self::$file_size,self::$file_mime_type,self::$file_content_type,self::$file_extension,true,true,false);

        if(empty(self::$file_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new PhpException($error);

        }

        self::$file_path=File::get_file_path_from_file_ID(self::$file_ID,true);

        if(file_put_contents(self::$file_path,base64_decode(self::$image_base64))===false){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not move to drive'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function add_image(){

//        self::$image_ID=Image::add_image(self::$file_ID,self::$file_content_type,self::$file_size,self::$file_mime_type,self::$file_extension,true,false);
//
//        if(empty(self::$image_ID)){
//
//            $error=[
//                'title'     =>PhpException::$title,
//                'info'      =>'Image ID is empty'
//            ];
//
//            throw new PhpException($error);
//
//        }

        $data=ImageUploadedPrepare::init(self::$file_ID,self::$file_content_type,self::$file_extension);

        if(empty($data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Image was not add'
            ];

            throw new PhpException($error);

        }

        self::$image_ID     =$data['image_ID'];
        self::$image_data   =$data;

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_image(){

        self::$face_image_ID=FaceImage::add_face_image(self::$face_ID,self::$file_ID,self::$image_ID,self::$facekit_image_ID,self::$face_coords);

        if(empty(self::$face_image_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face image was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_data_image_len(){

        if(!FaceData::update_face_data_image_ID(self::$face_ID,self::$image_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'   =>true,
            'data'      =>[
                'image_ID'      =>self::$image_ID,
                'file_path'     =>self::$file_path
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        if(self::isset_face_ID()){

            self::add_file();
            self::add_image();
            self::add_face_image();
            self::update_face_data_image_len();

            return self::set_return();

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['face_ID']))
            $error_info_list[]='Face ID is empty';

        if(empty($_POST['image_base64']))
            $error_info_list[]='Image base64 is empty';

        if(empty($_POST['file_size']))
            $error_info_list[]='File size is empty';

        if(empty($_POST['file_mime_type']))
            $error_info_list[]='File mime type is empty';

        if(empty($_POST['file_content_type']))
            $error_info_list[]='File content type is empty';

        if(empty($_POST['file_extension']))
            $error_info_list[]='File extension is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);
            
        }

        self::$face_ID                  =(int)$_POST['face_ID'];
        self::$facekit_image_ID         =empty($_POST['facekit_image_ID'])?NULL:(int)$_POST['facekit_image_ID'];
        self::$file_size                =(int)$_POST['file_size'];
        self::$file_extension           =$_POST['file_extension'];
        self::$file_mime_type           =$_POST['file_mime_type'];
        self::$file_content_type        =$_POST['file_content_type'];
        self::$image_base64             =$_POST['image_base64'];
        self::$face_coords              =empty($_POST['face_coords'])?NULL:Json::decode($_POST['face_coords']);

        return self::set();

    }

}