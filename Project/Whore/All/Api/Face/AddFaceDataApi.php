<?php

namespace Project\Whore\All\Api\Face;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Image\Image;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Source\Source;

class AddFaceDataApi{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $face_data_ID;

    /** @var int */
    private static $source_ID;

    /** @var int */
    private static $image_ID;

    /** @var int */
    private static $city_ID;

    /** @var string */
    private static $name;

    /** @var int */
    private static $age;

    /** @var string */
    private static $city;

    /** @var string */
    private static $source_account_link;

    /** @var int */
    private static $image_len;

    /** @var int */
    private static $image_facekit_len;

    /** @var string */
    private static $info;

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_ID(){

        if(!Face::isset_face_ID_with_type_array(self::$face_ID,[0,2])){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_source_ID(){

        if(!Source::isset_source_ID(self::$source_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Source ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_ID(){

        if(empty(self::$image_ID))
            return true;

        if(!Image::isset_image_ID(self::$image_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_face_data(){

        if(empty(self::$face_ID))
            return true;

        if(!FaceData::remove_face_data_from_face_ID(self::$face_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_source_account_link(){

        $face_ID=FaceData::get_face_ID_from_source_account_link(self::$source_account_link);

        if(!empty($face_ID)){

            if(self::$face_ID!=$face_ID){

                $error=[
                    'title'     =>ParametersValidationException::$title,
                    'info'      =>'Face ID have conflict'
                ];

                throw new ParametersValidationException($error);

            }

//            return self::remove_face_data();
            return true;

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_len(){

        self::$image_len=FaceImage::get_image_len(self::$face_ID);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_facekit_image_len(){

        self::$image_facekit_len=FaceImage::get_facekit_image_len(self::$face_ID);

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_data(){

        self::$face_data_ID=FaceData::add_face_data(self::$face_ID,self::$city_ID,self::$image_ID,self::$source_ID,self::$source_account_link,self::$name,self::$age,self::$city,self::$info,self::$image_len,self::$image_facekit_len);

        if(empty(self::$face_data_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'   =>true,
            'data'      =>[
                'face_data_ID'=>self::$face_data_ID
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_face_ID())
            if(self::isset_source_ID())
                if(self::isset_source_account_link())
                    if(self::isset_image_ID()){

                        self::set_image_len();
                        self::set_facekit_image_len();

                        self::add_face_data();

                        return self::set_return();

                    }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['face_ID']))
            $error_info_list[]='Face ID is empty';

        if(empty($_POST['source_ID']))
            $error_info_list[]='Source ID is empty';

        if(empty($_POST['source_account_link']))
            $error_info_list[]='Source account link is empty';

        if(empty($_POST['name']))
            $error_info_list[]='Name is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$face_ID                      =(int)$_POST['face_ID'];
        self::$source_ID                    =(int)$_POST['source_ID'];
        self::$image_ID                     =empty($_POST['image_ID'])?NULL:(int)$_POST['image_ID'];
        self::$source_account_link          =$_POST['source_account_link'];
        self::$name                         =$_POST['name'];
        self::$age                          =empty($_POST['age'])?NULL:(int)$_POST['age'];
        self::$city                         =empty($_POST['city'])?NULL:$_POST['city'];
        self::$info                         =empty($_POST['info'])?NULL:$_POST['info'];

        return self::set();

    }

}