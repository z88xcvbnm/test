<?php

namespace Project\Whore\All\Action\FaceKit;

use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Image\ImageConvert;
use Core\Module\Json\Json;
use Core\Module\Log\Log;
use Core\Module\Response\ResponseCustom;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\FaceKit\FaceKitConfig;

class UploadImageFaceKitAction{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $facekit_image_ID;

    /** @var string */
    private static $image_path;

    /** @var string */
    private static $url;

    /** @var bool */
    private static $is_without_error;

    /** @var bool */
    private static $is_need_error   =true;

    /** @var int */
    private static $sleep_timeout   =10;

    /** @var string */
    private static $file_log_path;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$face_ID              =NULL;
        self::$facekit_image_ID     =NULL;
        self::$image_path           =NULL;
        self::$url                  =NULL;
        self::$is_without_error     =NULL;
        self::$is_need_error        =true;
        self::$file_log_path        =NULL;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_ID(){

        return true;

        if(
              !Face::isset_face_ID(self::$face_ID,2)
            &&!Face::isset_face_ID(self::$face_ID,0)
        ){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function upload_face(){

        $data=[
            'custom_id'     =>self::$face_ID,
            'api_key'       =>FaceKitConfig::$face_kit_api_key,
            'image_base64'  =>ImageConvert::get_base64_encode_image(self::$image_path)
        ];

        $r=CurlPost::init(self::$url,[],$data);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Kit gave empty response'
            ];

            Log::init($error,true,true);

            if(self::$is_need_error)
                throw new PhpException($error);

            file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING upload image to FaceKit: ".Date::get_date_time_full()."\n",FILE_APPEND);

            sleep(self::$sleep_timeout);

            file_put_contents(self::$file_log_path,"RESTARTED upload image to FaceKit: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

            return self::upload_face();

        }

        switch($r['status']){

            case 500:
            case 501:
            case 502:
            case 503:
            case 504:{

                if(self::$is_without_error)
                    return false;

                $error=[
                    'error'     =>[
                        'title'     =>PhpException::$title,
                        'code'      =>$r['status'],
                        'info'      =>'Проблемы с FaceKit (#'.$r['status'].')'
                    ]
                ];

                Log::init($error,true,true);

                if(self::$is_need_error)
                    return ResponseCustom::init($error);

                file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING upload image to FaceKit: ".Date::get_date_time_full()."\n",FILE_APPEND);

                sleep(self::$sleep_timeout);

                file_put_contents(self::$file_log_path,"RESTARTED upload image to FaceKit: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                return self::upload_face();

            }

            default:{

                $r_array=Json::decode($r['data']);

                if(isset($r_array['code'])){

                    $error_info=NULL;

                    switch($r_array['code']){

                        case'not_enough_funds':{

                            $error_info='Недостаточно денего на балансе';

                            break;

                        }

                        case'too_small_distance_between_eyes':{

                            $error_info='Слишком маленькое лицо';

                            break;

                        }

                        case'no_face':{

                            $error_info='Нет лица';

                            break;

                        }

                        case'data_validation_errors':{

                            $error_info='Недостаточно параметров';

                            break;

                        }

                        case'unsupported_file_type':{

                            $error_info='Неподдерживаемый формат изображения';

                            break;

                        }

                        case'invalid_image_dimensions':{

                            $error_info='Недопустимый размер изображения';

                            break;

                        }

                        case'cannot_download_file':{

                            $error_info='Нет доступа к файлу';

                            break;

                        }

                        case'file_size_is_too_large':{

                            $error_info='Размер изображения слишком велик';

                            break;

                        }

                        default:{

                            $error=[
                                'title'     =>PhpException::$title,
                                'info'      =>'Undefined code',
                                'data'      =>$r_array
                            ];

                            throw new PhpException($error);

                        }

                    }

                    if(self::$is_without_error)
                        return false;

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>$error_info,
                        'data'      =>$r_array
                    ];

                    throw new PhpException($error);

                }

                if(!isset($r_array['id'])){

                    if(self::$is_without_error)
                        return false;

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Facekit error'
                    ];

                    throw new PhpException($error);

                }

                self::$facekit_image_ID=$r_array['id'];

                return true;

            }

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        self::$url=FaceKitConfig::get_url_api('add_image');

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'facekit_image_ID'=>self::$facekit_image_ID
        ];

        return $data;

    }

    /**
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_face_ID()){

            self::set_url();
            self::upload_face();

            return self::set_return();

        }

        return NULL;

    }

    /**
     * @param int|NULL $face_ID
     * @param string|NULL $image_path
     * @param bool $is_without_error
     * @param bool $is_need_error
     * @param string|NULL $file_log_path
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $face_ID=NULL,string $image_path=NULL,bool $is_without_error=false,bool $is_need_error=true,string $file_log_path=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($image_path))
            $error_info_list[]='Image path is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$face_ID              =$face_ID;
        self::$image_path           =$image_path;
        self::$is_without_error     =$is_without_error;
        self::$is_need_error        =$is_need_error;
        self::$file_log_path        =$file_log_path;

        return self::set();

    }

}