<?php

namespace Project\Whore\All\Action\FaceKit;

use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Image\Image;
use Core\Module\Image\ImageConvert;
use Core\Module\Json\Json;
use Core\Module\Log\Log;
use Core\Module\Response\ResponseCustom;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\All\Module\FaceKit\FaceKitConfig;

class GetFacesCoordsFaceKitAction{

    /** @var string */
    private static $url;

    /** @var int */
    private static $image_ID;

    /** @var string */
    private static $image_path;

    /** @var int */
    private static $face_len        =0;

    /** @var array */
    private static $face_list       =[];

    /** @var bool */
    private static $is_need_error   =true;

    /** @var int */
    private static $sleep_timeout   =10;

    /** @var string */
    private static $file_log_path;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$url              =NULL;
        self::$image_ID         =NULL;
        self::$image_path       =NULL;
        self::$is_need_error    =true;
        self::$file_log_path    =NULL;
        self::$face_len         =0;
        self::$face_list        =[];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_ID(){

        if(!empty(self::$image_path))
            return true;

        if(!Image::isset_image_ID(self::$image_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_face_list(){

        $data=[
            'api_key'       =>FaceKitConfig::$face_kit_api_key,
            'image_base64'  =>ImageConvert::get_base64_encode_image(self::$image_path)
        ];

        $r=CurlPost::init(self::$url,[],$data);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Kit gae empty response'
            ];

            Log::init($error,true,true);

            if(self::$is_need_error)
                throw new PhpException($error);

            file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n",FILE_APPEND);

            sleep(self::$sleep_timeout);

            file_put_contents(self::$file_log_path,"RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

            return self::get_face_list();

        }

        switch($r['status']){

            case 500:
            case 501:
            case 502:
            case 503:
            case 504:{

                $error=[
                    'error'     =>[
                        'title'     =>PhpException::$title,
                        'code'      =>$r['status'],
                        'info'      =>'Проблемы с FaceKit (#'.$r['status'].')'
                    ]
                ];

                Log::init($error,true,true);

                if(self::$is_need_error)
                    return ResponseCustom::init($error);

                file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n",FILE_APPEND);

                sleep(self::$sleep_timeout);

                file_put_contents(self::$file_log_path,"RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                return self::get_face_list();

            }

            default:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0)
                    return false;

                if(!isset($r_array['faces'])){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Faces is not exists'
                    ];

                    Log::init($error,true,true);

                    if(self::$is_need_error)
                        throw new PhpException($error);

                    file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n",FILE_APPEND);

                    sleep(self::$sleep_timeout);

                    file_put_contents(self::$file_log_path,"RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                    return self::get_face_list();

                }

                self::$face_len=$r_array['faces_detected'];

                if(self::$face_len==0)
                    return false;

                foreach($r_array['faces'] as $row)
                    self::$face_list[]=[
                        'facekit_image_ID'      =>empty($row['reference_photo_id'])?NULL:(int)$row['reference_photo_id'],
                        'face_ID'               =>empty($row['custom_id'])?NULL:(int)$row['custom_id'],
                        'confidence'            =>empty($row['confidence'])?NULL:(int)$row['confidence'],
                        'coords'                =>$row['coordinates']
                    ];

                return true;

            }

        }

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_path(){

        if(!empty(self::$image_path))
            return true;

        $r=ImagePathAction::get_image_path_data(self::$image_ID,true,true);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Image ID is not exists'
            ];

            throw new PhpException($error);

        }

        self::$image_path=$r['image_dir'].'/'.$r['image_item_ID_list']['large']['ID'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        self::$url=FaceKitConfig::get_url_api('detect_faces');

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'face_len'      =>self::$face_len,
            'face_list'     =>self::$face_list
        ];

        return $data;

    }

    /**
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_image_ID()){

            self::set_image_path();
            self::set_url();
            self::get_face_list();

            return self::set_return();

        }

        return NULL;

    }

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $image_path
     * @param bool $is_need_error
     * @param string|NULL $file_log_path
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $image_ID=NULL,string $image_path=NULL,bool $is_need_error=true,string $file_log_path=NULL){

        if(empty($image_ID)&&empty($image_path)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Image ID and image path are empty'
            ];

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$image_ID         =empty($image_ID)?NULL:$image_ID;
        self::$image_path       =empty($image_path)?NULL:$image_path;
        self::$is_need_error    =$is_need_error;
        self::$file_log_path    =$file_log_path;

        return self::set();

    }

}