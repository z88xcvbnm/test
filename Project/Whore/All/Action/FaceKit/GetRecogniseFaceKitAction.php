<?php

namespace Project\Whore\All\Action\FaceKit;

use Core\Module\Curl\CurlPost;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Image\ImageConvert;
use Core\Module\Json\Json;
use Project\Whore\All\Module\FaceKit\FaceKitConfig;

class GetRecogniseFaceKitAction{

    /** @var string */
    private static $image_path;

    /** @var string */
    private static $url;

    /** @var int */
    private static $request_ID;

    /** @var int */
    private static $face_len;

    /** @var array */
    private static $face_list;

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function detect_faces(){

        $data=[
            'api_key'       =>FaceKitConfig::$face_kit_api_key,
            'image_base64'  =>ImageConvert::get_base64_encode_image(self::$image_path)
        ];

        $r=CurlPost::init(self::$url,[],$data);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Kit gae empty response'
            ];

            throw new PhpException($error);

        }

        $r_array=Json::decode($r['data']);

        if(!isset($r_array['faces'])){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Facekit error'
            ];

            throw new PhpException($error);

        }

        self::$request_ID       =$r_array['request_id'];
        self::$face_len         =$r_array['faces_detected'];
        self::$face_list        =$r_array['faces'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        self::$url=FaceKitConfig::get_url_api('add_image');

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'face_request_ID'       =>self::$request_ID,
            'face_len'              =>self::$face_len,
            'face_list'             =>self::$face_list
        ];

        return $data;

    }

    /**
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_url();
        self::detect_faces();

        return self::set_return();

    }

    /**
     * @param int|NULL $face_ID
     * @param string|NULL $image_path
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $face_ID=NULL,string $image_path=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$image_path=$image_path;

        return self::set();

    }

}