<?php

namespace Project\Whore\All\Action\FaceKit;

use Core\Module\Curl\CurlPost;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Log\Log;
use Core\Module\Response\ResponseCustom;
use Project\Whore\All\Module\FaceKit\FaceKitConfig;

class RemoveImageFaceKitAction{

    /** @var string */
    private static $url;

    /** @var array */
    private static $facekit_image_ID_list;

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_facekit_image_list(){

        if(count(self::$facekit_image_ID_list)==0)
            return true;

        foreach(self::$facekit_image_ID_list as $facekit_image_ID)
            if(!empty($facekit_image_ID)){

                $data=[
                    'id'            =>$facekit_image_ID,
                    'api_key'       =>FaceKitConfig::$face_kit_api_key,
                    '_method'       =>'DELETE'
                ];

                $r=CurlPost::init(self::$url,[],$data);

                if(count($r)==0){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Cannot remove image from Face Kit',
                        'data'      =>$r
                    ];

                    throw new PhpException($error);

                }

                switch($r['status']){

                    case 200:
                        return true;

                    case 500:
                    case 501:
                    case 502:
                    case 503:
                    case 504:{

                        $error=[
                            'error'     =>[
                                'title'     =>PhpException::$title,
                                'code'      =>$r['status'],
                                'info'      =>'Проблемы с FaceKit (#'.$r['status'].')'
                            ]
                        ];

                        Log::init($error,true,true);

                        return ResponseCustom::init($error);

                    }

                    default:{

                        $error=[
                            'title'     =>PhpException::$title,
                            'info'      =>'Undefined Error'
                        ];

                        throw new PhpException($error);

                    }

                }

            }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        self::$url=FaceKitConfig::get_url_api('remove_image');

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return $data;

    }

    /**
     * @return array
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_url();
        self::remove_facekit_image_list();

        return self::set_return();

    }

    /**
     * @param array|NULL $facekit_image_ID_list
     * @return array
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(array $facekit_image_ID_list=NULL){

        if(empty($facekit_image_ID_list)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Facekit image ID list is empty'
            ];

            throw new ParametersException($error);

        }

        self::$facekit_image_ID_list=$facekit_image_ID_list;

        return self::set();

    }

}