<?php

namespace Project\Whore\All\Action\FacePlusPlus;

use Core\Module\Exception\PhpException;
use Core\Module\Log\Log;
use Core\Module\Response\ResponseCustom;
use Project\Whore\All\Module\Face\Faceset;

class GetLastFacesetFacePlusPlusAction{

    /** @var int */
    private static $faceset_ID;

    /** @var string */
    private static $faceset_token;

    /** @var int */
    private static $face_len;

    /** @var bool */
    private static $is_need_error               =true;

    /** @var string */
    private static $file_log_path;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$faceset_ID           =NULL;
        self::$faceset_token        =NULL;
        self::$face_len             =NULL;
        self::$is_need_error        =true;
        self::$file_log_path        =NULL;

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_last_faceset_data(){

        if(!empty(self::$file_log_path))
            file_put_contents(self::$file_log_path,"Try get last faceset ID with good limit of face token\n",FILE_APPEND);

        $r=Faceset::get_last_faceset_data(self::$face_len);

        if(!empty(self::$file_log_path))
            if(!empty($r))
                file_put_contents(self::$file_log_path,"-> Have good faceset\n",FILE_APPEND);

        if(empty($r))
            $r=CreateFasesetFacePlusPlusAction::init(self::$is_need_error,self::$file_log_path);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Faceset was not added'
            ];

            Log::init($error,true,true);

            if(self::$is_need_error)
                return ResponseCustom::init($error);

        }

        if(!empty(self::$file_log_path))
            file_put_contents(self::$file_log_path,print_r($r,true)."\n",FILE_APPEND);

        self::$faceset_ID           =$r['faceset_ID'];
        self::$faceset_token        =$r['faceset_token'];

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'faceset_ID'        =>self::$faceset_ID,
            'faceset_token'     =>self::$faceset_token
        ];

        return $data;

    }

    /**
     * @return array
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_last_faceset_data();

        return self::set_return();

    }

    /**
     * @param int|NULL $need_add_face_len
     * @param bool $is_need_error
     * @param string|NULL $file_log_path
     * @return array
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $need_add_face_len=NULL,bool $is_need_error=true,string $file_log_path=NULL){

        self::reset_data();

        self::$face_len             =$need_add_face_len;
        self::$is_need_error        =$is_need_error;
        self::$file_log_path        =$file_log_path;

        return self::set();

    }

}