<?php

namespace Project\Whore\All\Action\FacePlusPlus;

use Core\Module\Curl\CurlPost;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Image\Image;
use Core\Module\Json\Json;
use Core\Module\Sort\Sort;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Face\FaceSearchItem;
use Project\Whore\All\Module\Face\FaceSearchItemParts;
use Project\Whore\All\Module\Face\Faceset;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class SearchFaceTokenInFacesetPartsFacePlusPlusAction{

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $image_ID;

    /** @var int */
    private static $face_search_ID;

    /** @var int */
    private static $faceset_ID;

    /** @var int */
    private static $face_search_multi_ID;

    /** @var string */
    private static $faceset_token;

    /** @var int */
    private static $faceset_index;

    /** @var int */
    private static $faceset_len;

    /** @var string */
    private static $url;

    /** @var string */
    private static $face_token;

    /** @var float */
    private static $percent;

    /** @var int */
    private static $percent_min         =75;

    /** @var int */
    private static $face_len_max        =6;

    /** @var array */
    private static $face_coords         =[];

    /** @var array */
    private static $face_token_list     =[];

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image(){

        if(!Image::isset_image_ID(self::$image_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $faceset_ID
     * @param string|NULL $faceset_token
     * @return array
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_face_token(int $faceset_ID=NULL,string $faceset_token=NULL){

        $error_info_list=[];

        if(empty($faceset_ID))
            $error_info_list[]='Faceset ID is empty';

        if(empty($faceset_token))
            $error_info_list[]='Faceset ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $key_data=FacePlusPlusConfig::get_key_data();

        if(empty($key_data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Keys is not exists'
            ];

            throw new PhpException($error);

        }

        $data=[
            'api_key'                   =>$key_data['api_key'],
            'api_secret'                =>$key_data['api_secret_key'],
            'faceset_token'             =>$faceset_token,
            'face_token'                =>self::$face_token,
            'return_result_count'       =>FacePlusPlusConfig::$search_result_count
        ];

        $r=CurlPost::init(self::$url,[],$data);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Services gave empty response'
            ];

            throw new PhpException($error);

        }

        switch($r['status']){

            case 400:
            case 401:
            case 402:
            case 403:
            case 404:
            case 412:
            case 413:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0)
                    return NULL;

                $error=[
                    'title'         =>PhpException::$title,
                    'code'          =>$r['status'],
                    'info'          =>$r_array['error_message'],
                    'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used']

                ];

                $error_value_list=mb_split('\:',$r_array['error_message']);

                if(count($error_value_list)>0)
                    switch($error_value_list[0]){

                        case'CONCURRENCY_LIMIT_EXCEEDED':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Слишком частые запросы для бесплатного ключа';
                            else
                                $error['error']['info']='Слишком частые запросы';

                            break;

                        }

                        case'AUTHENTICATION_ERROR':{

                            if($r['status']==401)
                                $error['error']['info']='Проблемы авторизации';
                            else if($r['status']==403)
                                $error['error']['info']='Проблемы доступа к методу API';

                            break;

                        }

                        case'AUTHORIZATION_ERROR':{

                            switch($error_value_list[1]){

                                case'Denied by Client':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано клиентом';

                                    break;

                                }

                                case'Denied by Admin':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано администратором';

                                    break;

                                }

                                case'Insufficient Account Balance':{

                                    $error['error']['info']='Недостаточный остаток на счете';

                                    break;

                                }

                            }

                            throw new PhpException($error);

                            break;

                        }

                        case'MISSING_ARGUMENTS':{

                            $error['error']['info']='Не достаточно переменных';

                            break;

                        }

                        case'BAD_ARGUMENTS':{

                            $error['error']['info']='Плохие переменные';

                            break;

                        }

                        case'COEXISTENCE_ARGUMENTS':{

                            $error['error']['info']='Неверные аргументы';

                            break;

                        }

                        case'REQUEST_ENTITY_TOO_LARGE':
                        case'Request Entity Too Large':{

                            $error['error']['info']='Слишком большой запрос';

                            break;

                        }

                        case'API_NOT_FOUND':{

                            $error['error']['info']='Метод API не найден';

                            break;

                        }

                        case'INTERNAL_ERROR':{

                            $error['error']['info']='Сервис упал';

                            break;

                        }

                        case'IMAGE_ERROR_UNSUPPORTED_FORMAT':{

                            $error['error']['info']='Формат изображения не поддерживается';

                            break;

                        }

                        case'INVALID_IMAGE_SIZE':{

                            $error['error']['info']='Слишком большой размер изображения';

                            break;

                        }

                        case'INVALID_IMAGE_URL':{

                            $error['error']['info']='Изображение не загрузилось';

                            break;

                        }

                        case'IMAGE_FILE_TOO_LARGE':{

                            $error['error']['info']='Слишком большое изображение';

                            break;

                        }

                        case'INSUFFICIENT_PERMISSION':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Переменная не доступна для бесплатного ключа';
                            else
                                $error['error']['info']='Сервис что-то начудил';

                            break;

                        }

                        case'IMAGE_DOWNLOAD_TIMEOUT':{

                            $error['error']['info']='Превышен лимит времени для изображения';

                            break;

                        }

                        case'INVALID_FACESET_TOKEN':{

                            $error['error']['info']='Неверный faceset token';

                            break;

                        }

                        case'INVALID_FACE_TOKEN':{

                            $error['error']['info']='Неверный face token';

                            break;

                        }

                        case'INVALID_OUTER_ID':{

                            $error['error']['info']='Неверный outer ID';

                            break;

                        }

                        case'EMPTY_FACESET':{

//                            $error['error']['info']='Пустой faceset';
//
//                            break;

                            return[];

                        }

                        case'INVALID_FACE_TOKENS_SIZE':{

                            $error['error']['info']='Неверный размер face token';

                            break;

                        }

                    }

                throw new PhpException($error);

            }

            case 500:
            case 501:
            case 502:
            case 503:
            case 504:{

                $error=[
                    'title'         =>PhpException::$title,
                    'code'          =>$r['status'],
                    'info'          =>'Проблемы с Face Service(#'.$r['status'].')'
                ];

                throw new PhpException($error);

            }

            default:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0){

                    $error=[
                        'title'         =>PhpException::$title,
                        'code'          =>$r['status'],
                        'info'          =>'Проблемы с Face Service(#'.$r['status'].')'
                    ];

                    throw new PhpException($error);

                }

                if(isset($r_array['error_message'])){

                    $error=[
                        'error'     =>[
                            'title'         =>PhpException::$title,
                            'code'          =>$r['status'],
                            'info'          =>$r_array['error_message'],
                            'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used']
                        ]
                    ];

                    throw new PhpException($error);

                }

                if(!isset($r_array['results'])){

                    $error=[
                        'error'     =>[
                            'title'         =>PhpException::$title,
                            'info'          =>'Results is empty'
                        ]
                    ];

                    throw new PhpException($error);

                }

                $face_token_list            =$r_array['results'];
                $face_token_temp_list       =[];

                foreach($face_token_list as $row){

                    $face_token_temp_list[]     =$row['face_token'];
                    self::$face_token_list[]    =[
                        'request_ID'        =>$r_array['request_id'],
                        'faceset_ID'        =>$faceset_ID,
                        'faceset_token'     =>$faceset_token,
                        'face_token'        =>$row['face_token'],
                        'percent'           =>(int)($row['confidence']*100)
                    ];

                }

                return $face_token_temp_list;

            }

        }

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_search_item_parts(){

        if(count(self::$face_token_list)==0)
            return true;

        $r=FaceSearchItemParts::add_face_search_item_list(self::$face_search_ID,self::$faceset_index,self::$face_token_list,self::$face_search_multi_ID);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face search item was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_search_item(){

        if(count(self::$face_token_list)==0)
            return true;

        $list           =[];
        $percent_x_100  =self::$percent_min*100;

        self::$face_token_list=Sort::sort_with_key('percent',self::$face_token_list,'desc');

        $face_max_len   =20;
        $face_ID_list   =[];

        foreach(self::$face_token_list as $row)
            if($row['percent']>=$percent_x_100){

                $face_ID_list[$row['face_ID']]=true;

                $list[]=$row;

                if(count($face_ID_list)>=$face_max_len)
                    break;

            }

        if(count($list)==0)
            return true;

        $r=FaceSearchItem::add_face_search_item_list(self::$face_search_ID,$list,self::$face_search_multi_ID);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face search item was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param array $face_token_list
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_image_data(array $face_token_list=[]){

        if(count($face_token_list)==0)
            return[];

        $face_image_data_list=FaceImage::get_face_data_from_face_token_list($face_token_list);

        if(count($face_image_data_list)==0)
            return[];

        $list=[];

        foreach(self::$face_token_list as $face_token_index=>$face_token_row)
            if(isset($face_image_data_list[$face_token_row['face_token']]))
                $list[]=array_merge($face_token_row,$face_image_data_list[$face_token_row['face_token']]);

        return $list;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_faceset_list(){

        $r=Faceset::get_faceset_data_from_index(self::$faceset_index);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Faceset index is not correct'
            ];

            throw new PhpException($error);

        }

        self::$faceset_ID       =$r['faceset_ID'];
        self::$faceset_token    =$r['faceset_token'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        self::$url=FacePlusPlusConfig::get_url_api('search_faces');

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_token_list(){

        if(empty(self::$faceset_ID)||empty(self::$faceset_token))
            return true;

//        $face_token_list=[];
//
//        foreach(self::$faceset_list as $row){
//
//            $face_token_temp_list   =self::get_face_token($row['faceset_ID'],$row['faceset_token']);
//            $face_token_list        =array_merge($face_token_list,$face_token_temp_list);
//
//        }

        $face_token_list            =self::get_face_token(self::$faceset_ID,self::$faceset_token);

        if(count($face_token_list)>0)
            self::$face_token_list=self::prepare_face_image_data($face_token_list);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_facesearch_item(){

        if(self::$faceset_index!=self::$faceset_len)
            return true;

        self::$face_token_list=FaceSearchItemParts::get_face_search_item_data_list_from_face_search_ID(self::$face_search_ID);

        if(count(self::$face_token_list)==0)
            return true;

        self::add_face_search_item();

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_finish_data(){

        if(self::$faceset_index!=self::$faceset_len)
            return true;

        self::set_facesearch_item();

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        if(self::$faceset_index==self::$faceset_len)
            $data=[
                'success'               =>true,
                'face_search_ID'        =>self::$face_search_ID,
                'face_token_list'       =>self::$face_token_list
            ];
        else
            $data=[
                'success'=>true
            ];

        return $data;

    }

    /**
     * @return bool
     */
    private static function set_percent(){

        self::$percent=0;

        if(count(self::$face_token_list)==0)
            return true;

        foreach(self::$face_token_list as $row)
            if(self::$percent<$row['percent']){

                self::$face_ID      =$row['face_ID'];
                self::$percent      =$row['percent'];

            }

        return true;

    }

    /**
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_image()){

            self::set_url();

            self::set_faceset_list();
            self::set_face_token_list();

            self::set_percent();

            self::add_face_search_item_parts();

            self::prepare_finish_data();

            return self::set_return();

        }

        return NULL;

    }

    /**
     * @param int|NULL $face_search_ID
     * @param int|NULL $file_ID
     * @param int|NULL $image_ID
     * @param string|NULL $face_token
     * @param int|NULL $faceset_index
     * @param int|NULL $faceset_len
     * @param array $face_coords
     * @param int $face_len_max
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $face_search_ID=NULL,int $file_ID=NULL,int $image_ID=NULL,string $face_token=NULL,int $faceset_index=NULL,int $faceset_len=NULL,array $face_coords=[],int $face_len_max=6,int $face_search_multi_ID=NULL){

        $error_info_list=[];

        if(empty($face_search_ID))
            $error_info_list[]='Face search ID is empty';

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($face_token))
            $error_info_list[]='Face token is empty';

        if(count($face_coords)==0)
            $error_info_list[]='Face coords is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$face_search_ID           =$face_search_ID;
        self::$file_ID                  =$file_ID;
        self::$image_ID                 =$image_ID;
        self::$face_token               =$face_token;
        self::$faceset_index            =$faceset_index;
        self::$faceset_len              =$faceset_len;
        self::$face_coords              =$face_coords;
        self::$face_len_max             =$face_len_max;
        self::$face_search_multi_ID     =$face_search_multi_ID;

        return self::set();

    }

}