<?php

namespace Project\Whore\All\Action\FacePlusPlus;

use Core\Module\Curl\CurlPost;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Project\Whore\All\Module\Face\Faceset;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class RemoveFaceTokenListFacePlusPlusAction{

    /** @var string */
    private static $url;

    /** @var array */
    private static $face_token_list=[];

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        self::$url=FacePlusPlusConfig::get_url_api('remove_face');

        if(empty(self::$url)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'URL was not set'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_token_list(){

        if(count(self::$face_token_list)==0)
            return true;

        foreach(self::$face_token_list as $row)
            if(
                  !empty($row['faceset_token'])
                &&!empty($row['face_token'])
            )
                self::remove_face_token($row);

        return true;

    }

    /**
     * @param array|NULL $face_data
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_face_token(array $face_data=NULL){

        if(empty($face_data))
            return false;

        if(empty($face_data['faceset_token']))
            return false;

        $faceset_ID     =$face_data['faceset_ID'];
        $key_data       =FacePlusPlusConfig::get_key_data();

        if(empty($key_data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Keys is not exists'
            ];

            throw new PhpException($error);

        }

        $data=[
            'api_key'           =>$key_data['api_key'],
            'api_secret'        =>$key_data['api_secret_key'],
            'faceset_token'     =>$face_data['faceset_token'],
            'face_tokens'       =>$face_data['face_token']
        ];

        $r=CurlPost::init(self::$url,[],$data);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Services gave empty response'
            ];

            throw new PhpException($error);

        }

        switch($r['status']){

            case 400:
            case 401:
            case 402:
            case 403:
            case 404:
            case 412:
            case 413:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0)
                    return false;

                $error=[
                    'title'         =>PhpException::$title,
                    'code'          =>$r['status'],
                    'info'          =>$r_array['error_message'],
                    'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used']

                ];

                $error_value_list=mb_split('\:',$r_array['error_message']);

                if(count($error_value_list)>0)
                    switch($error_value_list[0]){

                        case'CONCURRENCY_LIMIT_EXCEEDED':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Слишком частые запросы для бесплатного ключа';
                            else
                                $error['error']['info']='Слишком частые запросы';

                            break;

                        }

                        case'AUTHENTICATION_ERROR':{

                            if($r['status']==401)
                                $error['error']['info']='Проблемы авторизации';
                            else if($r['status']==403)
                                $error['error']['info']='Проблемы доступа к методу API';

                            break;

                        }

                        case'AUTHORIZATION_ERROR':{

                            switch($error_value_list[1]){

                                case'Denied by Client':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано клиентом';

                                    break;

                                }

                                case'Denied by Admin':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано администратором';

                                    break;

                                }

                                case'Insufficient Account Balance':{

                                    $error['error']['info']='Недостаточный остаток на счете';

                                    break;

                                }

                            }

                            throw new PhpException($error);

                            break;

                        }

                        case'MISSING_ARGUMENTS':{

                            $error['error']['info']='Не достаточно переменных';

                            break;

                        }

                        case'BAD_ARGUMENTS':{

                            $error['error']['info']='Плохие переменные';

                            break;

                        }

                        case'COEXISTENCE_ARGUMENTS':{

                            $error['error']['info']='Неверные аргументы';

                            break;

                        }

                        case'REQUEST_ENTITY_TOO_LARGE':
                        case'Request Entity Too Large':{

                            $error['error']['info']='Слишком большой запрос';

                            break;

                        }

                        case'API_NOT_FOUND':{

                            $error['error']['info']='Метод API не найден';

                            break;

                        }

                        case'INTERNAL_ERROR':{

                            $error['error']['info']='Сервис упал';

                            break;

                        }

                        case'IMAGE_ERROR_UNSUPPORTED_FORMAT':{

                            $error['error']['info']='Формат изображения не поддерживается';

                            break;

                        }

                        case'INVALID_IMAGE_SIZE':{

                            $error['error']['info']='Слишком большой размер изображения';

                            break;

                        }

                        case'INVALID_IMAGE_URL':{

                            $error['error']['info']='Изображение не загрузилось';

                            break;

                        }

                        case'IMAGE_FILE_TOO_LARGE':{

                            $error['error']['info']='Слишком большое изображение';

                            break;

                        }

                        case'INSUFFICIENT_PERMISSION':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Переменная не доступна для бесплатного ключа';
                            else
                                $error['error']['info']='Сервис что-то начудил';

                            break;

                        }

                        case'IMAGE_DOWNLOAD_TIMEOUT':{

                            $error['error']['info']='Превышен лимит времени для изображения';

                            break;

                        }

                        case'INVALID_FACESET_TOKEN':{

                            $error['error']['info']='Неверный faceset token';

                            break;

                        }

                        case'INVALID_OUTER_ID':{

                            $error['error']['info']='Неверный outer ID';

                            break;

                        }

                        case'INVALID_FACE_TOKENS_SIZE':{

                            $error['error']['info']='Неверный размер face token';

                            break;

                        }

                    }

                throw new PhpException($error);

            }

            case 500:
            case 501:
            case 502:
            case 503:
            case 504:{

                $error=[
                    'title'         =>PhpException::$title,
                    'code'          =>$r['status'],
                    'info'          =>'Проблемы с Face Service(#'.$r['status'].')'
                ];

                throw new PhpException($error);

            }

            default:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0){

                    $error=[
                        'title'         =>PhpException::$title,
                        'code'          =>$r['status'],
                        'info'          =>'Проблемы с Face Service(#'.$r['status'].')'
                    ];

                    throw new PhpException($error);

                }

                if(isset($r_array['error_message'])){

                    $error=[
                        'error'     =>[
                            'title'         =>PhpException::$title,
                            'code'          =>$r['status'],
                            'info'          =>$r_array['error_message'],
                            'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used']
                        ]
                    ];

                    throw new PhpException($error);

                }

                if(!Faceset::update_faceset_face_len($faceset_ID,$r_array['face_count'])){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Faceset was not update'
                    ];

                    throw new PhpException($error);

                }

                return true;

            }

        }

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return $data;

    }

    /**
     * @return array
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_url();

        self::prepare_face_token_list();

        return self::set_return();

    }

    /**
     * @param array $face_token_list
     * @return array|null
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(array $face_token_list=[]){

        if(count($face_token_list)==0)
            return NULL;

        self::$face_token_list=$face_token_list;

        return self::set();

    }

}