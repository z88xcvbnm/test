<?php

namespace Project\Whore\All\Action\FacePlusPlus;

use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Core\Module\Log\Log;
use Core\Module\Response\ResponseCustom;
use Project\Whore\All\Module\Face\Faceset;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class CreateFasesetFacePlusPlusAction{

    /** @var int */
    private static $faceset_ID;

    /** @var string */
    private static $faceset_token;

    /** @var string */
    private static $url;

    /** @var int */
    private static $time_used                   =0;

    /** @var bool */
    private static $is_need_error               =true;

    /** @var string */
    private static $file_log_path;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$faceset_ID                   =NULL;
        self::$faceset_token                =NULL;
        self::$url                          =NULL;
        self::$is_need_error                =true;
        self::$file_log_path                =NULL;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_faceset_ID(){

        self::$faceset_ID=Faceset::add_faceset(NULL,NULL,0,2);

        if(empty(self::$faceset_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>"Faceset is empty"
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_faceset_token(){

        if(empty(self::$faceset_token)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Faceset token is empty'
            ];

            throw new PhpException($error);

        }

        if(!Faceset::update_faceset_token(self::$faceset_ID,self::$faceset_token,0)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Faceset was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function create_faceset(){

        $key_data=FacePlusPlusConfig::get_key_data();

        if(empty($key_data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Keys is not exists'
            ];

            throw new PhpException($error);

        }

        $data=[
            'api_key'           =>$key_data['api_key'],
            'api_secret'        =>$key_data['api_secret_key'],
            'outer_id'          =>self::$faceset_ID
        ];

        $r=CurlPost::init(self::$url,[],$data);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Services gave empty response'
            ];

            Log::init($error,true,true);

            if(self::$is_need_error)
                throw new PhpException($error);

            file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING upload image to FaceKit: ".Date::get_date_time_full()."\n",FILE_APPEND);

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            file_put_contents(self::$file_log_path,"RESTARTED upload image to FaceKit: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

            return self::upload_face();

        }

        switch($r['status']){

            case 400:
            case 401:
            case 402:
            case 403:
            case 404:
            case 412:
            case 413:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0)
                    return false;

                $error=[
                    'error'     =>[
                        'title'         =>PhpException::$title,
                        'code'          =>$r['status'],
                        'info'          =>$r_array['error_message'],
                        'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used'],
                        'force_merge'   =>FacePlusPlusConfig::$force_merge
                    ]
                ];

                $error_value_list=mb_split('\:',$r_array['error_message']);

                if(count($error_value_list)>0)
                    switch($error_value_list[0]){

                        case'CONCURRENCY_LIMIT_EXCEEDED':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Слишком частые запросы для бесплатного ключа';
                            else
                                $error['error']['info']='Слишком частые запросы';

                            break;

                        }

                        case'AUTHENTICATION_ERROR':{

                            if($r['status']==401)
                                $error['error']['info']='Проблемы авторизации';
                            else if($r['status']==403)
                                $error['error']['info']='Проблемы доступа к методу API';

                            break;

                        }

                        case'AUTHORIZATION_ERROR':{

                            switch($error_value_list[1]){

                                case'Denied by Client':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано клиентом';

                                    break;

                                }

                                case'Denied by Admin':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано администратором';

                                    break;

                                }

                                case'Insufficient Account Balance':{

                                    $error['error']['info']='Недостаточный остаток на счете';

                                    break;

                                }

                            }

                            throw new PhpException($error);

                            break;

                        }

                        case'MISSING_ARGUMENTS':{

                            $error['error']['info']='Не достаточно переменных';

                            break;

                        }

                        case'BAD_ARGUMENTS':{

                            $error['error']['info']='Плохие переменные';

                            break;

                        }

                        case'COEXISTENCE_ARGUMENTS':{

                            $error['error']['info']='Неверные аргументы';

                            break;

                        }

                        case'REQUEST_ENTITY_TOO_LARGE':
                        case'Request Entity Too Large':{

                            $error['error']['info']='Слишком большой запрос';

                            break;

                        }

                        case'API_NOT_FOUND':{

                            $error['error']['info']='Метод API не найден';

                            break;

                        }

                        case'INTERNAL_ERROR':{

                            $error['error']['info']='Сервис упал';

                            break;

                        }

                        case'IMAGE_ERROR_UNSUPPORTED_FORMAT':{

                            $error['error']['info']='Формат изображения не поддерживается';

                            break;

                        }

                        case'INVALID_IMAGE_SIZE':{

                            $error['error']['info']='Слишком большой размер изображения';

                            break;

                        }

                        case'INVALID_IMAGE_URL':{

                            $error['error']['info']='Изображение не загрузилось';

                            break;

                        }

                        case'IMAGE_FILE_TOO_LARGE':{

                            $error['error']['info']='Слишком большое изображение';

                            break;

                        }

                        case'INSUFFICIENT_PERMISSION':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Переменная не доступна для бесплатного ключа';
                            else
                                $error['error']['info']='Сервис что-то начудил';

                            break;

                        }

                        case'IMAGE_DOWNLOAD_TIMEOUT':{

                            $error['error']['info']='Превышен лимит времени для изображения';

                            break;

                        }

                        case'FACESET_EXIST':{

                            $error['error']['info']='Faceset уже существует';

                            break;

                        }

                        case'FACESET_QUOTA_EXCEEDED':{

                            $error['error']['info']='Достигнут лимит количества faceset';

                            break;

                        }

                        case'INVALID_FACE_TOKENS_SIZE':{

                            $error['error']['info']='Неверный размер face token';

                            break;

                        }

                    }

                Log::init($error,true,true);

                if(self::$is_need_error)
                    return ResponseCustom::init($error);

                file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n",FILE_APPEND);

                sleep(FacePlusPlusConfig::get_random_sleep_timeout());

                file_put_contents(self::$file_log_path,"RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                return self::create_faceset();

            }

            case 500:
            case 501:
            case 502:
            case 503:
            case 504:{

                $error=[
                    'error'     =>[
                        'title'         =>PhpException::$title,
                        'code'          =>$r['status'],
                        'info'          =>'Проблемы с Face Service(#'.$r['status'].')'
                    ]
                ];

                Log::init($error,true,true);

                if(self::$is_need_error)
                    return ResponseCustom::init($error);

                file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n",FILE_APPEND);

                sleep(FacePlusPlusConfig::get_random_sleep_timeout());

                file_put_contents(self::$file_log_path,"RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                return self::create_faceset();

            }

            default:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0)
                    return false;

                if(isset($r_array['error_message'])){

                    $error=[
                        'error'     =>[
                            'title'         =>PhpException::$title,
                            'code'          =>$r['status'],
                            'info'          =>$r_array['error_message'],
                            'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used']
                        ]
                    ];

                    Log::init($error,true,true);

                    if(self::$is_need_error)
                        return ResponseCustom::init($error);

                    Log::init($error,true,true);

                    if(self::$is_need_error)
                        return ResponseCustom::init($error);

                    file_put_contents(self::$file_log_path,"FAIL added face_token to faceset: ".print_r($error,true)." > ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                    return false;

                }

                if(isset($r_array['failure_detail']))
                    if(count($r_array['failure_detail'])>0){

                        $error=[
                            'error'     =>[
                                'title'         =>PhpException::$title,
                                'code'          =>$r['status']
                            ]
                        ];

                        if(count($r_array['failure_detail'])>0)
                            foreach($r_array['failure_detail'] as $fail_row)
                                switch($fail_row){

                                    case'INVALID_FACE_TOKEN':{

                                        $error['error']['info']='Face token не существует';

                                        break;

                                    }

                                    case'QUOTA_EXCEEDED':{

                                        $error['error']['info']='Достигнут лимит лиц в профиле';

                                        break;

                                    }

                                    default:{

                                        $error['error']['info']='Неизвестная ошибка';

                                        break;

                                    }

                                }

                        Log::init($error,true,true);

                        if(self::$is_need_error)
                            return ResponseCustom::init($error);

                        file_put_contents(self::$file_log_path,"FAIL added face_token to faceset: ".print_r($error,true)." > ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                        return false;

                    }

                if(!isset($r_array['faceset_token'])){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Faceset is not exists'
                    ];

                    Log::init($error,true,true);

                    if(self::$is_need_error)
                        throw new PhpException($error);

                    file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n",FILE_APPEND);

                    sleep(FacePlusPlusConfig::get_random_sleep_timeout());

                    file_put_contents(self::$file_log_path,"RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                    return self::create_faceset();

                }

                self::$faceset_token        =$r_array['faceset_token'];
                self::$time_used            =(int)$r_array['time_used'];

                self::$time_used+=(int)$r_array['time_used'];

                return true;

            }

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        self::$url=FacePlusPlusConfig::get_url_api('add_faceset');

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'faceset_ID'        =>self::$faceset_ID,
            'faceset_token'     =>self::$faceset_token,
            'time_used'         =>self::$time_used
        ];

        return $data;

    }

    /**
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_url();

        self::set_faceset_ID();
        self::create_faceset();
        self::update_faceset_token();

        return self::set_return();

    }

    /**
     * @param bool $is_need_error
     * @param string|NULL $file_log_path
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(bool $is_need_error=true,string $file_log_path=NULL){

        self::reset_data();

        self::$is_need_error        =$is_need_error;
        self::$file_log_path        =$file_log_path;

        return self::set();

    }

}