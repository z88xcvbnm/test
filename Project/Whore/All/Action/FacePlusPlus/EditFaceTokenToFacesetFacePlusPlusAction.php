<?php

namespace Project\Whore\All\Action\FacePlusPlus;

use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Core\Module\Log\Log;
use Core\Module\Response\ResponseCustom;
use Core\Module\User\User;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Face\Faceset;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class EditFaceTokenToFacesetFacePlusPlusAction{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $faceset_ID;

    /** @var string */
    private static $faceset_token;

    /** @var array */
    private static $face_token_list                 =[];

    /** @var array */
    private static $image_list                      =[];

    /** @var int */
    private static $face_len                        =0;

    /** @var string */
    private static $url;

    /** @var int */
    private static $time_used                       =0;

    /** @var bool */
    private static $is_need_error                   =true;

    /** @var bool */
    private static $need_update_exists_face_image   =false;

    /** @var string */
    private static $file_log_path;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$face_ID                              =NULL;
        self::$faceset_ID                           =NULL;
        self::$faceset_token                        =NULL;
        self::$face_token_list                      =[];
        self::$image_list                           =[];
        self::$face_len                             =0;
        self::$url                                  =NULL;
        self::$is_need_error                        =true;
        self::$file_log_path                        =NULL;
        self::$time_used                            =0;
        self::$need_update_exists_face_image        =false;

        return true;

    }

    /**
     * @return bool
     */
    private static function prepare_image_list(){

        if(count(self::$image_list)==0)
            return true;

        $image_list=[];

        foreach(self::$image_list as $row){

            if(!empty($row['face_token']))
                self::$face_token_list[$row['image_ID']]=$row['face_token'];

            $image_list[$row['image_ID']]=[
                'ID'                =>empty($row['ID'])?NULL:$row['ID'],
                'file_ID'           =>$row['file_ID'],
                'image_ID'          =>$row['image_ID'],
                'faceset_ID'        =>NULL,
                'faceset_token'     =>NULL,
                'face_token'        =>$row['face_token'],
                'face_count'        =>count($row['coords']),
                'coords'            =>$row['coords']
            ];

        }

        self::$image_list   =$image_list;
        self::$face_len     =count($image_list);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_faceset_last(){

        $r=GetLastFacesetFacePlusPlusAction::init(self::$face_len,true,self::$file_log_path);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Faceset was not create'
            ];

            throw new PhpException($error);

        }

        self::$faceset_ID       =$r['faceset_ID'];
        self::$faceset_token    =$r['faceset_token'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_list(){

        $face_part_len      =ceil(self::$face_len/FacePlusPlusConfig::$face_token_part_len);
        $face_part_index    =0;

        if(!empty(self::$file_log_path))
            file_put_contents(self::$file_log_path,"-->> ".$face_part_index." < ".$face_part_len."\n",FILE_APPEND);

        while($face_part_index<$face_part_len){

            $face_part_index        ++;
            $part_len               =count(self::$face_token_list)>FacePlusPlusConfig::$face_token_part_len?FacePlusPlusConfig::$face_token_part_len:count(self::$face_token_list);
            $face_token_list        =array_splice(self::$face_token_list,0,$part_len);

            if(!empty(self::$file_log_path))
                file_put_contents(self::$file_log_path,"-->> Face token list:\n".print_r($face_token_list,true)."\n",FILE_APPEND);

            if(count($face_token_list)==0)
                return true;

            self::add_face_list($face_token_list);

        }

        return true;

    }

    /**
     * @param array $face_token_list
     * @return bool|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_list(array $face_token_list=[]){

        if(count($face_token_list)==0)
            return NULL;

        $key_data=FacePlusPlusConfig::get_key_data();

        if(empty($key_data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Keys is not exists'
            ];

            throw new PhpException($error);

        }

        $data=[
            'api_key'           =>$key_data['api_key'],
            'api_secret'        =>$key_data['api_secret_key'],
            'faceset_token'     =>self::$faceset_token,
            'face_tokens'       =>implode(',',$face_token_list)
        ];

        $r=CurlPost::init(self::$url,[],$data);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Services gave empty response'
            ];

            Log::init($error,true,true);

            if(self::$is_need_error)
                throw new PhpException($error);

            file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING upload image to FaceKit: ".Date::get_date_time_full()."\n",FILE_APPEND);

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            file_put_contents(self::$file_log_path,"RESTARTED upload image to FaceKit: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

            return self::add_face_list($face_token_list);

        }

        switch($r['status']){

            case 400:
            case 401:
            case 402:
            case 403:
            case 404:
            case 412:
            case 413:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0)
                    return false;

                $error=[
                    'error'     =>[
                        'title'         =>PhpException::$title,
                        'code'          =>$r['status'],
                        'info'          =>$r_array['error_message'],
                        'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used'],
                        'force_merge'   =>FacePlusPlusConfig::$force_merge
                    ]
                ];

                $error_value_list=mb_split('\:',$r_array['error_message']);

                if(count($error_value_list)>0)
                    switch($error_value_list[0]){

                        case'CONCURRENCY_LIMIT_EXCEEDED':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Слишком частые запросы для бесплатного ключа';
                            else
                                $error['error']['info']='Слишком частые запросы';

                            break;

                        }

                        case'AUTHENTICATION_ERROR':{

                            if($r['status']==401)
                                $error['error']['info']='Проблемы авторизации';
                            else if($r['status']==403)
                                $error['error']['info']='Проблемы доступа к методу API';

                            break;

                        }

                        case'AUTHORIZATION_ERROR':{

                            switch($error_value_list[1]){

                                case'Denied by Client':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано клиентом';

                                    break;

                                }

                                case'Denied by Admin':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано администратором';

                                    break;

                                }

                                case'Insufficient Account Balance':{

                                    $error['error']['info']='Недостаточный остаток на счете';

                                    break;

                                }

                            }

                            throw new PhpException($error);

                            break;

                        }

                        case'MISSING_ARGUMENTS':{

                            $error['error']['info']='Не достаточно переменных';

                            break;

                        }

                        case'BAD_ARGUMENTS':{

                            $error['error']['info']='Плохие переменные';

                            break;

                        }

                        case'COEXISTENCE_ARGUMENTS':{

                            $error['error']['info']='Неверные аргументы';

                            break;

                        }

                        case'REQUEST_ENTITY_TOO_LARGE':
                        case'Request Entity Too Large':{

                            $error['error']['info']='Слишком большой запрос';

                            break;

                        }

                        case'API_NOT_FOUND':{

                            $error['error']['info']='Метод API не найден';

                            throw new PhpException($error);

                        }

                        case'INTERNAL_ERROR':{

                            $error['error']['info']='Сервис упал';

                            break;

                        }

                        case'IMAGE_ERROR_UNSUPPORTED_FORMAT':{

                            $error['error']['info']='Формат изображения не поддерживается';

                            break;

                        }

                        case'INVALID_IMAGE_SIZE':{

                            $error['error']['info']='Слишком большой размер изображения';

                            break;

                        }

                        case'INVALID_IMAGE_URL':{

                            $error['error']['info']='Изображение не загрузилось';

                            break;

                        }

                        case'IMAGE_FILE_TOO_LARGE':{

                            $error['error']['info']='Слишком большое изображение';

                            break;

                        }

                        case'INSUFFICIENT_PERMISSION':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Переменная не доступна для бесплатного ключа';
                            else
                                $error['error']['info']='Сервис что-то начудил';

                            break;

                        }

                        case'IMAGE_DOWNLOAD_TIMEOUT':{

                            $error['error']['info']='Превышен лимит времени для изображения';

                            break;

                        }

                        case'INVALID_FACESET_TOKEN':{

                            $error['error']['info']='Неверный faceset token';

                            throw new PhpException($error);

                        }

                        case'INVALID_OUTER_ID':{

                            $error['error']['info']='Неверный outer ID';

                            throw new PhpException($error);

                        }

                        case'INVALID_FACE_TOKENS_SIZE':{

                            $error['error']['info']='Неверный размер face token';

                            throw new PhpException($error);

                        }

                    }

                Log::init($error,true,true);

                if(self::$is_need_error)
                    return ResponseCustom::init($error);

                file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n",FILE_APPEND);

                sleep(FacePlusPlusConfig::get_random_sleep_timeout());

                file_put_contents(self::$file_log_path,"RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                return self::add_face_list($face_token_list);

            }

            case 500:
            case 501:
            case 502:
            case 503:
            case 504:{

                $error=[
                    'error'     =>[
                        'title'         =>PhpException::$title,
                        'code'          =>$r['status'],
                        'info'          =>'Проблемы с Face Service(#'.$r['status'].')'
                    ]
                ];

                Log::init($error,true,true);

                if(self::$is_need_error)
                    return ResponseCustom::init($error);

                file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n",FILE_APPEND);

                sleep(FacePlusPlusConfig::get_random_sleep_timeout());

                file_put_contents(self::$file_log_path,"RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                return self::add_face_list($face_token_list);

            }

            default:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0)
                    return false;

                if(isset($r_array['error_message'])){

                    $error=[
                        'error'     =>[
                            'title'         =>PhpException::$title,
                            'code'          =>$r['status'],
                            'info'          =>$r_array['error_message'],
                            'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used']
                        ]
                    ];

                    Log::init($error,true,true);

                    if(self::$is_need_error)
                        return ResponseCustom::init($error);

                    Log::init($error,true,true);

                    if(self::$is_need_error)
                        return ResponseCustom::init($error);

                    file_put_contents(self::$file_log_path,"FAIL added face_token to faceset: ".print_r($error,true)." > ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                    return false;

                }

                if(isset($r_array['failure_detail']))
                    if(count($r_array['failure_detail'])>0){

                        $error=[
                            'error'=>[
                                'title'         =>PhpException::$title,
                                'code'          =>$r['status']
                            ]
                        ];

                        if(!Faceset::update_faceset_face_len(self::$faceset_ID,$r_array['face_count'])){

                            $error=[
                                'title'     =>PhpException::$title,
                                'info'      =>'Faceset was not update'
                            ];

                            throw new PhpException($error);

                        }

                        $new_face_token_list=[];

                        foreach($r_array['failure_detail'] as $fail_row)
                            switch($fail_row['reason']){

                                case'INVALID_FACE_TOKEN':{

                                    $error['error']['info']='Face token не существует';

                                    break;

                                }

                                case'QUOTA_EXCEEDED':{

                                    $error['error']['info']='Достигнут лимит лиц в профиле';

                                    $new_face_token_list[]=$fail_row['face_token'];

                                    break;

                                }

                                default:{

                                    $error['error']['info']='Неизвестная ошибка';

                                    break;

                                }

                            }

                        if(count($new_face_token_list)>0){

                            foreach(self::$image_list as $image_ID=>$image_row)
                                if(array_search($image_row['face_token'],$new_face_token_list)===false){

                                    self::$image_list[$image_ID]['faceset_ID']      =self::$faceset_ID;
                                    self::$image_list[$image_ID]['faceset_token']   =self::$faceset_token;

                                }

                            self::set_faceset_last();

                            return self::add_face_list($new_face_token_list);

                        }

                        if(isset($r_array['face_count']))
                            if($r_array['face_count']>=FacePlusPlusConfig::$face_token_max_len)
                                self::set_faceset_last();

                        Log::init($error,true,true);

                        if(self::$is_need_error)
                            return ResponseCustom::init($error);

                        file_put_contents(self::$file_log_path,"FAIL added face_token to faceset: ".print_r($error,true)." > ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                        return false;

                    }

                if(!isset($r_array['faceset_token'])){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Faceset is not exists'
                    ];

                    Log::init($error,true,true);

                    if(self::$is_need_error)
                        throw new PhpException($error);

                    file_put_contents(self::$file_log_path,"\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n",FILE_APPEND);

                    sleep(FacePlusPlusConfig::get_random_sleep_timeout());

                    file_put_contents(self::$file_log_path,"RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n",FILE_APPEND);

                    return self::add_face_list($face_token_list);

                }

                for($i=0;$i<$r_array['face_added'];$i++)
                    foreach(self::$image_list as $image_ID=>$image_row)
                        if($face_token_list[$i]==$image_row['face_token']){

                            self::$image_list[$image_ID]['faceset_ID']      =self::$faceset_ID;
                            self::$image_list[$image_ID]['faceset_token']   =self::$faceset_token;

                        }

                if(isset($r_array['face_count'])){

                    if(!Faceset::update_faceset_face_len(self::$faceset_ID,$r_array['face_count'])){

                        $error=[
                            'title'     =>PhpException::$title,
                            'info'      =>'Faceset was not update'
                        ];

                        throw new PhpException($error);

                    }

                    if($r_array['face_count']>=FacePlusPlusConfig::$face_token_max_len)
                        self::set_faceset_last();

                }

                self::$time_used+=(int)$r_array['time_used'];

                return true;

            }

        }

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_image(){

        if(self::$need_update_exists_face_image)
            return self::update_face_image();

        return self::add_face_image();

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_image(){

        if(count(self::$image_list)==0)
            return true;

        foreach(self::$image_list as $row){

            $q=[
                'table' =>FaceImage::$table_name,
                'set'   =>[
                    'user_id'                   =>User::$user_ID,
                    'faceset_id'                =>empty($row['face_token'])?NULL:$row['faceset_ID'],
                    'face_coords'               =>empty($row['coords'])?NULL:Json::encode($row['coords']),
                    'face_count'                =>empty($row['coords'])?0:count($row['coords']),
                    'face_pp_faceset_token'     =>empty($row['faceset_token'])?NULL:$row['faceset_token'],
                    'face_pp_face_token'        =>empty($row['face_token'])?NULL:$row['face_token'],
                    'date_update'               =>'NOW()'
                ],
                'where'=>[
                    'face_id'   =>self::$face_ID,
                    'image_id'  =>$row['image_ID'],
                    'type'      =>0
                ]
            ];

            if(!Db::update($q)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'Face image was not update'
                ];

                throw new DbQueryException($error);

            }

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_image(){

        if(count(self::$image_list)==0)
            return true;

        $value_list=[];

        foreach(self::$image_list as $row)
            $value_list[]=[
                'user_id'                   =>User::$user_ID,
                'face_id'                   =>self::$face_ID,
                'file_id'                   =>$row['file_ID'],
                'faceset_id'                =>empty($row['faceset_ID'])?NULL:$row['faceset_ID'],
                'image_id'                  =>$row['image_ID'],
                'face_coords'               =>empty($row['coords'])?NULL:Json::encode($row['coords']),
                'face_count'                =>empty($row['coords'])?0:count($row['coords']),
                'face_pp_faceset_token'     =>empty($row['faceset_token'])?NULL:$row['faceset_token'],
                'face_pp_face_token'        =>empty($row['face_token'])?NULL:$row['face_token'],
                'date_create'               =>'NOW()',
                'date_update'               =>'NOW()',
                'type'                      =>0
            ];

        if(count($value_list)==0)
            return true;

        $q=[
            'table'     =>FaceImage::$table_name,
            'values'    =>$value_list
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face image was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_data_image_len(){

        if(!FaceData::update_face_data_face_plus_plus_image_len(self::$face_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face image was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        self::$url=FacePlusPlusConfig::get_url_api('add_face');

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'faceset_ID'    =>self::$faceset_ID,
            'time_used'     =>self::$time_used
        ];

        return $data;

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_url();
        self::set_faceset_last();
        self::prepare_image_list();
        self::prepare_face_list();
        self::prepare_face_image();
        self::update_face_data_image_len();

        return self::set_return();

    }

    /**
     * @param int|NULL $face_ID
     * @param array $image_list
     * @param bool $is_need_error
     * @param string|NULL $file_log_path
     * @param bool $need_update_exists_face_image
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $face_ID=NULL,array $image_list=[],bool $is_need_error=true,string $file_log_path=NULL,bool $need_update_exists_face_image=false){

        if(count($image_list)==0)
            return NULL;

        if(empty($face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$face_ID                          =$face_ID;
        self::$image_list                       =$image_list;
        self::$is_need_error                    =$is_need_error;
        self::$file_log_path                    =$file_log_path;
        self::$need_update_exists_face_image    =$need_update_exists_face_image;

        return self::set();

    }

}