<?php

namespace Project\Whore\All\Action\FacePlusPlus;

use Core\Module\Curl\CurlPost;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class GetFacesetListFacePlusPlusAction{

    /** @var array */
    private static $faceset_list=[];

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$faceset_list=[];

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_faceset_list(){

        $url        =FacePlusPlusConfig::get_url_api('get_faceset_list');
        $key_data   =FacePlusPlusConfig::get_key_data();

        if(empty($key_data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Keys is not exists'
            ];

            throw new PhpException($error);

        }

        $data=[
            'api_key'           =>$key_data['api_key'],
            'api_secret'        =>$key_data['api_secret_key'],
        ];

        $r=CurlPost::init($url,[],$data);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Services gave empty response'
            ];

            throw new PhpException($error);

        }

        switch($r['status']){

            case 400:
            case 401:
            case 402:
            case 403:
            case 404:
            case 412:
            case 413:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0)
                    return false;

                $error=[
                    'title'         =>PhpException::$title,
                    'code'          =>$r['status'],
                    'info'          =>$r_array['error_message'],
                    'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used']

                ];

                $error_value_list=mb_split('\:',$r_array['error_message']);

                if(count($error_value_list)>0)
                    switch($error_value_list[0]){

                        case'CONCURRENCY_LIMIT_EXCEEDED':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Слишком частые запросы для бесплатного ключа';
                            else
                                $error['error']['info']='Слишком частые запросы';

                            break;

                        }

                        case'AUTHENTICATION_ERROR':{

                            if($r['status']==401)
                                $error['error']['info']='Проблемы авторизации';
                            else if($r['status']==403)
                                $error['error']['info']='Проблемы доступа к методу API';

                            break;

                        }

                        case'AUTHORIZATION_ERROR':{

                            switch($error_value_list[1]){

                                case'Denied by Client':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано клиентом';

                                    break;

                                }

                                case'Denied by Admin':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано администратором';

                                    break;

                                }

                                case'Insufficient Account Balance':{

                                    $error['error']['info']='Недостаточный остаток на счете';

                                    break;

                                }

                            }

                            throw new PhpException($error);

                            break;

                        }

                        case'MISSING_ARGUMENTS':{

                            $error['error']['info']='Не достаточно переменных';

                            break;

                        }

                        case'BAD_ARGUMENTS':{

                            $error['error']['info']='Плохие переменные';

                            break;

                        }

                        case'COEXISTENCE_ARGUMENTS':{

                            $error['error']['info']='Неверные аргументы';

                            break;

                        }

                        case'REQUEST_ENTITY_TOO_LARGE':
                        case'Request Entity Too Large':{

                            $error['error']['info']='Слишком большой запрос';

                            break;

                        }

                        case'API_NOT_FOUND':{

                            $error['error']['info']='Метод API не найден';

                            break;

                        }

                        case'INTERNAL_ERROR':{

                            $error['error']['info']='Сервис упал';

                            break;

                        }

                        case'IMAGE_ERROR_UNSUPPORTED_FORMAT':{

                            $error['error']['info']='Формат изображения не поддерживается';

                            break;

                        }

                        case'INVALID_IMAGE_SIZE':{

                            $error['error']['info']='Слишком большой размер изображения';

                            break;

                        }

                        case'INVALID_IMAGE_URL':{

                            $error['error']['info']='Изображение не загрузилось';

                            break;

                        }

                        case'IMAGE_FILE_TOO_LARGE':{

                            $error['error']['info']='Слишком большое изображение';

                            break;

                        }

                        case'INSUFFICIENT_PERMISSION':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Переменная не доступна для бесплатного ключа';
                            else
                                $error['error']['info']='Сервис что-то начудил';

                            break;

                        }

                        case'IMAGE_DOWNLOAD_TIMEOUT':{

                            $error['error']['info']='Превышен лимит времени для изображения';

                            break;

                        }

                    }

                throw new PhpException($error);

            }

            case 500:
            case 501:
            case 502:
            case 503:
            case 504:{

                $error=[
                    'title'         =>PhpException::$title,
                    'code'          =>$r['status'],
                    'info'          =>'Проблемы с Face Service(#'.$r['status'].')'
                ];

                throw new PhpException($error);

            }

            default:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0){

                    $error=[
                        'title'         =>PhpException::$title,
                        'code'          =>$r['status'],
                        'info'          =>'Проблемы с Face Service(#'.$r['status'].')'
                    ];

                    throw new PhpException($error);

                }

                if(isset($r_array['error_message'])){

                    $error=[
                        'error'     =>[
                            'title'         =>PhpException::$title,
                            'code'          =>$r['status'],
                            'info'          =>$r_array['error_message'],
                            'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used']
                        ]
                    ];

                    throw new PhpException($error);

                }

                if(isset($r_array['facesets']))
                    foreach($r_array['facesets'] as $row)
                        self::$faceset_list[]=[
                            'faceset_token'     =>$row['faceset_token'],
                            'faceset_ID'        =>empty($row['outer_id'])?NULL:$row['outer_id'],
                            'name'              =>empty($row['display_name'])?NULL:$row['display_name'],
                            'tags'              =>empty($row['tags'])?NULL:$row['tags'],
                        ];

                return true;

            }

        }

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'faceset_list'=>self::$faceset_list
        ];

        return $data;

    }

    /**
     * @return array
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_faceset_list();

        return self::set_return();

    }

    /**
     * @return array
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::reset_data();

        return self::set();

    }

}