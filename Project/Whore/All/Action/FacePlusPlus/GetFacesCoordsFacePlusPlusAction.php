<?php

namespace Project\Whore\All\Action\FacePlusPlus;

use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Image\Image;
use Core\Module\Image\ImageConvert;
use Core\Module\Image\ImageFace;
use Core\Module\Image\ImageFaceAttribute;
use Core\Module\Json\Json;
use Core\Module\Log\Log;
use Core\Module\OsServer\OsServer;
use Core\Module\Response\ResponseCustom;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class GetFacesCoordsFacePlusPlusAction{

    /** @var string */
    private static $url;

    /** @var int */
    private static $image_ID;

    /** @var string */
    private static $request_ID;

    /** @var string */
    private static $face_plus_plus_image_ID;

    /** @var int */
    private static $time_used;

    /** @var string */
    private static $image_path;

    /** @var int */
    private static $face_len                =0;

    /** @var array */
    private static $face_list               =[];

    /** @var int  */
    private static $memory_usage            =0;

    /** @var int */
    private static $file_log_size_max       =10485760;

    /** @var bool */
    private static $is_need_error           =true;

    /** @var string */
    private static $file_log_path;
    
    /** @var string */
    private static $cookie_path;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$url              =NULL;
        self::$image_ID         =NULL;
        self::$image_path       =NULL;
        self::$is_need_error    =true;
        self::$file_log_path    =NULL;
        self::$cookie_path      =NULL;
        self::$face_len         =0;
        self::$face_list        =[];

        return true;

    }

    /**
     * @param $data
     * @return bool|int
     */
    private static function add_to_log($data){

        $memory         =round((memory_get_usage()/(1024*1024)),3);
        $memory_peak    =round((memory_get_peak_usage()/(1024*1024)),3);

        if($memory>self::$memory_usage){

            $old_memory             =self::$memory_usage;
            self::$memory_usage     =$memory;

            self::add_to_log('############## -> Memory usage update from '.$old_memory.' to '.self::$memory_usage.'/'.$memory_peak.'mb'."\n");

        }

        echo print_r($data,true);

        if(file_exists(self::$file_log_path))
            if(filesize(self::$file_log_path)>self::$file_log_size_max)
                self::set_log_path();

        if(!empty($_POST['need_log']))
            return file_put_contents(self::$file_log_path,$data,FILE_APPEND);

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_ID(){

        if(!empty(self::$image_path))
            return true;

        if(!Image::isset_image_ID(self::$image_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_face_list(){

        $key_data=FacePlusPlusConfig::get_key_data();

        if(empty($key_data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Keys is not exists'
            ];

            throw new PhpException($error);

        }

        $data=[
            'api_key'           =>$key_data['api_key'],
            'api_secret'        =>$key_data['api_secret_key'],
            'return_landmark'   =>FacePlusPlusConfig::$face_landmark,
            'image_base64'      =>ImageConvert::get_base64_encode_image(self::$image_path)
        ];

        if(!OsServer::$is_windows)
            $data['calculate_all']=FacePlusPlusConfig::$face_calculate_all;

        $r=CurlPost::init(self::$url,[],$data);

        if(count($r)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Services gae empty response'
            ];

            Log::init($error,true,true);

            if(self::$is_need_error)
                throw new PhpException($error);

            self::add_to_log("\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            self::add_to_log("RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n");

            return self::get_face_list();

        }

        switch($r['status']){

            case 400:
            case 401:
            case 402:
            case 403:
            case 404:
            case 412:
            case 413:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0)
                    return false;

                $error=[
                    'error'     =>[
                        'title'         =>PhpException::$title,
                        'code'          =>$r['status'],
                        'info'          =>$r_array['error_message'],
                        'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used']
                    ]
                ];

                $error_value_list=mb_split('\:',$r_array['error_message']);

                if(count($error_value_list)>0)
                    switch($error_value_list[0]){

                        case'CONCURRENCY_LIMIT_EXCEEDED':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Слишком частые запросы для бесплатного ключа';
                            else
                                $error['error']['info']='Слишком частые запросы';

                            break;

                        }

                        case'AUTHENTICATION_ERROR':{

                            if($r['status']==401)
                                $error['error']['info']='Проблемы авторизации';
                            else if($r['status']==403)
                                $error['error']['info']='Проблемы доступа к методу API';

                            break;

                        }

                        case'AUTHORIZATION_ERROR':{

                            switch(trim($error_value_list[1])){

                                case'Denied by Client':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано клиентом';

                                    break;

                                }

                                case'Denied by Admin':{

                                    $error['error']['info']='Нет разрешения для вызова API: Отказано администратором';

                                    break;

                                }

                                case'Insufficient Account Balance':{

                                    $error['error']['info']='Недостаточный остаток на счете';

                                    break;

                                }

                            }

                            throw new PhpException($error['error']);

                            break;

                        }

                        case'MISSING_ARGUMENTS':{

                            $error['error']['info']='Не достаточно переменных';

                            break;

                        }

                        case'BAD_ARGUMENTS':{

                            $error['error']['info']='Плохие переменные';

                            break;

                        }

                        case'COEXISTENCE_ARGUMENTS':{

                            $error['error']['info']='Неверные аргументы';

                            break;

                        }

                        case'REQUEST_ENTITY_TOO_LARGE':
                        case'Request Entity Too Large':{

                            $error['error']['info']='Слишком большой запрос';

                            break;

                        }

                        case'API_NOT_FOUND':{

                            $error['error']['info']='Метод API не найден';

                            break;

                        }

                        case'INTERNAL_ERROR':{

                            $error['error']['info']='Сервис упал';

                            break;

                        }

                        case'IMAGE_ERROR_UNSUPPORTED_FORMAT':{

                            $error['error']['info']='Формат изображения не поддерживается';

                            break;

                        }

                        case'INVALID_IMAGE_SIZE':{

                            $error['error']['info']='Слишком большой размер изображения';

                            break;

                        }

                        case'INVALID_IMAGE_URL':{

                            $error['error']['info']='Изображение не загрузилось';

                            break;

                        }

                        case'IMAGE_FILE_TOO_LARGE':{

                            $error['error']['info']='Слишком большое изображение';

                            break;

                        }

                        case'INSUFFICIENT_PERMISSION':{

                            if(FacePlusPlusConfig::$is_free)
                                $error['error']['info']='Переменная не доступна для бесплатного ключа';
                            else
                                $error['error']['info']='Сервис что-то начудил';

                            break;

                        }

                        case'IMAGE_DOWNLOAD_TIMEOUT':{

                            $error['error']['info']='Превышен лимит времени для изображения';

                            break;

                        }

                    }

                Log::init($error,true,true);

                if(count($error_value_list)>0)
                    switch($error_value_list[0]){

                        case'IMAGE_ERROR_UNSUPPORTED_FORMAT':
                        case'INVALID_IMAGE_SIZE':
                        case'INVALID_IMAGE_URL':
                        case'IMAGE_FILE_TOO_LARGE':
                            return false;

                    }

                if(self::$is_need_error)
                    return ResponseCustom::init($error);

                self::add_to_log("\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n");

                sleep(FacePlusPlusConfig::get_random_sleep_timeout());

                self::add_to_log("RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n");

                return self::get_face_list();

            }

            case 500:
            case 501:
            case 502:
            case 503:
            case 504:{

                $error=[
                    'error'     =>[
                        'title'         =>PhpException::$title,
                        'code'          =>$r['status'],
                        'info'          =>'Проблемы с Face Service(#'.$r['status'].')'
                    ]
                ];

                Log::init($error,true,true);

                if(self::$is_need_error)
                    return ResponseCustom::init($error);

                self::add_to_log("\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n");

                sleep(FacePlusPlusConfig::get_random_sleep_timeout());

                self::add_to_log("RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n");

                return self::get_face_list();

            }

            default:{

                $r_array=Json::decode($r['data']);

                if(count($r_array)==0)
                    return false;

                if(isset($r_array['error_message'])){

                    $error=[
                        'error'     =>[
                            'title'         =>PhpException::$title,
                            'code'          =>$r['status'],
                            'info'          =>$r_array['error_message'],
                            'time_used'     =>empty($r_array['time_used'])?NULL:(int)$r_array['time_used']
                        ]
                    ];

                    Log::init($error,true,true);

                    if(self::$is_need_error)
                        return ResponseCustom::init($error);

                }

                if(!isset($r_array['faces'])){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Faces is not exists'
                    ];

                    Log::init($error,true,true);

                    if(self::$is_need_error)
                        throw new PhpException($error);

                    self::add_to_log("\n=====================\nRESTARTING search faces: ".Date::get_date_time_full()."\n");

                    sleep(FacePlusPlusConfig::get_random_sleep_timeout());

                    self::add_to_log("RESTARTED search faces: ".Date::get_date_time_full()."\n=====================\n");

                    return self::get_face_list();

                }

                self::$face_len=count($r_array['faces']);

                if(self::$face_len==0)
                    return false;

                self::$request_ID                   =empty($r_array['request_id'])?NULL:$r_array['request_id'];
                self::$face_plus_plus_image_ID      =empty($r_array['image_id'])?NULL:$r_array['image_id'];
                self::$time_used                    =empty($r_array['time_used'])?NULL:$r_array['time_used'];

                foreach($r_array['faces'] as $row){

                    $face_token         =empty($row['face_token'])?NULL:$row['face_token'];
                    $face_rectangle     =empty($row['face_rectangle'])?NULL:$row['face_rectangle'];
                    $landmark           =empty($row['landmark'])?NULL:$row['landmark'];

                    $image_face_ID=ImageFace::add_image_face(self::$image_ID,self::$request_ID,self::$face_plus_plus_image_ID,$face_token,$face_rectangle,$landmark);

                    if(empty($image_face_ID)){

                        $error=[
                            'title'     =>PhpException::$title,
                            'info'      =>'Image face was nto add'
                        ];

                        throw new PhpException($error);

                    }

                    $image_face_attribute_ID_list=[];

                    if(isset($row['attributes']))
                        if(count($row['attributes'])!=0)
                            $image_face_attribute_ID_list=ImageFaceAttribute::add_image_face_attribute(self::$image_ID,$image_face_ID,$row['attributes']);

                    self::$face_list[]=[
                        'image_face_ID'             =>$image_face_ID,
                        'image_face_attribute'      =>$image_face_attribute_ID_list,
                        'face_token'                =>empty($row['face_token'])?NULL:$row['face_token'],
                        'landmark'                  =>empty($row['landmark'])?NULL:$row['landmark'],
                        'attributes'                =>empty($row['attributes'])?NULL:$row['attributes'],
                        'coords'                    =>empty($row['face_rectangle'])?NULL:$row['face_rectangle']
                    ];

                }

                return true;

            }

        }

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_path(){

        if(!empty(self::$image_path))
            return true;

        $r=ImagePathAction::get_image_path_data(self::$image_ID,true,true,true);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Image ID is not exists'
            ];

            throw new PhpException($error);

        }

//        self::$image_path=$r['image_dir'].'/'.$r['image_item_ID_list']['large']['ID'];
        self::$image_path=$r['image_dir'].'/'.$r['image_item_ID_list']['middle']['ID'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        self::$url=FacePlusPlusConfig::get_url_api('detect_faces');

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'time_used'     =>self::$time_used,
            'face_len'      =>self::$face_len,
            'face_list'     =>self::$face_list
        ];

        return $data;

    }

    /**
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_image_ID()){

            self::set_image_path();
            self::set_url();
            self::get_face_list();

            return self::set_return();

        }

        return NULL;

    }

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $image_path
     * @param bool $is_need_error
     * @param string|NULL $file_log_path
     * @param string|NULL $cookie_path
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $image_ID=NULL,string $image_path=NULL,bool $is_need_error=true,string $file_log_path=NULL,string $cookie_path=NULL){

        if(empty($image_ID)&&empty($image_path)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Image ID and image path are empty'
            ];

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$image_ID         =empty($image_ID)?NULL:$image_ID;
        self::$image_path       =empty($image_path)?NULL:$image_path;
        self::$is_need_error    =$is_need_error;
        self::$file_log_path    =$file_log_path;
        self::$cookie_path      =$cookie_path;

        return self::set();

    }

}