<?php

namespace Project\Whore\All\Action\FacePlusPlus;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Project\Whore\All\Module\Face\Faceset;

class SyncFacesetFacePlusPlusAction{

    /** @var array */
    private static $faceset_ID_list     =[];

    /** @var array */
    private static $faceset_list        =[];

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_faceset_list(){

        $r=GetFacesetListFacePlusPlusAction::init();

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Cannot get faceset list from Face services'
            ];

            throw new PhpException($error);

        }

        self::$faceset_list=$r['faceset_list'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_faceset_details(){

        if(count(self::$faceset_list)==0)
            return true;

        foreach(self::$faceset_list as $index=>$row){

            $r=GetFacesetDataFacePlusPlusAction::init($row['faceset_token']);

            if(empty($r)){

                $error=[
                    'title'     =>PhpException::$title,
                    'info'      =>'Cannot get faceset list from Face services'
                ];

                throw new PhpException($error);

            }

            self::$faceset_ID_list[]                    =$r['faceset_ID'];

            self::$faceset_list[$index]['faceset_ID']   =$r['faceset_ID'];
            self::$faceset_list[$index]['face_len']     =$r['face_len'];

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_faceset(){

        if(count(self::$faceset_list)==0)
            return true;

        foreach(self::$faceset_list as $row)
            if(!Faceset::update_faceset_face_len($row['faceset_ID'],$row['face_len'])){

                $error=[
                    'title'     =>PhpException::$title,
                    'info'      =>'Faceset face len was not update'
                ];

                throw new PhpException($error);

            }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_faceset(){

        if(!Faceset::remove_faceset_except_faceset_ID_list(self::$faceset_ID_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Faceset face len was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'faceset_list'=>self::$faceset_list
        ];

        return $data;

    }

    /**
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_faceset_list();
        self::set_faceset_details();
        self::update_faceset();
        self::remove_faceset();

        return self::set_return();

    }

    /**
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}