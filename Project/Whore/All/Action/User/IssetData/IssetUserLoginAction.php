<?php

namespace Project\Whore\All\Action\User\IssetData;

use Core\Module\Exception\ParametersException;
use Core\Module\User\UserHash;
use Core\Module\User\UserLogin;

class IssetUserLoginAction{

    /** @var int */
    private static $user_ID;

    /** @var string */
    private static $user_login;

    /** @var string */
    private static $hash;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_ID(){

        if(empty(self::$hash))
            return true;

        self::$user_ID=UserHash::get_user_ID(self::$hash);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_user_ID();

        return UserLogin::isset_user_login_in_active(self::$user_login,self::$user_ID);

    }

    /**
     * @param string|NULL $user_login
     * @param string|NULL $hash
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $user_login=NULL,string $hash=NULL){

        if(empty($user_login)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User login is empty'
            );

            throw new ParametersException($error);

        }

        self::$user_login   =$user_login;
        self::$hash         =$hash;

        return self::set();

    }

}