<?php

namespace Project\Whore\All\Action\User\IssetData;

use Core\Module\Exception\ParametersException;
use Core\Module\User\UserEmail;
use Core\Module\User\UserHash;

class IssetUserEmailAction{

    /** @var int */
    private static $user_ID;

    /** @var string */
    private static $user_email;

    /** @var string */
    private static $hash;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_ID(){

        if(empty(self::$hash))
            return true;

        self::$user_ID=UserHash::get_user_ID(self::$hash);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_user_ID();
        
//        return UserEmail::isset_user_email_in_active_list(self::$user_email,self::$user_ID);
        return UserEmail::isset_user_email_in_all_list(self::$user_email,self::$user_ID);

    }

    /**
     * @param string|NULL $user_email
     * @param string|NULL $hash
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $user_email=NULL,string $hash=NULL){

        if(empty($user_email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User email is empty'
            );

            throw new ParametersException($error);

        }

        self::$user_email   =$user_email;
        self::$hash         =$hash;

        return self::set();

    }

}