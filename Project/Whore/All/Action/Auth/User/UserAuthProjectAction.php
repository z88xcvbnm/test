<?php

namespace Project\Whore\All\Action\Auth\User;

use Core\Action\Auth\UserAuth\UserAuthSystemAction;
use Core\Module\Email\EmailValidation;
use Core\Module\Exception\ParametersException;

class UserAuthProjectAction{

    /** @var string */
    private static $login;

    /** @var string */
    private static $password;

    /** @var bool */
    private static $success;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     */
    private static function set_auth(){

        self::$success=UserAuthSystemAction::init(self::$login,self::$password);

        return true;

    }

//    /**
//     * @return bool
//     * @throws ParametersException
//     * @throws \Core\Module\Exception\AccessDeniedException
//     * @throws \Core\Module\Exception\DbParametersException
//     * @throws \Core\Module\Exception\DbQueryException
//     * @throws \Core\Module\Exception\DbQueryParametersException
//     * @throws \Core\Module\Exception\DbValidationValueException
//     * @throws \Core\Module\Exception\ParametersValidationException
//     * @throws \Core\Module\Exception\PathException
//     * @throws \Core\Module\Exception\PhpException
//     * @throws \Core\Module\Exception\SystemException
//     * @throws \Core\Module\Exception\UnknownException
//     */
//    private static function set_auth(){
//
//        self::$success=UserAuthSystemAction::init(self::$login,self::$password);
//
//        return true;
//
//    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'success'=>self::$success
        ];

        return $data;

    }

    /**
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     */
    private static function set(){

        self::set_auth();

        return self::set_return();

    }

    /**
     * @param string|NULL $login
     * @param string|NULL $password
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     */
    public  static function init(string $login=NULL,string $password=NULL){

        $error_info_list=[];

        if(empty($login))
            $error_info_list[]='Login is empty';

        if(empty($password))
            $error_info_list[]='Password is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$login        =$login;
        self::$password     =$password;

        return self::set();

    }

}
