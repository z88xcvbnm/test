<?php

namespace Project\Whore\Admin\File\Image;

use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PathException;
use Core\Module\Image\ImageItem;
use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Face\FaceSearchItem;

class ImageContentDownload{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $image_ID;

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $image_item_ID;

    /** @var string */
    private static $file_extension;

    /** @var string */
    private static $image_resolution;

    /** @var string */
    private static $date_create;

    /** @var string */
    private static $file_path;

    /** @var string */
    private static $hash;

    /**
     * @param string|NULL $data
     * @return mixed
     */
    private static function get_hash(string $data=NULL){

        $list=mb_split('\.',$data);

        return $list[0];

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_data(){

        $r=ImageItem::get_image_item_data_from_hash_link(self::$hash);

        if(empty($r)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Hash is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        self::$user_ID              =$r['user_ID'];
        self::$image_ID             =$r['image_ID'];
        self::$image_item_ID        =$r['ID'];
        self::$file_extension       =$r['file_extension'];
        self::$image_resolution     =$r['resolution_type'];
        self::$date_create          =$r['date_create'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_ID_for_image(){

        self::$face_ID=FaceImage::get_face_ID(self::$image_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_ID_in_face_search(){

        if(empty(self::$face_ID))
            return false;

        return FaceSearchItem::isset_face_ID_for_user_ID(self::$face_ID,User::$user_ID);

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_access(){

        if(
              UserAccess::$is_root
            ||UserAccess::$is_admin
        )
            return true;
        else if(
              UserAccess::$is_profile
            ||UserAccess::$is_profile_wallet
        ){

            if(
                  User::$user_ID==self::$user_ID
                ||is_null(self::$user_ID)
            )
                return true;
            else{

                self::set_face_ID_for_image();

                if(!empty(self::$face_ID))
                    if(self::isset_face_ID_in_face_search())
                        return true;

            }

        }

        if(self::$image_resolution=='preview')
            return true;

        $error=[
            'title'     =>AccessDeniedException::$title,
            'info'      =>'Access denied'
        ];

        throw new AccessDeniedException($error);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_file_path(){

        self::$file_path=Dir::get_dir_from_date(DirConfig::$dir_image,self::$date_create).'/'.self::$image_ID.'/'.self::$image_item_ID.'.'.self::$file_extension;

        if(!file_exists(self::$file_path)){

            $error=[
                'title'     =>PathException::$title,
                'info'      =>'File is not exists'
            ];

            throw new PathException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_image(){

        switch(Url::$list[0]){

            case'download':{

                header("X-Accel-Redirect: /".self::$file_path);
                header('Content-Type: application/octet-stream');
//                header('Content-Type: application/x-force-download');
                header("Cache-Control: no-cache, must-revalidate");
                header('Content-Disposition: attachment; filename='.basename('image_'.self::$hash.'.'.self::$file_extension));

                readfile(self::$file_path);

                return true;

            }

            case'show':{

                $file_info=getimagesize(self::$file_path);

                header('Content-type: '.$file_info['mime']);
                header("Expires: ".gmdate("D, d M Y H:i:s",time()+60*60*24)." GMT");
                header("Cache-Control: public");

                readfile(self::$file_path);

                return true;

            }

            default:{

                $error=[
                    'title'     =>ParametersException::$title,
                    'info'      =>'URL action is not valid'
                ];

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_image_data();

        if(self::check_access()){

            self::set_file_path();

            return self::prepare_image();

        }

        return false;

    }

    /**
     * @param string|NULL $hash
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $hash=NULL){

        if(empty($hash)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is empty'
            ];

            throw new ParametersException($error);

        }

        self::$hash=self::get_hash($hash);

        return self::set();

    }

}