<?php

namespace Project\Whore\Admin\File\Face;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Url\Url;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\FaceSearchPdf;
use Project\Whore\All\Module\Face\FaceSearchPdfDownload;

class FindFacePdfDownload{

    /** @var int */
    private static $face_search_pdf_ID;

    /** @var int */
    private static $face_search_pdf_download_ID;

    /** @var string */
    private static $file_path;

    /** @var string */
    private static $hash;

    /**
     * @param string|NULL $data
     * @return mixed
     */
    private static function get_hash(string $data=NULL){

        $list=mb_split('\.',$data);

        return $list[0];

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_face_search_pdf_ID(){

        self::$face_search_pdf_ID=FaceSearchPdf::get_face_search_pdf_ID_from_token(self::$hash);

        if(empty(self::$face_search_pdf_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Hash is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_file_path(){

        self::$file_path=DirConfigProject::$dir_pdf.'/'.self::$face_search_pdf_ID.'.pdf';

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_search_pdf_download(){

        $is_show        =Url::$list[0]=='show';
        $is_download    =Url::$list[0]=='download';

        self::$face_search_pdf_download_ID=FaceSearchPdfDownload::add_face_search_pdf_download(self::$face_search_pdf_ID,$is_show,$is_download);

        if(empty(self::$face_search_pdf_download_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face search PDF download was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_search_pdf_download(){

        if(!FaceSearchPdfDownload::update_face_search_pdf_download_to_downloaded(self::$face_search_pdf_download_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face search PDF download was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_pdf(){

//        echo self::$file_path;exit;
//        NGINX
//        header("X-Accel-Redirect: /".self::$file_path);
//        header('Content-Type: application/octet-stream');
//        header("Cache-Control: no-cache, must-revalidate");
//        header('Content-Disposition: attachment; filename='.basename('find_face_'.self::$hash.'.pdf'));

//        Apache
//        header('X-SendFile: ' . realpath($file));
//        header('Content-Type: application/octet-stream');
//        header('Content-Disposition: attachment; filename=' . basename($file));
//        exit;

        self::add_face_search_pdf_download();

        switch(Url::$list[0]){

            case'download':{

                header("X-Accel-Redirect: /".self::$file_path);
                header('Content-Type: application/octet-stream');
                header("Cache-Control: no-cache, must-revalidate");
                header('Content-Disposition: attachment; filename='.basename('find_face_'.self::$hash.'.pdf'));

                readfile(self::$file_path);

                return self::update_face_search_pdf_download();

            }

            case'show':{

                $file_info=getimagesize(self::$file_path);

                header('Content-type: '.$file_info['mime']);
                header("Cache-Control: no-cache, must-revalidate");

                readfile(self::$file_path);

                return self::update_face_search_pdf_download();

            }

            default:{

                $error=[
                    'title'     =>ParametersException::$title,
                    'info'      =>'URL action is not valid'
                ];

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::get_face_search_pdf_ID();
        self::set_file_path();

        return self::prepare_pdf();

    }

    /**
     * @param string|NULL $hash
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $hash=NULL){

        if(empty($hash)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is empty'
            ];

            throw new ParametersException($error);

        }

        self::$hash=self::get_hash($hash);

        return self::set();

    }

}