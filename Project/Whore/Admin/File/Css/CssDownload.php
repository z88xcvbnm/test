<?php

namespace Project\Whore\Admin\File\Css;

use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Url\Url;
use Core\Module\User\UserAccess;
use Project\Whore\Admin\Module\Dir\DirConfigAdminProject;

class CssDownload{

    /** @var array */
    private static $link_list=[];

    /** @var string */
    private static $file_path;

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_access(){

        switch(self::$link_list[2]){

            case'admin':{

                if(
                      UserAccess::$is_root
                    ||UserAccess::$is_admin
                )
                    return true;

                break;

            }

            case'profile':{

                if(
                      UserAccess::$is_profile
                    ||UserAccess::$is_profile_wallet
                )
                    return true;

                break;

            }

            case'system':
                return true;

        }

        $error=[
            'title'     =>AccessDeniedException::$title,
            'info'      =>'Access denied for CSS file'
        ];

        throw new AccessDeniedException($error);

    }

    /**
     * @return bool
     */
    private static function set_link_list(){

        self::$link_list=Url::$list;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_file_path(){

        self::$file_path=DirConfigAdminProject::$dir_css.'/'.ucfirst(self::$link_list[2]);

        for($i=3;$i<count(self::$link_list)-1;$i++)
            self::$file_path.='/'.ucfirst(self::$link_list[$i]);

        self::$file_path.='/'.end(self::$link_list);

        if(!file_exists(self::$file_path)){

            $error=[
                'title'     =>PathException::$title,
                'info'      =>'File is not exists'
            ];

            throw new PathException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_image(){

        switch(Url::$list[0]){

            case'show':{

                $file_info=getimagesize(self::$file_path);

                header('Content-type: '.$file_info['mime']);
                header("Expires: ".gmdate("D, d M Y H:i:s",time()+60*60*24)." GMT");
                header("Cache-Control: public");

                readfile(self::$file_path);

                return true;

            }

            default:{

                $error=[
                    'title'     =>ParametersException::$title,
                    'info'      =>'URL action is not valid'
                ];

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_link_list();

        if(self::check_access()){

            self::set_file_path();

            return self::prepare_image();

        }

        return false;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}