<?php

namespace Project\Whore\Admin\Template\Dom\Pdf;

use Project\Whore\Admin\Content\ContentAdminWhore;use Project\Whore\Admin\Route\Page\RouteRootAdminWhorePage;

class PdfFaceDownloadWhoreDom{

    /**
     * @return bool
     */
    public  static function init(){

        return true;

    }

}

?>

<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><?php echo ContentAdminWhore::get_project_name(); ?></title>

</head>

<body>

    <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; color: #222222; font-family: Arial, sans-serif">

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; padding: 0 0 5px 0; border-bottom: 1px solid #5585bf; margin: 0 0 20px 0;">
            <span style="font-size: 30px; color: #e9c2c1">Shluham.NET:</span>
            <span style="font-size: 22px; color: #17365d;"> результат поиска по лицам</span>
        </div>

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden;">

            <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; font-size: 20px; color: #376092">

                <div style="position: relative; top: 0; left: 0; width: 50%; float: left; overflow: hidden; text-align: center">Исходное изображение</div>

                <div style="position: relative; top: 0; left: 0; width: 50%; float: left; overflow: hidden; text-align: center">Обнаруженное совпадение (97.14%) </div>

            </div>

            <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden;">

                <div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">

                    <img src="/Resource/Image/2019/02/12/46820/187157.jpg" width="100%" />

                </div>

                <div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">

                    <img src="/Resource/Image/2019/02/12/46833/187209.jpg" width="100%" />

                </div>

            </div>

        </div>

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; color: #17365d; font-size: 25px; padding: 0 0 5px 0; border-bottom: 1px solid #5585bf; margin: 0 0 10px 0">Анкетные данные</div>

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; font-size: 14px; font-style: italic; color: #8c8c8c; margin: 0 0 10px 0;">

            <div style="position: relative; top: 0; left: 0; width: 49%; float: left; padding: 0 1% 0 0; overflow: hidden;">Основная информация:</div>

            <div style="position: relative; top: 0; left: 0; width: 49%; float: left; padding: 0 0 0 1%; overflow: hidden;">Дополнительная информация:</div>

        </div>

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; font-size: 14px;">

            <div style="position: relative; top: 0; left: 0; width: 48%; float: left; overflow: hidden; text-align: center; padding: 0 1% 0 1%">

                <table width="100%" cellpadding="1" cellspacing="0" border="0">

                    <tr>
                        <td width="30%" align="left">Имя:</td>
                        <td width="70%" align="left" style="font-weight: bold">Настя (Петросянова)</td>
                    </tr>

                    <tr>
                        <td width="30%" align="left">Возраст:</td>
                        <td width="70%" align="left" style="font-weight: bold">21</td>
                    </tr>

                    <tr>
                        <td width="30%" align="left">Город:</td>
                        <td width="70%" align="left" style="font-weight: bold">Москва, Россия</td>
                    </tr>

                    <tr>
                        <td width="30%" align="left">Источник:</td>
                        <td width="70%" align="left" style="font-weight: bold">https://source_link.com/profile/1012131230</td>
                    </tr>

                </table>

            </div>

            <div style="position: relative; top: 0; left: 0; width: 48%; float: left; overflow: hidden; text-align: center; padding: 0 1% 0 1%">

                <table width="100%" cellpadding="1" cellspacing="0" border="0">

                    <tr>
                        <td width="30%" align="left">Рост:</td>
                        <td width="70%" align="left" style="font-weight: bold">172</td>
                    </tr>

                    <tr>
                        <td width="30%" align="left">Вес:</td>
                        <td width="70%" align="left" style="font-weight: bold">45</td>
                    </tr>

                    <tr>
                        <td width="30%" align="left">Бюст:</td>
                        <td width="70%" align="left" style="font-weight: bold">3</td>
                    </tr>

                    <tr>
                        <td width="30%" align="left">Параметры:</td>
                        <td width="70%" align="left" style="font-weight: bold">90-60-90</td>
                    </tr>

                    <tr>
                        <td width="30%" align="left">Тату:</td>
                        <td width="70%" align="left" style="font-weight: bold">Нет</td>
                    </tr>

                    <tr>
                        <td width="30%" align="left">О себе:</td>
                        <td width="70%" align="left" style="font-weight: bold">КМС по гимнастике и бла-бла и что-то там еще...</td>
                    </tr>

                </table>

            </div>

        </div>

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; font-size: 14px; font-style: italic; color: #8c8c8c; margin: 0 0 10px 0;">Социальные сети:</div>

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; font-size: 14px; font-style: italic; color: #338dcd; margin: 0 0 10px 0;">

            <div style="position: relative; background: url(/Project/Whore/Admin/Template/Images/Social/facebook.png) 0 0 no-repeat; height: 40px; top: 0; left: 0; float: left; margin: 0 20px 0 0; padding: 10px 0 0 50px; overflow: hidden;">nastia_huastia</div>

            <div style="position: relative; background: url(/Project/Whore/Admin/Template/Images/Social/vk.png) 0 0 no-repeat; height: 40px; top: 0; left: 0; float: left; overflow: hidden; margin: 0 20px 0 0; padding: 10px 0 0 50px;">id12312313</div>

            <div style="position: relative; background: url(/Project/Whore/Admin/Template/Images/Social/instagram.png) 0 0 no-repeat; height: 40px; top: 0; left: 0; float: left; overflow: hidden; margin: 0 20px 0 0; padding: 10px 0 0 50px;">nastia_petrosian</div>

        </div>

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; color: #17365d; font-size: 25px; padding: 0 0 5px 0; border-bottom: 1px solid #5585bf; margin: 0 0 10px 0">Дополнительные фотографии</div>

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden;">

            <div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">

                <img src="/Resource/Image/2019/02/12/46820/187157.jpg" width="100%" />

            </div>

            <div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">

                <img src="/Resource/Image/2019/02/12/46833/187209.jpg" width="100%" />

            </div>

        </div>

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden;">

            <div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">

                <img src="/Resource/Image/2019/02/12/46820/187157.jpg" width="100%" />

            </div>

            <div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">

                <img src="/Resource/Image/2019/02/12/46833/187209.jpg" width="100%" />

            </div>

        </div>

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden;">

            <div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">

                <img src="/Resource/Image/2019/02/12/46820/187157.jpg" width="100%" />

            </div>

            <div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">

                <img src="/Resource/Image/2019/02/12/46833/187209.jpg" width="100%" />

            </div>

        </div>

        <div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden;">

            <div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">

                <img src="/Resource/Image/2019/02/12/46820/187157.jpg" width="100%" />

            </div>

            <div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">

                <img src="/Resource/Image/2019/02/12/46833/187209.jpg" width="100%" />

            </div>

        </div>

    </div>

</body>

</html>


