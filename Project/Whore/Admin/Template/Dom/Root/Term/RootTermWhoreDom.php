<?php

namespace Project\Whore\Admin\Template\Dom\Root\Term;

use Core\Module\Device\DeviceType;
use Project\Whore\Admin\Content\ContentAdminWhore;

class RootTermWhoreDom{

    /**
     * @return bool
     */
    public  static function init(){

        return true;

    }

}

?>

<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><?php echo ContentAdminWhore::get_page_title('root_term_page_title'); ?></title>

    <meta name="description" content="<?php echo ContentAdminWhore::get_content('root_description'); ?>" />

    <link rel="apple-touch-icon" sizes="57x57" href="/Resource/Favicon/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="/Resource/Favicon/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes=" 72x72" href="/Resource/Favicon/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/Resource/Favicon/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes=" 114x114" href="/Resource/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/Resource/Favicon/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/Resource/Favicon/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes=" 152x152" href="/Resource/Favicon/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/Resource/Favicon/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="/Resource/Favicon/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/Resource/Favicon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="/Resource/Favicon/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/Resource/Favicon/favicon-16x16.png" />
    <link rel=" manifest" href="/Resource/Favicon/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/Resource/Favicon/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

<?php

    DeviceType::is_mobile();

    if(DeviceType::$is_mobile)
        echo'<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1">';
//    else
//        echo'<meta name="viewport" content="width=1480, user-scalable=yes">';

?>

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,700' rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/show/css/system/default.css?t=<?php echo time();?>" />
<!--    <link rel="stylesheet" type="text/css" href="/show/css/style.css?t=--><?php //echo time();?><!--" />-->
<!--    <link rel="stylesheet" type="text/css" href="/show/css/style_new.css?t=--><?php //echo time();?><!--" />-->

    <script type="text/javascript" src="/show/js/jquery/jquery-3.1.1.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/jquery/jquery.extendext.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/system/config.js?t=<?php echo time();?>"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143269925-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-143269925-1');
    </script>

</head>

<body>

    <div id="all">

        <div id="header">
            <div id="logo"></div>
        </div>

        <div id="root_content" style="opacity: 0">

            <div id="root_face_preview_list"></div>

            <?php if(!DeviceType::$is_mobile):?>

            <div id="root_face_block">

                <div id="root_default_content">

                    <h1>Термины и определения в эскортном бизнесе</h1>
                    <p><b>Эскорт</b> - услуга сопровождения клиента на встречах, деловых поездках и на отдыхе. В подавляющем большинстве случаев эскорт-услуги являются скрытой проституцией. Предоставление интим-услуг оговаривается сторонами явно и заранее.</p>
                    <p><b>Тема</b> - это единица измерения эскорт-услуг. По сути, это поездка к клиенту: на вечеринку, на дачу или в отель. Тема может длиться от нескольких часов до нескольких дней. Перед темой обязательно инструктируют, то есть рассказывают, кто будет на этой тусовке, как себя вести, что можно говорить, а что нельзя, с кем нужно спать.</p>
                    <p><b>Тур</b> - это поездка за границу на несколько дней, иногда недель, когда девочка каждый день работает с разными клиентами, о встречах с которыми ее менеджер договорился заранее. Клиентов может быть от нескольких за поездку, до нескольких за день. Зачастую туры связаны с групповым сексом. Наиболее популярные направления туров - ОАЭ, Турция, Италия, Лондон, Париж.</p>
                    <p><b>Содержание</b> - форма эскорта, при которой девочка приоритетно работает на одного клиента, который платит ей заранее оговоренную сумму вознаграждения ежемесячно или еженедельно. Конечно, содержание не исключает поездок на темы полностью, но по первому звонку клиента девушка должна приехать к нему.</p>
                    <p><b>Шлюхомиграция</b> - это последовательный переезд девушки с пониженнной социальной ответственностью во все более крупные города, продиктованный двумя основными факторами: испорченной репутацией в текущем городе проживания и погоней за более высокими гонорарами. Конечным пунктом миграции почти всех проституток является Москва, где высокий спрос на мохнатое золото, продиктованный высокой концентрацией капитала и есть возможность раствориться в большой массе людей.</p>
                    <p><b>Псевдонимы</b> - это выдуманные имена, которые эскортницы могут применять в своей работе. Чем ниже уровень проститутки и ее клиентов, тем вероятнее она будет использовать псевдоним. Дорогие эскортницы чаще всего работают под настоящими именами, т.к. их работа связана с поездками, где необходима покупка и оформление билетов, а так же они часто проходят проверки служб безопасности состоятельных клиентов. Псевдонимы они могут брать только в заграничных турах в страны, где русские женские имена звучат сложно или неблагозвучно.</p>
                    <p><b>Рабочая</b> - есть девушки которые занимаются эскортом изредка, предоставляя услуги ограниченному кругу симпатизирующих им состоятельных клиентов, а есть девушки, которые вписываются во все предлагаемые темы подряд, не уточняя подробностей и не брезгуя никем. Вот последние и есть рабочие лошади.</p>
                    <p><b>Эстафетная содержанка</b> - это девушка нестойких моральных принципов, которая последовательно и регулярно меняет содержателей на все более богатых и влиятельных. Конечная цель такой особы, дойдя до некоторого топ-уровня, попытаться увести содержателя из семьи и женить на себе. Говорят, такому их учат на специальных дорогих тренингах.</p>
                    <p><b>Питательный хуй</b> - это перспективный, состоятельный клиент эскортницы, к которому она хотела бы попасть на содержание. Ежемесячная абонентка от одного мужчины - более безопасный и стабильный способ существования. Однако, даже находясь на содержании, большинство эскортниц продолжает тайно ездить на темы к старым клиентам.</p>
                    <p><b>Пробег</b> - это оценочное количество половых партнеров эскортницы (меряется в сотнях членов), который сильно влияет на стоимость шкуры на рынке. Сами девушки пытаются всеми правдами и неправдами уменьшать это количество, но, как правило, все в тусовке знают примерный пробег каждой эскортницы.</p>
                    <p><b>Нолик</b> - девственница на языке эскорт-менеджеров. Девушка с нулевым пробегом в хуях. Считается эксклюзивом и стоит заметно дороже подраздолбанных. Но, зачастую, это не так. Как правило за "ноликов" ушлые скауты выдают молодых провинциальных baby-face студенток. Известны случаи двойной, тройной продажи ноликов. Некоторые 16-17-ти летние писюхи умудряются продать свою девственность по 6 раз.</p>

                </div>

            </div>

            <?php endif;?>

        </div>

        <div id="footer">© 2019 Shluham.net</div>

    </div>

    <div id="worktime"></div>

    <div id="bg_load"><div id="load_icon"></div><div id="load_percent"></div></div>

    <script type="text/javascript" src="/show/js/system/system.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/resize_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_link_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_dialog_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_upload_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_upload_audio_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/preload_object.js?t=<?php echo time(); ?>"></script>

</body>

</html>


