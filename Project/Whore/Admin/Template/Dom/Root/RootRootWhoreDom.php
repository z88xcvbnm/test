<?php

namespace Project\Whore\Admin\Template\Dom\Root;

use Core\Module\Device\DeviceType;
use Project\Whore\Admin\Content\ContentAdminWhore;
use Project\Whore\Admin\Route\Page\RouteRootAdminWhorePage;

class RootRootWhoreDom{

    /**
     * @return bool
     */
    public  static function init(){

        return true;

    }

}

?>

<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><?php echo ContentAdminWhore::get_page_title('root_page_title'); ?></title>

    <meta name="description" content="<?php echo ContentAdminWhore::get_content('root_description'); ?>" />

    <link rel="apple-touch-icon" sizes="57x57" href="/Resource/Favicon/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="/Resource/Favicon/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/Resource/Favicon/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/Resource/Favicon/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/Resource/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/Resource/Favicon/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/Resource/Favicon/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/Resource/Favicon/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/Resource/Favicon/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="/Resource/Favicon/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/Resource/Favicon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="/Resource/Favicon/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/Resource/Favicon/favicon-16x16.png" />
    <link rel=" manifest" href="/Resource/Favicon/manifest.json" />

    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/Resource/Favicon/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

<?php

    DeviceType::is_mobile();

    if(DeviceType::$is_mobile)
        echo'<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">';
//        echo'<meta name="viewport" content="width=375, initial-scale=1, user-scalable=1">';
//    else
//        echo'<meta name="viewport" content="width=1480, user-scalable=yes">';

?>

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,700' rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/show/css/system/default.css?t=<?php echo time();?>" />

    <script type="text/javascript" src="/show/js/jquery/jquery-3.1.1.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/jquery/jquery.extendext.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/system/config.js?t=<?php echo time();?>"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143269925-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-143269925-1');
    </script>
    

</head>

<body>

    <div id="all">

        <div id="header">
            <div id="logo"></div>

            <?php

                if(DeviceType::$is_mobile)
                    echo'<div id="menu_button"></div>';

            ?>

        </div>

        <div id="root_content" style="opacity: 0">

            <div id="root_face_preview_list"></div>

            <?php if(!DeviceType::$is_mobile):?>

            <div id="root_face_block">

                <div id="root_left_content">

                    <h1>Проверь свою избранницу по самой полной базе данных <strong>эскортниц</strong>,</h1><p style="display: inline;"> <strong>содержанок</strong> и <strong>шлюх</strong>, используя фотографию ее лица в фас.</p>
                    <p style="margin: 10px 0 0 0;">Применяемый биометрический алгоритм распознавания лиц имеет вероятность ложноположительного срабатывания (FAR) 10<sup>-7</sup>. </p><p style="margin-top: 10px;">Чем лучше качество загружаемого фото, тем точнее будет результат.</p>

                </div>

                <div id="root_face_content">

                    <div id="root_search_face_content_loading"></div>

                    <div class="root_search_face_image_block_row">

                        <div id="root_search_face_image_row_container_0" class="root_search_face_image_row_container">

                            <div id="root_search_face_image_block_0" class="root_search_face_image_block">

                                <div id="root_search_face_button_choose_0" class="root_search_face_button_choose"><span>Выберите файл</span></div>
                                <div id="root_search_face_image_info_0" class="root_search_face_image_info">или перетащите его сюда</div>
                                <div id="root_search_face_image_loading_0" class="root_search_face_image_loading"><span>Загрузка...</span></div>
                                <div id="root_search_face_image_error_0" class="root_search_face_image_error">Невереный формат файла</div>

                                <div id="root_search_face_image_block_container_0" class="root_search_face_image_block_container">

                                    <div id="root_search_face_image_src_0" class="root_search_face_image_src"></div>

                                </div>

                                <input type="file" multiple="multiple" id="root_search_face_image_input_0" class="root_search_face_image_input" accept="image/png, image/jpeg" value="" title="" />

                            </div>

                        </div>

                        <div id="root_search_face_image_row_container_1" class="root_search_face_image_row_container">

                            <div id="root_search_face_image_block_1" class="root_search_face_image_block">

                                <div id="root_search_face_button_choose_1" class="root_search_face_button_choose"><span>Выберите файл</span></div>
                                <div id="root_search_face_image_info_1" class="root_search_face_image_info">или перетащите его сюда</div>
                                <div id="root_search_face_image_loading_1" class="root_search_face_image_loading"><span>Загрузка...</span></div>
                                <div id="root_search_face_image_error_1" class="root_search_face_image_error">Невереный формат файла</div>

                                <div id="root_search_face_image_block_container_1" class="root_search_face_image_block_container">

                                    <div id="root_search_face_image_src_1" class="root_search_face_image_src"></div>

                                </div>

                                <input type="file" multiple="multiple" id="root_search_face_image_input_1" class="root_search_face_image_input" accept="image/png, image/jpeg" value="" title="" />

                            </div>

                        </div>

                        <div id="root_search_face_image_row_container_2" class="root_search_face_image_row_container">

                            <div id="root_search_face_image_block_2" class="root_search_face_image_block">

                                <div id="root_search_face_button_choose_2" class="root_search_face_button_choose"><span>Выберите файл</span></div>
                                <div id="root_search_face_image_info_2" class="root_search_face_image_info">или перетащите его сюда</div>
                                <div id="root_search_face_image_loading_2" class="root_search_face_image_loading"><span>Загрузка...</span></div>
                                <div id="root_search_face_image_error_2" class="root_search_face_image_error">Невереный формат файла</div>

                                <div id="root_search_face_image_block_container_2" class="root_search_face_image_block_container">

                                    <div id="root_search_face_image_src_2" class="root_search_face_image_src"></div>

                                </div>

                                <input type="file" multiple="multiple" id="root_search_face_image_input_2" class="root_search_face_image_input" accept="image/png, image/jpeg" value="" title="" />

                            </div>

                        </div>

                    </div>

                    <div id="root_search_info_block" class="root_search_info_block"></div>

                    <div id="root_search_face_price" class="root_search_face_price">Стоимость запроса <span id="root_search_face_cost">0</span> ₽</div>

                    <div id="root_search_face_button" class="root_search_face_button">Проверить фото</div>

                    <?php

                        if(DeviceType::$is_mobile)
                            echo'<div id="root_search_face_button_cancel" class="root_search_face_button">Отмена</div>';

                    ?>

                </div>

                <div id="root_right_content">
                    <div id="root_right_content_container">

                        <h2>На текущий момент в базе содержится:</h2>
                        <p></p>
                        <p><b id="face_len"><?php echo number_format(RouteRootAdminWhorePage::$face_len,0,'',' ');?></b> анкет девушек занимающихся или занимавшихся продажей интимных услуг в России, Украине, Беларуси, Казахстане и других странах.</p>

                        <p style="margin: 0 0 0 0;">Топ городов по количеству шкур:</p>

                <?php

                    if(count(RouteRootAdminWhorePage::$city_list)>0){

                        echo'<p>';

                        foreach(RouteRootAdminWhorePage::$city_list as $row)
                            echo $row['name'].' - <b>'.number_format($row['len'],0,' ',' ').'</b><br />';

                        echo'</p>';

                    }

                ?>

                        <p>Проиндексировано <b id="face_image_len"><?php echo number_format(RouteRootAdminWhorePage::$face_image_indexed_len,0,' ',' ');?></b> лиц.</p>

                    </div>
                </div>

            </div>

            <?php endif;?>

        </div>

        <div id="footer">© 2019 Shluham.net</div>

    </div>

    <div id="worktime"></div>

    <div id="bg_load"><div id="load_icon"></div><div id="load_percent"></div></div>

    <script type="text/javascript" src="/show/js/system/system.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/resize_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_link_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_dialog_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_upload_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_upload_audio_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/preload_object.js?t=<?php echo time(); ?>"></script>

</body>

</html>


