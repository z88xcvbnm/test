<?php

namespace Project\Whore\Admin\Template\Dom\Root\PayPalPayment;

use Core\Module\Device\DeviceType;
use Core\Module\PayPal\PayPalConfig;
use Core\Module\PayPal\PayPalLiveConfig;
use Project\Whore\Admin\Content\ContentAdminWhore;

class RootPayPalPaymentWhoreDom{

    /**
     * @return bool
     */
    public  static function init(){

        return true;

    }

}

?>

<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title><?php echo ContentAdminWhore::get_project_name(); ?></title>

    <meta name="description" content="<?php echo ContentAdminWhore::get_content('root_description'); ?>" />

    <link rel="apple-touch-icon" sizes="57x57" href="/Resource/Favicon/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="/Resource/Favicon/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes=" 72x72" href="/Resource/Favicon/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/Resource/Favicon/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes=" 114x114" href="/Resource/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/Resource/Favicon/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/Resource/Favicon/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes=" 152x152" href="/Resource/Favicon/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/Resource/Favicon/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="/Resource/Favicon/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/Resource/Favicon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="/Resource/Favicon/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/Resource/Favicon/favicon-16x16.png" />
    <link rel=" manifest" href="/Resource/Favicon/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/Resource/Favicon/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

<?php

    DeviceType::is_mobile();

    if(DeviceType::$is_mobile)
        echo'<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1">';
//    else
//        echo'<meta name="viewport" content="width=1480, user-scalable=yes">';

?>

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,700' rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/show/css/system/default.css?t=<?php echo time();?>" />
<!--    <link rel="stylesheet" type="text/css" href="/show/css/style.css?t=--><?php //echo time();?><!--" />-->
<!--    <link rel="stylesheet" type="text/css" href="/show/css/style_new.css?t=--><?php //echo time();?><!--" />-->

    <script type="text/javascript" src="/show/js/jquery/jquery-3.1.1.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/jquery/jquery.extendext.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/system/config.js?t=<?php echo time();?>"></script>

    <script src="https://www.paypal.com/sdk/js?client-id=<?php echo PayPalLiveConfig::$client_ID;?>&currency=USD&commit=true"></script>

<!--            -->
<!--    <script>paypal.Buttons().render('body');</script>-->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143269925-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-143269925-1');
    </script>

</head>



<body>

    <div id="all">

    </div>

    <script>

        // paypal.Button.render({
        //   env: 'sandbox', // Optional: specify 'sandbox' environment
        //   client: {
        //     sandbox:    'Aei32cS-dtP0STlfHTJBnLeCV6XGOGUSgcBgijlDxdTPnh9y78WDPWhdYSzOgazNLmabqnYONZlAh7fU',
        //     production: 'ASRxEH-_kDVl4jTaQh0dUkpgV1G0K3JooLc1dVy1tQMsOCdn3VRnKjPBHHvr9kRWaDV9MLuqb7UTz8Ck'
        //   },
        //   commit: true, // Optional: show a 'Pay Now' button in the checkout flow
        //   payment: function (data, actions) {
        //     return actions.payment.create({
        //       payment: {
        //         transactions: [
        //           {
        //             amount: {
        //               total: '1.00',
        //               currency: 'USD'
        //             }
        //           }
        //         ]
        //       }
        //     });
        //   },
        //   onAuthorize: function (data, actions) {
        //     // Get the payment details
        //     return actions.payment.get()
        //       .then(function (paymentDetails) {
        //         // Show a confirmation using the details from paymentDetails
        //         // Then listen for a click on your confirm button
        //         document.querySelector('#confirm-button')
        //           .addEventListener('click', function () {
        //             // Execute the payment
        //             return actions.payment.execute()
        //               .then(function () {
        //                 // Show a success page to the buyer
        //               });
        //           });
        //       });
        //   }
        // }, '#all');

        var  amount=0.01
            ,is_sandbox=false;

        paypal.Buttons({

            // 'env': 'sandbox',
            'createOrder':function(data, actions) {

              return actions.order.create({
                purchase_units:[{
                  amount:{
                    value:amount
                  }
                }]
              });

            },
            'onApprove':function(data, actions){

                return actions.order.capture().then(function(details){

                    alert('Transaction completed by ' + details.payer.name.given_name);

                    // Call your server to save the transaction
                    return fetch('/api/json/paypal_payment_check',{
                        'method':'post',
                        'headers':{
                            'content-type': 'application/json'
                        },
                        'body':JSON.stringify({
                            'order_ID':data.orderID,
                            'amount':amount,
                            'is_sandbox':is_sandbox?1:0
                        })
                    });
                });

            }
        }).render('#all');

        // paypal.Buttons({
        //     createOrder: function(data, actions) {
        //       return actions.order.create({
        //         purchase_units: [{
        //           amount: {
        //             value: '0.01'
        //           }
        //         }]
        //       });
        //     },
        //     onApprove: function(data, actions) {
        //       return actions.order.capture().then(function(details) {
        //         alert('Transaction completed by ' + details.payer.name.given_name);
        //         // Call your server to save the transaction
        //         return fetch('/paypal-transaction-complete', {
        //           method: 'post',
        //           headers: {
        //             'content-type': 'application/json'
        //           },
        //           body: JSON.stringify({
        //             orderID: data.orderID
        //           })
        //         });
        //       });
        //     }
        //   }).render('#all');

    </script>

</body>

</html>


