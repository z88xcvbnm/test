<?php

namespace Project\Whore\Admin\Template\Dom\Root\Source;

use Core\Module\Device\DeviceType;
use Project\Whore\Admin\Content\ContentAdminWhore;

class RootSourceWhoreDom{

    /**
     * @return bool
     */
    public  static function init(){

        return true;

    }

}

?>

<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><?php echo ContentAdminWhore::get_page_title('root_source_page_title'); ?></title>

    <meta name="description" content="<?php echo ContentAdminWhore::get_content('root_description'); ?>" />

    <link rel="apple-touch-icon" sizes="57x57" href="/Resource/Favicon/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="/Resource/Favicon/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes=" 72x72" href="/Resource/Favicon/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/Resource/Favicon/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes=" 114x114" href="/Resource/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/Resource/Favicon/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/Resource/Favicon/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes=" 152x152" href="/Resource/Favicon/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/Resource/Favicon/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="/Resource/Favicon/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/Resource/Favicon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="/Resource/Favicon/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/Resource/Favicon/favicon-16x16.png" />
    <link rel=" manifest" href="/Resource/Favicon/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/Resource/Favicon/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

<?php

    DeviceType::is_mobile();

    if(DeviceType::$is_mobile)
        echo'<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1">';
//    else
//        echo'<meta name="viewport" content="width=1480, user-scalable=yes">';

?>

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,700' rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/show/css/system/default.css?t=<?php echo time();?>" />
<!--    <link rel="stylesheet" type="text/css" href="/show/css/style.css?t=--><?php //echo time();?><!--" />-->
<!--    <link rel="stylesheet" type="text/css" href="/show/css/style_new.css?t=--><?php //echo time();?><!--" />-->

    <script type="text/javascript" src="/show/js/jquery/jquery-3.1.1.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/jquery/jquery.extendext.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/system/config.js?t=<?php echo time();?>"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143269925-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-143269925-1');
    </script>

</head>

<body>

    <div id="all">

        <div id="header">
            <div id="logo"></div>
        </div>

        <div id="root_content" style="opacity: 0">

            <div id="root_face_preview_list"></div>

            <?php if(!DeviceType::$is_mobile):?>

            <div id="root_face_block">

                <p id="root_default_content">

                    <h1>Источники анкет эскортниц, проституток и содержанок</h1>
                    <p><b>Model Zone</b> - сайт, рассылка, канал и бот в мессенджере Telegram, которые агрегируют анкеты девушек, предоставляющих эскорт-услуги на территории стран СНГ. Иногда на одну и туже девушку может выдаваться несколько анкет из этого источника. Это означает, что девушка на время выходила из бизнеса (например, имела постоянные отношения), а потом вернулась в бизнес, создав новую анкету.</p>
                    <p><b>Рынок Шкур (РШ)</b> - канал в мессенджере Telegram, разоблачающий Instagram-звезд, различных блогерок, медийных персон, участниц реалити-шоу, спортсменок, певичек и успешных моделей, которые не брезгуют эскортом. Анкеты большинства из них вы и так можете найти в нашей базе, но бывают и эксклюзивы.</p>
                    <p><b>Атолин.ру</b> – сайт позиционирует себя как ресурс для конфиденциальных знакомств, помогающий состоятельным женатым мужчинам найти себе содержанку для долговременных возмездных отношений или девушку на одну ночь. По сути же - это просто каталог проституток-индивидуалок с рубрикацией по городам.</p>
                    <p><b>Содержанка.рф</b> - сайт с анкетами женщин, которые жаждут стать твоими любовницами, если ты успешный мужчина. Зачастую прямо в анкетах соискательниц на почетное звание содержанки стоит ценник, который начинается от 100К/мес. Короче, этот ресурс - еще одна добровольная перепись проституток России.</p>
                    <p><b>Dosug.ru (dosug.cz, dosug.nu)</b> - один из старейших и самых известных сайтов, рекламирующих интимные услуги в России. В основном содержит анкеты дорогих московских проституток, т.к. разместить информацию на нем стоит не дешего. В настоящий момент сайт заблокирован по решению Роскомнадзора, но это не сильно затрудняет доступ к нему.</p>
                    <p><b>Whores777.com</b> - самый большой и самый полный каталог проституток Москвы всех ценовых категорий, содержащий фотографии, параметры и контактные данные девушек легкого поведения, а так же полное описание предоставляемых ими услуг.</p>
                    <p><b>EscortNews.NL</b> - крупный агрегатор предложений от эскортниц и проституток в Голландии и Бельгии. Исторически так сложилось, что он содержит огромное количество предложений от наших соотечественниц, как от агентств, так и от инди.</p>
                    <p><b>Lovesss.ru</b> - как и другие сайты для начинающих проституток, позиционирует себя как "место, где встречаются красота и успех" и предназначен для поиска содержанок. По факту же - это очередная добровольная перепись эскортниц из России и стран СНГ.</p>
                    <p><b>Prostitutki SPb</b>, <b>Prostitutki Red</b>, <b>ProstitutkiPiteraGood.net</b>, <b>Gorchiza.com</b> - избыточный перечень сайтов-каталогов проституток Санкт-Петербурга, на которых представлены как индивидуалки, так и салоны-бордели</p>
                    <p><b>Lovelama.ru</b> - это очередное место встречи красивых девушек и состоятельных мужчин, ищущих содержанок/эскортниц. Лама - это, по сути, перуанская овца. Вот эти овцы и клюют на словосочетание "богатый мужчина", которое встречается в описании сайта 144 раза. Создают анкеты, заливают фото и встают на скользкую дорожку торговли телом.</p>
                    <p><b>Zolushka-project.com</b> - сайт знакомит "современных золушек" с мужчинами, желающими "отдохнуть от супружеской жизни", очевидно играя на комплексах симпатичных неудачниц, несумевших реализоваться в жизни любым другим способом, кроме торговли своим телом и временем.</p>
                    <p><b>Soderganki-online.ru</b> - как и десяток других сайтов выше, знакомит красивых бездельниц с неверными мужьями для промискуитета на возмездной основе, обещая первым сумки, цацки, перья и машины. Из особенностей - сайт пытается выстроить аналогии между посетительницами сайта и героинями одноименного российского сериала, обещая им образ жизни "как там" после продажи мохнатки.</p>
                    <p><b>Luxmodel.info</b> - сайт позиционирует себя как топовое эскорт-агентство Санкт-Петербурга, основной целевой аудиторией которого являются скучающие иностранцы, прибывшие в город на отдых или по делам (у сайта даже нет русскоязычного интерфейса). Но в целом по подаче, по почасовой оплате и доступным тарифам, перечню включенных в описании каждой девушки интимных услуг - это, скорее, обычная проститушная.</p>
                    <p><b>IntimCity.nl</b> - огромный онлайн каталог проституток Москвы с потертым дизайном из нулевых. Тут есть все - от пристарелых путан, которые пришли в профессию еще на волне "Интердевочки", до трансвеститов, которые сверху, по пояс, выглядят как модели "Victorias Secret", а снизу, по пояс - как Рокко Сиффреди. Наверняка многих из них вы встречаете на улице, а некоторые даже ваши соседи.</p>
                    <p><b>Favoritka.org</b> -  еще один сайт для поиска любовницы (содержанки) или спонсора в Москве и любом другом городе России, Украины или Беларуси. Заявляется, что все анкеты сайта проходят ручную модерацию, что гарантирует соответствие фотографий и указанных в анкете данных реальности. Причем это касается как анкет содержанок, так и анкет спонсоров. Спасибо, это то, что надо!</p>
                    <p><b>Paramours.ru (любовницы.ру)</b> - место, где богатые спонсоры ищут любовниц. "Этот ресурс создан удобным и понятным, чтобы каждый мужчина смог найти себе девушку содержанку или любовницу, а девушки, в свою очередь, смогли найти себе спонсора - богатого любовника. Любовницы.ру - это сайт знакомств для женатых мужчин (папиков) и красивых девушек-содержанок."</p>
                    <p><b>Prostitutki Kieva</b>, <b>ProstitutkiKieva.Info</b>, <b>Fei Kiev</b> - избыточный перечень сайтов-каталогов проституток Киева и других городов Украины, на страницах которых представлены как индивидуалки, так и салоны-бордели.</p>

                </div>

            </div>

            <?php endif;?>

        </div>

        <div id="footer">© 2019 Shluham.net</div>

    </div>

    <div id="worktime"></div>

    <script type="text/javascript" src="/show/js/system/system.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/resize_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_link_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_dialog_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_upload_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_upload_audio_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/preload_object.js?t=<?php echo time(); ?>"></script>

    <div id="bg_load"><div id="load_icon"></div><div id="load_percent"></div></div>

</body>

</html>


