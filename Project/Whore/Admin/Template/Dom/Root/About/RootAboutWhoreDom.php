<?php

namespace Project\Whore\Admin\Template\Dom\Root\About;

use Core\Module\Device\DeviceType;
use Project\Whore\Admin\Content\ContentAdminWhore;use Project\Whore\Admin\Route\Page\RouteRootAdminWhorePage;

class RootAboutWhoreDom{

    /**
     * @return bool
     */
    public  static function init(){

        return true;

    }

}

?>

<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><?php echo ContentAdminWhore::get_project_name(); ?></title>

    <meta name="description" content="<?php echo ContentAdminWhore::get_content('root_description'); ?>" />

    <link rel="apple-touch-icon" sizes="57x57" href="/Resource/Favicon/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="/Resource/Favicon/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes=" 72x72" href="/Resource/Favicon/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/Resource/Favicon/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes=" 114x114" href="/Resource/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/Resource/Favicon/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/Resource/Favicon/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes=" 152x152" href="/Resource/Favicon/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/Resource/Favicon/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="/Resource/Favicon/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/Resource/Favicon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="/Resource/Favicon/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/Resource/Favicon/favicon-16x16.png" />
    <link rel=" manifest" href="/Resource/Favicon/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/Resource/Favicon/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

<?php

    DeviceType::is_mobile();

    if(DeviceType::$is_mobile)
        echo'<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1">';
//    else
//        echo'<meta name="viewport" content="width=1480, user-scalable=yes">';

?>

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,700' rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/show/css/system/default.css?t=<?php echo time();?>" />
<!--    <link rel="stylesheet" type="text/css" href="/show/css/style.css?t=--><?php //echo time();?><!--" />-->
<!--    <link rel="stylesheet" type="text/css" href="/show/css/style_new.css?t=--><?php //echo time();?><!--" />-->

    <script type="text/javascript" src="/show/js/jquery/jquery-3.1.1.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/jquery/jquery.extendext.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/system/config.js?t=<?php echo time();?>"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143269925-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-143269925-1');
    </script>

</head>

<body>

    <div id="all">

        <div id="header">
            <div id="logo"></div>
        </div>

        <div id="root_content" style="opacity: 0">

            <div id="root_face_preview_list"></div>

            <?php if(!DeviceType::$is_mobile):?>

            <div id="root_face_block">

                <div id="root_default_content">

                    <h1 class="page_title">Для чего создан шлюхам.нет (шкурам.нет)?</h1>

                    <p>По нашей статистике в России, Украине, Беларуси и прочих государствах бывшего СССР примерно <b>250 тысяч</b> особей женского пола занимаются проституцией, эскортом и прочей торговлей собственным телом.</p>

                    <p>Вам кажется, что это мало?</p>

                    <p>Но на самом деле каждая вторая девушка, которую вы видите в клубе или ресторане вечером, вовлечена в занятие эскортом или проституцией.</p>

                    <p>Давайте посчитаем</p>:
                    <ul>
                        <li>женщин в возрасте от 18 до 35 лет в этих странах живет порядка 40 миллионов</li>
                        <li>из них в крупных городах проживает примерно 25 процентов – круг сузился до 10 миллионов, далее</li>
                        <li>половина из них условно страшненькие, уже - 5 миллионов</li>
                        <li>10% из них регулярно ходят в рестораны и клубы, получается, что круг сужается до 500 тысяч женщин, примерно половина из них ходит в клубы и рестораны с любимыми или друзьями (просто присмотритесь).</li>
                    </ul>

                    <p>И что у нас остается? Именно! Примерно половина девушек, которых вы видите в этих местах занимаются эскортом или проституцией. Так может и вам настало время проверить свою избранницу?</p>

                    <p>Скрытая проституция в виде всевозможных эскорт услуг стала настолько массовым явлением, что перестала существовать отдельно от повседневной жизни обычных людей. Именно поэтому мы и сделали для вас этот сервис.</p>

                </div>

            </div>

            <?php endif;?>

        </div>

        <div id="footer">© 2019 Shluham.net</div>

    </div>

    <div id="worktime"></div>

    <div id="bg_load"><div id="load_icon"></div><div id="load_percent"></div></div>

    <script type="text/javascript" src="/show/js/system/system.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/resize_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_link_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_dialog_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_upload_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_upload_audio_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/preload_object.js?t=<?php echo time(); ?>"></script>

</body>

</html>


