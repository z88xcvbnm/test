<?php

namespace Project\Whore\Admin\Template\Dom\Profile\City;

use Core\Module\Device\DeviceType;
use Project\Whore\Admin\Content\ContentAdminWhore;

class ProfileCityWhoreDom{

    /**
     * @return bool
     */
    public  static function init(){

        return true;

    }

}

?>

<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><?php echo ContentAdminWhore::get_page_title('root_city_page_title'); ?></title>

    <meta name="description" content="<?php echo ContentAdminWhore::get_content('city_description'); ?>" />

    <link rel="apple-touch-icon" sizes="57x57" href="/Resource/Favicon/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="/Resource/Favicon/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes=" 72x72" href="/Resource/Favicon/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/Resource/Favicon/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes=" 114x114" href="/Resource/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/Resource/Favicon/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/Resource/Favicon/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes=" 152x152" href="/Resource/Favicon/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/Resource/Favicon/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="/Resource/Favicon/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/Resource/Favicon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="/Resource/Favicon/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/Resource/Favicon/favicon-16x16.png" />
    <link rel=" manifest" href="/Resource/Favicon/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="/Resource/Favicon/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

<?php

    DeviceType::is_mobile();

    if(DeviceType::$is_mobile)
        echo'<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1">';
//    else
//        echo'<meta name="viewport" content="width=1480, user-scalable=yes">';

?>

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,700' rel='stylesheet' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/show/css/system/default.css?t=<?php echo time();?>" />
<!--    <link rel="stylesheet" type="text/css" href="/show/css/style.css?t=--><?php //echo time();?><!--" />-->
<!--    <link rel="stylesheet" type="text/css" href="/show/css/style_new.css?t=--><?php //echo time();?><!--" />-->

    <script type="text/javascript" src="/show/js/jquery/jquery-3.1.1.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/jquery/jquery.extendext.min.js?t=<?php echo time();?>"></script>
    <script type="text/javascript" src="/show/js/system/config.js?t=<?php echo time();?>"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143269925-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-143269925-1');
    </script>

</head>

<body>

    <div id="all">

        <div id="header">
            <div id="logo"></div>
        </div>

        <div id="root_content" style="opacity: 0">

            <div id="root_face_preview_list"></div>

            <?php if(!DeviceType::$is_mobile):?>

<!--            <div id="root_face_block">-->
<!---->
<!--                <div id="root_default_content">-->
<!---->
<!--                    inner+='<div id="root_default_content">';-->
<!---->
<!--                    inner+='<h1 style="text-align: center">Статистика городов по количеству шлюх</h1>';-->
<!---->
<!--                    inner+='<div id="root_city_stats_block">';-->
<!---->
<!--                        inner+='<div id="root_city_stats_header">';-->
<!---->
<!--                            inner+='<div class="root_city_stats_header_col" style="width: 10%; max-width: 50px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">#</td></tr></table></div>';-->
<!--                            inner+='<div class="root_city_stats_header_col" style="width: 50%; max-width: 200px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">Город</td></tr></table></div>';-->
<!--                            inner+='<div class="root_city_stats_header_col" style="width: 40%; max-width: 100px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">Анкет, шт</td></tr></table></div>';-->
<!--                            // inner+='<div class="root_city_stats_header_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">Фотографий, шт</td></tr></table></div>';-->
<!---->
<!--                        inner+='</div>';-->
<!---->
<!--                for(index in list){-->
<!---->
<!--                    inner+='<div class="root_city_stats_row_item">';-->
<!---->
<!--                        inner+='<div class="root_city_stats_row_item_col" style="width: 10%; max-width: 50px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(parseInt(index)+1)+'</td></tr></table></div>';-->
<!--                        inner+='<div class="root_city_stats_row_item_col" style="width: 50%; max-width: 200px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['name'])+'</td></tr></table></div>';-->
<!--                        inner+='<div class="root_city_stats_row_item_col" style="width: 40%; max-width: 100px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['len'])+'</td></tr></table></div>';-->
<!--                        // inner+='<div class="root_city_stats_row_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['image_len'])+'</td></tr></table></div>';-->
<!---->
<!--                    inner+='</div>';-->
<!---->
<!--                }-->
<!---->
<!--                            inner+='</div>';-->
<!--                    inner+='</div>';-->
<!---->
<!--                </div>-->
<!---->
<!--            </div>-->

            <?php endif;?>

        </div>

        <div id="footer">© 2019 Shluham.net</div>

    </div>

    <div id="worktime"></div>

    <div id="bg_load"><div id="load_icon"></div><div id="load_percent"></div></div>

    <script type="text/javascript" src="/show/js/system/system.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/resize_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_link_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_dialog_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_upload_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/page_upload_audio_object.js?t=<?php echo time(); ?>"></script>
    <script type="text/javascript" src="/show/js/system/preload_object.js?t=<?php echo time(); ?>"></script>

</body>

</html>


