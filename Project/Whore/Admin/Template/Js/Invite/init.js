page_object.action.invite={
    'data':{},
    'script_list':{
        'core':{
            'root':'/show/js/root/init.js'
        }
    },
    'style_list':[
        '/show/css/system/root.css',
        '/show/css/system/dialog_block.css'
    ],
    'content':{
        'ru':{
            'title':'Регистрация пользователя',
            'wait':'Подождите',
            'save':'Сохранить',
            'login':'Логин',
            'password':'Создайте пароль',
            'password_help':'мин. 6 символов',
            'repeat_password':'Повторите пароль',
            'email':'Email',
            'name':'Имя',
            'surname':'Фамилия'
        },
        'en':{
            'title':'User registration',
            'wait':'Wait',
            'save':'Save',
            'login':'Login',
            'password':'Create a password',
            'password_help':'min 6 symbols',
            'repeat_password':'Repeat password',
            'email':'Email',
            'name':'Name',
            'surname':'Surname'
        }
    },
    'init':function(data){

        page_object.action.invite.data=data;

        var temp={
            'script_list':config.updateScriptsList(page_object.action.invite.script_list, 'Root'),
            'style_list':config.updateScriptsList(page_object.action.invite.style_list, 'Root'),
            'init_list':[
                function(){

                    if (typeof config !== 'undefined' && $.isFunction(config.updateSectionChangeWorkflow)) {
                        page_object.action.root.section_change_workflow = config.updateSectionChangeWorkflow(page_object.action.root.section_change_workflow);
                    }

                    page_object.action.root.init(data);

                }
            ]
        };

        page_object.require.init(temp);

    },
    'run':function(data){

        page_object.action.invite.data=data;

        page_object.action.invite.create.init();

    },
    'create':{
        'dialog_index':null,
        'init':function(){

            var  lang_obj   =page_object.action.invite.content[page_object.lang]
                ,inner      =''
                ,data       =page_object.action.invite.data;

            inner+='<div id="dialog_login_block" class="dialog_row">';
            inner+='<div id="dialog_login_label" class="dialog_row_label_x2">';
            inner+='<span>'+stripSlashes(lang_obj['login'])+'</span>';
            inner+='<span id="invite_block" class="dialog_red_field">*</span>';
            inner+='</div>';
            inner+='<div id="dialog_login_input_block" class="dialog_row_input">';
            inner+='<input type="text" id="dialog_login_input_text" class="dialog_row_input_text" value="'+stripSlashes(empty(data['login'])?'':data['login'])+'" style="border-color: #84cf30; " />';
            inner+='</div>';
            inner+='</div>';

            inner+='<div id="dialog_email_block" class="dialog_row">';
            inner+='<div id="dialog_email_label" class="dialog_row_label_x2">';
            inner+='<span>'+stripSlashes(lang_obj['email'])+'</span>';
            inner+='<span class="dialog_red_field">*</span>';
            inner+='</div>';
            inner+='<div id="dialog_email_input_block" class="dialog_row_input">';
            inner+='<input type="text" id="dialog_email_input_text" class="dialog_row_input_text" value="'+stripSlashes(empty(data['email'])?'':data['email'])+'" style="border-color: #84cf30; " />';
            inner+='</div>';
            inner+='</div>';

            inner+='<div id="dialog_name_block" class="dialog_row">';
            inner+='<div id="dialog_name_label" class="dialog_row_label_x2">';
            inner+='<span>'+stripSlashes(lang_obj['name'])+'</span>';
            inner+='<span class="dialog_red_field">*</span>';
            inner+='</div>';
            inner+='<div id="dialog_name_input_block" class="dialog_row_input">';
            inner+='<input type="text" id="dialog_name_input_text" class="dialog_row_input_text" value="'+stripSlashes(empty(data['name'])?'':data['name'])+'" style="border-color: #84cf30; " />';
            inner+='</div>';
            inner+='</div>';

            inner+='<div id="dialog_surname_block" class="dialog_row">';
            inner+='<div id="dialog_surname_label" class="dialog_row_label_x2">';
            inner+='<span>'+stripSlashes(lang_obj['surname'])+'</span>';
            inner+='<span class="dialog_red_field">*</span>';
            inner+='</div>';
            inner+='<div id="dialog_surname_input_block" class="dialog_row_input">';
            inner+='<input type="text" id="dialog_surname_input_text" class="dialog_row_input_text" value="'+stripSlashes(empty(data['surname'])?'':data['surname'])+'" style="border-color: #84cf30; " />';
            inner+='</div>';
            inner+='</div>';

            inner+='<div id="dialog_password_block" class="dialog_row" style="padding-bottom: 0;">';
            inner+='<div id="dialog_password_label" class="dialog_row_label_x2">';
            inner+='<span>'+stripSlashes(lang_obj['password'])+'</span>';
            inner+='<span class="dialog_red_field">*</span>';
            inner+='</div>';
            inner+='<div id="dialog_password_input_block" class="dialog_row_input">';
            inner+='<input type="password" id="dialog_password_input_text" class="dialog_row_input_text" value="" />';
            inner+='<span class="dialog_password_help_label">'+stripSlashes(lang_obj['password_help'])+'</span>';
            inner+='</div>';
            inner+='</div>';

            inner+='<div id="dialog_repeat_password_block" class="dialog_row" style="margin-top: 0;">';
            inner+='<div id="dialog_repeat_password_label" class="dialog_row_label_x2">';
            inner+='<span>'+stripSlashes(lang_obj['repeat_password'])+'</span>';
            inner+='<span class="dialog_red_field">*</span>';
            inner+='</div>';
            inner+='<div id="dialog_repeat_password_input_block" class="dialog_row_input">';
            inner+='<input type="password" id="dialog_repeat_password_input_text" class="dialog_row_input_text" value="" />';
            inner+='</div>';
            inner+='</div>';

            page_object.action.invite.create.dialog_index=page_object.dialog.init({
                'title':lang_obj['title'],
                'w':520,
                'inner':inner,
                'save':function(){

                    page_object.action.invite.action.save(page_object.action.invite.create.dialog_index);

                },
                'on_create':function(){

                    page_object.action.invite.action.check_login.success         =true;
                    page_object.action.invite.action.check_email.success         =true;
                    page_object.action.invite.action.check_name.success          =true;
                    page_object.action.invite.action.check_surname.success       =true;
                    page_object.action.invite.action.check_password.success      =false;

                    //$d('dialog_email_input_text').onkeydown                 =page_object.action.invite.action.keydown;
                    $d('dialog_email_input_text').onkeyup                   =page_object.action.invite.action.check_email.init;

                    //$d('dialog_login_input_text').onkeydown                 =page_object.action.invite.action.keydown;
                    $d('dialog_login_input_text').onkeyup                   =page_object.action.invite.action.check_login.init;

                    //$d('dialog_name_input_text').onkeydown                  =page_object.action.invite.action.keydown;
                    $d('dialog_name_input_text').onkeyup                    =page_object.action.invite.action.check_name.init;

                    //$d('dialog_surname_input_text').onkeydown               =page_object.action.invite.action.keydown;
                    $d('dialog_surname_input_text').onkeyup                 =page_object.action.invite.action.check_surname.init;

                    //$d('dialog_password_input_text').onkeydown              =page_object.action.invite.action.keydown;
                    $d('dialog_password_input_text').onkeyup                =page_object.action.invite.action.check_password.init;

                    //$d('dialog_repeat_password_input_text').onkeydown       =page_object.action.invite.action.keydown;
                    $d('dialog_repeat_password_input_text').onkeyup         =page_object.action.invite.action.check_password.init;

                    page_object.link.preload.un_show();

                }
            });

        }
    },
    'action':{
        'save':function(dialog_index){

            if(
                  page_object.action.invite.action.check_login.success
                &&page_object.action.invite.action.check_email.success
                &&page_object.action.invite.action.check_name.success
                &&page_object.action.invite.action.check_surname.success
                &&page_object.action.invite.action.check_password.success
            ){

                var  post           =''
                    ,lang_obj       =page_object.action.invite.content[page_object.lang];

                post+='login='+uniEncode($d('dialog_login_input_text').value.toLowerCase());
                post+='&email='+uniEncode($d('dialog_email_input_text').value.toLowerCase());
                post+='&name='+uniEncode($d('dialog_name_input_text').value);
                post+='&surname='+uniEncode($d('dialog_surname_input_text').value);
                post+='&password='+uniEncode($d('dialog_password_input_text').value);
                post+='&hash='+uniEncode(page_object.action.invite.data['hash']);

                if(isset($d('dialog_submit_save_'+dialog_index))){

                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');

                    $d('dialog_submit_save_'+dialog_index).innerHTML      =lang_obj['wait'];
                    $d('dialog_submit_save_'+dialog_index).onclick        =function(){};

                }

                send({
                    'scriptPath':'/api/json/save_user_settings',
                    'postData':post,
                    'onComplete':function(j,worktime){

                        var  dataTemp
                            ,data;

                        dataTemp=j.responseText;
                        trace(dataTemp);
                        data=jsonDecode(dataTemp);
                        trace(data);
                        trace_worktime(worktime,data);

                        if(isset(data['error'])){

                            if(isset(data['error']['login'])){

                                page_object.action.invite.action.check_login.success=false;

                                $s('dialog_login_input_text').borderColor='#e14141';

                            }

                            if(isset(data['error']['email'])){

                                page_object.action.invite.action.check_email.success=false;

                                $s('dialog_email_input_text').borderColor='#e14141';

                            }

                            if(isset(data['error']['name'])){

                                page_object.action.invite.action.check_name.success=false;

                                $s('dialog_name_input_text').borderColor='#e14141';

                            }

                            if(isset(data['error']['surname'])){

                                page_object.action.invite.action.check_surname.success=false;

                                $s('dialog_surname_input_text').borderColor='#e14141';

                            }

                            if(isset(data['error']['password'])){

                                page_object.action.invite.action.check_password.success=false;

                                $s('dialog_password_input_text').borderColor='#e14141';
                                $s('dialog_repeat_password_input_text').borderColor='#e14141';

                            }

                            $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');

                            $d('dialog_submit_save_'+dialog_index).innerHTML      =stripSlashes(lang_obj['save']);
                            $d('dialog_submit_save_'+dialog_index).onclick        =page_object.action.invite.action.save;

                        }
                        else{

                            setUrl(data['title'],data['redirect']);

                            switch(data['type']){

                                case'profile':{

                                    break;

                                }

                                default:{

                                    page_object.action.root.action.root_content.un_show(true);

                                    break;

                                }

                            }

                            page_object.dialog.action.un_show.init(dialog_index,true);

                            // var link='/admin/'+data['login'];
                            //
                            // if(isset($d('root_content')))
                            //     page_object.action.root.action.root_content.un_show(true);
                            //
                            // setUrl(lang_obj['title']+' | '+lang_obj['wait'],link);
                            //
                            // page_object.dialog.action.un_show.init(dialog_index,true);

                        }

                    }
                });

            }
            else{

                if($v('dialog_login_input_text').length<3||$v('dialog_login_input_text').length>32)
                    $s('dialog_login_input_text').borderColor='#e14141';

                if($v('dialog_name_input_text').length<2||$v('dialog_name_input_text').length>32)
                    $s('dialog_name_input_text').borderColor='#e14141';

                if($v('dialog_surname_input_text').length<2||$v('dialog_surname_input_text').length>32)
                    $s('dialog_surname_input_text').borderColor='#e14141';

                if($v('dialog_password_input_text').length<6||$v('dialog_password_input_text').length>32)
                    $s('dialog_password_input_text').borderColor='#e14141';

                if($v('dialog_repeat_password_input_text').length<6||$v('dialog_repeat_password_input_text').length>32)
                    $s('dialog_repeat_password_input_text').borderColor='#e14141';

            }

        },
        'keydown':function(){

            this.style.borderColor='#1b93db';

        },
        'check_name':{
            'success':true,
            'timeout':null,
            'init':function(){

                if(isset(page_object.action.invite.action.check_name.timeout))
                    clearTimeout(page_object.action.invite.action.check_name.timeout);

                page_object.action.invite.action.check_name.timeout=setTimeout(page_object.action.invite.action.check_name.check,500);

            },
            'check':function(){

                if($d('dialog_name_input_text').value.length>=2&&$d('dialog_name_input_text').value.length<=32){

                    page_object.action.invite.action.check_name.success=true;

                    $s('dialog_name_input_text').borderColor='#84cf30';

                }
                else{

                    page_object.action.invite.action.check_name.success=false;

                    $s('dialog_name_input_text').borderColor='#e14141';

                }

            }
        },
        'check_surname':{
            'success':true,
            'timeout':null,
            'init':function(){

                if(isset(page_object.action.invite.action.check_surname.timeout))
                    clearTimeout(page_object.action.invite.action.check_surname.timeout);

                page_object.action.invite.action.check_surname.timeout=setTimeout(page_object.action.invite.action.check_surname.check,500);

            },
            'check':function(){

                if($d('dialog_surname_input_text').value.length>=2&&$d('dialog_surname_input_text').value.length<=32){

                    page_object.action.invite.action.check_surname.success=true;

                    $s('dialog_surname_input_text').borderColor='#84cf30';

                }
                else{

                    page_object.action.invite.action.check_surname.success=false;

                    $s('dialog_surname_input_text').borderColor='#e14141';

                }

            }
        },
        'check_email':{
            'success':true,
            'timeout':null,
            'init':function(){

                if(isset(page_object.action.invite.action.check_email.timeout))
                    clearTimeout(page_object.action.invite.action.check_email.timeout);

                page_object.action.invite.action.check_email.timeout=setTimeout(page_object.action.invite.action.check_email.send,500);

            },
            'send':function(){

                if(isEmail($d('dialog_email_input_text').value)){

                    send({
                        'scriptPath':'/api/json/isset_user_email',
                        'postData':'email='+uniEncode($d('dialog_email_input_text').value.toLowerCase())+'&hash='+uniEncode(page_object.action.invite.data['hash']),
                        'onComplete':function(j,worktime){

                            var  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                page_object.action.invite.action.check_email.success=false;

                                $s('dialog_email_input_text').borderColor='#e14141';

                            }
                            else if(data['isset_email']){

                                page_object.action.invite.action.check_email.success=false;

                                $s('dialog_email_input_text').borderColor='#e14141';

                            }
                            else{

                                page_object.action.invite.action.check_email.success=true;

                                $s('dialog_email_input_text').borderColor='#84cf30';

                            }

                        }
                    });

                    return true;

                }
                else{

                    $s('dialog_email_input_text').borderColor='#e14141';

                    page_object.action.invite.action.check_email.success=false;

                    return false;

                }

            }
        },
        'check_login':{
            'success':true,
            'timeout':null,
            'init':function(){

                if(isset(page_object.action.invite.action.check_login.timeout))
                    clearTimeout(page_object.action.invite.action.check_login.timeout);

                page_object.action.invite.action.check_login.timeout=setTimeout(page_object.action.invite.action.check_login.send,500);

            },
            'send':function(){

                if($d('dialog_login_input_text').value.length>=3&&$d('dialog_login_input_text').value.length<=32){

                    send({
                        'scriptPath':'/api/json/isset_user_login',
                        'postData':'login='+uniEncode($d('dialog_login_input_text').value.toLowerCase())+'&hash='+uniEncode(page_object.action.invite.data['hash']),
                        'onComplete':function(j,worktime){

                            var  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                page_object.action.invite.action.check_login.success=false;

                                $s('dialog_login_input_text').borderColor='#e14141';

                            }
                            else if(data['isset_login']){

                                page_object.action.invite.action.check_login.success=false;

                                $s('dialog_login_input_text').borderColor='#e14141';

                            }
                            else{

                                page_object.action.invite.action.check_login.success=true;

                                $s('dialog_login_input_text').borderColor='#84cf30';

                            }

                        }
                    });

                }
                else{

                    $s('dialog_login_input_text').borderColor='#e14141';

                    page_object.action.invite.action.check_login.success=false;

                    return false;

                }

            }
        },
        'check_password':{
            'success':false,
            'timeout':null,
            'init':function(){

                var el_ID=this.id;

                if(isset(page_object.action.invite.action.check_password.timeout))
                    clearTimeout(page_object.action.invite.action.check_password.timeout);

                page_object.action.invite.action.check_password.timeout=setTimeout(function(){

                    page_object.action.invite.action.check_password.check(el_ID);

                },40);

            },
            'check':function(el_ID){

                switch(el_ID){

                    case'dialog_password_input_text':{

                        if($d('dialog_password_input_text').value.length>=6&&$d('dialog_password_input_text').value.length<=32){

                            $s('dialog_password_input_text').borderColor='#84cf30';

                            if($d('dialog_repeat_password_input_text').value.length<6){

                                $s('dialog_repeat_password_input_text').borderColor='#1b93db';

                                page_object.action.invite.action.check_password.success=false;

                            }
                            else if($d('dialog_repeat_password_input_text').value.length>=6&&$d('dialog_repeat_password_input_text').value.length<=32&&$d('dialog_repeat_password_input_text').value==$d('dialog_password_input_text').value){

                                $s('dialog_repeat_password_input_text').borderColor='#84cf30';

                                page_object.action.invite.action.check_password.success=true;

                            }
                            else{

                                $s('dialog_repeat_password_input_text').borderColor='#e14141';

                                page_object.action.invite.action.check_password.success=false;

                            }

                        }
                        else if($d('dialog_password_input_text').value.length<6){

                            $s('dialog_password_input_text').borderColor='#1b93db';

                            page_object.action.invite.action.check_password.success=false;

                        }
                        else{

                            $s('dialog_password_input_text').borderColor='#e14141';

                            page_object.action.invite.action.check_password.success=false;

                        }

                        break;

                    }

                    case'dialog_repeat_password_input_text':{

                        if($d('dialog_repeat_password_input_text').value.length>=6&&$d('dialog_repeat_password_input_text').value.length<=32){

                            if($d('dialog_password_input_text').value.length<6){

                                $s('dialog_repeat_password_input_text').borderColor='#84cf30';

                                page_object.action.invite.action.check_password.success=false;

                            }
                            else if($d('dialog_password_input_text').value.length>=6&&$d('dialog_password_input_text').value.length<=32&&$d('dialog_repeat_password_input_text').value==$d('dialog_password_input_text').value){

                                $s('dialog_password_input_text').borderColor            ='#84cf30';
                                $s('dialog_repeat_password_input_text').borderColor     ='#84cf30';

                                page_object.action.invite.action.check_password.success=true;

                            }
                            else{

                                $s('dialog_repeat_password_input_text').borderColor='#e14141';

                                page_object.action.invite.action.check_password.success=false;

                            }

                        }
                        else if($d('dialog_repeat_password_input_text').value.length<6){

                            $s('dialog_repeat_password_input_text').borderColor='#1b93db';

                            page_object.action.invite.action.check_password.success=false;

                        }
                        else{

                            $s('dialog_repeat_password_input_text').borderColor='#e14141';

                            page_object.action.invite.action.check_password.success=false;

                        }

                        break;

                    }

                }

            }
        },
        'un_show':function(){

            if(!empty(page_object.action.invite.create.dialog_index))
                page_object.dialog.action.un_show.init(page_object.action.invite.create.dialog_index,true);

        }
    },
    'resize':function(){}
};