    function $d(id){

        if(isset(window.document.getElementById(id)))
            return window.document.getElementById(id);
        else
            return undefined;

    }

    function $s(id){

        if(isset(window.document.getElementById(id)))
            return window.document.getElementById(id).style;
        else
            return undefined;

    }

    function $v(id){

        if(isset($d(id)))
            return $d(id).value;
        else
            return null;

    }

    function $w(){

        return window;

    }

    function $b(){

        return window.document.body;

    }

    function $n(){

        return $w()['navigator'];

    }

    function $e(){

        return $w().event;

    }

    function removeElement(el){

        if(isset(el)){

            if(isObject(el))
                el.parentNode.removeChild(el);
            else
                $d(el).parentNode.removeChild($d(el));

        }

    }

//    function isTime(time){
//
//        return(/^([0-9_\-\+]+(\:))*[0-9_\-]{2,10}$/i).test(time);
//
//    }

//    function isPhone(phone){
//
//        return(/^([0-9_-]+\+)*[0-9_\-]{7,20}$/i).test(phone);
//
//    }

    function isEmail(email){

        return(/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i).test(email);

    }

    function get_host_name_from_link(link) {

        let list=String(link).match(/\w+\.\w+/ig);

        if(list===null)
            return null;

        return list[0];

    }

    function getLocationList(href) {
        var match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
        return match && {
            href: href,
            protocol: match[1],
            host: match[2],
            hostname: match[3],
            port: match[4],
            pathname: match[5],
            search: match[6],
            hash: match[7]
        }
    }

    function isNameAndSurname(name){

        return name.match(/^[A-Za-zА-Яа-яё\-]+\s+[A-Za-zА-Яа-яё\-]+$/i)!==null;
        // return name.search(/^[A-Za-zА-Я\s*Ёа-яё-]*$/g)===0;

    }

    function getCharFromEventKey(event){

        if(event.which===null){

            if(event.keyCode<32)
                return null;

            return String.fromCharCode(event.keyCode);

        }

        if(
              event.which!==0
            &&event.charCode!==0
        ){

            if(event.which<32)
                return null;

            return String.fromCharCode(event.which);

        }

        if(event.keyCode<32)
            return null;

        return String.fromCharCode(event.keyCode);

    }

    function isLogin(login){

        return(/^([a-z0-9_\-])*[a-z0-9_\-]$/i).test(login);

    }

    function isArray(obj){

        var obj_index_of=obj.constructor.toString().indexOf("Array");

        return!(obj_index_of==-1);

    }

    function isFunction(obj){

        return typeof(obj)=='function';

    }

    function isObject(obj){

        return typeof(obj)=='object';

    }

    function isNumber(val){

        //return typeof(v)=='number';

        return(/^[0-9]*$/i).test(val);

    }

    function isCarNumber(val){

        return(/^[etyopahkxcbmукенхваросмт]{1}[0-9]{3,4}[etyopahkxcbmукенхваросмт]{2}[0-9]{2,3}$/i).test(val);

    }

    function isset(el){

        return (el!=null&&el!=undefined);

    }

    function is_undefined(el){

        return el==undefined;

    }

    function is_null(el){

        return el==null||el==='null';

    }

    function empty(el){

        return(
              !isset(el)
            ||el==0
            ||el===''
        );

    }

    function isString(data){

        return typeof(data)==='string';

    }

    function isBoolean(data){

        return typeof(data)==='boolean';

    }

    function is_date(date){

        var date_test=new Date();

        return !isNaN(date_test.parse(date));

    }

    function is_ip_address(data){

        if(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(data))
            return true;

        return false;

    }

    function is_camera_rtsp_link(data){

        if(/^rtsp:\/\/[a-z0-9]*\:[0-9]*@(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/.test(data))
            return true;

        return false;

    }

    var count=function(data){

        return data.length;

    };

    function getKey(e){

        if(!e)e=$w().event;

        return e.keyCode;

    }

//    function uniDecode(data){
//
//        return decodeURIComponent(data);
//
//    }

    function includeScript(src,callback){

        let  head                   =document.getElementsByTagName('head')[0]
            ,script_list            =head.getElementsByTagName('script')
            ,script                 =addElement({
                'tag':'script',
                'type':'text/javascript'
            })
            ,isset_script           =false
            ,src_without_query      =get_link_without_query(src)
            ,i;

        for(i=0;i<count(script_list);i++)
            if(get_link_without_query(script_list[i].getAttribute('src'))===src_without_query){

                isset_script=true;

                break;

            }

        if(!isset_script){

            if(isset(callback)){

                var userAgent=$n().userAgent.toLowerCase();

                if(/msie/.test(userAgent)&&!/opera/.test(userAgent))
                    listener.set(script,'readystatechange',function(){

                        eval(callback)();

                    });
                else{

                    listener.set(script,'error',function(){

                        trace('Load script ERROR: '+src);
                        trace('Restart loading: '+src);

                        setTimeout(function(){

                            trace('Restarted loading: '+src);

                            includeScript(src,callback);

                        },100);


                    });
                    listener.set(script,'load',function(){

                        eval(callback)();

                    });

                }

            }

            script.src=src;

            head.appendChild(script);

        }
        else if(isset(callback))
            eval(callback)();

    }

    function includeStyle(src,callback){

        let  head                   =document.getElementsByTagName('head')[0]
            ,script_list            =head.getElementsByTagName('link')
            ,script                 =addElement({
                'tag':'link',
                'type':'text/css',
                'rel':'stylesheet',
                'media':'all',
                'href':src
            })
            ,isset_script           =false
            ,src_without_query      =get_link_without_query(src)
            ,i;

        for(i=0;i<count(script_list);i++)
            if(get_link_without_query(script_list[i].getAttribute('href'))===src_without_query){

                isset_script=true;

                break;

            }

        if(!isset_script){

            if(isset(callback)){

                var userAgent=$n().userAgent.toLowerCase();

                if(/msie/.test(userAgent)&&!/opera/.test(userAgent))
                    listener.set(script,'readystatechange',function(){

                        eval(callback)();

                    });
                else{

                    listener.set(script,'error',function(){

                        trace('Load CSS ERROR: '+src);
                        trace('Restart loading: '+src);

                        setTimeout(function(){

                            trace('Restarted loading CSS: '+src);

                            includeScript(src,callback);

                        },100);


                    });
                    listener.set(script,'load',function(){

                        eval(callback)();

                    });

                }

            }

            script.src=src;

            head.appendChild(script);

        }
        else if(isset(callback))
            eval(callback)();

    }

    function addElement(data){

        if(data['tag']!=undefined){

            var  el=window.document.createElement(data['tag'])
                ,key
                ,el_ID;

            for(key in data){

                el_ID=key;

                if(isset(el_ID)){

                    if(key!='tag'){

                        if(key=='inner')
                            el.innerHTML=data[el_ID];
                        else
                            el.setAttribute(el_ID,data[el_ID]);

                    }

                }

            }

            return el;

        }
        else
            return false;

    }

    function uniEncode(data){

        return encodeURIComponent(data);

    }

    function uniDecode(data){

        return decodeURIComponent(data);

    }

    function jsonDecode(data){

        var data_object=eval('('+data+')');

        if(isset(data_object['error'])){

            if(data_object['error']&&data_object['redir'])
                return false;

            return data_object;

        }

        return data_object;

    }

    function jsonEncode(mixed_val){
        
        var retVal, json=this.window.JSON;
        try {
            if (typeof json === 'object' && typeof json.stringify === 'function'){
                retVal=json.stringify(mixed_val); // Errors will not be caught here if our own equivalent to resource
                //  (an instance of PHPJS_Resource) is used
                if (retVal === undefined){
                    throw new SyntaxError('json_encode');
                }
                return retVal;
            }

            var value=mixed_val;

            var quote=function(string){
                var escapable =
                    /[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
                var meta={ // table of character substitutions
                    '\b': '\\b',
                    '\t': '\\t',
                    '\n': '\\n',
                    '\f': '\\f',
                    '\r': '\\r',
                    '"': '\\"',
                    '\\': '\\\\'
                };

                escapable.lastIndex=0;
                return escapable.test(string) ? '"' + string.replace(escapable, function(a){
                    var c=meta[a];
                    return typeof c === 'string' ? c : '\\u' + ('0000' + a.charCodeAt(0)
                        .toString(16))
                        .slice(-4);
                }) + '"' : '"' + string + '"';
            };

            var str=function(key, holder){
                var gap='';
                var indent='    ';
                var i=0; // The loop counter.
                var k=''; // The member key.
                var v=''; // The member value.
                var length=0;
                var mind=gap;
                var partial=[];
                var value=holder[key];

                // If the value has a toJSON method, call it to obtain a replacement value.
                if (value && typeof value === 'object' && typeof value.toJSON === 'function'){
                    value=value.toJSON(key);
                }

                // What happens next depends on the value's type.
                switch (typeof value){
                    case 'string':
                        return quote(value);

                    case 'number':
                        // JSON numbers must be finite. Encode non-finite numbers as null.
                        return isFinite(value) ? String(value) : '0';

                    case 'boolean':
                    case '0':
                        // If the value is a boolean or null, convert it to a string. Note:
                        // typeof null does not produce '0'. The case is included here in
                        // the remote chance that this gets fixed someday.
                        return String(value);

                    case 'object':
                        // If the type is 'object', we might be dealing with an object or an array or
                        // null.
                        // Due to a specification blunder in ECMAScript, typeof null is 'object',
                        // so watch out for that case.
                        if (!value){
                            return '0';
                        }
                        if ((this.PHPJS_Resource && value instanceof this.PHPJS_Resource) || (window.PHPJS_Resource &&
                            value instanceof window.PHPJS_Resource)){
                            throw new SyntaxError('json_encode');
                        }

                        // Make an array to hold the partial results of stringifying this object value.
                        gap += indent;
                        partial=[];

                        // Is the value an array?
                        if (Object.prototype.toString.apply(value) === '[object Array]'){
                            // The value is an array. Stringify every element. Use null as a placeholder
                            // for non-JSON values.
                            length=value.length;
                            for(i=0; i < length; i += 1){
                                partial[i]=str(i, value) || '0';
                            }

                            // Join all of the elements together, separated with commas, and wrap them in
                            // brackets.
                            v=partial.length === 0 ? '[]' : gap ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind +
                                ']' : '[' + partial.join(',') + ']';
                            gap=mind;
                            return v;
                        }

                        // Iterate through all of the keys in the object.
                        for(k in value){
                            if (Object.hasOwnProperty.call(value, k)){
                                v=str(k, value);
                                if (v){
                                    partial.push(quote(k) + (gap ? ': ' : ':') + v);
                                }
                            }
                        }

                        // Join all of the member texts together, separated with commas,
                        // and wrap them in braces.
                        v=partial.length === 0 ? '{}' : gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' :
                            '{' + partial.join(',') + '}';
                        gap=mind;
                        return v;
                    case 'undefined':
                    // Fall-through
                    case 'function':
                    // Fall-through
                    default:
                        throw new SyntaxError('json_encode');
                }
            };

            // Make a fake root object containing our value under the key of ''.
            // Return the result of stringifying the value.
            return str('', {
                '': value
            });

        } catch (err){ // Todo: ensure error handling above throws a SyntaxError in all cases where it could
            // (i.e., when the JSON global is not available and there is an error)
            if (!(err instanceof SyntaxError)){
                throw new Error('Unexpected error type in json_encode()');
            }
            this.php_js=this.php_js || {};
            this.php_js.last_error_json=4; // usable by json_last_error()
            return null;
        }
    }

    var winSize={
        'scrWidth':null,
        'scrHeight':null,
        'pageWidth':null,
        'pageHeight':null,
        'set':function(){

            winSize.scrWidth    =screen.width;
            winSize.scrHeight   =screen.height;

            if($w()['innerHeight']&&$w()['scrollMaxY']){

                winSize.xScroll    =$w()['innerWidth']+$w()['scrollMaxX'];
                winSize.yScroll    =$w()['innerHeight']+$w()['scrollMaxY'];

            }
            else if($b().scrollHeight>$b().offsetHeight){

                winSize.xScroll=$b().scrollWidth;
                winSize.yScroll=$b().scrollHeight;

            }
            else{

                winSize.xScroll=$b().offsetWidth;
                winSize.yScroll=$b().offsetHeight;

            }

            if(self['innerHeight']){

                if(window.document.documentElement.clientWidth)
                    this.winWidth=window.document.documentElement.clientWidth;
                else
                    this.winWidth=self['innerWidth'];

                this.winHeight=self['innerHeight'];

            }
            else if(window.document.documentElement&&window.document.documentElement.clientHeight){

                winSize.winWidth    =window.document.documentElement.clientWidth;
                winSize.winHeight   =window.document.documentElement.clientHeight;

            }
            else if($b()){

                winSize.winWidth    =$b().clientWidth;
                winSize.winHeight   =$b().clientHeight;

            }

            if(winSize.yScroll<winSize.winHeight)
                winSize.pageHeight=winSize.winHeight;
            else
                winSize.pageHeight=winSize.yScroll;

            if(winSize.xScroll<winSize.winWidth)
                winSize.pageWidth=winSize.xScroll;
            else
                winSize.pageWidth=winSize.winWidth;

            this.getCenter();

        },
        'checkBrowserSize':{
            'play':false,
            'winWidth':null,
            'winHeight':null,
            'func':function(){},
            'options':function(func){

                if(typeof func==='function')winSize.checkBrowserSize.func=func;

                winSize.checkBrowserSize.play=true;
                winSize.checkBrowserSize.start();

            },
            'start':function(){

                if(this.play){

                    winSize.set();

                    if(winSize.winWidth!=winSize.checkBrowserSize.winWidth||winSize.winHeight!=winSize.checkBrowserSize.winHeight&&isset(winSize.checkBrowserSize.winHeight)&&isset(winSize.checkBrowserSize.winWidth)){

                        winSize.checkBrowserSize.winWidth=winSize.winWidth;
                        winSize.checkBrowserSize.winHeight=winSize.winHeight;
                        winSize.getCenter();

                        if(isset(winSize.checkBrowserSize.func))eval(winSize.checkBrowserSize.func());

                    }else if(isset(winSize.checkBrowserSize.winHeight)&&isset(winSize.checkBrowserSize.winWidth)){

                        winSize.checkBrowserSize.winWidth=winSize.winWidth;
                        winSize.checkBrowserSize.winHeight=winSize.winHeight;

                    }

                    setTimeout(function(){winSize.checkBrowserSize.start()},40);

                }

            }
        },
        'getCenter':function(){

            if(!isset(this.winWidth)||!isset(this.winHeight))this.set();

            this.cWidth=this.winWidth/2;
            this.cHeight=this.winHeight/2;

            return{'cWidth':this.cWidth,'cHeight':this.cHeight};

        }
    };

    var setUrl=function(title,lnk1){

        window.document.title=title;

        $w()['history'].pushState(title,title,lnk1);

    };

    var OS={
        'isMobile':null,
        'os':null,
        'getOS':function(){

            if(!isset(this.os)){

                if(navigator.userAgent.indexOf('iPhone')!=-1)
                    this.os='iPhone';
                else if(navigator.userAgent.indexOf('iPad')!=-1)
                    this.os='iPad';
                else if(navigator.userAgent.indexOf('Android')!=-1)
                    this.os='Android';
                else{

                    var  userag     =navigator.userAgent.toLowerCase()
                        ,isAndroid  =userag.indexOf("android")>-1;

                    this.os=isAndroid?'Android':null;

                }

                return this.os;

            }
            else
                return this.os;

        },
        'getMobile':function(){

            if(!isset(this.isMobile)){

                if(!isset(this.os))
                    this.getOS();

                this.isMobile=(this.os=='iPad'||this.os=='iPhone'||this.os=='Android')

            }

            return this.isMobile;

        }
    };

    OS.getMobile();

    if(document.getElementsByClassName){

        getElementsByClass=function(node,classList){

            return(node||document).getElementsByClassName(classList);

        }

    }
    else{

        getElementsByClass=function(node_el,classList){

            var  node        =node_el||document
                ,list        =node.getElementsByTagName('*')
                ,length      =list.length
                ,classArray  =classList.split(/\s+/)
                ,classes     =classArray.length
                ,result      =[]
                ,i
                ,j;

            for(i=0;i<length;i++){

                for(j=0;j<classes;j++){

                    if(list[i].className.search('\\b'+classArray[j]+'\\b')!=-1){

                        result.push(list[i]);

                        break;

                    }

                }

            }

            return result;

        }

    }

    var elementSize={
        'width':function(id){

            if(typeof id==='object'){

                if(isset(id))
                    return id.clientWidth||id.offsetWidth;
                else
                    return false;

            }
            else{

                if(isset($d(id)))
                    return $d(id).clientWidth||$d(id).offsetWidth;
                else
                    return false;

            }

        },
        'height':function(id){

            if(typeof id==='object'){

                if(isset(id))
                    return id.clientHeight||id.offsetHeight;
                else
                    return false;

            }
            else{

                if(isset($d(id)))
                    return $d(id).clientHeight||$d(id).offsetHeight;
                else
                    return false;

            }

        },
        'getSize':function(obj){

            return{
                'w':this.width(obj),
                'h':this.height(obj)
            };

        }

    };

    var getObjectLength=function(list){

        var  len        =0
            ,key;

        for(key in list)
            len++;

        return len;

    };

    var browser={
        'name'          :null,
        'version'       :null,
        'type'          :null,
        'data':{
            'Opera'     :'o',
            'Firefox'   :'moz',
            'Chrome'    :'webkit',
            'Safari'    :'webkit',
            'MSIE'      :''
        },
        'getBrowser':function(chrAftervertex){

            if(!isset(this.name)&&!isset(this.version)){

                var  outputData
                    ,UA             =$n().userAgent
                    ,OperaB         =/Opera[ \/]+\w+\.\w+/i
                    ,OperaV         =/Version[ \/]+\w+\.\w+/i
                    ,FirefoxB       =/Firefox\/\w+\.\w+/i
                    ,ChromeB        =/Chrome\/\w+\.\w+/i
                    ,SafariB        =/Version\/\w+\.\w+/i
                    ,IEB            =/MSIE *\d+\.\w+/i
                    ,SafariV        =/Safari\/\w+\.\w+/i
                    ,browser        =new Array()
                    ,browserSplit   =/[ \/\.]/i
                    ,OperaV         =UA.match(OperaV)
                    ,Firefox        =UA.match(FirefoxB)
                    ,Chrome         =UA.match(ChromeB)
                    ,Safari         =UA.match(SafariB)
                    ,SafariV        =UA.match(SafariV)
                    ,IE             =UA.match(IEB)
                    ,Opera          =UA.match(OperaB)
                    ,browser;

                if((!Opera=='')||(!OperaV==''))
                    browser[0]=OperaV[0].replace(/Version/,"Opera");
                else{

                    if(!Opera=="")
                        rowser[0]=Opera[0];
                    else{

                        if(!IE=="")
                            browser[0]=IE[0];
                        else{

                            if(!Firefox=="")
                                browser[0]=Firefox[0];
                            else{

                                if(!Chrome=="")
                                    browser[0]=Chrome[0];
                                else if((!Safari=="")&&(!SafariV==""))
                                    browser[0]=Safari[0].replace("Version","Safari")

                            }

                        }

                    }

                }

                if(browser[0]!=null)
                    outputData=browser[0].split(browserSplit);

                if((chrAftervertex==null)&&(outputData!=null)){

                    chrAftervertex   =outputData[2].length;
                    outputData[2]   =outputData[2].substring(0,chrAftervertex);
                    this.name       =outputData[0];
                    this.version    =outputData[1];
                    this.type       =this.data[this.name];

                }
                else{

                    this.name       =null;
                    this.version    =null;
                    this.type       =null;

                }

            }

            return{'name':this.name,'version':this.version,'type':this.type}

        }

    };

    browser.getBrowser();

    var listener={
        'eventData':[],
        'set':function(instance,eventName,listener){

            if(instance!=null){

                var listenerFn=listener;

                if(instance.addEventListener)
                    instance.addEventListener(eventName,listenerFn,false);
                else if(instance.attachEvent){

                    listenerFn=function(){

                        listener(window.event);

                    };

                    instance.attachEvent("on"+eventName,listenerFn);

                }
                else throw new Error("Event registration not supported");

                var event={
                    instance    :instance,
                    name        :eventName,
                    listener    :listenerFn
                };

                // this.eventData.push(event);

                return event;

            }

            return false;

        },
        'remove':function(event){

            var  instance   =event.instance
                ,i          =0
                ,q          =0;

            if(!isArray(event)){

                if(instance.removeEventListener)
                    instance.removeEventListener(event.name,event.listener,false);
                else if(instance.detachEvent)
                    instance.detachEvent("on"+event.name,event.listener);

                for(i=0;i<this.eventData.length;i++){

                    if(this.eventData[i]==event){

                        this.eventData.splice(i,1);

                        break;

                    }

                }

            }
            else{

                for(q=0;q<event.length;q++){

                    instance=event[q].instance;

                    if(instance.removeEventListener)
                        instance.removeEventListener(event[q].name,event[q].listener,false);
                    else if(instance.detachEvent)
                        instance.detachEvent("on"+event[q].name,event[q].listener);

                    for(i=0;i<this.eventData.length;i++)
                        if(this.eventData[i]==event[q]){

                            this.eventData.splice(i,1);

                            break;

                        }

                }

            }

        },
        'removeAll':function(data){

            if(data==undefined&&isArray(data)){

                while(count(listener.eventData)>0)this.remove(listener.eventData[0]);

            }
            else while(count(data)>0)this.remove(data[0]);

        },
        'removeData':function(data){

            for(var i=0;i<count(data);i++)
                listener.remove(data[i]);

        }

    };

    var mouse={
        'eventData' :{},
        'data'      :{},
        'getPosition':function(e){

            e=e||window.event;

            if(e.pageX==null&&e.clientX!=null){

                var  html   =window.document.documentElement
                    ,body   =$b();

                e.pageX     =e.clientX+(html&&html.scrollLeft||body&&body.scrollLeft||0)-(html.clientLeft||0);
                e.pageY     =e.clientY+(html&&html.scrollTop||body&&body.scrollTop||0)-(html.clientTop||0);

            }

            if(!e.which&&e.button)
                e.which=e.button&1?1:(e.button&2?3:(e.button&4?2:0));

            return{
                'x':e['pageX'],
                'y':e['pageY']
            };

        },
        'start':function(el){

            if(!isset(this.eventData[el]))this.eventData[el]=[];

            this.eventData[el].push(listener.set(el,'mousemove',function(e){mouse.data[this]=mouse.getPosition(e)}));

        },
        'stop':function(el){

            if(isset(this.eventData[el]))
                listener.remove(this.eventData[el]);

        },
        'remove':function(){

            var key;

            for(key in this.eventData)
                listener.remove(this.eventData[key]);

        }

    };

    var get_document_scroll_position=function(){

        return window.pageYOffset||document.documentElement.scrollTop;

    };

    var getElementPosition=function(el){

        function getOffsetRect(elem){

            var  box            =elem.getBoundingClientRect()
                ,body           =document.body
                ,docElem        =document.documentElement
                ,scrollTop      =window.pageYOffset||docElem.scrollTop||body.scrollTop
                ,scrollLeft     =window.pageXOffset||docElem.scrollLeft||body.scrollLeft
                ,clientTop      =docElem.clientTop||body.clientTop||0
                ,clientLeft     =docElem.clientLeft||body.clientLeft||0
                ,top            =box.top+scrollTop-clientTop
                ,left           =box.left+scrollLeft-clientLeft;

            return{
                'top'   :Math.round(top),
                'left'  :Math.round(left)
            };

        }

        function getOffsetSum(elem){

            var  top    =0
                ,left   =0;

            while(elem){

                top     =top+parseFloat(elem.offsetTop);
                left    =left+parseFloat(elem.offsetLeft);
                elem    =elem.offsetParent;

            }

            return{
                'top'   :parseInt(Math.round(top)),
                'left'  :parseInt(Math.round(left))
            };

        }

        if(el.getBoundingClientRect)
            return getOffsetRect(el);

        return getOffsetSum(el);

    };

    var domLoad={
        'load':false,
        'set':function(func){

            if(window.document.addEventListener)
                window.document.addEventListener('DOMContentLoaded',func,false);
            else if(window.document.attachEvent){

                if(
                      window.document.documentElement.doScroll
                    &&$w()==$w().top
                ){

                    (

                        function(){

                            try{

                                window.document.documentElement.doScroll('left');

                                func();

                            }
                            catch(e){

                                setTimeout(arguments.callee,10);

                            }

                        }

                    )();

                }

            }

            if($w().attachEvent)
                $w().attachEvent('onload',func);
            else if($w().addEventListener)
                $w().addEventListener('load',func,false);

        }

    };

    var fullScreen={
        'full':false,
        'eventData':null,
        'test':function(){

            if((window.document.fullScreenElement&&window.document.fullScreenElement!==null)||(!window.document.mozFullScreen&&!window.document.webkitIsFullScreen)){

                this.full=false;

                return false;

            }
            else{

                this.full=true;

                return true;

            }

        },
        'options':function(bool,func){

            fullScreen.eventData=listener.set($d('all-doc'),'keypress',function(event){fullScreen.onEscape(event)});

            var  el
                ,rfs;

            if(bool){

                el=window.document.documentElement;

                fullScreen.full     =true;
                rfs                 =el.requestFullScreen||el.webkitRequestFullScreen||el.mozRequestFullScreen;

            }
            else{

                el                  =window.document;
                fullScreen.full     =false;
                rfs                 =el.exitFullscreen||el.webkitCancelFullScreen||el.mozCancelFullScreen||el.cancelFullScreen;

            }

            if(OS.isMobile){/**/}
            else{

                if(typeof rfs!="undefined"&&rfs)
                    rfs.call(el);
                else if(typeof $w().ActiveXObject!="undefined"){

                    var wscript=new ActiveXObject("WScript.Shell");

                    if(wscript!=null)
                        wscript.SendKeys("{F11}");

                }

            }

            if(isset(func))
                func();

        },
        'onEscape':function(e){

            var key_num=getKey(e);

        }
    };

    var scrollElement={
        'play'      :false,
        'step'      :1.8,
        'data'      :{},
        'eventData' :{},
        'options':function(data){

            var eventId;

            if(isset(data['event'])){

                var n=0;

                for(var key in data){

                    if(key!='event')n++;

                }

                do{

                    eventId=parseInt(Math.random()/Math.random()*12345);

                }while(isset(this.eventData[eventId]));

                this.eventData[eventId]={
                    'event':data['event'],
                    'count':n,
                    'complete':0
                };

            }

            for(var key in data){

                if(key!='event'&&isset($d(key))){

                    if(!isset(this.data[key]))this.data[key]=data[key];

                    this.data[key]['reset']     =true;
                    this.data[key]['play']      =true;

                    if(isset(eventId)){

                        if(!isset(this.data[key]['eventId']))this.data[key]['eventId']=[];

                        this.data[key]['eventId'].push(eventId);

                    }

                }

            }

            if(this.play)this.replay=true;

            if(!this.play){

                this.play       =true;
                this.replay     =false;

                browser.getBrowser();
                this.start();

            }

        },
        'start':function(){

            var  deg
                ,el
                ,allEl
                ,countEl    =0
                ,rReset     =false
                ,step;

            for(var key in this.data){

                if(isset($d(key))){

                    if(this.data[key]['play']){

                        var end=0;

                        countEl++;

                        if(isset(this.data[key]['step']))
                            step=this.data[key]['step'];
                        else
                            step=this.step;

                        if(isset(this.data[key]['w'])){

                            if(this.data[key]['w']==$d(key).scrollLeft){

                                end++;

                            }
                            else{

                                var  delta      =this.data[key]['w']-$d(key).scrollLeft
                                    ,sL         =0
                                    ,fix        =0;

                                sL=delta/step;

                                if(sL>=1||sL<=-1)
                                    fix=$d(key).scrollLeft+sL;
                                else
                                    fix=this.data[key]['w'];

                                if(this.data[key]['w']==fix)end++;

                                $d(key).scrollLeft=fix;

                            }

                        }
                        else end++;

                        if(isset(this.data[key]['h'])){

                            if(this.data[key]['h']==$d(key).scroll_top){

                                end++;

                            }
                            else{

                                var  delta      =this.data[key]['h']-$d(key).scrollTop
                                    ,sT         =0
                                    ,fix        =0;

                                sT=delta/step;

                                if(sT>=1||sT<=-1)fix=$d(key).scrollTop+sT;
                                else fix=this.data[key]['h'];

                                if(this.data[key]['h']==fix)end++;

                                if(fix==0)console.log('SCROLL_Y: '+0+' KEY: '+key);

                                $d(key).scrollTop=fix;

                            }

                        }
                        else
                            end++;

                        if(end==2){

                            this.data[key]['play']=false;

                            if(isset(this.data[key]['event'])){

                                this.data[key]['drop']=true;

                                eval(this.data[key]['event']());

                            }

                            if(isset(this.data[key]['eventId'])){

                                for(var int in this.data[key]['eventId']){

                                    if(isset(this.data[key]['eventId'][int])){

                                        if(isset(this.eventData[this.data[key]['eventId'][int]])){

                                            this.eventData[this.data[key]['eventId'][int]]['complete']++;

                                            if(this.eventData[this.data[key]['eventId'][int]]['complete']==this.eventData[this.data[key]['eventId'][int]]['count']){

                                                if(isset(this.eventData[this.data[key]['eventId'][int]]['event'])){

                                                    if(typeof this.eventData[this.data[key]['eventId'][int]]['event']=='function'){

                                                        eval(this.eventData[this.data[key]['eventId'][int]]['event']());

                                                    }

                                                }

                                            }

                                        }

                                    }

                                }

                            }

                            if(!isset(this.data[key]['reset'])||!this.data[key]['reset'])delete this.data[key];
                            else{

                                this.data[key]['reset']     =false;
                                rReset                      =true;
                                this.data[key]['play']      =true;

                            }

                        }

                    }

                }

            }

            if(this.play||rReset||this.replay){

                this.replay=false;

                setTimeout(function(){

                    scrollElement.start();

                },40);

            }

            else if(countEl==0)this.play=false;

        }
    };

    var send=function(config){

        var timestamp_start=new Date().getTime();

        trace('>>> SEND CONFIG');
        trace(config);

        var  j
            ,errorNum=false
            ,fileScript;

        try{

            j=new ActiveXObject("Msxml2.XMLHTTP");

        }
        catch(e){

            try{

                j=new ActiveXObject("Microsoft.XMLHTTP");

            }
            catch(E){

                j=false;

            }

        }

        if(!j&&typeof XMLHttpRequest!='undefined'){

            j=new XMLHttpRequest();

        }

        if(!j)
            alert('Error created sending object');
        else{

            if(isset(config['onLoad']))
                eval(config['onLoad']());

            if(isset(config['actionName']))
                fileScript=config['scriptPath']+'?action='+config['actionName'];
            else
                fileScript=config['scriptPath'];

            if(isset(config['postData']))
                j.open('POST',fileScript,true);
            else
                j.open('GET',fileScript,true);

            j.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");

            trace(document.cookie);

            j.onreadystatechange=function(){

                try{

                    if(j.readyState==1||j.readyState==2||j.readyState==3){

                        trace('>>>> API: Ready State: '+j.readyState+' Status: '+j.status+ ' : '+(isset(config['scriptPath'])?config['scriptPath']:''));

                    }
                    else if(j.readyState==4){

                        if(j.status>=500){

                            trace('RESEND with header status: '+j.status);

                            re_send(j,config);

                        }
                        else if(j.status==200){

//                            trace('');
//                            trace('');
                            trace('>>>> API: send complete: '+ j.status+ ' : '+(isset(config['scriptPath'])?config['scriptPath']:''));
//                            trace('>>>>>>>>> headers: '+j.getAllResponseHeaders());
//                            trace('>>>>');
//                            trace('');
//                            trace('');

                            var  timestamp_finish   =new Date().getTime()
                                ,worktime           =timestamp_finish-timestamp_start;

                            trace('Worktime: '+worktime);

                            if(isset(config['onComplete']))
                                eval(config['onComplete'](j,worktime));

                            j.abort();

                        }
                        else{

                            trace('RESEND with header status: '+j.status);

                            re_send(j,config);

                        }

                    }

                }
                catch(e){

                    trace('>>>> API: repeat send query');

                    re_send(j,config);

                }

            };

            if(isset(config['postData']))
                j.send(config['postData']);
            else
                j.send();

        }

    };

    var re_send=function(j,config){

        //return false;

        setTimeout(function(){

            trace('');
            trace('');
            trace('>>>> RESET SEND: '+ j.status);
            trace('>>>>>>>>> headers: '+j.getAllResponseHeaders());
            trace('>>>>');
            trace('');
            trace('');

            j.abort();

            send(config);

        },5000);

    };

    var setTransform=function(key){

        if(isset($d(key))){

            switch(browser.type){

                case'webkit':{

                    $s(key).webkitTransform='translate3d(0,0,0)';

                    break;

                }

                case'moz':{

                    $s(key).mozTransform='translate3d(0,0,0)';

                    break;

                }

                case'o':{

                    $s(key).oTransform='translate3d(0,0,0)';

                    break;

                }

                default:{

                    $s(key).transform='translate3d(0,0,0)';

                    break;

                }

            }

        }

    };

    var dayZone={
        'day':false,
        'set':function(){

            var  date	=new Date().toLocaleTimeString()
                ,h		=date.split(':')[0]
                ,m		=date.split(':')[1]
                ,s		=date.split(':')[2];

            if((h>=20&&h<=23)||(h<=8&&h>=0)){

                dayZone.day=false;

                if(h<=23&&h>=20)delta=(24-h-1+8)*3600+(60-m-1)*60+(60-s-1);
                else if(h>=0&&h<=8)delta=(8-h-1)*3600+(60-m-1)*60+(60-s-1);

                delta   +=10;
                delta   *=1000;

                setTimeout(dayZone.set,delta);

            }else{

                dayZone.day =true;
                delta       =(20-h-1)*3600+(60-m-1)*60+(60-s-1);

                delta       +=10;
                delta       *=1000;

                setTimeout(dayZone.set,delta);

            }
        }
    };

    dayZone.set();

    var stripSlashes=function(data){

        if(data=='')
            return data;

        data+='';

        //data    =data.replace(/\\'/g,"'");
        //data    =data.replace(/\\"/g,"\"");

        //data    =data.replace(/'/g,'&apos;');
        //data    =data.replace(/\"/g,'&quot;');

        data=data
            .replace(/\'/g,"'")
            .replace(/’/g,"'")
            .replace(/\"/g,"\"")
            .replace(/”/g,"\"")
            .replace(/“/g,"\"")
            .replace(/\&/g, "&amp;")
            .replace(/\</g, "&lt;")
            .replace(/\>/g, "&gt;")
            .replace(/\"/g, "&quot;")
            .replace(/\'/g, "&#039;");

        return data;

    };

    var preg_match_all=function(pattern,string){

        var  list           =[]
            ,match;

        while((match=pattern.exec(string))!=null)
            list.push({
                'index' :match['index'],
                'match' :match[1]
            });

        return list;

    };

    var str_replace=function(content,needle,repl){

        if(isset(content)){

            var temp_content;

            do{

                temp_content    =content;
                content         =content.replace(needle,repl);

            }
            while(temp_content!=content);

        }

        return content;

    };

    var opaElement={
        'setOpacity':function(obj,val){

            if(isset(obj)){

                if(!+"\v1")
                    typeof obj.filters.alpha!='undefined'?obj.filters.alpha.opacity=val*100:obj.style.filter='alpha(opacity='+val*100+');';

                obj.style.opacity=val;

            }

        },
        'getOpacity':function(obj){

            if(isset(obj)){

                var result=window.getComputedStyle?window.getComputedStyle(obj,null).opacity:null;

                if(!+"\v1")
                    result=obj.filters.alpha?(obj.filters.alpha.opacity/100):1;

                return result;

            }
            else
                return null;

        }
    };

    var trace=function(data){

        // if(window.location.host!=='shluham.net')
            console.log(data);

    };

    var trace_worktime=function(worktime,data){

        if(isset(data['worktime'])){

            trace('Full worktime: '+worktime+' ms');
            trace('Server worktime: '+(data['worktime']*1000)+' ms');
            trace('Transfer worktime: '+(worktime-(data['worktime']*1000))+' ms');

            if(isset($d('worktime'))){

                var inner='';

                inner+='<div>Full worktime: '+worktime+' ms</div>';
                inner+='<div>Server worktime: '+(empty(data['time_used'])?'':(data['time_used']+'/'))+(data['worktime']*1000).toFixed(0)+' ms</div>';
                inner+='<div>Transfer worktime: '+(worktime-(data['worktime']*1000)).toFixed(0)+' ms</div>';

                $d('worktime').innerHTML=inner;

            }

        }

    };

    var get_query_get=function(name){

        return decodeURIComponent((new RegExp('[?|&]'+name+'='+'([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g,'%20'))||null;

    };

    var http_protocol={
        'protocol':'http',
        'init':function(){

            http_protocol.protocol=location.protocol==='https:'?'https':'http';

            return true;

        }
    };

    http_protocol.init();

    var get_link_without_query=function(link_string){

        link_string=str_replace(link_string,(http_protocol.protocol+'://'+String(document.location.hostname)),'');

        return decodeURIComponent(link_string).split("?")[0].split("#")[0];

    };

    var strtolower=function(data){

        return data.toLowerCase();

    };

    var implode=function(glue,pieces){

        return((pieces instanceof Array)?pieces.join(glue):pieces);

    };

    var base64={
        'encode':function(data){

            return btoa(data);

        },
        'decode':function(data){

            return atob(data);

        }
    };

    var setCookie=function(name,value,expires,path,domain,secure){

        document.cookie=name+"="+escape(value)+((expires) ? "; expires="+expires : "")+((path) ? "; path="+path : "")+((domain) ? "; domain=" + domain : "")+((secure) ? "; secure" : "");

    };

    var getCookie=function(name){

        var  cookie	 	=" "+document.cookie
            ,search		=" "+name+"="
            ,setStr 	=null
            ,offset 	=0
            ,end 		=0;

        if(cookie.length>0){

            offset=cookie.indexOf(search);

            if(offset!=-1){

                offset	+=search.length;
                end		=cookie.indexOf(";",offset);

                if(end==-1)end=cookie.length;

                setStr=unescape(cookie.substring(offset,end));

            }

        }

        return(setStr);

    };

    var get_date_time=function(timestamp){
        
        var  date       =new Date(timestamp*1000)
            //,months     =['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
            ,year       =date.getFullYear()
            ,month      ='0'+(date.getMonth()+1)
            ,day        ='0'+date.getDate()
            ,hour       ='0'+date.getHours()
            ,min        ='0'+date.getMinutes()
            ,sec        ='0'+date.getSeconds();

        return day.substr(-2)+'.'+month.substr(-2)+'.'+year+' '+hour.substr(-2)+':'+min.substr(-2)+':'+sec.substr(-2);

    };

    var get_time=function(timestamp){

        var  date       =new Date(timestamp*1000)
            ,hour       ='0'+date.getHours()
            ,min        ='0'+date.getMinutes()
            ,sec        ='0'+date.getSeconds();

        return hour.substr(-2)+':'+min.substr(-2)+':'+sec.substr(-2);

    };

    var get_hour=function(timestamp){

        var date=new Date(timestamp*1000);

        return date.getHours();

    };

    var get_date=function(timestamp){

        var  date       =new Date(timestamp*1000)
            //,months     =['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
            ,year       =date.getFullYear()
            ,month      ='0'+(date.getMonth()+1)
            ,day        ='0'+date.getDate();

        return day.substr(-2)+'.'+month.substr(-2)+'.'+year;

    };

    var get_date_value=function(timestamp){

        var  date       =new Date(timestamp*1000)
            //,months     =['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
            ,year       =date.getFullYear()
            ,month      ='0'+(date.getMonth()+1)
            ,day        ='0'+date.getDate();

        return year+'-'+month.substr(-2)+'-'+day.substr(-2);

    };

    var get_timestamp=function(date){

        if(empty(date))
            return 0;

        return  Math.ceil(new Date(date).getTime()/1000);

    };

    var get_timestamp_from_time=function(time){

        if(empty(time))
            return 0;

        let  time_list      =time.split(':')
            ,hour           =parseInt(time_list[0])*3600
            ,minute         =parseInt(time_list[1])*60;

        return hour+minute;

    };

    var get_time_from_timestamp=function(timestamp){

        let  hour           =Math.floor(parseInt(timestamp)/3600)
            ,minute         =Math.floor((parseInt(timestamp)-hour*3600)/60)
            ,time           =('0'+hour).substr(-2)+':'+('0'+minute).substr(-2);

        return time;

    };

    var array_search=function(needle,list,key){

        var i;

        if(isset(key)){

            for(i=0;i<list.length;i++){

                if(list[i][key]===needle){

                    return true;

                }

            }

        }
        else{

            for(i=0;i<list.length;i++)
                if(list[i]===needle)
                    return true;

        }

        return false;

    };

    var get_input_file_name=function(fullPath){

        if(fullPath){

            var  startIndex =(fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'))
                ,filename   =fullPath.substring(startIndex);

            if(filename.indexOf('\\')===0||filename.indexOf('/')===0)
                filename=filename.substring(1);

            return filename;

        }

        return false;

    };

    var get_file_exctension=function(file_path){

        return file_path.split('.').pop();

    };

    var get_time_zone=function(){

        var  date           =new Date()
            ,timezone       =date.getTimezoneOffset()/60;

        return timezone*-1;

    };

    function sha1(str){

        var hash;
        
        try{
            
            var  crypto     =require('crypto')
                ,sha1sum    =crypto.createHash('sha1');
            
            sha1sum.update(str);
            hash=sha1sum.digest('hex');
            
        }catch(e){
            hash=undefined;
        }

        if(hash!==undefined)
            return hash;

        var _rotLeft=function(n,s){

            var t4=(n<<s)|(n>>>(32-s));

            return t4;

        };

        var _cvtHex=function(val){

            var  str=''
                ,i
                ,v;

            for(i=7;i>=0;i--){

                v       =(val>>>(i*4))&0x0f;
                str     +=v.toString(16)
            }

            return str;

        };

        var  blockstart
            ,i
            ,j
            ,W=new Array(80)
            ,H0=0x67452301
            ,H1=0xEFCDAB89
            ,H2=0x98BADCFE
            ,H3=0x10325476
            ,H4=0xC3D2E1F0
            ,A
            ,B
            ,C
            ,D
            ,E
            ,temp;

        str=unescape(encodeURIComponent(str));

        var  strLen     =str.length
            ,wordArray  =[];

        for(i=0;i<strLen-3;i+=4){

            j=str.charCodeAt(i)<<24|str.charCodeAt(i+1)<<16|str.charCodeAt(i+2)<<8|str.charCodeAt(i+3);

            wordArray.push(j);

        }

        switch(strLen%4){

            case 0:{

                i=0x080000000

                break;

            }

            case 1:{

                i=str.charCodeAt(strLen-1)<<24|0x0800000;

                break;

            }

            case 2:{

                i=str.charCodeAt(strLen - 2)<<24|str.charCodeAt(strLen-1)<<16|0x08000;

                break;

            }

            case 3:{

                i=str.charCodeAt(strLen-3)<<24|str.charCodeAt(strLen-2)<<16|str.charCodeAt(strLen-1)<<8|0x80;

                break;

            }

        }

        wordArray.push(i);

        while((wordArray.length%16)!==14)
            wordArray.push(0);

        wordArray.push(strLen>>>29);
        wordArray.push((strLen<<3)&0x0ffffffff);

        for(blockstart=0;blockstart<wordArray.length;blockstart+= 16){

            for(i=0;i<16;i++)
                W[i]=wordArray[blockstart+i];

            for(i=16;i<=79;i++)
                W[i]=_rotLeft(W[i-3]^W[i-8]^W[i-14]^W[i-16],1);

            A   =H0;
            B   =H1;
            C   =H2;
            D   =H3;
            E   =H4;

            for(i=0;i<=19;i++){
                
                temp    =(_rotLeft(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
                E       =D;
                D       =C;
                C       =_rotLeft(B, 30);
                B       =A;
                A       =temp;
                
            }

            for(i=20; i <= 39; i++){
                
                temp    =(_rotLeft(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
                E       =D;
                D       =C;
                C       =_rotLeft(B, 30);
                B       =A;
                A       =temp;
                
            }

            for(i=40;i<=59;i++){
                
                temp    =(_rotLeft(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
                E       =D;
                D       =C;
                C       =_rotLeft(B,30);
                B       =A;
                A       =temp;
                
            }

            for(i=60;i<=79;i++){
                
                temp    =(_rotLeft(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
                E       =D;
                D       =C;
                C       =_rotLeft(B, 30);
                B       =A;
                A       =temp;
                
            }

            H0  =(H0 + A) & 0x0ffffffff;
            H1  =(H1 + B) & 0x0ffffffff;
            H2  =(H2 + C) & 0x0ffffffff;
            H3  =(H3 + D) & 0x0ffffffff;
            H4  =(H4 + E) & 0x0ffffffff;
            
        }

        temp=_cvtHex(H0) + _cvtHex(H1) + _cvtHex(H2) + _cvtHex(H3) + _cvtHex(H4);

        return temp.toLowerCase();

    }

    var get_url_param=function(parameterName){

        var  result     =null,
             tmp        =[];

        location.search
            .substr(1)
            .split("&")
            .forEach(function(item){

                tmp=item.split("=");

                if(tmp[0]===parameterName)
                    result=decodeURIComponent(tmp[1]);

            });

        return result;

    };

    var copy_to_buffer=function(containerid){

        var  elm        =document.getElementById(containerid)
            ,range
            ,selection;

        if(document.body.createTextRange){

            range=document.body.createTextRange();

            range.moveToElementText(elm);
            range.select();
            document.execCommand("Copy");

            return true;

        }
        else if(window.getSelection){

            selection   =window.getSelection();
            range       =document.createRange();

            range.selectNodeContents(elm);
            selection.removeAllRanges();
            selection.addRange(range);
            document.execCommand("Copy");

            return true;

        }

        return false;

    };

    var get_number_with_space=function(number){

        return String(number).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');

    };

    var get_number_with_space_to_4=function(number){

        return String(number).replace(/(\d)(?=(\d{4})+([^\d]|$))/g, '$1 ');

    };

    var prepare_image_orientation={
        'reset_orientation':function reset_orientation(srcBase64,srcOrientation,callback,index){

            var img=new Image();

            img.onload=function(){

                var  width      =img.width
                    ,height     =img.height
                    ,canvas     =document.createElement('canvas')
                    ,ctx        =canvas.getContext("2d");


                if(
                      4<srcOrientation
                    &&srcOrientation<9
                ){

                    canvas.width    =height;
                    canvas.height   =width;

                }
                else{

                    canvas.width      =width;
                    canvas.height     =height;

                }

                trace('orent: '+srcOrientation);

                switch(srcOrientation){

                    case 2:{

                        ctx.transform(-1, 0, 0, 1, width, 0);

                        break;

                    }

                    case 3:{

                        ctx.transform(-1, 0, 0, -1, width, height);

                        break;

                    }

                    case 4:{

                        ctx.transform(1, 0, 0, -1, 0, height);

                        break;

                    }

                    case 5:{

                        ctx.transform(0, 1, 1, 0, 0, 0);

                        break;

                    }

                    case 6:{

                        ctx.transform(0, 1, -1, 0, height, 0);

                        break;

                    }

                    case 7:{

                        ctx.transform(0, -1, -1, 0, height, width);

                        break;

                    }

                    case 8:{

                        ctx.transform(0, -1, 1, 0, 0, width);

                        break;

                    }

                    default:
                        break;

                }

                ctx.drawImage(img,0,0);
                callback(canvas.toDataURL(),index);

            };

            img.src=srcBase64;

        },
        'get_image_orientation':function(file,callback,index){

            trace('get img orient: '+index);

            var reader=new FileReader();

            reader.onload=function(event) {

                var  view       =new DataView(event.target.result)
                    ,length
                    ,offset
                    ,marker
                    ,little
                    ,tags
                    ,i;

                trace('get img orient onload: '+index);

                if (view.getUint16(0, false) != 0xFFD8)
                    return callback(file,-2,index);

                length  =view.byteLength;
                offset  =2;

                while(offset<length){

                    marker  =view.getUint16(offset,false);
                    offset  +=2;

                    if(marker==0xFFE1){

                        if(view.getUint32(offset+=2,false)!=0x45786966)
                            return callback(file,-1,index);

                        little      =view.getUint16(offset+=6,false)==0x4949;
                        offset      +=view.getUint32(offset+4,little);
                        tags        =view.getUint16(offset,little);
                        offset      +=2;

                        for(i=0;i<tags;i++)
                            if(view.getUint16(offset+(i*12),little)==0x0112)
                                return callback(file,view.getUint16(offset+(i*12)+8,little),index);

                    }
                    else if((marker & 0xFF00)!=0xFF00)
                        break;
                    else
                        offset+=view.getUint16(offset,false);

                }

                return callback(file,-1,index);

            };

            reader.readAsArrayBuffer(file.slice(0, 64 * 1024));

        },
        'get_image_with_true_orientation':function(file,callback,index){

            trace('Orient: '+index);

            prepare_image_orientation.get_image_orientation(file,function(file,orientation,index){

                trace('get img orient in function: '+index);

                var reader=new FileReader();

                reader.onload=(function(){

                    trace('get img orient in function on_load: '+index);

                    return function(e){

                        trace('get img orient in function on_load return: '+index);

                        prepare_image_orientation.reset_orientation(e.target.result,orientation,function(image_link,index){

                            trace('reset orientation: '+index);

                            callback(image_link,function(){},index);

                        },index);

                    };

                })(file,index);

                reader.readAsDataURL(file);

            },index);

        }
    };

    var re_captcha={
        'key':'6Lez3J4UAAAAAKzX2vcL3Gd_inzV55NQdtaT9d29',
        'callback':null,
        'action':null,
        'init':function(){},
        'check':function(action,callback){

            re_captcha.callback     =callback;
            re_captcha.action       =action;

            if(window.location.host!=='shluham.net')
                re_captcha.callback(-1);

            grecaptcha.ready(function(){

                grecaptcha.execute(re_captcha.key,{action:re_captcha.action}).then(function(token){

                    re_captcha.callback(token);

                });

            });

        }
    };


