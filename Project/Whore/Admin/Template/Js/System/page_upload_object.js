page_object.upload={
    'data':null,
    'file_item':null,
    // 'chunk_size':1024*128,
    'chunk_size':1024*1024*0.2,
    'work':false,
    'init':function(dialog_index,data){

        page_object.upload.controls.disable(dialog_index);

        var  i
            ,len
            ,limit      =isset(data['limit'])
                ?(
                    data['limit']<data['list'].length
                        ?data['limit']
                        :data['list'].length
                    )
                :data['list'].length
            ,list       =[]
            ,temp;

        if(isset(page_object.upload.data)){

            if(!isset(page_object.upload.data['on_change'])&&isset(data['on_change']))
                page_object.upload.data['on_change']=data['on_change'];

            if(!isset(page_object.upload.data['on_progress'])&&isset(data['on_progress']))
                page_object.upload.data['on_progress']=data['on_progress'];

            if(!isset(page_object.upload.data['on_uploaded'])&&isset(data['on_uploaded']))
                page_object.upload.data['on_uploaded']=data['on_uploaded'];

            if(!isset(page_object.upload.data['on_complete'])&&isset(data['on_complete']))
                page_object.upload.data['on_complete']=data['on_complete'];

            if(!isset(page_object.upload.data['on_limit'])&&isset(data['on_limit']))
                page_object.upload.data['on_limit']=data['on_limit'];

            if(!isset(page_object.upload.data['file_type'])&&isset(data['file_type']))
                page_object.upload.data['file_type']=data['file_type'];

            if(!isset(page_object.upload.data['limit'])&&isset(data['limit']))
                page_object.upload.data['limit']=data['limit'];

            if(!isset(page_object.upload.data['chunk_size'])&&isset(data['chunk_size']))
                page_object.upload.data['chunk_size']=data['chunk_size'];

            len=page_object.upload.data['list'].length;

            if(isset(data['limit']))
                if(data['limit']<=(page_object.upload.data['list'].length+data['list'].length))
                    if(isset(page_object.upload.data['on_limit']))
                        page_object.upload.data['on_limit']();

            page_object.upload.data['list']=page_object.upload.data['list'].concat(data['list']);

            if(isset(data['limit'])){

                limit=isset(data['limit'])?(data['limit']<page_object.upload.data['list'].length?data['limit']:page_object.upload.data['list'].length):page_object.upload.data['list'].length;

                page_object.upload.data['list']=page_object.upload.data['list'].slice(0,limit);

            }

            if(!page_object.upload.work){

                page_object.upload.work=true;

                page_object.upload.change(dialog_index,len);

            }

        }
        else{

            if(isset(data['limit']))
                if(data['limit']<=data['list'].length)
                    if(isset(data['on_limit']))
                        data['on_limit']();

            for(i=0;i<limit;i++){

                temp=data['list'][i];

                temp['complete']        =false;
                temp['remove']          =false;
                temp['percent']         =0;
                temp['wait']            =false;

                list.push(temp);

            }

            data['list']            =list;

            if(isset(dialog_index))
                data['dialog_index']=dialog_index;

            page_object.upload.work     =true;
            page_object.upload.data     =data;

            if(data['list'].length>0)
                page_object.upload.change(dialog_index,0);

        }

    },
    'change':function(dialog_index,index){

        if(isset(page_object.upload.data['list'][index]))
            if(page_object.upload.data['list'][index]['remove']||page_object.upload.data['list'][index]['complete']){

                if(isset(page_object.upload.data['list'][++index]))
                    page_object.upload.change(dialog_index,index);
                else
                    page_object.upload.complete(dialog_index);

            }
            else{

                page_object.upload.file_item                =page_object.upload.data['list'][index];
                page_object.upload.file_item['index']       =index;
                page_object.upload.file_item['file_ID']     =0;

                var  file_size              =page_object.upload.file_item['file']['size']
                    ,chunk_size             =page_object.upload.chunk_size
                    ,chunk_len              =Math.ceil(file_size/chunk_size);

                if(isset(page_object.upload.data['on_change']))
                    page_object.upload.data['on_change'](page_object.upload.file_item['index']);

                if(file_size<chunk_size)
                    chunk_size=file_size;

                page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_index']=0;
                page_object.upload.file_item['chunk_index']=0;

                page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_len']=chunk_len;
                page_object.upload.file_item['chunk_len']=chunk_len;

                page_object.upload.data['list'][page_object.upload.file_item['index']]['wait']=true;
                page_object.upload.file_item['wait']=true;

                page_object.upload.file_slice(dialog_index,0,chunk_len,file_size,0,chunk_size);

            }
        else
            page_object.upload.complete(dialog_index);

    },
    'file_slice':function(dialog_index,chunk_index,chunk_len,file_size,start,end){

        if(isset(page_object.upload.file_item))
            if(page_object.upload.file_item['remove']||page_object.upload.file_item['complete']){

                if(!page_object.upload.file_item['remove'])
                    page_object.upload.file_item['index']++;

                if(page_object.upload.file_item['index']<page_object.upload.data['list'].length)
                    page_object.upload.change(dialog_index,page_object.upload.file_item['index']);
                else
                    page_object.upload.complete(dialog_index);

            }
            else{

                var  reader             =new FileReader()
                    ,chunk_size         =isset(page_object.upload.file_item['chunk_size'])?page_object.upload.file_item['chunk_size']:page_object.upload.chunk_size
                    ,file_slice         =page_object.upload.file_item['file'].slice(start,end)
                    ,file_mime_type     =page_object.upload.file_item['file'].type
                    ,chunk_blob
                    ,post               =''
                    ,file_content_type  =page_object.upload.data['file_type']
                    ,file_extension     =get_file_exctension(page_object.upload.file_item['file'].name)
                    ,loop               =true;

                if(file_size<chunk_size)
                    chunk_size=file_size;

                reader.onloadend=function(e){

                    if(e.target.readyState===FileReader.DONE){

                        chunk_blob=base64.encode(e.target.result);

                        if(end>file_size){

                            end         =file_size;
                            chunk_size  =file_size-start;
                            loop        =false;

                        }

                        post='';

                        post+='file_ID='+uniEncode(page_object.upload.file_item['file_ID']);
                        post+='&file_chunk_count='+uniEncode(chunk_len);
                        post+='&file_chunk_index='+uniEncode((chunk_index+1));
                        post+='&file_chunk_size='+uniEncode(chunk_size);
                        post+='&file_size='+uniEncode(file_size);
                        post+='&file_content_type='+uniEncode(file_content_type);
                        post+='&file_extension='+uniEncode(file_extension);
                        post+='&file_mime_type='+uniEncode(file_mime_type);

                        trace('postData: '+post);
                        trace('FILE INDEX: '+page_object.upload.file_item['index']);
                        trace('FILE SLICE: '+start+'/'+end+' file_size: '+file_size+' chunk_index: '+chunk_index+' chunk_len: '+chunk_len);

                        post+='&file_chunk='+uniEncode(chunk_blob);

                        send({
                            'scriptPath':'/api/json/upload_file_chunk',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                var  dataTemp
                                    ,data;

                                dataTemp        =j.responseText;
                                trace(dataTemp);
                                data            =jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                trace_worktime(worktime,data);

                                if(page_object.upload.file_item['remove']){

                                    if(isset(page_object.upload.data['list'][page_object.upload.file_item['index']]))
                                        page_object.upload.change(dialog_index,page_object.upload.file_item['index']);
                                    else
                                        page_object.upload.complete(dialog_index);

                                }
                                else
                                    if(!isset(data['error'])){

                                        page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_index']=chunk_index;
                                        page_object.upload.file_item['chunk_index']=chunk_index;

                                        page_object.upload.file_item['file_ID']=data['file_ID'];
                                        page_object.upload.data['list'][page_object.upload.file_item['index']]['file_ID']=data['file_ID'];

                                        page_object.upload.progress(dialog_index,end,file_size,chunk_index,chunk_len);

                                        if(data['complete']){
                                            
                                            if(isset(data['image_ID'])){

                                                page_object.upload.file_item['image_ID']            =data['image_ID'];
                                                page_object.upload.file_item['image_item_ID_list']  =data['image_item_ID_list'];
                                                page_object.upload.file_item['image_dir']           =data['image_dir'];

                                                if(isset(page_object.upload.data['image_index']))
                                                    page_object.upload.data['list'][page_object.upload.file_item['index']]['image_index']=page_object.upload.data['image_index'];

                                                page_object.upload.data['list'][page_object.upload.file_item['index']]['image_ID']              =data['image_ID'];
                                                page_object.upload.data['list'][page_object.upload.file_item['index']]['image_item_ID_list']    =data['image_item_ID_list'];
                                                page_object.upload.data['list'][page_object.upload.file_item['index']]['image_dir']             =data['image_dir'];
                                                
                                            }
                                            else if(isset(data['audio_ID'])){

                                                page_object.upload.file_item['audio_ID']            =data['audio_ID'];
                                                page_object.upload.file_item['audio_item_ID_list']  =data['audio_item_ID_list'];
                                                page_object.upload.file_item['audio_dir']           =data['audio_dir'];
                                            
                                                page_object.upload.data['list'][page_object.upload.file_item['index']]['audio_ID']              =data['audio_ID'];
                                                page_object.upload.data['list'][page_object.upload.file_item['index']]['audio_item_ID_list']    =data['audio_item_ID_list'];
                                                page_object.upload.data['list'][page_object.upload.file_item['index']]['audio_dir']             =data['audio_dir'];
                                                
                                            }
                                            else if(isset(data['video_ID'])){

                                                page_object.upload.file_item['video_ID']            =data['video_ID'];
                                                page_object.upload.file_item['video_path']          =data['video_path'];
                                            
                                                page_object.upload.data['list'][page_object.upload.file_item['index']]['video_ID']      =data['video_ID'];
                                                page_object.upload.data['list'][page_object.upload.file_item['index']]['video_path']    =data['video_path'];

                                            }
                                            
                                            page_object.upload.data['list'][page_object.upload.file_item['index']]['complete']=true;
                                            page_object.upload.file_item['complete']=true;

                                            page_object.upload.uploaded(dialog_index,page_object.upload.file_item['index']);

                                        }
                                        else{

                                            setTimeout(function(){

                                                start           +=chunk_size;
                                                end             +=chunk_size;
                                                chunk_index     ++;

                                                page_object.upload.file_slice(dialog_index,chunk_index,chunk_len,file_size,start,end);

                                            },10);

                                        }

                                    }

                            }
                        });

                        return true;

                    }

                };

                reader.readAsBinaryString(file_slice);

            }

    },
    'progress':function(dialog_index,end,file_size,chunk_index,chunk_len){

        if(!page_object.upload.file_item['remove'])
            if(isset(page_object.upload.data['list'][page_object.upload.file_item['index']])){

                page_object.upload.data['list'][page_object.upload.file_item['index']]['percent']=end/file_size;

                var wait=(chunk_index===(chunk_len-2));

                if(isset(page_object.upload.data['on_progress']))
                    page_object.upload.data['on_progress'](page_object.upload.file_item['index'],wait);

            }

    },
    'uploaded':function(dialog_index){

        if(page_object.upload.file_item['remove']){

            if(isset(page_object.upload.data['list'][page_object.upload.file_item['index']]))
                page_object.upload.change(dialog_index,page_object.upload.file_item['index']);
            else
                page_object.upload.complete(dialog_index);

        }
        else{

            var list=page_object.upload.data['list'];

            page_object.upload.data['list'][page_object.upload.file_item['index']]['wait']      =false;
            page_object.upload.data['list'][page_object.upload.file_item['index']]['complete']  =true;

            page_object.upload.file_item['wait']        =false;
            page_object.upload.file_item['complete']    =true;

            if(isset(page_object.upload.data['on_uploaded']))
                page_object.upload.data['on_uploaded'](page_object.upload.file_item['index']);

            if(++page_object.upload.file_item['index']<list.length)
                page_object.upload.change(dialog_index,page_object.upload.file_item['index']);
            else
                page_object.upload.check_list(dialog_index);

        }

    },
    'check_list':function(dialog_index){

        var  list           =page_object.upload.data['list']
            ,i
            ,complete       =true;

        for(i=0;i<list.length;i++)
            if(!list[i]['remove']&&!list[i]['complete']){

                complete=false;

                page_object.upload.change(i);

                break;

            }

        if(complete)
            page_object.upload.complete(dialog_index);

    },
    'complete':function(dialog_index){

        page_object.upload.work=false;

        if(isset(page_object.upload.data['on_complete']))
            page_object.upload.data['on_complete']();

        page_object.upload.controls.enable(dialog_index);

    },
    'reset':function(){

        page_object.upload.data             =null;
        page_object.upload.reverse_list     ={};
        page_object.upload.work             =false;
        page_object.upload.file_item        ={};

    },
    'controls':{
        'disable':function(dialog_index){

            if(isset($d('dialog_submit_save_'+dialog_index))){

                $d('dialog_submit_save_'+dialog_index).onclick=function(){};
                $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');

            }

        },
        'enable':function(dialog_index){

            if(isset($d('dialog_submit_save_'+dialog_index))){

                $d('dialog_submit_save_'+dialog_index).onclick=page_object.dialog.data[dialog_index]['source']['save'];
                $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');

            }

        }
    }
};