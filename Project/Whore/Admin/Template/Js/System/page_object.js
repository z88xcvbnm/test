var page_object={
    'lang':'ru',
    'lang_default':'ru',
    'work_w':1000,
    'data':{},
    'is_first_start':true,
    'show_info_dev':false,
    'timeout':{},
    'content':{
        'ru':{
            'title':'Shluham.NET',
            'loading':'Загрузка',
            'settings':'Настройки'
        },
        'en':{
            'title':'Shluham.NET',
            'loading':'Loading',
            'settings':'Settings'
        }
    },
    'init':function(data){

        if(!isset(page_object.data[data['type']]))
            page_object.data[data['type']]=data;

        page_object.require.init(data);

        //page_object.action['type'].init();

    },
    'link':{},
    'action':{},
    'event':{},
    'resize':{},
    'include':function(type,data,timestamp){

        if(!isset(page_object.action[type])||typeof(page_object.action[type].init)!=='function')
            includeScript('/templates/js/'+type+'/init.js?t='+timestamp,function(){
                page_object.action[type].init(data);
            });
        else
            page_object.action[type].init(data);

    },
    'include_style':function(type,data,timestamp){

        if(!isset(page_object.action[type])||typeof(page_object.action[type].init)!=='function')
            includeScript('/templates/js/'+type+'/init.js?t='+timestamp,function(){
                page_object.action[type].init(data);
            });
        else
            page_object.action[type].init(data);

    },
    'require':{
        'data':{},
        'init':function(data){

            let  func
                ,key;

            trace('REQUIRE');
            trace(data);

            if(isset(data['script_list'])){

                key                 =new Date().getTime();
                data['all']         =data['script_list'].length
                    +(isset(data['style_list'])?data['style_list'].length:0)
                    +(isset(data['image_list'])?data['image_list'].length:0);
                data['loaded']      =0;

                page_object.require.data[key]=data;

                page_object.require.create(key);

            }
            else if(isset(data['init_list'])){

                if(isString(data['init_list'])){

                    func=new Function(data['init_list']);

                    eval(func)();

                }
                else if(isArray(data['init_list'])){

                    let  i
                        ,list_len=count(data['init_list']);

                    for(i=0;i<count(list_len);i++){

                        func=new Function(data['init_list'][i]);

                        eval(func)();

                    }

                }

            }

        },
        'create':function(key){

            let  head                   =document.getElementsByTagName('head')[0]
                ,script_list            =head.getElementsByTagName('script')
                ,script_list_len        =count(script_list)
                ,data                   =page_object.require.data[key]
                ,list                   =data['script_list']
                ,list_len               =count(list)
                ,style_list             =isset(data['style_list'])?data['style_list']:[]
                ,image_list             =isset(data['image_list'])?data['image_list']:[]
                ,img_el
                ,script_path
                ,image_path
                ,token                  =getCookie('token')
                ,i
                ,i1
                ,isset_script
                ,data_script_link
                ,time                   =new Date().getTime()
                ,func                   =new Function("")
                ,func_error
                ,full_len               =list_len+style_list.length;

            for(i=0;i<image_list.length;i++){

                func            =new Function("page_object.require.on_load('"+key+"',"+i+",'image')");
                func_error      =new Function("page_object.require.on_error('"+key+"',"+i+",'image')");

                img_el              =new Image();
                img_el.src          ='/'+image_list[i];
                img_el.onload       =func;
                img_el.onerror      =func_error;

            }

            for(i=0;i<style_list.length;i++){

                func=new Function("page_object.require.on_load('"+key+"',"+i+",'style')");

                if(style_list[i].match(/(?:paypal|qiwi)/)!=null)
                    script_path=style_list[i];
                else if(style_list[i].match(/\?/)!=null)
                    script_path=style_list[i]+'&token='+uniEncode(token);
                else
                    script_path=style_list[i]+'?t='+time+'&token='+uniEncode(token);

                includeStyle(script_path,func);

            }

            for(i=0;i<list_len;i++){

                isset_script        =false;
                data_script_link    =get_link_without_query(list[i]);

                for(i1=script_list_len-1;i1>=0;i1--)
                    if(isset(script_list[i1].src))
                        if(data_script_link===get_link_without_query(script_list[i1].src)){

                            isset_script=true;

                            break;

                        }

                if(isset_script)
                    page_object.require.data[key]['loaded']++;
                else{

                    func=new Function("page_object.require.on_load('"+key+"',"+i+",'script')");

                    if(list[i].match(/(?:paypal|qiwi)/)!=null)
                        image_path=list[i];
                    else if(list[i].match(/google/)!=null)
                        image_path=list[i];
                    else if(list[i].match(/\?/)!=null)
                        image_path=list[i]+'&token='+uniEncode(token);
                    else
                        image_path=list[i]+'?t='+time+'&token='+uniEncode(token);

                    trace(image_path);

                    includeScript(image_path,func);

                }

            }

//            page_object.require.data[key]['callback']=func;

            if(page_object.require.data[key]['loaded']===page_object.require.data[key]['all'])
                page_object.require.on_complete(key);

        },
        'on_load':function(key,index,type){

            page_object.require.data[key]['loaded']++;

            switch(type){

                case'script':{

                    page_object.require.data[key]['script_list'][index]['loaded']=true;

                    trace(page_object.require.data[key]['loaded']+'/'+page_object.require.data[key]['all']+' index: '+index+' src: '+page_object.require.data[key]['script_list'][index]);

                    break;

                }

                case'style':{

                    page_object.require.data[key]['style_list'][index]['loaded']=true;

                    trace(page_object.require.data[key]['loaded']+'/'+page_object.require.data[key]['all']+' index: '+index+' src: '+page_object.require.data[key]['style_list'][index]);

                    break;

                }

                case'image':{

                    page_object.require.data[key]['image_list'][index]['loaded']=true;

                    trace(page_object.require.data[key]['loaded']+'/'+page_object.require.data[key]['all']+' index: '+index+' src: '+page_object.require.data[key]['image_list'][index]);

                    break;

                }

                default:{

                    trace(page_object.require.data[key]['loaded']+'/'+page_object.require.data[key]['all']+' index: '+index+' src: ????');

                    break;

                }

            }

            if(isset($d('load_percent')))
                $d('load_percent').innerHTML=Math.round(page_object.require.data[key]['loaded']/page_object.require.data[key]['all']*100)+'%';

            if(page_object.require.data[key]['loaded']>=page_object.require.data[key]['all']){

                if(isset($d('load_percent')))
                    $d('load_percent').innerHTML='Preparing...';

                let func=new Function("page_object.require.on_complete("+key+")");

                setTimeout(func,100);

            }

        },
        'on_error':function(key,index,type){

            page_object.require.data[key]['loaded']++;

            switch(type){

                case'script':{

                    page_object.require.data[key]['script_list'][index]['loaded']=false;

                    trace('ERROR: '+page_object.require.data[key]['loaded']+'/'+page_object.require.data[key]['all']+' index: '+index+' src: '+page_object.require.data[key]['script_list'][index]);

                    break;

                }

                case'style':{

                    page_object.require.data[key]['style_list'][index]['loaded']=false;

                    trace('ERROR: '+page_object.require.data[key]['loaded']+'/'+page_object.require.data[key]['all']+' index: '+index+' src: '+page_object.require.data[key]['style_list'][index]);

                    break;

                }

                case'image':{

                    page_object.require.data[key]['image_list'][index]['loaded']=false;

                    trace('ERROR: '+page_object.require.data[key]['loaded']+'/'+page_object.require.data[key]['all']+' index: '+index+' src: '+page_object.require.data[key]['image_list'][index]);

                    break;

                }

                default:{

                    trace(page_object.require.data[key]['loaded']+'/'+page_object.require.data[key]['all']+' index: '+index+' src: ????');

                    break;

                }

            }

            if(isset($d('load_percent')))
                $d('load_percent').innerHTML=Math.round(page_object.require.data[key]['loaded']/page_object.require.data[key]['all']*100)+'%';

            if(page_object.require.data[key]['loaded']>=page_object.require.data[key]['all']){

                if(isset($d('load_percent')))
                    $d('load_percent').innerHTML='Preparing...';

                let func=new Function("page_object.require.on_complete("+key+")");

                setTimeout(func,100);

            }

        },
        'on_complete':function(key){

            if(isset(page_object.require.data[key]['init_list']))
                if(isArray(page_object.require.data[key]['init_list'])){

                    let  i
                        ,list           =page_object.require.data[key]['init_list']
                        ,list_len       =count(list)
                        ,func;

                    for(i=0;i<list_len;i++){

                        switch(typeof(list[i])){

                            case'function':{

                                func=list[i];

                                break;

                            }

                            case'string':{

                                func=new Function(list[i]);

                                break;

                            }

                            default:{

                                func=function(){};

                                break;

                            }

                        }

                        eval(func)();

                    }

                }
                else if(isString(page_object.require.data[key]['init_list'])){

                    let func=new Function(page_object.require.data[key]['init_list']);

                    trace('callback script on load');
                    trace(func);

                    eval(func)();

                }
                else{

                    trace('drop_init: ');
                    trace(page_object.require.data[key]['init_list']);

                }

            setTimeout(function(){

                if(isset($d('load_percent')))
                    $d('load_percent').innerHTML='';

            },1);

            delete page_object.require.data[key];

        }
    }
};

