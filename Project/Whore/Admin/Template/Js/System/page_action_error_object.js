page_object.action.error={
    'content_obj':{
        'ru':{
            'title_error':'Ошибка',
            'title_warning':'Предупрежение',
            'title_info':'Информация',
            'close':'Закрыть',
            'ok':'OK',
            'submit':'Отправить',
            'drop':'Удалить',
            'recover':'Восстановить'
        },
        'en':{
            'title_error':'Error',
            'title_warning':'Warning',
            'title_info':'Info',
            'close':'Close',
            'ok':'OK',
            'submit':'Send',
            'drop':'Remove',
            'recover':'Recover'
        }
    },
    'timeout':null,
    'init':function(data){

        if(isString(data))
            data=jsonDecode(data);

        if(isset($d('error_block'))){

            page_object.action.error.action.un_show();

            page_object.action.error.timeout=setTimeout(function(){

                page_object.action.error.create(data);

            },350);

        }
        else
            page_object.action.error.create(data);

    },
    'create':function(data){

        var  inner      =''
            ,el
            ,lang_obj   =page_object.action.error.content_obj[lang]
            ,title      =''
            ,key;

        if(isset(data['error'])){

            title   =lang_obj['title_error'];
            key     ='error';

        }
        else if(isset(data['warning'])){

            title   =lang_obj['title_warning'];
            key     ='warning';

        }
        else if(isset(data['info'])){

            title   =lang_obj['title_info'];
            key     ='info';

        }

        inner+='<div id="error_block_container" class="info_block_container">';
        inner+='<div id="error_title" class="info_title">'+title+'</div>';
        inner+='<div id="error_info_block" class="row" style="text-align: center">';
        inner+=stripSlashes(data[key]['content']);
        inner+='</div>';

        if(isset(data[key]['submit'])){

            inner+='<div id="error_button_submit" class="info_button_hover" style="width: 185px; float: left;">'+lang_obj['submit']+'</div>';
            inner+='<div id="error_button_close" class="info_button_hover" style="width: 185px; float: left; margin-left: 20px;">'+lang_obj['close']+'</div>';

        }
        else if(isset(data[key]['drop'])){

            inner+='<div id="error_button_drop" class="info_button_hover" style="width: 185px; float: left;">'+lang_obj['drop']+'</div>';
            inner+='<div id="error_button_close" class="info_button_hover" style="width: 185px; float: left; margin-left: 20px;">'+lang_obj['close']+'</div>';

        }
        else if(isset(data[key]['recover'])){

            inner+='<div id="error_button_recover" class="info_button_hover" style="width: 185px; float: left;">'+lang_obj['recover']+'</div>';
            inner+='<div id="error_button_close" class="info_button_hover" style="width: 185px; float: left; margin-left: 20px;">'+lang_obj['close']+'</div>';

        }
        else if(isset(data[key]['ok']))
            inner+='<div id="error_button_ok" class="info_button_hover">'+lang_obj['ok']+'</div>';
        else
            inner+='<div id="error_button_close" class="info_button_hover">'+lang_obj['close']+'</div>';

        inner+='</div>';

        el=addElement({
            'tag':'div',
            'id':'error_block',
            'class':'info_block',
            'inner':inner,
            'style':'z-index: 101'
        });

        $d('all').appendChild(el);

        page_object.action.error.action.controls.init(key,data);
        page_object.action.error.action.show();

    },
    'action':{
        'controls':{
            'init':function(key,data){

                var func_un_show=function(){

                    page_object.action.error.action.un_show(key,data);

                };
                
                if(isset(data[key]['submit'])){

                    if(isset(data[key]['on_submit'])){

                        $d('error_button_submit').onclick=function(){

                            eval(data[key]['on_submit']);

                        };

                        $d('error_button_close').onclick=func_un_show;

                    }
                    else{

                        $d('error_button_submit').onclick=func_un_show;
                        $d('error_button_close').onclick=func_un_show;

                    }


                }
                else if(isset(data[key]['drop'])){

                    if(isset(data[key]['on_drop'])){

                        $d('error_button_drop').onclick=function(){

                            eval(data[key]['on_drop'])();

                        };
                        $d('error_button_close').onclick=func_un_show;

                    }
                    else{

                        $d('error_button_drop').onclick=func_un_show;
                        $d('error_button_close').onclick=func_un_show;

                    }


                }
                else if(isset(data[key]['recover'])){

                    if(isset(data[key]['on_recover'])){

                        $d('error_button_recover').onclick=function(){

                            eval(data[key]['on_recover']);

                        };
                        $d('error_button_close').onclick=func_un_show;

                    }
                    else{

                        $d('error_button_recover').onclick=func_un_show;
                        $d('error_button_close').onclick=func_un_show;

                    }


                }
                else if(isset(data[key]['ok'])){

                    if(isset(data[key]['on_ok'])){

                        $d('error_button_ok').onclick=function(){

                            eval(data[key]['on_ok']);

                        };
                        $d('error_button_close').onclick=func_un_show;

                    }
                    else{

                        $d('error_button_ok').onclick=func_un_show;
                        $d('error_button_close').onclick=func_un_show;

                    }

                }
                else if(isset(data[key]['close'])){

                    if(isset(data[key]['on_close'])){

                        $d('error_button_close').onclick=function(){

                            eval(data[key]['on_close']);

                        };

                    }
                    else
                        $d('error_button_close').onclick=func_un_show;

                }
                else{

                    if(isset(data[key]['on_close'])){

                        $d('error_button_close').onclick=function(){

                            eval(data[key]['on_close']);

                        };

                    }
                    else
                        $d('error_button_close').onclick=func_un_show;

                }

            }
        },
        'show':function(){

            if(isset($d('error_block'))){

                var  el_h       =$d('error_block').scrollHeight
                    ,m_t        =Math.ceil((winSize.winHeight-el_h)/2);

                opaElement.setOpacity($d('error_block'),1);

                $s('error_block').marginTop     =m_t+'px';
                $s('error_block').height        =el_h+'px';

                setTimeout(function(){

                    opaElement.setOpacity($d('error_block_container'),1);

                },300);

            }

        },
        'un_show':function(key,data){

            opaElement.setOpacity($d('error_block'),0);

            if(isset(key)&&isset(data)){

                if(isset(data[key]['key'])){

                    switch(data[key]['key']){

                        case 401:
                        case 403:{

                            includeScript('/templates/js/page_action_auth_object.js?t='+new Date().getTime(),function(){

                                page_object.action.auth.init('auth');

                            });

                            break;

                        }

                    }

                }

            }

            setTimeout(function(){

                removeElement($d('error_block'));

            },300);

        }
    }
};
