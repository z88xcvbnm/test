page_object.dialog={
    'position':{
        'bg':{
            'x':0,
            'y':0,
            'w':0,
            'h':0,
            'o':0,
            'o_max':.5,
            'bg':{
                'color':'#1b93db'
            }
        },
        'dialog':{
            'x':0,
            'y':0,
            'w':400,
            'h':0,
            'o':0,
            'm':{
                't':10,
                'l':10,
                'r':10,
                'b':10
            }
            //'m':{
            //    't':10,
            //    'l':10,
            //    'r':10,
            //    'b':10
            //}
        },
        'dialog_block':{
            'x':0,
            'y':0,
            'w':420,
            'h':0,
            'o':0
        },
        'controls':{
            'm':{
                'l':10,
                'r':0
            }
        }
    },
    'content':{
        'ru':{
            'ok':'OK',
            'wait':'Подождите',
            'save':'Сохранить',
            'no_save':'Не сохранять',
            'add':'Добавить',
            'create':'Создать',
            'reg':'Зарегистрироваться',
            'remove':'Удалить',
            'update':'Обновить',
            'block':'Заблокировать',
            'unblock':'Разблокировать',
            'cancel':'Отмена',
            'close':'Закрыть',
            'send':'Отправить'
        },
        'en':{
            'ok':'OK',
            'wait':'Wait',
            'save':'Save',
            'no_save':'Don\'t save',
            'add':'Add',
            'reg':'Registration',
            'create':'Create',
            'remove':'Remove',
            'update':'Update',
            'block':'Block',
            'unblock':'Unblock',
            'cancel':'Cancel',
            'close':'Close',
            'send':'Send'
        }
    },
    'data':[],
    'index':0,
    'box_active_len':0,
    'init':function(config){

        if(!isset(config['close_button']))
            config['close_button']=true;

        var  index      =page_object.dialog.index
            ,data       ={
                'source':config
            };

        page_object.dialog.data[index]=data;

        page_object.dialog.create.init(index);

        page_object.dialog.index++;

        return index;

    },
    'init_error':function(data){

        var  inner          =''
            ,key
            ,lang_obj       =page_object.action.admin.content[page_object.lang];

        if(isArray(data['info'])){

            for(key in data['info']){

                inner+='<div class="dialog_row'+(OS.isMobile?'_mobile':'')+'">';
                inner+='<span>'+stripSlashes(data['info'][key])+'</span>';
                inner+='</div>';

            }

        }
        else{

            inner+='<div class="dialog_row'+(OS.isMobile?'_mobile':'')+'">';
            inner+='<span>'+stripSlashes(data['info'])+'</span>';
            inner+='</div>';

        }

        page_object.dialog.init({
            'title':lang_obj['error']+(isset(data['code'])?(': '+data['code']):''),
            'inner':inner,
            'cancel':true
        });

    },
    'create':{
        'init':function(index){

            page_object.dialog.create.position.init(index);
            page_object.dialog.create.bg(index);
            page_object.dialog.create.dialog_block(index);
            page_object.dialog.create.set_action(index);
            page_object.dialog.action.body.lock(index);
            page_object.dialog.create.show(index);

        },
        'position':{
            'init':function(index){

                page_object.dialog.create.position.bg(index);
                page_object.dialog.create.position.dialog_block(index);

            },
            'bg':function(index){

                page_object.dialog.data[index]['bg']            ={};

                page_object.dialog.data[index]['bg']['o']       =0;
                page_object.dialog.data[index]['bg']['w']       =winSize.winWidth;
                page_object.dialog.data[index]['bg']['h']       =winSize.winHeight;
                page_object.dialog.data[index]['bg']['x']       =document.body.scrollLeft;
                page_object.dialog.data[index]['bg']['y']       =0;
                //page_object.dialog.data[index]['bg']['y']       =document.body.scrollTop;
                page_object.dialog.data[index]['bg']['z']       =isset(page_object.dialog.data[index]['source']['z'])?page_object.dialog.data[index]['source']['z']:((index+1)*2+1+200);
                page_object.dialog.data[index]['bg']['m']       ={
                    't':0,
                    'r':0,
                    'b':0,
                    'l':0
                };

                trace('bg Z');
                trace('index: '+index);
                trace(page_object.dialog.data[index]);
                trace(page_object.dialog.data[index]['bg']['z']);

            },
            'dialog_block':function(index){

                var w=(isset(page_object.dialog.data[index]['source']['w']))?page_object.dialog.data[index]['source']['w']:page_object.dialog.position.dialog.w;

                page_object.dialog.data[index]['dialog']                ={};
                page_object.dialog.data[index]['dialog']['w']           =w;
                page_object.dialog.data[index]['dialog']['o']           =0;
                page_object.dialog.data[index]['dialog']['m']           =page_object.dialog.position.dialog.m;

                page_object.dialog.data[index]['controls']              ={};
                page_object.dialog.data[index]['controls']['w']         =page_object.dialog.data[index]['dialog']['w'];
                page_object.dialog.data[index]['controls']['m']         =page_object.dialog.position.dialog.m;

                page_object.dialog.data[index]['dialog_block']          ={};
                page_object.dialog.data[index]['dialog_block']['o']     =0;
                page_object.dialog.data[index]['dialog_block']['w']     =page_object.dialog.data[index]['dialog']['w']+page_object.dialog.data[index]['dialog']['m']['l']+page_object.dialog.data[index]['dialog']['m']['r'];
                page_object.dialog.data[index]['dialog_block']['h']     =page_object.dialog.position.dialog_block.h;
                page_object.dialog.data[index]['dialog_block']['m']     ={
                    't':0,
                    'r':0,
                    'b':0,
                    'l':0
                };

                if(isset(page_object.dialog.data[index]['source']['h']))
                    page_object.dialog.data[index]['dialog']['h']=page_object.dialog.data[index]['source']['h'];

                page_object.dialog.data[index]['dialog_block']['o']     =0;
                page_object.dialog.data[index]['dialog_block']['h']     =0;
                page_object.dialog.data[index]['dialog_block']['w']     =w+page_object.dialog.data[index]['dialog_block']['m']['l']+page_object.dialog.data[index]['dialog_block']['m']['r'];
                page_object.dialog.data[index]['dialog_block']['x']     =Math.ceil((winSize.winWidth-page_object.dialog.data[index]['dialog_block']['w'])/2)+document.body.scrollLeft;
                //page_object.dialog.data[index]['dialog_block']['y']     =document.body.scrollTop+Math.ceil(winSize.winHeight/2);
                page_object.dialog.data[index]['dialog_block']['y']     =Math.ceil(winSize.winHeight/2);
                page_object.dialog.data[index]['dialog_block']['z']       =isset(page_object.dialog.data[index]['source']['z'])?(page_object.dialog.data[index]['source']['z']+1):((index+1)*2+2+200);
                // page_object.dialog.data[index]['dialog_block']['z']     =(index+1)*2+2+200;

            },
            'controls':function(index){

                if(isset($d('dialog_controls_'+index))){

                    var  list           =$d('dialog_controls_'+index).getElementsByTagName('div')
                        ,list_len       =list.length
                        ,i
                        ,w              =(isset(page_object.dialog.data[index]['source']['w'])?page_object.dialog.data[index]['source']['w']:page_object.dialog.position.dialog.w)
                        ,h
                        ,x              =0;

                    w=Math.ceil((w-((list_len-1)*(page_object.dialog.data[index]['controls']['m']['l']+page_object.dialog.data[index]['controls']['m']['r']))-(list_len*2))/list_len)+1;

                    if(list_len>0)
                        h=elementSize.height(list[0])+2;

                    if(list_len>1)
                        if(OS.isMobile)
                            $s('dialog_controls_'+index).height='auto';

                    for(i=0;i<list_len;i++)
                        if(isObject(list[i])){

                            if(OS.isMobile){

                                // let w_split=(100/list_len)-((list_len-1)*40);
                                // let w_split=(100/list_len)-((list_len-1)*1);

                                // trace('w_split: '+w_split);

                                // list[i].style.width=''+w_split+'%';
                                list[i].style.width='100%';

                                if(i<list_len-1)
                                    list[i].style.marginBottom='10px';

                            }
                            else{

                                list[i].style.transform     ='translate('+x+'px,0)';
                                list[i].style.width         =w+'px';

                                x+=w+page_object.dialog.data[index]['controls']['m']['l']+page_object.dialog.data[index]['controls']['m']['r']+2;

                            }

                        }

                    if(!OS.isMobile)
                        $s('dialog_controls_'+index).height=h+'px';

                }

            }
        },
        'bg':function(index){

            var  el
                ,style='';

            // style+='width: '+page_object.dialog.data[index]['bg']['w']+'px; ';
            // style+='height: '+page_object.dialog.data[index]['bg']['h']+'px; ';
            //style+='transform: translate('+page_object.dialog.data[index]['bg']['x']+'px,'+page_object.dialog.data[index]['bg']['y']+'px); ';
            style+='opacity: '+page_object.dialog.data[index]['bg']['o']+'; ';
            style+='background-color: '+page_object.dialog.position.bg.bg.color+'; ';
            style+='z-index: '+page_object.dialog.data[index]['bg']['z']+'; ';

            el=addElement({
                'tag':'div',
                'id':'bg_all_'+index,
                'class':'bg_all',
                'style':style
            });

            $d('all').appendChild(el);

        },
        'dialog_block':function(index){

            var  el
                ,style      =''
                ,inner      =''
                ,data       =page_object.dialog.data[index]['source']
                ,lang_obj   =page_object.dialog.content[page_object.lang]
                ,w          =(isset(page_object.dialog.data[index]['dialog']['w'])?page_object.dialog.data[index]['dialog']['w']:page_object.dialog.position.dialog.w);

            if(!OS.isMobile){

                style+='width: '+(w+page_object.dialog.data[index]['dialog']['m']['l']+page_object.dialog.data[index]['dialog']['m']['r'])+'px; ';
                style+='height: '+page_object.dialog.data[index]['dialog_block']['h']+'px; ';

            }
            // style+='transform: translate('+page_object.dialog.data[index]['dialog_block']['x']+'px,'+page_object.dialog.data[index]['dialog_block']['y']+'px); ';
            style+='opacity: '+page_object.dialog.data[index]['dialog_block']['o']+'; ';
            style+='z-index: '+page_object.dialog.data[index]['dialog_block']['z']+'; ';

            if(OS.isMobile){

                // style+='padding: 10px 10px 10px 10px; ';
                style+='font-size: 1em; ';

            }

            // inner+='<div id="dialog_'+index+'" class="dialog" style="width: '+w+'px;'+(isset(page_object.dialog.data[index]['dialog']['h'])?(' height: '+page_object.dialog.data[index]['dialog']['h']+'px; '):'')+' margin: '+page_object.dialog.data[index]['dialog']['m']['t']+'px '+page_object.dialog.data[index]['dialog']['m']['r']+'px '+page_object.dialog.data[index]['dialog']['m']['b']+'px '+page_object.dialog.data[index]['dialog']['m']['l']+'px; ">';
            inner+='<div id="dialog_'+index+'" class="dialog">';

            if(isset(data['close_button'])&&!OS.isMobile)
                if(data['close_button'])
                    inner+='<div id="dialog_close_button_'+index+'" class="dialog_close_button"></div>';

            if(isset(data['title']))
                inner+='<div id="dialog_title_'+index+'" class="dialog_title">'+stripSlashes(data['title'])+'</div>';

            if(isset(data['inner']))
                inner+='<div id="dialog_inner_'+index+'"'+(isset(data['title'])?'':' style="margin-top: 0"')+' class="dialog_inner">'+data['inner']+'</div>';

            inner+='<div id="dialog_controls_'+index+'" class="dialog_controls">';

            if(isset(data['ok']))
                inner+='<div id="dialog_submit_ok_'+index+'" class="dialog_submit_ok">'+stripSlashes(lang_obj['ok'])+'</div>';

            if(isset(data['reg']))
                inner+='<div id="dialog_submit_reg_'+index+'" class="dialog_submit_reg">'+stripSlashes(lang_obj['reg'])+'</div>';

            if(isset(data['remove']))
                inner+='<div id="dialog_submit_remove_'+index+'" class="dialog_submit_remove">'+stripSlashes(lang_obj['remove'])+'</div>';

            if(isset(data['update']))
                inner+='<div id="dialog_submit_update_'+index+'" class="dialog_submit_update">'+stripSlashes(lang_obj['update'])+'</div>';

            if(isset(data['create']))
                inner+='<div id="dialog_submit_create_'+index+'" class="dialog_submit_create">'+stripSlashes(lang_obj['create'])+'</div>';

            if(isset(data['add']))
                inner+='<div id="dialog_submit_add_'+index+'" class="dialog_submit_add">'+stripSlashes(lang_obj['add'])+'</div>';

            if(isset(data['send']))
                inner+='<div id="dialog_submit_send_'+index+'" class="dialog_submit_add">'+stripSlashes(lang_obj['send'])+'</div>';

            if(isset(data['unblock']))
                inner+='<div id="dialog_submit_unblock_'+index+'" class="dialog_submit_unblock">'+stripSlashes(lang_obj['unblock'])+'</div>';

            if(isset(data['block']))
                inner+='<div id="dialog_submit_block_'+index+'" class="dialog_submit_block">'+stripSlashes(lang_obj['block'])+'</div>';

            if(isset(data['save']))
                inner+='<div id="dialog_submit_save_'+index+'" class="dialog_submit_save">'+stripSlashes(lang_obj['save'])+'</div>';

            if(isset(data['no_save']))
                inner+='<div id="dialog_submit_no_save_'+index+'" class="dialog_submit_save">'+stripSlashes(lang_obj['no_save'])+'</div>';

            if(isset(data['cancel'])||isset(data['on_cancel']))
                inner+='<div id="dialog_submit_cancel_'+index+'" class="dialog_submit_cancel">'+stripSlashes(lang_obj['cancel'])+'</div>';

            if(isset(data['close']))
                inner+='<div id="dialog_submit_close_'+index+'" class="dialog_submit_close">'+stripSlashes(lang_obj['close'])+'</div>';

            inner+='</div>';

            inner+='</div>';

            el=addElement({
                'tag':'div',
                'id':'dialog_block_'+index,
                'class':'dialog_block',
                'inner':inner,
                'style':style
            });

            $d('all').appendChild(el);

        },
        'set_action':function(index){

            if(isset(page_object.dialog.data[index]))
                if(isset(page_object.dialog.data[index]['source'])){

                    if(isset($d('dialog_submit_ok_'+index)))
                        $d('dialog_submit_ok_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            eval(page_object.dialog.data[index]['source']['ok'])(dialog_index);

                        };

                    if(isset($d('dialog_submit_reg_'+index)))
                        $d('dialog_submit_reg_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            eval(page_object.dialog.data[index]['source']['reg'])(dialog_index);

                        };

                    if(isset($d('dialog_submit_create_'+index)))
                        $d('dialog_submit_create_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            eval(page_object.dialog.data[index]['source']['create'])(dialog_index);

                        };

                    if(isset($d('dialog_submit_add_'+index)))
                        $d('dialog_submit_add_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            eval(page_object.dialog.data[index]['source']['add'])(dialog_index);

                        };

                    if(isset($d('dialog_submit_send_'+index)))
                        $d('dialog_submit_send_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            eval(page_object.dialog.data[index]['source']['send'])(dialog_index);

                        };

                    if(isset($d('dialog_submit_block_'+index)))
                        $d('dialog_submit_block_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            eval(page_object.dialog.data[index]['source']['block'])(dialog_index);

                        };

                    if(isset($d('dialog_submit_unblock_'+index)))
                        $d('dialog_submit_unblock_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            eval(page_object.dialog.data[index]['source']['unblock'])(dialog_index);

                        };

                    if(isset($d('dialog_submit_remove_'+index)))
                        $d('dialog_submit_remove_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            eval(page_object.dialog.data[index]['source']['remove'])(dialog_index);

                        };

                    if(isset($d('dialog_submit_update_'+index)))
                        $d('dialog_submit_update_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            eval(page_object.dialog.data[index]['source']['update'])(dialog_index);

                        };

                    if(isset($d('dialog_submit_save_'+index)))
                        $d('dialog_submit_save_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            eval(page_object.dialog.data[index]['source']['save'])(dialog_index);

                        };

                    if(isset($d('dialog_submit_no_save_'+index)))
                        $d('dialog_submit_no_save_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            eval(page_object.dialog.data[index]['source']['no_save'])(dialog_index);

                        };

                    if(isset($d('dialog_submit_cancel_'+index)))
                        $d('dialog_submit_cancel_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            if(isset(page_object.dialog.data[index]['source']['cancel']))
                                if(typeof page_object.dialog.data[index]['source']['cancel']==='function')
                                    eval(page_object.dialog.data[index]['source']['cancel'])(dialog_index);

                            if(isset(page_object.dialog.data[index]['source']['on_cancel']))
                                if(typeof page_object.dialog.data[index]['source']['on_cancel']==='function')
                                    eval(page_object.dialog.data[index]['source']['on_cancel'])(dialog_index);

                            page_object.dialog.action.un_show.init(dialog_index,true);

                        };

                    if(isset($d('dialog_close_button_'+index)))
                        $d('dialog_close_button_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            if(isset(page_object.dialog.data[index]['source']['cancel']))
                                if(typeof page_object.dialog.data[index]['source']['cancel']==='function')
                                    eval(page_object.dialog.data[index]['source']['cancel'])(dialog_index);

                            if(isset(page_object.dialog.data[index]['source']['on_cancel']))
                                if(typeof page_object.dialog.data[index]['source']['on_cancel']==='function')
                                    eval(page_object.dialog.data[index]['source']['on_cancel'])(dialog_index);

                            page_object.dialog.action.un_show.init(dialog_index,true);

                        };

                    if(isset($d('dialog_submit_close_'+index)))
                        $d('dialog_submit_close_'+index).onclick=function(){

                            var dialog_index=this.id.split('_')[3];

                            if(isset(page_object.dialog.data[index]['source']['close']))
                                if(typeof page_object.dialog.data[index]['source']['close']==='function')
                                    page_object.dialog.data[index]['source']['close'](dialog_index);

                            page_object.dialog.action.un_show.init(dialog_index,true);

                        };

                    if(isset(page_object.dialog.data[index]['source']['on_create']))
                        eval(page_object.dialog.data[index]['source']['on_create'])(index);

                }

        },
        'show':function(index){

            setTimeout(function(){

                page_object.dialog.create.position.controls(index);

            },40);

            setTimeout(function(){

                page_object.dialog.action.show.init(index);

            },340);

        }
    },
    'action':{
        'show':{
            'init':function(index){

                page_object.dialog.box_active_len++;

                page_object.dialog.action.show.bg(index);

                setTimeout(function(){

                    page_object.dialog.action.show.dialog(index);

                },300);

            },
            'bg':function(index){

                if(isset($d('bg_all_'+index))){

                    page_object.dialog.data[index]['bg']['o']=page_object.dialog.position.bg.o_max;

                    $s('bg_all_'+index).opacity=page_object.dialog.data[index]['bg']['o'];

                }

            },
            'dialog':function(index){

                if(isset($d('dialog_block_'+index))){

                    var w=(isset(page_object.dialog.data[index]['source']['w']))?page_object.dialog.data[index]['source']['w']:page_object.dialog.position.dialog.w;

                    page_object.dialog.data[index]['dialog_block']['w']=w;

                    if(isset(page_object.dialog.data[index]['dialog_block']['h']))
                        $s('dialog_block_'+index).height=page_object.dialog.data[index]['dialog_block']['h']+'px';

                    page_object.dialog.data[index]['dialog_block']['o']     =1;
                    page_object.dialog.data[index]['dialog_block']['w']     =w+page_object.dialog.data[index]['dialog']['m']['l']+page_object.dialog.data[index]['dialog']['m']['r'];
                    page_object.dialog.data[index]['dialog_block']['h']     =$d('dialog_'+index).scrollHeight+20;
                    page_object.dialog.data[index]['dialog_block']['y']     =Math.ceil((winSize.winHeight-page_object.dialog.data[index]['dialog_block']['h'])/2);
                    //page_object.dialog.data[index]['dialog_block']['y']     =Math.ceil((winSize.winHeight-page_object.dialog.data[index]['dialog_block']['h'])/2)+$b().scrollTop;

                    if(!OS.isMobile){

                        $s('dialog_block_'+index).width=page_object.dialog.data[index]['dialog_block']['w']+'px';

                    }

                    $s('dialog_block_'+index).height           ='auto';
                    // $s('dialog_block_'+index).height           =page_object.dialog.data[index]['dialog_block']['h']+'px';
                    // $s('dialog_block_'+index).transform        ='translate('+page_object.dialog.data[index]['dialog_block']['x']+'px,'+page_object.dialog.data[index]['dialog_block']['y']+'px)';

                    $s('dialog_block_'+index).opacity          =page_object.dialog.data[index]['dialog_block']['o'];
                    $s('dialog_block_'+index).zIndex           =page_object.dialog.data[index]['dialog_block']['z']+'px';

                }

            }
        },
        'un_show':{
            'init':function(index,remove){

                page_object.dialog.box_active_len--;

                page_object.dialog.action.un_show.dialog(index,remove);

                setTimeout(function(){

                    page_object.dialog.action.un_show.bg(index,remove);

                },340);

                if(page_object.dialog.box_active_len===0)
                    setTimeout(page_object.dialog.action.body.un_lock,640);

            },
            'bg':function(index,remove){

                if(isset($d('bg_all_'+index))){

                    page_object.dialog.data[index]['bg']['o']=0;

                    $s('bg_all_'+index).opacity=page_object.dialog.data[index]['bg']['o'];

                    if(isset(remove))
                        if(remove)
                            setTimeout(function(){

                                page_object.dialog.action.remove.bg(index);

                                delete page_object.dialog.data[index];

                            },300);

                }

            },
            'dialog':function(index,remove){

                if(isset($d('dialog_block_'+index))){

                    page_object.dialog.data[index]['dialog_block']['o']     =0;
                    page_object.dialog.data[index]['dialog_block']['h']     =0;
                    page_object.dialog.data[index]['dialog_block']['y']     =Math.ceil(winSize.winHeight/2);
                    //page_object.dialog.data[index]['dialog_block']['y']     =Math.ceil(winSize.winHeight/2)+$b().scrollTop;

                    $s('dialog_block_'+index).transform        ='translate('+page_object.dialog.data[index]['dialog_block']['x']+'px,'+page_object.dialog.data[index]['dialog_block']['y']+'px)';
                    $s('dialog_block_'+index).opacity          =page_object.dialog.data[index]['dialog_block']['o'];
                    $s('dialog_block_'+index).height           =page_object.dialog.data[index]['dialog_block']['h']+'px';

                    if(isset(remove))
                        if(remove)
                            setTimeout(function(){

                                page_object.dialog.action.remove.dialog(index);

                            },300);

                }

            }
        },
        'remove':{
            'bg':function(index){

                if(isset($d('bg_all_'+index)))
                    removeElement($d('bg_all_'+index));

            },
            'dialog':function(index){

                if(isset($d('dialog_block_'+index)))
                    removeElement($d('dialog_block_'+index));

            }
        },
        'body':{
            'lock':function(){

                document.body.style.overflow='hidden';

            },
            'un_lock':function(){

                document.body.style.overflow='auto';

            }
        },
        'resize':function(){

            var  list               =page_object.dialog.data
                ,upload_list        =isset(page_object.upload.data)?(isset(page_object.upload.data['list'])?page_object.upload.data['list']:[]):[]
                ,list_len           =list.length
                ,index;


            if(isset($d('dialog_file_list'))){

                $s('dialog_file_list').marginTop=(upload_list.length===0||isset($d('dialog_content_left_block')))?0:'10px';

                if(OS.isMobile)
                    $s('dialog_file_list').marginBottom='10px';

            }

            for(index=0;index<list_len;index++){

                if(isset($d('dialog_block_'+index))){

                    page_object.dialog.data[index]['dialog_block']['h']     =$d('dialog_'+index).scrollHeight+page_object.dialog.data[index]['dialog']['m']['t']+page_object.dialog.data[index]['dialog']['m']['b'];
                    //page_object.dialog.data[index]['dialog_block']['y']     =Math.ceil((winSize.winHeight-page_object.dialog.data[index]['dialog_block']['h'])/2)+$b().scrollTop;
                    page_object.dialog.data[index]['dialog_block']['y']     =Math.ceil((winSize.winHeight-page_object.dialog.data[index]['dialog_block']['h'])/2);
                    page_object.dialog.data[index]['dialog_block']['w']     =page_object.dialog.data[index]['dialog']['w']+page_object.dialog.data[index]['dialog']['m']['l']+page_object.dialog.data[index]['dialog']['m']['r'];
                    page_object.dialog.data[index]['dialog_block']['x']     =Math.ceil((winSize.winWidth-page_object.dialog.data[index]['dialog_block']['w'])/2)+$b().scrollLeft;

                    if(!OS.isMobile){

                        $s('dialog_block_'+index).height           ='auto';
                        $s('dialog_block_'+index).width            =page_object.dialog.data[index]['dialog_block']['w']+'px';
                        // $s('dialog_block_'+index).transform        ='translate('+page_object.dialog.data[index]['dialog_block']['x']+'px,'+page_object.dialog.data[index]['dialog_block']['y']+'px)';

                    }
                    else
                        $s('dialog_block_'+index).height='auto';

                }

                if(isset($d('bg_all_'+index))){

                    page_object.dialog.data[index]['bg']['w']       =winSize.winWidth;
                    page_object.dialog.data[index]['bg']['h']       =winSize.winHeight;
                    page_object.dialog.data[index]['bg']['x']       =document.body.scrollLeft;
                    page_object.dialog.data[index]['bg']['y']       =0;
                    //page_object.dialog.data[index]['bg']['y']       =document.body.scrollTop;

                    // $s('bg_all_'+index).height             =page_object.dialog.data[index]['bg']['h']+'px';
                    // $s('bg_all_'+index).width              =page_object.dialog.data[index]['bg']['w']+'px';
                    //$s('bg_all_'+index).transform          ='translate('+page_object.dialog.data[index]['bg']['x']+'px,'+page_object.dialog.data[index]['bg']['y']+'px)';

                    page_object.dialog.create.position.controls(index);

                }

            }

        }
    }
};