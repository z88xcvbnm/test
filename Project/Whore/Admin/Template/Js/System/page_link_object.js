page_object.link={
    'link_name':null,
    'link_list':[],
    'back_link_list':[],
    'back_link_name':null,
    'time_out_step':40,
    'link_drop':{},
    'is_start':true,
    'init':function(){

        page_object.link.prepare();

    },
    'get_link_name':function(){

        let  host_name  =document.domain
            ,http       =http_protocol.protocol;

        return str_replace(String(document.location),(http+'://'+host_name+'/'),'');

    },
    'set':function(title,link_name){

        if(isset(title)&&isset(link_name)){

            setUrl(title,link_name);

            return true;

        }

        return false;

    },
    'start_prepare':function(){

        if(isset(page_object.timeout.link_work))
            clearTimeout(page_object.timeout.link_work);

        page_object.timeout.link_work=setTimeout(page_object.link.prepare,page_object.link.time_out_step);

    },
    'prepare':function(){

        page_object.link.back_link_name=page_object.link.get_link_name();

        if(page_object.link.back_link_name!==page_object.link.link_name){

            page_object.link.link_name          =page_object.link.back_link_name;
            page_object.link.back_link_list     =page_object.link.link_list;
            page_object.link.link_list          =page_object.link.link_name.split('/');

            page_object.link.trig_page(page_object.link.link_name);

        }

        page_object.link.start_prepare();

    },
    'trig_page':function(link_name){

        page_object.link.event.get_trigger(link_name);

    },
    'preload':{
        'show':function(){

            $s('all').cursor        ='wait';
            $b().style.overflow         ='hidden';

            if(!isset($d('bg_load'))){

                let el=addElement({
                    'tag':'div',
                    'id':'bg_load',
                    'inner':'<div id="load_icon"></div><div id="load_percent"></div>',
                    'style':'opacity: '+(page_object.link.is_start?1:.5)
                });

                $d('all').appendChild(el);

                setTimeout(function(){

                    $s('bg_load').opacity=(page_object.link.is_start?1:.5);

                    if(isset(page_object.action.admin))
                        setTimeout(page_object.action.admin.action.all.resize,300);

                },40);

            }
            else{

                //$s('bg_load').transform     ='translate(0,'+$d('all').scrollTop+'px)';
                $s('bg_load').opacity       =(page_object.link.is_start?1:.5);
                // $s('bg_load').width         =winSize.winWidth+'px';
                // $s('bg_load').height        =winSize.winHeight+'px';

                if(isset(page_object.action.admin))
                    page_object.action.admin.action.all.resize();

            }

            page_object.link.is_start=false;

        },
        'un_show':function(){

            $s('all').cursor        ='default';
            $b().style.overflow     ='auto';

            if(isset($d('bg_load'))){

                $s('bg_load').opacity=0;

                setTimeout(function(){

                    removeElement($d('bg_load'));

                },300);

            }

        }
    },
    'event':{
        'get_trigger':function(link_name){

            let  post           ='link='+uniEncode(link_name)
                ,lang_obj       =page_object.content[page_object.lang];

            if(isset(page_object.link.link_drop['/'+link_name]))
                if(page_object.link.link_drop['/'+link_name]){

                    delete page_object.link.link_drop['/'+link_name];

                    return false;

                }

            let  start                  =get_url_param('start')
                ,len                    =get_url_param('len')
                ,model_zone_ID          =get_url_param('model_zone_ID')
                ,face_ID                =get_url_param('face_ID')
                ,city                   =get_url_param('city')
                ,source_ID              =get_url_param('source_ID')
                ,source_link            =get_url_param('source_link')
                ,is_uploaded_image      =get_url_param('is_uploaded_image')
                ,sort_by                =get_url_param('sort_by')
                ,sort_direction         =get_url_param('sort_direction')
                ,is_not_one_face        =get_url_param('is_not_one_face')
                ,face_preview_len       =0;

            if(isset($d('root_face_preview_list'))){

                face_preview_len=$d('root_face_preview_list').getElementsByClassName('root_face_image_random').length;

                if(face_preview_len===0)
                    post+='&is_need_face_preview_list=1';

            }
            else
                post+='&is_need_face_preview_list=1';
            // else if(empty(link_name))
            //     post+='&is_need_face_preview_list=1';

            if(!empty(start))
                post+='&start='+uniEncode(start);

            if(!empty(len))
                post+='&len='+uniEncode(len);

            if(!empty(face_ID))
                post+='&face_ID='+uniEncode(face_ID);

            if(!empty(model_zone_ID))
                post+='&model_zone_ID='+uniEncode(model_zone_ID);

            if(!empty(city))
                post+='&city='+uniEncode(city);

            if(!empty(source_ID))
                post+='&source_ID='+uniEncode(source_ID);

            if(!empty(source_link))
                post+='&source_link='+uniEncode(source_link);

            if(!empty(sort_by))
                post+='&sort_by='+uniEncode(sort_by);

            if(!empty(sort_direction))
                post+='&sort_direction='+uniEncode(sort_direction);

            if(!empty(is_uploaded_image))
                post+='&is_uploaded_image='+uniEncode(is_uploaded_image);

            if(!empty(is_not_one_face))
                post+='&is_not_one_face='+uniEncode(is_not_one_face);

            send({
                'scriptPath':'/api/json/get_trigger',
                'postData':post,
                'onLoad':page_object.link.preload.show,
                'onComplete':function(j,worktime){

                    let dataTemp=j.responseText;

                    trace('>>>>>>>>>> API: get trigger');
                    trace('link_name: '+link_name);

                    trace(dataTemp);

                    let data=jsonDecode(dataTemp);

                    trace(data);
                    trace_worktime(worktime,data);

                    if(isset(data['token'])){

                        trace('isset token in response');

                        if(getCookie('token')!==data['token']){

                            let date=new Date();

                            trace('change cookie');
                            trace('old token: '+getCookie('token'));

                            date.setTime(date.getTime()+(365*24*60*60*1000));

                            setCookie('token',data['token'],date.toUTCString(),'/',document.location.host,document.location.host==='shluham.net');

                            trace('new token: '+getCookie('token'));

                        }

                    }

                    if(!page_object.link.error.init(data))
                        if(isset(data['redirect'])){

                            if(isset(data['token'])){

                                trace('isset token in response');

                                if(getCookie('token')!==data['token']){

                                    let date=new Date();

                                    trace('change cookie');
                                    trace('old token: '+getCookie('token'));

                                    date.setTime(date.getTime()+(365*24*60*60*1000));

                                    setCookie('token',data['token'],date.toUTCString(),'/',document.location.host,true);

                                    trace('new token: '+getCookie('token'));

                                }

                            }

                            trace('>>>>>');
                            trace('redirect: '+data['redirect']);

                            if(data['redirect']==='/auth'){

                                if(isset(page_object.action.profile))
                                    if(isset(page_object.action.profile.data['user']))
                                        if(isset(page_object.action.profile.section_change_workflow))
                                            page_object.action.profile.section_change_workflow.profile_logout.init();

                                if(isset(page_object.action.admin))
                                    if(isset(page_object.action.admin.data['user']))
                                        if(isset(page_object.action.admin.section_change_workflow))
                                            page_object.action.admin.section_change_workflow.admin_logout.init();
                            }

                            setUrl(data['title'],data['redirect']);

                        }
                        else if(isset(data['init'])||isset(data['init_list'])){

                            trace('case #2');

                            if(isset(data['title']))
                                window.document.title=data['title'];

                            if(isset(data['token'])){

                                trace('isset token in response');
                                trace(getCookie('token'));
                                trace(data['token']);

                                if(getCookie('token')!==data['token']){

                                    let date=new Date();

                                    trace('change cookie');
                                    trace('old token: '+getCookie('token'));

                                    date.setTime(date.getTime()+(365*24*60*60*1000));

                                    if(window.location.host!=='shluham.net')
                                        setCookie('token',data['token'],date.toUTCString(),'/',document.location.host,false);
                                    else
                                        setCookie('token',data['token'],date.toUTCString(),'/',document.location.host,true);

                                    trace('new token: '+getCookie('token'));

                                }

                            }

                            page_object.init(data);

                        }
                        else{

                            if(isset(page_object.action.profile))
                                if(isset(page_object.action.profile.data['user']))
                                    if(isset(page_object.action.profile.section_change_workflow))
                                        page_object.action.profile.section_change_workflow.profile_logout.init();

                            if(isset(page_object.action.admin))
                                if(isset(page_object.action.admin.data['user']))
                                    if(isset(page_object.action.admin.section_change_workflow))
                                        page_object.action.admin.section_change_workflow.admin_logout.init();

                            setUrl(data['title'],data['redirect']);

                        }

                    //page_object.link.preload.un_show();

                }
            });

            return true;
            
        }
    },
    'error':{
        'init':function(data){

            return false;

        }
    }
};