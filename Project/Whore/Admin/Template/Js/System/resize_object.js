var resize_object={
    'init':function(){

        resize_object.action.all();
        resize_object.action.auth_block();
        resize_object.action.admin_block();
        resize_object.action.partner_block();
        resize_object.action.resident_block();
        resize_object.action.dialog();

    },
    'action':{
        'all':function(){

            // $s('all').width             =winSize.winWidth+'px';
            // $s('all').minHeightheight   =winSize.winHeight+'px';

        },
        'auth_block':function(){

            if(isset(page_object.action.auth))
                page_object.action.auth.action.resize();

        },
        'admin_block':function(){

            if(isset(page_object.action.admin))
                if(isset(page_object.action.admin.data['user']))
                    page_object.action.admin.action.resize();

        },
        'partner_block':function(){

            if(isset(page_object.action.partner))
                if(isset(page_object.action.partner.data['user']))
                    page_object.action.partner.action.resize();

        },
        'resident_block':function(){

            if(isset(page_object.action.resident))
                if(isset(page_object.action.resident.data['user']))
                    page_object.action.resident.action.resize();

        },
        'dialog':function(){

            if(isset(page_object.dialog))
                page_object.dialog.action.resize();

        }
    }
};