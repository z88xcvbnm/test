var bubblegum_object={
    'is_first':true,
    'init':function(){

        bubblegum_object.is_first=false;

    }
};


var start_function=function(){

    if(!domLoad.load){

        domLoad.load=true;

        winSize.checkBrowserSize.options(bubblegum_object.init);

        object_object.init();

    }

};

domLoad.set(start_function);

var object_object={
    'data':[],
    'all':[],
    'object_trig':null,
    'place_ID':null,
    'init':function(){

        object_object.create();

    },
    'add_object':function(i){

        var  inner      =''
            ,list       =object_object.data
            ,i1;

        inner+='<div id="object_logo_'+i+'" class="object_logo" style="background: url('+list[i]['image']+') no-repeat;"></div>';

        inner+='<div id="object_content_'+i+'"class="object_content">';

        inner+='<div id="object_ID_'+i+'" class="object_title">';
            inner+='<span>object_ID: </span>';
            inner+='<span>'+list[i]['ID']+'</span>';
        inner+='</div>';

        inner+='<div id="object_title_'+i+'" class="object_title">';
            inner+='<span>Title: </span>';
            inner+='<span>'+stripSlashes(list[i]['name'])+'</span>';
        inner+='</div>';

        inner+='<div id="object_site_'+i+'" class="object_title">';

            inner+='<span>Site: </span>';
            inner+='<a href="'+stripSlashes(list[i]['site'])+'" target="_blank">'+stripSlashes(list[i]['site'])+'</a>';

        inner+='</div>';

        inner+='<div id="object_level_'+i+'" class="object_vertex">';

        inner+='<span>Levels: </span>';

        for(i1=0;i1<object_object.data[i]['level_list'].length;i1++){

            inner+=(i1==0?'':',')+'<span id="object_level_item_'+i+'_'+i1+'" class="object_level_item">'+object_object.data[i]['level_list'][i1]['level']+'</span>';

        }

        inner+='</div>';

        inner+='<div id="object_vertex_'+i+'" class="object_vertex">';

        inner+='<span>Vertexes: </span>';

        for(i1=0;i1<object_object.data[i]['vertex_ID_list'].length;i1++){

            inner+=(i1==0?'':',')+'<span>'+object_object.data[i]['vertex_ID_list'][i1]['ID']+'</span>';

        }

        inner+='</div>';

        inner+='<div id="object_info_'+i+'" class="object_info">'+stripSlashes(list[i]['info'])+'</div>';

        inner+='<div id="object_info_controls_'+i+'" class="object_controls" style="text-align: right;">';

        inner+='<div id="object_button_info_open_'+i+'" class="object_button">Развернуть</div>';
        inner+='<div id="object_button_info_edit_'+i+'" class="object_button">Редактировать</div>';

        inner+='</div>';

        inner+='</div>';

        inner+='<div id="object_controls_'+i+'" class="object_controls">';

        inner+='<div id="object_button_product_'+i+'" class="object_button">Продукция</div>';
        inner+='<div id="object_button_brand_'+i+'" class="object_button">Бренды</div>';
        inner+='<div id="object_button_root_group_'+i+'" class="object_button">Основная группа</div>';
        inner+='<div id="object_button_group_'+i+'" class="object_button">Группы</div>';

        inner+='</div>';

        var el=addElement({
            'tag':'div',
            'id':'object_item_'+i,
            'class':'object_item',
            'inner':inner
        });

        $d('all').appendChild(el);

        $d('object_button_info_open_'+i).onclick         =object_object.action.open_info.init;
        $d('object_button_info_edit_'+i).onclick         =object_object.action.get_info.init;
        $d('object_button_brand_'+i).onclick             =object_object.action.get_brand.init;
        $d('object_button_product_'+i).onclick           =object_object.action.get_product.init;
        $d('object_button_group_'+i).onclick             =object_object.action.get_group.init;
        $d('object_button_root_group_'+i).onclick        =object_object.action.get_root_group.init;

    },
    'get_select_input':function(){

        var  inner=''
            ,list       =object_object.all
            ,list_len   =count(list)
            ,i;

        inner+='<select id="object_add_select" class="object_title_text">';

        inner+='<option value="0"></option>';

        for(i=0;i<list_len;i++)
            inner+='<option value="'+list[i]['ID']+'">'+stripSlashes(list[i]['name'])+'</option>';

        inner+='</select>';

        return inner;

    },
    'create':function(){

        var  inner              =''
            ,list               =object_object.data
            ,len                =count(list)
            ,i
            ,i1
            ,el
            ,inner_select       =object_object.get_select_input();

        inner+='<div class="object_label">Добавление магазина</div>';

        inner+='<div class="object_title">';
        inner+=inner_select;
        inner+='</div>';

        inner+='<div class="object_title">';
        inner+='<input id="object_add_vertex" class="object_title_text" type="text" value="" />';
        inner+='</div>';

        inner+='<div class="object_title">';
        inner+='<div id="object_add_button" class="object_button">Добавить</div>';
        inner+='</div>';

        inner+='</div>';

        el=addElement({
            'tag':'div',
            'id':'object_add_block',
            'class':'object_content',
            'style':'margin-bottom: 50px; border-bottom: 1px solid #000000;',
            'inner':inner
        });

        $d('all').appendChild(el);

        $d('object_add_button').onclick=object_object.action.add_object.init;

        for(i=0;i<len;i++){

            object_object.data[i]['is_close_info']=false;

            object_object.add_object(i);

            for(i1=0;i1<object_object.data[i]['level_list'].length;i1++)
                $d('object_level_item_'+i+'_'+i1).onclick=object_object.action.get_map.init;

        }

    },
    'action':{
        'get_map':{
            'data':{},
            'init':function(){

                var  index_list         =this.id.split('_')
                    ,object_index        =index_list[3]
                    ,level_index        =index_list[4];

                object_object.action.get_map.get(object_index,level_index);
                object_object.action.get_map.create_map();
                object_object.action.get_map.zoom.init();
                object_object.action.get_map.move.init();

            },
            'set_map_size':function(map_w,map_h,mouse_x,mouse_y){

                var map_data=object_object.action.get_map.data;

                if(map_h*4<winSize.winHeight||map_h>map_data['h'])
                    return false;

                map_data['x']               =isset(mouse_x)?mouse_x:map_data['x'];
                map_data['y']               =isset(mouse_y)?mouse_y:map_data['y'];

                map_data['old_show_w']      =map_data['show_w'];
                map_data['old_show_h']      =map_data['show_h'];

//                map_data['show_h']          =(map_w*map_data['h'])/map_data['w'];
//                map_data['show_w']          =(map_data['show_h']*map_data['w'])/map_data['h'];

                map_data['show_h']          =map_h;
                map_data['show_w']          =(map_data['w']/map_data['h'])*map_data['show_h'];

                map_data['scale']           =map_data['show_h']/winSize.winHeight;

                map_data['map_left_w']      =map_data['x']+map_data['m_l'];
                map_data['map_left_h']      =map_data['y']+map_data['m_t'];

                map_data['old_map_left']    =(map_data['map_left_w']/map_data['old_show_w']);
                map_data['old_map_top']     =(map_data['map_left_h']/map_data['old_show_h']);

                map_data['map_delta_w']     =(map_data['show_w']-map_data['old_show_w'])/2;
                map_data['map_delta_h']     =(map_data['show_h']-map_data['old_show_h'])/2;

                map_data['map_left']        =(map_data['show_w']*map_data['old_map_left']-map_data['x']-map_data['map_delta_w']);
                map_data['map_top']         =(map_data['show_h']*map_data['old_map_top']-map_data['y']-map_data['map_delta_h']);

//                map_data['map_left']        =(map_data['show_w']*map_data['old_map_left']-map_data['x']-map_data['map_delta_w']);//+map_data['map_delta_w'];//-map_data['x']
//                map_data['map_top']         =(map_data['show_h']*map_data['old_map_top']-map_data['y']-map_data['map_delta_h']);//+map_data['map_delta_h'];//-map_data['y']

                map_data['m_l']             =map_data['map_left']/map_data['scale'];
                map_data['m_t']             =map_data['map_top']/map_data['scale'];

                var el_size=elementSize.getSize($d('map_image'));

                trace(el_size);

                object_object.action.get_map.data=map_data;

//                $s('map_image').transform   ='scale(1)';

//                $s('map_image').transformOrigin='0% 0%';
                $s('map_image').transformOrigin=mouse_x+'px '+mouse_y+'px';

                $s('map_image').transform   ='scale('+map_data['scale']+')';
//                $s('map_image').transform   ='scale('+map_data['scale']+') translate('+map_data['m_l']+'px,'+map_data['m_t']+'px)';
//                $s('map_image').margin      =(-1*map_data['m_t'])+'px 0 0 '+(-1*map_data['m_l'])+'px';

                return true;

            },
            'create_map':function(){

                var  map_data       =object_object.action.get_map.data
                    ,show_h         =winSize.winHeight
                    ,show_w         =(map_data['w']/map_data['h'])*show_h
//                    ,show_w         =(winSize.winHeight*map_data['w'])/map_data['h']
//                    ,show_h         =(show_w*map_data['h'])/map_data['w']
                    ,el
                    ,inner          ='';

                trace(show_w+' x '+show_h);

                inner+='<div id="map_block" class="map_block">';
                    inner+='<div id="map_image" class="map_image" style="background: url('+map_data['map_link']+') no-repeat; width: '+show_w+'px; height: '+show_h+'px; background-size: contain;"></div>';
                inner+='</div>';

                el=addElement({
                    'tag':'div',
                    'id':'level_block',
                    'class':'level_block',
                    'inner':inner
                });

                $d('all').appendChild(el);

                object_object.action.get_map.set_map_size(show_w,show_h,0,0);
//                object_object.action.get_map.set_map_size(winSize.winWidth,winSize.winHeight,((winSize.winWidth-show_w)/2),((winSize.winHeight-show_h)/2));

            },
            'get':function(object_index,level_index){

                var  map_data       =object_object.data[object_index]['level_list'][level_index]
                    ,show_h         =winSize.winHeight
                    ,show_w         =(map_data['w']/map_data['h'])*show_h
                    ,m_l            =(show_w-winSize.winWidth)/2
                    ,m_t            =(show_h-winSize.winHeight)/2;

                trace('show_w: '+show_w+' show_y: '+show_h);
                trace('m_t: '+m_t+' m_l: '+m_l);

                object_object.action.get_map.data={
                    'map_ID':map_data['map_ID'],
                    'x':0,
                    'y':0,
                    'const_w':show_w,
                    'const_h':show_h,
                    'w':map_data['w'],
                    'h':map_data['h'],
                    'show_w':show_w,
                    'show_h':show_h,
                    'real_w':map_data['real_w'],
                    'real_h':map_data['real_h'],
                    'm_l':m_l,
                    'm_t':m_t,
                    'map_link':'/images/map/'+map_data['map_ID']+'/source.png'
                };

            },
            'zoom':{
                'init':function(){

                    $d('map_block').onwheel=function(event){

                        object_object.action.get_map.zoom.set(event);

                        event.preventDefault();
                        event.returnValue=false;
                        return false;

                    };

                },
                'set':function(event){

                    var  delta          =wheel_object.get_delta(event)
                        ,map_data       =object_object.action.get_map.data
                        ,mouse_data     =mouse.getPosition(event)
                        ,zoom           =.2;

                    if(delta>0){

                        object_object.action.get_map.set_map_size(map_data['show_w']*(1+zoom),map_data['show_h']*(1+zoom),mouse_data['x'],mouse_data['y']);

                    }
                    else{

                        object_object.action.get_map.set_map_size(map_data['show_w']*(1-zoom),map_data['show_h']*(1-zoom),mouse_data['x'],mouse_data['y']);

                    }

                }
            },
            'move':{
                'is_move':false,
                'init':function(){

                    $d('map_block').onmousedown     =object_object.action.get_map.move.down;
                    $d('map_block').onmousemove     =object_object.action.get_map.move.move;
                    $d('map_block').onmouseup       =object_object.action.get_map.move.up;

                },
                'down':function(event){

                    object_object.action.get_map.move.is_move=true;

                    var  map_data           =object_object.action.get_map.data
                        ,mouse_data         =mouse.getPosition(event);

                    map_data['mouse_x']     =mouse_data['x'];
                    map_data['mouse_y']     =mouse_data['y'];

//                    $s('map_image').transformOrigin     ='100px 100px';
                    $s('map_image').transition          ='none';
                    $s('map_image').transformOrigin     =map_data['mouse_x']+'px '+map_data['mouse_y']+'px';

                    trace('mouse down');

                },
                'move':function(event){

                    if(object_object.action.get_map.move.is_move){

                        var  map_data       =object_object.action.get_map.data
                            ,mouse_data     =mouse.getPosition(event);

//                        map_data['m_l']     =mouse_data['x']-map_data['const_w']/2;
//                        map_data['m_t']     =mouse_data['y']-map_data['const_h']/2;

                        map_data['m_l']     =mouse_data['x'];
                        map_data['m_t']     =mouse_data['y'];

                        object_object.action.get_map.data=map_data;

                        $s('map_image').transform='scale('+map_data['scale']+') translate('+map_data['m_l']+'px,'+map_data['m_t']+'px)';

                        event.preventDefault();
                        event.returnValue=false;
                        return false;

                    }

                    return true;

                },
                'up':function(){

                    $s('map_image').transition='all .2s ease';

                    object_object.action.get_map.move.is_move=false;

                }
            }
        },
        'add_object':{
            'init':function(){

                object_object.action.add_object.save();

            },
            'save':function(){

                var  list           =object_object.data
                    ,list_len       =count(list)
                    ,i
                    ,object_ID       =$d('object_add_select').value
                    ,isset_object    =false;

                for(i=0;i<list_len;i++)
                    if(list[i]['ID']==object_ID){

                        isset_object=true;

                        break;

                    }

                if(!isset_object&&object_ID!=0){

                    $s('object_add_button').cursor='progress';
                    $d('object_add_button').onclick=function(){};
                    opaElement.setOpacity($d('object_add_button'),.5);

                    send({
                        'scriptPath':'/api/json/add_object_trig_vertex',
                        'postData':'place_ID='+uniEncode(object_object.place_ID)+'&object_ID='+object_ID+'&vertex_ID_list='+uniEncode($d('object_add_vertex').value),
                        'onComplete':function(j,worktime){

                            var dataTemp    =j.responseText;
                            trace(dataTemp);
                            var data        =jsonDecode(dataTemp);


                            if(isset(data['error']))
                                alert(data['error']['content']);
                            else{

                                var index=count(object_object.data);

                                object_object.data.push(data);

                                object_object.add_object(index);

                            }

                            $d('object_add_select').value=0;
                            $d('object_add_select').focus();
                            $d('object_add_vertex').value=parseInt($d('object_add_vertex').value)+1;

                            $s('object_add_button').cursor='pointer';
                            $d('object_add_button').onclick=object_object.action.add_object.init;
                            opaElement.setOpacity($d('object_add_button'),1);

                        }
                    });

                }

            }
        },
        'get_info':{
            'init':function(){

                var  index=this.id.split('_')[4];

                object_object.action.get_info.get(index);

            },
            'get':function(index){

                var inner='';

                inner+='<div id="object_info_label_'+index+'" class="object_label">Редактирование информации о магазине</div>';

                inner+='<input id="object_title_text_'+index+'" class="object_title_text" value="'+stripSlashes(object_object.data[index]['name'])+'" />';

                inner+='<input id="object_site_text_'+index+'" class="object_title_text" value="'+stripSlashes(object_object.data[index]['site'])+'" />';

                var vertex_list='',i;

                for(i=0;i<object_object.data[index]['vertex_ID_list'].length;i++)
                    vertex_list+=(i==0?'':',')+object_object.data[index]['vertex_ID_list'][i]['ID'];

                inner+='<textarea id="object_vertex_text_'+index+'" class="object_brand_list">'+vertex_list+'</textarea>';

                inner+='<textarea id="object_info_text_'+index+'" class="object_brand_list">'+stripSlashes(object_object.data[index]['info'])+'</textarea>';

                inner+='<div id="object_button_info_save_'+index+'" class="object_button">Сохранить</div>';
                inner+='<div id="object_button_info_close_'+index+'" class="object_button">Закрыть</div>';

                $s('object_title_'+index).display        ='none';
                $s('object_site_'+index).display         ='none';
                $s('object_level_'+index).display        ='none';
                $s('object_vertex_'+index).display       ='none';

                $d('object_info_'+index).innerHTML       =inner;

                $d('object_button_info_save_'+index).onclick=object_object.action.get_info.save;
                $d('object_button_info_close_'+index).onclick=object_object.action.get_info.close;

                $s('object_button_info_edit_'+index).display='none';
                $s('object_button_info_open_'+index).display='none';

                $s('object_info_'+index).height='auto';

                object_object.data[index]['is_close_info']=false;

                $d('object_button_info_open_'+index).innerHTML='Развернуть';

            },
            'save':function(){

                var  index      =this.id.split('_')[4]
                    ,object_ID   =object_object.data[index]['ID'];

                $s('object_button_info_save_'+index).cursor='progress';
                $d('object_button_info_save_'+index).onclick=function(){};
                opaElement.setOpacity($d('object_button_info_save_'+index),.5);

                $s('object_button_info_close_'+index).cursor='progress';
                $d('object_button_info_close_'+index).onclick=function(){};
                opaElement.setOpacity($d('object_button_info_close_'+index),.5);

                send({
                    'scriptPath':'/api/json/save_object_info',
                    'postData':'place_ID='+uniDecode(object_object.place_ID)+'&object_ID='+object_ID+'&info='+uniEncode($d('object_info_text_'+index).value)+'&name='+uniEncode($d('object_title_text_'+index).value)+'&vertex_ID_list='+uniEncode($d('object_vertex_text_'+index).value)+'&site='+uniEncode($d('object_site_text_'+index).value),
                    'onComplete':function(j,worktime){

                        var dataTemp    =j.responseText;
                        trace(dataTemp);
                        var data        =jsonDecode(dataTemp);
                        trace(data);
trace_worktime(worktime,data);

                        if(isset(data['error']))
                            alert(data['error']['content']);
                        else{

                            object_object.data[index]['name']                =$d('object_title_text_'+index).value;
                            object_object.data[index]['site']                =$d('object_site_text_'+index).value;
                            object_object.data[index]['info']                =$d('object_info_text_'+index).value;
                            object_object.data[index]['vertex_ID_list']      =data['vertex_ID_list'];
                            object_object.data[index]['level_list']          =data['level_list'];

//                            object_object.action.get_info.close(index);
                            $d('object_button_info_close_'+index).onclick=object_object.action.get_info.close;
                            $d('object_button_info_close_'+index).click();

                        }

                    }
                });

            },
            'close':function(){

                var  index=this.id.split('_')[4]
                    ,i
                    ,i1;

                $d('object_info_'+index).innerHTML=object_object.data[index]['info'];

                removeElement($d('object_info_label_'+index));
                removeElement($d('object_brand_list_'+index));
                removeElement($d('object_button_info_save_'+index));
                removeElement($d('object_button_info_close_'+index));

                $s('object_button_info_edit_'+index).display='inline-block';
                $s('object_button_info_open_'+index).display='inline-block';

                $s('object_info_'+index).height='40px';

                object_object.data[index]['is_close_info']=false;

                $d('object_button_info_open_'+index).innerHTML='Развернуть';

                $s('object_title_'+index).display        ='block';
                $s('object_site_'+index).display         ='block';
                $s('object_level_'+index).display        ='block';
                $s('object_vertex_'+index).display       ='block';

                $d('object_title_'+index).innerHTML      ='<span>Title: </span><span>'+stripSlashes(object_object.data[index]['name'])+'</span>';
                $d('object_site_'+index).innerHTML       ='<span>Site: </span><a href="'+stripSlashes(object_object.data[index]['site'])+'" target="_blank">'+stripSlashes(object_object.data[index]['site'])+'</a>';
                $d('object_info_'+index).innerHTML       =stripSlashes(object_object.data[index]['info']);

                var level_list='';

                level_list+='<span>Levels: </span>';

                for(i1=0;i1<object_object.data[index]['level_list'].length;i1++){

                    level_list+=(i1==0?'':',')+'<span id="object_level_item_'+i+'_'+i1+'" class="object_level_item">'+object_object.data[index]['level_list'][i1]['level']+'</span>';

                }

                $d('object_level_'+index).innerHTML=level_list;

                var vertex_list='';

                vertex_list+='<span>Vertexes: </span>';

                for(i1=0;i1<object_object.data[index]['vertex_ID_list'].length;i1++){

                    vertex_list+=(i1==0?'':',')+'<span>'+object_object.data[index]['vertex_ID_list'][i1]['ID']+'</span>';

                }

                $d('object_vertex_'+index).innerHTML=vertex_list;

            }
        },
        'open_info':{
            'init':function(){

                var index=this.id.split('_')[4];

                if(object_object.data[index]['is_close_info']){

                    object_object.data[index]['is_close_info']=false;

                    $s('object_info_'+index).height='40px';

                    $d('object_button_info_open_'+index).innerHTML='Развернуть';

                }
                else{

                    object_object.data[index]['is_close_info']=true;

                    $s('object_info_'+index).height=$d('object_info_'+index).scrollHeight+'px';

                    $d('object_button_info_open_'+index).innerHTML='Свернуть';

                }

            }
        },
        'get_brand':{
            'init':function(){

                var index=this.id.split('_')[3];

                object_object.action.get_brand.get(index,object_object.data[index]['ID']);

            },
            'get':function(index,object_ID){

                $s('object_button_brand_'+index).display             ='none';
                $s('object_button_group_'+index).display             ='none';
                $s('object_button_product_'+index).display           ='none';
                $s('object_button_root_group_'+index).display        ='none';

                send({
                    'scriptPath':'/api/json/get_brand_trig_object_list',
                    'postData':'object_ID='+object_ID,
                    'onComplete':function(j,worktime){

                        var dataTemp= j.responseText;
                        var data=jsonDecode(dataTemp);

                        if(isset(data['error']))
                            alert(data['error']['content']);
                        else{

                            var inner='';

                            inner+='<div id="object_brand_label_'+index+'" class="object_label">Редактирование списка брендов</div>';

                            inner+='<textarea id="object_brand_list_'+index+'" class="object_brand_list">';

                            for(var i=0;i<count(data['brand_list']);i++){

                                inner+=(i==0?'':",\n")+stripSlashes(data['brand_list'][i]['name']);

                            }

                            inner+='</textarea>';

                            inner+='<div id="object_brand_list_save_'+index+'" class="object_button">Сохранить</div>';
                            inner+='<div id="object_brand_list_close_'+index+'" class="object_button">Закрыть</div>';

                            var el=addElement({
                                'tag':'div',
                                'id':'object_brand_block_'+index,
                                'class':'object_brand_block',
                                'inner':inner
                            });

                            $d('object_content_'+index).appendChild(el);

                            $d('object_brand_list_save_'+index).onclick=object_object.action.get_brand.save;
                            $d('object_brand_list_close_'+index).onclick=object_object.action.get_brand.close;

                        }

//                        $d('object_button_brand_'+index).onclick=object_object.action.get_product.init;
//                        $s('object_button_brand_'+index).cursor='pointer';
//
//                        opaElement.setOpacity($d('object_button_brand_'+index),1);

                    }
                });

            },
            'save':function(){

                var  index      =this.id.split('_')[4]
                    ,object_ID   =object_object.data[index]['ID'];

                $d('object_brand_list_save_'+index).onclick=function(){};
                $s('object_brand_list_save_'+index).cursor='progress';

                $d('object_brand_list_close_'+index).onclick=function(){};
                $s('object_brand_list_close_'+index).cursor='progress';

                opaElement.setOpacity($d('object_brand_list_save_'+index),.5);
                opaElement.setOpacity($d('object_brand_list_close_'+index),.5);

                send({
                    'scriptPath':'/api/json/save_brand_trig_object_list',
                    'postData':'object_ID='+object_ID+'&content='+uniEncode($d('object_brand_list_'+index).value)+'&trig='+uniEncode(object_object.object_trig),
                    'onComplete':function(j,worktime){

                        var dataTemp    =j.responseText;
                        trace(dataTemp);
                        var data        =jsonDecode(dataTemp);

                        $d('object_brand_list_close_'+index).onclick=object_object.action.get_brand.close;
                        $d('object_brand_list_close_'+index).click();

                    }

                });

            },
            'close':function(){

                var  index=this.id.split('_')[4];

                $s('object_button_brand_'+index).display             ='inline-block';
                $d('object_button_brand_'+index).onclick             =object_object.action.get_brand.init;

                $s('object_button_product_'+index).display           ='inline-block';
                $d('object_button_product_'+index).onclick           =object_object.action.get_product.init;

                $s('object_button_group_'+index).display             ='inline-block';
                $d('object_button_group_'+index).onclick             =object_object.action.get_group.init;

                $s('object_button_root_group_'+index).display        ='inline-block';
                $d('object_button_root_group_'+index).onclick        =object_object.action.get_root_group.init;

                removeElement($d('object_brand_block_'+index));

            }
        },
        'get_product':{
            'init':function(){

                var index=this.id.split('_')[3];

                object_object.action.get_product.get(index,object_object.data[index]['ID']);

            },
            'get':function(index,object_ID){

                $s('object_button_brand_'+index).display             ='none';
                $s('object_button_group_'+index).display             ='none';
                $s('object_button_product_'+index).display           ='none';
                $s('object_button_root_group_'+index).display        ='none';

                send({
                    'scriptPath':'/api/json/get_product_trig_object_list',
                    'postData':'object_ID='+object_ID,
                    'onComplete':function(j,worktime){

                        var dataTemp= j.responseText;
                        var data=jsonDecode(dataTemp);

                        if(isset(data['error']))
                            alert(data['error']['content']);
                        else{

                            var inner='';

                            inner+='<div id="object_product_label_'+index+'" class="object_label">Редактирование списка продуктов</div>';

                            inner+='<textarea id="object_product_list_'+index+'" class="object_product_list">';

                            for(var i=0;i<count(data['product_list']);i++){

                                inner+=(i==0?'':",\n")+stripSlashes(data['product_list'][i]['name']);

                            }

                            inner+='</textarea>';

                            inner+='<div id="object_product_list_save_'+index+'" class="object_button">Сохранить</div>';
                            inner+='<div id="object_product_list_close_'+index+'" class="object_button">Закрыть</div>';

                            var el=addElement({
                                'tag':'div',
                                'id':'object_product_block_'+index,
                                'class':'object_product_block',
                                'inner':inner
                            });

                            $d('object_content_'+index).appendChild(el);

                            $d('object_product_list_save_'+index).onclick=object_object.action.get_product.save;
                            $d('object_product_list_close_'+index).onclick=object_object.action.get_product.close;

                        }

                    }
                });

            },
            'save':function(){

                var  index      =this.id.split('_')[4]
                    ,object_ID   =object_object.data[index]['ID'];

                $d('object_product_list_save_'+index).onclick=function(){};
                $s('object_product_list_save_'+index).cursor='progress';

                $d('object_product_list_close_'+index).onclick=function(){};
                $s('object_product_list_close_'+index).cursor='progress';

                opaElement.setOpacity($d('object_product_list_save_'+index),.5);
                opaElement.setOpacity($d('object_product_list_close_'+index),.5);

                send({
                    'scriptPath':'/api/json/save_product_trig_object_list',
                    'postData':'object_ID='+object_ID+'&content='+uniEncode($d('object_product_list_'+index).value),
                    'onComplete':function(j,worktime){

                        var dataTemp    =j.responseText;
                        trace(dataTemp);
                        var data        =jsonDecode(dataTemp);

                        $d('object_product_list_close_'+index).onclick=object_object.action.get_product.close;
                        $d('object_product_list_close_'+index).click();

                    }

                });

            },
            'close':function(){

                var index=this.id.split('_')[4];

                $s('object_button_brand_'+index).display             ='inline-block';
                $d('object_button_brand_'+index).onclick             =object_object.action.get_brand.init;

                $s('object_button_product_'+index).display           ='inline-block';
                $d('object_button_product_'+index).onclick           =object_object.action.get_product.init;

                $s('object_button_group_'+index).display             ='inline-block';
                $d('object_button_group_'+index).onclick             =object_object.action.get_group.init;

                $s('object_button_root_group_'+index).display        ='inline-block';
                $d('object_button_root_group_'+index).onclick        =object_object.action.get_root_group.init;

                removeElement($d('object_product_block_'+index));

            }
        },
        'get_group':{
            'init':function(){

                var index=this.id.split('_')[3];

                object_object.action.get_group.get(index,object_object.data[index]['ID']);

            },
            'get':function(index,object_ID){

                $s('object_button_brand_'+index).display             ='none';
                $s('object_button_group_'+index).display             ='none';
                $s('object_button_product_'+index).display           ='none';
                $s('object_button_root_group_'+index).display        ='none';

                send({
                    'scriptPath':'/api/json/get_group_trig_object_list',
                    'postData':'object_ID='+object_ID,
                    'onComplete':function(j,worktime){

                        var dataTemp= j.responseText;
                        trace(dataTemp);
                        var data=jsonDecode(dataTemp);
                        trace(data);
trace_worktime(worktime,data);
                        if(isset(data['error']))
                            alert(data['error']['content']);
                        else{

                            var  inner      =''
                                ,check      ='';

                            inner+='<div id="object_group_label_'+index+'" class="object_label">Редактирование групп</div>';

                            inner+='<div id="object_group_list_'+index+'" class="object_group_list">';

                            for(var i=0;i<count(data['list']);i++){

                                check=data['list'][i]['is_show']?' checked="checked"':'';

                                inner+='<div id="object_group_check_block_'+i+'" class="object_group">';
                                inner+='<label>';

                                    inner+='<input type="checkbox" id="object_group_check_'+index+'_'+i+'" value="'+data['list'][i]['ID']+'"'+check+' />';
                                    inner+=stripSlashes(data['list'][i]['name']);

                                inner+='</label>';
                                inner+='</div>';

                            }

                            inner+='</div>';

                            inner+='<div id="object_group_list_save_'+index+'" class="object_button">Сохранить</div>';
                            inner+='<div id="object_group_list_close_'+index+'" class="object_button">Закрыть</div>';

                            var el=addElement({
                                'tag':'div',
                                'id':'object_group_block_'+index,
                                'class':'object_group_block',
                                'inner':inner
                            });

                            $d('object_content_'+index).appendChild(el);

                            $d('object_group_list_save_'+index).onclick=object_object.action.get_group.save;
                            $d('object_group_list_close_'+index).onclick=object_object.action.get_group.close;

                        }

                    }
                });

            },
            'save':function(){

                var  index      =this.id.split('_')[4]
                    ,object_ID   =object_object.data[index]['ID']
                    ,list       =$d('object_group_list_'+index).getElementsByTagName('input')
                    ,list_len   =count(list)
                    ,i
                    ,val        ='';

                for(i=0;i<list_len;i++)
                    if($d('object_group_check_'+index+'_'+i).checked)
                        val+=(i==0?'':',')+$d('object_group_check_'+index+'_'+i).value;

                $d('object_group_list_save_'+index).onclick=function(){};
                $s('object_group_list_save_'+index).cursor='progress';

                $d('object_group_list_close_'+index).onclick=function(){};
                $s('object_group_list_close_'+index).cursor='progress';

                opaElement.setOpacity($d('object_group_list_save_'+index),.5);
                opaElement.setOpacity($d('object_group_list_close_'+index),.5);

                send({
                    'scriptPath':'/api/json/save_group_trig_object_list',
                    'postData':'object_ID='+object_ID+'&group_ID_list='+uniEncode(val),
                    'onComplete':function(j,worktime){

                        var dataTemp    =j.responseText;
                        trace(dataTemp);
                        var data        =jsonDecode(dataTemp);

                        $d('object_group_list_close_'+index).onclick=object_object.action.get_group.close;
                        $d('object_group_list_close_'+index).click();

                    }

                });

            },
            'close':function(){

                var index=this.id.split('_')[4];

                $s('object_button_brand_'+index).display             ='inline-block';
                $d('object_button_brand_'+index).onclick             =object_object.action.get_brand.init;

                $s('object_button_product_'+index).display           ='inline-block';
                $d('object_button_product_'+index).onclick           =object_object.action.get_product.init;

                $s('object_button_group_'+index).display             ='inline-block';
                $d('object_button_group_'+index).onclick             =object_object.action.get_group.init;

                $s('object_button_root_group_'+index).display        ='inline-block';
                $d('object_button_root_group_'+index).onclick        =object_object.action.get_root_group.init;

                removeElement($d('object_group_block_'+index));

            }
        },
        'get_root_group':{
            'init':function(){

                var index=this.id.split('_')[4];

                object_object.action.get_root_group.get(index,object_object.data[index]['ID']);

            },
            'get':function(index,object_ID){

                $s('object_button_brand_'+index).display             ='none';
                $s('object_button_group_'+index).display             ='none';
                $s('object_button_product_'+index).display           ='none';
                $s('object_button_root_group_'+index).display        ='none';

                send({
                    'scriptPath':'/api/json/get_root_group_trig_object_list',
                    'postData':'object_ID='+object_ID,
                    'onComplete':function(j,worktime){

                        var dataTemp= j.responseText;
                        trace(dataTemp);
                        var data=jsonDecode(dataTemp);
                        trace(data);
trace_worktime(worktime,data);
                        if(isset(data['error']))
                            alert(data['error']['content']);
                        else{

                            var  inner      =''
                                ,check      ='';

                            inner+='<div id="object_root_group_label_'+index+'" class="object_label">Редактирование групп</div>';

                            inner+='<div id="object_root_group_list_'+index+'" class="object_root_group_list">';

                            for(var i=0;i<count(data['list']);i++){

                                check=data['list'][i]['is_show']?' checked="checked"':'';

                                inner+='<div id="object_root_group_check_block_'+i+'" class="object_root_group">';
                                inner+='<label>';

                                    inner+='<input type="radio" name="object_root_group_'+object_ID+'" id="object_root_group_check_'+index+'_'+i+'" value="'+data['list'][i]['ID']+'"'+check+' />';
                                    inner+=stripSlashes(data['list'][i]['name']);

                                inner+='</label>';
                                inner+='</div>';

                            }

                            inner+='</div>';

                            inner+='<div id="object_root_group_list_save_'+index+'" class="object_button">Сохранить</div>';
                            inner+='<div id="object_root_group_list_close_'+index+'" class="object_button">Закрыть</div>';

                            var el=addElement({
                                'tag':'div',
                                'id':'object_root_group_block_'+index,
                                'class':'object_root_group_block',
                                'inner':inner
                            });

                            $d('object_content_'+index).appendChild(el);

                            $d('object_root_group_list_save_'+index).onclick     =object_object.action.get_root_group.save;
                            $d('object_root_group_list_close_'+index).onclick    =object_object.action.get_root_group.close;

                        }

                    }
                });

            },
            'save':function(){

                var  index      =this.id.split('_')[5]
                    ,object_ID   =object_object.data[index]['ID']
                    ,list       =$d('object_root_group_list_'+index).getElementsByTagName('input')
                    ,list_len   =count(list)
                    ,i
                    ,val        ='';

                for(i=0;i<list_len;i++)
                    if($d('object_root_group_check_'+index+'_'+i).checked){

                        val=$d('object_root_group_check_'+index+'_'+i).value;

                        break;

                    }

                $d('object_root_group_list_save_'+index).onclick         =function(){};
                $s('object_root_group_list_save_'+index).cursor          ='progress';

                $d('object_root_group_list_close_'+index).onclick        =function(){};
                $s('object_root_group_list_close_'+index).cursor         ='progress';

                opaElement.setOpacity($d('object_root_group_list_save_'+index),.5);
                opaElement.setOpacity($d('object_root_group_list_close_'+index),.5);

                send({
                    'scriptPath':'/api/json/save_root_group_trig_object_list',
                    'postData':'object_ID='+object_ID+'&group_ID='+uniEncode(val),
                    'onComplete':function(j,worktime){

                        var dataTemp    =j.responseText;
                        trace(dataTemp);
                        var data        =jsonDecode(dataTemp);

                        $d('object_root_group_list_close_'+index).onclick=object_object.action.get_root_group.close;
                        $d('object_root_group_list_close_'+index).click();

                    }

                });

            },
            'close':function(){

                var index=this.id.split('_')[5];

                $s('object_button_brand_'+index).display             ='inline-block';
                $d('object_button_brand_'+index).onclick             =object_object.action.get_brand.init;

                $s('object_button_product_'+index).display           ='inline-block';
                $d('object_button_product_'+index).onclick           =object_object.action.get_product.init;

                $s('object_button_group_'+index).display             ='inline-block';
                $d('object_button_group_'+index).onclick             =object_object.action.get_group.init;

                $s('object_button_root_group_'+index).display        ='inline-block';
                $d('object_button_root_group_'+index).onclick        =object_object.action.get_root_group.init;

                removeElement($d('object_root_group_block_'+index));

            }
        }
    }
};

