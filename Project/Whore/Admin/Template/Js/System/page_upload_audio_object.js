page_object.upload_audio={
    'data':null,
    'file_item':null,
    'index_list':{},
    'chunk_size':1024*1024*.5,
    'work':false,
    'init':function(data){

        page_object.upload_audio.controls.disable();

        var  i
            ,len
            ,limit      =isset(data['limit'])?(data['limit']<data['list'].length?data['limit']:data['list'].length):data['list'].length
            ,list       =[]
            ,temp;

        if(isset(page_object.upload_audio.data)){

            if(!isset(page_object.upload_audio.data['on_change'])&&isset(data['on_change']))
                page_object.upload_audio.data['on_change']=data['on_change'];

            if(!isset(page_object.upload_audio.data['on_progress'])&&isset(data['on_progress']))
                page_object.upload_audio.data['on_progress']=data['on_progress'];

            if(!isset(page_object.upload_audio.data['on_uploaded'])&&isset(data['on_uploaded']))
                page_object.upload_audio.data['on_uploaded']=data['on_uploaded'];

            if(!isset(page_object.upload_audio.data['on_complete'])&&isset(data['on_complete']))
                page_object.upload_audio.data['on_complete']=data['on_complete'];

            if(!isset(page_object.upload_audio.data['on_limit'])&&isset(data['on_limit']))
                page_object.upload_audio.data['on_limit']=data['on_limit'];

            if(!isset(page_object.upload_audio.data['file_extension'])&&isset(data['file_extension']))
                page_object.upload_audio.data['file_extension']=data['file_extension'];

            if(!isset(page_object.upload_audio.data['limit'])&&isset(data['limit']))
                page_object.upload_audio.data['limit']=data['limit'];

            len=page_object.upload_audio.data['list'].length;

            if(isset(data['limit']))
                if(data['limit']<=(page_object.upload_audio.data['list'].length+data['list'].length))
                    if(isset(page_object.upload_audio.data['on_limit']))
                        page_object.upload_audio.data['on_limit']();

            page_object.upload_audio.data['list']=page_object.upload_audio.data['list'].concat(data['list']);

            page_object.upload_audio.set_index_list();

            if(isset(data['limit'])){

                limit=isset(data['limit'])?(data['limit']<page_object.upload_audio.data['list'].length?data['limit']:page_object.upload_audio.data['list'].length):page_object.upload_audio.data['list'].length;

                page_object.upload_audio.data['list']=page_object.upload_audio.data['list'].slice(0,limit);

            }

            if(!page_object.upload_audio.work){

                page_object.upload_audio.work=true;

                page_object.upload_audio.change(len);

            }

        }
        else{

            if(isset(data['limit']))
                if(data['limit']<=data['list'].length)
                    if(isset(data['on_limit']))
                        data['on_limit']();

            for(i=0;i<limit;i++){

                temp=data['list'][i];

                temp['complete']        =false;
                temp['remove']          =false;
                temp['percent']         =0;
                temp['wait']            =false;

                list.push(temp);

            }

            data['list']=list;

            page_object.upload_audio.work     =true;
            page_object.upload_audio.data     =data;

            page_object.upload_audio.set_index_list();

            if(data['list'].length>0)
                page_object.upload_audio.change(0);

        }

    },
    'change':function(index){

        if(isset(page_object.upload_audio.data['list'][index]))
            if(page_object.upload_audio.data['list'][index]['remove']||page_object.upload_audio.data['list'][index]['complete']){

                if(isset(page_object.upload_audio.data['list'][++index]))
                    page_object.upload_audio.change(index);
                else
                    page_object.upload_audio.complete();

            }
            else{

                page_object.upload_audio.file_item                =page_object.upload_audio.data['list'][index];
                page_object.upload_audio.file_item['index']       =index;
                page_object.upload_audio.file_item['file_ID']    =0;

                var  file_size              =page_object.upload_audio.file_item['file']['size']
                    ,chunk_size             =page_object.upload_audio.chunk_size
                    ,chunk_len              =Math.ceil(file_size/chunk_size);

                if(isset(page_object.upload_audio.data['on_change']))
                    page_object.upload_audio.data['on_change'](page_object.upload_audio.file_item['index']);

                if(file_size<chunk_size)
                    chunk_size=file_size;

                page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['chunk_index']=0;
                page_object.upload_audio.file_item['chunk_index']=0;

                page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['chunk_len']=chunk_len;
                page_object.upload_audio.file_item['chunk_len']=chunk_len;

                page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['wait']=true;
                page_object.upload_audio.file_item['wait']=true;

                page_object.upload_audio.file_slice(0,chunk_len,file_size,0,chunk_size);

            }
        else
            page_object.upload_audio.complete();

    },
    'file_slice':function(chunk_index,chunk_len,file_size,start,end){

        if(isset(page_object.upload_audio.file_item))
            if(page_object.upload_audio.file_item['remove']||page_object.upload_audio.file_item['complete']){

                if(!page_object.upload_audio.file_item['remove'])
                    page_object.upload_audio.file_item['index']++;

                if(page_object.upload_audio.file_item['index']<page_object.upload_audio.data['list'].length)
                    page_object.upload_audio.change(page_object.upload_audio.file_item['index']);
                else
                    page_object.upload_audio.complete();

            }
            else{

                var  reader             =new FileReader()
                    ,chunk_size         =isset(page_object.upload_audio.file_item['chunk_size'])?page_object.upload_audio.file_item['chunk_size']:page_object.upload_audio.chunk_size
                    ,file_slice         =page_object.upload_audio.file_item['file'].slice(start,end)
                    ,file_mime_type     =page_object.upload_audio.file_item['file'].type
                    ,chunk_blob
                    ,post               =''
                    ,file_content_type  =page_object.upload_audio.data['file_type']
                    ,file_extension     =get_file_exctension(page_object.upload_audio.file_item['file'].name)
                    ,loop               =true;

                reader.onloadend=function(e){

                    if(e.target.readyState==FileReader.DONE){

                        chunk_blob=base64.encode(e.target.result);

                        if(end>file_size){

                            end         =file_size;
                            chunk_size  =file_size-start;
                            loop        =false;

                        }

                        post='';

                        post+='file_ID='+uniEncode(page_object.upload_audio.file_item['file_ID']);
                        post+='&file_chunk_count='+uniEncode(chunk_len);
                        post+='&file_chunk_index='+uniEncode((chunk_index+1));
                        post+='&file_chunk_size='+uniEncode(chunk_size);
                        post+='&file_size='+uniEncode(file_size);
                        post+='&file_content_type='+uniEncode(file_content_type);
                        post+='&file_extension='+uniEncode(file_extension);
                        post+='&file_mime_type='+uniEncode(file_mime_type);

                        trace('postData: '+post);
                        trace('FILE INDEX: '+page_object.upload_audio.file_item['index']);
                        trace('FILE SLICE: '+start+'/'+end+' file_size: '+file_size+' chunk_index: '+chunk_index+' chunk_len: '+chunk_len);

                        post+='&file_chunk='+uniEncode(chunk_blob);

                        send({
                            'scriptPath':'/api/json/upload_file_chunk',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                var  dataTemp
                                    ,data;

                                dataTemp        =j.responseText;
                                trace(dataTemp);
                                data            =jsonDecode(dataTemp);
                                trace(data);
trace_worktime(worktime,data);

                                trace_worktime(worktime,data);

                                if(page_object.upload_audio.file_item['remove']){

                                    if(isset(page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]))
                                        page_object.upload_audio.change(page_object.upload_audio.file_item['index']);
                                    else
                                        page_object.upload_audio.complete();

                                }
                                else{

                                    if(!isset(data['error'])){

                                        page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['chunk_index']=chunk_index;
                                        page_object.upload_audio.file_item['chunk_index']=chunk_index;

                                        page_object.upload_audio.file_item['file_ID']=data['file_ID'];
                                        page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['file_ID']=data['file_ID'];

                                        page_object.upload_audio.progress(end,file_size,chunk_index,chunk_len);

                                        if(
                                              data['complete']
                                            &&(page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['chunk_len']-1)>=page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['chunk_index']
                                        ){
                                            
                                            if(isset(data['image_ID'])){

                                                page_object.upload_audio.file_item['image_ID']            =data['image_ID'];
                                                page_object.upload_audio.file_item['image_item_ID_list']  =data['image_item_ID_list'];
                                                page_object.upload_audio.file_item['image_dir']           =data['image_dir'];
                                            
                                                page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['image_ID']              =data['image_ID'];
                                                page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['image_item_ID_list']    =data['image_item_ID_list'];
                                                page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['image_dir']             =data['image_dir'];
                                                
                                            }
                                            else if(isset(data['audio_ID'])){

                                                page_object.upload_audio.file_item['audio_ID']            =data['audio_ID'];
                                                page_object.upload_audio.file_item['audio_item_ID_list']  =data['audio_item_ID_list'];
                                                page_object.upload_audio.file_item['audio_dir']           =data['audio_dir'];
                                            
                                                page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['audio_ID']              =data['audio_ID'];
                                                page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['audio_item_ID_list']    =data['audio_item_ID_list'];
                                                page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['audio_dir']             =data['audio_dir'];
                                                
                                            }

                                            page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['complete']=true;
                                            page_object.upload_audio.file_item['complete']=true;

                                            page_object.upload_audio.uploaded(page_object.upload_audio.file_item['index']);

                                        }
                                        else{

                                            start           +=chunk_size;
                                            end             +=chunk_size;
                                            chunk_index     ++;

                                            page_object.upload_audio.file_slice(chunk_index,chunk_len,file_size,start,end);

                                            //setTimeout(function(){
                                            //
                                            //    start           +=chunk_size;
                                            //    end             +=chunk_size;
                                            //    chunk_index     ++;
                                            //
                                            //    page_object.upload_audio.file_slice(chunk_index,chunk_len,file_size,start,end);
                                            //
                                            //},10);

                                        }

                                    }

                                }

                            }
                        });

                        return true;

                    }

                };

                reader.readAsBinaryString(file_slice);

            }

    },
    'progress':function(end,file_size,chunk_index,chunk_len){

        if(!page_object.upload_audio.file_item['remove'])
            if(isset(page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']])){

                page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['percent']=end/file_size;

                var wait=(chunk_index==(chunk_len-2));

                if(isset(page_object.upload_audio.data['on_progress']))
                    page_object.upload_audio.data['on_progress'](page_object.upload_audio.file_item['index'],wait);

            }

    },
    'uploaded':function(){

        if(page_object.upload_audio.file_item['remove']){

            if(isset(page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]))
                page_object.upload_audio.change(page_object.upload_audio.file_item['index']);
            else
                page_object.upload_audio.complete();

        }
        else{

            var list=page_object.upload_audio.data['list'];

            page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['wait']      =false;
            page_object.upload_audio.data['list'][page_object.upload_audio.file_item['index']]['complete']  =true;

            page_object.upload_audio.file_item['wait']        =false;
            page_object.upload_audio.file_item['complete']    =true;

            if(isset(page_object.upload_audio.data['on_uploaded']))
                page_object.upload_audio.data['on_uploaded'](page_object.upload_audio.file_item['index']);

            if(++page_object.upload_audio.file_item['index']<list.length)
                page_object.upload_audio.change(page_object.upload_audio.file_item['index']);
            else
                setTimeout(page_object.upload_audio.check_list,10);

        }

    },
    'check_list':function(){

        var  list       =page_object.upload_audio.data['list']
            ,i
            ,complete   =true;

        for(i=0;i<list.length;i++)
            if(!list[i]['remove']&&!list[i]['complete']){

                complete=false;

                page_object.upload_audio.change(i);

                break;

            }

        if(complete)
            page_object.upload_audio.complete();

    },
    'complete':function(){

        page_object.upload_audio.work=false;

        if(isset(page_object.upload_audio.data['on_complete']))
            page_object.upload_audio.data['on_complete']();

        page_object.upload_audio.controls.enable();

    },
    'reset':function(){

        page_object.upload_audio.data             =null;
        page_object.upload_audio.reverse_list     ={};
        page_object.upload_audio.work             =false;
        page_object.upload_audio.file_item        ={};

    },
    'set_index_list':function(){

        if(isset(page_object.upload_audio.data)){

            var  list=page_object.upload_audio.data['list']
                ,i
                ,key;

            for(i=0;i<list.length;i++)
                if(!list[i]['remove']){

                    key=list[i]['exhibit_index']+':'+list[i]['exhibit_item_index'];

                    page_object.upload_audio.index_list[key]=i;

                }

        }

    },
    'controls':{
        'disable':function(){

            if(isset($d('dialog_submit_save'))){

                $d('dialog_submit_save').onclick=function(){};
                $d('dialog_submit_save').setAttribute('class','dialog_submit_disable');

            }

        },
        'enable':function(){

            if(isset($d('dialog_submit_save'))){

                $d('dialog_submit_save').onclick=page_object.dialog.data['save'];
                $d('dialog_submit_save').setAttribute('class','dialog_submit_save');

            }

        }
    }
};