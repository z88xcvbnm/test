var load_object={
    'data':{},
    'init':function(data){

        if(isset(data['list'])){

            data['complete_items']      =0;
            data['percent']             =0;
            data['all_items']           =count(data['list']);
            data['event_data']          =[];

            load_object.data[data['type']]=data;

            if(isset(data['animate']))
                load_animate_object.init(data['animate']);

            if(data['all_items']==0)
                eval(load_object.data[data['type']].callback)(1);
            else
                load_object.create_event(data['type']);

        }

    },
    'create_event':function(type){

        var  el_image
            ,i
            ,func=new Function("load_object.on_load('"+type+"')");

        for(i=0;i<count(load_object.data[type]['list']);i++){

            el_image=new Image();

            el_image.src=load_object.data[type]['list'][i];

            load_object.data[type]['event_data'].push(listener.set(el_image,'load',func));

        }

    },
    'on_load':function(type){

        load_object.data[type]['complete_items']++;

        load_object.data[type]['percent']=load_object.data[type]['complete_items']/load_object.data[type]['all_items'];

        eval(load_object.data[type].callback)(load_object.data[type]['percent']);

        if(load_object.data[type]['percent']===1&&isset(load_object.data[type]['animate']))
            load_animate_object.drop_list(load_object.data[type]['animate']);

    }
};

var load_animate_object={
    'time':200,
    'el_w':10,
    'el_h':5,
    'el_m':2,
    'el_t':0,
    'el_l':0,
    'zI':0,
    'len':5,
    'bg_color':'#000000',
    'data':{},
    'init':function(data){

        var key;

        for(key in data){

            if(!isset(data[key]['time']))
                data[key]['time']=load_animate_object.time;

            if(!isset(data[key]['len']))
                data[key]['len']=load_animate_object.len;

            if(!isset(data[key]['w']))
                data[key]['w']=load_animate_object.el_w;

            if(!isset(data[key]['h']))
                data[key]['h']=load_animate_object.el_h;

            if(!isset(data[key]['m']))
                data[key]['m']=load_animate_object.el_m;

            if(!isset(data[key]['t']))
                data[key]['t']=load_animate_object.el_t;

            if(!isset(data[key]['l']))
                data[key]['l']=load_animate_object.el_l;

            if(!isset(data[key]['m_t']))
                data[key]['m_t']=null;

            if(!isset(data[key]['m_l']))
                data[key]['m_l']=null;

            if(!isset(data[key]['zI']))
                data[key]['zI']=load_animate_object.zI;

            if(!isset(data[key]['bg_color']))
                data[key]['bg_color']=load_animate_object.bg_color;

            if(!isset(data[key]['list']))
                data[key]['list']=[];

            data[key]['timeout']=null;

            if(!isset(load_animate_object.data[key])){

                load_animate_object.data[key]=data[key];

                load_animate_object.create(key);

            }

        }

    },
    'create':function(key){

        var  inner          =''
            ,data           =load_animate_object.data[key]
            ,len            =data['len']
            ,i
            ,block_size     =elementSize.getSize($d(key))
            ,block_w        =block_size['w']
            ,block_h        =block_size['h']
            ,item_w         =data['w']
            ,item_h         =data['h']
            ,item_m         =data['m']
            ,el_t           =data['t']
            ,el_l           =data['l']
            ,el_zI          =data['zI']
            ,el_bg_color    =data['bg_color']
            ,el_w           =(item_w+item_m*2)*len
            ,el_h           =item_h
            ,m_l            =isset(data['m_l'])?data['m_l']:(block_w-el_w)/2
            ,m_t            =isset(data['m_t'])?data['m_t']:(block_h-el_h)/2
            ,step           =1/len;

        if(!isset($d('loading_'+key+'_block'))){

            for(i=0;i<len;i++){

                load_animate_object.data[key]['list'][i]=i*step;

                inner+='<div id="loading_'+key+'_item_'+i+'" class="loading_item" style="width: '+item_w+'px; height: '+item_h+'px; margin-left: '+item_m+'px; margin-right: '+item_m+'px; background-color: '+el_bg_color+'"></div>';

            }

            var el=addElement({
                'tag':'div',
                'id':'loading_'+key+'_block',
                'class':'loading_block',
                'inner':inner,
                'style':'margin: '+m_t+'px 0 0 '+m_l+'px; width: '+el_w+'px; height: '+el_h+'px; top: '+el_t+'; left: '+el_l+'; z-index: '+el_zI
            });

            opaElement.setOpacity(el,0);

            $d(key).appendChild(el);

            for(i=0;i<len;i++)
                opaElement.setOpacity($d('loading_'+key+'_item_'+i),load_animate_object.data[key]['list'][i]);

            load_animate_object.animate(key);

            opaElement.setOpacity($d('loading_'+key+'_block'),1);

        }

    },
    'animate':function(key){

        if(isset(load_animate_object.data[key])&&isset($d('loading_'+key+'_block'))){

            var  data       =load_animate_object.data[key]
                ,list       =data['list']
                ,len        =data['len']
                ,i
                ,max_opa    =1
                ,step       =1/len;

            for(i=0;i<len;i++){

                list[i]+=step;

                if(list[i]>max_opa)
                    list[i]=0;

                opaElement.setOpacity($d('loading_'+key+'_item_'+i),list[i]);

            }

            load_animate_object.data[key]['list']=list;

            if(isset(load_animate_object.data[key]['timeout']))
                clearTimeout(load_animate_object.data[key]['timeout']);

            load_animate_object.data[key]['timeout']=setTimeout(new Function("load_animate_object.animate('"+key+"')"),load_animate_object.data[key]['timeout']);

        }

    },
    'drop':function(key){

        if(isset(load_animate_object.data[key]))
            delete load_animate_object.data[key];

        if(isset($d('loading_'+key+'_block')))
            removeElement($d('loading_'+key+'_block'));

    },
    'drop_list':function(obj){

        var key;

        for(key in obj)
            load_animate_object.drop(key);

    }
};