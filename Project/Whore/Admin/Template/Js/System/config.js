var config = {
    debug: true,
    localization: {},
    script_list: {}
};

config.convertScriptGroupsToPlainArray = function (scriptsGroups) {
    if ($.isPlainObject(scriptsGroups)) {
        var list = [];
        for (var group in scriptsGroups) {
            if ($.isArray(scriptsGroups[group])) {
                list = list.concat(scriptsGroups[group]);
            } else if ($.isPlainObject(scriptsGroups[group])) {
                for (var key in scriptsGroups[group]) {
                    list.push(scriptsGroups[group][key]);
                }
            } else {
                list.push(scriptsGroups[group]);
            }
        }
        return list;
    } else {
        return $.isArray(scriptsGroups) ? scriptsGroups : [scriptsGroups];
    }
};

config.updateScriptsList = function (scriptsGroups, section) {
    if (typeof config.script_list[section] !== 'undefined') {
        scriptsGroups = $.extendext(true, 'concat', scriptsGroups || {}, config.script_list[section]);
    }
    return config.convertScriptGroupsToPlainArray(scriptsGroups || []);
};

config.updateSectionChangeWorkflow = function (workflowConfigs) {
    return workflowConfigs;
};