page_object.action.profile={
    'data':{},
    'script_list':{
        'core':{
            'localization':'/show/js/profile/localization.js',
            'elements_positions':'/show/js/profile/elements_positions.js',
            'section_change_workflow':'/show/js/profile/section_change_workflow.js'
        },
        'ui_elements_creation':{
            'header':'/show/js/profile/create/page_object_action_profile_create_header_object.js',
            'admin_menu':'/show/js/profile/create/page_object_action_profile_create_profile_menu_block_object.js',
            'settings':'/show/js/profile/create/page_object_action_profile_create_settings_object.js',
            'root_content':'/show/js/profile/create/page_object_action_profile_create_root_content_object.js',
        },
        'ui_elements_manipulation':{
            'header':'/show/js/profile/action/page_object_action_profile_action_header_object.js',
            'header_menu':'/show/js/profile/action/page_object_action_profile_action_header_menu_object.js',
            'header_profile':'/show/js/profile/action/page_object_action_profile_action_header_profile_object.js',
            'header_profile_menu_block':'/show/js/profile/action/page_object_action_profile_action_header_profile_menu_block_object.js',
            'root_content':'/show/js/profile/action/page_object_action_profile_action_root_content_object.js',
            'root_face_block':'/show/js/profile/action/page_object_action_profile_action_root_face_block_object.js',
        },
        'settings':'/show/js/profile/action/page_object_action_profile_action_settings_object.js',
        'system':{
            'exif':'/show/js/system/exif.js'
        },
        'recaptcha':{
            'script':'https://www.google.com/recaptcha/api.js?render=6Lez3J4UAAAAAKzX2vcL3Gd_inzV55NQdtaT9d29'
        },
        'sdk':{
            // 'paypal':'https://www.paypal.com/sdk/js?client-id=ASRxEH-_kDVl4jTaQh0dUkpgV1G0K3JooLc1dVy1tQMsOCdn3VRnKjPBHHvr9kRWaDV9MLuqb7UTz8Ck&currency=RUB&commit=true'
            'paypal':'https://www.paypal.com/sdk/js?client-id=ASRxEH-_kDVl4jTaQh0dUkpgV1G0K3JooLc1dVy1tQMsOCdn3VRnKjPBHHvr9kRWaDV9MLuqb7UTz8Ck&currency=RUB&commit=true',
            // 'qiwi':'https://oplata.qiwi.com/popup/v1.js'
        }
    },
    'style_list':[
        '/show/css/system/dialog_block.css',
        '/show/css/system/root.css',
        '/show/css/profile/profile.css'
    ],
    'section_change_workflow':{},
    'page_action':null,
    'back_page_action':null,
    'content':{},
    'position':{},
    'init':function(data){

        if(OS.isMobile){

            page_object.action.profile.style_list.push('/show/css/system/root_mobile.css');
            page_object.action.profile.style_list.push('/show/css/system/profile_mobile.css');

        }

        let temp={
            'script_list':config.updateScriptsList(page_object.action.profile.script_list, 'Profile'),
            'style_list':config.updateScriptsList(page_object.action.profile.style_list, 'Profile'),
            'init_list':[
                function(){
                    if (typeof config !== 'undefined' && $.isFunction(config.updateSectionChangeWorkflow)) {
                        page_object.action.profile.section_change_workflow = config.updateSectionChangeWorkflow(page_object.action.profile.section_change_workflow);
                    }
                    page_object.action.profile.run(data);
                }
            ]
        };

        if(isset(data['data']))
            if(isset(data['data']['face_image_random_list'])){

                let  el_len=isset($d('root_face_preview_list'))?$d('root_face_preview_list').getElementsByTagName('div').length:0;

                if(el_len===0){

                    let  image_list     =data['data']['face_image_random_list']
                        ,list           =[]
                        ,index;

                    for(index in image_list)
                        list.push(image_list[index]['link']);

                    temp['image_list']=list;

                }

            }

        page_object.require.init(temp);

    },
    'run':function(data){

        if(
              page_object.link.link_list[(page_object.link.link_list.length-1)]!==page_object.link.back_link_list[(page_object.link.back_link_list.length-1)]
            ||page_object.link.link_list[(page_object.link.link_list.length-2)]!==page_object.link.back_link_list[(page_object.link.back_link_list.length-2)]
            ||page_object.link.link_list[(page_object.link.link_list.length-3)]!==page_object.link.back_link_list[(page_object.link.back_link_list.length-3)]
        ){

            page_object.action.profile.data                   =data;
            page_object.action.profile.back_page_action       =page_object.action.profile.page_action;
            page_object.action.profile.page_action            =data['action'];

            //if(isset($d('dialog_block')))
            //    page_object.dialog.action.un_show.init(dialog_index,true);

            if(isset(page_object.action.profile.section_change_workflow[data['action']])){

                page_object.action.profile.section_change_workflow[data['action']].init();

                setTimeout(page_object.action.profile.action.all.resize,1200);

            }
            else
                trace('==> TRIG ACTION IS NOT EXISTS');

        }
        else
            trace('==> ACTION ALREADY ACTIVE');

    },
    'prepend_localization_content':function (data) {
        page_object.action.profile.content = $.extendext(true, 'concat', data, page_object.action.profile.content);
    },
    'append_localization_content':function (data) {
        page_object.action.profile.content = $.extendext(true, 'concat', page_object.action.profile.content, data);
    },
    'prepend_element_positions_settings':function (data) {
        page_object.action.profile.position = $.extendext(true, 'concat', data, page_object.action.profile.position);
    },
    'append_element_positions_settings':function (data) {
        page_object.action.profile.position = $.extendext(true, 'concat', page_object.action.profile.position, data);
    },
    'is_root':function(){

        return page_object.action.profile.data['is_root'];

    },
    'is_admin':function(){

        return page_object.action.profile.data['is_root']
            ||page_object.action.profile.data['is_admin'];

    },
    'is_manager':function(){

        return page_object.action.profile.data['is_root']
            ||page_object.action.profile.data['is_admin']
            ||page_object.action.profile.data['is_manager'];

    },
    'is_bookkeep':function(){

        return page_object.action.profile.data['is_bookkeep'];

    },
    'is_museum':function(){

        return page_object.action.profile.data['is_museum'];

    },
    'create':{},
    'action':{
        'all':{
            'resize':function(){

                if(isset($d('all'))){

                    if(isset(page_object.action.profile)&&isset(page_object.action.profile.position)){

                        page_object.action.profile.position.all.w=winSize.winWidth;

                        if(page_object.link.link_list[2]==='users'){

                            if(page_object.action.profile.data['action']==='admin_users'){

                                page_object.action.profile.position.all.h=page_object.action.profile.position.header.h
                                    +page_object.action.profile.position.header_menu_child_block.h
                                    +page_object.action.profile.position.users_list.m.t
                                    +page_object.action.profile.position.users_list.h;

                            }

                        }
                        else{

                            page_object.action.profile.position.all.h=winSize.winHeight;

                        }

                        // $s('all').wight     =page_object.action.profile.position.all.w+'px';
                        // $s('all').height    =page_object.action.profile.position.all.h+'px';

                        if(isset($d('bg_load'))){

                            //$s('bg_load').transform     ='translate(0,'+$b().scrollTop+'px)';
                            // $s('bg_load').width         =winSize.winWidth+'px';
                            // $s('bg_load').height        =winSize.winHeight+'px';

                        }

                    }

                }

            }
        },
        'scroll':{
            'animate':function(x,y,x_max,y_max,step){

                let  delta_x        =Math.floor(Math.abs(x-x_max)/step)
                    ,delta_y        =Math.floor(Math.abs(y-y_max)/step);

                delta_x=(delta_x<3)?3:delta_x;
                delta_y=(delta_y<3)?3:delta_y;

                if(x>=x_max){

                    x-=delta_x;

                    if(x<x_max)
                        x=x_max;

                }
                else{
                    
                    x+=delta_x;
                    
                    if(x>x_max)
                        x=x_max;
                    
                }
                
                $b().scrollLeft=x;

                if(y>=y_max){

                    y-=delta_y;

                    if(y<y_max)
                        y=y_max;

                }
                else{
                    
                    y+=delta_y;
                    
                    if(y>y_max)
                        y=y_max;
                    
                }
                
                $b().scrollTop=y;
                
                if(x===x_max&&y===y_max)
                    return false;
                    
                setTimeout(function(){

                    page_object.action.profile.action.scroll.animate(x,y,x_max,y_max,step);

                },40);

                return true;

            },
            'coords':function(x,y){

                let  doc_w          =$b().scrollWidth-winSize.winWidth
                    ,doc_h          =$b().scrollHeight-winSize.winHeight
                    ,doc_x          =$b().scrollLeft
                    ,doc_y          =$b().scrollTop;

                x       =(x>doc_w)?doc_w:x;
                y       =(y>doc_h)?doc_h:y;

                page_object.action.profile.action.scroll.animate(doc_x,doc_y,x,y,3);

            },
            'element':function(el){

                let  pos        =getElementPosition(el)
                    ,pos_x      =pos['left']
                    ,pos_y      =pos['top'];

                page_object.action.profile.action.scroll.coords(pos_x,pos_y);

            }
        },
        'resize':function(){

            if(isset(page_object.action.profile.action.header))
                page_object.action.profile.action.header.resize();

            if(isset(page_object.action.profile.action.header_profile_menu_block))
                page_object.action.profile.action.header_profile_menu_block.resize();

            if(isset(page_object.action.profile.action.settings))
                page_object.action.profile.action.settings.resize();

            if(isset(page_object.action.profile.action.all))
                page_object.action.profile.action.all.resize();

        }
    }
};