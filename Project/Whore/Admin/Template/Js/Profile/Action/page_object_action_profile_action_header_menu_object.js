page_object.action.profile.action.header_menu={
    'show':function(){

        if(isset($d('header_menu'))){

            page_object.action.profile.position.header_menu.o=1;

            $s('header_menu').opacity=page_object.action.profile.position.header_menu.o;

        }

    },
    'un_show':function(remove){

        if(isset($d('header_menu'))){

            page_object.action.profile.position.header_menu.o=0;

            $s('header_menu').opacity=page_object.action.profile.position.header_menu.o;

        }

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.profile.action.header_menu.remove,300);

    },
    'remove':function(){

        if(isset($d('header_menu')))
            removeElement($d('header_menu'));

    },
    'resize':function(){

        if(isset($d('header'))){

            page_object.action.profile.position.header.w=Math.ceil(winSize.winWidth *.62);

            $s('header').width=page_object.action.profile.position.header_menu.w+'px';

        }

    },
    'click':function(){

        let  el_ID          =this.id
            ,key            =implode('_',el_ID.split('_').slice(2))
            ,lang_obj       =page_object.content[page_object.lang]
            ,link           ='/profile/'+page_object.action.profile.data['user']['login']+'/'+key
            ,func;

        page_object.action.profile.action.header_menu.set_active(key);

        if(OS.isMobile)
            if(isset($d('header_menu')))
                page_object.action.profile.action.header.menu.un_show();

        setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

        return true;

    },
    'set_active':function(key){

        let  list               =[]
            ,id_el
            ,i
            ,el_x
            ,el_w
            ,isset_active       =false;

        if(OS.isMobile){

            id_el   =OS.isMobile?'header_menu_list':'header_menu';
            list    =$d(id_el).getElementsByTagName('div');

        }
        else{

            list = Array.prototype.concat.apply(list, $d('header_menu').getElementsByTagName("div"));
            list = Array.prototype.concat.apply(list, $d('header').getElementsByClassName("header_balance_block"));

        }

        if(!isset(key))
            if(isset(page_object.link.link_list[2]))
                key=page_object.link.link_list[2];

        for(i=0;i<list.length;i++)
            if(isObject(list[i])){

                if(list[i].getAttribute('id')===('header_menu_'+key)){

                    if(isset($d('header_menu_'+key)))
                        if($d('header_menu_'+key).getAttribute('class')!=='header_menu_item_disable'){

                            isset_active=true;

                            if(!OS.isMobile){

                                el_w            =elementSize.width($d('header_menu_'+key));
                                el_x            =getElementPosition($d('header_menu_'+key))['left'];

                                if(isset($d('header_menu_line_active'))){

                                    $s('header_menu_line_active').transform     ='translate('+el_x+'px,0)';
                                    $s('header_menu_line_active').width         =el_w+'px';

                                }

                            }

                            $d('header_menu_'+key).setAttribute('class','header_menu_item_active');

                        }

                }
                else if(
                    (
                          list[i].getAttribute('class')==='header_balance_block'
                        ||list[i].getAttribute('class')==='header_menu_item'
                    )
                    &&key==='payment'
                ){

                    if(!OS.isMobile){

                        el_w            =elementSize.width($d('header_balance_block'));
                        el_x            =getElementPosition($d('header_balance_block'))['left'];
                        isset_active    =true;

                        el_w+=40;
                        el_x-=20;

                        if(isset($d('header_menu_line_active'))){

                            $s('header_menu_line_active').transform     ='translate('+el_x+'px,0)';
                            $s('header_menu_line_active').width         =el_w+'px';

                        }

                    }

                }
                else if(
                      list[i].getAttribute('class')!=='header_menu_item_disable'
                    &&list[i].getAttribute('class')!=='header_balance_block'
                )
                    list[i].setAttribute('class','header_menu_item');

            }

        if(isset($d('header_menu_line_active')))
            $s('header_menu_line_active').opacity=isset_active?1:0;

    }
};