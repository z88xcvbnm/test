page_object.action.profile.action.root_face_block={
    'is_test':false,
    'show':function(){

        if(isset($d('root_face_block'))){

            let  h=elementSize.height($d('root_face_block'));

            page_object.action.profile.position.root_face_block.o=1;

            switch(page_object.action.profile.data['action']){

                case'profile_face':{

                    $s('root_face_block').opacity=1;

                    if(OS.isMobile){

                        $s('root_face_block').height            ='auto';
                        $s('root_face_block').paddingTop        ='';
                        $s('root_face_block').paddingBottom     ='';

                    }

                    // if(isset($d('root_right_content'))&&!OS.isMobile)
                    //     $s('root_right_content').height=elementSize.height($d('root_left_content'))+'px';

                    if(isset($d('root_right_content'))&&!OS.isMobile){

                        let left_height=elementSize.height('root_left_content');

                        if(left_height===0)
                            left_height=382;

                        $s('root_right_content').height=left_height+'px';

                    }
                        // $s('root_right_content').height='375px';

                    break;

                }

                case'profile_search_history':{

                    if(OS.isMobile){

                        $s('root_face_block').overflowY         ='auto';
                        $s('root_face_block').opacity           =1;
                        $s('root_face_block').height            ='calc(100vh - 70px)';
                        $s('root_face_block').paddingTop        =0;
                        $s('root_face_block').paddingBottom     =0;
                        $s('root_default_content').height       ='auto';
                        $s('root_default_content').maxHeight    ='calc(100vh - 70px)';
                        $s('root_default_content').paddingTop   ='10px';

                    }
                    else{

                        $s('root_face_block').transform     ='';
                        $s('root_face_block').top           ='50%';
                        $s('root_face_block').transform     ='translate(-50%,calc(-50% + 15px))';
                        $s('root_face_block').margin        ='0 0 0 0';
                        $s('root_face_block').width         ='1120px';
                        $s('root_face_block').maxHeight     ='calc(100vh - 130px)';
                        $s('root_face_block').overflowY     ='auto';
                        $s('root_face_block').opacity       =1;

                    }

                    break;

                }

                case'profile_city':{

                    if(OS.isMobile){

                        $s('root_face_block').overflowY         ='auto';
                        $s('root_face_block').opacity           =1;
                        $s('root_face_block').height            ='calc(100vh - 70px)';
                        $s('root_face_block').paddingTop        =0;
                        $s('root_face_block').paddingBottom     =0;
                        $s('root_default_content').height       ='auto';
                        $s('root_default_content').maxHeight    ='calc(100vh - 70px)';
                        $s('root_default_content').paddingTop   ='10px';

                    }
                    else{

                        $s('root_face_block').transform     ='';
                        $s('root_face_block').top           ='50%';
                        $s('root_face_block').transform     ='translate(-50%,calc(-50% + 15px))';
                        $s('root_face_block').margin        ='0 0 0 0';
                        $s('root_face_block').width         ='1120px';
                        $s('root_face_block').maxHeight     ='calc(100vh - 130px)';
                        $s('root_face_block').overflowY     ='auto';
                        $s('root_face_block').opacity       =1;

                    }

                    break;

                }

                case'profile_billing':{

                    if(OS.isMobile){

                        $s('root_face_block').overflowY         ='auto';
                        $s('root_face_block').opacity           =1;
                        $s('root_face_block').height            ='calc(100vh - 70px)';
                        $s('root_face_block').paddingTop        =0;
                        $s('root_face_block').paddingBottom     =0;
                        $s('root_default_content').height       ='auto';
                        $s('root_default_content').maxHeight    ='calc(100vh - 70px)';
                        $s('root_default_content').paddingTop   ='10px';

                    }
                    else{

                        $s('root_face_block').transform     ='';
                        $s('root_face_block').top           ='50%';
                        $s('root_face_block').transform     ='translate(-50%,calc(-50% + 15px))';
                        $s('root_face_block').margin        ='0 0 0 0';
                        $s('root_face_block').width         ='1260px';
                        $s('root_face_block').height        ='calc(100vh - 100px)';
                        $s('root_face_block').maxHeight     ='calc(100vh - 130px)';
                        $s('root_face_block').overflowY     ='auto';
                        $s('root_face_block').opacity       =1;

                    }

                    break;

                }

                case'profile_payment':{

                    if(OS.isMobile){

                        $s('root_face_block').overflowY         ='auto';
                        $s('root_face_block').opacity           =1;
                        $s('root_face_block').height            ='auto';
                        $s('root_face_block').paddingTop        ='';
                        $s('root_face_block').paddingBottom     ='';
                        $s('root_default_content').maxHeight    ='';

                    }
                    else{

                        $s('root_face_block').transform     ='';
                        $s('root_face_block').top           ='50%';
                        $s('root_face_block').transform     ='translate(-50%,calc(-50% + 15px))';
                        $s('root_face_block').margin        ='0 0 0 0';
                        $s('root_face_block').width         ='430px';
                        // $s('root_face_block').width         ='1204px';
                        $s('root_face_block').maxHeight     ='calc(100vh - 130px)';
                        $s('root_face_block').overflowY     ='auto';
                        $s('root_face_block').opacity       =1;

                    }

                    break;

                }

            }

        }

    },
    'un_show':function(remove){

        if(isset($d('root_face_block'))){

            page_object.action.profile.position.root_face_block.o  =0;
            page_object.action.profile.position.root_face_block.y  =-200;

            $s('root_face_block').opacity       =page_object.action.profile.position.root_face_block.o;
            // $s('root_face_block').transform     ='translate(-50%,-100%)';

        }

        page_object.action.profile.action.root_face_block.action.image.drag.remove_event_list();

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.profile.action.root_face_block.remove,300);

    },
    'action':{
        'init':function(){

            page_object.action.profile.action.root_face_block.action.image.init();

            if(isset($d('search_history_block')))
                page_object.action.profile.action.root_face_block.action.search_history.init();

            if(isset($d('profile_payment_block')))
                page_object.action.profile.action.root_face_block.action.payment.init();

            if(isset($d('billing_block')))
                page_object.action.profile.action.root_face_block.action.billing.init();

            if(isset($d('root_city_stats_block')))
                page_object.action.profile.action.root_face_block.action.city.init();

        },
        'image':{
            'list':[
                {
                    'image_ID':null,
                    'link':null,
                    'file':null
                },
                {
                    'image_ID':null,
                    'link':null,
                    'file':null
                },
                {
                    'image_ID':null,
                    'link':null,
                    'file':null
                }
            ],
            'image_len':0,
            'init':function(){

                let  index
                    ,list
                    ,image_len      =0;

                if(isset(page_object.action.root)){

                    let block_w;

                    list=page_object.action.root.action.root_face_block.action.image.list;

                    for(index in list)
                        if(
                              empty(list[index]['file'])
                            &&empty(list[index]['link'])
                        ){

                            if(isset($d('root_search_face_image_row_container_'+index))){

                                image_len++;

                                if(parseInt(index)===0)
                                    $s('root_search_face_image_row_container_'+index).display='block';
                                else
                                    $s('root_search_face_image_row_container_'+index).display='none';

                            }

                            if(!OS.isMobile){

                                if(isset($d('root_face_content'))){

                                    block_w=((3-image_len)*400+40);

                                    if(block_w<400)
                                        block_w=400;

                                    $s('root_face_content').width       =block_w+'px';
                                    $s('root_face_content').left        ='50%';
                                    $s('root_face_content').transform   ='translate(-50%,calc(-50% + 15px))';

                                }

                            }

                        }

                }

                image_len=0;

                for(index in list)
                    if(
                          !empty(list[index]['file'])
                        ||!empty(list[index]['link'])
                    )
                        if(isset($d('root_search_face_image_row_container_'+index)))
                            image_len++;

                trace('IMAGE LEN: '+image_len);

                list=page_object.action.profile.action.root_face_block.action.image.list;

                page_object.action.profile.action.root_face_block.action.image.list=[
                    {
                        'image_ID':null,
                        'link':null,
                        'file':null
                    },
                    {
                        'image_ID':null,
                        'link':null,
                        'file':null
                    },
                    {
                        'image_ID':null,
                        'link':null,
                        'file':null
                    }
                ];
                page_object.action.profile.action.root_face_block.action.image.face.list=[
                    {
                        'face_index':null,
                        'image_ID':null,
                        'image_data':null,
                        'file_ID':null,
                        'face_len':0,
                        'face_list':null
                    },
                    {
                        'face_index':null,
                        'image_ID':null,
                        'image_data':null,
                        'file_ID':null,
                        'face_len':0,
                        'face_list':null
                    },
                    {
                        'face_index':null,
                        'image_ID':null,
                        'image_data':null,
                        'file_ID':null,
                        'face_len':0,
                        'face_list':null
                    }
                ];

                page_object.upload.reset();

                page_object.action.profile.action.root_face_block.action.image.face.face_change_len     =0;
                page_object.action.profile.action.root_face_block.action.image.image_len                =image_len;

                page_object.action.profile.action.root_face_block.action.image.drag.init();

                if(isset($d('root_search_face_reset')))
                    $d('root_search_face_reset').onclick=page_object.action.profile.action.root_face_block.action.image.reset;

                for(index in list)
                    page_object.action.profile.action.root_face_block.action.image.init_index(index);


                if(isset(page_object.action.root))
                    page_object.action.root.action.root_face_block.action.image.list=[
                        {
                            'image_ID':null,
                            'file_ID':null,
                            'link':null,
                            'file':null,
                        },
                        {
                            'image_ID':null,
                            'file_ID':null,
                            'link':null,
                            'file':null
                        },
                        {
                            'image_ID':null,
                            'file_ID':null,
                            'link':null,
                            'file':null
                        }
                    ];

            },
            'init_index':function(index){

                if(OS.isMobile){

                    if(isset($d('root_search_face_image_info_'+index)))
                        $s('root_search_face_image_info_'+index).display='none';

                }

                OS.getOS();

                if(isset(page_object.action.root)){

                    page_object.action.root.action.root_face_block.action.image.drag.remove_event_list(index);

                    if(!empty(page_object.action.root.action.root_face_block.action.image.list[index]['file'])){

                        page_object.action.profile.action.root_face_block.action.image.list[index]['file']          =page_object.action.root.action.root_face_block.action.image.list[index]['file'];
                        page_object.action.profile.action.root_face_block.action.image.list[index]['file_ID']       =page_object.action.root.action.root_face_block.action.image.list[index]['file_ID'];
                        page_object.action.profile.action.root_face_block.action.image.list[index]['image_ID']      =page_object.action.root.action.root_face_block.action.image.list[index]['image_ID'];
                        page_object.action.profile.action.root_face_block.action.image.list[index]['link']          =page_object.action.root.action.root_face_block.action.image.list[index]['link'];

                    }
                    else if(!empty(page_object.action.root.action.root_face_block.action.image.list[index]['link'])){

                        page_object.action.profile.action.root_face_block.action.image.list[index]['file']          =page_object.action.root.action.root_face_block.action.image.list[index]['file'];
                        page_object.action.profile.action.root_face_block.action.image.list[index]['file_ID']       =page_object.action.root.action.root_face_block.action.image.list[index]['file_ID'];
                        page_object.action.profile.action.root_face_block.action.image.list[index]['image_ID']      =page_object.action.root.action.root_face_block.action.image.list[index]['image_ID'];
                        page_object.action.profile.action.root_face_block.action.image.list[index]['link']          =page_object.action.root.action.root_face_block.action.image.list[index]['link'];

                    }

                    if(
                          !empty(page_object.action.root.action.root_face_block.action.image.list[index]['file'])
                        ||!empty(page_object.action.root.action.root_face_block.action.image.list[index]['link'])
                    ){

                        let  user_data      =page_object.action.profile.data['user']
                            ,total_cost     =0;

                        switch(page_object.action.profile.action.root_face_block.action.image.image_len){

                            case 0:{

                                total_cost=0;

                                break;

                            }

                            case 1:{

                                total_cost=user_data['price'];

                                break;

                            }

                            default:{

                                total_cost=2*user_data['price'];

                                break;

                            }

                        }

                        trace('check balance: '+parseFloat(user_data['balance'])+' < '+total_cost);

                        if(parseFloat(user_data['balance'])<total_cost){

                            trace('check done: true');

                            page_object.action.profile.action.root_face_block.action.image.drag.remove_event_list();

                            if(isset($d('root_search_face_reset_'+index))){

                                $s('root_search_face_reset_'+index).opacity     =0;
                                $s('root_search_face_reset_'+index).display     ='none';

                            }

                            if(isset($d('root_search_face_button'))){

                                $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                                $d('root_search_face_button').innerHTML     ='<span class="red">Пополните баланс</span>';
                                $d('root_search_face_button').onclick       =function(){};

                            }

                        }
                        else{

                            trace('check done: false');

                            page_object.action.profile.action.root_face_block.action.image.drag.init();

                            if(isset($d('root_search_face_image_src_'+index))){

                                let image_list=$d('root_search_face_image_src_'+index).getElementsByTagName('img');

                                if(image_list.length!==0)
                                    if(isset($d('root_search_face_button')))
                                        $d('root_search_face_button').onclick=page_object.action.profile.action.root_face_block.action.search.init

                            }

                            if(!empty(page_object.action.root.action.root_face_block.action.image.list[index]['file'])){

                                page_object.action.root.action.root_face_block.action.image.list[index]['file']         =null;
                                page_object.action.root.action.root_face_block.action.image.list[index]['file_ID']      =null;
                                page_object.action.root.action.root_face_block.action.image.list[index]['image_ID']     =null;
                                page_object.action.root.action.root_face_block.action.image.list[index]['link']         =null;

                                page_object.action.profile.action.root_face_block.action.image.upload.file.init(index);

                            }
                            else if(!empty(page_object.action.root.action.root_face_block.action.image.list[index]['link'])){

                                page_object.action.root.action.root_face_block.action.image.list[index]['file']         =null;
                                page_object.action.root.action.root_face_block.action.image.list[index]['file_ID']      =null;
                                page_object.action.root.action.root_face_block.action.image.list[index]['image_ID']     =null;
                                page_object.action.root.action.root_face_block.action.image.list[index]['link']         =null;

                                page_object.action.profile.action.root_face_block.action.image.upload.link.init(index);

                            }

                        }

                    }
                    else{

                        if(isset($d('root_search_face_image_row_container_'+index))){

                            if(
                                  page_object.action.profile.action.root_face_block.action.image.image_len===0
                                &&parseInt(index)===0
                            ){

                                trace('root_search_face_image_row_container_'+index+' > 1');

                                $s('root_search_face_image_row_container_'+index).opacity   =1;
                                $s('root_search_face_image_row_container_'+index).display   ='block';

                            }
                            else{

                                trace('root_search_face_image_row_container_'+index+' > 0');

                                $s('root_search_face_image_row_container_'+index).opacity   =0;
                                $s('root_search_face_image_row_container_'+index).display   ='none';

                            }

                        }

                    }

                    if(isset($d('root_search_face_image_input_'+index))){

                        if(isset($d('root_search_face_button_choose_'+index)))
                            $d('root_search_face_button_choose_'+index).onclick=function(){

                                let index=this.id.split('_')[5];

                                if(OS.os==='Android')
                                    $d('root_search_face_image_input_'+index).setAttribute('accept','image/png, image/jpeg');
                                else
                                    $d('root_search_face_image_input_'+index).setAttribute('accept','image/png, image/jpeg');

                                if(isset($d('root_search_face_image_input_'+index)))
                                    $d('root_search_face_image_input_'+index).click();

                            };

                        $d('root_search_face_image_input_'+index).onchange=page_object.action.profile.action.root_face_block.action.image.change;

                    }

                    if(isset($d('root_search_face_reset_'+index)))
                        $d('root_search_face_reset_'+index).onclick=page_object.action.profile.action.root_face_block.action.image.reset_item;

                    return true;

                }

                if(isset($d('root_search_face_reset_'+index)))
                    $d('root_search_face_reset_'+index).onclick=page_object.action.profile.action.root_face_block.action.image.reset_item;

                if(isset($d('root_search_face_image_src_'+index))){

                    let image_list=$d('root_search_face_image_src_'+index).getElementsByTagName('img');

                    if(image_list.length!==0)
                        if(isset($d('root_search_face_button')))
                            $d('root_search_face_button').onclick=page_object.action.profile.action.root_face_block.action.search.init

                }

                if(isset($d('root_search_face_image_input_'+index))){

                    if(isset($d('root_search_face_button_choose_'+index)))
                        $d('root_search_face_button_choose_'+index).onclick=function(){

                            let index=this.id.split('_')[5];

                            if(OS.os==='Android')
                                $d('root_search_face_image_input_'+index).setAttribute('accept','image/png, image/jpeg');
                            else
                                $d('root_search_face_image_input_'+index).setAttribute('accept','image/png, image/jpeg');

                            if(isset($d('root_search_face_image_input_'+index)))
                                $d('root_search_face_image_input_'+index).click();

                        };

                    $d('root_search_face_image_input_'+index).onchange=page_object.action.profile.action.root_face_block.action.image.change;

                }

                return true;

            },
            'drag':{
                'is_work':false,
                'event_list':[],
                'remove_event_list':function(){

                    if(page_object.action.profile.action.root_face_block.action.image.drag.event_list.length>0)
                        listener.remove(page_object.action.profile.action.root_face_block.action.image.drag.event_list);

                    page_object.action.profile.action.root_face_block.action.image.drag.event_list=[];

                },
                'init':function(){

                    let  index
                        ,event_list=[];

                    page_object.action.profile.action.root_face_block.action.image.drag.remove_event_list();

                    event_list.push(listener.set(document.body,'dragenter',page_object.action.profile.action.root_face_block.action.image.drag.enter));
                    event_list.push(listener.set(document.body,'dragover',page_object.action.profile.action.root_face_block.action.image.drag.leave));
                    event_list.push(listener.set(document.body,'dragleave',page_object.action.profile.action.root_face_block.action.image.drag.over));
                    event_list.push(listener.set(document.body,'dragenter',page_object.action.profile.action.root_face_block.action.image.drag.prevent_defaults));
                    event_list.push(listener.set(document.body,'dragover',page_object.action.profile.action.root_face_block.action.image.drag.prevent_defaults));
                    event_list.push(listener.set(document.body,'dragleave',page_object.action.profile.action.root_face_block.action.image.drag.prevent_defaults));
                    event_list.push(listener.set(document.body,'drop',page_object.action.profile.action.root_face_block.action.image.drag.prevent_defaults));

                    page_object.action.profile.action.root_face_block.action.image.drag.event_list=page_object.action.profile.action.root_face_block.action.image.drag.event_list.concat(event_list);

                    for(index=0;index<3;index++)
                        page_object.action.profile.action.root_face_block.action.image.drag.init_index(index);

                },
                'init_index':function(index){

                    if(isset($d('root_search_face_image_block_'+index))){

                        let event_list=[];

                        event_list.push(listener.set($d('root_search_face_image_block_'+index),'drop',page_object.action.profile.action.root_face_block.action.image.drag.drop));

                        page_object.action.profile.action.root_face_block.action.image.drag.event_list=page_object.action.profile.action.root_face_block.action.image.drag.event_list.concat(event_list);

                        trace('after count event: '+page_object.action.profile.action.root_face_block.action.image.drag.event_list.length);

                    }

                },
                'enter':function(e){

                    let index=this.id.split('_')[5];

                    if(isset($d('root_search_face_image_block_'+index)))
                        $s('root_search_face_image_block_'+index).borderColor='#ff0000';

                },
                'leave':function(e){

                    let index=this.id.split('_')[5];

                    if(isset($d('root_search_face_image_block_'+index)))
                        $s('root_search_face_image_block_'+index).borderColor='#ff0000';

                },
                'over':function(e){

                    let index=this.id.split('_')[5];

                    if(isset($d('root_search_face_image_block_'+index)))
                        $s('root_search_face_image_block_'+index).borderColor='';

                },
                'drop':function(e){

                    trace('ID: '+this.id);

                    e=e||window.event;

                    let  dt             =e.dataTransfer
                        ,image_link     =dt.getData('text')
                        ,files          =dt.files
                        ,index          =this.id.split('_')[5];

                    if(files.length===0){

                        if(isset($d('root_search_face_image_input_'+index)))
                            $d('root_search_face_image_input_'+index).value='';

                        page_object.action.profile.action.root_face_block.action.image.list[index]['file_ID']       =null;
                        page_object.action.profile.action.root_face_block.action.image.list[index]['image_ID']      =null;
                        page_object.action.profile.action.root_face_block.action.image.list[index]['file']          =null;
                        page_object.action.profile.action.root_face_block.action.image.list[index]['link']          =image_link;

                        trace('image_link: '+image_link);

                        page_object.action.profile.action.root_face_block.action.image.send_link(image_link,index);

                    }
                    else
                        page_object.action.profile.action.root_face_block.action.image.drag.files.init(files,index);

                },
                'prevent_defaults':function(e){

                    e=e||window.event;

                    e.preventDefault();
                    e.stopPropagation();

                },
                'files':{
                    'init':function(files,index){

                        let file_index;

                        if(isset($d('root_search_face_button'))){

                            $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                            $d('root_search_face_button').innerHTML     ='Загрузка...';
                            $d('root_search_face_button').onclick       =function(){};

                        }
                        else
                            return false;

                        if(isset($d('root_search_face_image_block_'+index)))
                            $s('root_search_face_image_block_'+index).borderColor='';

                        if(isset($d('root_search_face_button_choose')))
                            $s('root_search_face_button_choose').opacity=0;

                        if(isset($d('root_search_face_image_info_'+index)))
                            $s('root_search_face_image_info_'+index).opacity=0;

                        if(isset($d('root_search_face_image_src_object_'+index)))
                            $s('root_search_face_image_src_object_'+index).opacity=0;

                        setTimeout(function(){

                            if(isset($d('root_search_face_button_choose_'+index)))
                                $s('root_search_face_button_choose_'+index).display='none';

                            if(isset($d('root_search_face_image_info_'+index)))
                                $s('root_search_face_image_info_'+index).display='none';

                            if(isset($d('root_search_face_image_loading_'+index)))
                                $s('root_search_face_image_loading_'+index).display='block';

                            setTimeout(function(){

                                if(isset($d('root_search_face_image_loading_'+index)))
                                    $s('root_search_face_image_loading_'+index).opacity=1;

                            },40);

                        },300);

                        for(file_index in files)
                            page_object.action.profile.action.root_face_block.action.image.drag.files.load(files[file_index],index);

                    },
                    'load':function(file,index){

                        page_object.action.profile.action.root_face_block.action.image.drag.is_work=false;

                        if(isObject(file)){

                            switch(file.type){

                                case'image/jpeg':
                                case'image/png':{

                                    var reader=new FileReader();

                                    page_object.action.profile.action.root_face_block.action.image.list[index]['link']  =null;
                                    page_object.action.profile.action.root_face_block.action.image.list[index]['file']  =file;

                                    reader.onload=function(){

                                        page_object.action.profile.action.root_face_block.action.image.preview(reader.result,function(){},index);

                                    };

                                    reader.readAsDataURL(file);

                                    break;

                                }

                                default:{

                                    page_object.action.profile.action.root_face_block.action.image.reset_item(index);

                                    $d('root_search_face_image_error_'+index).innerHTML     ='Неверный формат файла';
                                    $s('root_search_face_image_error_'+index).display       ='block';
                                    $s('root_search_face_image_error_'+index).color         ='#ff0000';

                                    setTimeout(function(){

                                        $s('root_search_face_image_error_'+index).opacity=1;

                                    },40);

                                    setTimeout(function(){

                                        $s('root_search_face_image_error_'+index).opacity=0;

                                        setTimeout(function(){

                                            $s('root_search_face_image_error_'+index).display    ='none';
                                            $s('root_search_face_image_error_'+index).color      ='#';
                                            $d('root_search_face_image_error_'+index).innerHTML  ='Загрузка...';

                                            if(isset($d('root_search_face_image_block_'+index)))
                                                $s('root_search_face_image_block_'+index).borderColor='';

                                        },340);

                                    },3000);

                                    break;

                                }

                            }

                        }

                    }
                }
            },
            'send_link':function(image_link,index){

                if(page_object.action.profile.action.root_face_block.action.image.drag.is_work)
                    return false;

                page_object.action.profile.action.root_face_block.action.image.drag.is_work=true;

                let post='';

                post+='image_link='+uniEncode(image_link);

                if(isset($d('root_search_face_button'))){

                    $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                    $d('root_search_face_button').innerHTML     ='Загрузка...';
                    $d('root_search_face_button').onclick       =function(){};

                }
                else
                    return false;

                if(isset($d('root_search_face_image_block_'+index)))
                    $s('root_search_face_image_block_'+index).borderColor='';

                if(isset($d('root_search_face_button_choose_'+index)))
                    $s('root_search_face_button_choose_'+index).opacity=0;

                if(isset($d('root_search_face_image_info_'+index)))
                    $s('root_search_face_image_info_'+index).opacity=0;

                if(isset($d('root_search_face_image_src_object_'+index)))
                    $s('root_search_face_image_src_object_'+index).opacity=0;

                setTimeout(function(){

                    if(isset($d('root_search_face_image_src_object_'+index)))
                        removeElement($d('root_search_face_image_src_object_'+index));

                    if(isset($d('root_search_face_button_choose_'+index)))
                        $s('root_search_face_button_choose_'+index).display='none';

                    if(isset($d('root_search_face_image_info_'+index)))
                        $s('root_search_face_image_info_'+index).display='none';

                    if(isset($d('root_search_face_image_loading_'+index)))
                        $s('root_search_face_image_loading_'+index).display='block';

                    setTimeout(function(){

                        $s('root_search_face_image_loading_'+index).opacity=1;

                    },40);

                },300);

                send({
                    'scriptPath':'/api/json/upload_file_from_link',
                    'postData':post,
                    'onComplete':function(j,worktime){

                        trace('SEND IMAGE LINK FROM PROFILE');

                        let  data
                            ,dataTemp;

                        dataTemp=j.responseText;
                        trace(dataTemp);
                        data=jsonDecode(dataTemp);
                        trace(data);
                        trace_worktime(worktime,data);

                        page_object.action.profile.action.root_face_block.action.image.drag.is_work=false;

                        if(isset(data['error'])){

                            if(isset($d('root_search_info_block'))){

                                $d('root_search_info_block').setAttribute('class','root_search_info_block_red');

                                $d('root_search_info_block').innerHTML='Проблемы с загрузкой файла';

                                setTimeout(function(){

                                    if(isset($d('root_search_info_block'))){

                                        $s('root_search_info_block').opacity    =1;
                                        $s('root_search_info_block').height     ='auto';

                                    }

                                },40);

                                setTimeout(function(){

                                    if(isset($d('root_search_info_block'))){

                                        $s('root_search_info_block').opacity    =0;
                                        $s('root_search_info_block').height     ='';

                                    }

                                },3000);

                            }

                            setTimeout(page_object.action.profile.action.root_face_block.action.image.reset,3000);

                        }
                        else{

                            page_object.action.profile.action.root_face_block.action.image.list[index]['file_ID']   =data['data']['file_ID'];
                            page_object.action.profile.action.root_face_block.action.image.list[index]['image_ID']  =data['data']['image_ID'];
                            page_object.action.profile.action.root_face_block.action.image.list[index]['link']      =data['data']['link'];

                            page_object.action.profile.action.root_face_block.action.image.preview(data['data']['link'],function(){},index);

                        }

                    }
                });

            },
            'image_load':function(image_link,callback,index){

                if(isset($d('root_search_face_image_src_object_'+index))){

                    $s('root_search_face_image_src_object_'+index).opacity=0;

                    setTimeout(function(){

                        if(isset($d('root_search_face_image_src_'+index)))
                            $d('root_search_face_image_src_'+index).innerHTML='';

                        page_object.action.profile.action.root_face_block.action.image.image_load(image_link,callback,index);

                    },300);

                    return true;

                }

                var img=new Image();

                img.src         =image_link;
                img.onload      =function(){

                    page_object.action.profile.action.root_face_block.action.image.image_len++;

                    if(isset($d('root_search_face_button'))){

                        let user_data=page_object.action.profile.data['user'];

                        if(parseFloat(user_data['balance'])<user_data['price']){

                            $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                            $d('root_search_face_button').innerHTML     ='<span class="red">Пополните баланс</span>';
                            $d('root_search_face_button').onclick       =function(){};

                        }
                        else{

                            $d('root_search_face_button').setAttribute('class','root_search_face_button');

                            $d('root_search_face_button').innerHTML     =page_object.action.profile.action.root_face_block.action.image.image_len===1?'Найти лица':'Найти лица';
                            $d('root_search_face_button').onclick       =page_object.action.profile.action.root_face_block.action.image.upload.init;

                        }

                        if(!isset(page_object.action.profile)){

                            $d('root_search_face_button').setAttribute('class','root_search_face_button');

                            $d('root_search_face_button').innerHTML     =page_object.action.profile.action.root_face_block.action.image.image_len===1?'Найти лица':'Найти лица';
                            $d('root_search_face_button').onclick       =page_object.action.profile.action.root_face_block.action.reg_or_auth.init;

                        }

                    }

                    if(!isset($d('root_search_face_reset_'+index))){

                        el=addElement({
                            'tag':'div',
                            'id':'root_search_face_reset_'+index,
                            'class':'root_search_face_reset',
                            'style':'margin-left: -19px; margin-top: -1px;'
                        });

                        if(isset($d('root_search_face_image_block_container_'+index)))
                            $d('root_search_face_image_block_container_'+index).appendChild(el);

                    }

                    if(!isset($d('root_search_face_reset'))&&!OS.isMobile){

                        el=addElement({
                            'tag':'div',
                            'id':'root_search_face_reset',
                            'class':'root_search_face_reset',
                            'style':'margin-left: -19px; margin-top: -1px;'
                        });

                        $d('root_face_content').appendChild(el);

                    }

                    if(isset($d('root_search_face_reset_'+index)))
                        $d('root_search_face_reset_'+index).onclick=page_object.action.profile.action.root_face_block.action.image.reset_item;

                    if(isset($d('root_search_face_reset')))
                        $d('root_search_face_reset').onclick=page_object.action.profile.action.root_face_block.action.image.reset;

                    if(isset($d('root_search_face_button_cancel')))
                        $d('root_search_face_button_cancel').onclick=page_object.action.profile.action.root_face_block.action.image.reset;

                    if(isset($d('root_search_face_image_loading_'+index)))
                        $s('root_search_face_image_loading_'+index).opacity=0;

                    setTimeout(function(){

                        if(isset($d('root_search_face_image_loading_'+index)))
                            $s('root_search_face_image_loading_'+index).display='none';

                    },300);

                    var  img_w      =this.width
                        ,img_h      =this.height
                        ,perc       =img_w/img_h
                        ,el_w       =360
                        ,el_h       =OS.isMobile?(winSize.winHeight-200):(winSize.winHeight-300)
                        ,r_img_w
                        ,r_img_h;

                    if(OS.isMobile){

                        // el_w=elementSize.width($d('root_search_face_image_block_'+index));
                        el_w=winSize.winWidth-35;

                    }

                    if(img_w>=img_h){

                        r_img_w     =el_w;
                        r_img_h     =r_img_w/perc;

                    }
                    else{

                        r_img_w     =el_w;
                        r_img_h     =r_img_w/perc;

                        if(r_img_h>el_h){

                            r_img_h     =el_h;
                            r_img_w     =r_img_h*perc;

                        }

                    }

                    var el=addElement({
                        'tag':'img',
                        'id':'root_search_face_image_src_object_'+index,
                        'class':'root_search_face_image_src_object',
                        'src':image_link,
                        'width':r_img_w,
                        'height':r_img_h
                    });

                    if(isset($d('root_search_face_image_src_'+index)))
                        $d('root_search_face_image_src_'+index).appendChild(el);

                    setTimeout(function(){

                        if(isset($d('root_search_face_image_src_object_'+index))){

                            if(isset($d('root_right_content'))){

                                $s('root_right_content').opacity=0;

                                setTimeout(function(){

                                    $s('root_right_content').display='none';

                                },300);

                            }

                            if(isset($d('root_left_content'))){

                                $s('root_left_content').opacity=0;

                                setTimeout(function(){

                                    $s('root_left_content').display='none';

                                },300);

                            }

                            if(isset($d('root_search_face_image_row_container_'+index))){

                                trace('IMAGE LOAD INDEX: '+index);

                                $s('root_search_face_image_row_container_'+index).height=r_img_h+'px';

                            }

                            if(isset($d('root_search_face_image_block_'+index))){

                                $s('root_search_face_image_block_'+index).width        =r_img_w+'px';
                                $s('root_search_face_image_block_'+index).height       =r_img_h+'px';
                                // $s('root_search_face_image_block_'+index).transform    ='translate(-'+((r_img_w)/2)+'px, 0)';

                                if(!OS.isMobile)
                                    $s('root_search_face_image_block_'+index).transform='translate(calc((400px - '+r_img_w+'px)/2), -50%)';

                                if(OS.isMobile){

                                    $s('root_search_face_image_block_'+index).top                       =0;
                                    $s('root_search_face_image_row_container_'+index).marginBottom      =index===2?0:'10px';

                                }

                            }

                            if(isset($d('root_search_face_reset_'+index)))
                                $s('root_search_face_reset_'+index).opacity=1;

                            if(isset($d('root_face_content'))){

                                if(!OS.isMobile){

                                    let user_data=page_object.action.profile.data['user'];

                                    if(parseFloat(user_data['balance'])>user_data['price'])
                                        $s('root_face_content').width='1200px';
                                    else
                                        $s('root_face_content').width='440px';

                                }

                                // $s('root_face_content').left=0;

                            }

                            if(isset($d('root_search_face_image_block_container_'+index))){

                                $s('root_search_face_image_block_container_'+index).height=r_img_h+'px';

                            }

                            setTimeout(function(){

                                let  h_max=0
                                    ,img_h
                                    ,image_index;

                                if(isset($d('root_search_face_image_src_'+index)))
                                    $s('root_search_face_image_src_'+index).opacity=1;

                                for(image_index=0;image_index<3;image_index++)
                                    if(isset($d('root_search_face_image_src_object_'+image_index))){

                                        img_h=elementSize.height($d('root_search_face_image_src_object_'+image_index));

                                        if(img_h>h_max)
                                            h_max=img_h;

                                    }

                                let user_data=page_object.action.profile.data['user'];

                                if(isset($d('root_search_face_image_row_container_0'))){

                                    if(!OS.isMobile)
                                        $s('root_search_face_image_row_container_0').height=h_max+'px';

                                    $s('root_search_face_image_row_container_0').display='block';

                                    setTimeout(function(){

                                        $s('root_search_face_image_row_container_0').opacity=1;

                                    },40);

                                }

                                if(parseFloat(user_data['balance'])>user_data['price']){

                                    if(isset($d('root_search_face_image_row_container_1'))){

                                        if(!OS.isMobile)
                                            $s('root_search_face_image_row_container_1').height=h_max+'px';

                                        $s('root_search_face_image_row_container_1').display='block';

                                        setTimeout(function(){

                                            $s('root_search_face_image_row_container_1').opacity=1;

                                        },40);

                                    }

                                    if(isset($d('root_search_face_image_row_container_2'))){

                                        if(!OS.isMobile)
                                            $s('root_search_face_image_row_container_2').height=h_max+'px';

                                        $s('root_search_face_image_row_container_2').display='block';

                                        setTimeout(function(){

                                            $s('root_search_face_image_row_container_2').opacity=1;

                                        },40);

                                    }

                                }

                            },300);

                        }

                    },40);

                };
                img.onerror     =function(){};

            },
            'preload':function(file,callback,index){

                if(isset($d('root_search_face_image_block_'+index)))
                    $s('root_search_face_image_block_'+index).borderColor='';

                if(isset($d('root_search_face_button_choose_'+index)))
                    $s('root_search_face_button_choose_'+index).opacity=0;

                if(isset($d('root_search_face_button'))){

                    $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                    $d('root_search_face_button').innerHTML     ='Загрузка...';
                    $d('root_search_face_button').onclick       =function(){};

                }
                else
                    return false;

                trace('preload!!!!!!');

                if(isset($d('root_search_face_image_loading_'+index)))
                    $s('root_search_face_image_loading_'+index).display='block';

                setTimeout(function(){

                    $s('root_search_face_image_loading_'+index).opacity=1;

                },40);

                if(OS.isMobile){

                    if(isset($d('root_left_content'))){

                        $s('root_left_content').opacity     =0;
                        $s('root_left_content').padding     =0;
                        $s('root_left_content').height      =0;

                    }

                    if(isset($d('root_right_content'))){

                        $s('root_right_content').opacity    =0;
                        $s('root_right_content').padding    =0;
                        $s('root_right_content').height     =0;

                    }

                    if(isset($d('root_search_face_button'))){

                        $s('root_search_face_button').display='block';

                    }

                    if(isset($d('root_search_face_button_cancel'))){

                        $s('root_search_face_button_cancel').display='block';

                    }

                    if(isset($d('root_search_face_button_shot')))
                        $s('root_search_face_button_shot').display='none';

                }

                if(isset($d('root_search_face_image_info_'+index)))
                    $s('root_search_face_image_info_'+index).opacity=0;

                if(isset($d('root_search_face_image_src_object_'+index)))
                    $s('root_search_face_image_src_object_'+index).opacity=0;

                setTimeout(function(){

                    if(isset($d('root_search_face_image_src_object_'+index)))
                        removeElement($d('root_search_face_image_src_object_'+index));

                    if(isset($d('root_search_face_button_choose_'+index)))
                        $s('root_search_face_button_choose_'+index).display='none';

                    if(isset($d('root_search_face_image_info_'+index)))
                        $s('root_search_face_image_info_'+index).display='none';

                    let el;

                    if(isset($d('root_search_face_image_loading_'+index)))
                        $s('root_search_face_image_loading_'+index).display='block';

                    setTimeout(function(){

                        $d('root_search_face_image_loading_'+index).innerHTML   ='<span>Загрузка...</span>';
                        $s('root_search_face_image_loading_'+index).opacity     =1;

                        callback(file,index);

                    },40);

                },300);

            },
            'preview':function(result,callback,index){

                page_object.action.profile.action.root_face_block.action.image.image_load(result,callback,index);

            },
            'change':function(e){
                
                e=e||window.event;

                let  file               =e.target.files
                    ,file_list          =page_object.action.profile.action.root_face_block.action.image.list
                    ,i
                    ,index              =this.id.split('_')[5]
                    ,image_price_len    =3
                    ,image_len          =page_object.action.profile.action.root_face_block.action.image.image_len
                    ,index_current      =0
                    ,image_max_len      =3-image_len
                    ,total_cost         =0
                    ,user_data          =page_object.action.profile.data['user'];

                switch(file.length){

                    case 0:{

                        total_cost=0;

                        break;

                    }

                    case 1:{

                        total_cost=user_data['price'];

                        break;

                    }

                    default:{

                        total_cost=2*user_data['price'];

                        break;

                    }

                }

                if(parseFloat(user_data['balance'])<total_cost){

                    file=[file[0]];

                }

                if(file.length===1)
                    page_object.action.profile.action.root_face_block.action.image.change_index(index,file[0]);
                else{

                    for(i=0;i<file.length;i++){

                        if(index_current!==0){

                            for(index=0;index<3;index++)
                                if(
                                      empty(file_list[index]['file'])
                                    &&empty(file_list[index]['link'])
                                )
                                    break;

                        }

                        page_object.action.profile.action.root_face_block.action.image.change_index(index,file[i]);

                        index_current++;

                        if(index_current>=image_max_len)
                            break;

                    }

                }

            },
            'change_index':function(index,f){

                page_object.action.profile.action.root_face_block.action.image.list[index]['file']=f;

                if(isset($d('root_search_face_image_src_'+index)))
                    $s('root_search_face_image_src_'+index).opacity=0;

                page_object.action.profile.action.root_face_block.action.image.preload(f,function(f,index){

                    prepare_image_orientation.get_image_with_true_orientation(f,function(file,callback,index){

                        page_object.action.profile.action.root_face_block.action.image.preview(file,callback,index);

                    },index);

                },index);

            },
            'reset_item':function(index){

                trace('RESET ITEM: '+index);

                let  i
                    ,list           =page_object.action.profile.action.root_face_block.action.image.list
                    ,list_count     =0;

                page_object.action.profile.action.root_face_block.action.image.image_len--;

                for(i=0;i<3;i++)
                    if(
                          !empty(list[i]['image_ID'])
                        ||!empty(list[i]['file'])
                        ||!empty(list[i]['link'])
                    )
                        list_count++;

                trace('RESET ITEM 1: '+index);

                if(list_count===1){

                    page_object.action.profile.action.root_face_block.action.image.reset();

                    return true;

                }

                trace('RESET ITEM 2: '+index);

                index=!isNumber(index)?this.id.split('_')[4]:index;

                trace('This: '+this.id);
                trace('Index: '+index);

                if(isset($d('root_search_face_image_input_'+index)))
                    $d('root_search_face_image_input_'+index).value='';

                trace('Index: '+index);

                page_object.action.profile.action.root_face_block.action.image.list[index]['file_ID']       =null;
                page_object.action.profile.action.root_face_block.action.image.list[index]['image_ID']      =null;
                page_object.action.profile.action.root_face_block.action.image.list[index]['file']          =null;
                page_object.action.profile.action.root_face_block.action.image.list[index]['link']          =null;

                if(isset($d('root_search_face_reset_'+index)))
                    $s('root_search_face_reset_'+index).opacity=0;

                if(isset($d('root_search_face_image_loading_'+index)))
                    $s('root_search_face_image_loading_'+index).opacity=0;

                if(isset($d('root_search_face_image_error_'+index)))
                    $s('root_search_face_image_error_'+index).opacity=0;

                if(isset($d('root_search_face_coords_list_'+index)))
                    removeElement($d('root_search_face_coords_list_'+index));

                if(isset($d('root_search_face_image_src_object_'+index)))
                    $s('root_search_face_image_src_object_'+index).opacity=0;

                if(isset($d('root_search_face_button_choose_'+index)))
                    $s('root_search_face_button_choose_'+index).display='block';

                if(OS.isMobile){

                    if(isset($d('root_search_face_image_block_'+index)))
                        $d('root_search_face_image_block_'+index).setAttribute('style','');

                }
                else{

                    if(isset($d('root_search_face_image_block_'+index))){

                        $s('root_search_face_image_block_'+index).width         ='';
                        $s('root_search_face_image_block_'+index).height        ='';
                        $s('root_search_face_image_block_'+index).transform     ='';

                    }

                }

                if(isset($d('root_search_face_image_row_container_'+index))){

                    trace('RESET INDEX CONTAINER: '+index);

                    $s('root_search_face_image_row_container_'+index).height    ='auto';


                }

                if(!OS.isMobile)
                    if(isset($d('root_search_face_image_info_'+index)))
                        $s('root_search_face_image_info_'+index).display='block';

                setTimeout(function(){

                    if(isset($d('root_search_face_button_choose_'+index)))
                        $s('root_search_face_button_choose_'+index).opacity=1;

                    if(isset($d('root_search_face_image_info_'+index)))
                        $s('root_search_face_image_info_'+index).opacity=1;

                    if(isset($d('root_search_face_image_src_'+index)))
                        $s('root_search_face_image_src_'+index).opacity=0;

                },40);

                setTimeout(function(){

                    if(isset($d('root_search_face_image_loading_'+index)))
                        $s('root_search_face_image_loading_'+index).display='none';

                    if(isset($d('root_search_face_image_error_'+index)))
                        $s('root_search_face_image_error_'+index).display='none';

                    if(isset($d('root_search_face_reset_'+index)))
                        removeElement($d('root_search_face_reset_'+index));

                    if(isset($d('root_search_face_image_src_object_'+index)))
                        removeElement($d('root_search_face_image_src_object_'+index));

                    if(isset($d('root_search_face_image_src_'+index)))
                        $d('root_search_face_image_src_'+index).innerHTML='';

                    let  i
                        ,list           =page_object.action.profile.action.root_face_block.action.image.list
                        ,list_count     =0;

                    for(i=0;i<3;i++)
                        if(
                              !empty(list[i]['image_ID'])
                            ||!empty(list[i]['file'])
                            ||!empty(list[i]['link'])
                        )
                            list_count++;

                    trace('LIST COUNT: '+list_count);

                },300);

            },
            'reset':function(){

                let index;

                page_object.upload.reset();

                page_object.action.profile.action.root_face_block.action.image.image_len            =0;
                page_object.action.profile.action.root_face_block.action.image.face.face_change_len =0;

                page_object.action.profile.action.root_face_block.action.image.list=[
                    {
                        'image_ID':null,
                        'link':null,
                        'file':null
                    },
                    {
                        'image_ID':null,
                        'link':null,
                        'file':null
                    },
                    {
                        'image_ID':null,
                        'link':null,
                        'file':null
                    }
                ];
                page_object.action.profile.action.root_face_block.action.image.face.list=[
                    {
                        'face_index':null,
                        'image_ID':null,
                        'image_data':null,
                        'file_ID':null,
                        'face_len':0,
                        'face_list':null
                    },
                    {
                        'face_index':null,
                        'image_ID':null,
                        'image_data':null,
                        'file_ID':null,
                        'face_len':0,
                        'face_list':null
                    },
                    {
                        'face_index':null,
                        'image_ID':null,
                        'image_data':null,
                        'file_ID':null,
                        'face_len':0,
                        'face_list':null
                    }
                ];

                if(isset($d('root_search_face_content_loading'))){

                    $s('root_search_face_content_loading').opacity=0;

                    setTimeout(function(){

                        if(isset($d('root_search_face_content_loading')))
                            $s('root_search_face_content_loading').display='none';

                    },300);

                }

                if(isset($d('root_search_face_price'))){

                    $s('root_search_face_price').opacity=0;

                    setTimeout(function(){

                        $s('root_search_face_price').display    ='none';
                        $d('root_search_face_price').innerHTML  ='Стоимость запроса <span id="root_search_face_cost">0</span> ₽';

                    },300);

                }

                for(index=0;index<3;index++){

                    if(isset($d('root_search_face_image_input_'+index)))
                        $d('root_search_face_image_input_'+index).value='';

                    page_object.action.profile.action.root_face_block.action.image.list[index]['file_ID']       =null;
                    page_object.action.profile.action.root_face_block.action.image.list[index]['image_ID']      =null;
                    page_object.action.profile.action.root_face_block.action.image.list[index]['file']          =null;
                    page_object.action.profile.action.root_face_block.action.image.list[index]['link']          =null;

                    if(isset($d('root_search_face_reset_'+index)))
                        $s('root_search_face_reset_'+index).opacity=0;

                    if(isset($d('root_search_face_image_loading_'+index)))
                        $s('root_search_face_image_loading_'+index).opacity=0;

                    if(isset($d('root_search_face_coords_list_'+index)))
                        removeElement($d('root_search_face_coords_list_'+index));

                    if(isset($d('root_search_face_image_src_object_'+index)))
                        $s('root_search_face_image_src_object_'+index).opacity=0;

                    if(isset($d('root_search_face_button_choose_'+index)))
                        $s('root_search_face_button_choose_'+index).display='block';

                    if(OS.isMobile){

                        if(isset($d('root_search_face_image_block_'+index)))
                            $d('root_search_face_image_block_'+index).setAttribute('style','');

                    }
                    else{

                        if(isset($d('root_search_face_image_block_'+index))){

                            $s('root_search_face_image_block_'+index).width         ='';
                            $s('root_search_face_image_block_'+index).height        ='';
                            $s('root_search_face_image_block_'+index).transform     ='';

                        }

                    }

                    if(isset($d('root_search_face_image_row_container_'+index))){

                        trace('RESET INDEX CONTAINER: '+index);

                        $s('root_search_face_image_row_container_'+index).display   =(parseInt(index)===0)?'block':'none';
                        $s('root_search_face_image_row_container_'+index).opacity   =(parseInt(index)===0)?1:0;
                        $s('root_search_face_image_row_container_'+index).height    ='auto';


                    }

                    if(isset($d('root_search_face_image_error_'+index))){

                        $s('root_search_face_image_error_'+index).opacity   =0;
                        $s('root_search_face_image_error_'+index).display   ='none';

                    }

                    if(!OS.isMobile)
                        if(isset($d('root_search_face_image_info_'+index)))
                            $s('root_search_face_image_info_'+index).display='block';

                }

                if(isset($d('root_left_content'))){

                    $s('root_left_content').display='block';

                    setTimeout(function(){

                        $s('root_left_content').opacity=1;

                    },40);

                }

                if(isset($d('root_right_content'))){

                    $s('root_right_content').display='block';

                    setTimeout(function(){

                        $s('root_right_content').opacity=1;

                    },40);

                }

                if(isset($d('root_face_content'))){

                    $s('root_face_content').width       ='';
                    $s('root_face_content').left        ='';
                    $s('root_face_content').transform   ='';

                }

                if(OS.isMobile){

                    if(isset($d('root_left_content'))){

                        $s('root_left_content').opacity     =1;
                        $s('root_left_content').padding     ='';
                        $s('root_left_content').height      ='auto';

                    }

                    if(isset($d('root_right_content'))){

                        $s('root_right_content').opacity    =1;
                        $s('root_right_content').padding    ='';
                        $s('root_right_content').height     ='auto';

                    }

                    if(isset($d('root_search_face_button')))
                        $s('root_search_face_button').display='none';

                    if(isset($d('root_search_face_button_cancel')))
                        $s('root_search_face_button_cancel').display='none';

                    if(isset($d('root_search_face_button_shot')))
                        $s('root_search_face_button_shot').display='block';

                }

                setTimeout(function(){

                    for(index=0;index<3;index++){

                        if(isset($d('root_search_face_button_choose_'+index)))
                            $s('root_search_face_button_choose_'+index).opacity=1;

                        if(isset($d('root_search_face_image_info_'+index)))
                            $s('root_search_face_image_info_'+index).opacity=1;

                        if(isset($d('root_search_face_image_src_'+index)))
                            $s('root_search_face_image_src_'+index).opacity=0;

                    }

                },40);

                setTimeout(function(){

                    if(isset($d('root_search_face_button_label')))
                        $d('root_search_face_button_label').innerHTML='Проверить фото';
                    else if(isset($d('root_search_face_button')))
                        $d('root_search_face_button').innerHTML='Проверить фото';

                    if(isset($d('root_search_face_button_progress')))
                        $s('root_search_face_button_progress').width=0;

                    if(isset($d('root_search_face_reset')))
                        removeElement($d('root_search_face_reset'));

                    for(index=0;index<3;index++){

                        if(isset($d('root_search_face_image_loading_'+index)))
                            $s('root_search_face_image_loading_'+index).display='none';

                        if(isset($d('root_search_face_reset_'+index)))
                            removeElement($d('root_search_face_reset_'+index));

                        if(isset($d('root_search_face_image_src_object_'+index)))
                            removeElement($d('root_search_face_image_src_object_'+index));

                        if(isset($d('root_search_face_image_src_'+index)))
                            $d('root_search_face_image_src_'+index).innerHTML='';

                    }

                    if(isset($d('root_search_face_button'))){

                        $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                        $d('root_search_face_button').innerHTML     ='Проверить фото';
                        $d('root_search_face_button').onclick       =function(){};

                    }

                },300);

            },
            'upload':{
                'reset_box_size':function(){

                    let  list       =page_object.action.profile.action.root_face_block.action.image.list
                        ,index
                        ,block_w
                        ,image_len  =0;

                    for(index in list)
                        if(
                              empty(list[index]['file'])
                            &&empty(list[index]['link'])
                        ){

                            if(isset($d('root_search_face_image_row_container_'+index))){

                                image_len++;

                                $s('root_search_face_image_row_container_'+index).display='none';

                            }

                            if(!OS.isMobile){

                                block_w=((3-image_len)*400+40);

                                $s('root_face_content').width       =block_w+'px';
                                $s('root_face_content').left        ='50%';
                                $s('root_face_content').transform   ='translate(-50%,calc(-50% + 15px))';

                            }

                        }

                    trace('IMAGE LEN: '+image_len);

                    if(image_len===3)
                        if(isset($d('root_search_face_image_row_container_0')))
                            $s('root_search_face_image_row_container_0').display='block';

                },
                'init':function(){

                    let  list       =page_object.action.profile.action.root_face_block.action.image.list
                        ,index
                        ,block_w
                        ,image_len  =0;

                    for(index in list)
                        if(
                              !empty(list[index]['file'])
                            ||!empty(list[index]['link'])
                        ){

                            if(isset($d('root_search_face_image_loading_'+index))){

                                $s('root_search_face_image_loading_'+index).display='block';

                            }

                            page_object.action.profile.action.root_face_block.action.image.upload.init_start(index);

                        }
                        else{

                            if(isset($d('root_search_face_image_row_container_'+index))){

                                image_len++;

                                $s('root_search_face_image_row_container_'+index).display='none';

                            }

                            if(!OS.isMobile){

                                block_w=((3-image_len)*400+40);

                                $s('root_face_content').width       =block_w+'px';
                                $s('root_face_content').left        ='50%';
                                $s('root_face_content').transform   ='translate(-50%,calc(-50% + 15px))';

                            }

                        }

                    setTimeout(function(){

                        let  list=page_object.action.profile.action.root_face_block.action.image.list
                            ,index;

                        for(index in list)
                            if(
                                  !empty(list[index]['file'])
                                ||!empty(list[index]['link'])
                            ){

                                $s('root_search_face_image_loading_'+index).opacity=1;

                            }

                    },40);

                },
                'init_start':function(index){

                    if(isset($d('root_search_face_content_loading'))){

                        $s('root_search_face_content_loading').opacity=0;

                        setTimeout(function(){

                            if(isset($d('root_search_face_content_loading')))
                                $s('root_search_face_content_loading').display='none';

                        },300);

                    }

                    if(!empty(page_object.action.profile.action.root_face_block.action.image.list[index]['file']))
                        page_object.action.profile.action.root_face_block.action.image.upload.file.init(index);
                    else if(!empty(page_object.action.profile.action.root_face_block.action.image.list[index]['link']))
                        page_object.action.profile.action.root_face_block.action.image.upload.link.init(index);

                },
                'link':{
                    'init':function(index){

                        page_object.action.profile.action.root_face_block.action.image.face.init(index);

                    }
                },
                'file':{
                    'init':function(index){

                        page_object.action.profile.action.root_face_block.action.image.upload.file.on_change_file(index);

                    },
                    'on_change_file':function(index){

                        trace('CHANGE UPLOAD INDEX: '+index);

                        let  data
                            ,list           =[]
                            ,i
                            ,file_list      =[
                                page_object.action.profile.action.root_face_block.action.image.list[index]['file']
                            ]
                            ,list_item      ={};

                        if(file_list.length===0)
                            return false;

                        let user_data=page_object.action.profile.data['user'];

                        if(parseFloat(user_data['balance'])<user_data['price']){

                            if(isset($d('root_search_face_button'))){

                                $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                                $d('root_search_face_button').innerHTML     ='<span class="red">Пополните баланс</span>';
                                $d('root_search_face_button').onclick       =function(){};

                            }

                            alert('Недостаточно средств для совершения действия');

                            return false;

                        }

                        if(isset($d('root_search_face_image_loading_'+index))){

                            $d('root_search_face_image_loading_'+index).innerHTML='<span>Загрузка 1%</span>';

                            if(isset($d('root_search_face_image_loading_'+index)))
                                $s('root_search_face_image_loading_'+index).display='block';

                            setTimeout(function(){

                                if(isset($d('root_search_face_image_loading_'+index)))
                                    $s('root_search_face_image_loading_'+index).opacity=1;

                            },40);

                        }

                        if(isset($d('root_search_face_button'))){

                            $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                            $d('root_search_face_button').innerHTML     ='Загрузка 1%';
                            $d('root_search_face_button').onclick       =function(){};

                        }
                        else
                            return false;

                        for(i=0;i<file_list.length;i++){

                            list_item={
                                'image_index':index,
                                'file_ID':null,
                                'remove':false,
                                'file':file_list[i]
                            };

                            list.push(list_item);

                        }

                        data={
                            'on_progress':function(){

                                page_object.action.profile.action.root_face_block.action.image.upload.file.on_progress.init(index);

                            },
                            'on_uploaded':function(){

                                page_object.action.profile.action.root_face_block.action.image.upload.file.on_uploaded.init(index);

                            },
                            'on_complete':function(){

                                page_object.action.profile.action.root_face_block.action.image.upload.file.on_complete.init(index);

                            },
                            'file_type':'image',
                            'list':list
                        };

                        // page_object.upload.reset();

                        page_object.upload.init(null,data);

                    },
                    'on_progress':{
                        'init':function(index){

                            index=page_object.upload.data['list'][page_object.upload.file_item['index']]['image_index'];

                            trace('Progress index: '+index);

                            let perc=(page_object.upload.data['list'][page_object.upload.file_item['index']]['percent']*100).toFixed(1);

                            if(isset($d('root_search_face_image_loading_'+index)))
                                $d('root_search_face_image_loading_'+index).innerHTML='<span>Загрузка '+perc+'%</span>';

                            if(isset($d('root_search_face_button'))){

                                let  list=page_object.action.profile.action.root_face_block.action.image.list
                                    ,index;

                                for(index in list)
                                    if(
                                          !empty(list[index]['file'])
                                        ||!empty(list[index]['link'])
                                    ){



                                    }

                                let perc=(page_object.upload.data['list'][page_object.upload.file_item['index']]['percent']*100).toFixed(1);

                                if((page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_index']+2)<page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_len'])
                                    $d('root_search_face_button').innerHTML='Загрузка '+perc+'%';
                                else
                                    $d('root_search_face_button').innerHTML='Обработка изображения...';

                            }

                        }
                    },
                    'on_uploaded':{
                        'init':function(index){

                            index=page_object.upload.data['list'][page_object.upload.file_item['index']]['image_index'];

                            trace('Uploaded index: '+index);

                            let  cost           =500
                                ,total_cost     =0;

                            if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===0)
                                total_cost=0;
                            else if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===1)
                                total_cost=cost;
                            else
                                total_cost=2*cost;

                            let user_data=page_object.action.profile.data['user'];

                            if(parseFloat(user_data['balance'])<total_cost){

                                if(isset($d('root_search_face_button'))){

                                    $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                                    $d('root_search_face_button').innerHTML     ='<span class="red">Пополните баланс</span>';
                                    $d('root_search_face_button').onclick       =page_object.action.profile.action.root_face_block.action.image.upload.init;

                                }

                                return false;

                            }

                            if(isset($d('root_search_face_image_loading_'+index)))
                                $d('root_search_face_image_loading_'+index).innerHTML='<span>Поиск лиц...</span>';

                            page_object.action.profile.action.root_face_block.action.image.list[index]['file_ID']      =page_object.upload.data['list'][page_object.upload.file_item['index']]['file_ID'];
                            page_object.action.profile.action.root_face_block.action.image.list[index]['image_ID']     =page_object.upload.data['list'][page_object.upload.file_item['index']]['image_ID'];

                            if(isset($d('root_search_face_button'))){

                                $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                                $d('root_search_face_button').innerHTML     ='Поиск лиц...';
                                $d('root_search_face_button').onclick       =function(){};

                            }

                            page_object.action.profile.action.root_face_block.action.image.face.init(index);

                        }
                    },
                    'on_complete':{
                        'init':function(index){}
                    }
                }
            },
            'face':{
                'list':[
                    {
                        'face_index':null,
                        'image_ID':null,
                        'image_data':null,
                        'file_ID':null,
                        'face_len':0,
                        'face_list':null
                    },
                    {
                        'face_index':null,
                        'image_ID':null,
                        'image_data':null,
                        'file_ID':null,
                        'face_len':0,
                        'face_list':null
                    },
                    {
                        'face_index':null,
                        'image_ID':null,
                        'image_data':null,
                        'file_ID':null,
                        'face_len':0,
                        'face_list':null
                    }
                ],
                'face_token_list':[],
                'face_change_len':0,
                'face_delta_x':1,
                'face_delta_y':1,
                'face_coords_list':null,
                'face_image_min_w':70,
                'face_image_min_h':70,
                'init':function(index){

                    if(
                          !isset($d('root_search_face_image_src_object_0'))
                        &&!isset($d('root_search_face_image_src_object_1'))
                        &&!isset($d('root_search_face_image_src_object_2'))
                    )
                        return false;

                    if(!isset($d('root_search_face_image_src_object_'+index)))
                        return false;

                    let  cost           =500
                        ,total_cost     =0;

                    if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===0)
                        total_cost=0;
                    else if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===1)
                        total_cost=cost;
                    else
                        total_cost=2*cost;

                    var  post       =''
                        ,user_data  =page_object.action.profile.data['user'];

                    if(parseFloat(user_data['balance'])<total_cost){

                        if(isset($d('root_search_face_button'))){

                            $d('root_search_face_button').setAttribute('class','root_search_face_button');

                            $d('root_search_face_button').innerHTML     ='Проверить фото';
                            $d('root_search_face_button').onclick       =page_object.action.profile.action.root_face_block.action.image.upload.init;

                        }

                        alert('Недостаточно средств для совершения действия');

                        return false;

                    }

                    if(isset($d('root_search_face_button'))){

                        $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                        $d('root_search_face_button').innerHTML     ='Поиск лиц...';
                        $d('root_search_face_button').onclick       =function(){};

                    }

                    post+='image_ID='+uniEncode(page_object.action.profile.action.root_face_block.action.image.list[index]['image_ID']);
                    post+='&file_ID='+uniEncode(page_object.action.profile.action.root_face_block.action.image.list[index]['file_ID']);

                    send({
                        'scriptPath':'/api/json/get_faces_coords',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            if(
                                  !isset($d('root_search_face_image_src_object_0'))
                                &&!isset($d('root_search_face_image_src_object_1'))
                                &&!isset($d('root_search_face_image_src_object_2'))
                            )
                                return false;

                            if(!isset($d('root_search_face_image_src_object_'+index)))
                                return false;

                            let  data
                                ,dataTemp;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if(isset($d('root_search_info_block'))){

                                    $d('root_search_info_block').setAttribute('class','root_search_info_block_red');
                                    $d('root_search_info_block').innerHTML=stripSlashes(data['error']['info']);

                                    setTimeout(function(){

                                        if(isset($d('root_search_info_block'))){

                                            $s('root_search_info_block').opacity    =1;
                                            $s('root_search_info_block').height     ='auto';

                                        }

                                    },40);

                                    setTimeout(function(){

                                        if(isset($d('root_search_info_block'))){

                                            $s('root_search_info_block').opacity    =0;
                                            $s('root_search_info_block').height     ='';

                                        }

                                    },3000);

                                }

                                setTimeout(function(){

                                    page_object.action.profile.action.root_face_block.action.image.reset_item(index);

                                    page_object.action.profile.action.root_face_block.action.image.upload.reset_box_size();

                                },3000);

                            }
                            else{

                                page_object.action.profile.action.root_face_block.action.image.face.list[index]['face_index']       =null;
                                page_object.action.profile.action.root_face_block.action.image.face.list[index]['file_ID']          =data['data']['file_ID'];
                                page_object.action.profile.action.root_face_block.action.image.face.list[index]['image_ID']         =data['data']['image_ID'];
                                page_object.action.profile.action.root_face_block.action.image.face.list[index]['image_data']       =data['data']['image'];
                                page_object.action.profile.action.root_face_block.action.image.face.list[index]['face_len']         =data['data']['face_len'];
                                page_object.action.profile.action.root_face_block.action.image.face.list[index]['face_list']        =data['data']['face_list'];

                                if(isset($d('root_search_face_image_loading_'+index))){

                                    $s('root_search_face_image_loading_'+index).opacity=0;

                                    setTimeout(function(){

                                        $s('root_search_face_image_loading_'+index).display='none';

                                    },300);

                                }

                                if(data['data']['face_len']===0){

                                    if(isset($d('root_search_face_image_error_'+index))){

                                        $d('root_search_face_image_error_'+index).innerHTML ='<span>Лица не найдены</span>';
                                        $s('root_search_face_image_error_'+index).display   ='block';

                                        setTimeout(function(){

                                            if(isset($d('root_search_face_image_error_'+index)))
                                                $s('root_search_face_image_error_'+index).opacity=1;

                                        },40);

                                    }

                                    setTimeout(function(){

                                        page_object.action.profile.action.root_face_block.action.image.reset_item(index);
                                        page_object.action.profile.action.root_face_block.action.image.upload.reset_box_size();

                                    },3000);

                                    return true;

                                }

                                if(isset($d('root_search_face_reset_'+index)))
                                    $s('root_search_face_reset_'+index).display='none';

                                page_object.action.profile.action.root_face_block.action.image.face.show_face_list(index);

                            }

                        }
                    });

                },
                'show_face_list':function(index){

                    if(!isset($d('root_search_face_image_src_object_'+index)))
                        return false;

                    if(isset($d('root_search_face_button'))){

                        if(page_object.action.profile.action.root_face_block.action.image.image_len>0)
                            $d('root_search_face_button').innerHTML=page_object.action.profile.action.root_face_block.action.image.image_len===1?'Выберите лицо':'Выберите лица';
                        else
                            $d('root_search_face_button').innerHTML=page_object.action.profile.action.root_face_block.action.image.image_len===1?'Лицо не найдено':'Лица не найдены';

                        $d('root_search_face_button').onclick=function(){};

                    }

                    if(isset($d('root_search_face_coords_list_'+index)))
                        removeElement($d('root_search_face_coords_list_'+index));

                    let el=addElement({
                        'tag':'div',
                        'id':'root_search_face_coords_list_'+index,
                        'class':'root_search_face_coords_list',
                    });

                    if(isset($d('root_search_face_image_block_container_'+index)))
                        $d('root_search_face_image_block_container_'+index).appendChild(el);

                    let  image_data             =page_object.action.profile.action.root_face_block.action.image.face.list[index]['image_data']
                        ,face_delta_x           =page_object.action.profile.action.root_face_block.action.image.face.face_delta_x
                        ,face_delta_y           =page_object.action.profile.action.root_face_block.action.image.face.face_delta_y
                        ,image_w                =image_data['image_item_ID_list']['middle']['width']
                        ,image_h                =image_data['image_item_ID_list']['middle']['height']
                        ,box_width              =elementSize.width($d('root_search_face_image_block_container_'+index))
                        ,face_list              =page_object.action.profile.action.root_face_block.action.image.face.list[index]['face_list']
                        ,face_len               =page_object.action.profile.action.root_face_block.action.image.face.list[index]['face_len']
                        ,face_index
                        ,face_w
                        ,face_h
                        ,face_x
                        ,face_y
                        ,face_w_new
                        ,face_h_new
                        ,face_x_new
                        ,face_y_new
                        ,image_w_new
                        ,image_h_new
                        ,coords
                        ,face_check_len         =0
                        ,perc                   =parseInt(box_width)/parseInt(image_w);

                    page_object.action.profile.action.root_face_block.action.image.face.face_coords_list=[];

                    for(face_index in face_list){

                        coords              =face_list[face_index]['coords'];

                        face_x              =coords['left'];
                        face_y              =coords['top'];
                        // face_w              =coords['right']-coords['left'];
                        // face_h              =coords['bottom']-coords['top'];
                        face_w              =coords['width'];
                        face_h              =coords['height'];

                        face_y              -=Math.ceil(face_h/3);
                        face_h              +=Math.ceil(face_h/3);

                        face_check_len++;

                        face_w      =Math.ceil(face_w*perc);
                        face_h      =Math.ceil(face_h*perc);

                        face_x      =Math.ceil(face_x*perc);
                        face_y      =Math.ceil(face_y*perc);

                        face_w_new  =face_w*face_delta_x;
                        face_h_new  =face_h*face_delta_y;

                        face_x_new  =face_x-Math.ceil((face_w_new-face_w)/2);
                        face_y_new  =face_y-Math.ceil((face_h_new-face_h)/2);

                        if(face_x_new<0)
                            face_x_new=0;

                        if(face_y_new<0)
                            face_y_new=0;

                        image_w_new     =Math.ceil(image_w*perc);
                        image_h_new     =Math.ceil(image_h*perc);

                        if(face_x_new+face_w_new>image_w_new)
                            face_w_new=image_w_new-face_x_new;

                        if(face_y_new+face_h_new>image_h_new)
                            face_h_new=image_h_new-face_y_new;

                        page_object.action.profile.action.root_face_block.action.image.face.face_coords_list.push({
                            'x':Math.ceil(face_x_new/perc),
                            'y':Math.ceil(face_y_new/perc),
                            'w':Math.ceil(face_w_new/perc),
                            'h':Math.ceil(face_h_new/perc),
                            'face_token':face_list[face_index]['face_token']
                        });

                        el=addElement({
                            'tag':'div',
                            'id':'dialog_face_image_item_'+index+'_'+face_index,
                            'class':'dialog_face_image_item',
                            'style':'width: '+face_w_new+'px; height: '+face_h_new+'px; margin: '+face_y_new+'px 0 0 '+face_x_new+'px;'
                        });

                        el.onclick=page_object.action.profile.action.root_face_block.action.image.face.change_face;

                        $d('root_search_face_coords_list_'+index).appendChild(el);

                    }

                    if(face_check_len===0){

                        if(isset($d('root_search_info_block'))){

                            $d('root_search_info_block').setAttribute('class','root_search_info_block_red');

                            if(face_len===1)
                                $d('root_search_info_block').innerHTML='Слишком маленькое лицо на изображении';
                            else
                                $d('root_search_info_block').innerHTML='Слишком маленькие лица на изображении';

                            setTimeout(function(){

                                if(isset($d('root_search_info_block'))){

                                    $s('root_search_info_block').opacity    =1;
                                    $s('root_search_info_block').height     ='auto';

                                }

                            },40);

                            setTimeout(function(){

                                if(isset($d('root_search_info_block'))){

                                    $s('root_search_info_block').opacity    =0;
                                    $s('root_search_info_block').height     ='';

                                }

                            },3000);

                        }

                        setTimeout(page_object.action.profile.action.root_face_block.action.image.reset,3000);

                    }

                },
                'change_face':function(){

                    let  face_index     =this.id.split('_')[5]
                        ,index          =this.id.split('_')[4]
                        ,is_active      =this.getAttribute('class')==='dialog_face_image_item_active'
                        ,list           =$d('root_search_face_coords_list_'+index).getElementsByTagName('div')
                        ,i;

                    for(i in list)
                        if(isObject(list[i]))
                            list[i].setAttribute('class','dialog_face_image_item');

                    this.setAttribute('class',is_active?'dialog_face_image_item':'dialog_face_image_item_active');

                    page_object.action.profile.action.root_face_block.action.image.face.list[index]['face_index']=is_active?null:parseInt(face_index);

                    if(is_active)
                        page_object.action.profile.action.root_face_block.action.image.face.face_change_len--;
                    else
                        page_object.action.profile.action.root_face_block.action.image.face.face_change_len++;

                    if(isset($d('root_search_face_button'))){

                        if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===0){

                            if(page_object.action.profile.action.root_face_block.action.image.image_len===1)
                                $d('root_search_face_button').innerHTML='Выберите лицо';
                            else
                                $d('root_search_face_button').innerHTML='Выберите лица';

                            $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                            $d('root_search_face_button').onclick=function(){};

                        }
                        else{

                            if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===1)
                                $d('root_search_face_button').innerHTML='Проверить лицо';
                            else if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len>0)
                                $d('root_search_face_button').innerHTML='Проверить лица';

                            $d('root_search_face_button').setAttribute('class','root_search_face_button');

                            $d('root_search_face_button').onclick=page_object.action.profile.action.root_face_block.action.search.init;

                        }

                    }

                    if(isset($d('root_search_face_button'))){

                        let  cost           =page_object.action.profile.data['data']['cost']
                            ,total_cost     =0
                            ,button_inner;

                        if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===0)
                            button_inner='Выберите лица';
                        else if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===1){

                            total_cost      =page_object.action.profile.data['data']['cost']['face_1'];
                            button_inner    ='Проверить лицо за '+total_cost+'₽';

                        }
                        else if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===2){

                            total_cost      =page_object.action.profile.data['data']['cost']['face_2'];
                            button_inner    ='Проверить лицa за '+total_cost+'₽';

                        }
                        else{

                            total_cost      =page_object.action.profile.data['data']['cost']['face_3'];
                            button_inner    ='Проверить лица за '+(total_cost)+'₽';

                        }

                        let  user_data=page_object.action.profile.data['user'];

                        if(parseFloat(user_data['balance'])<total_cost){

                            $d('root_search_face_button').innerHTML     ='Недостаточный баланс';
                            $d('root_search_face_button').onclick       =function(){};

                            $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');


                        }
                        else{

                            $d('root_search_face_button').innerHTML     =button_inner;
                            $d('root_search_face_button').onclick       =page_object.action.profile.action.root_face_block.action.search.init;

                            $d('root_search_face_button').setAttribute('class','root_search_face_button');

                        }

                    }

                },
                'change_image':function(){

                }
            }
        },
        'search':{
            'type':1,
            'dialog_index':null,
            'init':function(){

                switch(page_object.action.profile.action.root_face_block.action.search.type){

                    case 0:
                        return page_object.action.profile.action.root_face_block.action.search.old.init();

                    case 1:
                        return page_object.action.profile.action.root_face_block.action.search.new.init();

                    default:{

                        alert('error');

                        break;
                        
                    }

                }

            },
            'old':{
                'init':function(index){

                    let  cost           =500
                        ,total_cost     =0;

                    if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===0)
                        total_cost=0;
                    else if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===1)
                        total_cost=cost;
                    else
                        total_cost=2*cost;

                    let user_data=page_object.action.profile.data['user'];
    
                    if(parseFloat(user_data['balance'])<total_cost){
    
                        alert('Недостаточно средств для совершения действия');
    
                        return false;
    
                    }
    
                    let  post=''
                        ,file_ID        =page_object.action.profile.action.root_face_block.action.image.face.list[index]['file_ID']
                        ,image_ID       =page_object.action.profile.action.root_face_block.action.image.face.list[index]['image_ID']
                        ,face_list      =page_object.action.profile.action.root_face_block.action.image.face.list[index]['face_coords_list']
                        ,face_index     =page_object.action.profile.action.root_face_block.action.image.face.list[index]['face_index']
                        ,face_coords    =face_list[face_index]
                        ,inner          =''
                        ,face_token     =face_coords['face_token'];
    
                    if(isset($d('root_search_face_button'))){
    
                        $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                        inner+='<div id="root_search_face_button_label">Ищем совпадения...</div>';
                        inner+='<div id="root_search_face_button_progress"></div>';

                        $d('root_search_face_button').innerHTML     =inner;
                        $d('root_search_face_button').onclick       =function(){};
    
                    }
    
                    post+='file_ID='+uniEncode(file_ID);
                    post+='&image_ID='+uniEncode(image_ID);
                    post+='&face_coords='+uniEncode(jsonEncode(face_coords));
                    post+='&face_token='+uniEncode(face_token);
    
                    send({
                        'scriptPath':'/api/json/search_face',
                        'postData':post,
                        'onComplete':function(j,worktime){
    
                            let  data
                                ,dataTemp;
    
                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);
    
                            if(isset(data['error'])){
    
                                if(isset($d('root_search_info_block'))){
    
                                    $d('root_search_info_block').setAttribute('class','root_search_info_block_red');
    
                                    $d('root_search_info_block').innerHTML=data['error']['info'];
    
                                    setTimeout(function(){
    
                                        if(isset($d('root_search_info_block'))){

                                            $s('root_search_info_block').opacity    =1;
                                            $s('root_search_info_block').height     ='auto';

                                        }

                                    },40);

                                    setTimeout(function(){

                                        if(isset($d('root_search_info_block'))){

                                            $s('root_search_info_block').opacity    =0;
                                            $s('root_search_info_block').height     ='';

                                        }

                                    },3000);
    
                                }
    
                                setTimeout(page_object.action.profile.action.root_face_block.action.image.reset,3000);
    
                            }
                            else{

                                if(isset(data['balance'])){

                                    if(isset($d('user_balance'))){

                                        $d('user_balance').innerHTML=empty(data['balance'])?0:data['balance'];

                                        if(data['balance']<page_object.action.profile.data['user']['price'])
                                            $s('user_balance').color='#ff0000';

                                    }

                                    page_object.action.profile.data['user']['balance']=data['balance'];

                                }
    
                                page_object.action.profile.action.root_face_block.action.search.old.show_results.index      =0;
                                page_object.action.profile.action.root_face_block.action.search.old.show_results.data       =data;
    
                                page_object.action.profile.action.root_face_block.action.search.old.show_results.init();
    
                            }
    
                        }
                    });
    
                },
                'show_results':{
                    'data':null,
                    'index':0,
                    'init':function(){

                        page_object.action.profile.action.root_face_block.un_show();

                        page_object.action.profile.action.root_face_block.action.search.old.show_results.create();

                        return true;
    
                    },
                    'get_result_image_list':function(index){

                        let  list                   =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list']
                            ,face_data              =list[index]
                            ,image_result_link
                            ,image_list             =face_data['image_list']
                            ,inner                  ='';

                        inner+='<div id="root_face_result_profile_image_list_block">';

                            inner+='<div id="root_face_result_profile_image_scroll_up" class="root_face_result_profile_image_scroll_up"></div>';

                            inner+='<div id="root_face_result_profile_image_list">';

                        for(index=0;index<image_list.length;index++){

                            image_result_link='/'+image_list[index]['image_dir']+'/'+image_list[index]['image_item_ID_list']['preview']['ID'];

                            inner+='<div id="root_face_result_profile_image_item_'+index+'" class="root_face_result_profile_image_item'+(image_list[index]['image_ID']===face_data['image']['image_ID']?'_active':'')+'" style="background: url('+image_result_link+') 50% 50% no-repeat; background-size: cover;"></div>';

                        }

                            inner+='</div>';

                            inner+='<div id="root_face_result_profile_image_scroll_down" class="root_face_result_profile_image_scroll_down"></div>';

                        inner+='</div>';

                        return inner;

                    },
                    'get_result_info':function(index){

                        let  list                   =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list']
                            ,face_data              =list[index]
                            ,image_result_link
                            ,image_list             =face_data['image_list']
                            ,isset_profile_info     =false
                            ,inner                  =''
                            ,video_list
                            ,video_index
                            ,source_link_index;

                        inner+='<div id="root_face_result_profile_info_block" class="root_face_result_profile_info_block">';

                        if(!empty(face_data['name'])){

                            inner+='<div class="root_face_result_profile_info_row">';
                            inner+='<div class="root_face_result_profile_info_label">Имя:</div>';
                            inner+='<div class="root_face_result_profile_info_data">'+face_data['name']+'</div>';
                            inner+='</div>';

                            isset_profile_info=true;

                        }

                        if(!empty(face_data['age'])){

                            inner+='<div class="root_face_result_profile_info_row">';
                            inner+='<div class="root_face_result_profile_info_label">Возраст:</div>';
                            inner+='<div class="root_face_result_profile_info_data">'+face_data['age']+'</div>';
                            inner+='</div>';

                            isset_profile_info=true;

                        }

                        if(!empty(face_data['city'])){

                            inner+='<div class="root_face_result_profile_info_row">';
                            inner+='<div class="root_face_result_profile_info_label">Город:</div>';
                            inner+='<div class="root_face_result_profile_info_data">'+face_data['city']+'</div>';
                            inner+='</div>';

                            isset_profile_info=true;

                        }

                        if(
                            !empty(face_data['info'])
                            ||!empty(face_data['source_account_link'])
                        ){

                            inner+='<div class="root_face_result_profile_info_row" style="margin-top: 10px;">';
                            inner+='<i>Дополнительная информация:</i>';
                            inner+='</div>';

                        }

                        if(!empty(face_data['info'])){

                            let  info_list=face_data['info']
                                ,info_index;

                            for(info_index in info_list){

                                inner+='<div class="root_face_result_profile_info_row">';
                                inner+='<div class="root_face_result_profile_info_label">'+stripSlashes(info_list[info_index]['label'])+':</div>';
                                inner+='<div class="root_face_result_profile_info_data">'+stripSlashes(info_list[info_index]['value'])+'</div>';
                                inner+='</div>';

                            }

                            isset_profile_info=true;

                        }

                        if(!empty(face_data['source_account_link'])){

                            inner+='<div class="root_face_result_profile_info_row">';
                            inner+='<div class="root_face_result_profile_info_label">Источник'+(getObjectLength(face_data['source_account_link_list'])>1?'и':'')+':</div>';
                            inner+='<div class="root_face_result_profile_info_data">';

                            for(source_link_index in face_data['source_account_link_list']){

                                inner+='<div>';
                                    inner+='<a href="'+face_data['source_account_link_list'][source_link_index]+'" target="_blank" style="float: ">'+stripSlashes(face_data['source_account_link_list'][source_link_index])+'</a>';
                                inner+='</div>';

                            }

                            // inner+='<a href="'+face_data['source_account_link']+'" target="_blank">'+stripSlashes(face_data['source_account_link'])+'</a>';

                            inner+='</div>';
                            inner+='</div>';

                            isset_profile_info=true;

                        }

                        if(!empty(face_data['video_list']))
                            if(face_data['video_list'].length>0){

                                inner+='<div class="root_face_result_profile_info_row">';
                                inner+='<div class="root_face_result_profile_info_label">Видео:</div>';
                                inner+='<div class="root_face_result_profile_info_data">';

                                video_list=face_data['video_list'];

                                for(video_index in video_list)
                                    inner+='<a href="/'+video_list[video_index]['video_path']+'" target="_blank">Видео #'+(parseInt(video_index)+1)+'</a>&nbsp&nbsp&nbsp';

                                inner+='</div>';
                                inner+='</div>';

                                isset_profile_info=true;

                            }

                        if(!isset_profile_info){

                            inner+='<div class="root_face_result_profile_info_row">';
                            inner+='<div class="root_face_result_profile_info_none">Данных нет</div>';
                            inner+='</div>';

                        }

                                inner+='</div>';

                        return inner;

                    },
                    'get_source_image_block':function(chosen_index){

                        trace('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');

                        var  list                   =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list']
                            // ,chosen_index           =page_object.action.profile.action.root_face_block.action.search.old.show_results.index
                            ,image_list             =page_object.action.profile.action.root_face_block.action.image.face.list
                            ,image_data
                            ,i
                            ,image_ID
                            ,face_data              =list[chosen_index]
                            ,image_source_link;

                        for(image_ID in face_data['source_image_list']){

                            trace('PERCENT MODE: '+parseInt(face_data['source_image_list'][image_ID])+' === '+parseInt(face_data['percent_mode']));

                            if(parseInt(face_data['source_image_list'][image_ID])===parseInt(face_data['percent_mode'])){

                                trace('image ID: '+image_ID);

                                for(i=0;i<image_list.length;i++)
                                    if(parseInt(image_list[i]['image_ID'])===parseInt(image_ID)){

                                        image_data          =image_list[i]['image_data'];
                                        image_source_link   ='/'+image_data['image_dir']+'/'+image_data['image_item_ID_list']['large']['ID'];

                                        break;

                                    }

                                trace('Image source link: '+image_source_link);

                                if(empty(image_data))
                                    return false;

                                if(isset($d('root_face_result_source_src_image'))){

                                    if($d('root_face_result_source_src_image').getAttribute('src')!==image_source_link){

                                        $s('root_face_result_source_src').opacity=0;

                                        setTimeout(function(){

                                            trace(image_source_link);

                                            $d('root_face_result_source_src').innerHTML='<img id="root_face_result_source_src_image" src="'+image_source_link+'" width="" height="100%">';

                                            $d('root_face_result_source_src_image').onload=function(){

                                                $s('root_face_result_source_src').opacity=1;

                                                let  img_source_width           =$d('root_face_result_source_src_image').width
                                                    ,img_source_height          =$d('root_face_result_source_src_image').height
                                                    ,block_source_width         =elementSize.width($d('root_face_result_source_src'))
                                                    ,block_source_height        =elementSize.height($d('root_face_result_source_src'))
                                                    ,img_source_perc            =img_source_width/img_source_height
                                                    ,img_source_in_width        =block_source_width*img_source_perc
                                                    ,img_source_in_height       =block_source_width/img_source_perc;

                                                if(img_source_in_width>img_source_in_height){

                                                    if(img_source_in_width>block_source_width){

                                                        img_source_in_width     =block_source_width;
                                                        img_source_in_height    =img_source_in_width/img_source_perc;

                                                        if(img_source_in_height<block_source_height){

                                                            $d('root_face_result_source_src_image').setAttribute('width','100%');
                                                            $d('root_face_result_source_src_image').setAttribute('height','');

                                                        }
                                                        else{

                                                            $d('root_face_result_source_src_image').setAttribute('width','');
                                                            $d('root_face_result_source_src_image').setAttribute('height','100%');

                                                        }

                                                    }
                                                    else{

                                                        img_source_in_height    =block_source_height;
                                                        img_source_in_width     =img_source_in_height*img_source_perc;

                                                        if(img_source_in_height<block_source_height){

                                                            $d('root_face_result_source_src_image').setAttribute('width','100%');
                                                            $d('root_face_result_source_src_image').setAttribute('height','');

                                                        }
                                                        else{

                                                            $d('root_face_result_source_src_image').setAttribute('width','');
                                                            $d('root_face_result_source_src_image').setAttribute('height','100%');

                                                        }

                                                    }


                                                }
                                                else{

                                                    if(img_source_in_height>block_source_height){

                                                        img_source_in_height    =block_source_height;
                                                        img_source_in_width     =img_source_in_height*img_source_perc;

                                                        if(img_source_in_height<block_source_height){

                                                            $d('root_face_result_source_src_image').setAttribute('width','100%');
                                                            $d('root_face_result_source_src_image').setAttribute('height','');

                                                        }
                                                        else{

                                                            $d('root_face_result_source_src_image').setAttribute('width','');
                                                            $d('root_face_result_source_src_image').setAttribute('height','100%');

                                                        }

                                                    }
                                                    else{

                                                        img_source_in_width     =block_source_width;
                                                        img_source_in_height    =img_source_in_width/img_source_perc;

                                                        if(img_source_in_height<block_source_height){

                                                            $d('root_face_result_source_src_image').setAttribute('width','100%');
                                                            $d('root_face_result_source_src_image').setAttribute('height','');

                                                        }
                                                        else{

                                                            $d('root_face_result_source_src_image').setAttribute('width','');
                                                            $d('root_face_result_source_src_image').setAttribute('height','100%');

                                                        }

                                                    }

                                                }

                                            };

                                        },340);

                                    }

                                }

                                return true;

                            }

                        }

                        return true;

                    },
                    'get_result_block':function(index,is_start){
    
                        let  list                   =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list']
                            ,face_data              =list[index]
                            ,image_result_link
                            ,inner                  ='';

                        if(face_data['percent']<=100)
                            face_data['percent']*=100;

                        image_result_link='/'+face_data['image']['image_dir']+'/'+face_data['image']['image_item_ID_list']['large']['ID'];

                        inner+='<div id="root_face_result_profile_block">';
    
                            inner+='<div id="root_face_result_profile_total_img_block" class="root_face_result_profile_total_img_block">';
    
                                inner+='<div class="root_face_result_title">'+(index===0&&face_data['percent']>=9000?(OS.isMobile?'Совпадение':'Наиболее вероятное совпадение'):(OS.isMobile?'Похожее лицо':'Девушка с похожим лицом'))+' ('+stripSlashes((face_data['percent']/100).toFixed(2))+'%)</div>';
    
                                inner+='<div id="root_face_result_profile_total_img_container" class="root_face_result_profile_total_img_container">';
                                    inner+='<div id="root_face_result_profile_total_img_src_border" class="root_face_result_profile_total_img_src_border">';
                                        inner+='<img id="root_face_result_profile_total_img_src" class="root_face_result_profile_total_img_src" src="'+image_result_link+'" />';
                                        inner+='<div id="root_face_result_profile_total_img_source_type"></div>';
                                    inner+='</div>';
                                inner+='</div>';

                                if(!OS.isMobile)
                                    inner+=page_object.action.profile.action.root_face_block.action.search.old.show_results.get_result_info(index);

                            inner+='</div>';

                        inner+='</div>';

                        if(!OS.isMobile)
                            inner+=page_object.action.profile.action.root_face_block.action.search.old.show_results.get_result_image_list(index);
    
                        return inner;
    
                    },
                    'profile':{
                        'init':function(){
    
                            let  list                   =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list']
                                ,chosen_index           =page_object.action.profile.action.root_face_block.action.search.old.show_results.index
                                ,index;
    
                            for(index=0;index<list.length;index++)
                                // if(list[index]['face_ID']!==list[chosen_index]['face_ID'])
                                $d('root_face_result_other_item_container_'+index).onclick=page_object.action.profile.action.root_face_block.action.search.old.show_results.profile.load;
                                // $d('root_face_result_other_item_'+index).onclick=page_object.action.profile.action.root_face_block.action.search.old.show_results.profile.load;
                        },
                        'load':function(){
    
                            var  data                   =page_object.action.profile.action.root_face_block.action.search.old.show_results.data
                                ,list                   =data['face_list']
                                ,search_token           =data['search_token']
                                ,chosen_index           =parseInt(this.id.split('_')[6])
                                ,face_data              =list[chosen_index]
                                ,index
                                ,face_ID                =face_data['face_ID']
                                ,post                   ='';
    
                            post+='search_token='+uniEncode(search_token);
                            post+='&face_ID='+uniEncode(face_ID);
                            post+='&is_test=1';
    
                            page_object.action.profile.action.root_face_block.action.search.old.show_results.index=chosen_index;
    
                            for(index=0;index<list.length;index++)
                                if(isset($d('root_face_result_other_item_'+index)))
                                    $d('root_face_result_other_item_'+index).setAttribute('class','root_face_result_other_item'+(parseInt(index)===parseInt(chosen_index)?'_active':''));
    
                            if(isset($d('root_face_result_container')))
                                $d('root_face_result_container').scrollTop=0;
    
                            if(isset($d('root_face_result_third_block')))
                                $s('root_face_result_third_block').opacity=0;
    
                            if(isset($d('root_face_result_third_nun')))
                                $s('root_face_result_third_nun').opacity=0;
    
                            if(isset($d('root_face_result_third_loading')))
                                $s('root_face_result_third_loading').display='block';

                            if(OS.isMobile){

                                if(isset($d('root_face_result_profile_info_block'))){

                                    $s('root_face_result_profile_info_block').opacity   =0;
                                    $s('root_face_result_profile_info_block').height    =0;

                                }

                                if(isset($d('root_face_result_profile_image_list_block'))){

                                    $s('root_face_result_profile_image_list_block').opacity     =0;
                                    $s('root_face_result_profile_image_list_block').height      =0;

                                }

                            }
    
                            setTimeout(function(){
    
                                if(isset($d('root_face_result_third_loading')))
                                    $s('root_face_result_third_loading').opacity=1;
    
                                if(isset($d('root_face_result_third_block')))
                                    $s('root_face_result_third_block').display='none';
    
                                if(isset($d('root_face_result_third_nun')))
                                    $s('root_face_result_third_nun').display='none';

                                if(OS.isMobile){

                                    if(isset($d('root_face_result_profile_info_block')))
                                        $s('root_face_result_profile_info_block').display='none';

                                    if(isset($d('root_face_result_profile_image_list_block')))
                                        $s('root_face_result_profile_image_list_block').display='none';

                                }
    
                                setTimeout(function(){
    
                                    if(isset($d('root_face_result_third_block')))
                                        $d('root_face_result_third_block').innerHTML='';

                                    if(OS.isMobile){

                                        if(isset($d('root_face_result_profile_info_block')))
                                            $d('root_face_result_profile_info_block').innerHTML='';

                                        if(isset($d('root_face_result_profile_image_list_block')))
                                            $d('root_face_result_profile_image_list_block').innerHTML='';

                                    }
    
                                    send({
                                        'scriptPath':'/api/json/get_profile_face_image_list',
                                        'postData':post,
                                        'onComplete':function(j,worktime){
    
                                            let  data
                                                ,dataTemp;
    
                                            dataTemp=j.responseText;
                                            trace(dataTemp);
                                            data=jsonDecode(dataTemp);
                                            trace(data);
                                            trace_worktime(worktime,data);
    
                                            if(isset(data['error'])){
    
                                            }
                                            else{
    
                                                page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list'][chosen_index]['image_list']  =data['data']['image_list'];
                                                page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list'][chosen_index]['video_list']  =data['data']['video_list'];

                                                page_object.action.profile.action.root_face_block.action.search.old.show_results.get_source_image_block(chosen_index);

                                                let inner=page_object.action.profile.action.root_face_block.action.search.old.show_results.get_result_block(chosen_index,true);

                                                if(isset($d('root_face_result_third_block')))
                                                    $d('root_face_result_third_block').innerHTML=inner;
    
                                                if(isset($d('root_face_result_third_block')))
                                                    $s('root_face_result_third_block').display='block';

                                                if(OS.isMobile){

                                                    if(isset($d('root_face_result_profile_info_block'))){

                                                        $d('root_face_result_profile_info_block').innerHTML     =page_object.action.profile.action.root_face_block.action.search.old.show_results.get_result_info(chosen_index);
                                                        $s('root_face_result_profile_info_block').display       ='block';

                                                    }

                                                    if(isset($d('root_face_result_profile_image_list_block'))){

                                                        $d('root_face_result_profile_image_list_block').innerHTML   =page_object.action.profile.action.root_face_block.action.search.old.show_results.get_result_image_list(chosen_index);
                                                        $s('root_face_result_profile_image_list_block').display     ='block';

                                                    }

                                                }
    
                                                page_object.action.profile.action.root_face_block.action.search.old.show_results.action.init();
    
                                                let  img=new Image();
    
                                                img.src     ='/'+page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list'][chosen_index]['image']['image_dir']+'/'+page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list'][chosen_index]['image']['image_item_ID_list']['large']['ID'];
                                                img.onload  =function(){

                                                    let  face_data      =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list'][chosen_index]
                                                        ,image_list     =face_data['image_list']
                                                        ,image_index;

                                                    for(image_index=0;image_index<image_list.length;image_index++)
                                                        if(face_data['image']['image_ID']===image_list[image_index]['image_ID']){

                                                            if(isset($d('root_face_result_profile_image_item_'+image_index)))
                                                                $d('root_face_result_profile_image_item_'+image_index).click();

                                                            break;

                                                        }
    
                                                    setTimeout(function(){
    
                                                        if(isset($d('root_face_result_third_block')))
                                                            $s('root_face_result_third_block').opacity=1;

                                                        if(isset($d('root_face_result_profile_info_block'))){

                                                            $s('root_face_result_profile_info_block').height    ='auto';
                                                            $s('root_face_result_profile_info_block').opacity   =1;

                                                        }

                                                        if(isset($d('root_face_result_profile_image_list_block'))){

                                                            $s('root_face_result_profile_image_list_block').height      ='auto';
                                                            $s('root_face_result_profile_image_list_block').opacity     =1;

                                                        }
    
                                                        if(isset($d('root_face_result_third_loading')))
                                                            $s('root_face_result_third_loading').opacity=0;
    
                                                    },300);
    
                                                    setTimeout(function(){
    
                                                        if(isset($d('root_face_result_third_loading')))
                                                            $s('root_face_result_third_loading').display='none';
    
                                                    },340);
    
    
                                                };
    
                                            }
    
                                        }
                                    });
    
                                },40);
    
                            },300);
    
                        }
                    },
                    'create':function(){

                        if(
                              !isset($d('root_search_face_image_src_object_0'))
                            &&!isset($d('root_search_face_image_src_object_1'))
                            &&!isset($d('root_search_face_image_src_object_2'))
                        ){

                            trace('empty root_search_face_image_src_object list');

                            return false;

                        }

                        let  list                   =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list']
                            ,chosen_index           =page_object.action.profile.action.root_face_block.action.search.old.show_results.index
                            ,image_list             =page_object.action.profile.action.root_face_block.action.image.face.list
                            ,image_data             =image_list[0]['image_data']
                            ,image_ID
                            ,index
                            ,source_link_index
                            ,i
                            ,face_data              =list[0]
                            ,image_source_link      ='/'+image_data['image_dir']+'/'+image_data['image_item_ID_list']['large']['ID']
                            ,image_result_link
                            ,el
                            ,isset_profile_info     =false
                            ,video_list
                            ,video_index
                            ,inner                  ='';

                        if(list.length>0){

                            if(face_data['percent']<=100)
                                face_data['percent']*=100;

                            if(face_data['percent']>=9000){

                                for(image_ID in face_data['source_image_list'])
                                    if(parseInt(face_data['source_image_list'][image_ID])===parseInt(face_data['percent_mode'])){

                                        for(i=0;i<image_list.length;i++)
                                            if(parseInt(image_list[i]['image_ID'])===parseInt(image_ID)){

                                                image_data          =image_list[i]['image_data'];
                                                image_source_link   ='/'+image_data['image_dir']+'/'+image_data['image_item_ID_list']['large']['ID'];

                                                break;

                                            }

                                    }

                            }
                            else{

                                let  face_token_list    =page_object.action.profile.action.root_face_block.action.image.face.face_token_list
                                    ,image_ID           =face_token_list[0]['image_ID']
                                    ,index;

                                for(index in image_list)
                                    if(parseInt(image_list[index]['image_ID'])===parseInt(image_ID)){

                                        image_data          =image_list[index]['image_data'];
                                        image_source_link   ='/'+image_data['image_dir']+'/'+image_data['image_item_ID_list']['large']['ID'];

                                        break;

                                    }

                            }

                        }
                        else{

                            let  face_token_list    =page_object.action.profile.action.root_face_block.action.image.face.face_token_list
                                ,image_ID           =face_token_list[0]['image_ID']
                                ,index;

                            for(index in image_list)
                                if(parseInt(image_list[index]['image_ID'])===parseInt(image_ID)){

                                    image_data          =image_list[index]['image_data'];
                                    image_source_link   ='/'+image_data['image_dir']+'/'+image_data['image_item_ID_list']['large']['ID'];

                                    break;

                                }

                        }

                        inner+='<div id="root_face_result_close_button" class="dialog_close_button"'+(OS.isMobile?' style="margin: -11px 0 0 11px; padding: 0;"':' style="margin: -11px 0 0 11px; padding: 0;"')+'></div>';
    
                            inner+='<div id="root_face_result_container">';
    
                                inner+='<div id="root_face_result_first_row_block">';
    
                                    inner+='<div id="root_face_result_first_block">';
    
                                        inner+='<div class="root_face_result_title">Исходное изображение</div>';
    
                                        inner+='<div id="root_face_result_source_src" class="root_face_result_source_src">';
                                            inner+='<img id="root_face_result_source_src_image" src="'+image_source_link+'" />';
                                        inner+='</div>';
    
                                    inner+='</div>';
    
                                    inner+='<div id="root_face_result_second_block"></div>';
    
                                    inner+='<div id="root_face_result_third_loading">Загрузка...</div>';
                                    inner+='<div id="root_face_result_third_nun">';
                                        inner+='<div id="root_face_result_third_nun_text">';
                                            inner+='<span class="span_red">Поздравляем! Ничего не найдено!</span>';
                                            inner+='<span class="span_blue">На всякий случай можете посмотреть <br />похожих девушек в списке ниже</span>';
                                        inner+='</div>';
                                    inner+='</div>';
    
                                    inner+='<div id="root_face_result_third_block">';

                                    if(list.length>0)
                                        inner+=page_object.action.profile.action.root_face_block.action.search.old.show_results.get_result_block(chosen_index,true);

                                    inner+='</div>';

                                    if(OS.isMobile)
                                        if(list.length>0)
                                            inner+=page_object.action.profile.action.root_face_block.action.search.old.show_results.get_result_info(chosen_index);

                                    if(OS.isMobile)
                                        if(list.length>0)
                                            inner+=page_object.action.profile.action.root_face_block.action.search.old.show_results.get_result_image_list(chosen_index);

                                inner+='</div>';

                        if(list.length>0){

                            inner+='<div id="root_face_result_other_row_block">';

                                inner+='<div class="root_face_result_title">Девушки с похожими лицами</div>';

                                inner+='<div id="root_face_result_other_list">';

                            if(list.length>0)
                                for(index=0;index<list.length;index++){

                                    isset_profile_info  =false;
                                    face_data           =list[index];
                                    image_result_link   ='/'+face_data['image']['image_dir']+'/'+face_data['image']['image_item_ID_list']['preview']['ID'];

                                    if(list[0]['percent']<=100)
                                        list[0]['percent']*=100;

                                    inner+='<div id="root_face_result_other_item_'+index+'" class="root_face_result_other_item'+((list[0]['percent']>=9000&&face_data['face_ID']===list[chosen_index]['face_ID'])?'_active':'')+'">';

                                    inner+='<div id="root_face_result_other_item_container_'+index+'" class="root_face_result_other_item_container">';

                                    inner+='<div id="root_face_result_other_item_img_block_'+index+'" class="root_face_result_other_item_img_block" style="background: url('+image_result_link+') 50% 50% no-repeat; background-size: cover;">';

                                    inner+='<div class="root_face_result_other_item_img_percent">'+stripSlashes((face_data['percent']/100).toFixed(2))+'%</div>';
                                    inner+='<div class="root_face_result_other_item_img_len">'+stripSlashes(face_data['image_len'])+'</div>';

                                    if(face_data['face_ID_list'].length>1)
                                        inner+='<div class="root_face_result_other_item_profile_len" title="Найдено несколько копий данной анкеты на одном или нескольких сайтах"></div>';

                                    inner+='</div>';

                                    inner+='<div class="root_face_result_other_item_profile_info_block">';

                                        // inner+='<div class="root_face_result_profile_info_row">';
                                        //     inner+='<div class="root_face_result_profile_title_label">Анкетные данные:</div>';
                                        // inner+='</div>';

                                    inner+='<table width="100%" cellspacing="0" cellpadding="0"><tr><td valign="middle"'+(OS.isMobile?'':' height="150"')+'>';

                                    if(!empty(face_data['name'])){

                                        inner+='<div class="root_face_result_profile_info_row">';
                                        inner+='<div class="root_face_result_profile_info_other_label">Имя:</div>';
                                        inner+='<div class="root_face_result_profile_info_data">'+face_data['name']+'</div>';
                                        inner+='</div>';

                                        isset_profile_info=true;

                                    }

                                    if(!empty(face_data['age'])){

                                        inner+='<div class="root_face_result_profile_info_row">';
                                        inner+='<div class="root_face_result_profile_info_other_label">Возраст:</div>';
                                        inner+='<div class="root_face_result_profile_info_data">'+face_data['age']+'</div>';
                                        inner+='</div>';

                                        isset_profile_info=true;

                                    }

                                    if(!empty(face_data['city'])){

                                        inner+='<div class="root_face_result_profile_info_row">';
                                        inner+='<div class="root_face_result_profile_info_other_label">Город:</div>';
                                        inner+='<div class="root_face_result_profile_info_data">'+face_data['city']+'</div>';
                                        inner+='</div>';

                                        isset_profile_info=true;

                                    }

                                    if(!empty(face_data['source_account_link'])){

                                        inner+='<div class="root_face_result_profile_info_row">';
                                        inner+='<div class="root_face_result_profile_info_other_label">Источник'+(getObjectLength(face_data['source_account_link_list'])>1?'и':'')+':</div>';
                                        inner+='<div class="root_face_result_profile_info_data">';

                                        // if(page_object.action.profile.action.root_face_block.is_test){
                                        //
                                        //     for(source_link_index in face_data['source_account_link_list'])
                                        //         inner+='<a href="'+face_data['source_account_link_list'][source_link_index]+'" target="_blank">'+stripSlashes(face_data['source_account_link_list'][source_link_index].substr(0,10)+'...')+'</a>';
                                        //
                                        // }
                                        // else
                                        inner+='<a href="'+face_data['source_account_link']+'" target="_blank">'+stripSlashes(face_data['source_account_link'].substr(0,10)+'...')+'</a>';

                                        // inner+='<a href="'+face_data['source_account_link']+'" target="_blank">'+(OS.isMobile?stripSlashes((get_host_name_from_link(face_data['source_account_link'])===null?'Link':get_host_name_from_link(face_data['source_account_link']))):stripSlashes(face_data['source_account_link']))+'</a>';

                                        inner+='</div>';
                                        inner+='</div>';

                                        isset_profile_info=true;

                                    }

                                    if(!empty(face_data['video_list']))
                                        if(face_data['video_list'].length>0){

                                            inner+='<div class="root_face_result_profile_info_row">';
                                            inner+='<div class="root_face_result_profile_info_label">Видео:</div>';
                                            inner+='<div class="root_face_result_profile_info_data">';

                                            video_list=face_data['video_list'];

                                            for(video_index in video_list)
                                                inner+='<a href="/'+video_list[video_index]['video_path']+'" target="_blank">Видео #'+(parseInt(video_index)+1)+'</a>&nbsp&nbsp&nbsp';

                                            inner+='</div>';
                                            inner+='</div>';

                                            isset_profile_info=true;

                                        }

                                    if(!isset_profile_info){

                                        inner+='<div class="root_face_result_profile_info_row">';
                                            inner+='<div class="root_face_result_profile_info_none">Данных нет</div>';
                                        inner+='</div>';

                                    }

                                    inner+='</td></tr></table>';

                                    inner+='</div>';

                                    if(!empty(face_data['info'])&&!OS.isMobile){

                                        let  info_list=face_data['info']
                                            ,info_index;

                                        inner+='<div class="root_face_result_other_item_profile_info_block">';

                                        inner+='<div class="root_face_result_profile_info_row" style="margin-top: 10px;">';
                                        inner+='<i>Дополнительная информация:</i>';
                                        inner+='</div>';

                                        for(info_index in info_list){

                                            inner+='<div class="root_face_result_profile_info_second_row">';
                                            inner+='<div class="root_face_result_profile_info_label">'+stripSlashes(info_list[info_index]['label'])+':</div>';
                                            inner+='<div class="root_face_result_profile_info_data">'+stripSlashes(info_list[info_index]['value'])+'</div>';
                                            inner+='</div>';

                                        }

                                        inner+='</div>';

                                    }

                                    inner+='</div>';

                                    inner+='<div class="root_face_result_other_item_profile_controls_block">';

                                        inner+='<div id="root_face_result_other_item_profile_show_'+index+'" class="root_face_result_other_item_profile_show"><span>Смотреть отчет</span></div>';
                                        inner+='<div id="root_face_result_other_item_profile_download_'+index+'" class="root_face_result_other_item_profile_download"><span>Скачать в PDF</span></div>';

                                    inner+='</div>';

                                    inner+='</div>';

                                }

                                inner+='</div>';

                            inner+='</div>';

                        }
    
                        inner+='</div>';
    
                        el=addElement({
                            'tag':'div',
                            'id':'root_face_result_block',
                            'inner':inner,
                            'style':'opacity: 0'
                        });
    
                        $d('root_content').appendChild(el);

                        page_object.action.profile.action.root_face_block.action.search.old.show_results.action.init();

                        let el_img=new Image();
    
                        el_img.src      =image_source_link;
                        el_img.onload   =page_object.action.profile.action.root_face_block.action.search.old.show_results.show;

                    },
                    'action':{
                        'init':function(){
    
                            if(isset($d('root_face_result_profile_image_scroll_up'))){

                                let  list                   =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list']
                                    ,index                  =page_object.action.profile.action.root_face_block.action.search.old.show_results.index
                                    ,face_data              =list[index]
                                    ,image_list             =list[index]['image_list']
                                    ,scroll_top;

                                for(index=0;index<image_list.length;index++)
                                    if(image_list[index]['image_ID']===face_data['image']['image_ID']){

                                        scroll_top=index*120;

                                        $d('root_face_result_profile_image_list').scrollTop=scroll_top;

                                        page_object.action.profile.action.root_face_block.action.search.old.show_results.scroll.buttons();

                                        break;

                                    }
    
                                $d('root_face_result_profile_image_scroll_up').onclick          =page_object.action.profile.action.root_face_block.action.search.old.show_results.scroll.up;
                                $d('root_face_result_profile_image_scroll_down').onclick        =page_object.action.profile.action.root_face_block.action.search.old.show_results.scroll.down;
    
                                page_object.action.profile.action.root_face_block.action.search.old.show_results.scroll.buttons();
                                page_object.action.profile.action.root_face_block.action.search.old.show_results.profile.init();
                                page_object.action.profile.action.root_face_block.action.search.old.show_results.image_list.init();
                                page_object.action.profile.action.root_face_block.action.search.old.show_results.pdf.init();
    
                            }
    
                        }
                    },
                    'pdf':{
                        'init':function(){
    
                            let  face_list      =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list']
                                ,index;
    
                            for(index=0;index<face_list.length;index++){
    
                                if(isset($d('root_face_result_other_item_profile_download_'+index)))
                                    $d('root_face_result_other_item_profile_download_'+index).onclick=page_object.action.profile.action.root_face_block.action.search.old.show_results.pdf.create;
    
                                if(isset($d('root_face_result_other_item_profile_show_'+index)))
                                    $d('root_face_result_other_item_profile_show_'+index).onclick=page_object.action.profile.action.root_face_block.action.search.old.show_results.pdf.create;
    
                            }
    
                        },
                        'create':function(){
    
                            var  data                   =page_object.action.profile.action.root_face_block.action.search.old.show_results.data
                                ,face_list              =data['face_list']
                                ,search_token           =data['search_token']
                                ,key_list               =this.id.split('_')
                                ,key                    =key_list[6]
                                ,index                  =parseInt(key_list[7])
                                ,face_data              =face_list[index]
                                ,post                   ='';
    
                            post+='face_token='+uniEncode(face_data['token']);
                            post+='&face_search_multi_token='+uniEncode(search_token);
    
                            if(isset($d('root_face_result_other_item_profile_'+key+'_'+index))){
    
                                $d('root_face_result_other_item_profile_'+key+'_'+index).setAttribute('class','root_face_result_other_item_profile_'+key+'_disable');
    
                                $d('root_face_result_other_item_profile_'+key+'_'+index).onclick       =function(){};
                                $d('root_face_result_other_item_profile_'+key+'_'+index).innerHTML     ='<span>Загрузка...</span>';
    
                            }
    
                            send({
                                'scriptPath':'/api/json/create_face_pdf_file_combo',
                                'postData':post,
                                'onComplete':function(j,worktime){
    
                                    let  data
                                        ,dataTemp;
    
                                    dataTemp=j.responseText;
                                    trace(dataTemp);
                                    data=jsonDecode(dataTemp);
                                    trace(data);
                                    trace_worktime(worktime,data);
    
                                    if(isset(data['error'])){
    
                                        if(isset($d('root_face_result_other_item_profile_'+key+'_'+index))){
    
                                            $d('root_face_result_other_item_profile_'+key+'_'+index).setAttribute('class','root_face_result_other_item_profile_'+key);
    
                                            $d('root_face_result_other_item_profile_'+key+'_'+index).onclick       =page_object.action.profile.action.root_face_block.action.search.old.show_results.pdf.create;
                                            $d('root_face_result_other_item_profile_'+key+'_'+index).innerHTML     ='<span>'+(key==='download'?'Скачать в PDF':'Смотреть отчет')+'</span>';
    
                                        }
    
                                    }
                                    else{
    
                                        window.open('/'+key+'/pdf/find_face/'+data['data']['hash'], "_blank");
    
                                        if(isset($d('root_face_result_other_item_profile_'+key+'_'+index))){
    
                                            $d('root_face_result_other_item_profile_'+key+'_'+index).setAttribute('class','root_face_result_other_item_profile_'+key);
    
                                            $d('root_face_result_other_item_profile_'+key+'_'+index).onclick       =page_object.action.profile.action.root_face_block.action.search.old.show_results.pdf.create;
                                            $d('root_face_result_other_item_profile_'+key+'_'+index).innerHTML     ='<span>'+(key==='download'?'Скачать в PDF':'Смотреть отчет')+'</span>';
    
                                        }
    
                                    }
    
                                }
                            });
    
                        }
                    },
                    'image_list':{
                        'init':function(){
    
                            let  list               =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list']
                                ,chosen_index       =page_object.action.profile.action.root_face_block.action.search.old.show_results.index
                                ,face_data          =list[chosen_index]
                                ,image_list         =face_data['image_list']
                                ,index;
    
                            for(index=0;index<image_list.length;index++)
                                if(isset($d('root_face_result_profile_image_item_'+index)))
                                    $d('root_face_result_profile_image_item_'+index).onclick=page_object.action.profile.action.root_face_block.action.search.old.show_results.show_image;
    
                        }
                    },
                    'show_image':function(){
    
                        var  list           =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list']
                            ,chosen_index   =page_object.action.profile.action.root_face_block.action.search.old.show_results.index
                            ,face_data      =list[chosen_index]
                            ,index          =parseInt(this.id.split('_')[6])
                            ,image_list     =face_data['image_list']
                            ,image_data     =image_list[index]
                            ,image_index
                            ,image_link     ='/'+image_data['image_dir']+'/'+image_data['image_item_ID_list']['large']['ID']
                            ,el             =new Image()
                            ,el_load        =addElement({
                                'tag':'div',
                                'id':'root_face_result_profile_image_loading',
                                'inner':'Загрузка...',
                                'style':'opacity: 0'
                            });

                        if(isset($d('root_face_result_profile_total_img_src_border'))){

                            $s('root_face_result_profile_total_img_src_border').opacity=0;

                            trace(face_data);

                            setTimeout(function(){

                                let  border_block_w     =elementSize.width($d('root_face_result_profile_total_img_container'))
                                    ,border_block_h     =elementSize.height($d('root_face_result_profile_total_img_container'));

                                $s('root_face_result_profile_total_img_src_border').width   =Math.floor(border_block_w)+'px';
                                $s('root_face_result_profile_total_img_src_border').height  =Math.floor(border_block_h)+'px';

                                if(isset(face_data['rang']))
                                    switch(face_data['rang']){

                                        case 1:{

                                            $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_red');

                                            $d('root_face_result_profile_total_img_source_type').innerHTML='Проституция';

                                            break;

                                        }

                                        case 2:{

                                            $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_orange');

                                            $d('root_face_result_profile_total_img_source_type').innerHTML='Эскорт';

                                            break;

                                        }

                                        case 3:{

                                            $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_yellow');

                                            $d('root_face_result_profile_total_img_source_type').innerHTML='Содержание';

                                            break;

                                        }

                                        case 4:{

                                            $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_green');

                                            $d('root_face_result_profile_total_img_source_type').innerHTML='Начинающая';

                                            break;

                                        }

                                    }

                            },300);

                        }
    
                        $d('root_face_result_profile_total_img_block').appendChild(el_load);
    
                        if(isset($d('root_face_result_third_nun')))
                            $s('root_face_result_third_nun').opacit=0;
    
                        setTimeout(function(){
    
                            if(isset($d('root_face_result_profile_image_loading')))
                                $s('root_face_result_profile_image_loading').opacity=1;
    
                        },40);
    
                        for(image_index in image_list)
                            if(isset($d('root_face_result_profile_image_item_'+image_index)))
                                $d('root_face_result_profile_image_item_'+image_index).setAttribute('class','root_face_result_profile_image_item'+(parseInt(image_data['image_ID'])===parseInt(image_list[image_index]['image_ID'])?'_active':''))
    
                        $s('root_face_result_profile_total_img_src').opacity=0;
    
                        el.src          =image_link;
                        el.onload       =function(){
    
                            if(isset($d('root_face_result_profile_image_loading')))
                                $s('root_face_result_profile_image_loading').opacity=0;
    
                            setTimeout(function(){
    
                                $d('root_face_result_profile_total_img_src').src=image_link;
    
                                if(isset($d('root_face_result_profile_image_loading')))
                                    removeElement($d('root_face_result_profile_image_loading'));
    
                                setTimeout(function(){
    
                                    if(isset($d('root_face_result_profile_image_loading')))
                                        $s('root_face_result_profile_image_loading').opacity=1;
    
                                    if(isset($d('root_face_result_third_nun')))
                                        $s('root_face_result_third_nun').display='none';
    
                                },40);
    
                                setTimeout(function(){
    
                                    let  img_source_width           =$d('root_face_result_source_src_image').width
                                        ,img_source_height          =$d('root_face_result_source_src_image').height
                                        ,img_result_width           =$d('root_face_result_profile_total_img_src').width
                                        ,img_result_height          =$d('root_face_result_profile_total_img_src').height
                                        ,block_source_width         =elementSize.width($d('root_face_result_source_src'))
                                        ,block_source_height        =elementSize.height($d('root_face_result_source_src'))
                                        ,block_result_width         =elementSize.width($d('root_face_result_profile_total_img_container'))
                                        ,block_result_height        =elementSize.height($d('root_face_result_profile_total_img_container'))
                                        ,img_source_perc            =img_source_width/img_source_height
                                        ,img_result_perc            =img_result_width/img_result_height
                                        ,img_source_in_width        =block_source_width*img_source_perc
                                        ,img_source_in_height       =block_source_width/img_source_perc
                                        ,img_result_in_width        =block_result_width*img_result_perc
                                        ,img_result_in_height       =block_result_width/img_result_perc;

                                    trace('source: '+block_source_width+' x '+block_source_height);

                                    if(img_source_in_width>img_source_in_height){

                                        if(img_source_in_width>block_source_width){

                                            img_source_in_width     =block_source_width;
                                            img_source_in_height    =img_source_in_width/img_source_perc;

                                            trace('case 1');
                                            trace(img_source_in_width+' x '+img_source_in_height);

                                            if(img_source_in_height<block_source_height){

                                                $d('root_face_result_source_src_image').setAttribute('width','100%');
                                                $d('root_face_result_source_src_image').setAttribute('height','');

                                            }
                                            else{

                                                $d('root_face_result_source_src_image').setAttribute('width','');
                                                $d('root_face_result_source_src_image').setAttribute('height','100%');

                                            }

                                        }
                                        else{

                                            img_source_in_height    =block_source_height;
                                            img_source_in_width     =img_source_in_height*img_source_perc;

                                            trace('case 2');
                                            trace(img_source_in_width+' x '+img_source_in_height);

                                            if(img_source_in_height<block_source_height){

                                                $d('root_face_result_source_src_image').setAttribute('width','100%');
                                                $d('root_face_result_source_src_image').setAttribute('height','');

                                            }
                                            else{

                                                $d('root_face_result_source_src_image').setAttribute('width','');
                                                $d('root_face_result_source_src_image').setAttribute('height','100%');

                                            }

                                        }


                                    }
                                    else{

                                        if(img_source_in_height>block_source_height){

                                            img_source_in_height    =block_source_height;
                                            img_source_in_width     =img_source_in_height*img_source_perc;

                                            trace('case 3');
                                            trace(img_source_in_width+' x '+img_source_in_height);

                                            if(img_source_in_height<block_source_height){

                                                $d('root_face_result_source_src_image').setAttribute('width','100%');
                                                $d('root_face_result_source_src_image').setAttribute('height','');

                                            }
                                            else{

                                                $d('root_face_result_source_src_image').setAttribute('width','');
                                                $d('root_face_result_source_src_image').setAttribute('height','100%');

                                            }

                                        }
                                        else{

                                            img_source_in_width     =block_source_width;
                                            img_source_in_height    =img_source_in_width/img_source_perc;

                                            trace('case 4');
                                            trace(img_source_in_width+' x '+img_source_in_height);

                                            if(img_source_in_height<block_source_height){

                                                $d('root_face_result_source_src_image').setAttribute('width','100%');
                                                $d('root_face_result_source_src_image').setAttribute('height','');

                                            }
                                            else{

                                                $d('root_face_result_source_src_image').setAttribute('width','');
                                                $d('root_face_result_source_src_image').setAttribute('height','100%');

                                            }

                                        }

                                    }

                                    if(img_result_in_width>img_result_in_height){

                                        if(img_result_in_width>block_result_width){

                                            img_result_in_width     =block_result_width;
                                            img_result_in_height    =img_result_in_width/img_result_perc;

                                            trace('case 1');
                                            trace(img_result_in_width+' x '+img_result_in_height);

                                            if(img_result_in_height<block_result_height){

                                                trace('case 1.1');

                                                $d('root_face_result_profile_total_img_src').setAttribute('width','100%');
                                                $d('root_face_result_profile_total_img_src').setAttribute('height','');

                                            }
                                            else{

                                                trace('case 1.2');

                                                img_result_in_height    =block_result_height;
                                                img_result_in_width     =img_result_in_height*img_result_perc;

                                                $d('root_face_result_profile_total_img_src').setAttribute('width','');
                                                $d('root_face_result_profile_total_img_src').setAttribute('height','100%');

                                            }

                                        }
                                        else{

                                            img_result_in_height    =block_result_height;
                                            img_result_in_width     =img_result_in_height*img_result_perc;

                                            trace('case 2');
                                            trace(img_result_in_width+' x '+img_result_in_height);

                                            if(img_result_in_height<block_result_height){

                                                trace('case 2.1');

                                                $d('root_face_result_profile_total_img_src').setAttribute('width','100%');
                                                $d('root_face_result_profile_total_img_src').setAttribute('height','');

                                            }
                                            else{

                                                trace('case 2.2');

                                                img_result_in_height    =block_result_height;
                                                img_result_in_width     =img_result_in_height*img_result_perc;

                                                $d('root_face_result_profile_total_img_src').setAttribute('width','');
                                                $d('root_face_result_profile_total_img_src').setAttribute('height','100%');

                                            }

                                        }

                                    }
                                    else{

                                        if(img_result_in_height>block_result_height){

                                            img_result_in_height    =block_result_height;
                                            img_result_in_width     =img_result_in_height*img_result_perc;

                                            trace('case 3');
                                            trace(img_result_in_width+' x '+img_result_in_height);

                                            if(img_result_in_height<block_result_height){

                                                trace('case 3.1');

                                                $d('root_face_result_profile_total_img_src').setAttribute('width','100%');
                                                $d('root_face_result_profile_total_img_src').setAttribute('height','');

                                            }
                                            else{

                                                trace('case 3.2');

                                                img_result_in_height    =block_result_height;
                                                img_result_in_width     =img_result_in_height*img_result_perc;

                                                $d('root_face_result_profile_total_img_src').setAttribute('width','');
                                                $d('root_face_result_profile_total_img_src').setAttribute('height','100%');

                                            }

                                        }
                                        else{

                                            img_result_in_width     =block_result_width;
                                            img_result_in_height    =img_result_in_width/img_result_perc;

                                            trace('case 4');
                                            trace(img_result_in_width+' x '+img_result_in_height);

                                            if(img_result_in_height<block_result_height){

                                                trace('case 4.1');

                                                $d('root_face_result_profile_total_img_src').setAttribute('width','100%');
                                                $d('root_face_result_profile_total_img_src').setAttribute('height','');

                                            }
                                            else{

                                                trace('case 4.2');

                                                img_result_in_height    =block_result_height;
                                                img_result_in_width     =img_result_in_height*img_result_perc;

                                                $d('root_face_result_profile_total_img_src').setAttribute('width','');
                                                $d('root_face_result_profile_total_img_src').setAttribute('height','100%');

                                            }

                                        }

                                    }

                                    if(isset($d('root_face_result_profile_total_img_src_border'))){

                                        $s('root_face_result_profile_total_img_src_border').opacity =1;

                                        $s('root_face_result_profile_total_img_src_border').width   =Math.floor(img_result_in_width)+'px';
                                        $s('root_face_result_profile_total_img_src_border').height  =Math.floor(img_result_in_height)+'px';

                                        if(isset(face_data['rang']))
                                            switch(face_data['rang']){

                                                case 1:{

                                                    $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_red');

                                                    $d('root_face_result_profile_total_img_source_type').innerHTML='Проституция';

                                                    break;

                                                }

                                                case 2:{

                                                    $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_orange');

                                                    $d('root_face_result_profile_total_img_source_type').innerHTML='Эскорт';

                                                    break;

                                                }

                                                case 3:{

                                                    $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_yellow');

                                                    $d('root_face_result_profile_total_img_source_type').innerHTML='Содержание';

                                                    break;

                                                }

                                                case 4:{

                                                    $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_green');

                                                    $d('root_face_result_profile_total_img_source_type').innerHTML='Начинающая';

                                                    break;

                                                }

                                            }

                                    }

                                    setTimeout(function(){

                                        $s('root_face_result_profile_total_img_src').opacity=1;

                                    },300);
    
                                },40);
    
                            },300);
    
                        };
    
                    },
                    'show':function(){
    
                        setTimeout(function(){

                            var  list                       =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['face_list']
                                ,chosen_index               =0
                                ,face_data                  =list[chosen_index]
                                // ,image_data                 =page_object.action.profile.action.root_face_block.action.search.old.show_results.data['image']
                                // ,img_source_width           =image_data['image_item_ID_list']['middle']['width']
                                // ,img_source_height          =image_data['image_item_ID_list']['middle']['height']
                                ,img_source_width           =$d('root_face_result_source_src_image').width
                                ,img_source_height          =$d('root_face_result_source_src_image').height
                                ,block_source_width         =elementSize.width($d('root_face_result_source_src'))
                                ,block_source_height        =elementSize.height($d('root_face_result_source_src'))
                                ,img_source_perc            =img_source_width/img_source_height
                                ,img_source_in_width        =block_source_width*img_source_perc
                                ,img_source_in_height       =block_source_width/img_source_perc;

                            if(img_source_in_width>img_source_in_height){

                                if(img_source_in_width>block_source_width){

                                    img_source_in_width     =block_source_width;
                                    img_source_in_height    =img_source_in_width/img_source_perc;

                                    trace('case 1');
                                    trace(img_source_in_width+' x '+img_source_in_height);

                                    if(img_source_in_height<block_source_height){

                                        $d('root_face_result_source_src_image').setAttribute('width','100%');
                                        $d('root_face_result_source_src_image').setAttribute('height','');

                                    }
                                    else{

                                        $d('root_face_result_source_src_image').setAttribute('width','');
                                        $d('root_face_result_source_src_image').setAttribute('height','100%');

                                    }

                                }
                                else{

                                    img_source_in_height    =block_source_height;
                                    img_source_in_width     =img_source_in_height*img_source_perc;

                                    trace('case 2');
                                    trace(img_source_in_width+' x '+img_source_in_height);

                                    if(img_source_in_height<block_source_height){

                                        $d('root_face_result_source_src_image').setAttribute('width','100%');
                                        $d('root_face_result_source_src_image').setAttribute('height','');

                                    }
                                    else{

                                        $d('root_face_result_source_src_image').setAttribute('width','');
                                        $d('root_face_result_source_src_image').setAttribute('height','100%');

                                    }

                                }


                            }
                            else{

                                if(img_source_in_height>block_source_height){

                                    img_source_in_height    =block_source_height;
                                    img_source_in_width     =img_source_in_height*img_source_perc;

                                    trace('case 3');
                                    trace(img_source_in_width+' x '+img_source_in_height);

                                    if(img_source_in_height<block_source_height){

                                        $d('root_face_result_source_src_image').setAttribute('width','100%');
                                        $d('root_face_result_source_src_image').setAttribute('height','');

                                    }
                                    else{

                                        $d('root_face_result_source_src_image').setAttribute('width','');
                                        $d('root_face_result_source_src_image').setAttribute('height','100%');

                                    }

                                }
                                else{

                                    img_source_in_width     =block_source_width;
                                    img_source_in_height    =img_source_in_width/img_source_perc;

                                    trace('case 4');
                                    trace(img_source_in_width+' x '+img_source_in_height);

                                    if(img_source_in_height<block_source_height){

                                        $d('root_face_result_source_src_image').setAttribute('width','100%');
                                        $d('root_face_result_source_src_image').setAttribute('height','');

                                    }
                                    else{

                                        $d('root_face_result_source_src_image').setAttribute('width','');
                                        $d('root_face_result_source_src_image').setAttribute('height','100%');

                                    }

                                }

                            }

                            if(isset($d('root_face_result_profile_total_img_src'))){

                                var  img_result_width           =$d('root_face_result_profile_total_img_src').width
                                    ,img_result_height          =$d('root_face_result_profile_total_img_src').height
                                    ,block_result_width         =elementSize.width($d('root_face_result_profile_total_img_container'))
                                    ,block_result_height        =elementSize.height($d('root_face_result_profile_total_img_container'))
                                    ,img_result_perc            =img_result_width/img_result_height
                                    ,img_result_in_width        =block_result_width*img_result_perc
                                    ,img_result_in_height       =block_result_width/img_result_perc;

                                if(img_result_in_width>img_result_in_height){

                                    if(img_result_in_width>block_result_width){

                                        img_result_in_width     =block_result_width;
                                        img_result_in_height    =img_result_in_width/img_result_perc;

                                        trace('case 1');
                                        trace(img_result_in_width+' x '+img_result_in_height);

                                        if(img_result_in_height<block_result_height){

                                            $d('root_face_result_profile_total_img_src').setAttribute('width','100%');
                                            $d('root_face_result_profile_total_img_src').setAttribute('height','');

                                        }
                                        else{

                                            $d('root_face_result_profile_total_img_src').setAttribute('width','');
                                            $d('root_face_result_profile_total_img_src').setAttribute('height','100%');

                                        }

                                    }
                                    else{

                                        img_result_in_height    =block_result_height;
                                        img_result_in_width     =img_result_in_height*img_result_perc;

                                        trace('case 2');
                                        trace(img_result_in_width+' x '+img_result_in_height);

                                        if(img_result_in_height<block_result_height){

                                            $d('root_face_result_profile_total_img_src').setAttribute('width','100%');
                                            $d('root_face_result_profile_total_img_src').setAttribute('height','');

                                        }
                                        else{

                                            $d('root_face_result_profile_total_img_src').setAttribute('width','');
                                            $d('root_face_result_profile_total_img_src').setAttribute('height','100%');

                                        }

                                    }


                                }
                                else{

                                    if(img_result_in_height>block_result_height){

                                        img_result_in_height    =block_result_height;
                                        img_result_in_width     =img_result_in_height*img_result_perc;

                                        if(img_result_in_height<block_result_height){

                                            $d('root_face_result_profile_total_img_src').setAttribute('width','100%');
                                            $d('root_face_result_profile_total_img_src').setAttribute('height','');

                                        }
                                        else{

                                            $d('root_face_result_profile_total_img_src').setAttribute('width','');
                                            $d('root_face_result_profile_total_img_src').setAttribute('height','100%');

                                        }

                                    }
                                    else{

                                        img_result_in_width     =block_result_width;
                                        img_result_in_height    =img_result_in_width/img_result_perc;

                                        trace('case 4');
                                        trace(img_result_in_width+' x '+img_result_in_height);

                                        if(img_result_in_height<block_result_height){

                                            $d('root_face_result_profile_total_img_src').setAttribute('width','100%');
                                            $d('root_face_result_profile_total_img_src').setAttribute('height','');

                                        }
                                        else{

                                            $d('root_face_result_profile_total_img_src').setAttribute('width','');
                                            $d('root_face_result_profile_total_img_src').setAttribute('height','100%');

                                        }

                                    }

                                }

                                if(isset($d('root_face_result_profile_total_img_src_border'))){

                                    $s('root_face_result_profile_total_img_src_border').opacity =1;

                                    $s('root_face_result_profile_total_img_src_border').width   =Math.floor(img_result_in_width)+'px';
                                    $s('root_face_result_profile_total_img_src_border').height  =Math.floor(img_result_in_height)+'px';

                                    if(isset(face_data['rang']))
                                        switch(face_data['rang']){

                                            case 1:{

                                                $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_red');

                                                $d('root_face_result_profile_total_img_source_type').innerHTML='Проституция';

                                                break;

                                            }

                                            case 2:{

                                                $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_orange');

                                                $d('root_face_result_profile_total_img_source_type').innerHTML='Эскорт';

                                                break;

                                            }

                                            case 3:{

                                                $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_yellow');

                                                $d('root_face_result_profile_total_img_source_type').innerHTML='Содержание';

                                                break;

                                            }

                                            case 4:{

                                                $d('root_face_result_profile_total_img_src_border').setAttribute('class','root_face_result_profile_total_img_src_border_green');

                                                $d('root_face_result_profile_total_img_source_type').innerHTML='Начинающая';

                                                break;

                                            }

                                        }

                                }

                            }

                            if(isset($d('root_face_result_close_button')))
                                $d('root_face_result_close_button').onclick=page_object.action.profile.action.root_face_block.action.search.old.show_results.un_show;
    
                            if(isset($d('root_face_result_block')))
                                $s('root_face_result_block').opacity=1;

                            if(list.length===0){

                                if(isset($d('root_face_result_third_nun'))){

                                    $s('root_face_result_third_nun').display        ='block';
                                    $s('root_face_result_third_nun').opacity        =1;

                                }

                                if(isset($d('root_face_result_third_block')))
                                    $s('root_face_result_third_block').display='none';

                                if(OS.isMobile){

                                    if(isset($d('root_face_result_profile_info_block')))
                                        $s('root_face_result_profile_info_block').display='none';

                                    if(isset($d('root_face_result_profile_image_list_block')))
                                        $s('root_face_result_profile_image_list_block').display='none';

                                }

                            }
                            else{

                                if(list[0]['percent']<9000){

                                    if(isset($d('root_face_result_third_nun'))){

                                        $s('root_face_result_third_nun').display        ='block';
                                        $s('root_face_result_third_nun').opacity        =1;

                                    }

                                    if(isset($d('root_face_result_third_block')))
                                        $s('root_face_result_third_block').display='none';

                                    if(OS.isMobile){

                                        if(isset($d('root_face_result_profile_info_block')))
                                            $s('root_face_result_profile_info_block').display='none';

                                        if(isset($d('root_face_result_profile_image_list_block')))
                                            $s('root_face_result_profile_image_list_block').display='none';

                                    }

                                }
                                else{

                                    if(isset($d('root_face_result_third_nun'))){

                                        $s('root_face_result_third_nun').display        ='none';
                                        $s('root_face_result_third_nun').opacity        =0;

                                    }

                                    if(isset($d('root_face_result_third_block')))
                                        $s('root_face_result_third_block').display='block';

                                    if(OS.isMobile){

                                        if(isset($d('root_face_result_profile_info_block')))
                                            $s('root_face_result_profile_info_block').display='block';

                                        if(isset($d('root_face_result_profile_image_list_block')))
                                            $s('root_face_result_profile_image_list_block').display='block';

                                    }

                                }

                            }

                        },300);
    
                    },
                    'un_show':function(){
    
                        if(isset($d('root_face_result_block')))
                            $s('root_face_result_block').opacity=0;
    
                        page_object.action.profile.action.root_face_block.action.image.reset();
    
                        setTimeout(function(){
    
                            if(isset($d('root_face_result_block')))
                                removeElement($d('root_face_result_block'));
    
                            page_object.action.profile.create.root_content.init();
    
                        },300);
    
                    },
                    'scroll':{
                        'delta':130,
                        'up':function(){
    
                            let  scroll_top         =$d('root_face_result_profile_image_list').scrollTop
                                ,delta_step         =page_object.action.profile.action.root_face_block.action.search.old.show_results.scroll.delta
                                ,scroll_top_new     =scroll_top-delta_step;
    
                            if(scroll_top_new<0)
                                scroll_top_new=0;
    
                            $d('root_face_result_profile_image_list').scrollTop=scroll_top_new;
    
                            page_object.action.profile.action.root_face_block.action.search.old.show_results.scroll.buttons();
    
                        },
                        'down':function(){
    
                            let  scroll_height      =$d('root_face_result_profile_image_list').scrollHeight
                                ,scroll_top         =$d('root_face_result_profile_image_list').scrollTop
                                ,block_height       =elementSize.height($d('root_face_result_profile_image_list'))
                                ,delta_step         =page_object.action.profile.action.root_face_block.action.search.old.show_results.scroll.delta
                                ,scroll_top_new     =scroll_top+delta_step;
    
                            if(scroll_top_new+block_height>scroll_height)
                                scroll_top_new=scroll_height-block_height;
    
                            $d('root_face_result_profile_image_list').scrollTop=scroll_top_new;
    
                            page_object.action.profile.action.root_face_block.action.search.old.show_results.scroll.buttons();
    
                        },
                        'buttons':function(){
    
                            let  scroll_height      =$d('root_face_result_profile_image_list').scrollHeight
                                ,scroll_top         =$d('root_face_result_profile_image_list').scrollTop
                                ,block_height       =elementSize.height($d('root_face_result_profile_image_list'))
                                ,delta              =scroll_top+block_height;
    
                            if(scroll_top===0){
    
                                $d('root_face_result_profile_image_scroll_up').setAttribute('class','root_face_result_profile_image_scroll_up_disable');
                                $d('root_face_result_profile_image_scroll_up').onclick=function(){};
    
                            }
                            else{
    
                                $d('root_face_result_profile_image_scroll_up').setAttribute('class','root_face_result_profile_image_scroll_up');
                                $d('root_face_result_profile_image_scroll_up').onclick=page_object.action.profile.action.root_face_block.action.search.old.show_results.scroll.up;
    
                            }
    
                            if(delta===scroll_height){
    
                                $d('root_face_result_profile_image_scroll_down').setAttribute('class','root_face_result_profile_image_scroll_down_disable');
                                $d('root_face_result_profile_image_scroll_down').onclick=function(){};
    
                            }
                            else{
    
                                $d('root_face_result_profile_image_scroll_down').setAttribute('class','root_face_result_profile_image_scroll_down');
                                $d('root_face_result_profile_image_scroll_down').onclick=page_object.action.profile.action.root_face_block.action.search.old.show_results.scroll.down;
    
                            }
    
                        }
                    }
                },
                'un_show':function(){

                    if(!empty(page_object.action.profile.action.root_face_block.action.search.dialog_index))
                        page_object.dialog.action.un_show.init(page_object.action.profile.action.root_face_block.action.search.dialog_index,true);

                }
            },
            'new':{
                'face_search_multi_token':null,
                'face_search_token_list':[],
                'part_index':null,
                'parts_len':null,
                'init':function(){

                    page_object.action.profile.action.root_face_block.action.search.new.face_search_multi_token     =null;
                    page_object.action.profile.action.root_face_block.action.search.new.face_search_token_list      =[];
                    page_object.action.profile.action.root_face_block.action.search.new.part_index                  =0;
                    page_object.action.profile.action.root_face_block.action.search.new.parts_len                   =0;

                    let  cost           =500
                        ,total_cost     =0;

                    if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===0)
                        total_cost=0;
                    else if(page_object.action.profile.action.root_face_block.action.image.face.face_change_len===1)
                        total_cost=cost;
                    else
                        total_cost=2*cost;

                    let user_data=page_object.action.profile.data['user'];
    
                    if(parseFloat(user_data['balance'])<total_cost){
    
                        alert('Недостаточно средств для совершения действия');
    
                        return false;
    
                    }
    
                    let  post=''
                        ,list           =page_object.action.profile.action.root_face_block.action.image.face.list
                        ,inner          =''
                        ,image_list     =[]
                        ,index;
    
                    if(isset($d('root_search_face_button'))){
    
                        $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                        inner+='<div id="root_search_face_button_label">Ищем совпадения...</div>';
                        inner+='<div id="root_search_face_button_progress"></div>';

                        $d('root_search_face_button').innerHTML     =inner;
                        $d('root_search_face_button').onclick       =function(){};
    
                    }

                    trace('Face token list default:');
                    trace(list);

                    for(index in list)
                        if(!is_null(list[index]['face_index'])){

                            image_list.push({
                                'face_index':list[index]['face_index'],
                                'file_ID':list[index]['file_ID'],
                                'image_ID':list[index]['image_ID'],
                                'face_token':list[index]['face_list'][list[index]['face_index']]['face_token'],
                                'face_coords_list':list[index]['face_list']
                            });

                            if(image_list.length===3)
                                break;

                        }

                    page_object.action.profile.action.root_face_block.action.image.face.face_token_list=image_list;

                    trace('Face token list for search:');
                    trace(image_list);

                    trace('List before send:');
                    trace(image_list);
    
                    post+='list='+uniEncode(jsonEncode(image_list));

                    post+='&is_test=1';
    
                    send({
                        'scriptPath':'/api/json/get_face_search_parts_len_combo',
                        'postData':post,
                        'onComplete':function(j,worktime){
    
                            let  data
                                ,dataTemp;
    
                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);
    
                            if(isset(data['error'])){
    
                                if(isset($d('root_search_info_block'))){
    
                                    $d('root_search_info_block').setAttribute('class','root_search_info_block_red');
    
                                    $d('root_search_info_block').innerHTML=data['error']['info'];
    
                                    setTimeout(function(){
    
                                        if(isset($d('root_search_info_block'))){

                                            $s('root_search_info_block').opacity    =1;
                                            $s('root_search_info_block').height     ='auto';

                                        }
    
                                    },40);
    
                                    setTimeout(function(){
    
                                        if(isset($d('root_search_info_block'))){

                                            $s('root_search_info_block').opacity    =0;
                                            $s('root_search_info_block').height     ='';

                                        }
    
                                    },3000);
    
                                }
    
                                setTimeout(page_object.action.profile.action.root_face_block.action.image.reset,3000);
    
                            }
                            else{

                                if(isset($d('user_balance'))){

                                    $d('user_balance').innerHTML=empty(data['data']['balance'])?0:data['data']['balance'];

                                    if(data['data']['balance']<page_object.action.profile.data['user']['price'])
                                        $s('user_balance').color='#ff0000';

                                }

                                page_object.action.profile.data['user']['balance']=data['data']['balance'];

                                page_object.action.profile.action.root_face_block.action.search.new.face_search_multi_token     =data['data']['face_search_multi_token'];
                                page_object.action.profile.action.root_face_block.action.search.new.face_search_token_list      =data['data']['face_search_token_list'];
                                page_object.action.profile.action.root_face_block.action.search.new.parts_len                   =data['data']['parts_len'];

                                trace('Stared find face new parts');

                                page_object.action.profile.action.root_face_block.action.search.new.parts.init();

                                trace('Finish find face new parts');
    
                            }
    
                        }
                    });
                    
                },
                'parts':{
                    'init':function(){

                        let face_search_token_list=page_object.action.profile.action.root_face_block.action.search.new.face_search_token_list;

                        if(face_search_token_list.length===0)
                            return false;

                        if(isset($d('root_search_face_content_loading'))){

                            $d('root_search_face_content_loading').innerHTML='<span>Не обновляйте страницу</span>';

                            $s('root_search_face_content_loading').display='block';

                            setTimeout(function(){

                                if(isset($d('root_search_face_content_loading')))
                                    $s('root_search_face_content_loading').opacity=1;

                            },40);

                        }

                        page_object.action.profile.action.root_face_block.action.search.new.parts.send(0);

                        return true;

                    },
                    'send':function(index){

                        if(
                              !isset($d('root_search_face_image_src_object_0'))
                            &&!isset($d('root_search_face_image_src_object_1'))
                            &&!isset($d('root_search_face_image_src_object_2'))
                        )
                            return false;

                        var  face_search_multi_token    =page_object.action.profile.action.root_face_block.action.search.new.face_search_multi_token
                            ,face_search_token_list     =page_object.action.profile.action.root_face_block.action.search.new.face_search_token_list
                            ,face_search_token          =face_search_token_list[index];

                        page_object.action.profile.action.root_face_block.action.search.new.part_index++;

                        trace('start part index: '+page_object.action.profile.action.root_face_block.action.search.new.part_index);

                        let post='';

                        post+='&face_search_multi_token='+uniEncode(face_search_multi_token);
                        post+='&face_search_token='+uniEncode(face_search_token);

                        post+='&is_test=1';

                        send({
                            'scriptPath':'/api/json/search_face_part_combo',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                if(
                                      !isset($d('root_search_face_image_src_object_0'))
                                    &&!isset($d('root_search_face_image_src_object_1'))
                                    &&!isset($d('root_search_face_image_src_object_2'))
                                )
                                    return false;

                                let  data
                                    ,dataTemp;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(!isset($d('root_search_face_button_label')))
                                    return false;

                                if(isset(data['error'])){

                                    if(isset($d('root_search_info_block'))){

                                        $d('root_search_info_block').setAttribute('class','root_search_info_block_red');

                                        $d('root_search_info_block').innerHTML=data['error']['info'];

                                        setTimeout(function(){

                                            if(isset($d('root_search_info_block'))){

                                                $s('root_search_info_block').opacity    =1;
                                                $s('root_search_info_block').height     ='auto';

                                            }

                                        },40);

                                        setTimeout(function(){

                                            if(isset($d('root_search_info_block'))){

                                                $s('root_search_info_block').opacity    =0;
                                                $s('root_search_info_block').height     ='';

                                            }

                                        },3000);

                                    }

                                    setTimeout(page_object.action.profile.action.root_face_block.action.image.reset,3000);

                                }
                                else{

                                    let  current_part       =index*page_object.action.profile.action.root_face_block.action.search.new.parts_len+page_object.action.profile.action.root_face_block.action.search.new.part_index
                                        ,face_len           =page_object.action.profile.action.root_face_block.action.search.new.face_search_token_list.length
                                        ,full_parts_len     =face_len*page_object.action.profile.action.root_face_block.action.search.new.parts_len;

                                    let  perc=((current_part)/(full_parts_len)*100).toFixed(2);

                                    trace('');
                                    trace(page_object.action.profile.action.root_face_block.action.search.new.part_index+' / '+page_object.action.profile.action.root_face_block.action.search.new.parts_len);
                                    trace('Percent: '+perc);
                                    trace('Full parts len: '+full_parts_len);

                                    if(isset($d('root_search_face_button_label'))){

                                        if(perc>=100)
                                            $d('root_search_face_button_label').innerHTML='Формируем результат...';
                                        else
                                            // $d('root_search_face_button_label').innerHTML='Ищем совпадения...'+perc+'%';
                                            $d('root_search_face_button_label').innerHTML='Ищем совпадения...';

                                        $s('root_search_face_button_progress').width=perc+'%';

                                    }

                                    trace('CURRENT PART:');
                                    trace(current_part+' === '+full_parts_len);

                                    if(page_object.action.profile.action.root_face_block.action.search.new.part_index===page_object.action.profile.action.root_face_block.action.search.new.parts_len){

                                        if(current_part===full_parts_len){

                                            page_object.action.profile.action.root_face_block.action.search.old.show_results.index      =0;
                                            page_object.action.profile.action.root_face_block.action.search.old.show_results.data       =data;

                                            if(isset(data['balance'])){

                                                if(isset($d('user_balance'))){

                                                    $d('user_balance').innerHTML=empty(data['balance'])?0:data['balance'];

                                                    if(data['balance']<page_object.action.profile.data['user']['price'])
                                                        $s('user_balance').color='#ff0000';

                                                }

                                                page_object.action.profile.data['user']['balance']=data['balance'];

                                            }

                                            if(isset($d('root_search_face_button')))
                                                $d('root_search_face_button').innerHTML='Подсчитываем результаты...';

                                            page_object.action.profile.action.root_face_block.action.search.new.parts.calculation_results();

                                            trace('DONE');

                                        }
                                        else{

                                            index++;

                                            page_object.action.profile.action.root_face_block.action.search.new.part_index=0;

                                            page_object.action.profile.action.root_face_block.action.search.new.parts.send(index);

                                        }

                                    }
                                    else
                                        page_object.action.profile.action.root_face_block.action.search.new.parts.send(index);

                                }

                                return true;

                            }
                        });

                    },
                    'calculation_results':function(){

                        if(
                              !isset($d('root_search_face_image_src_object_0'))
                            &&!isset($d('root_search_face_image_src_object_1'))
                            &&!isset($d('root_search_face_image_src_object_2'))
                        )
                            return false;

                        let  face_search_multi_token    =page_object.action.profile.action.root_face_block.action.search.new.face_search_multi_token
                            ,post                       ='';

                        post+='&face_search_multi_token='+uniEncode(face_search_multi_token);

                        post+='&is_test=1';

                        send({
                            'scriptPath':'/api/json/get_face_search_combo_result',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                if(
                                      !isset($d('root_search_face_image_src_object_0'))
                                    &&!isset($d('root_search_face_image_src_object_1'))
                                    &&!isset($d('root_search_face_image_src_object_2'))
                                ){

                                    trace('Empty root search src object');

                                    return false;

                                }

                                let  data
                                    ,dataTemp;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset($d('root_search_face_content_loading'))){

                                    $s('root_search_face_content_loading').opacity=0;

                                    setTimeout(function(){

                                        if(isset($d('root_search_face_content_loading')))
                                            $s('root_search_face_content_loading').display='none';

                                    },300);

                                }

                                if(isset(data['error'])){

                                    if(isset($d('root_search_info_block'))){

                                        $d('root_search_info_block').setAttribute('class','root_search_info_block_red');

                                        $d('root_search_info_block').innerHTML=data['error']['info'];

                                        setTimeout(function(){

                                            if(isset($d('root_search_info_block'))){

                                                $s('root_search_info_block').opacity    =1;
                                                $s('root_search_info_block').height     ='auto';

                                            }

                                        },40);

                                        setTimeout(function(){

                                            if(isset($d('root_search_info_block'))){

                                                $s('root_search_info_block').opacity    =0;
                                                $s('root_search_info_block').height     ='';

                                            }

                                        },3000);

                                    }

                                    setTimeout(page_object.action.profile.action.root_face_block.action.image.reset,3000);

                                }
                                else{

                                    page_object.action.profile.action.root_face_block.action.search.old.show_results.index      =0;
                                    page_object.action.profile.action.root_face_block.action.search.old.show_results.data       =data['data'];

                                    page_object.action.profile.action.root_face_block.action.search.old.show_results.init();

                                    trace('DONE');

                                }

                                return true;

                            }
                        });

                    }
                }
            }
        },
        'search_history':{
            'init':function(){

                page_object.action.profile.action.root_face_block.action.search_history.pdf.init();

            },
            'un_show':function(){

                if(isset($d('logo')))
                    $d('logo').click();

            },
            'pdf':{
                'init':function(){

                    let  face_list      =page_object.action.profile.data['data']['face_search_history_list']
                        ,search_index
                        ,face_index;

                    for(search_index in face_list)
                        for(face_index in face_list[search_index]['face_list']){

                        if(isset($d('search_history_item_profile_pdf_'+search_index+'_'+face_index)))
                            $d('search_history_item_profile_pdf_'+search_index+'_'+face_index).onclick=page_object.action.profile.action.root_face_block.action.search_history.pdf.create;

                    }

                    if(isset($d('settings_close_button')))
                        $d('settings_close_button').onclick=page_object.action.profile.action.root_face_block.action.search_history.un_show;

                },
                'create':function(){

                    var  face_list          =page_object.action.profile.data['data']['face_search_history_list']
                        ,key_list           =this.id.split('_')
                        ,search_index       =key_list[5]
                        ,face_index         =key_list[6]
                        ,search_data        =face_list[search_index]
                        ,face_data          =search_data['face_list'][face_index]
                        ,post               ='';

                    post+='face_token='+uniEncode(face_data['face_token']);
                    post+='&face_search_multi_token='+uniEncode(search_data['face_search_multi_token']);

                    if(isset($d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index))){

                        $d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).innerHTML   ='Загрузка...';
                        $s('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).opacity     =1;

                    }

                    if(isset($d('search_history_item_profile_pdf_info_'+search_index+'_'+face_index)))
                        $s('search_history_item_profile_pdf_info_'+search_index+'_'+face_index).opacity=0;

                    send({
                        'scriptPath':'/api/json/create_face_pdf_file_combo',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  data
                                ,dataTemp;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if(isset($d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index))){

                                    $d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).innerHTML='Ошибка доступа!';

                                    setTimeout(function(){

                                        $s('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).opacity=0;

                                        setTimeout(function(){

                                            $d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).innerHTML='Загрузка...';

                                        },400);

                                    },3000);

                                }

                                if(isset($d('search_history_item_profile_pdf_info_'+search_index+'_'+face_index)))
                                    $s('search_history_item_profile_pdf_info_'+search_index+'_'+face_index).opacity=1;

                            }
                            else{

                                if(isset($d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index)))
                                    $s('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).opacity=0;

                                if(isset($d('search_history_item_profile_pdf_info_'+search_index+'_'+face_index)))
                                    $s('search_history_item_profile_pdf_info_'+search_index+'_'+face_index).opacity=1;

                                if(isset($d('pdf_link_'+search_index+'_'+face_index)))
                                    removeElement($d('pdf_link_'+search_index+'_'+face_index));

                                let  link='/show/pdf/find_face/'+data['data']['hash']
                                    ,el;

                                el=addElement({
                                    'tag':'a',
                                    'id':'pdf_link_'+search_index+'_'+face_index,
                                    'href':link,
                                    'target':'_blank',
                                });

                                $d('all').appendChild(el);

                                // window.open(link);

                                $d('pdf_link_'+search_index+'_'+face_index).click();

                            }

                        }
                    });

                }
            },
        },
        'payment':{
            'init':function(){

                page_object.action.profile.action.root_face_block.action.payment.wallet.init();
                page_object.action.profile.action.root_face_block.action.payment.sum.init();
                page_object.action.profile.action.root_face_block.action.payment.total.init();
                page_object.action.profile.action.root_face_block.action.payment.cancel();
                page_object.action.profile.action.root_face_block.action.payment.copy_to_buffer.init();
                page_object.action.profile.action.root_face_block.action.payment.submit.init();

                if(isset($d('settings_close_button')))
                    $d('settings_close_button').onclick=page_object.action.profile.action.root_face_block.action.search_history.un_show;

            },
            'cancel':function(){

                if(isset($d('profile_payment_cancel')))
                    $d('profile_payment_cancel').onclick=page_object.action.profile.action.root_face_block.action.payment.reset;

            },
            'un_show':function(){

                if(isset($d('logo')))
                    $d('logo').click();

            },
            'reset':function(){

                let  list       =$d('profile_payment_wallet_list').getElementsByClassName('profile_payment_wallet_item_active')
                    ,index;

                for(index in list)
                    if(isObject(list[index]))
                        list[index].setAttribute('class','profile_payment_wallet_item');

                page_object.action.profile.action.root_face_block.action.payment.wallet.index=null;

                page_object.action.profile.action.root_face_block.action.payment.sum.reset();
                page_object.action.profile.action.root_face_block.action.payment.total.reset();

                page_object.action.profile.action.root_face_block.action.payment.total.action.un_show();
                page_object.action.profile.action.root_face_block.action.payment.sum.action.un_show();

            },
            'wallet':{
                'index':null,
                'init':function(){

                    page_object.action.profile.action.root_face_block.action.payment.wallet.index=0;

                    let  list       =$d('profile_payment_wallet_list').getElementsByClassName('profile_payment_wallet_item')
                        ,index;

                    for(index in list)
                        if(isObject(list[index]))
                            list[index].onclick=page_object.action.profile.action.root_face_block.action.payment.wallet.check;

                },
                'check':function(){

                    var  data               =page_object.action.profile.data['data']
                        ,wallet_list        =data['wallet_list']
                        ,wallet_index       =this.id.split('_')[3]
                        ,index;

                    page_object.action.profile.action.root_face_block.action.payment.wallet.index=wallet_index;

                    if($d('profile_payment_wallet_'+wallet_index).getAttribute('class')==='profile_payment_wallet_item_active'){

                        page_object.action.profile.action.root_face_block.action.payment.sum.action.un_show();

                    }
                    else{

                        if(page_object.action.profile.action.root_face_block.action.payment.sum.is_open){

                            page_object.action.profile.action.root_face_block.action.payment.sum.action.un_show();
                            page_object.action.profile.action.root_face_block.action.payment.total.action.un_show();

                            setTimeout(function(){

                                switch(wallet_index){

                                    case'pp':
                                    case'cc':{

                                        if(isset($d('profile_payment_sum_500')))
                                            $s('profile_payment_sum_500').display='block';

                                        if(isset($d('profile_payment_sum_10000')))
                                            $s('profile_payment_sum_10000').display='none';

                                        break;

                                    }

                                    default:{

                                        if(isset($d('profile_payment_sum_500')))
                                            $s('profile_payment_sum_500').display='none';

                                        if(isset($d('profile_payment_sum_10000')))
                                            $s('profile_payment_sum_10000').display='block';

                                        break;

                                    }

                                }

                                switch(wallet_index){

                                    case'sberbank':{

                                        if(isset($d('profile_payment_wallet_from_block')))
                                            $s('profile_payment_wallet_from_block').display='block';

                                        if(isset($d('profile_payment_wallet_button')))
                                            $s('profile_payment_wallet_button').display='none';

                                        if(isset($d('profile_payment_label_type')))
                                            $d('profile_payment_label_type').innerHTML='Введите последние 4 цифры КАРТЫ списания средств <span id="wallet_name">Сбербанка</span>:';
                                            // $d('profile_payment_label_type').innerHTML='Имя и отчество владельца счета <span id="wallet_name">Сбербанка</span>:';

                                        break;

                                    }

                                    case'qiwi':{

                                        if(isset($d('profile_payment_wallet_from_block')))
                                            $s('profile_payment_wallet_from_block').display='block';

                                        if(isset($d('profile_payment_wallet_button')))
                                            $s('profile_payment_wallet_button').display='none';

                                        if(isset($d('profile_payment_label_type')))
                                            $d('profile_payment_label_type').innerHTML='Номер вашего <span id="wallet_name">QIWI</span> кошелька:';

                                        break;

                                    }

                                    case'cc':
                                    case'pp':{

                                        if(isset($d('profile_payment_sum_1000')))
                                            $d('profile_payment_sum_1000').click();

                                        if(isset($d('profile_payment_wallet_from_block')))
                                            $s('profile_payment_wallet_from_block').display='none';

                                        if(isset($d('profile_payment_wallet_button')))
                                            $s('profile_payment_wallet_button').display='block';

                                        var is_sandbox=false;

                                        if(isset($d('profile_payment_wallet_button')))
                                            $d('profile_payment_wallet_button').innerHTML='';

                                        paypal.Buttons({

                                            // 'env': 'sandbox',
                                            'createOrder':function(data, actions) {

                                                // page_object.action.profile.action.root_face_block.action.payment.sum.index=1;

                                                return actions.order.create({
                                                    purchase_units:[{
                                                        amount:{
                                                            value:page_object.action.profile.action.root_face_block.action.payment.sum.index
                                                        }
                                                    }]
                                                });

                                            },
                                            'onApprove':function(data, actions){

                                                page_object.link.preload.show();

                                                return actions.order.capture().then(function(details){

                                                    // page_object.action.profile.action.root_face_block.action.payment.sum.index=1;

                                                    var post='';

                                                    post+='order_ID='+uniEncode(data.orderID);
                                                    post+='&amount='+uniEncode(page_object.action.profile.action.root_face_block.action.payment.sum.index);
                                                    post+='&is_sandbox='+uniEncode(is_sandbox?1:0);

                                                    send({
                                                        'scriptPath':'/api/json/paypal_payment_check',
                                                        'postData':post,
                                                        'onComplete':function(j,worktime){

                                                            let  data
                                                                ,dataTemp;

                                                            dataTemp=j.responseText;
                                                            trace(dataTemp);
                                                            data=jsonDecode(dataTemp);
                                                            trace(data);
                                                            trace_worktime(worktime,data);

                                                            page_object.link.preload.un_show();

                                                            var  inner=''
                                                                ,dialog_index;

                                                            if(isset(data['error'])){

                                                                inner+='<div class="dialog_row" style="color: #ff0000;">Произошла ошибка. Попробуйте позже...</div>';

                                                                dialog_index=page_object.dialog.init({
                                                                    'title':'Ошибка',
                                                                    'inner':inner,
                                                                    'ok':function(){

                                                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                                                    }
                                                                });

                                                            }
                                                            else{

                                                                if(isset($d('user_balance')))
                                                                    $d('user_balance').innerHTML=data['data']['balance'];

                                                                page_object.action.profile.action.root_face_block.action.payment.reset();

                                                                inner+='<div class="dialog_row">Ваш счет пополнен</div>';

                                                                dialog_index=page_object.dialog.init({
                                                                    'title':'Пополнение баланса',
                                                                    'inner':inner,
                                                                    'ok':function(){

                                                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                                                        if(isset($d('logo')))
                                                                            $d('logo').click();

                                                                    }
                                                                });

                                                            }

                                                            page_object.link.preload.un_show();

                                                        }
                                                    });

                                                });

                                            }
                                        }).render('#profile_payment_wallet_button');

                                        break;

                                    }

                                    default:{

                                        if(isset($d('profile_payment_wallet_from_block')))
                                            $s('profile_payment_wallet_from_block').display='block';

                                        if(isset($d('profile_payment_wallet_button')))
                                            $s('profile_payment_wallet_button').display='none';

                                        if(isset($d('profile_payment_label_type')))
                                            $d('profile_payment_label_type').innerHTML='Адрес вашего <span id="wallet_name">'+wallet_index+'</span> кошелька:';

                                        break;

                                    }

                                }

                                setTimeout(function(){

                                    page_object.action.profile.action.root_face_block.action.payment.sum.action.show();

                                },400);

                            },800);

                        }
                        else{

                            switch(wallet_index){

                                case'pp':
                                case'cc':{

                                    if(isset($d('profile_payment_sum_500')))
                                        $s('profile_payment_sum_500').display='block';

                                    if(isset($d('profile_payment_sum_10000')))
                                        $s('profile_payment_sum_10000').display='none';

                                    break;

                                }

                                default:{

                                    if(isset($d('profile_payment_sum_500')))
                                        $s('profile_payment_sum_500').display='none';

                                    if(isset($d('profile_payment_sum_10000')))
                                        $s('profile_payment_sum_10000').display='block';

                                    break;

                                }

                            }

                            switch(wallet_index){

                                case'sberbank':{

                                        if(isset($d('profile_payment_label_type')))
                                            $d('profile_payment_label_type').innerHTML='Введите последние 4 цифры КАРТЫ списания средств <span id="wallet_name">Сбербанка</span>:';
                                            // $d('profile_payment_label_type').innerHTML='Имя и отчество владельца счета <span id="wallet_name">Сбербанка</span>:';

                                    break;

                                }

                                case'qiwi':{

                                    if(isset($d('profile_payment_label_type')))
                                        $d('profile_payment_label_type').innerHTML='Номер вашего <span id="wallet_name">QIWI</span> кошелька:';

                                    break;

                                }

                                case'cc':
                                case'pp':{

                                    if(isset($d('profile_payment_sum_1000')))
                                        $d('profile_payment_sum_1000').click();

                                    if(isset($d('profile_payment_wallet_from_block')))
                                        $s('profile_payment_wallet_from_block').display='none';

                                    if(isset($d('profile_payment_wallet_button')))
                                        $s('profile_payment_wallet_button').display='block';

                                    var is_sandbox=false;

                                    if(isset($d('profile_payment_wallet_button')))
                                        $d('profile_payment_wallet_button').innerHTML='';

                                    paypal.Buttons({

                                        // 'env': 'sandbox',
                                        // 'disable-funding':'credit',
                                        'createOrder':function(data, actions) {

                                            // page_object.action.profile.action.root_face_block.action.payment.sum.index=1;

                                            return actions.order.create({
                                                purchase_units:[{
                                                    amount:{
                                                        value:page_object.action.profile.action.root_face_block.action.payment.sum.index
                                                    }
                                                }]
                                            });

                                        },
                                        'onCreate':function(){

                                            if(wallet_index==='cc')
                                                $d('profile_payment_wallet_button').getElementsByClassName('paypal-button')[0].style.display='none';

                                        },
                                        'onApprove':function(data, actions){

                                            page_object.link.preload.show();

                                            return actions.order.capture().then(function(details){

                                                // page_object.action.profile.action.root_face_block.action.payment.sum.index=1;

                                                var post='';

                                                post+='order_ID='+uniEncode(data.orderID);
                                                post+='&amount='+uniEncode(page_object.action.profile.action.root_face_block.action.payment.sum.index);
                                                post+='&is_sandbox='+uniEncode(is_sandbox?1:0);

                                                send({
                                                    'scriptPath':'/api/json/paypal_payment_check',
                                                    'postData':post,
                                                    'onComplete':function(j,worktime){

                                                        let  data
                                                            ,dataTemp;

                                                        dataTemp=j.responseText;
                                                        trace(dataTemp);
                                                        data=jsonDecode(dataTemp);
                                                        trace(data);
                                                        trace_worktime(worktime,data);

                                                        page_object.link.preload.un_show();

                                                        var  inner=''
                                                            ,dialog_index;

                                                        if(isset(data['error'])){

                                                            inner+='<div class="dialog_row" style="color: #ff0000;">Произошла ошибка. Попробуйте позже...</div>';

                                                            dialog_index=page_object.dialog.init({
                                                                'title':'Ошибка',
                                                                'inner':inner,
                                                                'ok':function(){

                                                                    page_object.dialog.action.un_show.init(dialog_index,true);

                                                                }
                                                            });

                                                        }
                                                        else{

                                                            if(isset($d('user_balance')))
                                                                $d('user_balance').innerHTML=data['data']['balance'];

                                                            page_object.action.profile.action.root_face_block.action.payment.reset();

                                                            inner+='<div class="dialog_row">Ваш счет пополнен</div>';

                                                            dialog_index=page_object.dialog.init({
                                                                'title':'Пополнение баланса',
                                                                'inner':inner,
                                                                'ok':function(){

                                                                    page_object.dialog.action.un_show.init(dialog_index,true);

                                                                    if(isset($d('logo')))
                                                                        $d('logo').click();

                                                                }
                                                            });

                                                        }

                                                        page_object.link.preload.un_show();

                                                    }
                                                });

                                            });

                                        }
                                    }).render('#profile_payment_wallet_button');

                                    break;

                                }

                                default:{

                                    if(isset($d('profile_payment_label_type')))
                                        $d('profile_payment_label_type').innerHTML='Адрес вашего <span id="wallet_name">'+wallet_index+'</span> кошелька:';

                                    break;

                                }

                            }

                            setTimeout(function(){

                                page_object.action.profile.action.root_face_block.action.payment.sum.action.show();

                            },400);

                        }

                    }

                    for(index in wallet_list)
                        if(isset($d('profile_payment_wallet_'+index))){

                            trace(wallet_index+' === '+index);

                            if(wallet_index===index){

                                if($d('profile_payment_wallet_'+wallet_index).getAttribute('class')==='profile_payment_wallet_item')
                                    $d('profile_payment_wallet_'+index).setAttribute('class','profile_payment_wallet_item_active');
                                else if($d('profile_payment_wallet_'+wallet_index).getAttribute('class')!=='profile_payment_wallet_item_disable')
                                    $d('profile_payment_wallet_'+index).setAttribute('class','profile_payment_wallet_item');

                            }
                            else if($d('profile_payment_wallet_'+wallet_index).getAttribute('class')!=='profile_payment_wallet_item_disable')
                                if($d('profile_payment_wallet_'+index).getAttribute('class')!=='profile_payment_wallet_item_disable')
                                    $d('profile_payment_wallet_'+index).setAttribute('class','profile_payment_wallet_item');

                        }

                }
            },
            'sum':{
                'is_open':false,
                'index':null,
                'init':function(){

                    page_object.action.profile.action.root_face_block.action.payment.sum.index=null;

                    page_object.action.profile.action.root_face_block.action.payment.sum.price.init();
                    page_object.action.profile.action.root_face_block.action.payment.sum.address.init();

                },
                'price':{
                    'init':function(){

                        let  list       =$d('profile_payment_sum_list').getElementsByTagName('div')
                            ,index;

                        for(index in list)
                            if(isObject(list[index]))
                                list[index].onclick=page_object.action.profile.action.root_face_block.action.payment.sum.price.check;

                    },
                    'check':function(){

                        var  list               =$d('profile_payment_sum_list').getElementsByTagName('div')
                            ,index
                            ,price              =this.id.split('_')[3]
                            ,data               =page_object.action.profile.data['data']
                            ,currency_list      =data['currency_list']
                            ,wallet_list        =data['wallet_list']
                            ,wallet_index       =page_object.action.profile.action.root_face_block.action.payment.wallet.index
                            ,wallet_currency    =currency_list[wallet_index]
                            ,wallet_name        =wallet_list[wallet_index];

                        page_object.action.profile.action.root_face_block.action.payment.sum.index=price;

                        if($d('profile_payment_sum_'+price).getAttribute('class')==='profile_payment_sum_item_active'){

                            if(wallet_index!=='pp'){

                                page_object.action.profile.action.root_face_block.action.payment.sum.index=null;

                                page_object.action.profile.action.root_face_block.action.payment.total.action.un_show();

                            }

                        }
                        else{

                            if(page_object.action.profile.action.root_face_block.action.payment.total.is_open){

                                if(isset($d('wallet_from_text')))
                                    $d('wallet_from_text').innerHTML=$v('profile_payment_wallet_from_input');

                                if(isset($d('wallet_to_text')))
                                    $d('wallet_to_text').innerHTML=(wallet_index==='sberbank')?get_number_with_space_to_4(wallet_name):wallet_name;

                                if(isset($d('wallet_sum_total'))){

                                    switch(wallet_index){

                                        case'qiwi':
                                        case'sberbank':{

                                            $d('wallet_sum_total').innerHTML=price+' RUB';

                                            break;

                                        }

                                        default:{

                                            $d('wallet_sum_total').innerHTML=(wallet_currency*parseInt(price)).toFixed(5)+' '+(wallet_index).toUpperCase();

                                            break;

                                        }

                                    }

                                }

                                if(page_object.action.profile.action.root_face_block.action.payment.sum.address.is_valid)
                                    page_object.action.profile.action.root_face_block.action.payment.total.action.show();

                            }
                            else{

                                if(isset($d('wallet_from_text')))
                                    $d('wallet_from_text').innerHTML=$v('profile_payment_wallet_from_input');

                                if(isset($d('wallet_to_text')))
                                    $d('wallet_to_text').innerHTML=(wallet_index==='sberbank')?get_number_with_space_to_4(wallet_name):wallet_name;

                                if(isset($d('wallet_sum_total'))){

                                    switch(wallet_index){

                                        case'qiwi':
                                        case'sberbank':{

                                            $d('wallet_sum_total').innerHTML=price+' RUB';

                                            break;

                                        }

                                        default:{

                                            $d('wallet_sum_total').innerHTML=(wallet_currency*parseInt(price)).toFixed(5)+' '+(wallet_index).toUpperCase();

                                            break;

                                        }

                                    }

                                }

                                if(page_object.action.profile.action.root_face_block.action.payment.sum.address.is_valid)
                                    page_object.action.profile.action.root_face_block.action.payment.total.action.show();

                            }

                        }

                        for(index in list)
                            if(isObject(list[index])){

                                if(
                                      list[index].getAttribute('class')==='profile_payment_sum_item'
                                    &&list[index].id==='profile_payment_sum_'+price
                                )
                                    list[index].setAttribute('class','profile_payment_sum_item_active');
                                else if(wallet_index==='pp'&&list[index].id==='profile_payment_sum_'+price)
                                    list[index].setAttribute('class','profile_payment_sum_item_active');
                                else
                                    list[index].setAttribute('class','profile_payment_sum_item');

                            }

                        if(wallet_index!=='pp')
                            $d('profile_payment_sum_'+price).setAttribute('class','profile_payment_sum_item_active');

                    }
                },
                'address':{
                    'is_valid':false,
                    'init':function(){

                        page_object.action.profile.action.root_face_block.action.payment.sum.address.is_valid=false;

                        if(isset($d('profile_payment_wallet_from_input'))){

                            $d('profile_payment_wallet_from_input').onkeyup     =page_object.action.profile.action.root_face_block.action.payment.sum.address.action;
                            $d('profile_payment_wallet_from_input').onkeydown   =page_object.action.profile.action.root_face_block.action.payment.sum.address.action_down;

                        }

                    },
                    'action_down':function(e){

                        let wallet_index   =page_object.action.profile.action.root_face_block.action.payment.wallet.index;

                        switch(wallet_index){

                            case'sberbank':{

                                e=e||window.event;

                                let symb=getCharFromEventKey(event);

                                console.log(symb.search(/^[0-9]$/g));

                                if(symb!==null)
                                    if(symb.search(/^[0-9]$/g)!==0)
                                        return event.preventDefault();

                                break;

                            }

                            // case'sberbank':{
                            //
                            //     e=e||window.event;
                            //
                            //     let symb=getCharFromEventKey(event);
                            //
                            //     if(symb!==null)
                            //         if(symb.search(/^[A-Za-zА-Я\s*Ёа-яё\-]*$/g)!==0)
                            //             return event.preventDefault();
                            //
                            //     break;
                            //
                            // }

                            case'qiwi':{

                                e=e||window.event;

                                let symb=getCharFromEventKey(event);

                                if(symb!==null)
                                    if(symb.search(/^[0-9]*$/g)!==0)
                                        return event.preventDefault();

                                break;

                            }

                            default:
                                return null;

                        }

                    },
                    'action':function(){

                        var  val            =this.value
                            ,len            =val.length
                            ,symbol         =val.substr(0,1)
                            ,is_error       =false
                            ,wallet_index   =page_object.action.profile.action.root_face_block.action.payment.wallet.index;

                        if(wallet_index!=='sberbank'){

                            if(isset($d('profile_payment_label_please')))
                                $d('profile_payment_label_please').innerHTML='Пожалуйста, переведите с вашего кошелька:';

                            if(isset($d('profile_payment_label_service')))
                                $d('profile_payment_label_service').innerHTML='На кошелек сервиса Shluham.NET:';

                        }

                        switch(wallet_index){

                            case'sberbank':{

                                // console.log(val.match(/^[0-9]{4}$/i));

                                if(val.match(/^[0-9]{4}$/i)!==null){

                                    if(len===4)
                                        $s('profile_payment_wallet_from_input').borderColor='#158400';
                                    else{

                                        is_error=true;

                                        $s('profile_payment_wallet_from_input').borderColor='';

                                    }

                                }
                                else{

                                    is_error=true;

                                    $s('profile_payment_wallet_from_input').borderColor='#ff0000';

                                }

                                if(isset($d('profile_payment_label_please')))
                                    $d('profile_payment_label_please').innerHTML='Пожалуйста, переведите со счета Сбербанка, оканчивающегося на:';

                                if(isset($d('profile_payment_label_service')))
                                    $d('profile_payment_label_service').innerHTML='На карту Сбербанка сервиса Shluham.net:';

                                break;

                            }

                            case'btc':{

                                if(
                                      len>=26
                                    &&len<=35
                                    &&(
                                          symbol==='1'
                                        ||symbol==='3'
                                    )
                                )
                                    $s('profile_payment_wallet_from_input').borderColor='#158400';
                                else{

                                    is_error=true;

                                    $s('profile_payment_wallet_from_input').borderColor='#ff0000';

                                }

                                break;

                            }

                            case'eth':{

                                if(
                                      len>=40
                                    &&len<=44
                                    &&val.substr(0,2)==='0x'
                                )
                                    $s('profile_payment_wallet_from_input').borderColor='#158400';
                                else{

                                    is_error=true;

                                    $s('profile_payment_wallet_from_input').borderColor='#ff0000';

                                }

                                break;

                            }

                            case'qiwi':{

                                if(isset($d('profile_payment_label_please')))
                                    $d('profile_payment_label_please').innerHTML='Пожалуйста, переведите с вашего QIWI кошелька:';

                                if(isset($d('profile_payment_label_service')))
                                    $d('profile_payment_label_service').innerHTML='На QIWI кошелек сервиса Shluham.NET:';

                                if(len>=10)
                                    $s('profile_payment_wallet_from_input').borderColor='#158400';
                                else{

                                    is_error=true;

                                    $s('profile_payment_wallet_from_input').borderColor='#ff0000';

                                }

                                break;

                            }

                        }

                        if(is_error){

                            $s('wallet_from_text').color            ='#ff0000';
                            $d('wallet_from_text').innerHTML        =(wallet_index==='sberbank')?'Некорректный номер':'Неверный адрес кошелька';

                            if(isset($d('profile_payment_submit')))
                                $d('profile_payment_submit').setAttribute('class','profile_payment_button_disable');

                            page_object.action.profile.action.root_face_block.action.payment.sum.address.is_valid=false;

                        }
                        else{

                            $s('wallet_from_text').color            ='';

                            switch(wallet_index){

                                case'sberbank':{

                                    $d('wallet_from_text').innerHTML='...'+val;

                                    break;

                                }

                                default:{

                                    $d('wallet_from_text').innerHTML=val;

                                    break;

                                }

                            }

                            if(isset($d('profile_payment_submit')))
                                $d('profile_payment_submit').setAttribute('class','profile_payment_button');

                            page_object.action.profile.action.root_face_block.action.payment.sum.address.is_valid=true;

                            if(
                                  !page_object.action.profile.action.root_face_block.action.payment.total.is_open
                                &&!empty(page_object.action.profile.action.root_face_block.action.payment.sum.index)
                            )
                                page_object.action.profile.action.root_face_block.action.payment.total.action.show();

                        }

                    }
                },
                'reset':function(){

                    if(isset($d('profile_payment_sum_list'))){

                        let  list       =$d('profile_payment_sum_list').getElementsByTagName('div')
                            ,index;

                        for(index in list)
                            if(isObject(list[index]))
                                list[index].setAttribute('class','profile_payment_sum_item');

                        page_object.action.profile.action.root_face_block.action.payment.sum.index              =null;
                        page_object.action.profile.action.root_face_block.action.payment.sum.address.is_valid   =false;

                        if(isset($d('profile_payment_wallet_from_input'))){

                            $s('profile_payment_wallet_from_input').borderColor     ='';
                            $d('profile_payment_wallet_from_input').value           ='';

                        }

                    }

                },
                'action':{
                    'show':function(){

                        if(isset($d('root_face_block'))){

                            page_object.action.profile.action.root_face_block.action.payment.sum.is_open=true;

                            if(!OS.isMobile)
                                $s('root_face_block').width='910px';
                            else
                                $s('root_default_content').height='calc(100vh - 195px)';

                            setTimeout(function(){

                                $s('profile_payment_step_2').display='block';

                            },300);

                            setTimeout(function(){

                                $s('profile_payment_step_2').opacity=1;

                                if(OS.isMobile)
                                    $s('profile_payment_step_2').height='auto';

                            },340);

                            if(OS.isMobile)
                                setTimeout(function(){

                                    $d('root_default_content').scrollTo({
                                        top: 450,
                                        behavior: "smooth"
                                    });

                                    // $d('root_default_content').scrollTop=340;

                                },640);

                        }

                    },
                    'un_show':function(){

                        if(isset($d('root_face_block'))){

                            page_object.action.profile.action.root_face_block.action.payment.sum.is_open        =false;
                            page_object.action.profile.action.root_face_block.action.payment.total.is_open      =false;

                            page_object.action.profile.action.root_face_block.action.payment.sum.index          =false;

                            if(OS.isMobile){

                                $d('root_default_content').scrollTo({
                                    top: 0,
                                    behavior: "smooth"
                                });

                                $s('root_default_content').height='';

                                setTimeout(function(){

                                    $s('profile_payment_step_2').height    =0;
                                    $s('profile_payment_step_3').height    =0;

                                },300);

                            }

                            $s('profile_payment_step_2').opacity    =0;
                            $s('profile_payment_step_3').opacity    =0;

                            setTimeout(function(){

                                if(!OS.isMobile)
                                    $s('root_face_block').width='430px';

                                $s('profile_payment_step_2').display    ='none';
                                $s('profile_payment_step_3').display    ='none';

                                page_object.action.profile.action.root_face_block.action.payment.sum.reset();

                            },600);

                        }

                    }
                }
            },
            'total':{
                'is_open':false,
                'init':function(){},
                'reset':function(){

                },
                'action':{
                    'show':function(){

                        if(isset($d('root_face_block'))){

                            page_object.action.profile.action.root_face_block.action.payment.total.is_open=true;

                            if(!OS.isMobile)
                                $s('root_face_block').width='1350px';

                            setTimeout(function(){

                                $s('profile_payment_step_3').display='block';

                            },300);

                            setTimeout(function(){

                                $s('profile_payment_step_3').opacity=1;

                                if(OS.isMobile)
                                    $s('profile_payment_step_3').height='auto';

                            },340);

                            if(OS.isMobile)
                                setTimeout(function(){

                                    $d('root_default_content').scrollTo({
                                        top: 800,
                                        behavior: "smooth"
                                    });

                                },640);

                        }

                    },
                    'un_show':function(){

                        if(isset($d('root_face_block'))){

                            page_object.action.profile.action.root_face_block.action.payment.total.is_open=false;

                            var s_top=$d('root_default_content').scrollHeight-elementSize.height($d('root_default_content'))-elementSize.height($d('profile_payment_step_3'));

                            if(s_top<0)
                                s_top=0;

                            $d('root_default_content').scrollTo({
                                top: s_top,
                                behavior: "smooth"
                            });

                            $s('profile_payment_step_3').opacity=0;

                            setTimeout(function(){

                                if(!OS.isMobile)
                                    $s('root_face_block').width='910px';

                                if(OS.isMobile){

                                    $s('profile_payment_step_3').height=0;

                                    setTimeout(function(){

                                        $s('profile_payment_step_3').display='none';

                                    },300);

                                }
                                else
                                    $s('profile_payment_step_3').display='none';

                                page_object.action.profile.action.root_face_block.action.payment.total.reset();

                            },500);

                        }

                    }
                }
            },
            'copy_to_buffer':{
                'init':function(){

                    if(isset($d('wallet_to_copy_buffer')))
                        $d('wallet_to_copy_buffer').onclick=page_object.action.profile.action.root_face_block.action.payment.copy_to_buffer.click;

                },
                'is_work':false,
                'click':function(){

                    if(page_object.action.profile.action.root_face_block.action.payment.copy_to_buffer.is_work)
                        return false;

                    if(copy_to_buffer('wallet_to_text')){

                        page_object.action.profile.action.root_face_block.action.payment.copy_to_buffer.is_work=true;

                        $s('wallet_to_text_info').display='block';

                        setTimeout(function(){

                            $s('wallet_to_text_info').opacity=1;

                            setTimeout(function(){

                                $s('wallet_to_text_info').opacity=0;

                                setTimeout(function(){

                                    $s('wallet_to_text_info').display='none';

                                    page_object.action.profile.action.root_face_block.action.payment.copy_to_buffer.is_work=false;

                                },300);

                            },3000);

                        },40);

                    }

                }
            },
            'submit':{
                'init':function(){

                    if(isset($d('profile_payment_submit')))
                        $d('profile_payment_submit').onclick=page_object.action.profile.action.root_face_block.action.payment.submit.send;

                },
                'send':function(){

                    page_object.link.preload.show();

                    var  data               =page_object.action.profile.data['data']
                        ,wallet_list        =data['wallet_list']
                        ,wallet_index       =page_object.action.profile.action.root_face_block.action.payment.wallet.index
                        ,sum_index          =page_object.action.profile.action.root_face_block.action.payment.sum.index
                        ,target_wallet      =wallet_list[wallet_index]
                        ,user_wallet        =$v('profile_payment_wallet_from_input')
                        ,post               ='';

                    post+='&wallet_name='+uniEncode(wallet_index);
                    post+='&user_wallet_address='+uniEncode(user_wallet);
                    post+='&target_wallet_address='+uniEncode(target_wallet);
                    post+='&transaction_sum_rub='+uniEncode(sum_index);

                    send({
                        'scriptPath':'/api/json/add_payment',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  data
                                ,dataTemp;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            var  inner=''
                                ,dialog_index;

                            if(isset(data['error'])){

                                inner+='<div class="dialog_row" style="color: #ff0000;">Произошла ошибка. Попробуйте позже...</div>';

                                dialog_index=page_object.dialog.init({
                                    'title':'Ошибка',
                                    'inner':inner,
                                    'ok':function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    }
                                });

                            }
                            else{

                                page_object.action.profile.action.root_face_block.action.payment.reset();

                                inner+='<div class="dialog_row">Заявка на пополнение счета принята. После поступления и зачисления средств Вы получите уведомление на e-mail</div>';

                                dialog_index=page_object.dialog.init({
                                    'title':'Пополнение баланса',
                                    'inner':inner,
                                    'ok':function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                        if(isset($d('logo')))
                                            $d('logo').click();

                                    }
                                });

                            }

                            page_object.link.preload.un_show();

                        }
                    });

                }
            }
        },
        'billing':{
            'init':function(){

                if(isset($d('settings_close_button')))
                    $d('settings_close_button').onclick=page_object.action.profile.action.root_face_block.action.billing.un_show;

            },
            'un_show':function(){

                if(isset($d('logo')))
                    $d('logo').click();

            },
        },
        'feedback':{
            'dialog_index':null,
            'init':function(){

                if(isset($d('dialog_email_input_text')))
                    $d('dialog_email_input_text').onkeyup=page_object.action.profile.action.root_face_block.action.feedback.input.init;

                if(isset($d('dialog_content_input_text')))
                    $d('dialog_content_input_text').onkeyup=page_object.action.profile.action.root_face_block.action.feedback.input.init;

            },
            'input':{
                'init':function(){

                    switch(this.id){

                        case'dialog_email_input_text':{

                            if(empty(this.value))
                                this.style.borderColor='#ff0000';
                            else if(!isEmail(this.value))
                                this.style.borderColor='#ff0000';
                            else
                                this.style.borderColor='';

                            break;

                        }

                        case'dialog_content_input_text':{

                            if(empty(this.value))
                                this.style.borderColor='#ff0000';
                            else
                                this.style.borderColor='';

                            break;

                        }

                    }

                }
            },
            'send':function(dialog_index){

                var lang_obj=page_object.content[page_object.lang];

                if(isset($d('dialog_submit_send_'+dialog_index))){

                    $d('dialog_submit_send_'+dialog_index).setAttribute('class','dialog_submit_disable');

                    $d('dialog_submit_send_'+dialog_index).innerHTML    ='Подождите';
                    $d('dialog_submit_send_'+dialog_index).onclick      =function(){};

                }

                if(isset($d('dialog_submit_cancel_'+dialog_index))){

                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                }

                re_captcha.check('feedback',function(token){

                    var post='';

                    post+='&email='+uniEncode($v('dialog_email_input_text').toLowerCase());
                    post+='&content='+uniEncode($v('dialog_content_input_text'));
                    post+='&re_captcha_token='+uniEncode(token);

                    send({
                        'scriptPath':'/api/json/feedback',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            var  data
                                ,dataTemp;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            page_object.dialog.action.un_show.init(page_object.action.profile.action.root_face_block.action.feedback.dialog_index,true);

                            if(!isset(data['error'])){

                                var inner='';

                                inner+='<div class="dialog_row" style="text-align: center">Ваше сообщение отправлено</div>';

                                var dialog_index=page_object.dialog.init({
                                    'title':lang_obj['forgot_title'],
                                    'inner':inner,
                                    'ok':function(){

                                        var lang_obj=page_object.content[page_object.lang];

                                        setUrl(lang_obj['title']+' | '+lang_obj['loading'],'/profile/'+page_object.action.profile.data['user']['login']+'/face');

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    }
                                });

                            }

                        }
                    });

                });

            }
        },
        'city':{
            'init':function(){

                if(isset($d('settings_close_button')))
                    $d('settings_close_button').onclick=page_object.action.profile.action.root_face_block.action.search_history.un_show;

            }
        }
    },
    'remove':function(){

        if(isset($d('root_face_block')))
            removeElement($d('root_face_block'));

    },
    'resize':function(){}
};