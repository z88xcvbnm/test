page_object.action.profile.action.settings={
    'show':function(){

        if(isset($d('settings'))){

            page_object.action.profile.position.settings.o=1;

            $s('settings').opacity=page_object.action.profile.position.settings.o;

            if(isset($d('header_profile')))
                $d('header_profile').setAttribute('class','header_profile_active');

            page_object.action.profile.action.settings.resize();

            setTimeout(page_object.link.preload.un_show,300);

        }

    },
    'un_show':function(remove){

        if(isset($d('settings'))){

            page_object.action.profile.position.settings.o        =0;
            page_object.action.profile.position.settings.h        =0;

            $s('settings').height           =page_object.action.profile.position.settings.h+'px';
            $s('settings').opacity          =page_object.action.profile.position.settings.o+'px';

            if(isset($d('header_profile')))
                $d('header_profile').setAttribute('class','header_profile');

            if(isset(remove))
                if(remove)
                    setTimeout(page_object.action.profile.action.settings.remove,300);

        }

    },
    'remove':function(){

        if(isset($d('settings')))
            removeElement($d('settings'));

    },
    'click':{
        'edit':{
            'error':false,
            'input':{
                'list':{
                    'name':[2,32],
                    'surname':[2,32],
                    'email':[6,64],
                    'login':[2,32],
                    'pas1':[6,32],
                    'pas2':[6,32],
                    'pasold':[6,32]
                },
                'init':function(){

                    let  id_list            =this.id.split('_')
                        ,key                =id_list[1]
                        ,list               =page_object.action.profile.action.settings.click.edit.input.list
                        ,len_min            =list[key][0]
                        ,len_max            =list[key][1]
                        ,val                =this.value
                        ,val_len            =val.length
                        ,error              =false;

                    switch(key){

                        case'pasold':
                        case'pas1':
                        case'pas2':{

                            let  pasold_len     =$v('dialog_pasold_input_text').length
                                ,pas1_len       =$v('dialog_pas1_input_text').length
                                ,pas2_len       =$v('dialog_pas2_input_text').length;

                            if(pasold_len>=len_min&&pasold_len<=len_max){

                                $s('dialog_pasold_input_text').border='1px solid #74b65f';

                            }
                            else{

                                $s('dialog_pasold_input_text').border='1px solid #ff001d';

                                error=true;

                            }

                            if(pas1_len>=len_min&&pas1_len<=len_max&&pas2_len>=len_min&&pas2_len<=len_max){

                                if($v('dialog_pas1_input_text')===$v('dialog_pas2_input_text')){

                                    $s('dialog_pas1_input_text').border='1px solid #74b65f';
                                    $s('dialog_pas2_input_text').border='1px solid #74b65f';

                                }
                                else{

                                    $s('dialog_pas1_input_text').border='1px solid #ff001d';
                                    $s('dialog_pas2_input_text').border='1px solid #ff001d';

                                    error=true;

                                }

                            }
                            else{

                                if(pas1_len>=len_min&&pas1_len<=len_max)
                                    $s('dialog_pas1_input_text').border='1px solid #74b65f';
                                else{

                                    $s('dialog_pas1_input_text').border='1px solid #ff001d';

                                    error=true;

                                }

                                if(pas2_len>=len_min&&pas2_len<=len_max)
                                    $s('dialog_pas2_input_text').border='1px solid #74b65f';
                                else{

                                    $s('dialog_pas2_input_text').border='1px solid #ff001d';

                                    error=true;

                                }

                            }

                            page_object.action.profile.action.settings.click.edit.error=error;

                            break;

                        }

                        case'name':
                        case'surname':{

                            let  name_len       =$v('dialog_name_input_text').length
                                ,surname_len    =$v('dialog_surname_input_text').length;

                            if(name_len>=len_min&&name_len<=len_max)
                                $s('dialog_name_input_text').border='1px solid #74b65f';
                            else{

                                $s('dialog_name_input_text').border='1px solid #ff001d';

                                error=true;

                            }

                            if(surname_len>=len_min&&surname_len<=len_max)
                                $s('dialog_surname_input_text').border='1px solid #74b65f';
                            else{

                                $s('dialog_surname_input_text').border='1px solid #ff001d';

                                error=true;

                            }

                            page_object.action.profile.action.settings.click.edit.error=error;

                            break;

                        }

                        case'email':{

                            let email_len=$v('dialog_email_input_text').length;

                            if(email_len>=len_min&&email_len<=len_max&&isEmail($v('dialog_email_input_text')))
                                $s('dialog_email_input_text').border='1px solid #74b65f';
                            else{

                                $s('dialog_email_input_text').border='1px solid #ff001d';

                                error=true;

                            }

                            page_object.action.profile.action.settings.click.edit.error=error;

                            break;

                        }

                        case'login':{

                            let login_len=$v('dialog_login_input_text').length;

                            if(login_len>=len_min&&login_len<=len_max&&isLogin($v('dialog_login_input_text')))
                                $s('dialog_login_input_text').border='1px solid #74b65f';
                            else{

                                $s('dialog_login_input_text').border='1px solid #ff001d';

                                error=true;

                            }

                            page_object.action.profile.action.settings.click.edit.error=error;

                            break;

                        }

                        default:{

                            if(val_len>=len_min&&val_len<=len_max)
                                this.style.border='1px solid #74b65f';
                            else{

                                this.style.border='1px solid #ff001d';

                                error=true;

                            }

                            page_object.action.profile.action.settings.click.edit.error=error;

                            break;

                        }

                    }

                }
            },
            'name':{
                'init':function(){

                    let  lang_obj       =page_object.action.profile.content[page_object.lang]
                        ,inner          =''
                        ,data           =page_object.action.profile.data['settings']
                        ,name           =data['name']
                        ,surname        =data['surname'];

                    page_object.action.profile.action.settings.click.edit.error=false;

                    inner+='<div id="dialog_name_block" class="dialog_row">';
                        inner+='<div id="dialog_name_label" class="dialog_row_label">';
                            inner+='<span>'+stripSlashes(lang_obj['dialog_settings_name'])+'</span>';
                        inner+='</div>';
                        inner+='<div id="dialog_name_input_block" class="dialog_row_input">';
                            inner+='<input type="text" id="dialog_name_input_text" class="dialog_row_input_text" value="'+stripSlashes(name)+'" />';
                        inner+='</div>';
                    inner+='</div>';

                    inner+='<div id="dialog_surname_block" class="dialog_row">';
                        inner+='<div id="dialog_surname_label" class="dialog_row_label">';
                            inner+='<span>'+stripSlashes(lang_obj['dialog_settings_surname'])+'</span>';
                        inner+='</div>';
                        inner+='<div id="dialog_surname_input_block" class="dialog_row_input">';
                            inner+='<input type="text" id="dialog_surname_input_text" class="dialog_row_input_text" value="'+stripSlashes(surname)+'" />';
                        inner+='</div>';
                    inner+='</div>';

                    var dialog_index=page_object.dialog.init({
                        'title':lang_obj['dialog_settings_name_title'],
                        'inner':inner,
                        'save':function(){

                            page_object.action.profile.action.settings.click.save.name.init(dialog_index);

                        },
                        'cancel':true,
                        'on_create':function(){

                            $d('dialog_name_input_text').onkeydown      =page_object.action.profile.action.settings.click.edit.input.init;
                            $d('dialog_name_input_text').onkeyup        =page_object.action.profile.action.settings.click.edit.input.init;

                            $d('dialog_surname_input_text').onkeydown   =page_object.action.profile.action.settings.click.edit.input.init;
                            $d('dialog_surname_input_text').onkeyup     =page_object.action.profile.action.settings.click.edit.input.init;

                            //if(data['name']!='')
                            //    $d('dialog_name_input_text').keydown();
                            //
                            //if(data['surname']!='')
                            //    $d('dialog_surname_input_text').keydown();

                        }
                    });

                }
            },
            'email':{
                'init':function(){

                    let  lang_obj       =page_object.action.profile.content[page_object.lang]
                        ,inner          =''
                        ,data           =page_object.action.profile.data['settings']
                        ,email          =data['email'];

                    page_object.action.profile.action.settings.click.edit.error=false;

                    inner+='<div id="dialog_email_block" class="dialog_row">';
                    inner+='<div id="dialog_email_label" class="dialog_row_label">';
                    inner+='<span>'+stripSlashes(lang_obj['dialog_settings_email'])+'</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_email_input_block" class="dialog_row_input">';
                    inner+='<input type="text" id="dialog_email_input_text" class="dialog_row_input_text" value="'+stripSlashes(email)+'" />';
                    inner+='</div>';
                    inner+='</div>';

                    var dialog_index=page_object.dialog.init({
                        'title':lang_obj['dialog_settings_email_title'],
                        'inner':inner,
                        'save':function(){

                            page_object.action.profile.action.settings.click.save.email.init(dialog_index);

                        },
                        'cancel':true,
                        'on_create':function(){

                            $d('dialog_email_input_text').onkeydown      =page_object.action.profile.action.settings.click.edit.input.init;
                            $d('dialog_email_input_text').onkeyup        =page_object.action.profile.action.settings.click.edit.input.init;

                            //if(data['email']!='')
                            //    $d('dialog_email_input_text').keydown();

                        }
                    });
                
                }
            },
            'login':{
                'init':function(){

                    let  lang_obj       =page_object.action.profile.content[page_object.lang]
                        ,inner          =''
                        ,data           =page_object.action.profile.data
                        ,login          =data['user']['login'];

                    page_object.action.profile.action.settings.click.edit.error=false;

                    inner+='<div id="dialog_login_block" class="dialog_row">';
                    inner+='<div id="dialog_login_label" class="dialog_row_label">';
                    inner+='<span>'+stripSlashes(lang_obj['dialog_settings_login'])+'</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_login_input_block" class="dialog_row_input">';
                    inner+='<input type="text" id="dialog_login_input_text" class="dialog_row_input_text" value="'+stripSlashes(login)+'" />';
                    inner+='</div>';
                    inner+='</div>';

                    var dialog_index=page_object.dialog.init({
                        'title':lang_obj['dialog_settings_login_title'],
                        'inner':inner,
                        'save':function(){

                            page_object.action.profile.action.settings.click.save.login.init(dialog_index);

                        },
                        'cancel':true,
                        'on_create':function(){

                            $d('dialog_login_input_text').onkeydown      =page_object.action.profile.action.settings.click.edit.input.init;
                            $d('dialog_login_input_text').onkeyup        =page_object.action.profile.action.settings.click.edit.input.init;

                            //if(data['login']!='')
                            //    $d('dialog_login_input_text').keydown();

                        }
                    });
                
                }
            },
            'password':{
                'init':function(){

                    let  lang_obj       =page_object.action.profile.content[page_object.lang]
                        ,inner          ='';

                    page_object.action.profile.action.settings.click.edit.error=false;

                    inner+='<div id="dialog_pasold_block" class="dialog_row">';
                    inner+='<div id="dialog_pasold_label" class="dialog_row_label"'+(OS.isMobile?'':''+(OS.isMobile?'':' style="width: 240px;"')+'')+'>';
                    inner+='<span>'+stripSlashes(lang_obj['dialog_settings_pas_old'])+'</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_pasold_input_block" class="dialog_row_input">';
                    inner+='<input type="password" id="dialog_pasold_input_text" class="dialog_row_input_text" value="" />';
                    inner+='</div>';
                    inner+='</div>';

                    inner+='<div id="dialog_pas1_block" class="dialog_row">';
                    inner+='<div id="dialog_pas1_label" class="dialog_row_label"'+(OS.isMobile?'':' style="width: 240px;"')+'>';
                    inner+='<span>'+stripSlashes(lang_obj['dialog_settings_pas'])+'</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_pas1_input_block" class="dialog_row_input">';
                    inner+='<input type="password" id="dialog_pas1_input_text" class="dialog_row_input_text" value="" />';
                    inner+='</div>';
                    inner+='</div>';

                    inner+='<div id="dialog_pas2_block" class="dialog_row">';
                    inner+='<div id="dialog_pas2_label" class="dialog_row_label"'+(OS.isMobile?'':' style="width: 240px;"')+'>';
                    inner+='<span>'+stripSlashes(lang_obj['dialog_settings_pas_repeat'])+'</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_pas2_input_block" class="dialog_row_input">';
                    inner+='<input type="password" id="dialog_pas2_input_text" class="dialog_row_input_text" value="" />';
                    inner+='</div>';
                    inner+='</div>';

                    var dialog_index=page_object.dialog.init({
                        'title':lang_obj['dialog_settings_pas_title'],
                        'inner':inner,
                        'w':600,
                        'save':function(){

                            page_object.action.profile.action.settings.click.save.password.init(dialog_index);

                        },
                        'cancel':true,
                        'on_create':function(){

                            $d('dialog_pasold_input_text').onkeydown      =page_object.action.profile.action.settings.click.edit.input.init;
                            $d('dialog_pasold_input_text').onkeyup        =page_object.action.profile.action.settings.click.edit.input.init;

                            $d('dialog_pas1_input_text').onkeydown      =page_object.action.profile.action.settings.click.edit.input.init;
                            $d('dialog_pas1_input_text').onkeyup        =page_object.action.profile.action.settings.click.edit.input.init;

                            $d('dialog_pas2_input_text').onkeydown      =page_object.action.profile.action.settings.click.edit.input.init;
                            $d('dialog_pas2_input_text').onkeyup        =page_object.action.profile.action.settings.click.edit.input.init;

                        }
                    });
                
                }
            },
            'image_add':{
                'init':function(){

                    let  data                   =page_object.action.profile.data
                        ,upload_list            ={
                            'list':[]
                        }
                        ,inner                  =''
                        ,image_link             =''
                        ,lang_obj               =page_object.action.profile.content[page_object.lang];

                    page_object.upload.reset();

                    data=data['user'];

                    inner+='<div id="dialog_file_list" class="dialog_row_none">';

                    if(isset(data['image_ID']))
                        if(data['image_ID']!==0){

                            upload_list['list'].push({
                                'file_ID':data['image_ID'],
                                'image_ID':data['image_ID'],
                                'image_dir':data['image_dir'],
                                'image_item_ID_list':data['image_item_ID_list'],
                                'order':0,
                                'remove':false,
                                'complete':true
                            });

                            image_link='/'+data['image_dir']+'/'+data['image_item_ID_list']['preview']['ID'];

                            inner+='<div id="dialog_file_item_0" class="dialog_file_item">';

                            inner+='<div id="dialog_file_item_image_0" class="dialog_file_item_image" style="background: url('+image_link+') 50% 50% no-repeat; background-size: contain; height: 100px; "></div>';

                            inner+='<div id="dialog_file_item_progress_0" class="dialog_file_item_progress" style="height: 0; opacity: 0; ">';
                            inner+='<div id="dialog_file_item_progress_line_0" class="dialog_file_item_progress_line"></div>';
                            inner+='<div id="dialog_file_item_progress_percent_0" class="dialog_file_item_progress_percent">0 %</div>';
                            inner+='</div>';

                            inner+='<div id="dialog_file_item_controls_0" class="dialog_file_item_controls">';
                            inner+='<div id="dialog_file_item_remove_0" class="dialog_file_item_remove"></div>';
                            inner+='</div>';

                            inner+='</div>';

                        }

                    page_object.upload.data=upload_list;

                    inner+='</div>';

                    inner+='<div id="dialog_change_controls" class="dialog_row"'+(OS.isMobile?'':' style="height: 29px; "')+'>';
                    inner+='<div id="dialog_submit_image_add" class="dialog_submit_add"'+(OS.isMobile?'':' style="width: 396px; "')+'>'+stripSlashes((data['image_ID']===0)?'Загрузить изобаржение':'Загрузить другое изображение')+'</div>';
                    inner+='<input id="dialog_file_change" type="file" value="" accept="image/*" style="display: none;"/>';
                    inner+='</div>';

                    var dialog_index=page_object.dialog.init({
                        'title':'Изменение аватара пользователя',
                        'inner':inner,
                        'save':function(){

                            page_object.action.profile.action.settings.click.edit.image_add.add(dialog_index);

                        },
                        'cancel':true,
                        'on_create':function(){

                            page_object.action.profile.action.settings.click.edit.image_add.resize(dialog_index);
                            page_object.action.profile.action.settings.click.edit.image_add.on_change.set_action();

                            if(data['image_ID']===0)
                                page_object.upload.controls.disable(dialog_index);

                            $d('dialog_submit_image_add').onclick=function(){

                                $d('dialog_file_change').click();

                            };

                            $d('dialog_file_change').onchange=page_object.action.profile.action.settings.click.edit.image_add.on_change_file;

                        }
                    });

                },
                'on_change_file':function(dialog_index){

                    let  data
                        ,list           =[]
                        ,i
                        ,file_list      =this.files
                        ,list_item      ={};

                    for(i=0;i<file_list.length;i++){

                        list_item={
                            'file_ID':null,
                            'remove':false,
                            'file':file_list[i]
                        };

                        list.push(list_item);

                    }

                    data={
                        'on_change':page_object.action.profile.action.settings.click.edit.image_add.on_change.init,
                        'on_progress':page_object.action.profile.action.settings.click.edit.image_add.on_progress.init,
                        'on_uploaded':page_object.action.profile.action.settings.click.edit.image_add.on_uploaded.init,
                        'on_complete':page_object.action.profile.action.settings.click.edit.image_add.on_complete.init,
                        'on_limit':page_object.action.profile.action.settings.click.edit.image_add.on_limit.init,
                        'file_type':'image_ico',
                        'list':list
                    };

                    page_object.upload.reset();

                    page_object.upload.init(dialog_index,data);

                },
                'on_change':{
                    'init':function(index){

                        if(isset(page_object.upload.data['list'][index]))
                            if(!page_object.upload.data['list'][index]['remove']){

                                let  el
                                    ,inner      =''
                                    ,style      ='';

                                if(!OS.isMobile)
                                    style+='width: '+page_object.dialog.position.dialog.w+'px; ';

                                if(isset($d('dialog_file_item_'+index))){

                                    $s('dialog_file_item_image_'+index).height                  =0;
                                    $s('dialog_file_item_image_'+index).opacity                 =0;

                                    $s('dialog_file_item_progress_'+index).height               ='66px';
                                    $s('dialog_file_item_progress_'+index).opacity              =1;

                                    $s('dialog_file_item_progress_line_'+index).width           =0;
                                    $d('dialog_file_item_progress_percent_'+index).innerHTML    ='0 %';

                                }
                                else{

                                    inner+='<div id="dialog_file_item_image_'+index+'" class="dialog_file_item_image"></div>';

                                    inner+='<div id="dialog_file_item_progress_'+index+'" class="dialog_file_item_progress">';
                                    inner+='<div id="dialog_file_item_progress_line_'+index+'" class="dialog_file_item_progress_line"></div>';
                                    inner+='<div id="dialog_file_item_progress_percent_'+index+'" class="dialog_file_item_progress_percent">0 %</div>';
                                    inner+='</div>';

                                    inner+='<div id="dialog_file_item_controls_'+index+'" class="dialog_file_item_controls">';
                                    inner+='<div id="dialog_file_item_remove_'+index+'" class="dialog_file_item_remove"></div>';
                                    inner+='</div>';

                                    el=addElement({
                                        'tag':'div',
                                        'id':'dialog_file_item_'+index,
                                        'class':'dialog_file_item',
                                        'inner':inner,
                                        'style':style
                                    });

                                    $d('dialog_file_list').appendChild(el);

                                }

                                setTimeout(function(){

                                    page_object.action.profile.action.settings.click.edit.image_add.on_change.show(index);

                                },40);

                            }

                    },
                    'show':function(index){

                        if(isset($d('dialog_file_item_'+index)))
                            page_object.action.profile.action.settings.click.edit.image_add.resize();

                        page_object.action.profile.action.settings.click.edit.image_add.on_change.set_action();

                    },
                    'set_action':function(){

                        let  list=page_object.upload.data['list']
                            ,i;

                        for(i=0;i<list.length;i++)
                            if(isset($d('dialog_file_item_controls_'+i)))
                                if(!list[i]['remove'])
                                    $d('dialog_file_item_remove_'+i).onclick=page_object.action.profile.action.settings.click.edit.image_add.remove;

                    }
                },
                'on_progress':{
                    'init':function(){

                        if(isset($d('dialog_file_item_'+page_object.upload.file_item['index'])))
                            if(isset(page_object.upload.data['list'][page_object.upload.file_item['index']]))
                                if(!page_object.upload.data['list'][page_object.upload.file_item['index']]['remove']){

                                    let  block_w        =elementSize.width($d('dialog_file_item_progress_'+page_object.upload.file_item['index']))
                                        ,w              =Math.ceil(page_object.upload.data['list'][page_object.upload.file_item['index']]['percent']*block_w)
                                        ,percent        =Math.ceil(page_object.upload.data['list'][page_object.upload.file_item['index']]['percent']*100);

                                    $s('dialog_file_item_progress_line_'+page_object.upload.file_item['index']).width           =w+'px';
                                    $d('dialog_file_item_progress_percent_'+page_object.upload.file_item['index']).innerHTML    =percent+' %';

                                }

                    }
                },
                'on_uploaded':{
                    'init':function(){

                        if(isset($d('dialog_file_item_'+page_object.upload.file_item['index'])))
                            if(isset(page_object.upload.data['list'][page_object.upload.file_item['index']]))
                                if(!page_object.upload.data['list'][page_object.upload.file_item['index']]['remove']){

                                    let  data           =page_object.upload.data['list'][page_object.upload.file_item['index']]
                                        ,link           ='/'+data['image_dir']+'/'+data['image_item_ID_list']['preview']['ID']
                                        ,lang_obj       =page_object.action.profile.content[page_object.lang];

                                    $s('dialog_file_item_'+page_object.upload.file_item['index']).height                    ='100px';

                                    $s('dialog_file_item_progress_'+page_object.upload.file_item['index']).height           =0;
                                    $s('dialog_file_item_progress_'+page_object.upload.file_item['index']).opacity          =0;

                                    $s('dialog_file_item_image_'+page_object.upload.file_item['index']).background          ='url('+link+') 50% 50% no-repeat';
                                    $s('dialog_file_item_image_'+page_object.upload.file_item['index']).backgroundSize      ='contain';
                                    $s('dialog_file_item_image_'+page_object.upload.file_item['index']).height              ='100px';
                                    $s('dialog_file_item_image_'+page_object.upload.file_item['index']).opacity             =1;

                                    $s('dialog_file_item_remove_'+page_object.upload.file_item['index']).display            ='block';

                                    $d('dialog_submit_image_add').innerHTML='Загрузитьб другое изображение';

                                    page_object.action.profile.action.settings.click.edit.image_add.resize();

                                }

                    }
                },
                'on_complete':{
                    'init':function(){}
                },
                'on_limit':{
                    'init':function(){

                        if(isset($d('dialog_submit_image_add'))){

                            $d('dialog_submit_image_add').onclick=function(){};
                            $d('dialog_submit_image_add').setAttribute('class','dialog_submit_disable');

                        }

                    }
                },
                'add':function(dialog_index){

                    let list=page_object.upload.data['list'];

                    if(isset(list[0]))
                        if(list[0]['image_ID']!==0)
                            page_object.action.profile.action.settings.click.save.image_add.init(dialog_index,list[0]);

                },
                'remove':function(dialog_index){

                    let  list           =page_object.upload.data['list']
                        ,list_len       =list.length
                        ,list_left      =[]
                        ,list_right     =[]
                        ,index          =parseInt(this.id.split('_')[4])
                        ,i
                        ,i_old
                        ,lang_obj       =page_object.action.profile.content[page_object.lang];

                    if(isset(page_object.upload.file_item['index'])){

                        if(index===page_object.upload.file_item['index'])
                            page_object.upload.data['list'][index]['remove']=true;
                        else if(index<page_object.upload.file_item['index'])
                            page_object.upload.file_item['index']--;

                    }

                    if(index===0)
                        list_right=list.slice(1);
                    else if(index===(list_len-1))
                        list_left=list.slice(0,(list_len-1));
                    else{

                        list_left       =list.slice(0,index);
                        list_right      =list.slice((index+1));

                    }

                    list=list_left.concat(list_right);

                    page_object.upload.data['list']=list;

                    if(isset($d('dialog_file_item_'+index))){

                        $s('dialog_file_item_'+index).opacity       =0;
                        $s('dialog_file_item_'+index).height        =0;

                        setTimeout(function(){

                            removeElement($d('dialog_file_item_'+index));

                            for(i=index;i<list.length;i++){

                                i_old=i+1;

                                if(isset($d('dialog_file_item_'+i_old))){

                                    $d('dialog_file_item_'+i_old).setAttribute('id','dialog_file_item_'+i);

                                    $d('dialog_file_item_image_'+i_old).setAttribute('id','dialog_file_item_image_'+i);
                                    $d('dialog_file_item_progress_'+i_old).setAttribute('id','dialog_file_item_progress_'+i);
                                    $d('dialog_file_item_progress_line_'+i_old).setAttribute('id','dialog_file_item_progress_line_'+i);
                                    $d('dialog_file_item_progress_percent_'+i_old).setAttribute('id','dialog_file_item_progress_percent_'+i);
                                    $d('dialog_file_item_controls_'+i_old).setAttribute('id','dialog_file_item_controls_'+i);
                                    $d('dialog_file_item_remove_'+i_old).setAttribute('id','dialog_file_item_remove_'+i);

                                }

                            }

                            if(isset($d('dialog_submit_image_add'))){

                                $d('dialog_submit_image_add').onclick=function(){

                                    $d('dialog_file_change').click();

                                };
                                $d('dialog_submit_image_add').setAttribute('class','dialog_submit_add');
                                $d('dialog_submit_image_add').innerHTML='Загрузить изображение';

                            }

                            setTimeout(page_object.action.profile.action.settings.click.edit.image_add.on_change.set_action,40);
                            setTimeout(page_object.action.profile.action.settings.click.edit.image_add.resize,40);

                        },300);

                    }

                    if(isset($d('dialog_submit_save_'+dialog_index))){

                        if(page_object.upload.data['list'].length===0){

                            $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');
                            $d('dialog_submit_save_'+dialog_index).onclick=function(){};

                        }
                        else{

                            $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                            $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                page_object.action.profile.action.settings.click.edit.image_add.add(dialog_index);

                            };

                        }

                    }

                },
                'resize':function(dialog_index){

                    let  i
                        ,h
                        ,y          =0
                        ,list       =page_object.upload.data['list'];

                    for(i=0;i<list.length;i++)
                        if(isset($d('dialog_file_item_'+i))){

                            h=(list[i]['complete']&&!list[i]['remove'])?100:40;

                            $s('dialog_file_item_'+i).height        =h+'px';
                            $s('dialog_file_item_'+i).transform     ='translate(0,'+y+'px)';

                            y+=h;

                        }

                    if(isset($d('dialog_file_list')))
                        $s('dialog_file_list').height=y+'px';

                    if(isset($d('dialog_block_'+dialog_index)))
                        $s('dialog_block_'+dialog_index).height=(y+133)+'px';

                    setTimeout(page_object.dialog.action.resize,300);

                }
            }
        },
        'save':{
            'name':{
                'init':function(dialog_index){

                    let  name           =$v('dialog_name_input_text')
                        ,surname        =$v('dialog_surname_input_text')
                        ,post           =''
                        ,error          =false;

                    if(name===''){

                        $s('dialog_name_input_text').border='1px solid #ff001d';

                        error=true;

                    }
                    else
                        post+='name='+uniEncode(name);

                    if(surname===''){

                        $s('dialog_surname_input_text').border='1px solid #ff001d';

                        error=true;

                    }
                    else
                        post+='&surname='+uniEncode(surname);

                    if(!error){

                        if(isset($d('dialog_submit_save_'+dialog_index))){

                            $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');
                            $d('dialog_submit_save_'+dialog_index).onclick=function(){};

                        }

                        if(isset($d('dialog_submit_cancel_'+dialog_index))){

                            $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');
                            $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                        }

                        send({
                            'scriptPath':'/api/json/set_user_name',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                let  data
                                    ,dataTemp;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    $s('dialog_name_input_text').border         ='1px solid #ff001d';
                                    $s('dialog_surname_input_text').border      ='1px solid #ff001d';

                                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                    $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                        page_object.action.profile.action.settings.click.save.name.init();

                                    };

                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');
                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };

                                }
                                else{

                                    page_object.action.profile.data['settings']['name']       =name;
                                    page_object.action.profile.data['settings']['surname']    =surname;

                                    $d('settings_name').innerHTML       =stripSlashes(name);
                                    $d('settings_surname').innerHTML    =stripSlashes(surname);

                                    page_object.dialog.action.un_show.init(dialog_index,true);

                                }

                            }
                        })

                    }

                }
            },
            'login':{
                'init':function(dialog_index){

                    let  login          =$v('dialog_login_input_text')
                        ,post           =''
                        ,error          =false;

                    if(login===''){

                        $s('dialog_login_input_text').border='1px solid #ff001d';

                        error=true;

                    }
                    else
                        post+='login='+uniEncode(login);

                    if(!error){

                        if(isset($d('dialog_submit_save_'+dialog_index))){

                            $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');
                            $d('dialog_submit_save_'+dialog_index).onclick=function(){};

                        }

                        if(isset($d('dialog_submit_cancel_'+dialog_index))){

                            $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');
                            $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                        }

                        send({
                            'scriptPath':'/api/json/set_user_login',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                let  data
                                    ,dataTemp;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    $s('dialog_login_input_text').border='1px solid #ff001d';

                                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                    $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                        page_object.action.profile.action.settings.click.save.login.init();

                                    };

                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');
                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };

                                }
                                else{

                                    page_object.action.profile.data['user']['login']  =login;

                                    $d('settings_login').innerHTML          =stripSlashes(login);
                                    $d('header_profile_login').innerHTML    =stripSlashes(login);

                                    let lang_obj=page_object.content[page_object.lang];

                                    setUrl(lang_obj['title']+' | '+lang_obj['loading'],'/profile/'+login+'/settings');

                                    page_object.dialog.action.un_show.init(dialog_index,true);

                                }

                            }
                        })

                    }

                }
            },
            'email':{
                'init':function(dialog_index){

                    let  email          =$v('dialog_email_input_text')
                        ,post           =''
                        ,error          =false;

                    if(email===''||!isEmail(email)){

                        $s('dialog_email_input_text').border='1px solid #ff001d';

                        error=true;

                    }
                    else
                        post+='email='+uniEncode(email);

                    if(!error){

                        if(isset($d('dialog_submit_save_'+dialog_index))){

                            $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');
                            $d('dialog_submit_save_'+dialog_index).onclick=function(){};

                        }

                        if(isset($d('dialog_submit_cancel_'+dialog_index))){

                            $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');
                            $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                        }

                        send({
                            'scriptPath':'/api/json/set_user_email',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                let  data
                                    ,dataTemp;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    $s('dialog_email_input_text').border='1px solid #ff001d';

                                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                    $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                        page_object.action.profile.action.settings.click.save.email.init();

                                    };

                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');
                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };

                                }
                                else{

                                    page_object.action.profile.data['settings']['email']=email;

                                    $d('settings_email').innerHTML=stripSlashes(email);

                                    page_object.dialog.action.un_show.init(dialog_index,true);

                                }

                            }
                        })

                    }

                }
            },
            'password':{
                'init':function(dialog_index){

                    let  pas_old        =$v('dialog_pasold_input_text')
                        ,pas_1          =$v('dialog_pas1_input_text')
                        ,pas_2          =$v('dialog_pas2_input_text')
                        ,post           =''
                        ,error          =false;

                    if(pas_old===''){

                        $s('dialog_pasold_input_text').border='1px solid #ff001d';

                        error=true;

                    }
                    else
                        post+='pas_old='+uniEncode(pas_old);

                    if(pas_1===pas_2&&pas_1!=='')
                        post+='&pas='+uniEncode(pas_1);
                    else{

                        $s('dialog_pas1_input_text').border='1px solid #ff001d';
                        $s('dialog_pas2_input_text').border='1px solid #ff001d';

                        error=true;

                    }

                    if(!error){

                        if(isset($d('dialog_submit_save_'+dialog_index))){

                            $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');
                            $d('dialog_submit_save_'+dialog_index).onclick=function(){};

                        }

                        if(isset($d('dialog_submit_cancel_'+dialog_index))){

                            $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');
                            $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                        }

                        send({
                            'scriptPath':'/api/json/set_user_password',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                let  data
                                    ,dataTemp;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    $s('dialog_pasold_input_text').border       ='1px solid #ff001d';
                                    $s('dialog_pas1_input_text').border         ='1px solid #ff001d';
                                    $s('dialog_pas2_input_text').border         ='1px solid #ff001d';

                                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                    $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                        page_object.action.profile.action.settings.click.save.password.init();

                                    };

                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');
                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };

                                }
                                else{

                                    page_object.dialog.action.un_show.init(dialog_index,true);

                                }

                            }
                        })

                    }

                }
            },
            'image_add':{
                'init':function(dialog_index,data_image){

                    let  image_link
                        ,post           ='image_ID='+uniEncode(data_image['image_ID']);

                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');
                    $d('dialog_submit_save_'+dialog_index).onclick=function(){};

                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');
                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    send({
                        'scriptPath':'/api/json/set_user_image',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  data
                                ,dataTemp;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                $s('dialog_login_input_text').border='1px solid #ff001d';

                                $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                    page_object.action.profile.action.settings.click.edit.image_add.add();

                                };

                                $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');
                                $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                    page_object.dialog.action.un_show.init(dialog_index,true);

                                };

                            }
                            else{

                                page_object.action.profile.data['image_ID']               =data_image['image_ID'];
                                page_object.action.profile.data['image_dir']              =data_image['image_dir'];
                                page_object.action.profile.data['image_item_ID_list']     =data_image['image_item_ID_list'];

                                image_link=(data_image['image_ID']===0)?'url(/Project/Whore/Admin/Template/Images/System/bg_plus.png) 50% 50% no-repeat':'url(/'+data_image['image_dir']+'/'+data_image['image_item_ID_list']['preview']['ID']+') 50% 50% no-repeat';

                                if(isset($d('settings_image'))){

                                    $s('settings_image').background             =image_link;
                                    $s('settings_image').cursor                 ='pointer';

                                }

                                if(isset($d('header_profile_image'))){

                                    $s('header_profile_image').background       =image_link;
                                    $s('header_profile_image').backgroundSize   ='cover';

                                }

                                page_object.dialog.action.un_show.init(dialog_index,true);

                            }

                        }
                    });

                }
            }
        },
        'account_remove':{
            'init':function(){

                    let  lang_obj       =page_object.action.profile.content[page_object.lang]
                        ,inner          ='';

                    page_object.action.profile.action.settings.click.edit.error=false;

                    // inner+='<div id="dialog_email_block" class="dialog_row"></div>';

                    var dialog_index=page_object.dialog.init({
                        'title':'Удалить аккаунт?',
                        'inner':inner,
                        'remove':function(){

                            page_object.action.profile.action.settings.click.account_remove.send(dialog_index);

                        },
                        'cancel':true,
                        'on_create':function(){

                        }
                    });

            },
            'send':function(dialog_index){

                let post='';

                if(isset($d('dialog_submit_remove_'+dialog_index))){

                    $d('dialog_submit_remove_'+dialog_index).setAttribute('class','dialog_submit_disable');
                    $d('dialog_submit_remove_'+dialog_index).onclick=function(){};

                }

                if(isset($d('dialog_submit_cancel_'+dialog_index))){

                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');
                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                }

                send({
                    'scriptPath':'/api/json/remove_profile_account',
                    'postData':post,
                    'onComplete':function(j,worktime){

                        let  data
                            ,dataTemp;

                        dataTemp=j.responseText;
                        trace(dataTemp);
                        data=jsonDecode(dataTemp);
                        trace(data);
                        trace_worktime(worktime,data);

                        if(isset(data['error'])){

                            setUrl('Redirecting','/');

                            page_object.dialog.action.un_show.init(dialog_index,true);

                        }
                        else{

                            page_object.dialog.action.un_show.init(dialog_index,true);

                            page_object.action.profile.section_change_workflow.profile_logout.init();

                        }

                    }
                });

            }
        }
    },
    'resize':function(){

        if(isset($d('settings'))){

            page_object.action.profile.position.settings.w        =winSize.winWidth-page_object.action.profile.position.settings.m.l-page_object.action.profile.position.settings.m.r;
            page_object.action.profile.position.settings.h        =winSize.winHeight
                                                                    -page_object.action.profile.position.header.h
                                                                    -page_object.action.profile.position.settings.m.t;
            page_object.action.profile.position.settings.x        =page_object.action.profile.position.settings.m.l;
            page_object.action.profile.position.settings.y        =page_object.action.profile.position.header.h
                                                                    +page_object.action.profile.position.settings.m.t;

            if(page_object.action.profile.position.settings.h<$d('settings').scrollHeight)
                page_object.action.profile.position.settings.h=$d('settings').scrollHeight;

            if(!OS.isMobile){

                $s('settings').width=page_object.action.profile.position.settings.w+'px';
                $s('settings').height=page_object.action.profile.position.settings.h+'px';
                $s('settings').transform='translate('+page_object.action.profile.position.settings.x+'px,'+page_object.action.profile.position.settings.y+'px)';

            }

        }

    }
};