page_object.action.profile.action.header={
    'show':function(){

        if(isset($d('header'))){

            page_object.action.profile.position.header.o=1;
            page_object.action.profile.position.header.y=0;

            $s('header').opacity        =page_object.action.profile.position.header.o;
            $s('header').transform      ='translate('+page_object.action.profile.position.header.x+'px,'+page_object.action.profile.position.header.y+'px)'

        }

        if(isset($d('menu_button')))
            $d('menu_button').onclick=page_object.action.profile.action.header.menu.show;

        if(isset($d('footer')))
            $s('footer').opacity=1;

    },
    'un_show':function(remove){

        if(isset($d('header'))){

            page_object.action.profile.position.header.o=0;
            page_object.action.profile.position.header.y=-page_object.action.profile.position.header.h;

            $s('header').opacity        =page_object.action.profile.position.header.o;
            $s('header').transform      ='translate('+page_object.action.profile.position.header.x+'px,'+page_object.action.profile.position.header.y+'px)'

        }

        if(isset($d('footer')))
            $s('footer').opacity=0;

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.profile.action.header.remove,300);

    },
    'logo':{
        'click':function(){

            let  lang_obj       =page_object.content[page_object.lang]
                ,link           ='/profile/'+page_object.action.profile.data['user']['login']+'/face';

            page_object.action.profile.action.header_menu.set_active();

            setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

        }
    },
    'menu':{
        'show':function(){

            if(isset($d('header_menu'))){

                $s('header_menu').transform     ='translate(-100%,0)';
                $s('header_menu').opacity       =1;

            }

        },
        'un_show':function(){

            if(isset($d('header_menu'))){

                $s('header_menu').transform     ='translate(0,0)';
                $s('header_menu').opacity       =0;

            }

        },
    },
    'remove':function(){

        if(isset($d('header')))
            removeElement($d('header'));

        if(isset($d('footer')))
            removeElement($d('footer'));

    },
    'resize':function(){

        if(isset($d('header'))){

            // page_object.action.profile.position.header.w=winSize.winWidth;
            //
            // $s('header').width=page_object.action.profile.position.header.w+'px';

        }

    }
};