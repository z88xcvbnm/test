page_object.action.profile.action.header_profile={
    'show':function(){

        if(isset($d('header_profile'))){

            page_object.action.profile.position.header_profile.o=1;

            $s('header_profile').opacity=page_object.action.profile.position.header_profile.o;

        }

    },
    'un_show':function(remove){

        if(isset($d('header_profile'))){

            page_object.action.profile.position.header_profile.o=0;

            $s('header_profile').opacity=page_object.action.profile.position.header_profile.o;

        }

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.profile.action.header_profile.remove,300);

    },
    'remove':function(){

        if(isset($d('header_profile')))
            removeElement($d('header_profile'));

    },
    'click':function(){

        page_object.action.profile.create.admin_menu_block.init();

    },
    'resize':function(){}
};