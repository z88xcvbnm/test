page_object.action.profile.action.face_content={
    'show':function(){

        if(isset($d('face_content'))){

            page_object.action.profile.position.face_content.o       =1;
            page_object.action.profile.position.face_content.y       =page_object.action.profile.position.header.h;

            $s('face_content').opacity     =page_object.action.profile.position.face_content.o;
            $s('face_content').transform   ='translate('+page_object.action.profile.position.face_content.x+'px,'+page_object.action.profile.position.face_content.y+'px)';

        }

        setTimeout(page_object.link.preload.un_show,300);

    },
    'un_show':function(remove){

        if(isset($d('face_content'))){

            page_object.action.profile.position.face_content.o       =0;
            page_object.action.profile.position.face_content.y       =0;
            page_object.action.profile.position.face_content.h       =0;

            $s('face_content').opacity     =page_object.action.profile.position.face_content.o;
            $s('face_content').height      =page_object.action.profile.position.face_content.h='px';
            $s('face_content').transform   ='translate('+page_object.action.profile.position.face_content.x+'px,'+page_object.action.profile.position.face_content.y+'px)';

            if(isset(remove))
                if(remove)
                    setTimeout(page_object.action.profile.action.face_content.remove,300);

        }

    },
    'remove':function(){

        if(isset($d('face_content')))
            removeElement($d('face_content'))

    },
    'resize':function(){

    }
};