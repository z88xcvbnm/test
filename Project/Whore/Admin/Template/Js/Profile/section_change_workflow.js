page_object.action.profile.section_change_workflow={
    'profile':{
        'init':function(){

            page_object.action.profile.create.header.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.profile.action.header_profile_menu_block.un_show(true);

            if(isset($d('settings')))
                page_object.action.profile.action.settings.un_show(true);

            if(isset($d('header_menu')))
                page_object.action.profile.action.header_menu.set_active();

            if(isset($d('root_face_result_block')))
                page_object.action.profile.action.root_face_block.action.search.old.show_results.un_show();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.profile.action.root_face_block.un_show();

                setTimeout(page_object.action.profile.create.root_content.init,300);

            }
            else
                page_object.action.profile.create.root_content.init();

        }
    },
    'profile_face':{
        'init':function(){

            page_object.action.profile.create.header.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.profile.action.header_profile_menu_block.un_show(true);

            if(isset($d('settings')))
                page_object.action.profile.action.settings.un_show(true);

            if(isset($d('header_menu')))
                page_object.action.profile.action.header_menu.set_active();

            if(isset($d('root_face_result_block')))
                page_object.action.profile.action.root_face_block.action.search.old.show_results.un_show();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.profile.action.root_face_block.un_show();

                setTimeout(page_object.action.profile.create.root_content.init,300);

            }
            else
                setTimeout(page_object.action.profile.create.root_content.init,300);

        }
    },
    'profile_city':{
        'init':function(){

            page_object.action.profile.create.header.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.profile.action.header_profile_menu_block.un_show(true);

            if(isset($d('settings')))
                page_object.action.profile.action.settings.un_show(true);

            if(isset($d('header_menu')))
                page_object.action.profile.action.header_menu.set_active();

            if(isset($d('root_face_result_block')))
                page_object.action.profile.action.root_face_block.action.search.old.show_results.un_show();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.profile.action.root_face_block.un_show();

                setTimeout(page_object.action.profile.create.root_content.init,300);

            }
            else
                setTimeout(page_object.action.profile.create.root_content.init,300);

        }
    },
    'profile_payment':{
        'init':function(){

            page_object.action.profile.create.header.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.profile.action.header_profile_menu_block.un_show(true);

            if(isset($d('settings')))
                page_object.action.profile.action.settings.un_show(true);

            if(isset($d('header_menu')))
                page_object.action.profile.action.header_menu.set_active();

            if(isset($d('root_face_result_block')))
                page_object.action.profile.action.root_face_block.action.search.old.show_results.un_show();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.profile.action.root_face_block.un_show();

                setTimeout(page_object.action.profile.create.root_content.init,300);

            }
            else
                setTimeout(page_object.action.profile.create.root_content.init,300);

        }
    },
    'profile_billing':{
        'init':function(){

            page_object.action.profile.create.header.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.profile.action.header_profile_menu_block.un_show(true);

            if(isset($d('settings')))
                page_object.action.profile.action.settings.un_show(true);

            if(isset($d('header_menu')))
                page_object.action.profile.action.header_menu.set_active();

            if(isset($d('root_face_result_block')))
                page_object.action.profile.action.root_face_block.action.search.old.show_results.un_show();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.profile.action.root_face_block.un_show();

                setTimeout(page_object.action.profile.create.root_content.init,300);

            }
            else
                setTimeout(page_object.action.profile.create.root_content.init,300);

        }
    },
    'profile_search_history':{
        'init':function(){

            page_object.action.profile.create.header.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.profile.action.header_profile_menu_block.un_show(true);

            if(isset($d('settings')))
                page_object.action.profile.action.settings.un_show(true);

            if(isset($d('header_menu')))
                page_object.action.profile.action.header_menu.set_active();

            if(isset($d('root_face_result_block')))
                page_object.action.profile.action.root_face_block.action.search.old.show_results.un_show();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.profile.action.root_face_block.un_show();

                setTimeout(page_object.action.profile.create.root_content.init,300);

            }
            else
                setTimeout(page_object.action.profile.create.root_content.init,300);

        }
    },
    'profile_settings':{
        'init':function(){

            page_object.action.profile.create.header.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.profile.action.header_profile_menu_block.un_show(true);

            if(isset($d('settings')))
                page_object.action.profile.action.settings.un_show(true);

            if(isset($d('root_face_block')))
                page_object.action.profile.action.root_face_block.un_show(true);

            if(isset($d('root_face_result_block')))
                page_object.action.profile.action.root_face_block.action.search.old.show_results.un_show();

            if(isset($d('header_menu')))
                page_object.action.profile.action.header_menu.set_active();

            setTimeout(function(){

                page_object.action.profile.create.root_content.init();
                page_object.action.profile.create.settings.init();

            },300);

        }
    },
    'profile_feedback':{
        'init':function(){

            page_object.action.profile.create.header.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.profile.action.header_profile_menu_block.un_show(true);

            if(isset($d('settings')))
                page_object.action.profile.action.settings.un_show(true);

            if(isset($d('header_menu')))
                page_object.action.profile.action.header_menu.set_active();

            if(isset($d('root_face_result_block')))
                page_object.action.profile.action.root_face_block.action.search.old.show_results.un_show();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.profile.action.root_face_block.un_show();

                setTimeout(page_object.action.profile.create.root_content.init,300);

            }
            else
                setTimeout(page_object.action.profile.create.root_content.init,300);

        }
    },
    'profile_logout':{
        'init':function(){

            page_object.action.profile.action.header.un_show(true);
            page_object.action.profile.action.header_menu.un_show(true);
            page_object.action.profile.action.root_face_block.un_show(true);
            page_object.action.profile.action.settings.un_show(true);
            page_object.action.profile.action.header_profile_menu_block.un_show(true);

            if(isset($d('root_face_result_block')))
                page_object.action.profile.action.root_face_block.action.search.old.show_results.un_show();

            page_object.action.profile.data={};

            setTimeout(page_object.action.profile.section_change_workflow.profile_logout.redirect,300);

        },
        'redirect':function(){

            let  lang_obj       =page_object.content[page_object.lang]
                ,link           ='/';

            setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

            // if(OS.isMobile)
            //     document.location.reload();

        }
    }
};
