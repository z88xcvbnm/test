page_object.action.profile.create.admin_menu_block={
    'init':function(){

        if(isset($d('header_profile_menu_block')))
            page_object.action.profile.action.header_profile_menu_block.un_show(true);
        else{

            page_object.action.profile.create.admin_menu_block.position.init();
            page_object.action.profile.create.admin_menu_block.create();
            page_object.action.profile.create.admin_menu_block.set_action();
            page_object.action.profile.create.admin_menu_block.show();

        }

    },
    'position':{
        'init':function(){

            page_object.action.profile.create.admin_menu_block.position.admin_profile_block();

        },
        'admin_profile_block':function(){

            var pos=getElementPosition($d('header_profile_image'));

            page_object.action.profile.position.admin_menu_block.x        =pos['left']-73;
            page_object.action.profile.position.admin_menu_block.y        =pos['top']+page_object.action.profile.position.header_profile.h+5;

        }
    },
    'create':function(){

        var  el
            ,inner      =''
            ,style      =''
            ,lang_obj   =page_object.action.profile.content[page_object.lang];

        inner+='<div id="header_profile_arrow"></div>';
        inner+='<div id="header_profile_menu">';
        inner+='<div id="header_profile_menu_settings" class="header_profile_menu_item'+(page_object.link.link_list[2]==='settings'?'_active':'')+'">'+stripSlashes(lang_obj['header_profile_menu_settings'])+'</div>';
        inner+='<div id="header_profile_menu_logout" class="header_profile_menu_item">'+stripSlashes(lang_obj['header_profile_menu_logout'])+'</div>';
        inner+='</div>';

        style+='transform: translate('+page_object.action.profile.position.admin_menu_block.x+'px,'+page_object.action.profile.position.admin_menu_block.y+'px); ';
        style+='opacity: '+page_object.action.profile.position.admin_menu_block.o+'; ';
        style+='height: '+page_object.action.profile.position.admin_menu_block.h+'px; ';

        el=addElement({
            'tag':'div',
            'id':'header_profile_menu_block',
            'inner':inner,
            'style':style
        });

        $d('all').appendChild(el);

    },
    'set_action':function(){

        $d('header_profile_menu_settings').onclick      =(page_object.link.link_list[2]=='settings')?function(){}:page_object.action.profile.action.header_profile_menu_block.click.init;
        $d('header_profile_menu_logout').onclick        =page_object.action.profile.action.header_profile_menu_block.click.init;

    },
    'show':function(){

        page_object.action.profile.create.admin_menu_block.set_action();
        page_object.action.profile.action.header_profile_menu_block.mouse.init();
        page_object.action.profile.action.header_profile_menu_block.show();

    }
};
