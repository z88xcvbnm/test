page_object.action.profile.create.header={
    'init':function(){

        if(
            (
                  isset($d('header'))
                &&!isset($d('header_profile'))
                // &&!isset($d('header_login'))
                &&!OS.isMobile
            )
            ||(
                  isset($d('header'))
                &&elementSize.width($d('logo'))!==60
                &&OS.isMobile
            )
        ){

            page_object.action.profile.action.header.un_show(true);

            setTimeout(page_object.action.profile.create.header.create.init,300);

        }
        else{

            page_object.action.profile.create.header.create.init();

        }

    },
    'position':{
        'header_menu':function(){

            page_object.action.profile.position.header_menu.w=Math.ceil(winSize.winWidth*.63);

        },
        'header':function(){

            page_object.action.profile.position.header.w      =winSize.winWidth;
            page_object.action.profile.position.header.x      =0;
            page_object.action.profile.position.header.y      =-page_object.action.profile.position.header.h;

        },
        'header_profile':function(){

            if(isset($d('header_profile'))){

                page_object.action.profile.position.header_profile.o      =0;
                page_object.action.profile.position.header_profile.w      =elementSize.width($d('header_profile'));
                page_object.action.profile.position.header_profile.x      =winSize.winWidth-page_object.action.profile.position.header_profile.m.r-page_object.action.profile.position.header_profile.w;

                $s('header_profile').opacity            =page_object.action.profile.position.header_profile.o;
                // $s('header_profile').transform          ='translate('+page_object.action.profile.position.header_profile.x+'px,'+page_object.action.profile.position.header_profile.y+'px)';
                $s('header_profile').height             =page_object.action.profile.position.header_profile.h+'px';

            }

        }
    },
    'create':{
        'init':function(){

            if(isset($d('header')))
                return false;

            page_object.action.profile.create.header.position.header();
            page_object.action.profile.create.header.position.header_menu();
            page_object.action.profile.create.header.create.header();
            page_object.action.profile.create.header.create.footer();
            page_object.action.profile.create.header.set_action();
            page_object.action.profile.create.header.show();

            return true;

        },
        'header':function(){

            let  el
                ,inner                      =''
                ,header_style               =''
                ,header_menu_style          =''
                ,profile_style              =''
                ,lang_object                =page_object.action.profile.content[page_object.lang]
                ,data                       =page_object.action.profile.data['user']
                ,image_ID                   =isset(data['image_ID'])?data['image_ID']:null
                ,login                      =data['login']
                ,profile_image              =(image_ID===0)?'/Project/Whore/Admin/Template/Images/Avatar/admin_icon.png':'/'+data['image_dir']+'/'+data['image_item_ID_list']['preview']['ID'];

            inner+='<div id="logo"'+(OS.isMobile?' style="width: 55px"':'')+'></div>';

            if(OS.isMobile)
                inner+='<div id="menu_button"></div>';

            if(!OS.isMobile)
                header_menu_style+='transform: translate('+page_object.action.profile.position.header_menu.x+'px,'+page_object.action.profile.position.header_menu.y+'px); ';

            header_menu_style+='opacity: '+page_object.action.profile.position.header_menu.o+'; ';

            if(OS.isMobile){

                if(!isset($d('header_menu'))){

                    let header_menu_inner='';

                    header_menu_inner+='<div id="menu_close"></div>';

                    header_menu_inner+='<div id="header_menu_list">';

                        header_menu_inner+='<div id="header_menu_" class="header_menu_item">Главная</div>';
                        // header_menu_inner+='<div id="header_menu_face" class="header_menu_item">Поиск</div>';
                        header_menu_inner+='<div id="header_menu_payment" class="header_menu_item">Пополнить баланс</div>';
                        header_menu_inner+='<div id="header_menu_search_history" class="header_menu_item">История запросов</div>';
                        header_menu_inner+='<div id="header_menu_city" class="header_menu_item">Статистика</div>';
                        // header_menu_inner+='<div id="header_menu_billing" class="header_menu_item_disable">Биллинг</div>';
                        header_menu_inner+='<div id="header_menu_billing" class="header_menu_item">Биллинг</div>';
                        header_menu_inner+='<div id="header_menu_settings" class="header_menu_item">Настройки</div>';
                        header_menu_inner+='<div id="header_menu_feedback" class="header_menu_item">Связаться с нами</div>';
                        header_menu_inner+='<div id="header_menu_logout" class="header_menu_item">Выход</div>';

                    header_menu_inner+='</div>';

                    el=addElement({
                        'tag':'div',
                        'id':'header_menu',
                        'inner':header_menu_inner
                    });

                    $d('all').appendChild(el);

                }

                inner+='<div id="header_balance_block" class="header_balance_block">';
                    inner+='<div id="header_balance_label"><span>Ваш баланс: </span><span id="user_balance"'+(data['balance']<data['price']?' style="color: #ff0000;"':'')+'>'+get_number_with_space(data['balance'])+'</span><span> ₽</span></div>';
                inner+='</div>';

            }
            else{

                inner+='<div id="header_menu" style="'+header_menu_style+'">';

                    inner+='<div id="header_menu_billing" class="header_menu_item" style="margin-left: 0;">Биллинг</div>';
                    inner+='<div id="header_menu_search_history" class="header_menu_item">История запросов</div>';
                    inner+='<div id="header_menu_city" class="header_menu_item">Статистика</div>';

                inner+='</div>';

                inner+='<div id="header_balance_block" class="header_balance_block">';
                    inner+='<div id="header_balance_label">Ваш баланс: <span id="user_balance"'+(data['balance']<data['price']?' style="color: #ff0000;"':'')+'>'+data['balance']+'</span> ₽</div>';
                    inner+='<div id="header_balance_payment" class="header_balance_button">Пополнить баланс</div>';
                inner+='</div>';

                inner+='<div id="header_menu_line_active" class="header_menu_line_active"></div>';

                inner+='<div id="header_profile" class="header_profile" style="'+profile_style+'">';
                inner+='<div id="header_profile_image" style="background: url('+profile_image+') 50% 50% no-repeat; background-size: cover;"></div>';
                inner+='<div id="header_profile_login">'+stripSlashes(login)+'</div>';
                inner+='</div>';

            }


            header_style+='transform: translate('+page_object.action.profile.position.header.x+'px,'+page_object.action.profile.position.header.y+'px); ';
            // header_style+='width: '+page_object.action.profile.position.header.w+'px; ';
            // header_style+='height: '+page_object.action.profile.position.header.h+'px; ';

            el=addElement({
                'tag':'div',
                'id':'header',
                'inner':inner,
                'style':header_style
            });

            $d('all').appendChild(el);

        },
        'footer':function(){

            let  el
                ,inner='';

            inner+='<div class="footer_copyright">© 2019 Shluham.net</div>';
            inner+='<div id="footer_feedback"><span>Связаться с нами</span></div>';

            el=addElement({
                'tag':'div',
                'id':'footer',
                'inner':inner,
                'style':'opacity: 0'
            });

            $d('all').appendChild(el);

        }
    },
    'set_action':function(){

        if(isset($d('header_menu'))){

            let  id_el              =OS.isMobile?'header_menu_list':'header_menu'
                ,list               =$d(id_el).getElementsByTagName('div')
                ,i;

            for(i=0;i<list.length;i++)
                if(isObject(list[i]))
                    if(list[i].getAttribute('class')!=='header_menu_item_disable')
                        list[i].onclick=page_object.action.profile.action.header_menu.click;

        }

        if(isset($d('header_balance_payment')))
            $d('header_balance_payment').onclick=page_object.action.profile.action.header_menu.click;

        if(isset($d('header_profile')))
            $d('header_profile').onclick=page_object.action.profile.action.header_profile.click;

        if(isset(page_object.link.link_list[2]))
            page_object.action.profile.action.header_menu.set_active(page_object.link.link_list[2]);

        if(isset($d('logo')))
            $d('logo').onclick=page_object.action.profile.action.header.logo.click;

        if(isset($d('menu_button')))
            $d('menu_button').onclick=page_object.action.profile.action.header.menu.show;

        if(isset($d('menu_close')))
            $d('menu_close').onclick=page_object.action.profile.action.header.menu.un_show;

        if(isset($d('footer_feedback')))
            $d('footer_feedback').onclick=function(){

                var lang_obj=page_object.content[page_object.lang];

                setUrl(lang_obj['title']+' | '+lang_obj['loading'],'/profile/'+page_object.action.profile.data['user']['login']+'/feedback');

            };

    },
    'show':function(){

        setTimeout(page_object.action.profile.action.header.show,40);
        setTimeout(page_object.action.profile.action.header_menu.show,400);
        setTimeout(page_object.action.profile.create.header.position.header_profile,40);
        setTimeout(page_object.action.profile.action.header_profile.show,400);

    }
};
