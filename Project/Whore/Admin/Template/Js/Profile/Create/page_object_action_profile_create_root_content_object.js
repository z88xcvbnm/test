page_object.action.profile.create.root_content={
    'init':function(){

        page_object.action.profile.create.root_content.position.init();
        page_object.action.profile.create.root_content.create.init();
        page_object.action.profile.create.root_content.set_action();
        page_object.action.profile.create.root_content.show();

    },
    'position':{
        'init':function(){}
    },
    'create':{
        'init':function(){

            page_object.action.profile.create.root_content.create.root_content.init();
            page_object.action.profile.create.root_content.create.root_face_preview_list();

        },
        'get_search_history_row':function(index){

            let  data                   =page_object.action.profile.data['data']
                ,list                   =data['face_search_history_list']
                ,face_data              =list[index]
                ,face_index
                ,image_source_link      =empty(face_data['image'])?null:('/'+face_data['image']['image_dir']+'/'+face_data['image']['image_item_ID_list']['preview']['ID'])
                ,image_dest_link
                ,name                   =''
                ,percent
                ,percent_min            =100
                ,percent_max            =0
                ,inner                  ='';

            if(OS.isMobile){

                inner+='<div class="search_history_item_date_row">'+(empty(face_data['date_create'])?'-':(get_date(face_data['date_create'])+' '+get_time(face_data['date_create'])))+'</div>';

                inner+='<div class="search_history_item_mobile_row">';

                    inner+='<div class="search_history_item_mobile_col">';

                        inner+='<div class="search_history_item_image" style="background: url('+image_source_link+') 50% 50% no-repeat; background-size: cover;"></div>';

                    inner+='</div>';

                    inner+='<div class="search_history_item_mobile_col_arrow"></div>';

                    inner+='<div class="search_history_item_mobile_col">';

                if(
                    parseInt(face_data['percent'])>9000
                    ||(
                          parseInt(face_data['percent'])<=100
                        &&parseInt(face_data['percent'])>90
                    )
                ){

                    image_dest_link='/'+face_data['face_list'][0]['image']['image_dir']+'/'+face_data['face_list'][0]['image']['image_item_ID_list']['preview']['ID'];

                    if(face_data['percent']>10000)
                        percent=(parseFloat(face_data['percent'])/10000);
                    else if(face_data['percent']>100)
                        percent=(parseFloat(face_data['percent'])/100);
                    else
                        percent=(parseFloat(face_data['percent']));

                    if(!empty(face_data['name']))
                        name+=face_data['name'];

                    if(!empty(face_data['age']))
                        name+=(!empty(name)?', ':'')+face_data['age'];

                    if(!empty(face_data['city']))
                        name+=(!empty(name)?', ':'')+face_data['city'];

                    if(name.length>24)
                        name=name.substr(0,24)+'...';

                    inner+='<div class="search_history_item_info">'+stripSlashes(name)+'</div>';
                    inner+='<div class="search_history_item_image'+(empty(name)?'':'_right')+'" style="background: url('+image_dest_link+') 50% 50% no-repeat; background-size: cover;">';
                        inner+='<div class="search_history_item_image_percent">'+percent+'%</div>';
                    inner+='</div>';

                }
                else
                    inner+='<div class="search_history_item_info_image_empty"><table><tr><td valign="middle" style="background: url(/show/image/style/admin/system/nun_30.png) 50% 50% no-repeat; background-size: 80%;">Совпадений<br />не найдено</td></tr></table></div>';

                    inner+='</div>';

                inner+='</div>';

                if(face_data['face_list'].length>0){

                    for(face_index in face_data['face_list']){

                        if(face_data['face_list'][face_index]['percent']>10000)
                            percent=(parseFloat(face_data['face_list'][face_index]['percent'])/10000).toFixed(2);
                        else if(face_data['face_list'][face_index]['percent']>100)
                            percent=(parseFloat(face_data['face_list'][face_index]['percent'])/100).toFixed(2);
                        else
                            percent=(parseFloat(face_data['face_list'][face_index]['percent'])).toFixed(2);

                        if(percent_min>percent)
                            percent_min=percent;

                        if(percent_max<percent)
                            percent_max=percent;

                    }

                    if(!empty(percent_min)&&!empty(percent_max))
                        inner+='<div class="search_history_item_mobile_row" style="text-align: left;">Похожие девушки, совпадение от '+percent_max+'% до '+percent_min+'%:</div>';
                    else
                        inner+='<div class="search_history_item_mobile_row" style="text-align: left;">Похожие девушки:</div>';

                    inner+='<div class="search_history_item_mobile_row">';

                    inner+='<div class="search_history_item_profile_list">';

                    for(face_index in face_data['face_list']){

                        name='';

                        if(!empty(face_data['face_list'][face_index]['name']))
                            name+=face_data['face_list'][face_index]['name'];

                        if(!empty(face_data['face_list'][face_index]['age']))
                            name+=(!empty(name)?', ':'')+face_data['face_list'][face_index]['age'];

                        if(!empty(face_data['face_list'][face_index]['city']))
                            name+=(!empty(name)?', ':'')+face_data['face_list'][face_index]['city'];

                        image_dest_link='/'+face_data['face_list'][face_index]['image']['image_dir']+'/'+face_data['face_list'][face_index]['image']['image_item_ID_list']['preview']['ID'];

                        inner+='<div id="search_history_item_profile_pdf_'+index+'_'+face_index+'" class="search_history_item_profile_pdf">';

                            if(face_data['face_list'][face_index]['percent']>10000)
                                percent=(parseFloat(face_data['face_list'][face_index]['percent'])/10000);
                            if(face_data['face_list'][face_index]['percent']>100)
                                percent=(parseFloat(face_data['face_list'][face_index]['percent'])/100);
                            else
                                percent=(parseFloat(face_data['face_list'][face_index]['percent']));

                            inner+='<div class="search_history_item_profile_pdf_image" style="background: url('+image_dest_link+') 50% 50% no-repeat; background-size: cover;"></div>';
                            inner+='<div id="search_history_item_profile_pdf_info_'+index+'_'+face_index+'" class="search_history_item_profile_pdf_info"><table><tr><td>'+name+' - <i>'+percent+'%</i></td></tr></table></div>';
                            inner+='<div id="search_history_item_profile_pdf_load_'+index+'_'+face_index+'" class="search_history_item_profile_pdf_load">Загрузка...</div>';

                        inner+='</div>';

                    }

                        inner+='</div>';

                    inner+='</div>';

                }

            }
            else{

                inner+='<div class="search_history_item_col" style="width: 50px;"><table><tr><td>'+face_data['ID']+'</td></tr></table></div>';
                inner+='<div class="search_history_item_col"><table><tr><td>'+(empty(face_data['date_create'])?'-':(get_date(face_data['date_create'])+'<br />'+get_time(face_data['date_create'])))+'</td></tr></table></div>';
                inner+='<div class="search_history_item_col">';

                if(parseFloat(face_data['percent'])>10000)
                    percent=(parseFloat(face_data['percent'])/10000);
                else if(parseInt(face_data['percent'])>100)
                    percent=(parseFloat(face_data['percent'])/100);
                else
                    percent=(parseFloat(face_data['percent']));

                if(percent>=90)
                    image_source_link='/'+face_data['face_list'][0]['image']['image_dir']+'/'+face_data['face_list'][0]['image']['image_item_ID_list']['preview']['ID']

                    inner+='<div class="search_history_item_image" style="background: url('+image_source_link+') 50% 50% no-repeat; background-size: cover;"></div>';
                    inner+='<div class="search_history_item_info">'+parseInt(face_data['image']['image_item_ID_list']['large']['file_size']/1024)+'kb '+face_data['image']['image_item_ID_list']['large']['width']+'x'+face_data['image']['image_item_ID_list']['large']['height']+'</div>';

                inner+='</div>';
                inner+='<div class="search_history_item_col">';

                if(
                    parseInt(face_data['percent'])>9000
                    ||(
                          parseInt(face_data['percent'])>90
                        &&parseInt(face_data['percent'])<=100
                    )
                ){

                    image_dest_link='/'+face_data['face_list'][0]['image']['image_dir']+'/'+face_data['face_list'][0]['image']['image_item_ID_list']['preview']['ID'];

                    if(!empty(face_data['name']))
                        name+=face_data['name'];

                    if(!empty(face_data['age']))
                        name+=(!empty(name)?', ':'')+face_data['age'];

                    if(!empty(face_data['city']))
                        name+=(!empty(name)?', ':'')+face_data['city'];

                    inner+='<div class="search_history_item_image" style="background: url('+image_dest_link+') 50% 50% no-repeat; background-size: cover;">';
                        inner+='<div class="search_history_item_image_percent">'+percent+'%</div>';
                    inner+='</div>';
                    inner+='<div class="search_history_item_info">'+stripSlashes(name)+'</div>';

                }
                else
                    inner+='<div class="search_history_item_info"><table><tr><td style="background: url(/show/image/style/admin/system/nun_30.png) 50% 50% no-repeat; background-size: 80%;">Совпадений<br />не найдено</td></tr></table></div>';

                inner+='</div>';
                inner+='<div class="search_history_item_col" style="width: 400px">';

                if(face_data['face_list'].length>0){

                    inner+='<div class="search_history_item_profile_list">';

                    for(face_index in face_data['face_list']){

                        name='';

                        if(!empty(face_data['face_list'][face_index]['name']))
                            name+=face_data['face_list'][face_index]['name'];

                        if(!empty(face_data['face_list'][face_index]['age']))
                            name+=(!empty(name)?', ':'')+face_data['face_list'][face_index]['age'];

                        if(!empty(face_data['face_list'][face_index]['city']))
                            name+=(!empty(name)?', ':'')+face_data['face_list'][face_index]['city'];

                        image_dest_link='/'+face_data['face_list'][face_index]['image']['image_dir']+'/'+face_data['face_list'][face_index]['image']['image_item_ID_list']['preview']['ID'];

                        inner+='<div id="search_history_item_profile_pdf_'+index+'_'+face_index+'" class="search_history_item_profile_pdf">';

                            if(face_data['face_list'][face_index]['percent']>10000)
                                percent=(parseInt(face_data['face_list'][face_index]['percent'])/10000).toFixed(2);
                            else if(face_data['face_list'][face_index]['percent']>100)
                                percent=(parseInt(face_data['face_list'][face_index]['percent'])/100).toFixed(2);
                            else
                                percent=(parseInt(face_data['face_list'][face_index]['percent'])).toFixed(2);

                            inner+='<div class="search_history_item_profile_pdf_image" style="background: url('+image_dest_link+') 50% 50% no-repeat; background-size: cover;"></div>';
                            inner+='<div id="search_history_item_profile_pdf_info_'+index+'_'+face_index+'" class="search_history_item_profile_pdf_info"><table><tr><td>'+name+' - <i>'+percent+'%</i></td></tr></table></div>';
                            inner+='<div id="search_history_item_profile_pdf_load_'+index+'_'+face_index+'" class="search_history_item_profile_pdf_load">Загрузка...</div>';

                        inner+='</div>';

                    }

                        inner+='</div>';

                }
                else
                    inner+='<div class="search_history_item_info"><table><tr><td>Список пуст</td></tr></table></div>';

                inner+='</div>';

            }

            return inner;

        },
        'get_face_right_content':function(){

        },
        'get_billing_row':function(index){

            let  data                   =page_object.action.profile.data['data']
                ,list                   =data['billing_list']
                ,billing_data           =list[index]
                ,inner                  ='';

            if(OS.isMobile){

                let  action         =billing_data['action']
                    ,action_type    ='Списание'
                    ,source         =(empty(billing_data['wallet_name'])?'-':(billing_data['wallet_name']==='hand'?'Ручное пополнение':((billing_data['wallet_name'].toUpperCase().substr(0,4)+' - '))+(empty(billing_data['user_wallet_address'])?'-':stripSlashes(billing_data['user_wallet_address']))));

                switch(billing_data['action']){

                    case'promo_login_invite':{

                        action_type     ='Пополнение';
                        source          ='Реферальная регистрация пользователя '+billing_data['promo_code'];

                        break;

                    }

                    case'promo_login_use':{

                        action_type     ='Пополнение';
                        source          ='Регистрация по приглашению '+billing_data['promo_code'];

                        break;

                    }

                    case'promo_code':{

                        action_type     ='Пополнение';
                        source          ='Бонус за регистрацию по промокоду '+billing_data['promo_code'];

                        break;

                    }

                    case'add':{

                        action_type='Пополнение';

                        break;

                    }

                }

                inner+='<div class="search_history_item_date_row">'+(empty(billing_data['date_create'])?'-':(get_date(billing_data['date_create'])+' '+get_time(billing_data['date_create'])))+' > '+action_type+'</div>';

                inner+='<div class="search_history_item_mobile_row" style="text-align: left; margin: 0 0 10px 0;">';
                    inner+='<table width="100%">';
                        inner+='<tr><td width="50%" align="right">Сумма операции в рублях:</td><td width="50%" align="left"><b><i>'+billing_data['transaction_sum']+' ₽</i></b></td></tr>';

                        if(billing_data['action']==='add'){

                            inner+='<tr><td width="50%" align="right">Источник:</td><td width="50%" align="left"><b><i>'+source+'</i></b></td></tr>';
                            inner+='<tr><td width="50%" align="right">Сумма в валюте источника:</td><td width="50%" align="left"><b><i>'+(empty(billing_data['transaction_wallet_sum'])?'-':(billing_data['transaction_wallet_sum']+(empty(billing_data['wallet_name'])?' ₽':(' '+(billing_data['wallet_name']==='sberbank'?'₽':billing_data['wallet_name'].toUpperCase())))))+'</i></b></td></tr>';

                        }
                        else if(
                              billing_data['action']==='promo_code'
                            ||billing_data['action']==='promo_login_use'
                            ||billing_data['action']==='promo_login_invite'
                        )
                            inner+='<tr><td width="50%" align="right">Источник:</td><td width="50%" align="left"><b><i>'+source+'</i></b></td></tr>';

                        inner+='<tr><td width="50%" align="right">Баланс после операции:</td><td width="50%" align="left"><b><i>'+(empty(billing_data['balance'])?'-':(billing_data['balance']+' ₽'))+'</i></b></td></tr>';
                    inner+='</table>';
                inner+='</div>';

            }
            else{

                let  action         =billing_data['action']
                    ,action_type    ='Списание'
                    ,source         =(empty(billing_data['wallet_name'])?'-':(billing_data['wallet_name']==='hand'?'Ручное пополнение':((billing_data['wallet_name'].toUpperCase().substr(0,4)+' - '))+(empty(billing_data['user_wallet_address'])?'-':stripSlashes(billing_data['user_wallet_address']))));

                switch(billing_data['action']){

                    case'promo_login_invite':{

                        action_type     ='Пополнение';
                        source          ='Реферальная регистрация пользователя '+billing_data['promo_code'];

                        break;

                    }

                    case'promo_login_use':{

                        action_type     ='Пополнение';
                        source          ='Регистрация по приглашению '+billing_data['promo_code'];

                        break;

                    }

                    case'promo_code':{

                        action_type     ='Пополнение';
                        source          ='Бонус за регистрацию по промокоду '+billing_data['promo_code'];

                        break;

                    }

                    case'add':{

                        action_type='Пополнение';

                        break;

                    }

                }

                inner+='<div class="billing_item_col" style="width: 50px;"><table><tr><td>'+billing_data['ID']+'</td></tr></table></div>';
                inner+='<div class="billing_item_col"><table><tr><td>'+(empty(billing_data['date_create'])?'-':(get_date(billing_data['date_create'])+'<br />'+get_time(billing_data['date_create'])))+'</td></tr></table></div>';
                inner+='<div class="billing_item_col" style="width: 130px;"><table><tr><td>'+action_type+'</td></tr></table></div>';
                inner+='<div class="billing_item_col"><table><tr><td>'+billing_data['transaction_sum']+' ₽</td></tr></table></div>';

                inner+='<div class="billing_item_col" style="width: 420px;"><table><tr><td>'+source+'</td></tr></table></div>';
                inner+='<div class="billing_item_col"><table><tr><td>'+(empty(billing_data['transaction_wallet_sum'])?'-':(billing_data['transaction_wallet_sum']+(empty(billing_data['wallet_name'])?' ₽':(' '+(billing_data['wallet_name']==='sberbank'?'₽':billing_data['wallet_name'].toUpperCase())))))+'</td></tr></table></div>';
                inner+='<div class="billing_item_col"><table><tr><td>'+(empty(billing_data['balance'])?'-':(billing_data['balance']+' ₽'))+'</td></tr></table></div>';

            }

            return inner;

        },
        'root_face_preview_list':function(){

            var  el
                ,img_el
                ,data           =page_object.action.profile.data['data']
                ,face_list      =data['face_list']
                ,list           =data['face_image_random_list']
                ,random_len     =5
                ,index;

            if(!isset($d('root_face_preview_list'))){

                el=addElement({
                    'tag':'div',
                    'id':'root_face_preview_list'
                });

                $d('root_content').appendChild(el);

            }

            var image_len=$d('root_face_preview_list').getElementsByClassName('root_face_image_random').length;

            if(image_len===0){

                let  image_list             =[]
                    ,temp_image_list        ={}
                    ,image_random_index
                    ,image_index            =0
                    ,ration                 =window.devicePixelRatio
                    ,win_width              =window.screen.width*ration
                    ,win_height             =window.screen.height*ration
                    ,image_w                =50
                    ,image_h                =55
                    ,image_x_len            =Math.ceil(win_width/image_w)+1
                    ,image_y_len            =Math.ceil(win_height/image_h)+2
                    ,image_len              =list.length
                    ,image_max_len          =image_x_len*image_y_len
                    // ,image_max_len          =800
                    ,need_add               =true
                    ,index;

                // alert(window.devicePixelRatio);

                // alert(window.screen.width+' x '+window.screen.height);

                // alert(image_max_len);

                do{

                    do{

                        image_random_index  =Math.floor(Math.random()*image_len);
                        need_add            =true;

                        // trace(image_random_index);

                        for(index=image_list.length-1;index>image_list.length-random_len;index--)
                            if(isset(image_list[index]))
                                if(isset(image_list[index]['link'])){

                                    // trace(image_list[index]['link']+' === '+list[image_random_index]['link']);

                                    if(image_list[index]['link']===list[image_random_index]['link']){

                                        need_add=false;

                                        break;

                                    }

                                }

                    }while(!need_add);

                    image_list.push(list[image_random_index]);

                    if(!isset(temp_image_list[list[image_random_index]['link']]))
                        temp_image_list[list[image_random_index]['link']]=0;

                        temp_image_list[list[image_random_index]['link']]++;

                    image_index++;

                }while(image_index<=image_max_len);

                trace('Temp image list: ');
                trace(temp_image_list);
                trace(getObjectLength(temp_image_list));

                if(isset($d('root_face_preview_list')))
                    for(index in image_list){

                        img_el=addElement({
                            'tag':'div',
                            'id':'root_face_image_random_item_'+index,
                            'class':'root_face_image_random',
                            'title':face_list[image_list[index]['face_ID']],
                            'style':'background: url('+'/'+image_list[index]['link']+') 50% 50% no-repeat; background-size: cover;'
                        });

                        if(isset($d('root_face_preview_list')))
                            $d('root_face_preview_list').appendChild(img_el);

                        img_el.style.opacity=1;

                    }

            }

        },
        'root_content':{
            'init':function(){

                let  action=page_object.action.profile.data['action'];

                switch(action){

                    case'profile_face':{

                        page_object.action.profile.create.root_content.create.root_content.face();

                        break;

                    }

                    case'profile_city':{

                        page_object.action.profile.create.root_content.create.root_content.city();

                        break;

                    }

                    case'profile_payment':{

                        page_object.action.profile.create.root_content.create.root_content.payment();

                        break;

                    }

                    case'profile_search_history':{

                        page_object.action.profile.create.root_content.create.root_content.search_history();

                        break;

                    }

                    case'profile_billing':{

                        page_object.action.profile.create.root_content.create.root_content.billing();

                        break;

                    }

                    case'profile_feedback':{

                        page_object.action.profile.create.root_content.create.root_content.feedback();

                        break;

                    }

                }

            },
            'face':function(){

                let  inner      =''
                    ,i
                    ,el
                    ,data       =page_object.action.profile.data
                    ,cost       =data['data']['cost']
                    ,index;

                if(isset($d('root_face_content'))){

                    let inner='';

                    inner+='<p>Проверь свою избранницу по самой полной базе данных <strong>эскортниц, содержанок и шлюх</strong>, используя фотографию ее лица в фас.</p>';
                    inner+='<p>Применяемый биометрический алгоритм распознавания лиц имеет вероятность ложноположительного срабатывания (FAR) 10<sup>-7</sup>. </p><p style="margin-top: 10px;">Чем лучше качество загружаемого фото, тем точнее будет результат.</p>';
                    inner+='<p>Стоимость запроса'+(empty(cost['black_friday_date_limit'])?'':('<br /><b> до '+cost['black_friday_date_limit']+'</b>'))+':</p>';
                    inner+='<p>- по 1 фотографии - <strong>'+cost['face_1']+'₽</strong></p>';
                    inner+='<p>- по 3 фотографиям - <strong>'+cost['face_3']+'₽</strong></p>';
                    // inner+='<p>Стоимость одного запроса - 1000 ₽. Скидка 50% до 31 мая. <strong>Текущая стоимость: 500 ₽</strong></p>';

                    if(isset($d('root_left_content')))
                        $d('root_left_content').innerHTML=inner;

                    // if(isset($d('root_right_content'))&&!OS.isMobile)
                    //     $s('root_right_content').height=elementSize.height($d('root_left_content'))+'px';

                    if(isset($d('root_right_content'))&&!OS.isMobile){

                        inner='';
                        inner+='<div id="root_right_content_container">';

                            inner+='<p>На текущий момент в базе содержится:</p>';
                            inner+='<p><b>'+get_number_with_space(data['data']['face_len'])+'</b> анкет девушек занимающихся или занимавшихся продажей интимных услуг в России, Украине, Беларуси, Казахстане и других странах.</p>';

                    inner+='<p>Топ городов по количеству шкур:';

                    if(data['data']['city_list'].length>0){

                        inner+='<p>';

                        for(index in data['data']['city_list'])
                            inner+=stripSlashes(data['data']['city_list'][index]['name'])+' - <b>'+get_number_with_space(data['data']['city_list'][index]['len'])+'</b><br />';

                        inner+='</p>';

                    }

                            inner+='<p>Проиндексировано <b>'+get_number_with_space(data['data']['face_image_indexed_len'])+'</b> лиц.</p>';
                            inner+='<p>Нет денег? Бывает.. Подписывайся на <a href="https://t.me/shluham_net" target="_blank">t.me/shluham_net</a> и выиграй пополнение на <b>3000 руб.</b></p>';

                        inner+='</div>';

                        $d('root_right_content').innerHTML  =inner;
                        $s('root_right_content').height     =elementSize.height('root_left_content')+'px';

                    }

                    return false;

                }

                // inner+='<div id="root_face_block_bg"></div>';

                inner+='<div id="root_left_content">';

                inner+='<p>Проверь свою избранницу по самой полной базе данных <strong>эскортниц, содержанок и шлюх</strong>, используя фотографию ее лица в фас.</p>';
                inner+='<p>Применяемый биометрический алгоритм распознавания лиц имеет вероятность ложноположительного срабатывания (FAR) 10<sup>-7</sup>. </p><p style="margin-top: 10px;">Чем лучше качество загружаемого фото, тем точнее будет результат.</p>';
                inner+='<p>Стоимость запроса'+(empty(cost['black_friday_date_limit'])?'':(' <b> до '+cost['black_friday_date_limit']+'</b>'))+':</p>';
                inner+='<p>- по 1 фотографии - <strong>'+cost['face_1']+'₽</strong></p>';
                inner+='<p>- по 3 фотографиям - <strong>'+cost['face_3']+'₽</strong></p>';

                inner+='</div>';

                inner+='<div id="root_face_content">';

                inner+='<div id="root_search_face_content_loading"></div>';

                for(i=0;i<3;i++){

                    inner+='<div id="root_search_face_image_row_container_'+i+'" class="root_search_face_image_row_container">';

                        inner+='<div id="root_search_face_image_block_'+i+'" class="root_search_face_image_block">';

                            inner+='<div id="root_search_face_button_choose_'+i+'" class="root_search_face_button_choose"><span>Выбрать'+(i>0?' ещё':'')+' фотографию</span></div>';

                            inner+='<div id="root_search_face_image_info_'+i+'" class="root_search_face_image_info">или перетащите его сюда</div>';
                            inner+='<div id="root_search_face_image_loading_'+i+'" class="root_search_face_image_loading"><span>Загрузка...</span></div>';
                            inner+='<div id="root_search_face_image_error_'+i+'" class="root_search_face_image_error">Невереный формат файла</div>';

                            inner+='<div id="root_search_face_image_block_container_'+i+'" class="root_search_face_image_block_container">';

                                inner+='<div id="root_search_face_image_src_'+i+'" class="root_search_face_image_src"></div>';

                            inner+='</div>';

                            inner+='<input type="file" multiple="multiple" id="root_search_face_image_input_'+i+'" class="root_search_face_image_input" accept="image/png, image/jpeg" value="" title="" />';

                        inner+='</div>';

                    inner+='</div>';

                }

                    inner+='<div id="root_search_info_block" class="root_search_info_block"></div>';

                    inner+='<div id="root_search_face_price" class="root_search_face_price">Стоимость запроса <span id="root_search_face_cost">0</span> ₽</div>';

                    inner+='<div id="root_search_face_button" class="root_search_face_button_disable">Проверить фото</div>';

                if(OS.isMobile)
                    inner+='<div id="root_search_face_button_cancel" class="root_search_face_button">Отмена</div>';

                inner+='</div>';

                inner+='<div id="root_right_content">';
                    inner+='<div id="root_right_content_container">';

                        inner+='<p>На текущий момент в базе содержится:</p>';
                        inner+='<p><b>'+get_number_with_space(data['data']['face_len'])+'</b> анкет девушек занимающихся или занимавшихся продажей интимных услуг в России, Украине, Беларуси, Казахстане и других странах.</p>';

                inner+='<p>Топ городов по количеству шкур:';

                if(data['data']['city_list'].length>0){

                    inner+='<p>';

                    for(index in data['data']['city_list'])
                        inner+=stripSlashes(data['data']['city_list'][index]['name'])+' - <b>'+get_number_with_space(data['data']['city_list'][index]['len'])+'</b><br />';

                    inner+='</p>';

                }

                        inner+='<p>Проиндексировано <b>'+get_number_with_space(data['data']['face_image_indexed_len'])+'</b> лиц.</p>';
                        inner+='<p>Нет денег? Бывает.. Подписывайся на <a href="https://t.me/shluham_net" target="_blank">t.me/shluham_net</a> и выиграй пополнение на <b>3000 руб.</b></p>';

                    inner+='</div>';
                inner+='</div>';

                if(!isset($d('root_content'))){

                    el=addElement({
                        'tag':'div',
                        'id':'root_content',
                        'inner':inner,
                        'style':'opacity: 0'
                    });

                    $d('all').appendChild(el);

                    return true;

                }

                if(!isset($d('root_face_block'))){

                    let el=addElement({
                        'tag':'div',
                        'id':'root_face_block',
                        'inner':inner,
                        'style':'opacity: 0'
                    });

                    $d('root_content').appendChild(el);

                }
                else if(isset($d('root_face_block'))){

                    $d('root_face_block').innerHTML     =inner;
                    $s('root_face_block').width         ='';

                }

                if(OS.isMobile){

                    $s('root_face_block').top               ='50%';
                    // $s('root_face_block').transform         ='translate(-50%,calc(-50%)';

                }

            },
            'search_history':function(){

                let  data       =page_object.action.profile.data['data']
                    ,list       =data['face_search_history_list']
                    ,index
                    ,inner      ='';

                // if(OS.isMobile)
                    inner+='<div id="settings_close_button" class="dialog_close_button"'+(OS.isMobile?' style="margin: -11px 0 0 11px; padding: 0;"':' style="margin: -10px 0 0 10px; padding: 0;"')+'></div>';

                if(OS.isMobile)
                    inner+='<div id="root_default_content">';
                else
                    inner+='<div id="root_static_content"'+(list.length===0?' style="top: 50%;"':'')+'>';

                // if(!OS.isMobile)
                //     inner+='<div id="settings_close_button" class="dialog_close_button"></div>';

                    inner+='<div id="search_history_block">';

                if(list.length===0){

                    inner+='<div id="search_history_label_none">Список пуст</div>';

                }
                else{

                    if(OS.isMobile){

                        inner+='<div id="search_history_header" style="height: 30px; padding: 3px 0 0 0;">ИСТОРИЯ ЗАПРОСОВ</div>';

                    }
                    else{

                        inner+='<div id="search_history_header">';
                            inner+='<div class="search_history_header_col" style="width: 50px;">ID</div>';
                            inner+='<div class="search_history_header_col">Данные запроса</div>';
                            inner+='<div class="search_history_header_col">Исходное изображение</div>';
                            inner+='<div class="search_history_header_col">Результат запроса</div>';
                            inner+='<div class="search_history_header_col" style="width: 400px">Похожие анкеты</div>';
                        inner+='</div>';

                    }

                        inner+='<div id="search_history_list">';

                    for(index in list){

                        inner+='<div id="search_history_item_'+index+'" class="search_history_item">';

                            inner+=page_object.action.profile.create.root_content.create.get_search_history_row(index);

                        inner+='</div>';

                    }

                        inner+='</div>';

                }

                    inner+='</div>';

                inner+='</div>';

                if(!isset($d('root_face_block'))){

                    let el=addElement({
                        'tag':'div',
                        'id':'root_face_block',
                        'inner':inner,
                        'style':'opacity: 0'
                    });

                    $d('root_content').appendChild(el);

                }
                else if(isset($d('root_face_block'))){

                    $d('root_face_block').innerHTML     =inner;
                    $s('root_face_block').width         ='';

                }

                if(OS.isMobile){

                    $s('root_face_block').top               ='50%';
                    $s('root_face_block').transform         ='translate(-50%,calc(-50% + 30px)';

                }

            },
            'billing':function(){

                let  data       =page_object.action.profile.data['data']
                    ,list       =data['billing_list']
                    ,index
                    ,inner      ='';

                // if(OS.isMobile)
                    inner+='<div id="settings_close_button" class="dialog_close_button"'+(OS.isMobile?' style="margin: -11px 0 0 11px; padding: 0;"':' style="margin: -10px 0 0 10px; padding: 0;"')+'></div>';

                if(OS.isMobile)
                    inner+='<div id="root_default_content">';
                else
                    inner+='<div id="root_static_content"'+(list.length===0?' style="top: 50%;"':'')+'>';

                // if(!OS.isMobile)
                //     inner+='<div id="settings_close_button" class="dialog_close_button"></div>';

                    inner+='<div id="billing_block">';

                if(list.length===0){

                    inner+='<div id="search_history_label_none">Список пуст</div>';

                }
                else{

                    if(OS.isMobile){

                        inner+='<div id="search_history_header" style="height: 30px; padding: 3px 0 0 0;">БИЛЛИНГ</div>';

                    }
                    else{

                        inner+='<div id="billing_header">';
                            inner+='<div class="billing_header_col" style="width: 50px;"><table><tr><td>ID</td></tr></table></div>';
                            inner+='<div class="billing_header_col"><table><tr><td>Дата и время</td></tr></table></div>';
                            inner+='<div class="billing_header_col" style="width: 130px;"><table><tr><td>Тип операции</td></tr></table></div>';
                            inner+='<div class="billing_header_col"><table><tr><td>Сумма операции в рублях</td></tr></table></div>';
                            inner+='<div class="billing_header_col" style="width: 420px;"><table><tr><td>Источник</td></tr></table></div>';
                            inner+='<div class="billing_header_col"><table><tr><td>Сумма в валюте источника</td></tr></table></div>';
                            inner+='<div class="billing_header_col"><table><tr><td>Баланс после операции</td></tr></table></div>';
                        inner+='</div>';

                    }

                    inner+='<div id="billing_list">';

                    for(index in list){

                        inner+='<div id="billing_item_'+index+'" class="billing_item">';

                            inner+=page_object.action.profile.create.root_content.create.get_billing_row(index);

                        inner+='</div>';

                    }

                    inner+='</div>';

                }

                    inner+='</div>';

                inner+='</div>';

                if(!isset($d('root_face_block'))){

                    let el=addElement({
                        'tag':'div',
                        'id':'root_face_block',
                        'inner':inner,
                        'style':'opacity: 0'
                    });

                    $d('root_content').appendChild(el);

                }
                else if(isset($d('root_face_block'))){

                    $d('root_face_block').innerHTML     =inner;
                    $s('root_face_block').width         ='';

                }

                if(OS.isMobile){

                    $s('root_face_block').top               ='50%';
                    $s('root_face_block').transform         ='translate(-50%,calc(-50% + 30px)';

                }

            },
            // 'billing':function(){
            //
            //     let  data       =page_object.action.profile.data['data']
            //         ,list       =data['billing_list']
            //         ,index
            //         ,inner      ='';
            //
            //     inner+='<div id="root_static_content">';
            //
            //         inner+='<div id="settings_close_button" class="dialog_close_button"'+(OS.isMobile?'':'')+'></div>';
            //
            //         inner+='<div id="billing_block">';
            //
            //             inner+='<div id="billing_header">';
            //                 inner+='<div class="billing_header_col" style="width: 50px;"><table><tr><td>ID</td></tr></table></div>';
            //                 inner+='<div class="billing_header_col"><table><tr><td>Дата и время</td></tr></table></div>';
            //                 inner+='<div class="billing_header_col" style="width: 130px;"><table><tr><td>Тип операции</td></tr></table></div>';
            //                 inner+='<div class="billing_header_col"><table><tr><td>Сумма операции в рублях</td></tr></table></div>';
            //                 inner+='<div class="billing_header_col" style="width: 420px;"><table><tr><td>Источник</td></tr></table></div>';
            //                 inner+='<div class="billing_header_col"><table><tr><td>Сумма в валюте источника</td></tr></table></div>';
            //                 inner+='<div class="billing_header_col"><table><tr><td>Баланс после операции</td></tr></table></div>';
            //             inner+='</div>';
            //
            //             inner+='<div id="billing_list">';
            //
            //     for(index in list){
            //
            //         inner+='<div id="billing_item_'+index+'" class="billing_item">';
            //
            //             inner+=page_object.action.profile.create.root_content.create.get_billing_row(index);
            //
            //         inner+='</div>';
            //
            //     }
            //
            //             inner+='</div>';
            //
            //         inner+='</div>';
            //
            //     inner+='</div>';
            //
            //     if(!isset($d('root_face_block'))){
            //
            //         let el=addElement({
            //             'tag':'div',
            //             'id':'root_face_block',
            //             'inner':inner,
            //             'style':'opacity: 0'
            //         });
            //
            //         $d('root_content').appendChild(el);
            //
            //     }
            //     else if(isset($d('root_face_block'))){
            //
            //         $d('root_face_block').innerHTML     =inner;
            //         $s('root_face_block').width         ='';
            //
            //     }
            //
            // },
            'payment':function(){

                let  data               =page_object.action.profile.data['data']
                    ,currency_list      =data['currency_list']
                    ,wallet_list        =data['wallet_list']
                    ,time               =get_hour(data['timestamp'])
                    ,inner              ='';

                // if(OS.isMobile)
                    inner+='<div id="settings_close_button" class="dialog_close_button"'+(OS.isMobile?' style="margin: -11px 0 0 11px; padding: 0;"':' style="margin: -10px 0 0 10px; padding: 0;"')+'></div>';

                inner+='<div id="root_default_content">';

                    // if(!OS.isMobile)
                    //     inner+='<div id="settings_close_button" class="dialog_close_button"'+(OS.isMobile?'':'')+'></div>';

                    inner+='<div id="profile_payment_block">';

                        inner+='<div class="profile_payment_title">Пополнение баланса Shluham.NET</div>';

                        inner+='<div class="profile_payment_row">';

                            inner+='<div id="profile_payment_step_1" class="profile_payment_col">';

                                inner+='<div class="profile_payment_col_title">1. Выберите способ оплаты</div>';

                                inner+='<div id="profile_payment_wallet_list" class="profile_payment_list">';

                                    inner+='<div id="profile_payment_wallet_sberbank" class="profile_payment_wallet_item" title="Оплата через Сбербанк онлайн"></div>';
                                    inner+='<div id="profile_payment_wallet_qiwi" class="profile_payment_wallet_item" title="Оплата через KIWI"></div>';
                                    inner+='<div id="profile_payment_wallet_btc" class="profile_payment_wallet_item" title="Bitcoin"></div>';
                                    inner+='<div id="profile_payment_wallet_eth" class="profile_payment_wallet_item" title="Ethereum"></div>';
                                    // inner+='<div id="profile_payment_wallet_sberbank" class="profile_payment_wallet_item'+((time>=1&&time<=7)?'_disable':'')+'"'+((time>=1&&time<=7)?' style="opacity: 0.2"':'')+' title="Оплата через Сбербанк онлайн"></div>';
                                    inner+='<div id="profile_payment_wallet_pp" class="profile_payment_wallet_item_disable" title="Оплата через PayPal"></div>';
                                    inner+='<div id="profile_payment_wallet_cc" class="profile_payment_wallet_item_disable" title="Оплата банковскими картами через PayPal"></div>';

                                inner+='</div>';

                                // inner+='<div id="profile_payment_wallet_lock" class="profile_payment_lock">Временно не доступно</div>';

                            inner+='</div>';

                            inner+='<div id="profile_payment_step_2" class="profile_payment_col" style="display: none; opacity: 0;'+(OS.isMobile?'height: 0;':' width: 460px;')+'">';

                                inner+='<div class="profile_payment_col_title" style="margin-bottom: 10px;">2. Сумма и детали пополнения</div>';

                                inner+='<div id="profile_payment_sum_list" class="profile_payment_list">';

                                    // inner+='<div id="profile_payment_sum_500" class="profile_payment_sum_item" style="display: none">500₽</div>';
                                    inner+='<div id="profile_payment_sum_500" class="profile_payment_sum_item">500₽</div>';
                                    inner+='<div id="profile_payment_sum_1000" class="profile_payment_sum_item">1000₽</div>';
                                    inner+='<div id="profile_payment_sum_3000" class="profile_payment_sum_item">3000₽</div>';
                                    inner+='<div id="profile_payment_sum_5000" class="profile_payment_sum_item">5000₽</div>';
                                    inner+='<div id="profile_payment_sum_10000" class="profile_payment_sum_item">10000₽</div>';

                                inner+='</div>';

                                inner+='<div id="profile_payment_wallet_button" class="profile_payment_row" style="display: none; padding: 10px 0 0 0;"></div>';
                                inner+='<div id="profile_payment_wallet_from_block" class="profile_payment_row">';

                                    inner+='<div id="profile_payment_label_type" class="profile_payment_label">Адрес вашего <span id="wallet_name">BTC</span> кошелька:</div>';

                                    inner+='<input id="profile_payment_wallet_from_input" type="text" value="" />';

                                inner+='</div>';

                            inner+='</div>';

                            inner+='<div id="profile_payment_step_3" class="profile_payment_col" style="display: none; opacity: 0;'+(OS.isMobile?'height: 0;':' width: 450px;')+'">';

                                inner+='<div class="profile_payment_col_title">3. Пополнение баланса</div>';

                                inner+='<div id="profile_payment_currency_list" class="profile_payment_list">';

                                    inner+='<div id="profile_payment_wallet_from_block" class="profile_payment_row">';

                                        inner+='<div id="profile_payment_label_please" class="profile_payment_label">Пожалуйста, переведите с вашего кошелька:</div>';

                                        inner+='<div id="wallet_from_text">'+wallet_list['BTC']+'</div>';
                                        inner+='<div class="profile_payment_row">Сумму <span id="wallet_sum_total">'+(currency_list['BTC']*1000).toFixed(5)+'BTC</span></div>';

                                        inner+='<div id="profile_payment_label_service" class="profile_payment_label">На кошелек сервиса Shluham.NET:</div>';
                                        inner+='<div class="profile_payment_row" style="height: 48px;">';
                                            inner+='<div id="wallet_to_text" style="width: auto; display: inline-block; margin-bottom: 2px;">'+wallet_list['BTC']+'</div>';
                                            inner+='<div id="wallet_to_copy_buffer"></div>';
                                            inner+='<div id="wallet_to_text_info">Адрес кошелька скопирован в буфер обмена</div>';
                                        inner+='</div>';
                                        inner+='<div class="profile_payment_message">После того, как мы получим перевод - баланс вашего аккаунта пополнится автоматически.</div>';

                                    inner+='</div>';

                                inner+='</div>';

                                inner+='<div id="profile_payment_controls">';

                                    inner+='<div id="profile_payment_submit" class="profile_payment_button">Все готово</div>';
                                    inner+='<div id="profile_payment_cancel" class="profile_payment_button">Отмена</div>';

                                inner+='</div>';

                            inner+='</div>';

                        inner+='</div>';

                    inner+='</div>';

                inner+='</div>';

                if(!isset($d('root_face_block'))){

                    let el=addElement({
                        'tag':'div',
                        'id':'root_face_block',
                        'inner':inner,
                        'style':'opacity: 0'
                    });

                    $d('root_content').appendChild(el);

                }
                else if(isset($d('root_face_block'))){

                    $d('root_face_block').innerHTML     =inner;
                    $s('root_face_block').width         ='';

                }

                if(OS.isMobile){

                    $s('root_face_block').top               ='50%';
                    $s('root_face_block').transform         ='translate(-50%,calc(-50%)';
                    $s('root_face_block').maxHeight         ='calc(100vh - 70px)';
                    $s('root_default_content').height       ='auto';
                    $s('root_default_content').maxHeight    ='';

                }

            },
            'feedback':function(){

                var  inner      =''
                    ,data       =page_object.action.profile.data;

                inner+='<div id="dialog_content_block" class="dialog_row">Ваш email:</div>';

                inner+='<div id="dialog_email_block" class="dialog_row">';
                    // inner+='<div id="dialog_email_label" class="dialog_row_label">';
                    //     inner+='<span>Email</span>';
                    // inner+='</div>';
                    // inner+='<div id="dialog_email_input_block" class="dialog_row_input">';
                        inner+='<input type="text" id="dialog_email_input_text" class="dialog_row_input_text" value="'+stripSlashes(data['data']['email'])+'" />';
                    // inner+='</div>';
                inner+='</div>';

                inner+='<div id="dialog_content_block" class="dialog_row">Сообщение:</div>';
                inner+='<div id="dialog_content_block" class="dialog_row">';
                    // inner+='<div id="dialog_content_label" class="dialog_row_label">';
                    //     inner+='<span>Сообщение</span>';
                    // inner+='</div>';
                    // inner+='<div id="dialog_content_input_block" class="dialog_row_input">';
                        inner+='<textarea id="dialog_content_input_text" class="dialog_row_textarea"></textarea>';
                    // inner+='</div>';
                inner+='</div>';

                page_object.action.profile.action.root_face_block.action.feedback.dialog_index=page_object.dialog.init({
                    'title':'Обратная связь',
                    'inner':inner,
                    'send':function(){

                        page_object.action.profile.action.root_face_block.action.feedback.send(page_object.action.profile.action.root_face_block.action.feedback.dialog_index);

                    },
                    'cancel':function(){

                        var lang_obj=page_object.content[page_object.lang];

                        setUrl(lang_obj['title']+' | '+lang_obj['loading'],'/profile/'+page_object.action.profile.data['user']['login']+'/face');

                    },
                    'on_create':function(){

                        page_object.action.profile.action.root_face_block.action.feedback.init();

                    }
                });

            },
            'city':function(){

                // page_object.action.root.create.root_content.create.root_content.reset_root_face_block();

                let  data       =page_object.action.profile.data['data']
                    ,inner      =''
                    ,el
                    ,list       =data['city_list']
                    ,index;

                // if(OS.isMobile)
                inner+='<div id="settings_close_button" class="dialog_close_button"'+(OS.isMobile?' style="margin: -11px 0 0 11px; padding: 0;"':' style="margin: -10px 0 0 10px; padding: 0;"')+'></div>';

                inner+='<div id="root_default_content">';

                    inner+='<h1 style="text-align: center">Статистика городов по количеству шлюх</h1>';

                    inner+='<div id="root_city_stats_block">';

                        inner+='<div id="root_city_stats_header" style="width: 100%">';

                            inner+='<div class="root_city_stats_header_col" style="width: 10%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">#</td></tr></table></div>';
                            inner+='<div class="root_city_stats_header_col" style="width: 50%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">Город</td></tr></table></div>';
                            inner+='<div class="root_city_stats_header_col" style="width: 40%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">Анкет, шт</td></tr></table></div>';
                            // inner+='<div class="root_city_stats_header_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">Фотографий, шт</td></tr></table></div>';

                        inner+='</div>';

                for(index in list){

                    inner+='<div class="root_city_stats_row_item" style="width: 100%">';

                        inner+='<div class="root_city_stats_row_item_col" style="width: 10%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(parseInt(index)+1)+'</td></tr></table></div>';
                        inner+='<div class="root_city_stats_row_item_col" style="width: 50%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['name'])+'</td></tr></table></div>';
                        inner+='<div class="root_city_stats_row_item_col" style="width: 40%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['len'])+'</td></tr></table></div>';
                        // inner+='<div class="root_city_stats_row_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['image_len'])+'</td></tr></table></div>';

                    inner+='</div>';

                }

                            inner+='</div>';
                    inner+='</div>';

                if(!isset($d('root_face_block'))){

                    el=addElement({
                        'tag':'div',
                        'id':'root_face_block',
                        'inner':inner,
                        'style':'opacity: 0'
                    });

                    $d('root_content').appendChild(el);

                }
                else if(isset($d('root_face_block')))
                    $d('root_face_block').innerHTML=inner;

            }
        }
    },
    'set_action':function(){

        page_object.action.profile.action.root_content.action.init();
        page_object.action.profile.action.root_face_block.action.init();

    },
    'show':function(){

        setTimeout(page_object.action.profile.action.root_content.show,40);
        setTimeout(page_object.action.profile.action.root_face_block.show,400);

    }
};
