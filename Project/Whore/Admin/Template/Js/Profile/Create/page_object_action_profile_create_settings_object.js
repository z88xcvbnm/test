page_object.action.profile.create.settings={
    'init':function(){

        page_object.action.profile.create.settings.position.init();
        page_object.action.profile.create.settings.create.init();
        page_object.action.profile.create.settings.set_action.init();
        page_object.action.profile.create.settings.show();

    },
    'position':{
        'init':function(){

            page_object.action.profile.create.settings.position.settings();

        },
        'settings':function(){

            page_object.action.profile.position.settings.o        =0;
            page_object.action.profile.position.settings.w        =winSize.winWidth-page_object.action.profile.position.settings.m.l-page_object.action.profile.position.settings.m.r;
            page_object.action.profile.position.settings.h        =winSize.winHeight
                                                                    -page_object.action.profile.position.header.h
                                                                    -page_object.action.profile.position.settings.m.t;
            page_object.action.profile.position.settings.x        =page_object.action.profile.position.settings.m.l;
            page_object.action.profile.position.settings.y        =page_object.action.profile.position.header.h
                                                                    +page_object.action.profile.position.settings.m.t;

        }
    },
    'create':{
        'init':function(){

            page_object.action.profile.create.settings.create.settings();

        },
        'settings':function(){

            if(!isset($d('settings'))){

                let  lang_obj           =page_object.action.profile.content[page_object.lang]
                    ,data               =page_object.action.profile.data
                    ,login              =data['user']['login']
                    ,image_ID           =data['user']['image_ID']
                    ,name               =data['settings']['name']
                    ,surname            =data['settings']['surname']
                    ,email              =data['settings']['email']
                    ,image_link         =(image_ID===0)?'/Project/Whore/Admin/Template/Images/Avatar/admin_icon.png':'/'+data['user']['image_dir']+'/'+data['user']['image_item_ID_list']['preview']['ID']
                    ,inner              =''
                    ,style              =''
                    ,el;

                if(OS.isMobile)
                    style+='width: calc(100% - 10px);';
                else{

                    style+='width: '+page_object.action.profile.position.settings.w+'px;';
                    style+='height: '+page_object.action.profile.position.settings.h+'px;';
                    style+='transform: translate('+page_object.action.profile.position.settings.x+'px, '+page_object.action.profile.position.settings.y+'px);';

                }

                style+='opacity: '+page_object.action.profile.position.settings.o+';';

                if(OS.isMobile)
                    inner+='<div id="settings_close_button" class="dialog_close_button" style="margin: -11px 0 0 11px; padding: 0;"></div>';

                inner+='<div id="settings_content">';

                    inner+='<div id="settings_content_container">';

                        if(!OS.isMobile)
                            inner+='<div id="settings_close_button" class="dialog_close_button"'+(OS.isMobile?' style="margin: -11px 0 0 11px; padding: 0;"':' style="margin: -10px 0 0 10px; padding: 0;"')+'></div>';

                        inner+='<div id="settings_container">';

                            inner+='<div id="settings_image_block">';
                                inner+='<div id="settings_image" style="background: url('+image_link+') 50% 50% no-repeat; background-size: contain;"></div>';
                                inner+='<div id="settings_image_edit" title="'+stripSlashes(lang_obj['settings_image_edit_title'])+'"></div>';
                            inner+='</div>';

                            inner+='<div id="settings_list">';

                                inner+='<div id="settings_login_block" class="settings_row" style="border: none; margin: 0;">';

                                    inner+='<div class="settings_row_child">';
                                        inner+='<div class="settings_label">'+stripSlashes(lang_obj['settings_login_label'])+'</div>';
                                        inner+='<div id="settings_login" class="settings_value">'+stripSlashes(login)+'</div>';
                                    inner+='</div>';

                                    inner+='<div id="settings_login_edit" class="settings_row_edit" title="'+stripSlashes(lang_obj['settings_login_edit_title'])+'"></div>';

                                inner+='</div>';

                                inner+='<div id="settings_email_block" class="settings_row">';

                                    inner+='<div class="settings_row_child">';
                                        inner+='<div class="settings_label">'+stripSlashes(lang_obj['settings_email_label'])+'</div>';
                                        inner+='<div id="settings_email" class="settings_value">'+stripSlashes(email)+'</div>';
                                    inner+='</div>';

                                    inner+='<div id="settings_email_edit" class="settings_row_edit" title="'+stripSlashes(lang_obj['settings_email_edit_title'])+'"></div>';

                                inner+='</div>';

                                inner+='<div id="settings_name_block" class="settings_row">';

                                    inner+='<div class="settings_row_child">';
                                        inner+='<div class="settings_label">'+stripSlashes(lang_obj['settings_name_label'])+'</div>';
                                        inner+='<div id="settings_name" class="settings_value">'+stripSlashes(name)+'</div>';
                                    inner+='</div>';

                                    inner+='<div class="settings_row_child">';
                                        inner+='<div class="settings_label">'+stripSlashes(lang_obj['settings_surname_label'])+'</div>';
                                        inner+='<div id="settings_surname" class="settings_value">'+stripSlashes(surname)+'</div>';
                                    inner+='</div>';

                                    inner+='<div id="settings_name_edit" class="settings_row_edit" title="'+stripSlashes(lang_obj['settings_name_edit_title'])+'"></div>';

                                inner+='</div>';

                                inner+='<div id="settings_pas_block" class="settings_row">';

                                    inner+='<div id="settings_pas_edit_block" class="settings_row_child">';
                                        inner+='<div id="settings_pas_edit">'+stripSlashes(lang_obj['settings_pas_label'])+'</div>';
                                        inner+='<div id="settings_account_remove">Удалить профиль</div>';
                                    inner+='</div>';

                                inner+='</div>';

                            inner+='</div>';

                        inner+='</div>';

                        inner+='<div class="settings_promo">';
                            inner+='<p><b>Расскажите о нас друзьям!</b></p>';
                            inner+='<p>За каждого пользователя, который при регистрации укажет Ваше имя пользователя (<b>'+page_object.action.profile.data['user']['login']+'</b>), Вы получите <b>100 руб.</b> на счёт. Пользователь, которого Вы пригласили - 50 руб.</p>';
                        inner+='</div>';

                    inner+='</div>';

                inner+='</div>';

                el=addElement({
                    'tag':'div',
                    'id':'settings',
                    'inner':inner,
                    'style':style
                });

                $d('all').appendChild(el);

            }

        }
    },
    'set_action':{
        'init':function(){

            page_object.action.profile.create.settings.set_action.settings();

        },
        'settings':function(){

            if(isset($d('settings_image_edit')))
                $d('settings_image_edit').onclick=page_object.action.profile.action.settings.click.edit.image_add.init;

            if(isset($d('settings_login_edit')))
                $d('settings_login_edit').onclick=page_object.action.profile.action.settings.click.edit.login.init;

            if(isset($d('settings_email_edit')))
                $d('settings_email_edit').onclick=page_object.action.profile.action.settings.click.edit.email.init;

            if(isset($d('settings_name_edit')))
                $d('settings_name_edit').onclick=page_object.action.profile.action.settings.click.edit.name.init;

            if(isset($d('settings_pas_edit')))
                $d('settings_pas_edit').onclick=page_object.action.profile.action.settings.click.edit.password.init;

            if(isset($d('settings_account_remove')))
                $d('settings_account_remove').onclick=page_object.action.profile.action.settings.click.account_remove.init;

            if(isset($d('settings_close_button')))
                $d('settings_close_button').onclick=page_object.action.profile.action.header.logo.click;

        }
    },
    'show':function(){

        setTimeout(page_object.action.profile.action.settings.show,40);

    }
};