page_object.action.auth={
    'w':300,
    'h':140,
    'x':0,
    'y':0,
    'is_show':false,
    'script_list':{
        'core':{
            'root':'/show/js/root/init.js'
        }
    },
    'style_list':[
        '/show/css/system/root.css',
        '/show/css/system/dialog_block.css'
    ],
    'section_change_workflow':{},
    'content':{
        'ru':{
            'title':'Авторизация',
            'login':'Логин',
            'password':'Пароль',
            'submit':'Войти',
            'wait':'Подождите',
            'keep':'Запомнить меня',
            'forgot':'Восстановить пароль',
            'forgot_title':'Восстановление пароля',
            'reset_title':'Восстановление пароля',
            'email':'Email',
            'cancel':'Отмена',
            'forgot_info':'Инструкции по восстановлению пароля высланы Вам на почту.',
            'reset_info':'Ваш пароль изменен.',
            'pas_old':'Старый пароль',
            'pas1':'Новый пароль',
            'pas2':'Повторите новый пароль',
            'save':'Сохранить'
        },
        'en':{
            'title':'Sign In',
            'login':'Login',
            'password':'Password',
            'submit':'Sign In',
            'wait':'Wait',
            'keep':'Remember me',
            'forgot':'Forgot password',
            'forgot_title':'Forgot password',
            'reset_title':'Forgot password',
            'email':'Email',
            'cancel':'Cancel',
            'forgot_info':'How to recover password sent to you in the mail.',
            'reset_info':'Your password is changed.',
            'pas_old':'Old password',
            'pas1':'New password',
            'pas2':'Repeat new password',
            'save':'Save'
        }
    },
    'reset':function(){

        page_object.action.auth.w=300;
        page_object.action.auth.h=140;
        page_object.action.auth.x=0;
        page_object.action.auth.y=0;

    },
    'init':function(data){

        var temp={
            'script_list':config.updateScriptsList(page_object.action.auth.script_list, 'Root'),
            'style_list':config.updateScriptsList(page_object.action.auth.style_list, 'Root'),
            'init_list':[
                function(){

                    if (typeof config !== 'undefined' && $.isFunction(config.updateSectionChangeWorkflow)) {
                        page_object.action.root.section_change_workflow = config.updateSectionChangeWorkflow(page_object.action.root.section_change_workflow);
                    }

                    page_object.action.root.init(data);

                }
            ]
        };

        page_object.require.init(temp);

    },
    'run':function(data){

        if(isset(page_object.action.profile))
            if(isset(page_object.action.profile.data['user']))
                if(isset(page_object.action.profile.section_change_workflow))
                    page_object.action.profile.section_change_workflow.profile_logout.init();

        if(isset(page_object.action.admin))
            if(isset(page_object.action.admin.data['user']))
                if(isset(page_object.action.admin.section_change_workflow))
                    page_object.action.admin.section_change_workflow.admin_logout.init();

        switch(data['action']){

            case'auth':{

                page_object.action.auth.action.keep.is_keep         =true;
                page_object.action.auth.action.forgot.is_forgot     =false;
                page_object.action.auth.action.reset.is_reset       =false;

                break;

            }

            case'forgot':{

                page_object.action.auth.action.keep.is_keep         =true;
                page_object.action.auth.action.forgot.is_forgot     =false;
                page_object.action.auth.action.reset.is_reset       =true;

                break;

            }

        }

        if(!isset($d('auth_block'))){

            page_object.action.auth.create.bg();
            page_object.action.auth.create.auth();

            page_object.dialog.index++;

        }

        page_object.action.auth.action.input.init();

        setTimeout(page_object.action.auth.action.show,300);

    },
    'create':{
        'auth':function(){

            var  inner          =''
                ,lang_obj       =page_object.action.auth.content[page_object.lang]
                ,el
                ,m_l            =Math.ceil((winSize.winWidth-page_object.action.auth.w)/2)
                ,z_index        =(page_object.dialog.index+1)*2+2+200;

            inner+='<div id="dialog_close_button" class="dialog_close_button"></div>';

            inner+='<div id="auth_title" class="block_title">'+stripSlashes((page_object.action.auth.action.reset.is_reset)?lang_obj['reset_title']:lang_obj['title'])+'</div>';

            inner+='<div id="auth_login_block" class="auth_row"'+(!page_object.action.auth.action.forgot.is_forgot&&!page_object.action.auth.action.reset.is_reset?'':' style="opacity: 0; margin: 0; height: 0;"')+'>';
                inner+='<div id="auth_login_label" class="input_label">'+stripSlashes(lang_obj['login'])+'</div>';
                inner+='<input type="text" id="auth_login_text" name="login" class="input_text" value="" />';
            inner+='</div>';

            inner+='<div id="auth_password_block" class="auth_row"'+(!page_object.action.auth.action.forgot.is_forgot&&!page_object.action.auth.action.reset.is_reset?'':' style="opacity: 0; margin: 0; height: 0;"')+'>';
                inner+='<div id="auth_password_label" class="input_label">'+stripSlashes(lang_obj['password'])+'</div>';
                inner+='<input type="password" id="auth_password_text" name="password" class="input_text" value="" />';
            inner+='</div>';

            inner+='<div id="auth_keep_block" class="auth_row"'+(!page_object.action.auth.action.forgot.is_forgot&&!page_object.action.auth.action.reset.is_reset?'':' style="opacity: 0; margin: 0; height: 0;"')+'>';
                inner+='<div id="auth_keep_button">';
                    inner+='<div id="auth_keep" class="auth_keep_active"></div>';
                    inner+='<div id="auth_keep_label">'+stripSlashes(lang_obj['keep'])+'</div>';
                inner+='</div>';
                inner+='<div id="auth_forgot_button">'+stripSlashes(lang_obj['forgot'])+'</div>';
            inner+='</div>';

            inner+='<div id="auth_submit_block" class="auth_row"'+(!page_object.action.auth.action.forgot.is_forgot&&!page_object.action.auth.action.reset.is_reset?'':' style="opacity: 0; margin: 0; height: 0;"')+'>';
                inner+='<div id="auth_submit_button" class="submit_button" style="margin-bottom: 0">'+lang_obj['submit']+'</div>';
            inner+='</div>';

            inner+='<div id="auth_email_block" class="auth_row"'+(page_object.action.auth.action.forgot.is_forgot?'':' style="opacity: 0; margin: 0; height: 0;"')+'>';
            inner+='<div id="auth_email_label" class="input_label">'+stripSlashes(lang_obj['email'])+'</div>';
            inner+='<input type="text" id="auth_email_text" class="input_text" value="" />';
            inner+='</div>';

            inner+='<div id="auth_forgot_submit_block" class="auth_row"'+(page_object.action.auth.action.forgot.is_forgot?'':' style="opacity: 0; margin: 0; height: 0;"')+'>';

            if(OS.isMobile){

                inner+='<div id="auth_forgot_submit_button" class="submit_button" style="">'+lang_obj['forgot']+'</div>';
                inner+='<div id="auth_forgot_cancel_button" class="submit_button" style="margin-bottom: 0">'+lang_obj['cancel']+'</div>';

            }
            else{

                inner+='<div id="auth_forgot_submit_button" class="submit_half_button" style="width: calc(50% - 1%); margin-right: 0;">'+lang_obj['forgot']+'</div>';
                inner+='<div id="auth_forgot_cancel_button" class="submit_half_button" style="width: calc(50% - 1%); margin-left: 2%;">'+lang_obj['cancel']+'</div>';

            }

            inner+='</div>';

            inner+='<div id="auth_pas1_block" class="auth_row"'+(page_object.action.auth.action.reset.is_reset?'':' style="opacity: 0; margin: 0; height: 0;"')+'>';
                inner+='<div id="auth_pas1_label" class="input_label">'+stripSlashes(lang_obj['pas1'])+'</div>';
                inner+='<input type="password" id="auth_pas1_text" class="input_text" value="" />';
            inner+='</div>';

            inner+='<div id="auth_pas2_block" class="auth_row"'+(page_object.action.auth.action.reset.is_reset?'':' style="opacity: 0; margin: 0; height: 0;"')+'>';
                inner+='<div id="auth_pas2_label" class="input_label">'+stripSlashes(lang_obj['pas2'])+'</div>';
                inner+='<input type="password" id="auth_pas2_text" class="input_text" value="" />';
            inner+='</div>';

            inner+='<div id="auth_reset_submit_block" class="auth_row"'+(page_object.action.auth.action.reset.is_reset?'':' style="opacity: 0; margin: 0; height: 0;"')+'>';

            if(OS.isMobile){

                inner+='<div id="auth_reset_submit_button" class="submit_button" style="">'+lang_obj['save']+'</div>';
                inner+='<div id="auth_reset_cancel_button" class="submit_button" style="margin-bottom: 0">'+lang_obj['cancel']+'</div>';

            }
            else{

                inner+='<div id="auth_reset_submit_button" class="submit_half_button" style="width: calc(50% - 1%); margin-right: 0;">'+lang_obj['save']+'</div>';
                inner+='<div id="auth_reset_cancel_button" class="submit_half_button" style="width: calc(50% - 1%); margin-left: 2%;">'+lang_obj['cancel']+'</div>';

            }

                // inner+='<div id="auth_forgot_submit_button" class="submit_half_button" style="width: calc(50% - 1%); margin-right: 0;">'+lang_obj['forgot']+'</div>';
                // inner+='<div id="auth_forgot_cancel_button" class="submit_half_button" style="width: calc(50% - 1%); margin-left: 2%;">'+lang_obj['cancel']+'</div>';

            inner+='</div>';

            el=addElement({
                'tag':'div',
                'id':'auth_block',
                'class':'auth_block',
                'inner':inner,
                'style':'margin: 0; z-index: '+z_index+';'
            });

            $d('all').appendChild(el);

            $d('dialog_close_button').onclick=page_object.action.root.action.header.logo.click;

            page_object.action.auth.x=m_l;

        },
        'bg':function(){

            let z_index=(page_object.dialog.index+1)*2+1+200;

            let el=addElement({
                'tag':'div',
                'id':'bg_all_auth',
                'class':'bg_all',
                'style':'background: #1b93db; z-index: '+z_index
            });

            $d('all').appendChild(el);

            $d('bg_all_auth').onclick=page_object.action.root.action.header.logo.click;

        }
    },
    'action':{
        'show':function(){

            if(isset($d('auth_block'))){

                if(isset($d('bg_all_auth')))
                    $s('bg_all_auth').opacity=.5;

                page_object.action.auth.y+=Math.ceil((winSize.winHeight-page_object.action.auth.h)/2);

                // $s('auth_block').transform          ='translate('+page_object.action.auth.x+'px,'+page_object.action.auth.y+'px)';
                $s('auth_block').opacity            =1;

                page_object.action.auth.is_show         =true;

                setTimeout(page_object.link.preload.un_show,300);

            }

        },
        'un_show':function(remove){

            if(isset($d('auth_block'))){

                if(isset($d('bg_all_auth'))){

                    $s('bg_all_auth').opacity=0;

                    setTimeout(function(){

                        removeElement($d('bg_all_auth'));

                    },340);

                }

                $s('auth_block').transform          ='translate('+page_object.action.auth.x+'px,-'+page_object.action.auth.y+'px)';
                $s('auth_block').opacity            =0;

                page_object.action.auth.x           =0;
                page_object.action.auth.y           =0;

                page_object.action.auth.is_show     =false;

            }

            if(isset(remove))
                if(remove)
                    page_object.action.auth.action.remove();

        },
        'remove':function(){

            if(isset($d('auth_block')))
                removeElement($d('auth_block'));

        },
        'resize':function(){

            return true;

            if(isset($d('auth_block'))&&page_object.action.auth.is_show){

                page_object.action.auth.h=elementSize.height($d('auth_block'));

                var  m_t        =page_object.action.auth.y-Math.ceil((winSize.winHeight-page_object.action.auth.h)/2)
                    ,m_l        =page_object.action.auth.x-Math.ceil((winSize.winWidth-page_object.action.auth.w)/2);

                page_object.action.auth.y-=m_t;
                page_object.action.auth.x-=m_l;

                $s('auth_block').transform='translate('+page_object.action.auth.x+'px,'+page_object.action.auth.y+'px)';

            }

        },
        'input':{
            'init':function(){

                $d('auth_login_text').onfocus               =page_object.action.auth.action.input.focus;
                $d('auth_login_text').onblur                =page_object.action.auth.action.input.blur;
                $d('auth_login_text').onkeydown             =page_object.action.auth.action.input.key_down;
                $d('auth_login_text').onkeyup               =page_object.action.auth.action.input.key_up;
                $d('auth_login_text').onchange              =page_object.action.auth.action.input.change;

                $d('auth_password_text').onfocus            =page_object.action.auth.action.input.focus;
                $d('auth_password_text').onblur             =page_object.action.auth.action.input.blur;
                $d('auth_password_text').onkeydown          =page_object.action.auth.action.input.key_down;
                $d('auth_password_text').onkeyup            =page_object.action.auth.action.input.key_up;
                $d('auth_password_text').onchange           =page_object.action.auth.action.input.change;

                $d('auth_email_text').onfocus               =page_object.action.auth.action.input.focus;
                $d('auth_email_text').onblur                =page_object.action.auth.action.input.blur;
                $d('auth_email_text').onkeydown             =page_object.action.auth.action.input.key_down;
                $d('auth_email_text').onkeyup               =page_object.action.auth.action.input.key_up;

                $d('auth_pas1_text').onfocus                =page_object.action.auth.action.input.focus;
                $d('auth_pas1_text').onblur                 =page_object.action.auth.action.input.blur;
                $d('auth_pas1_text').onkeydown              =page_object.action.auth.action.input.key_down;
                $d('auth_pas1_text').onkeyup                =page_object.action.auth.action.input.key_up;

                $d('auth_pas2_text').onfocus                =page_object.action.auth.action.input.focus;
                $d('auth_pas2_text').onblur                 =page_object.action.auth.action.input.blur;
                $d('auth_pas2_text').onkeydown              =page_object.action.auth.action.input.key_down;
                $d('auth_pas2_text').onkeyup                =page_object.action.auth.action.input.key_up;

                $d('auth_submit_button').onclick            =page_object.action.auth.action.submit;

                $d('auth_keep_button').onclick              =page_object.action.auth.action.keep.init;

                $d('auth_forgot_button').onclick            =page_object.action.auth.action.forgot.init;
                $d('auth_forgot_submit_button').onclick     =page_object.action.auth.action.forgot.send;
                $d('auth_forgot_cancel_button').onclick     =page_object.action.auth.action.forgot.init;

                if(isset($d('auth_reset_submit_button'))){

                    $d('auth_reset_submit_button').onclick      =page_object.action.auth.action.reset.send;
                    $d('auth_reset_cancel_button').onclick      =page_object.action.auth.action.reset.init;

                }

            },
            'focus':function(){

                var  key_list   =this.id.split('_')
                    ,key        =key_list[1]
                    ,el_label   =$d('auth_'+key+'_label');

                if(this.value=='')
                    el_label.style.opacity=.5;
                else
                    el_label.style.opacity=0;

            },
            'blur':function(){

                var  key_list   =this.id.split('_')
                    ,key        =key_list[1]
                    ,el_label   =$d('auth_'+key+'_label');

                if(this.value=='')
                    el_label.style.opacity=1;
                else
                    el_label.style.opacity=0;

            },
            'key_down':function(event){

                var  key_list   =this.id.split('_')
                    ,key        =key_list[1]
                    ,el_label   =$d('auth_'+key+'_label')
                    ,key_ID     =getKey(event);

                if(key_ID==13)
                    page_object.action.auth.action.submit();

                if(this.value=='')
                    el_label.style.opacity=.5;
                else
                    el_label.style.opacity=0;

            },
            'key_up':function(){

                var  key_list   =this.id.split('_')
                    ,key        =key_list[1]
                    ,el_label   =$d('auth_'+key+'_label');

                if(key=='email'){

                    if(this.value.length>=8&&isEmail(this.value)&&this.value.length<=64)
                        this.style.border='1px solid #74b65f';
                    else
                        this.style.border='';

                }
                else if(key=='login'){

                    if(this.value.length>=4&&isLogin(this.value)&&this.value.length<=32)
                        this.style.border='1px solid #74b65f';
                    else
                        this.style.border='';

                }
                else if(key=='password'){

                    if(this.value.length>=6&&this.value.length<=32)
                        this.style.border='1px solid #74b65f';
                    else
                        this.style.border='';

                }
                else if(key=='pas1'||key=='pas2'){

                    if(this.value.length>=6&&this.value.length<=32)
                        this.style.border='1px solid #74b65f';
                    else
                        this.style.border='';

                    var  pas_1          =$v('auth_pas1_text')
                        ,pas_2          =$v('auth_pas2_text')
                        ,pas_1_len      =pas_1.length
                        ,pas_2_len      =pas_2.length;

                    if(pas_1_len>=6&&pas_1_len<=32&&pas_2_len>=6&&pas_2_len<=32){

                        if(pas_1==pas_2){

                            $s('auth_pas1_text').border     ='1px solid #74b65f';
                            $s('auth_pas2_text').border     ='1px solid #74b65f';

                        }
                        else{

                            $s('auth_pas1_text').border     ='1px solid #e14141';
                            $s('auth_pas2_text').border     ='1px solid #e14141';

                        }

                    }

                }

                if(this.value=='')
                    el_label.style.opacity=.5;
                else
                    el_label.style.opacity=0;

            },
            'change':function(){

                var  key_list   =this.id.split('_')
                    ,key        =key_list[1];

                if(this.value!=''){

                    $s('auth_login_label').opacity      =0;
                    $s('auth_password_label').opacity   =0;

                }

            }
        },
        'submit':function(){

            if(!page_object.action.root.action.root_face_block.action.disclaimer.init())
                return false;

            var lang_obj=page_object.action.auth.content[page_object.lang];

            if($d('auth_login_text').value!==''&&$d('auth_password_text').value!==''){

                $d('auth_submit_button').setAttribute('class','submit_button_enable');
                $d('auth_submit_button').innerHTML=lang_obj['wait'];

                re_captcha.check('auth',function(token){

                    var  post='';

                    post+='login='+uniEncode($d('auth_login_text').value.toLowerCase());
                    post+='&password='+uniEncode($d('auth_password_text').value);
                    post+='&keep='+uniEncode(page_object.action.auth.action.keep.is_keep?1:0);
                    post+='&re_captcha_token='+uniEncode(token);

                    trace(post);

                    send({
                        'scriptPath':'/api/json/auth',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            var  data_temp
                                ,data;

                            data_temp       =j.responseText;
                            trace(data_temp);
                            data            =jsonDecode(data_temp);

                            if(isset(data['error'])){

                                if(isset(data['error']['data']))
                                    if(isset(data['error']['data']['info']))
                                        switch(data['error']['data']['info']){

                                            case'Email was not confirm':{

                                                var  inner=''
                                                    ,dialog_index;

                                                inner+='<div class="dialog_row">';

                                                    inner+='Для успешной авторизации, пожалуйста, подтвердите свой адрес электронной почты, перейдя по ссылке из письма, которое мы Вам отправили';

                                                inner+='</div>';

                                                dialog_index=page_object.dialog.init({
                                                    'title':'Ошибка',
                                                    'inner':inner,
                                                    'ok':function(){

                                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                                    }
                                                });

                                                break;

                                            }

                                        }

                                $s('auth_login_text').borderColor        ='#e14141';
                                $s('auth_password_text').borderColor     ='#e14141';

                                $d('auth_submit_button').setAttribute('class','submit_button');
                                $d('auth_submit_button').innerHTML=lang_obj['submit'];

                            }
                            else if(isset(data['redirect'])){

                                //var lang_obj=page_object.content[page_object.lang];

                                setUrl(data['title'],data['redirect']);

                                switch(data['type']){

                                    case'profile':{

                                        break;

                                    }

                                    default:{

                                        page_object.action.root.action.root_content.un_show(true);

                                        break;

                                    }

                                }

                                page_object.action.root.action.header_menu.un_show(true);

                                page_object.action.auth.action.un_show(true);

                                // if(OS.isMobile)
                                //     document.location.reload();

                            }

                        }
                    });


                });

            }
            else{

                $s('auth_login_text').borderColor        ='#e14141';
                $s('auth_password_text').borderColor     ='#e14141';

                $d('auth_submit_button').setAttribute('class','submit_button');
                $d('auth_submit_button').innerHTML=lang_obj['submit'];

            }

        },
        'keep':{
            'is_keep':true,
            'init':function(){

                if(page_object.action.auth.action.keep.is_keep){

                    page_object.action.auth.action.keep.is_keep=false;

                    $d('auth_keep').setAttribute('class','auth_keep');

                    $s('auth_keep_button').opacity='';

                }
                else{

                    page_object.action.auth.action.keep.is_keep=true;

                    $d('auth_keep').setAttribute('class','auth_keep_active');

                    $s('auth_keep_button').opacity=1;

                }

            }
        },
        'forgot':{
            'is_forgot':false,
            'init':function(){

                var lang_obj=page_object.action.auth.content[page_object.lang];

                if(page_object.action.auth.action.forgot.is_forgot){

                    page_object.action.auth.action.forgot.is_forgot=false;

                    $d('auth_email_text').value                 ='';
                    $d('auth_login_text').value                 ='';
                    $d('auth_password_text').value              ='';

                    $s('auth_email_text').border                ='';
                    $s('auth_login_text').border                ='';
                    $s('auth_password_text').border             ='';

                    $s('auth_email_label').opacity              =1;
                    $s('auth_login_label').opacity              =1;
                    $s('auth_password_label').opacity           =1;

                    $d('auth_title').innerHTML                  =stripSlashes(lang_obj['title']);

                    $s('auth_login_block').height               ='';
                    $s('auth_login_block').opacity              ='';
                    $s('auth_login_block').margin               ='';

                    $s('auth_password_block').height            ='';
                    $s('auth_password_block').opacity           ='';
                    $s('auth_password_block').margin            ='';

                    $s('auth_keep_block').height                ='';
                    $s('auth_keep_block').opacity               ='';
                    $s('auth_keep_block').margin                ='';

                    $s('auth_submit_block').height              ='';
                    $s('auth_submit_block').opacity             ='';
                    $s('auth_submit_block').margin              ='';


                    $s('auth_email_block').height               =0;
                    $s('auth_email_block').opacity              =0;
                    $s('auth_email_block').margin               =0;

                    $s('auth_forgot_submit_block').height       =0;
                    $s('auth_forgot_submit_block').opacity      =0;
                    $s('auth_forgot_submit_block').margin       =0;


                    $s('auth_pas1_block').height                =0;
                    $s('auth_pas1_block').opacity               =0;
                    $s('auth_pas1_block').margin                =0;

                    $s('auth_pas2_block').height                =0;
                    $s('auth_pas2_block').opacity               =0;
                    $s('auth_pas2_block').margin                =0;

                    $s('auth_reset_submit_block').height        =0;
                    $s('auth_reset_submit_block').opacity       =0;
                    $s('auth_reset_submit_block').margin        =0;

                }
                else{

                    page_object.action.auth.action.forgot.is_forgot=true;

                    $d('auth_email_text').value                 ='';
                    $d('auth_login_text').value                 ='';
                    $d('auth_password_text').value              ='';

                    $s('auth_email_text').border                ='';
                    $s('auth_login_text').border                ='';
                    $s('auth_password_text').border             ='';

                    $s('auth_email_label').opacity              =1;
                    $s('auth_login_label').opacity              =1;
                    $s('auth_password_label').opacity           =1;

                    $d('auth_title').innerHTML                  =stripSlashes(lang_obj['forgot_title']);

                    $s('auth_login_block').height               =0;
                    $s('auth_login_block').opacity              =0;
                    $s('auth_login_block').margin               =0;

                    $s('auth_password_block').height            =0;
                    $s('auth_password_block').opacity           =0;
                    $s('auth_password_block').margin            =0;

                    $s('auth_keep_block').height                =0;
                    $s('auth_keep_block').opacity               =0;
                    $s('auth_keep_block').margin                =0;

                    $s('auth_submit_block').height              =0;
                    $s('auth_submit_block').opacity             =0;
                    $s('auth_submit_block').margin              =0;


                    $s('auth_email_block').height               ='';
                    $s('auth_email_block').opacity              ='';
                    $s('auth_email_block').margin               ='';

                    $s('auth_forgot_submit_block').height       ='';
                    $s('auth_forgot_submit_block').opacity      ='';
                    $s('auth_forgot_submit_block').margin       ='';


                    $s('auth_pas1_block').height                =0;
                    $s('auth_pas1_block').opacity               =0;
                    $s('auth_pas1_block').margin                =0;

                    $s('auth_pas2_block').height                =0;
                    $s('auth_pas2_block').opacity               =0;
                    $s('auth_pas2_block').margin                =0;

                    $s('auth_reset_submit_block').height        =0;
                    $s('auth_reset_submit_block').opacity       =0;
                    $s('auth_reset_submit_block').margin        =0;

                }

                page_object.action.auth.action.resize();
                
            },
            'send':function(){

                var  lang_obj       =page_object.action.auth.content[page_object.lang]
                    ,email          =$v('auth_email_text')
                    ,email_len      =email.length
                    ,error          =false;

                if(email_len<8&&email_len>64&&isEmail(email)){

                    $s('auth_email_text').border='1px solid #e14141';

                    error=true;

                }

                if(!error){

                    $d('auth_forgot_submit_button').setAttribute('class','submit_half_button_enable');
                    $d('auth_forgot_submit_button').onclick=function(){};

                    $d('auth_forgot_cancel_button').setAttribute('class','submit_half_button_enable');
                    $d('auth_forgot_cancel_button').onclick=function(){};

                    re_captcha.check('recovery_password',function(token){

                        var post='';

                        post+='&email='+uniEncode($v('auth_email_text').toLowerCase());
                        post+='&re_captcha_token='+uniEncode(token);

                        send({
                            'scriptPath':'/api/json/forgot_password',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                var  data
                                    ,dataTemp;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                $d('auth_forgot_submit_button').setAttribute('class','submit_half_button');
                                $d('auth_forgot_submit_button').onclick=page_object.action.auth.action.forgot.send;

                                $d('auth_forgot_cancel_button').setAttribute('class','submit_half_button');
                                $d('auth_forgot_cancel_button').onclick=page_object.action.auth.action.forgot.init;

                                if(isset(data['error']))
                                    $s('auth_email_text').border='1px solid #e14141';
                                else{

                                    page_object.action.auth.action.forgot.init();

                                    var inner='';

                                    inner+='<div class="dialog_row" style="text-align: center">'+stripSlashes(lang_obj['forgot_info'])+'</div>';

                                    var dialog_index=page_object.dialog.init({
                                        'title':lang_obj['forgot_title'],
                                        'inner':inner,
                                        'ok':function(){

                                            page_object.dialog.action.un_show.init(dialog_index,true);

                                        }
                                    });

                                }

                            }
                        });

                    });

                }

            }
        },
        'reset':{
            'is_reset':false,
            'init':function(){

                var lang_obj=page_object.action.auth.content[page_object.lang];

                if(page_object.action.auth.action.reset.is_reset){

                    page_object.action.auth.action.reset.is_reset   =false;
                    page_object.action.auth.action.reset.is_forgot  =false;

                    $d('auth_email_text').value                 ='';
                    $d('auth_login_text').value                 ='';
                    $d('auth_password_text').value              ='';
                    $d('auth_pas1_text').value                  ='';
                    $d('auth_pas2_text').value                  ='';

                    $s('auth_email_text').border                ='';
                    $s('auth_login_text').border                ='';
                    $s('auth_password_text').border             ='';
                    $s('auth_pas1_text').border                 ='';
                    $s('auth_pas2_text').border                 ='';

                    $s('auth_email_label').opacity              =1;
                    $s('auth_login_label').opacity              =1;
                    $s('auth_password_label').opacity           =1;
                    $s('auth_pas1_label').opacity               =1;
                    $s('auth_pas2_label').opacity               =1;

                    $d('auth_title').innerHTML                  =stripSlashes(lang_obj['title']);

                    $s('auth_login_block').height               ='';
                    $s('auth_login_block').opacity              ='';
                    $s('auth_login_block').margin               ='';

                    $s('auth_password_block').height            ='';
                    $s('auth_password_block').opacity           ='';
                    $s('auth_password_block').margin            ='';

                    $s('auth_keep_block').height                ='';
                    $s('auth_keep_block').opacity               ='';
                    $s('auth_keep_block').margin                ='';

                    $s('auth_submit_block').height              ='';
                    $s('auth_submit_block').opacity             ='';
                    $s('auth_submit_block').margin              ='';


                    $s('auth_email_block').height               =0;
                    $s('auth_email_block').opacity              =0;
                    $s('auth_email_block').margin               =0;

                    $s('auth_forgot_submit_block').height       =0;
                    $s('auth_forgot_submit_block').opacity      =0;
                    $s('auth_forgot_submit_block').margin       =0;


                    $s('auth_pas1_block').height                =0;
                    $s('auth_pas1_block').opacity               =0;
                    $s('auth_pas1_block').margin                =0;

                    $s('auth_pas2_block').height                =0;
                    $s('auth_pas2_block').opacity               =0;
                    $s('auth_pas2_block').margin                =0;

                    $s('auth_reset_submit_block').height        =0;
                    $s('auth_reset_submit_block').opacity       =0;
                    $s('auth_reset_submit_block').margin        =0;

                }
                else{

                    page_object.action.auth.action.reset.is_reset   =true;
                    page_object.action.auth.action.reset.is_forgot  =false;

                    $d('auth_email_text').value                 =0;
                    $d('auth_login_text').value                 =0;
                    $d('auth_password_text').value              =0;
                    $d('auth_pas1_text').value                  =0;
                    $d('auth_pas2_text').value                  =0;

                    $s('auth_email_text').border                =0;
                    $s('auth_login_text').border                =0;
                    $s('auth_password_text').border             =0;
                    $s('auth_pas1_text').border                 =0;
                    $s('auth_pas2_text').border                 =0;

                    $s('auth_email_label').opacity              =1;
                    $s('auth_login_label').opacity              =1;
                    $s('auth_password_label').opacity           =1;
                    $s('auth_pas1_label').opacity               =1;
                    $s('auth_pas2_label').opacity               =1;

                    $d('auth_title').innerHTML                  =stripSlashes(lang_obj['reset_title']);

                    $s('auth_login_block').height               =0;
                    $s('auth_login_block').opacity              =0;
                    $s('auth_login_block').margin               =0;

                    $s('auth_password_block').height            =0;
                    $s('auth_password_block').opacity           =0;
                    $s('auth_password_block').margin            =0;

                    $s('auth_keep_block').height                =0;
                    $s('auth_keep_block').opacity               =0;
                    $s('auth_keep_block').margin                =0;

                    $s('auth_submit_block').height              =0;
                    $s('auth_submit_block').opacity             =0;
                    $s('auth_submit_block').margin              =0;


                    $s('auth_email_block').height               ='';
                    $s('auth_email_block').opacity              ='';
                    $s('auth_email_block').margin               ='';

                    $s('auth_forgot_submit_block').height       ='';
                    $s('auth_forgot_submit_block').opacity      ='';
                    $s('auth_forgot_submit_block').margin       ='';


                    $s('auth_pas1_block').height                ='';
                    $s('auth_pas1_block').opacity               ='';
                    $s('auth_pas1_block').margin                ='';

                    $s('auth_pas2_block').height                ='';
                    $s('auth_pas2_block').opacity               ='';
                    $s('auth_pas2_block').margin                ='';

                    $s('auth_reset_submit_block').height        ='';
                    $s('auth_reset_submit_block').opacity       ='';
                    $s('auth_reset_submit_block').margin        ='';

                }

                page_object.action.auth.action.resize();

            },
            'send':function(){

                var  lang_obj       =page_object.action.auth.content[page_object.lang]
                    ,pas_1          =$v('auth_pas1_text')
                    ,pas_1_len      =pas_1.length
                    ,pas_2          =$v('auth_pas2_text')
                    ,pas_2_len      =pas_2.length
                    ,len_min        =6
                    ,len_max        =32
                    ,error          =false;

                if(pas_1_len<len_min||pas_1_len>len_max){

                    $s('auth_pas1_text').border='1px solid #e14141';

                    error=true;

                }

                if(pas_2_len<len_min||pas_2_len>len_max){

                    $s('auth_pas2_text').border='1px solid #e14141';

                    error=true;

                }

                if(pas_1!==pas_2){

                    $s('auth_pas1_text').border     ='1px solid #e14141';
                    $s('auth_pas2_text').border     ='1px solid #e14141';

                    error=true;

                }

                if(!error){

                    $d('auth_reset_submit_button').setAttribute('class','submit_half_button_enable');
                    $d('auth_reset_submit_button').onclick=function(){};

                    $d('auth_reset_cancel_button').setAttribute('class','submit_half_button_enable');
                    $d('auth_reset_cancel_button').onclick=function(){};

                    re_captcha.check('reset_password',function(token){

                        var post='';

                        post+='&hash='+uniEncode(page_object.link.link_list[1]);
                        post+='&pas='+uniEncode(pas_1);
                        post+='&re_captcha_token='+uniEncode(token);

                        send({
                            'scriptPath':'/api/json/reset_password',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                var  data
                                    ,dataTemp;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                $d('auth_reset_submit_button').setAttribute('class','submit_half_button');
                                $d('auth_reset_submit_button').onclick=page_object.action.auth.action.reset.send;

                                $d('auth_reset_cancel_button').setAttribute('class','submit_half_button');
                                $d('auth_reset_cancel_button').onclick=page_object.action.auth.action.reset.init;

                                if(isset(data['error'])){

                                    $s('auth_pas1_text').border     ='1px solid #e14141';
                                    $s('auth_pas2_text').border     ='1px solid #e14141';

                                }
                                else{

                                    page_object.link.link_list      =['auth'];
                                    page_object.link.link_name      ='auth';

                                    var lang_obj_2=page_object.content[page_object.lang];

                                    setUrl(lang_obj_2['title']+' | '+lang_obj_2['loading'],'/auth');

                                    page_object.action.auth.action.reset.init();

                                    $d('auth_login_text').value         =stripSlashes(data['login']);
                                    $s('auth_login_label').opacity      =0;

                                    var inner='';

                                    inner+='<div class="dialog_row" style="text-align: center">'+stripSlashes(lang_obj['reset_info'])+'</div>';

                                    var dialog_index=page_object.dialog.init({
                                        'title':lang_obj['reset_title'],
                                        'inner':inner,
                                        'ok':function(){

                                            page_object.dialog.action.un_show.init(dialog_index,true);

                                        }
                                    });

                                }

                            }
                        });

                    });

                }

            }
        }
    }
};