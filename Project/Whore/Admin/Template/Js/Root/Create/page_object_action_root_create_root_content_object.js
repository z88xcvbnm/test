page_object.action.root.create.root_content={
    'init':function(){

        page_object.action.root.create.root_content.position.init();
        page_object.action.root.create.root_content.create.init();
        page_object.action.root.create.root_content.set_action();
        page_object.action.root.create.root_content.show();

    },
    'position':{
        'init':function(){}
    },
    'create':{
        'init':function(){

            page_object.action.root.create.root_content.create.root_content.init();
            page_object.action.root.create.root_content.create.root_face_preview_list();

        },
        'root_face_preview_list':function(){

            var  el
                ,img_el
                ,data           =page_object.action.root.data['data']
                ,face_list      =data['face_list']
                ,list           =data['face_image_random_list']
                ,random_len     =5
                ,index;

            if(!isset($d('root_face_preview_list'))){

                el=addElement({
                    'tag':'div',
                    'id':'root_face_preview_list'
                });

                $d('root_content').appendChild(el);

            }

            var image_len=$d('root_face_preview_list').getElementsByClassName('root_face_image_random').length;

            if(image_len===0){

                let  image_list             =[]
                    ,temp_image_list        ={}
                    ,image_random_index
                    ,image_index            =0
                    ,ration                 =window.devicePixelRatio
                    ,win_width              =window.screen.width*ration
                    ,win_height             =window.screen.height*ration
                    ,image_w                =50
                    ,image_h                =55
                    ,image_x_len            =Math.ceil(win_width/image_w)+1
                    ,image_y_len            =Math.ceil(win_height/image_h)+2
                    ,image_len              =list.length
                    ,image_max_len          =image_x_len*image_y_len
                    // ,image_max_len          =OS.isMobile?(image_x_len*image_y_len):900
                    // ,image_max_len          =800
                    ,need_add               =true
                    ,index;

                trace('image len: '+image_max_len);

                do{

                    do{

                        image_random_index  =Math.floor(Math.random()*image_len);
                        need_add            =true;

                        // trace(image_random_index);

                        for(index=image_list.length-1;index>image_list.length-random_len;index--)
                            if(isset(image_list[index]))
                                if(isset(image_list[index]['link'])){

                                    // trace(image_list[index]['link']+' === '+list[image_random_index]['link']);

                                    if(image_list[index]['link']===list[image_random_index]['link']){

                                        need_add=false;

                                        break;

                                    }

                                }

                    }while(!need_add);

                    image_list.push(list[image_random_index]);

                    if(!isset(temp_image_list[list[image_random_index]['link']]))
                        temp_image_list[list[image_random_index]['link']]=0;

                        temp_image_list[list[image_random_index]['link']]++;

                    image_index++;

                }while(image_index<=image_max_len);

                trace('Temp image list: ');
                trace(temp_image_list);
                trace(getObjectLength(temp_image_list));

                // trace('TEST: '+image_list.length);

                if(isset($d('root_face_preview_list')))
                    for(index in image_list){

                        img_el=addElement({
                            'tag':'div',
                            'id':'root_face_image_random_item_'+index,
                            'class':'root_face_image_random',
                            'title':face_list[image_list[index]['face_ID']],
                            'style':'background: url('+'/'+image_list[index]['link']+') 50% 50% no-repeat; background-size: cover;'
                        });

                        if(isset($d('root_face_preview_list')))
                            $d('root_face_preview_list').appendChild(img_el);

                        img_el.style.opacity=1;

                    }

            }

        },
        'root_content':{
            'init':function(){

                let  action=page_object.action.root.data['action'];

                switch(action){

                    case'auth':
                    case'reg':
                    case'confirm_email':
                    case'root':{

                        page_object.action.root.create.root_content.create.root_content.face();

                        break;

                    }

                    case'root_about':{

                        page_object.action.root.create.root_content.create.root_content.about();

                        break;

                    }

                    case'root_source':{

                        page_object.action.root.create.root_content.create.root_content.source();

                        break;

                    }

                    case'root_term':{

                        page_object.action.root.create.root_content.create.root_content.term();

                        break;

                    }

                    case'root_city':{

                        page_object.action.root.create.root_content.create.root_content.city();

                        break;

                    }

                }

            },
            'reset_root_face_block':function(){

                if(OS.isMobile)
                    if(page_object.is_first_start){

                        page_object.is_first_start=false;

                        if(isset($d('root_face_block')))
                            removeElement($d('root_face_block'));

                    }


            },
            'face':function(){

                page_object.action.root.create.root_content.create.root_content.reset_root_face_block();

                if(isset($d('root_face_content'))){

                    let data=page_object.action.root.data;

                    if(isset($d('face_len')))
                        $d('face_len').innerHTML=get_number_with_space(data['data']['face_len']);

                    if(isset($d('face_image_len')))
                        $d('face_image_len').innerHTML=get_number_with_space(data['data']['face_image_indexed_len']);

                    return false;

                }

                let  inner      =''
                    ,i
                    ,el
                    ,data       =page_object.action.root.data
                    ,index;

                // inner+='<div id="root_face_block_bg"></div>';

                inner+='<div id="root_left_content">';

                inner+='<h1>Проверь свою избранницу по самой полной базе данных <strong>эскортниц</strong>,</h1><p style="display: inline;"> <strong>содержанок</strong> и <strong>шлюх</strong>, используя фотографию ее лица в фас.</p>';
                // inner+='<p>Проверь свою избранницу по самой полной базе данных <strong>эскортниц</strong>, содержанок и шлюх, используя фотографию ее лица в фас.</p>';
                inner+='<p style="margin: 10px 0 0 0;">Применяемый биометрический алгоритм распознавания лиц имеет вероятность ложноположительного срабатывания (FAR) 10<sup>-7</sup>. </p><p style="margin-top: 10px;">Чем лучше качество загружаемого фото, тем точнее будет результат.</p>';

                inner+='</div>';

                inner+='<div id="root_face_content">';

                    inner+='<div id="root_search_face_content_loading"></div>';

                    for(i=0;i<3;i++){

                        inner+='<div id="root_search_face_image_row_container_'+i+'" class="root_search_face_image_row_container">';

                            inner+='<div id="root_search_face_image_block_'+i+'" class="root_search_face_image_block">';

                                inner+='<div id="root_search_face_button_choose_'+i+'" class="root_search_face_button_choose"><span>Выбрать фотографию</span></div>';

                                inner+='<div id="root_search_face_image_info_'+i+'" class="root_search_face_image_info">или перетащите его сюда</div>';
                                inner+='<div id="root_search_face_image_loading_'+i+'" class="root_search_face_image_loading"><span>Загрузка...</span></div>';
                                inner+='<div id="root_search_face_image_error_'+i+'" class="root_search_face_image_error">Невереный формат файла</div>';

                                inner+='<div id="root_search_face_image_block_container_'+i+'" class="root_search_face_image_block_container">';

                                    inner+='<div id="root_search_face_image_src_'+i+'" class="root_search_face_image_src"></div>';

                                inner+='</div>';

                                inner+='<input type="file" multiple="multiple" id="root_search_face_image_input_'+i+'" class="root_search_face_image_input" accept="image/png, image/jpeg" value="" title="" />';

                            inner+='</div>';

                        inner+='</div>';

                    }

                    inner+='<div id="root_search_info_block" class="root_search_info_block"></div>';

                    inner+='<div id="root_search_face_price" class="root_search_face_price">Стоимость запроса <span id="root_search_face_cost">0</span> ₽</div>';

                    inner+='<div id="root_search_face_button" class="root_search_face_button_disable">Проверить фото</div>';

                    if(OS.isMobile)
                        inner+='<div id="root_search_face_button_cancel" class="root_search_face_button">Отмена</div>';

                inner+='</div>';

                inner+='<div id="root_right_content">';

                    inner+='<div id="root_right_content_container">';

                        inner+='<h2>На текущий момент в базе содержится:</h2>';
                        inner+='<p></p>';
                        // inner+='<p>На текущий момент в базе содержится:</p>';
                        inner+='<p><b id="face_len">'+get_number_with_space(data['data']['face_len'])+'</b> анкет девушек занимающихся или занимавшихся продажей интимных услуг в России, Украине, Беларуси, Казахстане и других странах.</p>';

                        inner+='<p style="margin: 0 0 0 0;">Топ городов по количеству шкур:</p>';

                if(data['data']['city_list'].length>0){

                    inner+='<p>';

                    for(index in data['data']['city_list'])
                        inner+=stripSlashes(data['data']['city_list'][index]['name'])+' - <b>'+get_number_with_space(data['data']['city_list'][index]['len'])+'</b><br />';

                    inner+='</p>';

                }

                        inner+='<p>Проиндексировано <b id="face_image_len">'+get_number_with_space(data['data']['face_image_indexed_len'])+'</b> лиц.</p>';

                    inner+='</div>';
                inner+='</div>';

                if(!isset($d('root_content'))){

                    el=addElement({
                        'tag':'div',
                        'id':'root_content',
                        'style':'opacity: 0'
                    });

                    $d('all').appendChild(el);

                }

                if(!isset($d('root_face_block'))){

                    el=addElement({
                        'tag':'div',
                        'id':'root_face_block',
                        'inner':inner,
                        'style':'opacity: 0'
                    });

                    $d('root_content').appendChild(el);

                }
                else if(isset($d('root_face_block')))
                    $d('root_face_block').innerHTML=inner;

            },
            'about':function(){

                page_object.action.root.create.root_content.create.root_content.reset_root_face_block();

                let  data   =page_object.action.root.data['data']
                    ,inner  =''
                    ,el;

                // if(OS.isMobile)
                    inner+='<div id="settings_close_button" class="dialog_close_button"'+(OS.isMobile?' style="margin: -11px 0 0 11px; padding: 0;"':' style="margin: -10px 0 0 10px; padding: 0;"')+'></div>';

                inner+='<div id="root_default_content">';

                // if(!OS.isMobile)
                //     inner+='<div id="settings_close_button" class="dialog_close_button"></div>';

                    inner+=data['about'];
                inner+='</div>';

                if(!isset($d('root_face_block'))){

                    el=addElement({
                        'tag':'div',
                        'id':'root_face_block',
                        'inner':inner,
                        'style':'opacity: 0'
                    });

                    $d('root_content').appendChild(el);

                }
                else if(isset($d('root_face_block')))
                    $d('root_face_block').innerHTML=inner;

            },
            'source':function(){

                page_object.action.root.create.root_content.create.root_content.reset_root_face_block();

                let  data   =page_object.action.root.data['data']
                    ,inner  =''
                    ,el;

                // if(OS.isMobile)
                    inner+='<div id="settings_close_button" class="dialog_close_button"'+(OS.isMobile?' style="margin: -11px 0 0 11px; padding: 0;"':' style="margin: -10px 0 0 10px; padding: 0;"')+'></div>';

                inner+='<div id="root_default_content">';

                // if(!OS.isMobile)
                //     inner+='<div id="settings_close_button" class="dialog_close_button"></div>';

                    inner+=data['source'];
                inner+='</div>';

                if(!isset($d('root_face_block'))){

                    el=addElement({
                        'tag':'div',
                        'id':'root_face_block',
                        'inner':inner,
                        'style':'opacity: 0'
                    });

                    $d('root_content').appendChild(el);

                }
                else if(isset($d('root_face_block')))
                    $d('root_face_block').innerHTML=inner;

            },
            'term':function(){

                page_object.action.root.create.root_content.create.root_content.reset_root_face_block();

                let  data   =page_object.action.root.data['data']
                    ,inner  =''
                    ,el;

                // if(OS.isMobile)
                    inner+='<div id="settings_close_button" class="dialog_close_button"'+(OS.isMobile?' style="margin: -11px 0 0 11px; padding: 0;"':' style="margin: -10px 0 0 10px; padding: 0;"')+'></div>';

                inner+='<div id="root_default_content">';

                // if(!OS.isMobile)
                //     inner+='<div id="settings_close_button" class="dialog_close_button"></div>';

                    inner+=data['term'];
                inner+='</div>';

                if(!isset($d('root_face_block'))){

                    el=addElement({
                        'tag':'div',
                        'id':'root_face_block',
                        'inner':inner,
                        'style':'opacity: 0'
                    });

                    $d('root_content').appendChild(el);

                }
                else if(isset($d('root_face_block')))
                    $d('root_face_block').innerHTML=inner;

            },
            'city':function(){

                page_object.action.root.create.root_content.create.root_content.reset_root_face_block();

                let  data       =page_object.action.root.data['data']
                    ,inner      =''
                    ,el
                    ,list       =data['city_list']
                    ,index;

                // if(OS.isMobile)
                inner+='<div id="settings_close_button" class="dialog_close_button"'+(OS.isMobile?' style="margin: -11px 0 0 11px; padding: 0;"':' style="margin: -10px 0 0 10px; padding: 0;"')+'></div>';

                inner+='<div id="root_default_content">';

                    inner+='<h1 style="text-align: center">Статистика городов по количеству шлюх</h1>';

                    inner+='<div id="root_city_stats_block">';

                        inner+='<div id="root_city_stats_header" style="width: 100%">';

                            inner+='<div class="root_city_stats_header_col" style="width: 10%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">#</td></tr></table></div>';
                            inner+='<div class="root_city_stats_header_col" style="width: 50%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">Город</td></tr></table></div>';
                            inner+='<div class="root_city_stats_header_col" style="width: 40%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">Анкет, шт</td></tr></table></div>';
                            // inner+='<div class="root_city_stats_header_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">Фотографий, шт</td></tr></table></div>';

                        inner+='</div>';

                for(index in list){

                    inner+='<div class="root_city_stats_row_item" style="width: 100%">';

                        inner+='<div class="root_city_stats_row_item_col" style="width: 10%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(parseInt(index)+1)+'</td></tr></table></div>';
                        inner+='<div class="root_city_stats_row_item_col" style="width: 50%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['name'])+'</td></tr></table></div>';
                        inner+='<div class="root_city_stats_row_item_col" style="width: 40%;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['len'])+'</td></tr></table></div>';
                        // inner+='<div class="root_city_stats_row_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['image_len'])+'</td></tr></table></div>';

                    inner+='</div>';

                }

                            inner+='</div>';
                    inner+='</div>';

                if(!isset($d('root_face_block'))){

                    el=addElement({
                        'tag':'div',
                        'id':'root_face_block',
                        'inner':inner,
                        'style':'opacity: 0'
                    });

                    $d('root_content').appendChild(el);

                }
                else if(isset($d('root_face_block')))
                    $d('root_face_block').innerHTML=inner;

            }
        },
    },
    'set_action':function(){

        if(isset($d('settings_close_button')))
            $d('settings_close_button').onclick=page_object.action.root.action.header.logo.click;

        page_object.action.root.action.root_face_block.action.disclaimer.init();
        page_object.action.root.action.root_content.action.init();
        page_object.action.root.action.root_face_block.action.init();

    },
    'show':function(){

        setTimeout(page_object.action.root.action.root_content.show,40);
        setTimeout(page_object.action.root.action.root_face_block.show,400);

    }
};
