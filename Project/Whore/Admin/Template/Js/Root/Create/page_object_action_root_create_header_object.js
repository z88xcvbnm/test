page_object.action.root.create.header={
    'init':function(){

        if(
            (
                  isset($d('header'))
                &&!isset($d('header_profile'))
                &&!isset($d('header_login'))
                &&!OS.isMobile
            )
            ||(
                  isset($d('header'))
                &&!isset($d('header_profile'))
                &&!isset($d('header_login'))
                &&!isset($d('menu_close'))
                &&OS.isMobile
            )
        ){

            page_object.action.root.action.header.un_show(true);

            // setTimeout(page_object.action.root.create.header.create.init,600);
            setTimeout(page_object.action.root.create.header.create.init,300);

        }
        else{

            setTimeout(page_object.action.root.create.header.create.init,40);
            // setTimeout(page_object.action.root.create.header.create.init,300);

        }

    },
    'position':{
        'header_menu':function(){

            page_object.action.root.position.header_menu.w=Math.ceil(winSize.winWidth*.63);

        },
        'header':function(){

            page_object.action.root.position.header.w      =winSize.winWidth;
            page_object.action.root.position.header.x      =0;
            page_object.action.root.position.header.y      =-page_object.action.root.position.header.h;

        },
        'header_profile':function(){

            if(isset($d('header_profile'))){

                page_object.action.root.position.header_profile.o      =0;
                page_object.action.root.position.header_profile.w      =elementSize.width($d('header_profile'));
                page_object.action.root.position.header_profile.x      =winSize.winWidth-page_object.action.root.position.header_profile.m.r-page_object.action.root.position.header_profile.w;

                $s('header_profile').opacity            =page_object.action.root.position.header_profile.o;
                // $s('header_profile').transform          ='translate('+page_object.action.root.position.header_profile.x+'px,'+page_object.action.root.position.header_profile.y+'px)';
                $s('header_profile').height             =page_object.action.root.position.header_profile.h+'px';

            }

        }
    },
    'create':{
        'init':function(){

            if(isset($d('header'))){

                page_object.action.root.create.header.show();
                page_object.action.root.create.header.set_action();

                return true;

            }

            page_object.action.root.create.header.position.header();
            page_object.action.root.create.header.position.header_menu();
            page_object.action.root.create.header.create.header();
            page_object.action.root.create.header.create.footer();
            page_object.action.root.create.header.set_action();
            page_object.action.root.create.header.show();

            return true;

        },
        'header':function(){

            if(isset($d('header')))
                return false;

            let  el
                ,inner                      =''
                ,header_style               =''
                ,header_menu_style          ='';

            inner+='<div id="logo"></div>';

            if(OS.isMobile)
                inner+='<div id="menu_button"></div>';

            if(!OS.isMobile)
                header_menu_style+='transform: translate('+page_object.action.root.position.header_menu.x+'px,'+page_object.action.root.position.header_menu.y+'px); ';

            header_menu_style+='opacity: '+page_object.action.root.position.header_menu.o+'; ';

            if(OS.isMobile){

                if(!isset($d('header_menu'))){

                    let header_menu_inner='';

                    header_menu_inner+='<div id="menu_close"></div>';

                    header_menu_inner+='<div id="header_menu_list">';

                        header_menu_inner+='<div id="header_menu_" class="header_menu_item">Главная</div>';
                        header_menu_inner+='<div id="header_menu_login" class="header_menu_item">Войти</div>';
                        header_menu_inner+='<div id="header_menu_reg" class="header_menu_item">Зарегистрироваться</div>';

                        header_menu_inner+='<div id="header_menu_about" class="header_menu_item" style="margin-left: 0;">О проекте</div>';
                        header_menu_inner+='<div id="header_menu_term" class="header_menu_item">Термины и определения</div>';
                        header_menu_inner+='<div id="header_menu_source" class="header_menu_item">Источники</div>';
                        header_menu_inner+='<div id="header_menu_city" class="header_menu_item">Статистика</div>';

                    header_menu_inner+='</div>';

                    el=addElement({
                        'tag':'div',
                        'id':'header_menu',
                        'inner':header_menu_inner
                    });

                    $d('all').appendChild(el);

                }

            }
            else{

                inner+='<div id="header_menu" style="'+header_menu_style+'">';

                    inner+='<div id="header_menu_about" class="header_menu_item" style="margin-left: 0;">О проекте</div>';
                    inner+='<div id="header_menu_term" class="header_menu_item">Термины и определения</div>';
                    inner+='<div id="header_menu_source" class="header_menu_item">Источники</div>';
                    inner+='<div id="header_menu_city" class="header_menu_item">Статистика</div>';

                inner+='</div>';

                inner+='<div id="header_menu_line_active" class="header_menu_line_active"></div>';

                inner+='<div id="header_reg" class="header_login">Зарегистрироваться</div>';
                inner+='<div id="header_login" class="header_login">Войти</div>';

            }

            header_style+='transform: translate('+page_object.action.root.position.header.x+'px,'+page_object.action.root.position.header.y+'px); ';
            // header_style+='width: '+page_object.action.root.position.header.w+'px; ';
            // header_style+='height: '+page_object.action.root.position.header.h+'px; ';

            el=addElement({
                'tag':'div',
                'id':'header',
                'inner':inner,
                'style':header_style
            });

            $d('all').appendChild(el);

        },
        'footer':function(){

            let  el;

            el=addElement({
                'tag':'div',
                'id':'footer',
                'inner':'© 2019 Shluham.net',
                'style':'opacity: 0'
            });

            $d('all').appendChild(el);

        }
    },
    'set_action':function(){

        if(isset($d('header_menu'))){

            let  id_el              =OS.isMobile?'header_menu_list':'header_menu'
                ,list               =$d(id_el).getElementsByTagName('div')
                ,i;

            for(i=0;i<list.length;i++)
                if(isObject(list[i]))
                    list[i].onclick=page_object.action.root.action.header_menu.click;

            if(isset(page_object.link.link_list[0]))
                page_object.action.root.action.header_menu.set_active(page_object.link.link_list[0]);

        }

        if(isset($d('logo')))
            $d('logo').onclick=page_object.action.root.action.header.logo.click;

        if(isset($d('header_login')))
            $d('header_login').onclick=page_object.action.root.action.header.auth.click;

        if(isset($d('header_reg')))
            $d('header_reg').onclick=page_object.action.root.action.header.reg.click;

        if(isset($d('menu_button')))
            $d('menu_button').onclick=page_object.action.root.action.header.menu.show;

        if(isset($d('menu_close')))
            $d('menu_close').onclick=page_object.action.root.action.header.menu.un_show;

    },
    'show':function(){

        setTimeout(page_object.action.root.action.header.show,40);
        setTimeout(page_object.action.root.action.header_menu.show,40);

    }
};
