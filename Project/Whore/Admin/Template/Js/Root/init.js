page_object.action.root={
    'data':{},
    'script_list':{
        'core':{
            'localization':'/show/js/root/localization.js',
            'elements_positions':'/show/js/root/elements_positions.js',
            'section_change_workflow':'/show/js/root/section_change_workflow.js'
        },
        'ui_elements_creation':{
            'header':'/show/js/root/create/page_object_action_root_create_header_object.js',
            'root_content':'/show/js/root/create/page_object_action_root_create_root_content_object.js',
            'reg':'/show/js/reg/init.js',
            'auth':'/show/js/auth/init.js',
        },
        'ui_elements_manipulation':{
            'header':'/show/js/root/action/page_object_action_root_action_header_object.js',
            'header_menu':'/show/js/root/action/page_object_action_root_action_header_menu_object.js',
            'root_content':'/show/js/root/action/page_object_action_root_action_root_content_object.js',
            'root_face_block':'/show/js/root/action/page_object_action_root_action_root_face_block_object.js',
        },
        'system':{
            'exif':'/show/js/system/exif.js'
        },
        'recaptcha':{
            'script':'https://www.google.com/recaptcha/api.js?render=6Lez3J4UAAAAAKzX2vcL3Gd_inzV55NQdtaT9d29'
        }
    },
    'style_list':[
        '/show/css/system/root.css',
        '/show/css/system/dialog_block.css'
    ],
    'section_change_workflow':{},
    'page_action':null,
    'back_page_action':null,
    'content':{},
    'position':{},
    'init':function(data){

        if(OS.isMobile)
            page_object.action.root.style_list.push('/show/css/system/root_mobile.css');

        var temp={
            'script_list':config.updateScriptsList(page_object.action.root.script_list,'Root'),
            'style_list':config.updateScriptsList(page_object.action.root.style_list,'Root'),
            'init_list':[
                function(){

                    if (typeof config !== 'undefined' && $.isFunction(config.updateSectionChangeWorkflow)) {
                        page_object.action.root.section_change_workflow = config.updateSectionChangeWorkflow(page_object.action.root.section_change_workflow);
                    }

                    page_object.action.root.run(data);
                }
            ]
        };

        if(isset(data['data']['face_image_random_list'])){

            let  el_len=isset($d('root_face_preview_list'))?$d('root_face_preview_list').getElementsByTagName('div').length:0;

            if(el_len===0){

                let  image_list     =data['data']['face_image_random_list']
                    ,list           =[]
                    ,index;

                for(index in image_list)
                    list.push(image_list[index]['link']);

                temp['image_list']=list;

            }

        }

        trace(temp);

        page_object.require.init(temp);

    },
    'run':function(data){

        if(
              page_object.link.link_list[(page_object.link.link_list.length-1)]!==page_object.link.back_link_list[(page_object.link.back_link_list.length-1)]
            ||page_object.link.link_list[(page_object.link.link_list.length-2)]!==page_object.link.back_link_list[(page_object.link.back_link_list.length-2)]
            ||page_object.link.link_list[(page_object.link.link_list.length-3)]!==page_object.link.back_link_list[(page_object.link.back_link_list.length-3)]
        ){

            page_object.action.root.data                   =data;
            page_object.action.root.back_page_action       =page_object.action.root.page_action;
            page_object.action.root.page_action            =data['action'];

            //if(isset($d('dialog_block')))
            //    page_object.dialog.action.un_show.init(dialog_index,true);

            if(isset(page_object.action.root.section_change_workflow[data['action']])){

                page_object.action.root.section_change_workflow[data['action']].init();

                setTimeout(page_object.action.root.action.all.resize,1200);

            }
            else
                trace('==> TRIG ACTION IS NOT EXISTS');

        }
        else
            trace('==> ACTION ALREADY ACTIVE');

    },
    'prepend_localization_content':function (data) {
        page_object.action.root.content = $.extendext(true, 'concat', data, page_object.action.root.content);
    },
    'append_localization_content':function (data) {
        page_object.action.root.content = $.extendext(true, 'concat', page_object.action.root.content, data);
    },
    'prepend_element_positions_settings':function (data) {
        page_object.action.root.position = $.extendext(true, 'concat', data, page_object.action.root.position);
    },
    'append_element_positions_settings':function (data) {
        page_object.action.root.position = $.extendext(true, 'concat', page_object.action.root.position, data);
    },
    'create':{},
    'action':{
        'all':{
            'resize':function(){}
        }
    }
};