page_object.action.root.action.root_content={
    'show':function(){

        if(isset($d('root_content'))){

            page_object.action.root.position.root_content.o=1;
            page_object.action.root.position.root_content.y=0;

            $s('root_content').opacity        =page_object.action.root.position.root_content.o;
            $s('root_content').transform      ='translate('+page_object.action.root.position.root_content.x+'px,'+page_object.action.root.position.root_content.y+'px)'

        }

        setTimeout(page_object.link.preload.un_show,300);

    },
    'un_show':function(remove){

        if(isset($d('root_content'))){

            page_object.action.root.position.root_content.o=0;
            page_object.action.root.position.root_content.y=-page_object.action.root.position.root_content.h;

            $s('root_content').opacity        =page_object.action.root.position.root_content.o;
            $s('root_content').transform      ='translate('+page_object.action.root.position.root_content.x+'px,'+page_object.action.root.position.root_content.y+'px)'

        }

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.root.action.root_content.remove,300);

    },
    'un_show_face_block':function(remove){

        if(isset($d('root_content'))){

            page_object.action.root.position.root_content.o=0;
            page_object.action.root.position.root_content.y=-page_object.action.root.position.root_content.h;

            $s('root_content').opacity        =page_object.action.root.position.root_content.o;
            $s('root_content').transform      ='translate('+page_object.action.root.position.root_content.x+'px,'+page_object.action.root.position.root_content.y+'px)'

        }

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.root.action.root_content.remove,300);

    },
    'action':{
        'init':function(){

        }
    },
    'remove':function(){

        if(isset($d('root_content')))
            removeElement($d('root_content'));

    },
    'resize':function(){}
};