page_object.action.root.action.root_face_block={
    'show':function(){

        if(isset($d('root_face_block'))){

            let  h=elementSize.height($d('root_face_block'));

            page_object.action.root.position.root_face_block.o=1;

            // $s('root_face_block').transform     ='translate(-50%,-50%)';
            $s('root_face_block').opacity       =1;

            // if(isset($d('root_right_content'))&&!OS.isMobile)
            //     $s('root_right_content').height=elementSize.height($d('root_left_content'))+'px';

            if(isset($d('root_right_content'))&&!OS.isMobile)
                $s('root_right_content').height='300px';

        }

    },
    'un_show':function(remove){

        if(isset($d('root_face_block'))){

            page_object.action.root.position.root_face_block.o  =0;
            page_object.action.root.position.root_face_block.y  =-200;

            $s('root_face_block').opacity       =page_object.action.root.position.root_face_block.o;
            // $s('root_face_block').transform     ='translate(-50%,-100%)';

        }

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.root.action.root_face_block.remove,300);

    },
    'action':{
        'init':function(){

            page_object.action.root.action.root_face_block.action.image.init();

        },
        'image':{
            'list':[
                {
                    'image_ID':null,
                    'file_ID':null,
                    'link':null,
                    'file':null
                },
                {
                    'image_ID':null,
                    'file_ID':null,
                    'link':null,
                    'file':null
                },
                {
                    'image_ID':null,
                    'file_ID':null,
                    'link':null,
                    'file':null
                }
            ],
            'image_len':0,
            'init':function(){

                let  list   =page_object.action.root.action.root_face_block.action.image.list
                    ,index;

                if(isset(page_object.action.profile)){

                    if(isset(page_object.action.profile.data['user'])){

                        page_object.action.root.action.root_face_block.action.image.face.face_change_len=0;
                        page_object.action.root.action.root_face_block.action.image.face.list=[
                            {
                                'face_index':null,
                                'image_ID':null,
                                'image_data':null,
                                'file_ID':null,
                                'face_len':0,
                                'face_list':null
                            },
                            {
                                'face_index':null,
                                'image_ID':null,
                                'image_data':null,
                                'file_ID':null,
                                'face_len':0,
                                'face_list':null
                            },
                            {
                                'face_index':null,
                                'image_ID':null,
                                'image_data':null,
                                'file_ID':null,
                                'face_len':0,
                                'face_list':null
                            }
                        ];

                    }

                }

                page_object.upload.reset();
                page_object.action.root.action.root_face_block.action.image.image_len=0;

                page_object.action.root.action.root_face_block.action.image.drag.init();

                if(isset($d('root_search_face_button')))
                    $d('root_search_face_button').onclick=page_object.action.root.action.root_face_block.action.image.upload.init;

                for(index in list)
                    page_object.action.root.action.root_face_block.action.image.init_index(index);

            },
            'init_index':function(index){

                if(OS.isMobile){

                    if(isset($d('root_search_face_image_info_'+index)))
                        $s('root_search_face_image_info_'+index).display='none';

                }

                OS.getOS();

                if(isset($d('root_search_face_reset_'+index)))
                    $d('root_search_face_reset_'+index).onclick=page_object.action.root.action.root_face_block.action.image.reset_item;

                if(isset($d('root_search_face_image_input_'+index))){

                    if(isset($d('root_search_face_button_choose_'+index)))
                        $d('root_search_face_button_choose_'+index).onclick=function(){

                            let index=this.id.split('_')[5];

                            if(!page_object.action.root.action.root_face_block.action.disclaimer.init())
                                return false;

                            if(OS.os==='Android')
                                $d('root_search_face_image_input_'+index).setAttribute('accept','image/png, image/jpeg');
                            else
                                $d('root_search_face_image_input_'+index).setAttribute('accept','image/png, image/jpeg');

                            if(isset($d('root_search_face_image_input_'+index)))
                                $d('root_search_face_image_input_'+index).click();

                        };

                    $d('root_search_face_image_input_'+index).onchange=page_object.action.root.action.root_face_block.action.image.change;

                }

            },
            'drag':{
                'is_work':false,
                'event_list':[],
                'remove_event_list':function(){

                    if(page_object.action.root.action.root_face_block.action.image.drag.event_list.length>0)
                        listener.remove(page_object.action.root.action.root_face_block.action.image.drag.event_list);

                    page_object.action.root.action.root_face_block.action.image.drag.event_list=[];

                },
                'init':function(){

                    let  index
                        ,event_list=[];

                    page_object.action.root.action.root_face_block.action.image.drag.remove_event_list();

                    event_list.push(listener.set(document.body,'dragenter',page_object.action.root.action.root_face_block.action.image.drag.enter));
                    event_list.push(listener.set(document.body,'dragover',page_object.action.root.action.root_face_block.action.image.drag.leave));
                    event_list.push(listener.set(document.body,'dragleave',page_object.action.root.action.root_face_block.action.image.drag.over));
                    event_list.push(listener.set(document.body,'dragenter',page_object.action.root.action.root_face_block.action.image.drag.prevent_defaults));
                    event_list.push(listener.set(document.body,'dragover',page_object.action.root.action.root_face_block.action.image.drag.prevent_defaults));
                    event_list.push(listener.set(document.body,'dragleave',page_object.action.root.action.root_face_block.action.image.drag.prevent_defaults));
                    event_list.push(listener.set(document.body,'drop',page_object.action.root.action.root_face_block.action.image.drag.prevent_defaults));

                    page_object.action.root.action.root_face_block.action.image.drag.event_list=page_object.action.root.action.root_face_block.action.image.drag.event_list.concat(event_list);

                    for(index=0;index<3;index++)
                        page_object.action.root.action.root_face_block.action.image.drag.init_index(index);

                },
                'init_index':function(index){

                    if(isset($d('root_search_face_image_block_'+index))){

                        let event_list=[];

                        event_list.push(listener.set($d('root_search_face_image_block_'+index),'drop',page_object.action.root.action.root_face_block.action.image.drag.drop));

                        page_object.action.root.action.root_face_block.action.image.drag.event_list=page_object.action.root.action.root_face_block.action.image.drag.event_list.concat(event_list);

                        trace('after count event: '+page_object.action.root.action.root_face_block.action.image.drag.event_list.length);

                    }

                },
                'enter':function(e){

                    let index=this.id.split('_')[5];

                    if(isset($d('root_search_face_image_block_'+index)))
                        $s('root_search_face_image_block_'+index).borderColor='#ff0000';

                },
                'leave':function(e){

                    let index=this.id.split('_')[5];

                    if(isset($d('root_search_face_image_block_'+index)))
                        $s('root_search_face_image_block_'+index).borderColor='#ff0000';

                },
                'over':function(e){

                    let index=this.id.split('_')[5];

                    if(isset($d('root_search_face_image_block_'+index)))
                        $s('root_search_face_image_block_'+index).borderColor='';

                },
                'drop':function(e){

                    trace('ID: '+this.id);

                    e=e||window.event;

                    let  dt             =e.dataTransfer
                        ,image_link     =dt.getData('text')
                        ,files          =dt.files
                        ,index          =this.id.split('_')[5];

                    if(files.length===0){

                        if(isset($d('root_search_face_image_input_'+index)))
                            $d('root_search_face_image_input_'+index).value='';

                        page_object.action.root.action.root_face_block.action.image.list[index]['file_ID']       =null;
                        page_object.action.root.action.root_face_block.action.image.list[index]['image_ID']      =null;
                        page_object.action.root.action.root_face_block.action.image.list[index]['file']          =null;
                        page_object.action.root.action.root_face_block.action.image.list[index]['link']          =image_link;

                        trace('image_link: '+image_link);

                        page_object.action.root.action.root_face_block.action.image.send_link(image_link,index);

                    }
                    else
                        page_object.action.root.action.root_face_block.action.image.drag.files.init(files,index);

                },
                'prevent_defaults':function(e){

                    e=e||window.event;

                    e.preventDefault();
                    e.stopPropagation();

                },
                'files':{
                    'init':function(files,index){

                        let file_index;

                        if(isset($d('root_search_face_button'))){

                            $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                            $d('root_search_face_button').innerHTML     ='Загрузка...';
                            $d('root_search_face_button').onclick       =function(){};

                        }
                        else
                            return false;

                        if(isset($d('root_search_face_image_block_'+index)))
                            $s('root_search_face_image_block_'+index).borderColor='';

                        if(isset($d('root_search_face_button_choose')))
                            $s('root_search_face_button_choose').opacity=0;

                        if(isset($d('root_search_face_image_info_'+index)))
                            $s('root_search_face_image_info_'+index).opacity=0;

                        if(isset($d('root_search_face_image_src_object_'+index)))
                            $s('root_search_face_image_src_object_'+index).opacity=0;

                        setTimeout(function(){

                            if(isset($d('root_search_face_button_choose_'+index)))
                                $s('root_search_face_button_choose_'+index).display='none';

                            if(isset($d('root_search_face_image_info_'+index)))
                                $s('root_search_face_image_info_'+index).display='none';

                            if(isset($d('root_search_face_image_loading_'+index)))
                                $s('root_search_face_image_loading_'+index).display='block';

                            setTimeout(function(){

                                if(isset($d('root_search_face_image_loading_'+index)))
                                    $s('root_search_face_image_loading_'+index).opacity=1;

                            },40);

                        },300);

                        for(file_index in files)
                            page_object.action.root.action.root_face_block.action.image.drag.files.load(files[file_index],index);

                    },
                    'load':function(file,index){

                        page_object.action.root.action.root_face_block.action.image.drag.is_work=false;

                        if(isObject(file)){

                            switch(file.type){

                                case'image/jpeg':
                                case'image/png':{

                                    var reader=new FileReader();

                                    page_object.action.root.action.root_face_block.action.image.list[index]['link']  =null;
                                    page_object.action.root.action.root_face_block.action.image.list[index]['file']  =file;

                                    reader.onload=function(){

                                        page_object.action.root.action.root_face_block.action.image.preview(reader.result,function(){},index);

                                    };

                                    reader.readAsDataURL(file);

                                    break;

                                }

                                default:{

                                    page_object.action.root.action.root_face_block.action.image.reset_item(index);

                                    $d('root_search_face_image_error_'+index).innerHTML     ='Неверный формат файла';
                                    $s('root_search_face_image_error_'+index).display       ='block';
                                    $s('root_search_face_image_error_'+index).color         ='#ff0000';

                                    setTimeout(function(){

                                        $s('root_search_face_image_error_'+index).opacity=1;

                                    },40);

                                    setTimeout(function(){

                                        $s('root_search_face_image_error_'+index).opacity=0;

                                        setTimeout(function(){

                                            $s('root_search_face_image_error_'+index).display    ='none';
                                            $s('root_search_face_image_error_'+index).color      ='#';
                                            $d('root_search_face_image_error_'+index).innerHTML  ='Загрузка...';

                                            if(isset($d('root_search_face_image_block_'+index)))
                                                $s('root_search_face_image_block_'+index).borderColor='';

                                        },340);

                                    },3000);

                                    break;

                                }

                            }

                        }

                    }
                }
            },
            'send_link':function(image_link,index){

                if(page_object.action.root.action.root_face_block.action.image.drag.is_work)
                    return false;

                page_object.action.root.action.root_face_block.action.image.drag.is_work=true;

                let post='';

                post+='image_link='+uniEncode(image_link);

                if(isset($d('root_search_face_button'))){

                    $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                    $d('root_search_face_button').innerHTML     ='Загрузка...';
                    $d('root_search_face_button').onclick       =function(){};

                }
                else
                    return false;

                if(isset($d('root_search_face_image_block_'+index)))
                    $s('root_search_face_image_block_'+index).borderColor='';

                if(isset($d('root_search_face_button_choose_'+index)))
                    $s('root_search_face_button_choose_'+index).opacity=0;

                if(isset($d('root_search_face_image_info_'+index)))
                    $s('root_search_face_image_info_'+index).opacity=0;

                if(isset($d('root_search_face_image_src_object_'+index)))
                    $s('root_search_face_image_src_object_'+index).opacity=0;

                setTimeout(function(){

                    if(isset($d('root_search_face_image_src_object_'+index)))
                        removeElement($d('root_search_face_image_src_object_'+index));

                    if(isset($d('root_search_face_button_choose_'+index)))
                        $s('root_search_face_button_choose_'+index).display='none';

                    if(isset($d('root_search_face_image_info_'+index)))
                        $s('root_search_face_image_info_'+index).display='none';

                    if(isset($d('root_search_face_image_loading_'+index)))
                        $s('root_search_face_image_loading_'+index).display='block';

                    setTimeout(function(){

                        $s('root_search_face_image_loading_'+index).opacity=1;

                    },40);

                },300);

                send({
                    'scriptPath':'/api/json/upload_file_from_link',
                    'postData':post,
                    'onComplete':function(j,worktime){

                        trace('SEND IMAGE LINK FROM root');

                        let  data
                            ,dataTemp;

                        dataTemp=j.responseText;
                        trace(dataTemp);
                        data=jsonDecode(dataTemp);
                        trace(data);
                        trace_worktime(worktime,data);

                        page_object.action.root.action.root_face_block.action.image.drag.is_work=false;

                        if(isset(data['error'])){

                            if(isset($d('root_search_info_block'))){

                                $d('root_search_info_block').setAttribute('class','root_search_info_block_red');

                                $d('root_search_info_block').innerHTML='Проблемы с загрузкой файла';

                                setTimeout(function(){

                                    if(isset($d('root_search_info_block'))){

                                        $s('root_search_info_block').opacity    =1;
                                        $s('root_search_info_block').height     ='auto';

                                    }

                                },40);

                                setTimeout(function(){

                                    if(isset($d('root_search_info_block'))){

                                        $s('root_search_info_block').opacity    =0;
                                        $s('root_search_info_block').height     ='';

                                    }

                                },3000);

                            }

                            setTimeout(page_object.action.root.action.root_face_block.action.image.reset,3000);

                        }
                        else{

                            page_object.action.root.action.root_face_block.action.image.list[index]['file_ID']   =data['data']['file_ID'];
                            page_object.action.root.action.root_face_block.action.image.list[index]['image_ID']  =data['data']['image_ID'];
                            page_object.action.root.action.root_face_block.action.image.list[index]['link']      =data['data']['link'];

                            page_object.action.root.action.root_face_block.action.image.preview(data['data']['link'],function(){},index);

                        }

                    }
                });

            },
            'image_load':function(image_link,callback,index){

                if(isset($d('root_search_face_image_src_object_'+index))){

                    $s('root_search_face_image_src_object_'+index).opacity=0;

                    setTimeout(function(){

                        if(isset($d('root_search_face_image_src_'+index)))
                            $d('root_search_face_image_src_'+index).innerHTML='';

                        page_object.action.root.action.root_face_block.action.image.image_load(image_link,callback,index);

                    },300);

                    return true;

                }

                var img=new Image();

                img.src         =image_link;
                img.onload      =function(){

                    page_object.action.root.action.root_face_block.action.image.image_len++;

                    if(isset($d('root_search_face_button'))){

                        $d('root_search_face_button').setAttribute('class','root_search_face_button');

                        let  i
                            ,list           =page_object.action.root.action.root_face_block.action.image.list
                            ,list_count     =0;

                        for(i=0;i<3;i++)
                            if(
                                  !empty(list[i]['image_ID'])
                                ||!empty(list[i]['file'])
                                ||!empty(list[i]['link'])
                            )
                                list_count++;

                        if(isset($d('root_search_face_button_label')))
                            $d('root_search_face_button_label').innerHTML=(list_count>1)?'Проверить фотографии':'Проверить фотографию';
                        else if(isset($d('root_search_face_button')))
                            $d('root_search_face_button').innerHTML=(list_count>1)?'Проверить фотографии':'Проверить фотографию';

                        // $d('root_search_face_button').innerHTML=page_object.action.root.action.root_face_block.action.image.image_len===1?'Проверить фотографию':'Проверить фотографии';

                        if(isset(page_object.action.profile)){

                            if(empty(page_object.action.profile.data['user']))
                                $d('root_search_face_button').onclick=page_object.action.root.action.root_face_block.action.reg_or_auth.init;
                            else
                                $d('root_search_face_button').onclick=page_object.action.root.action.root_face_block.action.image.upload.init;

                        }
                        else
                            $d('root_search_face_button').onclick=page_object.action.root.action.root_face_block.action.reg_or_auth.init;

                    }

                    if(!isset($d('root_search_face_reset_'+index))){

                        el=addElement({
                            'tag':'div',
                            'id':'root_search_face_reset_'+index,
                            'class':'root_search_face_reset',
                            'style':'margin-left: -19px; margin-top: -1px;'
                        });

                        if(isset($d('root_search_face_image_block_container_'+index)))
                            $d('root_search_face_image_block_container_'+index).appendChild(el);

                    }

                    if(!isset($d('root_search_face_reset'))&&!OS.isMobile){

                        el=addElement({
                            'tag':'div',
                            'id':'root_search_face_reset',
                            'class':'root_search_face_reset',
                            'style':'margin-left: -19px; margin-top: -1px;'
                        });

                        $d('root_face_content').appendChild(el);

                    }

                    if(isset($d('root_search_face_reset_'+index)))
                        $d('root_search_face_reset_'+index).onclick=page_object.action.root.action.root_face_block.action.image.reset_item;

                    if(isset($d('root_search_face_reset')))
                        $d('root_search_face_reset').onclick=page_object.action.root.action.root_face_block.action.image.reset;

                    if(isset($d('root_search_face_button_cancel')))
                        $d('root_search_face_button_cancel').onclick=page_object.action.root.action.root_face_block.action.image.reset;

                    if(isset($d('root_search_face_image_loading_'+index)))
                        $s('root_search_face_image_loading_'+index).opacity=0;

                    setTimeout(function(){

                        if(isset($d('root_search_face_image_loading_'+index)))
                            $s('root_search_face_image_loading_'+index).display='none';

                    },300);

                    var  img_w      =this.width
                        ,img_h      =this.height
                        ,perc       =img_w/img_h
                        ,el_w       =360
                        ,el_h       =OS.isMobile?(winSize.winHeight-200):(winSize.winHeight-300)
                        ,r_img_w
                        ,r_img_h;

                    if(OS.isMobile){

                        // el_w=elementSize.width($d('root_search_face_image_block_'+index));
                        el_w=winSize.winWidth-35;

                    }

                    if(img_w>=img_h){

                        r_img_w     =el_w;
                        r_img_h     =r_img_w/perc;

                    }
                    else{

                        r_img_w     =el_w;
                        r_img_h     =r_img_w/perc;

                        if(r_img_h>el_h){

                            r_img_h     =el_h;
                            r_img_w     =r_img_h*perc;

                        }

                    }

                    var el=addElement({
                        'tag':'img',
                        'id':'root_search_face_image_src_object_'+index,
                        'class':'root_search_face_image_src_object',
                        'src':image_link,
                        'width':r_img_w,
                        'height':r_img_h
                    });

                    if(isset($d('root_search_face_image_src_'+index)))
                        $d('root_search_face_image_src_'+index).appendChild(el);

                    setTimeout(function(){

                        if(isset($d('root_search_face_image_src_object_'+index))){

                            if(isset($d('root_right_content'))){

                                $s('root_right_content').opacity=0;

                                setTimeout(function(){

                                    $s('root_right_content').display='none';

                                },300);

                            }

                            if(isset($d('root_left_content'))){

                                $s('root_left_content').opacity=0;

                                setTimeout(function(){

                                    $s('root_left_content').display='none';

                                },300);

                            }

                            if(isset($d('root_search_face_image_row_container_'+index))){

                                $s('root_search_face_image_row_container_'+index).height=r_img_h+'px';

                            }

                            if(isset($d('root_search_face_image_block_'+index))){

                                $s('root_search_face_image_block_'+index).width        =r_img_w+'px';
                                $s('root_search_face_image_block_'+index).height       =r_img_h+'px';
                                // $s('root_search_face_image_block_'+index).transform    ='translate(-'+((r_img_w)/2)+'px, 0)';

                                if(!OS.isMobile)
                                    $s('root_search_face_image_block_'+index).transform='translate(calc((400px - '+r_img_w+'px)/2), -50%)';

                                if(OS.isMobile){

                                    $s('root_search_face_image_block_'+index).top                       =0;
                                    $s('root_search_face_image_row_container_'+index).marginBottom      =index===2?0:'10px';

                                }

                            }

                            if(isset($d('root_search_face_reset_'+index)))
                                $s('root_search_face_reset_'+index).opacity=1;

                            if(isset($d('root_face_content'))){

                                if(!OS.isMobile)
                                    $s('root_face_content').width='1200px';

                                // $s('root_face_content').left=0;

                            }

                            if(isset($d('root_search_face_image_block_container_'+index))){

                                $s('root_search_face_image_block_container_'+index).height=r_img_h+'px';

                            }

                            setTimeout(function(){

                                let  h_max=0
                                    ,img_h
                                    ,image_index;

                                if(isset($d('root_search_face_image_src_'+index)))
                                    $s('root_search_face_image_src_'+index).opacity=1;

                                for(image_index=0;image_index<3;image_index++)
                                    if(isset($d('root_search_face_image_src_object_'+image_index))){

                                        img_h=elementSize.height($d('root_search_face_image_src_object_'+image_index));

                                        if(img_h>h_max)
                                            h_max=img_h;

                                    }

                                if(isset($d('root_search_face_image_row_container_0'))){

                                    if(!OS.isMobile)
                                        $s('root_search_face_image_row_container_0').height=h_max+'px';

                                    $s('root_search_face_image_row_container_0').display='block';

                                    setTimeout(function(){

                                        $s('root_search_face_image_row_container_0').opacity=1;

                                    },40);

                                }

                                if(isset($d('root_search_face_image_row_container_1'))){

                                    if(!OS.isMobile)
                                        $s('root_search_face_image_row_container_1').height=h_max+'px';

                                    $s('root_search_face_image_row_container_1').display='block';

                                    setTimeout(function(){

                                        $s('root_search_face_image_row_container_1').opacity=1;

                                    },40);

                                }

                                if(isset($d('root_search_face_image_row_container_2'))){

                                    if(!OS.isMobile)
                                        $s('root_search_face_image_row_container_2').height=h_max+'px';

                                    $s('root_search_face_image_row_container_2').display='block';

                                    setTimeout(function(){

                                        $s('root_search_face_image_row_container_2').opacity=1;

                                    },40);

                                }

                            },300);

                        }

                    },40);

                };
                img.onerror     =function(){};

            },
            'preload':function(file,callback,index){

                if(isset($d('root_search_face_image_block_'+index)))
                    $s('root_search_face_image_block_'+index).borderColor='';

                if(isset($d('root_search_face_button_choose_'+index)))
                    $s('root_search_face_button_choose_'+index).opacity=0;

                if(isset($d('root_search_face_button'))){

                    $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                    $d('root_search_face_button').innerHTML     ='Загрузка...';
                    $d('root_search_face_button').onclick       =function(){};

                }
                else
                    return false;

                if(isset($d('root_search_face_image_loading_'+index)))
                    $s('root_search_face_image_loading_'+index).display='block';

                setTimeout(function(){

                    $s('root_search_face_image_loading_'+index).opacity=1;

                },40);

                if(OS.isMobile){

                    if(isset($d('root_left_content'))){

                        $s('root_left_content').opacity     =0;
                        $s('root_left_content').padding     =0;
                        $s('root_left_content').height      =0;

                    }

                    if(isset($d('root_right_content'))){

                        $s('root_right_content').opacity    =0;
                        $s('root_right_content').padding    =0;
                        $s('root_right_content').height     =0;

                    }

                    if(isset($d('root_search_face_button'))){

                        $s('root_search_face_button').display='block';

                    }

                    if(isset($d('root_search_face_button_cancel'))){

                        $s('root_search_face_button_cancel').display='block';

                    }

                    if(isset($d('root_search_face_button_shot')))
                        $s('root_search_face_button_shot').display='none';

                }

                if(isset($d('root_search_face_image_info_'+index)))
                    $s('root_search_face_image_info_'+index).opacity=0;

                if(isset($d('root_search_face_image_src_object_'+index)))
                    $s('root_search_face_image_src_object_'+index).opacity=0;

                setTimeout(function(){

                    trace('Preload 4: '+index);

                    if(isset($d('root_search_face_image_src_object_'+index)))
                        removeElement($d('root_search_face_image_src_object_'+index));

                    if(isset($d('root_search_face_button_choose_'+index)))
                        $s('root_search_face_button_choose_'+index).display='none';

                    if(isset($d('root_search_face_image_info_'+index)))
                        $s('root_search_face_image_info_'+index).display='none';

                    let el;

                    if(isset($d('root_search_face_image_loading_'+index)))
                        $s('root_search_face_image_loading_'+index).display='block';

                    setTimeout(function(){

                        $s('root_search_face_image_loading_'+index).opacity=1;

                        callback(file,index);

                    },40);

                },300);

            },
            'preview':function(result,callback,index){

                page_object.action.root.action.root_face_block.action.image.image_load(result,callback,index);

            },
            'change':function(e){
                
                e=e||window.event;

                let  file           =e.target.files
                    ,file_list      =page_object.action.root.action.root_face_block.action.image.list
                    ,i
                    ,index          =this.id.split('_')[5]
                    ,image_len      =page_object.action.root.action.root_face_block.action.image.image_len
                    ,index_current  =0
                    ,image_max_len  =3-image_len;

                if(file.length===1)
                    page_object.action.root.action.root_face_block.action.image.change_index(index,file[0]);
                else{

                    for(i=0;i<file.length;i++){

                        if(index_current!==0){

                            for(index=0;index<3;index++)
                                if(
                                      empty(file_list[index]['file'])
                                    &&empty(file_list[index]['link'])
                                )
                                    break;

                        }

                        page_object.action.root.action.root_face_block.action.image.change_index(index,file[i]);

                        index_current++;

                        if(index_current>=image_max_len)
                            break;

                    }

                }

            },
            'change_index':function(index,f){

                page_object.action.root.action.root_face_block.action.image.list[index]['file']=f;

                if(isset($d('root_search_face_image_src_'+index)))
                    $s('root_search_face_image_src_'+index).opacity=0;

                page_object.action.root.action.root_face_block.action.image.preload(f,function(f,index){

                    prepare_image_orientation.get_image_with_true_orientation(f,function(file,callback,index){

                        page_object.action.root.action.root_face_block.action.image.preview(file,callback,index);

                    },index);

                },index);

            },
            'reset_item':function(index){

                let  i
                    ,list           =page_object.action.root.action.root_face_block.action.image.list
                    ,list_count     =0;

                page_object.action.root.action.root_face_block.action.image.image_len--;

                for(i=0;i<3;i++)
                    if(
                          !empty(list[i]['image_ID'])
                        ||!empty(list[i]['file'])
                        ||!empty(list[i]['link'])
                    )
                        list_count++;

                if(list_count===1){

                    page_object.action.root.action.root_face_block.action.image.reset();

                    return true;

                }

                index=!isNumber(index)?this.id.split('_')[4]:index;

                trace('Index: '+index);

                if(isset($d('root_search_face_image_input_'+index)))
                    $d('root_search_face_image_input_'+index).value='';

                if(!isset(page_object.action.root.action.root_face_block.action.image.list[index]))
                    page_object.action.root.action.root_face_block.action.image.list[index]={};

                page_object.action.root.action.root_face_block.action.image.list[index]['file_ID']       =null;
                page_object.action.root.action.root_face_block.action.image.list[index]['image_ID']      =null;
                page_object.action.root.action.root_face_block.action.image.list[index]['file']          =null;
                page_object.action.root.action.root_face_block.action.image.list[index]['link']          =null;

                if(isset($d('root_search_face_reset_'+index)))
                    $s('root_search_face_reset_'+index).opacity=0;

                if(isset($d('root_search_face_image_loading_'+index)))
                    $s('root_search_face_image_loading_'+index).opacity=0;

                if(isset($d('root_search_face_coords_list_'+index)))
                    removeElement($d('root_search_face_coords_list_'+index));

                if(isset($d('root_search_face_image_src_object_'+index)))
                    $s('root_search_face_image_src_object_'+index).opacity=0;

                if(isset($d('root_search_face_button_choose_'+index)))
                    $s('root_search_face_button_choose_'+index).display='block';

                if(OS.isMobile)
                    if(isset($d('root_search_face_image_row_container_'+index)))
                        $s('root_search_face_image_row_container_'+index).height='';

                if(OS.isMobile){

                    if(isset($d('root_search_face_image_block_'+index)))
                        $d('root_search_face_image_block_'+index).setAttribute('style','');

                }
                else{

                    if(isset($d('root_search_face_image_block_'+index))){

                        $s('root_search_face_image_block_'+index).width         ='';
                        $s('root_search_face_image_block_'+index).height        ='';
                        $s('root_search_face_image_block_'+index).transform     ='';

                    }

                }

                if(!OS.isMobile)
                    if(isset($d('root_search_face_image_info_'+index)))
                        $s('root_search_face_image_info_'+index).display='block';

                setTimeout(function(){

                    if(isset($d('root_search_face_button_choose_'+index)))
                        $s('root_search_face_button_choose_'+index).opacity=1;

                    if(isset($d('root_search_face_image_info_'+index)))
                        $s('root_search_face_image_info_'+index).opacity=1;

                    if(isset($d('root_search_face_image_src_'+index)))
                        $s('root_search_face_image_src_'+index).opacity=0;

                },40);

                setTimeout(function(){

                    if(isset($d('root_search_face_button_progress')))
                        $s('root_search_face_button_progress').width=0;

                    if(isset($d('root_search_face_reset_'+index)))
                        removeElement($d('root_search_face_reset_'+index));

                    if(isset($d('root_search_face_image_src_object_'+index)))
                        removeElement($d('root_search_face_image_src_object_'+index));

                    if(isset($d('root_search_face_image_src_'+index)))
                        $d('root_search_face_image_src_'+index).innerHTML='';

                    let  i
                        ,list           =page_object.action.root.action.root_face_block.action.image.list
                        ,list_count     =0;

                    for(i=0;i<3;i++)
                        if(
                              !empty(list[i]['image_ID'])
                            ||!empty(list[i]['file'])
                            ||!empty(list[i]['link'])
                        )
                            list_count++;

                    if(isset($d('root_search_face_button_label')))
                        $d('root_search_face_button_label').innerHTML=(list_count>1)?'Проверить фотографии':'Проверить фотографию';
                    else if(isset($d('root_search_face_button')))
                        $d('root_search_face_button').innerHTML=(list_count>1)?'Проверить фотографии':'Проверить фотографию';

                    trace('LIST COUNT 2: '+list_count);

                    if(list_count===0){

                        if(isset($d('root_search_face_button'))){

                            $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                            $d('root_search_face_button').onclick=function(){};

                        }

                    }
                    else{

                        if(isset($d('root_search_face_button'))){

                            $d('root_search_face_button').setAttribute('class','root_search_face_button');

                            if(isset(page_object.action.profile)){

                                if(empty(page_object.action.profile.data['user']))
                                    $d('root_search_face_button').onclick=page_object.action.root.action.root_face_block.action.reg_or_auth.init;
                                else
                                    $d('root_search_face_button').onclick=page_object.action.root.action.root_face_block.action.image.upload.init;

                            }
                            else
                                $d('root_search_face_button').onclick=page_object.action.root.action.root_face_block.action.reg_or_auth.init;

                        }

                    }

                },300);

            },
            'reset':function(){

                let index;

                page_object.upload.reset();

                page_object.action.root.action.root_face_block.action.image.list=[
                    {
                        'image_ID':null,
                        'link':null,
                        'file':null
                    },
                    {
                        'image_ID':null,
                        'link':null,
                        'file':null
                    },
                    {
                        'image_ID':null,
                        'link':null,
                        'file':null
                    }
                ];
                page_object.action.root.action.root_face_block.action.image.image_len=0;

                if(isset($d('page_object.action.profile'))){

                    page_object.action.root.action.root_face_block.action.image.face.face_change_len=0;
                    page_object.action.root.action.root_face_block.action.image.face.list=[
                        {
                            'face_index':null,
                            'image_ID':null,
                            'image_data':null,
                            'file_ID':null,
                            'face_len':0,
                            'face_list':null
                        },
                        {
                            'face_index':null,
                            'image_ID':null,
                            'image_data':null,
                            'file_ID':null,
                            'face_len':0,
                            'face_list':null
                        },
                        {
                            'face_index':null,
                            'image_ID':null,
                            'image_data':null,
                            'file_ID':null,
                            'face_len':0,
                            'face_list':null
                        }
                    ];

                }

                if(isset($d('root_search_face_content_loading'))){

                    $s('root_search_face_content_loading').opacity=0;

                    setTimeout(function(){

                        if(isset($d('root_search_face_content_loading')))
                            $s('root_search_face_content_loading').display='none';

                    },300);

                }

                if(isset($d('root_search_face_price'))){

                    $s('root_search_face_price').opacity=0;

                    setTimeout(function(){

                        $s('root_search_face_price').display    ='none';
                        $d('root_search_face_price').innerHTML  ='Стоимость запроса <span id="root_search_face_cost">0</span> ₽';

                    },300);

                }

                for(index=0;index<3;index++){

                    // if(OS.isMobile)
                    //     if(isset($d('root_search_face_image_row_container_'+index)))
                    //         $s('root_search_face_image_row_container_'+index).marginBottom=0;

                    if(isset($d('root_search_face_image_input_'+index)))
                        $d('root_search_face_image_input_'+index).value='';

                    page_object.action.root.action.root_face_block.action.image.list[index]['file_ID']       =null;
                    page_object.action.root.action.root_face_block.action.image.list[index]['image_ID']      =null;
                    page_object.action.root.action.root_face_block.action.image.list[index]['file']          =null;
                    page_object.action.root.action.root_face_block.action.image.list[index]['link']          =null;

                    if(isset($d('root_search_face_reset_'+index)))
                        $s('root_search_face_reset_'+index).opacity=0;

                    if(isset($d('root_search_face_image_loading_'+index)))
                        $s('root_search_face_image_loading_'+index).opacity=0;

                    if(isset($d('root_search_face_coords_list_'+index)))
                        removeElement($d('root_search_face_coords_list_'+index));

                    if(isset($d('root_search_face_image_src_object_'+index)))
                        $s('root_search_face_image_src_object_'+index).opacity=0;

                    if(isset($d('root_search_face_button_choose_'+index)))
                        $s('root_search_face_button_choose_'+index).display='block';

                    if(OS.isMobile){

                        if(isset($d('root_search_face_image_block_'+index)))
                            $d('root_search_face_image_block_'+index).setAttribute('style','');

                    }
                    else{

                        if(isset($d('root_search_face_image_block_'+index))){

                            $s('root_search_face_image_block_'+index).width         ='';
                            $s('root_search_face_image_block_'+index).height        ='';
                            $s('root_search_face_image_block_'+index).transform     ='';

                        }

                    }

                    if(isset($d('root_search_face_image_row_container_'+index))){

                        $s('root_search_face_image_row_container_'+index).display   =(parseInt(index)===0)?'block':'none';
                        $s('root_search_face_image_row_container_'+index).opacity   =(parseInt(index)===0)?1:0;

                        $s('root_search_face_image_row_container_'+index).height='';

                    }

                    if(!OS.isMobile)
                        if(isset($d('root_search_face_image_info_'+index)))
                            $s('root_search_face_image_info_'+index).display='block';

                }

                if(isset($d('root_left_content'))){

                    $s('root_left_content').display='block';

                    setTimeout(function(){

                        $s('root_left_content').opacity=1;

                    },40);

                }

                if(isset($d('root_right_content'))){

                    $s('root_right_content').display='block';

                    setTimeout(function(){

                        $s('root_right_content').opacity=1;

                    },40);

                }

                if(isset($d('root_face_content'))){

                    $s('root_face_content').width   ='';
                    $s('root_face_content').left    ='';

                }

                if(OS.isMobile){

                    if(isset($d('root_left_content'))){

                        $s('root_left_content').opacity     =1;
                        $s('root_left_content').padding     ='';
                        $s('root_left_content').height      ='auto';

                    }

                    if(isset($d('root_right_content'))){

                        $s('root_right_content').opacity    =1;
                        $s('root_right_content').padding    ='';
                        $s('root_right_content').height     ='auto';

                    }

                    if(isset($d('root_search_face_button')))
                        $s('root_search_face_button').display='none';

                    if(isset($d('root_search_face_button_cancel')))
                        $s('root_search_face_button_cancel').display='none';

                    if(isset($d('root_search_face_button_shot')))
                        $s('root_search_face_button_shot').display='block';

                }

                setTimeout(function(){

                    for(index=0;index<3;index++){

                        if(isset($d('root_search_face_button_choose_'+index)))
                            $s('root_search_face_button_choose_'+index).opacity=1;

                        if(isset($d('root_search_face_image_info_'+index)))
                            $s('root_search_face_image_info_'+index).opacity=1;

                        if(isset($d('root_search_face_image_src_'+index)))
                            $s('root_search_face_image_src_'+index).opacity=0;

                    }

                },40);

                setTimeout(function(){

                    if(isset($d('root_search_face_button_label')))
                        $d('root_search_face_button_label').innerHTML='Проверить фото';
                    else if(isset($d('root_search_face_button')))
                        $d('root_search_face_button').innerHTML='Проверить фото';

                    if(isset($d('root_search_face_button_progress')))
                        $s('root_search_face_button_progress').width=0;

                    if(isset($d('root_search_face_reset')))
                        removeElement($d('root_search_face_reset'));

                    for(index=0;index<3;index++){

                        if(isset($d('root_search_face_image_loading_'+index)))
                            $s('root_search_face_image_loading_'+index).display='none';

                        if(isset($d('root_search_face_reset_'+index)))
                            removeElement($d('root_search_face_reset_'+index));

                        if(isset($d('root_search_face_image_src_object_'+index)))
                            removeElement($d('root_search_face_image_src_object_'+index));

                        if(isset($d('root_search_face_image_src_'+index)))
                            $d('root_search_face_image_src_'+index).innerHTML='';

                    }

                    if(isset($d('root_search_face_button'))){

                        $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                        $d('root_search_face_button').onclick=function(){};

                    }

                },300);

            },
            'upload':{
                'init':function(){

                    if(!empty(page_object.action.root.action.root_face_block.action.image.file))
                        page_object.action.root.action.root_face_block.action.image.upload.file.init();
                    else if(!empty(page_object.action.root.action.root_face_block.action.image.link))
                        page_object.action.root.action.root_face_block.action.image.upload.link.init();

                },
                'link':{
                    'init':function(){

                        alert('В данной версии приложения сервис не доступен');

                    }
                },
                'file':{
                    'init':function(){
                        
                        page_object.action.root.action.root_face_block.action.image.upload.file.on_change_file();

                    },
                    'on_change_file':function(){

                        let  data
                            ,list           =[]
                            ,i
                            ,file_list      =[
                                page_object.action.root.action.root_face_block.action.image.file
                            ]
                            ,list_item      ={};

                        if(file_list.length===0)
                            return false;

                        $d('root_search_face_button').setAttribute('class','root_search_face_button_disable');

                        $d('root_search_face_button').innerHTML     ='Загрузка 1%';
                        $d('root_search_face_button').onclick       =function(){};

                        for(i=0;i<file_list.length;i++){

                            list_item={
                                'file_ID':null,
                                'remove':false,
                                'file':file_list[i]
                            };

                            list.push(list_item);

                        }

                        data={
                            'on_progress':page_object.action.root.action.root_face_block.action.image.upload.file.on_progress.init,
                            'on_uploaded':page_object.action.root.action.root_face_block.action.image.upload.file.on_uploaded.init,
                            'on_complete':page_object.action.root.action.root_face_block.action.image.upload.file.on_complete.init,
                            'file_type':'image',
                            'list':list
                        };

                        page_object.upload.reset();

                        page_object.upload.init(null,data);

                    },
                    'on_progress':{
                        'init':function(){

                            if(isset($d('dialog_face_image_progress_line'))){

                                let perc=(page_object.upload.data['list'][page_object.upload.file_item['index']]['percent']*100).toFixed(1);

                                if((page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_index']+2)<page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_len'])
                                    $d('root_search_face_button').innerHTML='Загрузка '+perc+'%';
                                else
                                    $d('root_search_face_button').innerHTML='Обработка изображения';

                            }

                        }
                    },
                    'on_uploaded':{
                        'init':function(){

                            if(isset($d('root_search_face_button'))){

                                $d('root_search_face_button').innerHTML     ='Выберите лицо';
                                $d('root_search_face_button').onclick       =function(){};

                            }

                        }
                    },
                    'on_complete':{
                        'init':function(){}
                    }
                }
            }
        },
        'reg_or_auth':{
            'dialog_index':null,
            'init':function(){

                var  inner='';

                inner+='<div id="reg_or_auth_block" class="dialog_row'+(OS.isMobile?'_mobile':'')+'">';

                    inner+='Услуга поиска по фотографиям доступна только зарегистрированным и авторизованным пользователям. Пожалуйста, войдите в систему чтобы продолжить';

                inner+='</div>';

                inner+='<div class="dialog_row'+(OS.isMobile?'_mobile':'')+'">';

                    inner+='<div id="dialog_reg" class="dialog_submit_reg" style="position: relative;">Зарегистрироваться</div>';

                inner+='</div>';

                inner+='<div class="dialog_row'+(OS.isMobile?'_mobile':'')+'">';

                    inner+='<div id="dialog_auth" class="dialog_submit_reg" style="position: relative;">Авторизоваться</div>';

                inner+='</div>';

                page_object.action.root.action.root_face_block.action.reg_or_auth.dialog_index=page_object.dialog.init({
                    'inner':inner,
                    'on_create':function(){

                        trace('Test START');

                        if(isset($d('dialog_reg')))
                            $d('dialog_reg').onclick=page_object.action.root.action.header.reg.click;

                        if(isset($d('dialog_auth')))
                            $d('dialog_auth').onclick=page_object.action.root.action.header.auth.click;

                    },
                    'cancel':true
                });

            },
            'un_show':function(){

                if(!empty(page_object.action.root.action.root_face_block.action.reg_or_auth.dialog_index))
                    page_object.dialog.action.un_show.init(page_object.action.root.action.root_face_block.action.reg_or_auth.dialog_index,true);

            }
        },
        'disclaimer':{
            'dialog_index':null,
            'init':function(){

                var isset_success=getCookie('disclaimer');

                if(is_null(isset_success)||parseInt(isset_success)===0){

                    page_object.action.root.action.root_face_block.action.disclaimer.box();

                    return false;

                }

                return true;

            },
            'box':function(){

                var  inner='';

                inner+='<div class="dialog_row'+(OS.isMobile?'_mobile':'')+'" style="max-height: calc(100vh - 120px);overflow-y: auto">';

                inner+='<div class="dialog_row'+(OS.isMobile?'_mobile':'')+'" style="text-align: center">';
                    inner+='<b style="font-size: 32px;color: #1b93db;">DISCLAIMER</b>';
                inner+='</div>';

                inner+='<div id="reg_or_auth_block" class="dialog_row'+(OS.isMobile?'_mobile':'')+'">';

                    inner+='<p style="margin-bottom: 10px; text-align: center"><i>[отказ от ответственности за возможные последствия использования настоящего информационного ресурса]</i></p>';
                    inner+='<p style="margin-bottom: 10px;"><b>Shluham.net</b> - специализированная Поисковая система, основная функция которой - индексация текстовых и графических данных, находящихся в открытом доступе в сети Интернет и обеспечение поиска по этим данным с использованием системы распознавания лиц. Функционирование информационной системы происходит полностью автоматически. Администрация и разработчики НЕ добавляют, НЕ редактируют и НЕ удаляют данные поискового индекса в ручном режиме.</p>';
                    inner+='<p style="margin-bottom: 10px;">Информация, которую Вы можете найти с использованием настоящей Поисковой системы, может оказаться достаточно чувствительной как для Вас лично, так и для третьих лиц. Поэтому, продолжая использовать shluham.net, Вы соглашаетесь с тем, что принимаете на себя всю полноту ответственности за все возможные прямые и косвенные последствия использования настоящей Поисковой системы и не вправе требовать компенсации любого, прямого и/или косвенного ущерба причиненного Вам лично и/или третьим лицам и явившемся следствием использования настоящей Поисковой системы.</p>';
                    inner+='<p>Материалы, к которым Вы можете получить доступ при использовании настоящей Поисковой системы могут содержать информацию, запрещенную к распространению среди детей (18+). Продолжая использование настоящей Поисковой системы Вы подтверждаете, что Вам больше 18 лет.</p>';

                inner+='</div>';

                inner+='<div class="dialog_row'+(OS.isMobile?'_mobile':'')+'">';

                    inner+='<div id="dialog_cancel" class="dialog_submit_reg" style="position: relative;">Отказаться от использования</div>';

                inner+='</div>';

                inner+='<div class="dialog_row'+(OS.isMobile?'_mobile':'')+'">';

                    inner+='<div id="dialog_success" class="dialog_submit_reg" style="position: relative;'+(OS.isMobile?' height: auto;':'')+'">Согласиться с условиями и продолжить использование. Мне больше 18 лет</div>';

                inner+='</div>';
                inner+='</div>';

                page_object.action.root.action.root_face_block.action.disclaimer.dialog_index=page_object.dialog.init({
                    'w':900,
                    'z':999999,
                    'inner':inner,
                    'on_create':function(){

                        if(isset($d('dialog_cancel')))
                            $d('dialog_cancel').onclick=page_object.action.root.action.root_face_block.action.disclaimer.cancel;

                        if(isset($d('dialog_success')))
                            $d('dialog_success').onclick=page_object.action.root.action.root_face_block.action.disclaimer.success;


                    }
                });

            },
            'success':function(){

                let date=new Date();

                date.setTime(date.getTime()+(365*24*60*60*1000));

                setCookie('disclaimer','1',date.toUTCString(),'/',document.location.host,(location.protocol==='https:'));

                page_object.dialog.action.un_show.init(page_object.action.root.action.root_face_block.action.disclaimer.dialog_index,true);

            },
            'cancel':function(){

                let date=new Date();

                date.setTime(date.getTime()+(365*24*60*60*1000));

                setCookie('disclaimer','0',date.toUTCString(),'/',document.location.host,(location.protocol==='https:'));

                page_object.dialog.action.un_show.init(page_object.action.root.action.root_face_block.action.disclaimer.dialog_index,true);

            }
        }
    },
    'remove':function(){

        if(isset($d('root_face_block')))
            removeElement($d('root_face_block'));

    },
    'resize':function(){}
};