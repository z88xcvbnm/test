page_object.action.root.action.header={
    'show':function(){

        if(isset($d('header'))){

            page_object.action.root.position.header.o=1;
            page_object.action.root.position.header.y=0;

            $s('header').opacity        =page_object.action.root.position.header.o;
            $s('header').transform      ='translate('+page_object.action.root.position.header.x+'px,'+page_object.action.root.position.header.y+'px)'

        }

        if(isset($d('menu_button')))
            $d('menu_button').onclick=page_object.action.root.action.header.menu.show;

        if(isset($d('footer')))
            $s('footer').opacity=1;

    },
    'un_show':function(remove){

        if(isset($d('header'))){

            page_object.action.root.position.header.o=0;
            page_object.action.root.position.header.y=-page_object.action.root.position.header.h;

            $s('header').opacity        =page_object.action.root.position.header.o;
            $s('header').transform      ='translate('+page_object.action.root.position.header.x+'px,'+page_object.action.root.position.header.y+'px)'

        }

        if(isset($d('footer')))
            $s('footer').opacity=0;

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.root.action.header.remove,300);

    },
    'logo':{
        'click':function(){

            let  lang_obj       =page_object.content[page_object.lang]
                ,link           ='/';

            setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

        }
    },
    'auth':{
        'click':function(){

            if(isset(page_object.action.root.action.root_face_block.action.reg_or_auth.dialog_index))
                page_object.dialog.action.un_show.init(page_object.action.root.action.root_face_block.action.reg_or_auth.dialog_index,true);

            let  lang_obj       =page_object.content[page_object.lang]
                ,link           ='/auth';

            setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

        }
    },
    'reg':{
        'click':function(){

            trace('test 0');

            if(isset(page_object.action.root.action.root_face_block.action.reg_or_auth.dialog_index))
                page_object.dialog.action.un_show.init(page_object.action.root.action.root_face_block.action.reg_or_auth.dialog_index,true);

            trace('test 1');

            let  lang_obj       =page_object.content[page_object.lang]
                ,link           ='/reg';

            trace('test 2');

            setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

            trace('test 3');

        }
    },
    'menu':{
        'show':function(){

            if(isset($d('header_menu'))){

                $s('header_menu').transform     ='translate(-100%,0)';
                $s('header_menu').opacity       =1;

            }

        },
        'un_show':function(){

            if(isset($d('header_menu'))){

                $s('header_menu').transform     ='translate(0,0)';
                $s('header_menu').opacity       =0;

            }

        },
    },
    'remove':function(){

        if(isset($d('header')))
            removeElement($d('header'));

        if(isset($d('footer')))
            removeElement($d('footer'));

    },
    'resize':function(){}
};