page_object.action.root.section_change_workflow={
    'root':{
        'init':function(){

            if(isset($d('reg_block')))
                page_object.action.reg.action.un_show();

            if(isset($d('auth_block')))
                page_object.action.auth.action.un_show(true);

            if(isset($d('confirm_email_block')))
                page_object.action.confirm_email.action.un_show(true);

            if(isset($d('reg_or_auth_block')))
                page_object.action.root.action.root_face_block.action.reg_or_auth.un_show(true);

            if(isset($d('invite_block')))
                page_object.action.invite.action.un_show();

            page_object.action.root.create.header.init();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'auth':
                        case'forgot':
                        case'invite':
                        case'registration_invite':
                        case'confirm_email':
                        case'reg':{

                            page_object.action.root.create.root_content.init();

                            return true;


                        }

                    }

                page_object.action.root.action.root_face_block.un_show();

                setTimeout(page_object.action.root.create.root_content.init,300);

            }
            else
                page_object.action.root.create.root_content.init();

        }
    },
    'auth':{
        'init':function(){

            if(isset($d('reg_block')))
                page_object.action.reg.action.un_show();

            if(isset($d('auth_block')))
                page_object.action.auth.action.un_show(true);

            if(isset($d('confirm_email_block')))
                page_object.action.confirm_email.action.un_show(true);

            if(isset($d('reg_or_auth_block')))
                page_object.action.root.action.root_face_block.action.reg_or_auth.un_show(true);

            if(isset($d('invite_block')))
                page_object.action.invite.action.un_show();

            page_object.action.root.create.header.init();

            if(isset($d('root_text_content'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.root.action.root_face_block.un_show();

                setTimeout(page_object.action.root.create.root_content.init,300);

            }
            else
                page_object.action.root.create.root_content.init();

            setTimeout(function(){

                page_object.action.auth.run(page_object.action.root.data);

            },300);

        }
    },
    'forgot':{
        'init':function(){

            if(isset($d('reg_block')))
                page_object.action.reg.action.un_show();

            if(isset($d('auth_block')))
                page_object.action.auth.action.un_show(true);

            if(isset($d('confirm_email_block')))
                page_object.action.confirm_email.action.un_show(true);

            if(isset($d('reg_or_auth_block')))
                page_object.action.root.action.root_face_block.action.reg_or_auth.un_show(true);

            if(isset($d('invite_block')))
                page_object.action.invite.action.un_show();

            page_object.action.root.create.header.init();

            if(isset($d('root_text_content'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.root.action.root_face_block.un_show();

                setTimeout(page_object.action.root.create.root_content.init,300);

            }
            else
                page_object.action.root.create.root_content.init();

            setTimeout(function(){

                page_object.action.auth.run(page_object.action.root.data);

            },300);

        }
    },
    'reg':{
        'init':function(){

            if(isset($d('auth_block')))
                page_object.action.auth.action.un_show(true);

            if(isset($d('confirm_email_block')))
                page_object.action.confirm_email.action.un_show(true);

            if(isset($d('reg_or_auth_block')))
                page_object.action.root.action.root_face_block.action.reg_or_auth.un_show(true);

            if(isset($d('invite_block')))
                page_object.action.invite.action.un_show();

            page_object.action.root.create.header.init();

            if(isset($d('root_text_content'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.root.action.root_face_block.un_show();

                setTimeout(page_object.action.root.create.root_content.init,300);

            }
            else
                page_object.action.root.create.root_content.init();

            setTimeout(function(){

                page_object.action.reg.run(page_object.action.root.data);

            },300);

        }
    },
    'invite':{
        'init':function(){

            if(isset($d('reg_block')))
                page_object.action.reg.action.un_show();

            if(isset($d('auth_block')))
                page_object.action.auth.action.un_show(true);

            if(isset($d('confirm_email_block')))
                page_object.action.confirm_email.action.un_show(true);

            if(isset($d('reg_or_auth_block')))
                page_object.action.root.action.root_face_block.action.reg_or_auth.un_show(true);

            page_object.action.root.create.header.init();

            if(isset($d('root_text_content'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.root.action.root_face_block.un_show();

                setTimeout(page_object.action.root.create.root_content.init,300);

            }
            else
                page_object.action.root.create.root_content.init();

            setTimeout(function(){

                page_object.action.invite.run(page_object.action.root.data);

            },300);

        }
    },
    'confirm_email':{
        'init':function(){

            if(isset($d('reg_block')))
                page_object.action.reg.action.un_show();

            if(isset($d('auth_block')))
                page_object.action.auth.action.un_show(true);

            if(isset($d('confirm_email_block')))
                page_object.action.confirm_email.action.un_show(true);

            if(isset($d('reg_or_auth_block')))
                page_object.action.root.action.root_face_block.action.reg_or_auth.un_show(true);

            if(isset($d('invite_block')))
                page_object.action.invite.action.un_show();

            page_object.action.root.create.header.init();

            if(isset($d('root_text_content'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.root.action.root_face_block.un_show();

                setTimeout(page_object.action.root.create.root_content.init,300);

            }
            else
                page_object.action.root.create.root_content.init();

            setTimeout(function(){

                page_object.action.confirm_email.run(page_object.action.root.data);

            },300);


        }
    },
    'root_about':{
        'init':function(){

            if(isset($d('reg_block')))
                page_object.action.reg.action.un_show();

            if(isset($d('auth_block')))
                page_object.action.auth.action.un_show(true);

            if(isset($d('confirm_email_block')))
                page_object.action.confirm_email.action.un_show(true);

            if(isset($d('reg_or_auth_block')))
                page_object.action.root.action.root_face_block.action.reg_or_auth.un_show(true);

            if(isset($d('invite_block')))
                page_object.action.invite.action.un_show();

            page_object.action.root.create.header.init();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.root.action.root_face_block.un_show();

                setTimeout(page_object.action.root.create.root_content.init,300);

            }
            else
                page_object.action.root.create.root_content.init();

            return true;

        }
    },
    'root_source':{
        'init':function(){

            if(isset($d('reg_block')))
                page_object.action.reg.action.un_show();

            if(isset($d('auth_block')))
                page_object.action.auth.action.un_show(true);

            if(isset($d('confirm_email_block')))
                page_object.action.confirm_email.action.un_show(true);

            if(isset($d('reg_or_auth_block')))
                page_object.action.root.action.root_face_block.action.reg_or_auth.un_show(true);

            if(isset($d('invite_block')))
                page_object.action.invite.action.un_show();

            page_object.action.root.create.header.init();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.root.action.root_face_block.un_show();

                setTimeout(page_object.action.root.create.root_content.init,300);

            }
            else
                page_object.action.root.create.root_content.init();

            return true;

        }
    },
    'root_term':{
        'init':function(){

            if(isset($d('reg_block')))
                page_object.action.reg.action.un_show();

            if(isset($d('auth_block')))
                page_object.action.auth.action.un_show(true);

            if(isset($d('confirm_email_block')))
                page_object.action.confirm_email.action.un_show(true);

            if(isset($d('reg_or_auth_block')))
                page_object.action.root.action.root_face_block.action.reg_or_auth.un_show(true);

            if(isset($d('invite_block')))
                page_object.action.invite.action.un_show();

            page_object.action.root.create.header.init();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.root.action.root_face_block.un_show();

                setTimeout(page_object.action.root.create.root_content.init,300);

            }
            else
                page_object.action.root.create.root_content.init();

        }
    },
    'root_city':{
        'init':function(){

            if(isset($d('reg_block')))
                page_object.action.reg.action.un_show();

            if(isset($d('auth_block')))
                page_object.action.auth.action.un_show(true);

            if(isset($d('confirm_email_block')))
                page_object.action.confirm_email.action.un_show(true);

            if(isset($d('reg_or_auth_block')))
                page_object.action.root.action.root_face_block.action.reg_or_auth.un_show(true);

            if(isset($d('invite_block')))
                page_object.action.invite.action.un_show();

            page_object.action.root.create.header.init();

            if(isset($d('root_face_block'))){

                if(isset(page_object.link.back_link_list[0]))
                    switch(page_object.link.back_link_list[0]){

                        case'registration_invite':
                        case'auth':{

                            page_object.action.profile.create.root_content.init();

                            return true;

                        }

                    }

                page_object.action.root.action.root_face_block.un_show();

                setTimeout(page_object.action.root.create.root_content.init,300);

            }
            else
                page_object.action.root.create.root_content.init();

        }
    }
};
