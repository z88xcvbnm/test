page_object.action.reg={
    'w':300,
    'h':140,
    'x':0,
    'y':0,
    'is_show':false,
    'script_list':{
        'core':{
            'root':'/show/js/root/init.js'
        }
    },
    'style_list':[
        '/show/css/system/root.css',
        '/show/css/system/dialog_block.css'
    ],
    'section_change_workflow':{},
    'reset':function(){

        page_object.action.reg.w=300;
        page_object.action.reg.h=140;
        page_object.action.reg.x=0;
        page_object.action.reg.y=0;

    },
    'init':function(data){

        var temp={
            'script_list':config.updateScriptsList(page_object.action.reg.script_list, 'Root'),
            'style_list':config.updateScriptsList(page_object.action.reg.style_list, 'Root'),
            'init_list':[
                function(){

                    if (typeof config !== 'undefined' && $.isFunction(config.updateSectionChangeWorkflow)) {
                        page_object.action.root.section_change_workflow = config.updateSectionChangeWorkflow(page_object.action.root.section_change_workflow);
                    }

                    page_object.action.root.init(data);

                }
            ]
        };

        page_object.require.init(temp);

    },
    'run':function(data){

        if(!isset($d('reg_block')))
            page_object.action.reg.create.init();

        setTimeout(page_object.action.reg.action.show,300);

    },
    'create':{
        'dialog_index':null,
        'init':function(){

            page_object.action.reg.create.reg();

        },
        'reg':function(){

            let inner='';

            inner+='<div id="reg_block" class="dialog_row">';

            inner+='<div class="dialog_row_label">';
            inner+='<span>Login</span>';
            inner+='<span class="dialog_red_field">*</span>';
            inner+='</div>';
            inner+='<div class="dialog_row_input">';
            inner+='<input type="text" id="dialog_login_input_text" class="dialog_row_input_text" value="" />';
            inner+='</div>';

            inner+='</div>';

            inner+='<div class="dialog_row">';

            inner+='<div class="dialog_row_label">';
            inner+='<span>Email</span>';
            inner+='<span class="dialog_red_field">*</span>';
            inner+='</div>';
            inner+='<div class="dialog_row_input">';
            inner+='<input type="text" id="dialog_email_input_text" class="dialog_row_input_text" value="" />';
            inner+='</div>';

            inner+='</div>';

            inner+='<div class="dialog_row">';

            inner+='<div class="dialog_row_label">';
            inner+='<span>Пароль</span>';
            inner+='<span class="dialog_red_field">*</span>';
            inner+='</div>';
            inner+='<div class="dialog_row_input">';
            inner+='<input type="password" id="dialog_password_input_text" class="dialog_row_input_text" value="" />';
            inner+='</div>';

            inner+='</div>';

            inner+='<div class="dialog_row">';

            inner+='<div class="dialog_row_label">';
            inner+='<span>Пароль еще раз</span>';
            inner+='<span class="dialog_red_field">*</span>';
            inner+='</div>';
            inner+='<div class="dialog_row_input">';
            inner+='<input type="password" id="dialog_repeat_password_input_text" class="dialog_row_input_text" value="" />';
            inner+='</div>';

            inner+='</div>';

            inner+='<div class="dialog_row" style="border-top: 1px solid #e7e7e7; padding: 10px 0 0 0;">';
                inner+='<span style="display: block; margin: 0 0 5px 0; opacity: .7;">Введите имя пользователя, который вас пригласил или промо-код:</span>';
                inner+='<input type="text" id="dialog_code_input_text" class="dialog_row_input_text" value="" />';
            inner+='</div>';

            page_object.action.reg.create.dialog_index=page_object.dialog.init({
                'title':'Регистрация',
                'inner':inner,
                'reg':function(){

                    page_object.action.reg.action.send.init(page_object.action.reg.create.dialog_index);

                },
                'on_create':function(){

                    page_object.action.reg.action.input.init();

                },
                'on_cancel':function(){

                    let  lang_obj       =page_object.content[page_object.lang]
                        ,link           ='/';

                    setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

                }
            });

        }
    },
    'action':{
        'init':{

        },
        'un_show':function(){

            if(!empty(page_object.action.reg.create.dialog_index))
                page_object.dialog.action.un_show.init(page_object.action.reg.create.dialog_index,true);

        },
        'input':{
            'init':function(){

                if(isset($d('dialog_login_input_text')))
                    $d('dialog_login_input_text').onkeyup=page_object.action.reg.action.input.check_login.init;

                if(isset($d('dialog_email_input_text')))
                    $d('dialog_email_input_text').onkeyup=page_object.action.reg.action.input.check_email.init;

                if(isset($d('dialog_password_input_text')))
                    $d('dialog_password_input_text').onkeyup=page_object.action.reg.action.input.check_password.init;

                if(isset($d('dialog_repeat_password_input_text')))
                    $d('dialog_repeat_password_input_text').onkeyup=page_object.action.reg.action.input.check_password.init;

            },
            'check_email':{
                'success':true,
                'timeout':null,
                'init':function(){

                    if(isset(page_object.action.reg.action.input.check_email.timeout))
                        clearTimeout(page_object.action.reg.action.input.check_email.timeout);

                    page_object.action.reg.action.input.check_email.timeout=setTimeout(page_object.action.reg.action.input.check_email.send,500);

                },
                'send':function(){

                    if(isEmail($d('dialog_email_input_text').value)){

                        send({
                            'scriptPath':'/api/json/isset_user_email',
                            'postData':'email='+uniEncode($d('dialog_email_input_text').value.toLowerCase()),
                            'onComplete':function(j,worktime){

                                var  dataTemp
                                    ,data;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    page_object.action.reg.action.input.check_email.success=false;

                                    $s('dialog_email_input_text').borderColor='#e14141';

                                }
                                else if(data['isset_email']){

                                    page_object.action.reg.action.input.check_email.success=false;

                                    $s('dialog_email_input_text').borderColor='#e14141';

                                }
                                else{

                                    page_object.action.reg.action.input.check_email.success=true;

                                    $s('dialog_email_input_text').borderColor='#84cf30';

                                }

                            }
                        });

                        return true;

                    }
                    else{

                        $s('dialog_email_input_text').borderColor='#e14141';

                        page_object.action.reg.action.input.check_email.success=false;

                        return false;

                    }

                }
            },
            'check_login':{
                'success':true,
                'timeout':null,
                'init':function(){

                    if(isset(page_object.action.reg.action.input.check_login.timeout))
                        clearTimeout(page_object.action.reg.action.input.check_login.timeout);

                    page_object.action.reg.action.input.check_login.timeout=setTimeout(page_object.action.reg.action.input.check_login.send,500);

                },
                'send':function(){

                    if($d('dialog_login_input_text').value.length>=3&&$d('dialog_login_input_text').value.length<=32){

                        send({
                            'scriptPath':'/api/json/isset_user_login',
                            'postData':'login='+uniEncode($d('dialog_login_input_text').value.toLowerCase()),
                            'onComplete':function(j,worktime){

                                var  dataTemp
                                    ,data;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    page_object.action.reg.action.input.check_login.success=false;

                                    $s('dialog_login_input_text').borderColor='#e14141';

                                }
                                else if(data['isset_login']){

                                    page_object.action.reg.action.input.check_login.success=false;

                                    $s('dialog_login_input_text').borderColor='#e14141';

                                }
                                else{

                                    page_object.action.reg.action.input.check_login.success=true;

                                    $s('dialog_login_input_text').borderColor='#84cf30';

                                }

                            }
                        });

                    }
                    else{

                        $s('dialog_login_input_text').borderColor='#e14141';

                        page_object.action.reg.action.input.check_login.success=false;

                        return false;

                    }

                }
            },
            'check_password':{
                'success':false,
                'timeout':null,
                'init':function(){

                    let el_ID=this.id;

                    if(isset(page_object.action.reg.action.input.check_password.timeout))
                        clearTimeout(page_object.action.reg.action.input.check_password.timeout);

                    page_object.action.reg.action.input.check_password.timeout=setTimeout(function(){

                        page_object.action.reg.action.input.check_password.check(el_ID);

                    },40);

                },
                'check':function(el_ID){

                    switch(el_ID){

                        case'dialog_password_input_text':{

                            if($d('dialog_password_input_text').value.length>=6&&$d('dialog_password_input_text').value.length<=32){

                                $s('dialog_password_input_text').borderColor='#84cf30';

                                if($d('dialog_repeat_password_input_text').value.length<6){

                                    $s('dialog_repeat_password_input_text').borderColor='#1b93db';

                                    page_object.action.reg.action.input.check_password.success=false;

                                }
                                else if($d('dialog_repeat_password_input_text').value.length>=6&&$d('dialog_repeat_password_input_text').value.length<=32&&$d('dialog_repeat_password_input_text').value==$d('dialog_password_input_text').value){

                                    $s('dialog_repeat_password_input_text').borderColor='#84cf30';

                                    page_object.action.reg.action.input.check_password.success=true;

                                }
                                else{

                                    $s('dialog_repeat_password_input_text').borderColor='#e14141';

                                    page_object.action.reg.action.input.check_password.success=false;

                                }

                            }
                            else if($d('dialog_password_input_text').value.length<6){

                                $s('dialog_password_input_text').borderColor='#1b93db';

                                page_object.action.reg.action.input.check_password.success=false;

                            }
                            else{

                                $s('dialog_password_input_text').borderColor='#e14141';

                                page_object.action.reg.action.input.check_password.success=false;

                            }

                            break;

                        }

                        case'dialog_repeat_password_input_text':{

                            if($d('dialog_repeat_password_input_text').value.length>=6&&$d('dialog_repeat_password_input_text').value.length<=32){

                                if($d('dialog_password_input_text').value.length<6){

                                    $s('dialog_repeat_password_input_text').borderColor='#84cf30';

                                    page_object.action.reg.action.input.check_password.success=false;

                                }
                                else if($d('dialog_password_input_text').value.length>=6&&$d('dialog_password_input_text').value.length<=32&&$d('dialog_repeat_password_input_text').value==$d('dialog_password_input_text').value){

                                    $s('dialog_password_input_text').borderColor            ='#84cf30';
                                    $s('dialog_repeat_password_input_text').borderColor     ='#84cf30';

                                    page_object.action.reg.action.input.check_password.success=true;

                                }
                                else{

                                    $s('dialog_repeat_password_input_text').borderColor='#e14141';

                                    page_object.action.reg.action.input.check_password.success=false;

                                }

                            }
                            else if($d('dialog_repeat_password_input_text').value.length<6){

                                $s('dialog_repeat_password_input_text').borderColor='#1b93db';

                                page_object.action.reg.action.input.check_password.success=false;

                            }
                            else{

                                $s('dialog_repeat_password_input_text').borderColor='#e14141';

                                page_object.action.reg.action.input.check_password.success=false;

                            }

                            break;

                        }

                    }

                }
            }
        },
        'send':{
            'init':function(){

                let is_error=false;

                if(!page_object.action.reg.action.input.check_password.success){

                    is_error=true;

                    $s('dialog_password_input_text').borderColor            ='#ff0000';
                    $s('dialog_repeat_password_input_text').borderColor     ='#ff0000';

                }

                if(!page_object.action.reg.action.input.check_login.success){

                    is_error=true;

                    $s('dialog_login_input_text').borderColor='#ff0000';

                }

                if(!page_object.action.reg.action.input.check_email.success){

                    is_error=true;

                    $s('dialog_email_input_text').borderColor='#ff0000';

                }

                if(!is_error)
                    page_object.action.reg.action.send.submit();

            },
            'submit':function(){

                if(!page_object.action.root.action.root_face_block.action.disclaimer.init())
                    return false;

                let  dialog_index   =page_object.action.reg.create.dialog_index
                    ,lang_obj       =page_object.dialog.content[page_object.lang];

                if(isset($d('dialog_submit_reg_'+dialog_index))){

                    $d('dialog_submit_reg_'+dialog_index).setAttribute('class','dialog_submit_disable');

                    $d('dialog_submit_reg_'+dialog_index).innerHTML    =lang_obj['wait'];
                    $d('dialog_submit_reg_'+dialog_index).onclick      =function(){};

                }

                if(isset($d('dialog_submit_cancel_'+dialog_index))){

                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                }

                re_captcha.check('reg',function(token){

                    var post='';

                    post+='&re_captcha_token='+uniEncode(token);
                    post+='&login='+uniEncode($v('dialog_login_input_text').toLowerCase());
                    post+='&email='+uniEncode($v('dialog_email_input_text').toLowerCase());
                    post+='&password='+uniEncode($v('dialog_password_input_text'));
                    post+='&promo_code='+uniEncode($v('dialog_code_input_text'));

                    send({
                        'scriptPath':'/api/json/reg',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if(isset($d('dialog_login_input_text')))
                                    $s('dialog_login_input_text').borderColor='#ff0000';

                                if(isset($d('dialog_email_input_text')))
                                    $s('dialog_email_input_text').borderColor='#ff0000';

                                if($d('dialog_submit_reg_'+dialog_index)){

                                    $d('dialog_submit_reg_'+dialog_index).onclick=function(){

                                        page_object.action.reg.action.send.init(page_object.action.reg.create.dialog_index);

                                    };
                                    $d('dialog_submit_reg_'+dialog_index).setAttribute('class','dialog_submit_reg');
                                    $d('dialog_submit_reg_'+dialog_index).innerHTML=lang_obj['reg'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                            }
                            else
                                page_object.action.reg.action.send.prepare(dialog_index,data);

                        }
                    });

                });

            },
            'prepare':function(dialog_index,data){

                page_object.dialog.action.un_show.init(dialog_index,true);

                let inner='';

                inner+='<div id="reg_block" class="dialog_row">';

                    inner+='Спасибо за регистрацию. Пожалуйста, подтвердите свой адрес электронной почты, перейдя по ссылке из письма, которое мы Вам отправили';

                inner+='</div>';

                page_object.action.reg.create.dialog_index=page_object.dialog.init({
                    'title':'Регистрация',
                    'inner':inner,
                    'ok':function(){

                        page_object.dialog.action.un_show.init(page_object.action.reg.create.dialog_index,true);

                        let  lang_obj       =page_object.content[page_object.lang]
                            ,link           ='/auth';

                        setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

                    }
                });

            }
        }
    }
};