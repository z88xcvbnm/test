page_object.action.confirm_email={
    'w':300,
    'h':140,
    'x':0,
    'y':0,
    'is_show':false,
    'script_list':{
        'core':{
            'root':'/show/js/root/init.js'
        }
    },
    'style_list':[
        '/show/css/system/root.css',
        '/show/css/system/dialog_block.css'
    ],
    'section_change_workflow':{},
    'reset':function(){

        page_object.action.confirm_email.w=300;
        page_object.action.confirm_email.h=140;
        page_object.action.confirm_email.x=0;
        page_object.action.confirm_email.y=0;

    },
    'init':function(data){

        var temp={
            'script_list':config.updateScriptsList(page_object.action.confirm_email.script_list, 'Root'),
            'style_list':config.updateScriptsList(page_object.action.confirm_email.style_list, 'Root'),
            'init_list':[
                function(){

                    if(typeof config !== 'undefined' && $.isFunction(config.updateSectionChangeWorkflow)) {
                        page_object.action.root.section_change_workflow = config.updateSectionChangeWorkflow(page_object.action.root.section_change_workflow);
                    }

                    page_object.action.root.init(data);

                }
            ]
        };

        page_object.require.init(temp);

    },
    'run':function(data){

        if(!isset($d('confirm_email_block')))
            page_object.action.confirm_email.create.init();

    },
    'create':{
        'dialog_index':null,
        'init':function(){

            page_object.action.confirm_email.create.box();

        },
        'box':function(){ let inner='';

            inner+='<div id="reg_block" class="dialog_row">';

            // if(page_object.action.root.data['data']['success'])
                inner+='<b style="color: #169600;">Ваша регистрация успешно подтверждена. Пожалуйста, авторизуйтесь для входа в систему</b>';
            // else
            //     inner+='<b style="color: #ff0000;">Ваша ссылка не действительна. Попробуйте авторизоваться, возможно, Вы уже подтвердили свой аккаунт. Приносим извинения.</b>';

            inner+='</div>';

            page_object.action.confirm_email.create.dialog_index=page_object.dialog.init({
                // 'title':page_object.action.root.data['data']['success']?'Информация':'Ошибка подтверждения',
                'title':'Информация',
                'inner':inner,
                'ok':function(){

                    page_object.dialog.action.un_show.init(page_object.action.confirm_email.create.dialog_index,true);

                    let  lang_obj       =page_object.content[page_object.lang]
                        ,link           ='/auth';

                    setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

                }
            });

        }
    }
};