page_object.action.admin.section_change_workflow={
    'admin':{
        'init':function(){

            page_object.action.admin.create.header.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.admin.action.header_profile_menu_block.un_show(true);

            if(isset($d('face_content')))
                page_object.action.admin.action.face_content.un_show(true);

            if(isset($d('dashboard_content')))
                page_object.action.admin.action.dashboard_content.un_show(true);

            if(isset($d('users_content')))
                page_object.action.admin.action.users.un_show(true);

            if(isset($d('settings')))
                page_object.action.admin.action.settings.un_show(true);

            if(isset($d('header_menu'))){

                page_object.action.admin.action.header_menu_child_block.un_show(true);
                page_object.action.admin.action.header_menu.set_active();

            }

        }
    },
    'admin_dashboard':{
        'init':function(){

            page_object.action.admin.create.header.init();

            if(isset($d('header_menu')))
                page_object.action.admin.action.header_menu.set_active();
            else
                page_object.action.admin.section_change_workflow.admin.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.admin.action.header_profile_menu_block.un_show(true);

            if(isset($d('dashboard_content')))
                page_object.action.admin.action.dashboard_content.un_show(true);

            if(isset($d('face_content')))
                page_object.action.admin.action.face_content.un_show(true);

            if(isset($d('profile_content')))
                page_object.action.admin.action.profile_content.un_show(true);

            if(isset($d('users_content')))
                page_object.action.admin.action.users.un_show(true);

            if(isset($d('settings')))
                page_object.action.admin.action.settings.un_show(true);

            setTimeout(page_object.action.admin.create.dashboard.init,300);

        }
    },
    'admin_face':{
        'init':function(){

            page_object.action.admin.create.header.init();

            if(isset($d('header_menu')))
                page_object.action.admin.action.header_menu.set_active();
            else
                page_object.action.admin.section_change_workflow.admin.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.admin.action.header_profile_menu_block.un_show(true);

            if(isset($d('dashboard_content')))
                page_object.action.admin.action.dashboard_content.un_show(true);

            if(isset($d('profile_content')))
                page_object.action.admin.action.profile_content.un_show(true);

            if(isset($d('users_content')))
                page_object.action.admin.action.users.un_show(true);

            if(isset($d('settings')))
                page_object.action.admin.action.settings.un_show(true);

            setTimeout(page_object.action.admin.create.face.init,400);

        }
    },
    'admin_users':{
        'init':function(){

            page_object.action.admin.create.header.init();

            if(isset($d('header_menu')))
                page_object.action.admin.action.header_menu.set_active();
            else
                page_object.action.admin.section_change_workflow.admin.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.admin.action.header_profile_menu_block.un_show(true);

            if(isset($d('dashboard_content')))
                page_object.action.admin.action.dashboard_content.un_show(true);

            if(isset($d('face_content')))
                page_object.action.admin.action.face_content.un_show(true);

            if(isset($d('profile_content')))
                page_object.action.admin.action.profile_content.un_show(true);

            if(isset($d('settings')))
                page_object.action.admin.action.settings.un_show(true);

            page_object.action.admin.create.users.init();

        }
    },
    'admin_profile_list':{
        'init':function(){

            page_object.action.admin.create.header.init();

            if(isset($d('header_menu')))
                page_object.action.admin.action.header_menu.set_active();
            else
                page_object.action.admin.section_change_workflow.admin.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.admin.action.header_profile_menu_block.un_show(true);

            if(isset($d('dashboard_content')))
                page_object.action.admin.action.dashboard_content.un_show(true);

            if(isset($d('face_content')))
                page_object.action.admin.action.face_content.un_show(true);

            if(isset($d('users_content')))
                page_object.action.admin.action.users.un_show(true);

            if(isset($d('settings')))
                page_object.action.admin.action.settings.un_show(true);

            page_object.action.admin.create.profile.init();

        }
    },
    'admin_settings':{
        'init':function(){

            page_object.action.admin.create.header.init();

            if(isset($d('header_menu')))
                page_object.action.admin.action.header_menu.set_active();
            else
                page_object.action.admin.section_change_workflow.admin.init();

            if(isset($d('header_profile_menu_block')))
                page_object.action.admin.action.header_profile_menu_block.un_show(true);

            if(isset($d('dashboard_content')))
                page_object.action.admin.action.dashboard_content.un_show(true);

            if(isset($d('face_content')))
                page_object.action.admin.action.face_content.un_show(true);

            if(isset($d('profile_content')))
                page_object.action.admin.action.profile_content.un_show(true);

            if(isset($d('users_content')))
                page_object.action.admin.action.users.un_show(true);

            page_object.action.admin.create.settings.init();

        }
    },
    'admin_logout':{
        'init':function(){

            page_object.action.admin.action.header.un_show(true);
            page_object.action.admin.action.dashboard_content.un_show(true);
            page_object.action.admin.action.face_content.un_show(true);
            page_object.action.admin.action.users.un_show(true);
            page_object.action.admin.action.settings.un_show(true);
            page_object.action.admin.action.header_profile_menu_block.un_show(true);
            page_object.action.admin.action.profile_content.un_show(true);

            page_object.action.admin.data={};

            setTimeout(page_object.action.admin.section_change_workflow.admin_logout.redirect,300);

        },
        'redirect':function(){

            let  lang_obj       =page_object.content[page_object.lang]
                ,link           ='/';

            setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

            if(OS.isMobile)
                document.location.reload();

        }
    }
};
