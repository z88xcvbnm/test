page_object.action.admin={
    'data':{},
    'script_list':{
        'core':{
            'localization':'/show/js/admin/localization.js',
            'elements_positions':'/show/js/admin/elements_positions.js',
            'section_change_workflow':'/show/js/admin/section_change_workflow.js'
        },
        'ui_elements_creation':{
            'header':'/show/js/admin/create/page_object_action_admin_create_header_object.js',
            'admin_menu':'/show/js/admin/create/page_object_action_admin_create_admin_menu_block_object.js',
            'face':'/show/js/admin/create/page_object_action_admin_create_face_object.js',
            'users':'/show/js/admin/create/page_object_action_admin_create_users_content_object.js',
            'profile':'/show/js/admin/create/page_object_action_admin_create_profile_object.js',
            'settings':'/show/js/admin/create/page_object_action_admin_create_settings_object.js',
            'dashboard':'/show/js/admin/create/page_object_action_admin_create_dashboard_object.js'
        },
        'ui_elements_manipulation':{
            'header':'/show/js/admin/action/page_object_action_admin_action_header_object.js',
            'header_menu':'/show/js/admin/action/page_object_action_admin_action_header_menu_object.js',
            'header_profile':'/show/js/admin/action/page_object_action_admin_action_header_profile_object.js',
            'header_profile_menu_block':'/show/js/admin/action/page_object_action_admin_action_header_profile_menu_block_object.js'
        },
        'face':[
            '/show/js/admin/action/page_object_action_admin_action_face_content_object.js',
            '/show/js/admin/action/page_object_action_admin_action_face_item_object.js',
            '/show/js/admin/action/page_object_action_admin_action_face_item_block_object.js',
            '/show/js/admin/action/page_object_action_admin_action_face_item_data_object.js',
        ],
        'dashboard':[
            '/show/js/admin/action/page_object_action_admin_action_dashboard_content_object.js',
        ],
        'users':[
            '/show/js/admin/action/page_object_action_admin_action_users_content_object.js'
        ],
        'profile':[
            '/show/js/admin/action/page_object_action_admin_action_profile_content_object.js',
            '/show/js/admin/action/page_object_action_admin_action_profile_item_object.js',
            '/show/js/admin/action/page_object_action_admin_action_profile_item_block_object.js',
            '/show/js/admin/action/page_object_action_admin_action_profile_item_data_object.js',
        ],
        'page_check_edit':'/show/js/admin/action/page_object_action_admin_action_page_check_edit_object.js',
        'settings':'/show/js/admin/action/page_object_action_admin_action_settings_object.js',
        'maps':'https://api-maps.yandex.ru/2.1/?lang=ru_RU'
    },
    'style_list':[
        '/show/css/system/dialog_block.css',
        '/show/css/admin/style.css',
        '/show/css/admin/style_new.css'
    ],
    'section_change_workflow':{},
    'page_action':null,
    'back_page_action':null,
    'content':{},
    'position':{},
    'init':function(data){

        page_object.require.init({
            'script_list':config.updateScriptsList(page_object.action.admin.script_list, 'Admin'),
            'style_list':config.updateScriptsList(page_object.action.admin.style_list, 'Admin'),
            'init_list':[
                function(){
                    if (typeof config !== 'undefined' && $.isFunction(config.updateSectionChangeWorkflow)) {
                        page_object.action.admin.section_change_workflow = config.updateSectionChangeWorkflow(page_object.action.admin.section_change_workflow);
                    }
                    page_object.action.admin.run(data);
                }
            ]
        });

    },
    'run':function(data){

        if(
              page_object.link.link_list[(page_object.link.link_list.length-1)]!==page_object.link.back_link_list[(page_object.link.back_link_list.length-1)]
            ||page_object.link.link_list[(page_object.link.link_list.length-2)]!==page_object.link.back_link_list[(page_object.link.back_link_list.length-2)]
            ||page_object.link.link_list[(page_object.link.link_list.length-3)]!==page_object.link.back_link_list[(page_object.link.back_link_list.length-3)]
        ){

            page_object.action.admin.data                   =data;
            page_object.action.admin.back_page_action       =page_object.action.admin.page_action;
            page_object.action.admin.page_action            =data['action'];

            //if(isset($d('dialog_block')))
            //    page_object.dialog.action.un_show.init(dialog_index,true);

            if(isset(page_object.action.admin.section_change_workflow[data['action']])){

                page_object.action.admin.section_change_workflow[data['action']].init();

                setTimeout(page_object.action.admin.action.all.resize,1200);

            }
            else
                trace('==> TRIG ACTION IS NOT EXISTS');

        }
        else
            trace('==> ACTION ALREADY ACTIVE');

    },
    'prepend_localization_content':function (data) {
        page_object.action.admin.content = $.extendext(true, 'concat', data, page_object.action.admin.content);
    },
    'append_localization_content':function (data) {
        page_object.action.admin.content = $.extendext(true, 'concat', page_object.action.admin.content, data);
    },
    'prepend_element_positions_settings':function (data) {
        page_object.action.admin.position = $.extendext(true, 'concat', data, page_object.action.admin.position);
    },
    'append_element_positions_settings':function (data) {
        page_object.action.admin.position = $.extendext(true, 'concat', page_object.action.admin.position, data);
    },
    'is_root':function(){

        return page_object.action.admin.data['is_root'];

    },
    'is_admin':function(){

        return page_object.action.admin.data['is_root']
            ||page_object.action.admin.data['is_admin'];

    },
    'is_manager':function(){

        return page_object.action.admin.data['is_root']
            ||page_object.action.admin.data['is_admin']
            ||page_object.action.admin.data['is_manager'];

    },
    'is_bookkeep':function(){

        return page_object.action.admin.data['is_bookkeep'];

    },
    'is_museum':function(){

        return page_object.action.admin.data['is_museum'];

    },
    'create':{},
    'action':{
        'all':{
            'resize':function(){

                if(isset($d('all'))){

                    if(isset(page_object.action.admin)&&isset(page_object.action.admin.position)){

                        page_object.action.admin.position.all.w=winSize.winWidth;

                        if(page_object.link.link_list[2]==='users'){

                            if(page_object.action.admin.data['action']==='admin_users'){

                                page_object.action.admin.position.all.h=page_object.action.admin.position.header.h
                                    +page_object.action.admin.position.header_menu_child_block.h
                                    +page_object.action.admin.position.users_list.m.t
                                    +page_object.action.admin.position.users_list.h;

                            }

                        }
                        else{

                            page_object.action.admin.position.all.h=winSize.winHeight;

                        }

                        // $s('all').wight     =page_object.action.admin.position.all.w+'px';
                        // $s('all').height    =page_object.action.admin.position.all.h+'px';

                        if(isset($d('bg_load'))){

                            //$s('bg_load').transform     ='translate(0,'+$b().scrollTop+'px)';
                            // $s('bg_load').width         =winSize.winWidth+'px';
                            // $s('bg_load').height        =winSize.winHeight+'px';

                        }

                    }

                }

            }
        },
        'scroll':{
            'animate':function(x,y,x_max,y_max,step){

                let  delta_x        =Math.floor(Math.abs(x-x_max)/step)
                    ,delta_y        =Math.floor(Math.abs(y-y_max)/step);

                delta_x=(delta_x<3)?3:delta_x;
                delta_y=(delta_y<3)?3:delta_y;

                if(x>=x_max){

                    x-=delta_x;

                    if(x<x_max)
                        x=x_max;

                }
                else{
                    
                    x+=delta_x;
                    
                    if(x>x_max)
                        x=x_max;
                    
                }
                
                $b().scrollLeft=x;

                if(y>=y_max){

                    y-=delta_y;

                    if(y<y_max)
                        y=y_max;

                }
                else{
                    
                    y+=delta_y;
                    
                    if(y>y_max)
                        y=y_max;
                    
                }
                
                $b().scrollTop=y;
                
                if(x===x_max&&y===y_max)
                    return false;
                    
                setTimeout(function(){

                    page_object.action.admin.action.scroll.animate(x,y,x_max,y_max,step);

                },40);

                return true;

            },
            'coords':function(x,y){

                let  doc_w          =$b().scrollWidth-winSize.winWidth
                    ,doc_h          =$b().scrollHeight-winSize.winHeight
                    ,doc_x          =$b().scrollLeft
                    ,doc_y          =$b().scrollTop;

                x       =(x>doc_w)?doc_w:x;
                y       =(y>doc_h)?doc_h:y;

                page_object.action.admin.action.scroll.animate(doc_x,doc_y,x,y,3);

            },
            'element':function(el){

                let  pos        =getElementPosition(el)
                    ,pos_x      =pos['left']
                    ,pos_y      =pos['top'];

                page_object.action.admin.action.scroll.coords(pos_x,pos_y);

            }
        },
        'resize':function(){

            if(isset(page_object.action.admin.action.header))
                page_object.action.admin.action.header.resize();

            if(isset(page_object.action.admin.action.header_profile_menu_block))
                page_object.action.admin.action.header_profile_menu_block.resize();

            if(isset(page_object.action.admin.action.users))
                page_object.action.admin.action.users.resize();

            if(isset(page_object.action.admin.action.settings))
                page_object.action.admin.action.settings.resize();

            if(isset(page_object.action.admin.action.dashboard))
                page_object.action.admin.action.dashboard_content.resize();

            if(isset(page_object.action.admin.action.all))
                page_object.action.admin.action.all.resize();

        }
    }
};