page_object.action.admin.action.header={
    'show':function(){

        if(isset($d('header'))){

            page_object.action.admin.position.header.o=1;
            page_object.action.admin.position.header.y=0;

            $s('header').opacity        =page_object.action.admin.position.header.o;
            $s('header').transform      ='translate('+page_object.action.admin.position.header.x+'px,'+page_object.action.admin.position.header.y+'px)'

        }

        if(isset($d('footer')))
            $s('footer').opacity=1;

    },
    'un_show':function(remove){

        if(isset($d('header'))){

            page_object.action.admin.position.header.o=0;
            page_object.action.admin.position.header.y=-page_object.action.admin.position.header.h;

            $s('header').opacity        =page_object.action.admin.position.header.o;
            $s('header').transform      ='translate('+page_object.action.admin.position.header.x+'px,'+page_object.action.admin.position.header.y+'px)'

        }

        if(isset($d('footer')))
            $s('footer').opacity=0;

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.admin.action.header.remove,300);

    },
    'logo':{
        'click':function(){

            let  lang_obj       =page_object.content[page_object.lang]
                ,link           ='/admin/'+page_object.action.admin.data['user']['login']+'/dashboard'
                ,func;

            func=function(){

                page_object.action.admin.action.header_menu.set_active();

                setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

            };

            page_object.action.admin.action.page_check_edit.init(func);

        }
    },
    'remove':function(){

        if(isset($d('header')))
            removeElement($d('header'));

        if(isset($d('footer')))
            removeElement($d('footer'));

    },
    'resize':function(){

        if(isset($d('header'))){

            // page_object.action.admin.position.header.w=winSize.winWidth;
            //
            // $s('header').width=page_object.action.admin.position.header.w+'px';

        }

    }
};