page_object.action.admin.action.profile_content={
    'show':function(){

        if(isset($d('profile_content'))){

            page_object.action.admin.position.profile_content.o       =1;
            page_object.action.admin.position.profile_content.y       =page_object.action.admin.position.header.h;

            $s('profile_content').opacity     =page_object.action.admin.position.profile_content.o;
            $s('profile_content').transform   ='translate('+page_object.action.admin.position.profile_content.x+'px,'+page_object.action.admin.position.profile_content.y+'px)';

        }

    },
    'un_show':function(remove){

        if(isset($d('profile_content'))){

            page_object.action.admin.position.profile_content.o       =0;
            page_object.action.admin.position.profile_content.y       =0;
            page_object.action.admin.position.profile_content.h       =0;

            $s('profile_content').opacity     =page_object.action.admin.position.profile_content.o;
            $s('profile_content').height      =page_object.action.admin.position.profile_content.h='px';
            $s('profile_content').transform   ='translate('+page_object.action.admin.position.profile_content.x+'px,'+page_object.action.admin.position.profile_content.y+'px)';

            if(isset(remove))
                if(remove)
                    setTimeout(page_object.action.admin.action.profile_content.remove,300);

        }

    },
    'remove':function(){

        if(isset($d('profile_content')))
            removeElement($d('profile_content'))

    },
    'resize':function(){

    }
};