page_object.action.admin.action.face_item={
    'show':function(){

        if(isset($d('face_item'))){

            page_object.action.admin.position.face_item.o=1;

            $s('face_item').opacity=page_object.action.admin.position.face_item.o;

            page_object.action.admin.action.face_item.resize();
            page_object.action.admin.action.face_content.resize();

        }

        setTimeout(page_object.link.preload.un_show,200);

    },
    'un_show':function(remove){

        if(isset($d('face_item'))){

            page_object.action.admin.position.face_item.o=0;

            $s('face_item').opacity=page_object.action.admin.position.face_item.o;

            if(isset(remove))
                if(remove)
                    setTimeout(page_object.action.admin.action.face_item.remove,300);

        }

    },
    'action':{
        'init':function(){}
    },
    'resize':function(){}
};