page_object.action.admin.action.dashboard_content={
    'show':function(){

        if(isset($d('dashboard_content'))){

            page_object.action.admin.position.dashboard_content.o       =1;
            page_object.action.admin.position.dashboard_content.y       =page_object.action.admin.position.header.h;
            page_object.action.admin.position.dashboard_content.h       =winSize.winHeight-page_object.action.admin.position.header.h;

            $s('dashboard_content').opacity     =page_object.action.admin.position.dashboard_content.o;
            $s('dashboard_content').minHeight   =page_object.action.admin.position.dashboard_content.h+'px';
            $s('dashboard_content').transform   ='translate('+page_object.action.admin.position.dashboard_content.x+'px,'+page_object.action.admin.position.dashboard_content.y+'px)';

        }

        setTimeout(page_object.link.preload.un_show,300);

    },
    'un_show':function(remove){

        if(isset($d('dashboard_content'))){

            page_object.action.admin.position.dashboard_content.o       =0;
            page_object.action.admin.position.dashboard_content.y       =0;
            page_object.action.admin.position.dashboard_content.h       =0;

            $s('dashboard_content').opacity     =page_object.action.admin.position.dashboard_content.o;
            $s('dashboard_content').height      =page_object.action.admin.position.dashboard_content.h='px';
            $s('dashboard_content').transform   ='translate('+page_object.action.admin.position.dashboard_content.x+'px,'+page_object.action.admin.position.dashboard_content.y+'px)';

            if(isset(remove))
                if(remove)
                    setTimeout(page_object.action.admin.action.dashboard_content.remove,300);

        }

    },
    'action':{
        'init':function(){

            page_object.action.admin.action.dashboard_content.action.user_reg_stats.init();
            page_object.action.admin.action.dashboard_content.action.user_parsing_stats.init();
            page_object.action.admin.action.dashboard_content.action.user_city_stats.init();

        },
        'user_reg_stats':{
            'init':function(){

                if(isset($d('user_reg_stats')))
                    $d('user_reg_stats').onclick=page_object.action.admin.action.dashboard_content.action.user_reg_stats.show;

            },
            'show':function(){

                let  inner      =''
                    ,list       =page_object.action.admin.data['dashboard']['user_stats']['reg']
                    ,index;

                 inner+='<div class="dialog_row" style="max-height: calc(100vh - 200px); overflow-y: auto;">';

                 inner+='<div id="dashboard_user_reg_stats_header">';

                    inner+='<div class="user_reg_stats_header_col">Дата</div>';
                    inner+='<div class="user_reg_stats_header_col">Всего</div>';
                    inner+='<div class="user_reg_stats_header_col">Подтвержденные</div>';
                    inner+='<div class="user_reg_stats_header_col">Пополнения, шт</div>';
                    inner+='<div class="user_reg_stats_header_col">Поступления, р</div>';
                    inner+='<div class="user_reg_stats_header_col">Прирост анкет</div>';

                inner+='</div>';

                for(index in list){

                    inner+='<div class="dialog_user_reg_stats_item_row">';

                        inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(index)+'</td></tr></table></div>';
                        inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['all'])+'</td></tr></table></div>';
                        inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['confirmed_email'])+(empty(list[index]['all'])?'':(' <i style="font-size: 12px;">('+((list[index]['confirmed_email']/list[index]['all']*100).toFixed(2))+'%)</i>'))+'</td></tr></table></div>';

                        inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['paid_len'])+'</td></tr></table></div>';
                        inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['paid'])+'</td></tr></table></div>';
                        inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['face_len'])+'</td></tr></table></div>';

                    inner+='</div>';

                }

                inner+='</div>';

                page_object.dialog.init({
                    'title':'Статистика регистраций',
                    'w':800,
                    'inner':inner,
                    'cancel':true
                });

            }
        },
        'user_parsing_stats':{
            'init':function(){

                if(isset($d('user_parsing_stats')))
                    $d('user_parsing_stats').onclick=page_object.action.admin.action.dashboard_content.action.user_parsing_stats.show;

            },
            'show':function(){

                let  inner          =''
                    ,list           =page_object.action.admin.data['dashboard']['user_stats']['parsing']
                    ,source_list    =page_object.action.admin.data['dashboard']['source_list']
                    ,index
                    ,source_index   =1;

                 inner+='<div class="dialog_row" style="max-height: calc(100vh - 200px); overflow-y: auto;">';

                 inner+='<div id="dashboard_user_reg_stats_header">';

                    inner+='<div class="user_reg_stats_header_col" style="width: 50px;">#</div>';
                    inner+='<div class="user_reg_stats_header_col" style="width: 200px;">Ресурс</div>';
                    inner+='<div class="user_reg_stats_header_col">Всего, шт</div>';
                    inner+='<div class="user_reg_stats_header_col">Сегодня, шт</div>';
                    inner+='<div class="user_reg_stats_header_col">Скорость, шт/час</div>';

                inner+='</div>';

                for(index in list){

                    inner+='<div class="dialog_user_reg_stats_item_row">';

                        inner+='<div class="user_reg_stats_item_col" style="width: 50px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(source_index)+'</td></tr></table></div>';
                        inner+='<div class="user_reg_stats_item_col" style="width: 200px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(source_list[index])+'</td></tr></table></div>';
                        inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['all_len'])+'</td></tr></table></div>';
                        inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['day_len'])+'</td></tr></table></div>';
                        inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['hour_len'])+'</td></tr></table></div>';

                    inner+='</div>';

                    source_index++;

                }

                for(index in source_list)
                    if(!isset(list[index])){

                        inner+='<div class="dialog_user_reg_stats_item_row">';

                            inner+='<div class="user_reg_stats_item_col" style="width: 50px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(source_index)+'</td></tr></table></div>';
                            inner+='<div class="user_reg_stats_item_col" style="width: 200px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(source_list[index])+'</td></tr></table></div>';
                            inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">0</td></tr></table></div>';
                            inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">0</td></tr></table></div>';
                            inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">0</td></tr></table></div>';

                        inner+='</div>';

                        source_index++;

                    }

                inner+='</div>';

                page_object.dialog.init({
                    'title':'Статистика парсинга',
                    'w':800,
                    'inner':inner,
                    'cancel':true
                });

            }
        },
        'user_city_stats':{
            'init':function(){

                if(isset($d('user_city_stats')))
                    $d('user_city_stats').onclick=page_object.action.admin.action.dashboard_content.action.user_city_stats.show;

            },
            'show':function(){

                let  inner          =''
                    ,list           =page_object.action.admin.data['dashboard']['user_stats']['city']
                    ,index;

                 inner+='<div class="dialog_row" style="max-height: calc(100vh - 200px); overflow-y: auto;">';

                 inner+='<div id="dashboard_user_reg_stats_header">';

                    inner+='<div class="user_reg_stats_header_col" style="width: 50px;">#</div>';
                    inner+='<div class="user_reg_stats_header_col" style="width: 200px;">Город</div>';
                    inner+='<div class="user_reg_stats_header_col">Анкет, шт</div>';

                inner+='</div>';

                for(index in list){

                    inner+='<div class="dialog_user_reg_stats_item_row">';

                        inner+='<div class="user_reg_stats_item_col" style="width: 50px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(parseInt(index)+1)+'</td></tr></table></div>';
                        inner+='<div class="user_reg_stats_item_col" style="width: 200px;"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['name'])+'</td></tr></table></div>';
                        inner+='<div class="user_reg_stats_item_col"><table cellpadding="0" cellspacing="0" width="100%"><tr><td height="30" valign="middle" align="center">'+stripSlashes(list[index]['len'])+'</td></tr></table></div>';

                    inner+='</div>';

                }

                inner+='</div>';

                page_object.dialog.init({
                    'title':'Статистика городов',
                    'w':450,
                    'inner':inner,
                    'cancel':true
                });

            }
        }
    },
    'remove':function(){

        if(isset($d('dashboard_content')))
            removeElement($d('dashboard_content'))

    },
    'resize':function(){

        if(isset($d('dashboard_content'))){

            let h=winSize.winHeight-page_object.action.admin.position.header.h-page_object.action.admin.position.header_menu_child_block.h

            page_object.action.admin.position.dashboard_content.w       =winSize.winWidth;
            page_object.action.admin.position.dashboard_content.h       =h;

            $s('dashboard_content').width   =page_object.action.admin.position.dashboard_content.w+'px';
            $s('dashboard_content').height  =page_object.action.admin.position.dashboard_content.h+'px';

        }

    }
};