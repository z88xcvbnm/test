page_object.action.admin.action.header_profile_menu_block={
    'show':function(){

        if(isset($d('header_profile_menu_block'))){

            page_object.action.admin.position.admin_menu_block.o        =1;
            page_object.action.admin.position.admin_menu_block.h        =$d('header_profile_menu_block').scrollHeight;

            $s('header_profile_menu_block').opacity     =page_object.action.admin.position.admin_menu_block.o;
            $s('header_profile_menu_block').height      =page_object.action.admin.position.admin_menu_block.h+'px';

        }

    },
    'un_show':function(remove){

        if(isset($d('header_profile_menu_block'))){

            page_object.action.admin.position.admin_menu_block.o        =1;
            page_object.action.admin.position.admin_menu_block.h        =0;

            $s('header_profile_menu_block').opacity     =page_object.action.admin.position.admin_menu_block.o;
            $s('header_profile_menu_block').height      =page_object.action.admin.position.admin_menu_block.h+'px';

        }

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.admin.action.header_profile_menu_block.remove,300);


    },
    'remove':function(){

        if(isset($d('header_profile_menu_block')))
            removeElement($d('header_profile_menu_block'));

    },
    'mouse':{
        'is_over_menu':false,
        'is_over_profile':false,
        'timeout':null,
        'init':function(){

            if(isset($d('header_profile_menu_block'))){

                $d('header_profile_menu_block').onmouseover     =page_object.action.admin.action.header_profile_menu_block.mouse.over;
                $d('header_profile_menu_block').onmouseout      =page_object.action.admin.action.header_profile_menu_block.mouse.out;

            }

            if(isset($d('header_profile'))){

                $d('header_profile').onmouseover        =page_object.action.admin.action.header_profile_menu_block.mouse.over;
                $d('header_profile').onmouseout         =page_object.action.admin.action.header_profile_menu_block.mouse.out;

            }

        },
        'over':function(){

            switch(this.id){

                case'header_profile_menu_block':{

                    page_object.action.admin.action.header_profile_menu_block.mouse.is_over_profile=true;

                    break;

                }

                case'header_profile':{

                    page_object.action.admin.action.header_profile_menu_block.mouse.is_over_profile=true;

                    break;

                }

            }

        },
        'out':function(){

            switch(this.id){

                case'header_profile_menu_block':{

                    page_object.action.admin.action.header_profile_menu_block.mouse.is_over_profile=false;

                    break;

                }

                case'header_profile':{

                    page_object.action.admin.action.header_profile_menu_block.mouse.is_over_profile=false;

                    break;

                }

            }

            if(!page_object.action.admin.action.header_profile_menu_block.mouse.is_over_profile&&!page_object.action.admin.action.header_profile_menu_block.mouse.is_over_profile){

                if(isset(page_object.action.admin.action.header_profile_menu_block.mouse.timeout))
                    clearTimeout(page_object.action.admin.action.header_profile_menu_block.mouse.timeout);

                page_object.action.admin.action.header_profile_menu_block.mouse.timeout=setTimeout(page_object.action.admin.action.header_profile_menu_block.mouse.un_show,1000);

            }

        },
        'un_show':function(){

            if(!page_object.action.admin.action.header_profile_menu_block.mouse.is_over_profile&&!page_object.action.admin.action.header_profile_menu_block.mouse.is_over_profile)
                page_object.action.admin.action.header_profile_menu_block.un_show(true);

        }
    },
    'click':{
        'init':function(){

            var key=this.id.split('_')[3];

            switch(key){

                case'settings':{

                    page_object.action.admin.action.header_profile_menu_block.click.settings();

                    break;

                }

                case'logout':{

                    page_object.action.admin.action.header_profile_menu_block.click.logout();

                    break;

                }

            }

        },
        'settings':function(){

            var  lang_obj       =page_object.content[page_object.lang]
                ,link           ='/admin/'+page_object.action.admin.data['user']['login']+'/settings'
                ,func;


            func=function(){

                setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

            };

            page_object.action.admin.action.page_check_edit.init(func);

        },
        'logout':function(){

            var  lang_obj       =page_object.content[page_object.lang]
                ,link           ='/admin/'+page_object.action.admin.data['user']['login']+'/logout'
                ,func;

            func=function(){

                setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

            };

            page_object.action.admin.action.page_check_edit.init(func);

        }
    },
    'resize':function(){}
};