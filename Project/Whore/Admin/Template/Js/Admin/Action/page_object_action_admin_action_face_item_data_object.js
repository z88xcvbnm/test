page_object.action.admin.action.face_item_data={
    'show':function(){

        page_object.action.admin.action.face_item_block.show();

    },
    'un_show':function(remove){

        page_object.action.admin.action.face_item_block.un_show(remove);

    },
    'action':{
        'init':function(){

            page_object.action.admin.action.face_item_data.action.filter.init();
            page_object.action.admin.action.face_item_data.action.face_none.init();
            page_object.action.admin.action.face_item_data.action.add_face.init();
            page_object.action.admin.action.face_item_data.action.edit_face.init();
            page_object.action.admin.action.face_item_data.action.edit_photo.init();
            page_object.action.admin.action.face_item_data.action.edit_video.init();
            page_object.action.admin.action.face_item_data.action.remove_face.init();
            page_object.action.admin.action.face_item_data.action.scroll.init();
            
        },
        'filter':{
            'is_open':false,
            'is_edit':false,
            'timeout':300,
            'init':function(){

                if(isset($d('face_filter_list'))){

                    let el_h=elementSize.height($d('face_filter_list'));

                    page_object.action.admin.action.face_item_data.action.filter.is_open=(el_h>40);

                }

                // page_object.action.admin.action.face_item_data.action.filter.is_edit=false;

                if(isset($d('face_filter')))
                    $d('face_filter').onclick=page_object.action.admin.action.face_item_data.action.filter.show;

                page_object.action.admin.action.face_item_data.action.filter.face_ID.init();
                page_object.action.admin.action.face_item_data.action.filter.check_box.init();
                page_object.action.admin.action.face_item_data.action.filter.city.init();
                page_object.action.admin.action.face_item_data.action.filter.link.init();
                page_object.action.admin.action.face_item_data.action.filter.source.init();
                page_object.action.admin.action.face_item_data.action.filter.level.init();
                page_object.action.admin.action.face_item_data.action.filter.sort.init();

            },
            'show':function(){

                if(page_object.action.admin.action.face_item_data.action.filter.is_open){

                    $s('face_filter_list').height           =0;
                    $s('face_filter_list').opacity          =0;
                    $s('face_filter_list').paddingTop       =0;
                    $s('face_filter_list').paddingBottom    =0;
                    $s('face_filter_list').marginTop        =0;

                    if(isset($d('face_filter_source')))
                        $d('face_filter_source').value=0;

                    if(isset($d('face_filter_image_sort_count')))
                        $d('face_filter_image_sort_count').value=0;

                    if(isset($d('face_filter_level')))
                        $d('face_filter_level').value=0;

                    if(isset($d('face_filter_link')))
                        $d('face_filter_link').value='';

                    if(isset($d('face_filter_face_ID')))
                        $d('face_filter_face_ID').value='';

                    if(isset($d('face_filter_city')))
                        $d('face_filter_city').value='';

                    if(isset($d('face_filter_check_fpp')))
                        $d('face_filter_check_fpp').setAttribute('class','face_filter_check');

                    if(isset($d('face_filter_check_face_count')))
                        $d('face_filter_check_face_count').setAttribute('class','face_filter_check');

                    trace('IS EDIT: '+page_object.action.admin.action.face_item_data.action.filter.is_edit);

                    if(page_object.action.admin.action.face_item_data.action.filter.is_edit)
                        setTimeout(page_object.action.admin.action.face_item_data.action.filter.send,400);

                    page_object.action.admin.action.face_item_data.action.filter.is_edit=false;

                }
                else{

                    page_object.action.admin.action.face_item_data.action.filter.is_edit=false;

                    $s('face_filter_list').height           ='auto';
                    $s('face_filter_list').opacity          =1;
                    $s('face_filter_list').paddingTop       ='15px';
                    $s('face_filter_list').paddingBottom    ='20px';
                    $s('face_filter_list').marginTop        ='15px';

                }

                page_object.action.admin.action.face_item_data.action.filter.is_open=!page_object.action.admin.action.face_item_data.action.filter.is_open;

            },
            'source':{
                'init':function(){

                    if(isset($d('face_filter_source')))
                        $d('face_filter_source').onchange=page_object.action.admin.action.face_item_data.action.filter.source.change;

                },
                'change':function(){

                    page_object.action.admin.action.face_item_data.action.filter.is_edit=true;

                    page_object.action.admin.action.face_item_data.action.filter.send();

                }
            },
            'level':{
                'init':function(){

                    if(isset($d('face_filter_level')))
                        $d('face_filter_level').onchange=page_object.action.admin.action.face_item_data.action.filter.level.change;

                },
                'change':function(){

                    page_object.action.admin.action.face_item_data.action.filter.is_edit=true;

                    page_object.action.admin.action.face_item_data.action.filter.send();

                }
            },
            'sort':{
                'init':function(){

                    if(isset($d('face_filter_image_sort_count')))
                        $d('face_filter_image_sort_count').onchange=page_object.action.admin.action.face_item_data.action.filter.sort.change;

                },
                'change':function(){

                    page_object.action.admin.action.face_item_data.action.filter.is_edit=true;

                    page_object.action.admin.action.face_item_data.action.filter.send();

                }
            },
            'check_box':{
                'init':function(){

                    if(isset($d('face_filter_check_fpp')))
                        $d('face_filter_check_fpp').onclick=page_object.action.admin.action.face_item_data.action.filter.check_box.check;

                    if(isset($d('face_filter_check_face_count')))
                        $d('face_filter_check_face_count').onclick=page_object.action.admin.action.face_item_data.action.filter.check_box.check;

                    if(isset($d('face_filter_check_not_city')))
                        $d('face_filter_check_not_city').onclick=page_object.action.admin.action.face_item_data.action.filter.check_box.check;

                    if(isset($d('face_filter_check_not_age')))
                        $d('face_filter_check_not_age').onclick=page_object.action.admin.action.face_item_data.action.filter.check_box.check;

                },
                'check':function(){

                    page_object.action.admin.action.face_item_data.action.filter.is_edit=true;

                    if(this.getAttribute('class')==='face_filter_check')
                        this.setAttribute('class','face_filter_check_active');
                    else
                        this.setAttribute('class','face_filter_check');

                    page_object.action.admin.action.face_item_data.action.filter.send();

                }
            },
            'face_ID':{
                'timeout_event':null,
                'init':function(){

                    if(!isset(page_object.action.admin.action.face_item_data.action.filter.face_ID.timeout_event))
                        clearTimeout(page_object.action.admin.action.face_item_data.action.filter.face_ID.timeout_event);

                    page_object.action.admin.action.face_item_data.action.filter.face_ID.timeout_event=null;

                    if(isset($d('face_filter_face_ID')))
                        $d('face_filter_face_ID').onkeyup=page_object.action.admin.action.face_item_data.action.filter.face_ID.keyup;

                },
                'keyup':function(){

                    if(!isset(page_object.action.admin.action.face_item_data.action.filter.face_ID.timeout_event))
                        clearTimeout(page_object.action.admin.action.face_item_data.action.filter.face_ID.timeout_event);

                    page_object.action.admin.action.face_item_data.action.filter.face_ID.timeout_event=setTimeout(function(){

                        page_object.action.admin.action.face_item_data.action.filter.is_edit=true;

                        page_object.action.admin.action.face_item_data.action.filter.send();

                    },page_object.action.admin.action.face_item_data.action.filter.timeout);

                }
            },
            'link':{
                'timeout_event':null,
                'init':function(){

                    if(!isset(page_object.action.admin.action.face_item_data.action.filter.link.timeout_event))
                        clearTimeout(page_object.action.admin.action.face_item_data.action.filter.link.timeout_event);

                    page_object.action.admin.action.face_item_data.action.filter.link.timeout_event=null;

                    if(isset($d('face_filter_link')))
                        $d('face_filter_link').onkeyup=page_object.action.admin.action.face_item_data.action.filter.link.keyup;

                },
                'keyup':function(){

                    if(!isset(page_object.action.admin.action.face_item_data.action.filter.link.timeout_event))
                        clearTimeout(page_object.action.admin.action.face_item_data.action.filter.link.timeout_event);

                    page_object.action.admin.action.face_item_data.action.filter.link.timeout_event=setTimeout(function(){

                        page_object.action.admin.action.face_item_data.action.filter.is_edit=true;

                        page_object.action.admin.action.face_item_data.action.filter.send();

                    },page_object.action.admin.action.face_item_data.action.filter.timeout);

                }
            },
            'city':{
                'timeout_event':null,
                'init':function(){

                    ymaps.ready(function(){

                        ymaps.modules.require('SuggestView',function(SuggestView){

                            var suggestView=new SuggestView('face_filter_city',{container:all});

                            suggestView.events.add('select',function(event){

                                page_object.action.admin.action.face_item_data.action.filter.is_edit=true;

                                page_object.action.admin.action.face_item_data.action.filter.send();
                                // page_object.action.admin.action.place_item_info.click.yandex.geocoder.init('dialog_city_input_text');

                            });

                        });

                    });

                }
            },
            'send':function(){

                let post='';

                if(!empty($v('face_filter_source')))
                    post+='&source_ID='+uniEncode($v('face_filter_source'));

                if(!empty($v('face_filter_level')))
                    post+='&level='+uniEncode($v('face_filter_level'));

                if(!empty($v('face_filter_face_ID')))
                    post+='&face_ID='+uniEncode($v('face_filter_face_ID'));

                if(!empty($v('face_filter_city'))){

                    let  city_list  =$v('face_filter_city').split(',')
                        ,city_len   =city_list.length;

                    if(city_len>0)
                        post+='&city='+uniEncode(city_list[city_len-1].trim());

                }

                if(!empty($v('face_filter_link')))
                    post+='&source_link='+uniEncode($v('face_filter_link'));

                if(!empty($v('face_filter_image_sort_count'))){

                    post+='&sort_by=image_count';
                    post+='&sort_direction='+uniEncode(parseInt($v('face_filter_image_sort_count'))===1?'asc':'desc');

                }

                if($d('face_filter_check_fpp').getAttribute('class')==='face_filter_check_active')
                    post+='&is_uploaded_image=1';

                if($d('face_filter_check_face_count').getAttribute('class')==='face_filter_check_active')
                    post+='&is_not_one_face=1';

                if($d('face_filter_check_not_city').getAttribute('class')==='face_filter_check_active')
                    post+='&is_not_city=1';

                if($d('face_filter_check_not_age').getAttribute('class')==='face_filter_check_active')
                    post+='&is_not_age=1';

                send({
                    'scriptPath':'/api/json/get_face_list',
                    'postData':post,
                    'onComplete':function(j,worktime){

                        let  dataTemp
                            ,data;

                        dataTemp=j.responseText;
                        trace(dataTemp);
                        data=jsonDecode(dataTemp);
                        trace(data);
                        trace_worktime(worktime,data);

                        if(isset(data['error'])){

                        }
                        else{

                             page_object.action.admin.action.face_item_data.action.filter.prepare(data);

                        }

                    }
                });

            },
            'prepare':function(data){

                page_object.action.admin.data['face_data']['face_list']=data['data']['face_list'];

                if(isset($d('face_block'))){

                    $s('face_block').opacity=0;

                    setTimeout(function(){

                        $d('face_list').innerHTML='';

                        if(page_object.action.admin.data['face_data']['face_list'].length===0){

                            page_object.action.admin.action.face_item_data.action.face_none.init();

                            return true;

                        }

                        let  index
                            ,el
                            ,inner;

                        for(index=0;index<page_object.action.admin.data['face_data']['face_list'].length;index++){

                            inner=page_object.action.admin.create.face.create.get_face_item_data_row(index);

                            el=addElement({
                                'tag':'div',
                                'id':'face_item_'+index,
                                'class':'face_item',
                                'inner':inner
                            });

                            $d('face_list').appendChild(el);

                        }

                        page_object.action.admin.action.face_item_data.action.init();

                        return true;

                    },300);

                }

            }
        },
        'face_none':{
            'init':function(){

                let face_list=page_object.action.admin.data['face_data']['face_list'];

                trace(face_list.length);

                if(face_list.length===0){

                    trace('ok');

                    $s('face_none').opacity     =.4;
                    $s('face_block').opacity    =0;

                }
                else{

                    $s('face_none').opacity     =0;
                    $s('face_block').opacity    =1;

                }

            }
        },
        'rename_face':{
            'move_down':function(){

                let  list           =page_object.action.admin.data['face_data']['face_list']
                    ,list_len       =list.length
                    ,index_from
                    ,index_to;

                for(index_to=list_len;index_to>0;index_to--){

                    index_from=parseInt(index_to)-1;

                    page_object.action.admin.action.face_item_data.action.rename_face.rename_index(index_from,index_to);

                }

            },
            'move_up':function(index){

                let  list           =page_object.action.admin.data['face_data']['face_list']
                    ,list_len       =list.length
                    ,index_from
                    ,index_to;

                for(index_to=index;index_to<list_len;index_to++){

                    index_from=parseInt(index_to)+1;

                    page_object.action.admin.action.face_item_data.action.rename_face.rename_index(index_from,index_to);

                }

            },
            'rename_index':function(index_from,index_to){

                if(isset($d('face_item_'+index_from))){

                    $d('face_item_'+index_from).setAttribute('id','face_item_'+index_to);

                    $d('face_item_number_'+index_from).setAttribute('id','face_item_number_'+index_to);
                    $d('face_item_image_'+index_from).setAttribute('id','face_item_image_'+index_to);
                    $d('face_item_image_block_'+index_from).setAttribute('id','face_item_image_block_'+index_to);
                    $d('face_item_image_len_'+index_from).setAttribute('id','face_item_image_len_'+index_to);
                    $d('face_item_source_'+index_from).setAttribute('id','face_item_source_'+index_to);
                    $d('face_item_name_'+index_from).setAttribute('id','face_item_name_'+index_to);
                    $d('face_item_age_'+index_from).setAttribute('id','face_item_age_'+index_to);
                    $d('face_item_city_'+index_from).setAttribute('id','face_item_city_'+index_to);
                    $d('face_item_date_'+index_from).setAttribute('id','face_item_date_'+index_to);

                    $d('face_item_controls_'+index_from).setAttribute('id','face_item_controls_'+index_to);
                    $d('face_item_edit_'+index_from).setAttribute('id','face_item_edit_'+index_to);
                    $d('face_item_photo_'+index_from).setAttribute('id','face_item_photo_'+index_to);
                    $d('face_item_remove_'+index_from).setAttribute('id','face_item_remove_'+index_to);

                }

            }
        },
        'rename_image':{
            'move_down':function(){

                let  list           =page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list
                    ,list_len       =list.length
                    ,index_from
                    ,index_to;

                for(index_to=list_len;index_to>0;index_to--){

                    index_from=parseInt(index_to)-1;

                    page_object.action.admin.action.face_item_data.action.rename_image.rename_index(index_from,index_to);

                }

            },
            'move_up':function(index){

                let  list           =page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list
                    ,list_len       =list.length
                    ,index_from
                    ,index_to;

                for(index_to=index;index_to<list_len;index_to++){

                    index_from=parseInt(index_to)+1;

                    page_object.action.admin.action.face_item_data.action.rename_image.rename_index(index_from,index_to);

                }

            },
            'rename_index':function(index_from,index_to){

                if(isset($d('dialog_face_item_'+index_from))){

                    $d('dialog_face_item_'+index_from).setAttribute('id','dialog_face_item_'+index_to);

                    $d('dialog_face_item_number_'+index_from).setAttribute('id','dialog_face_item_number_'+index_to);
                    $d('dialog_face_item_image_'+index_from).setAttribute('id','dialog_face_item_image_'+index_to);
                    $d('dialog_face_item_image_background_'+index_from).setAttribute('id','dialog_face_item_image_background_'+index_to);
                    $d('dialog_face_item_image_len_'+index_from).setAttribute('id','dialog_face_item_image_len_'+index_to);
                    $d('dialog_face_item_image_block_'+index_from).setAttribute('id','dialog_face_item_image_block_'+index_to);
                    $d('dialog_face_item_checkbox_'+index_from).setAttribute('id','dialog_face_item_checkbox_'+index_to);
                    $d('dialog_face_item_checkbox_item_'+index_from).setAttribute('id','dialog_face_item_checkbox_item_'+index_to);
                    $d('dialog_face_item_image_size_'+index_from).setAttribute('id','dialog_face_item_image_size_'+index_to);
                    $d('dialog_face_item_size_'+index_from).setAttribute('id','dialog_face_item_size_'+index_to);
                    $d('dialog_face_item_date_'+index_from).setAttribute('id','dialog_face_item_date_'+index_to);
                    $d('dialog_face_item_controls_'+index_from).setAttribute('id','dialog_face_item_controls_'+index_to);
                    $d('dialog_face_item_remove_'+index_from).setAttribute('id','dialog_face_item_remove_'+index_to);

                }

            }
        },
        'rename_video':{
            'move_down':function(){

                let  list           =page_object.action.admin.action.face_item_data.action.edit_video.box.video_list
                    ,list_len       =list.length
                    ,index_from
                    ,index_to;

                for(index_to=list_len;index_to>0;index_to--){

                    index_from=parseInt(index_to)-1;

                    page_object.action.admin.action.face_item_data.action.rename_video.rename_index(index_from,index_to);

                }

            },
            'move_up':function(index){

                let  list           =page_object.action.admin.action.face_item_data.action.edit_video.box.video_list
                    ,list_len       =list.length
                    ,index_from
                    ,index_to;

                for(index_to=index;index_to<list_len;index_to++){

                    index_from=parseInt(index_to)+1;

                    page_object.action.admin.action.face_item_data.action.rename_video.rename_index(index_from,index_to);

                }

            },
            'rename_index':function(index_from,index_to){

                if(isset($d('dialog_face_item_'+index_from))){

                    $d('dialog_face_item_'+index_from).setAttribute('id','dialog_face_item_'+index_to);

                    $d('dialog_face_item_number_'+index_from).setAttribute('id','dialog_face_item_number_'+index_to);
                    $d('dialog_face_item_video_'+index_from).setAttribute('id','dialog_face_item_video_'+index_to);
                    $d('dialog_face_item_date_'+index_from).setAttribute('id','dialog_face_item_date_'+index_to);
                    $d('dialog_face_item_controls_'+index_from).setAttribute('id','dialog_face_item_controls_'+index_to);
                    $d('dialog_face_item_remove_'+index_from).setAttribute('id','dialog_face_item_remove_'+index_to);

                }

            }
        },
        'add_face':{
            'init':function(){

                if(isset($d('face_add')))
                    $d('face_add').onclick=page_object.action.admin.action.face_item_data.action.add_face.box.init;

            },
            'box':{
                'dialog_index':null,
                'width':400,
                'isset_source_account_link':false,
                'init':function(){

                    let  inner              =''
                        ,data               =page_object.action.admin.data['face_data']
                        ,source_list        =data['source_list']
                        ,source_index;

                    inner+='<div id="dialog_face_image" class="dialog_row_block">';

                        inner+='<div id="dialog_face_image_block" style="margin: 0 0 0 0">';
                            inner+='<div id="dialog_face_image_item"></div>';
                            inner+='<div id="dialog_face_coords_list"></div>';
                        inner+='</div>';

                        inner+='<div id="dialog_face_image_label"></div>';

                        inner+='<div id="dialog_face_image_progress_block">';
                            inner+='<div id="dialog_face_image_progress_line"></div>';
                        inner+='</div>';

                        inner+='<div id="dialog_face_image_choose" class="dialog_face_image_choose">Загрузить изображение</div>';
                        inner+='<input type="file" id="dialog_face_image_file" style="display: none" />';

                    inner+='</div>';

                    inner+='<div id="dialog_face_data" class="dialog_row_block" style="display: none;">';

                        inner+='<div id="dialog_face_image_change" class="dialog_face_image_choose">Изменить лицо</div>';

                        inner+='<div class="dialog_row">';

                            inner+='<div id="dialog_source_label" class="dialog_row_label">';
                                inner+='<span>Источник</span>';
                                inner+='<span class="dialog_red_field">*</span>';
                            inner+='</div>';
                            inner+='<div id="dialog_source_input_block" class="dialog_row_input">';

                    inner+='<select id="dialog_source_input_select" class="dialog_row_input_text">';

                    for(source_index in source_list)
                        inner+='<option value="'+source_list[source_index]['ID']+'">'+stripSlashes(source_list[source_index]['name'])+'</option>';

                    inner+='</select>';

                            inner+='</div>';

                        inner+='</div>';

                        inner+='<div class="dialog_row">';

                            inner+='<div id="dialog_source_link_label" class="dialog_row_label">';
                                inner+='<span>Ссылка</span>';
                            inner+='</div>';
                            inner+='<div id="dialog_source_link_input_block" class="dialog_row_input">';
                                inner+='<input type="text" id="dialog_source_link_input_text" class="dialog_row_input_text" value="" />';
                            inner+='</div>';

                        inner+='</div>';

                        inner+='<div class="dialog_row">';

                            inner+='<div id="dialog_name_label" class="dialog_row_label">';
                                inner+='<span>Имя</span>';
                                inner+='<span class="dialog_red_field">*</span>';
                            inner+='</div>';
                            inner+='<div id="dialog_name_input_block" class="dialog_row_input">';
                                inner+='<input type="text" id="dialog_name_input_text" class="dialog_row_input_text" value="" />';
                            inner+='</div>';

                        inner+='</div>';

                        inner+='<div class="dialog_row">';

                            inner+='<div id="dialog_age_label" class="dialog_row_label">';
                                inner+='<span>Возраст</span>';
                                inner+='<span class="dialog_red_field">*</span>';
                            inner+='</div>';
                            inner+='<div id="dialog_age_input_block" class="dialog_row_input">';
                                inner+='<input type="number" id="dialog_age_input_text" class="dialog_row_input_text" value="" min="16" max="60" />';
                            inner+='</div>';

                        inner+='</div>';

                        inner+='<div class="dialog_row">';

                            inner+='<div id="dialog_city_label" class="dialog_row_label">';
                                inner+='<span>Город</span>';
                                inner+='<span class="dialog_red_field">*</span>';
                            inner+='</div>';
                            inner+='<div id="dialog_city_input_block" class="dialog_row_input">';
                                inner+='<input type="text" id="dialog_city_input_text" class="dialog_row_input_text" value="" />';
                            inner+='</div>';

                        inner+='</div>';

                        inner+='<div class="dialog_row">';

                            inner+='<div id="dialog_note_label" class="dialog_row_label">';
                                inner+='<span>Примечание</span>';
                            inner+='</div>';
                            inner+='<div id="dialog_note_input_block" class="dialog_row_input">';
                                inner+='<textarea id="dialog_note_input_text" class="dialog_row_textarea"></textarea>';
                            inner+='</div>';

                        inner+='</div>';

                    inner+='</div>';

                    page_object.action.admin.action.face_item_data.action.add_face.box.dialog_index=page_object.dialog.init({
                        'title':'Добавление нового лица',
                        'w':page_object.action.admin.action.face_item_data.action.add_face.box.width,
                        'inner':inner,
                        'on_create':function(){

                            $d('dialog_name_input_text').onkeyup                =page_object.action.admin.action.face_item_data.action.add_face.box.check;
                            $d('dialog_age_input_text').onkeyup                 =page_object.action.admin.action.face_item_data.action.add_face.box.check;
                            $d('dialog_source_link_input_text').onkeyup         =page_object.action.admin.action.face_item_data.action.add_face.box.check;
                            $d('dialog_city_input_text').onkeyup                =page_object.action.admin.action.face_item_data.action.add_face.box.check;

                            $d('dialog_face_image_choose').onclick=page_object.action.admin.action.face_item_data.action.add_face.box.upload.init;

                            ymaps.ready(function(){

                                ymaps.modules.require('SuggestView',function(SuggestView){

                                    var suggestView=new SuggestView('dialog_city_input_text',{container:all});

                                    suggestView.events.add('select',function(event){

                                        // page_object.action.admin.action.place_item_info.click.yandex.geocoder.init('dialog_city_input_text');

                                    });

                                });

                            });

                        },
                        'cancel':true
                    });

                },
                'check':function(){

                    let  id=this.id;

                    switch(id){

                        case'dialog_name_input_text':{

                            if(!empty($v(id)))
                                this.style.border='1px solid #1ACC00';

                            break;

                        }

                        case'dialog_city_input_text':{

                            if(!empty($v(id)))
                                this.style.border='1px solid #1ACC00';

                            break;

                        }

                        case'dialog_age_input_text':{

                            if(!empty($v(id)))
                                if(parseInt($v(id))>=18)
                                    this.style.border='1px solid #1ACC00';
                                else
                                    this.style.border='1px solid #ff0000';

                            break;

                        }

                        case'dialog_source_link_input_text':{

                            if(!empty($v(id)))
                                page_object.action.admin.action.face_item_data.action.add_face.box.source_account_link.init();
                            else
                                this.style.border='1px solid #ff0000';

                            break;

                        }

                        default:{

                            if(!empty($v(id)))
                                $s(id).borderColor='';

                            break;

                        }

                    }

                },
                'source_account_link':{
                    'timeout_event':null,
                    'timeout':300,
                    'init':function(){

                        if(empty($v('dialog_source_link_input_text')))
                            return false;

                        page_object.action.admin.action.face_item_data.action.add_face.box.source_account_link.restart_timeout();

                    },
                    'remove_timeout':function(){

                        if(isset(page_object.action.admin.action.face_item_data.action.add_face.box.source_account_link.timeout_event))
                            clearTimeout(page_object.action.admin.action.face_item_data.action.add_face.box.source_account_link.timeout_event);

                    },
                    'restart_timeout':function(){

                        page_object.action.admin.action.face_item_data.action.add_face.box.source_account_link.remove_timeout();

                        page_object.action.admin.action.face_item_data.action.add_face.box.source_account_link.timeout_event=setTimeout(page_object.action.admin.action.face_item_data.action.add_face.box.source_account_link.send,page_object.action.admin.action.face_item_data.action.add_face.box.source_account_link.timeout);

                    },
                    'send':function(){

                        let post='';

                        post+='source_account_link='+uniEncode($v('dialog_source_link_input_text'));

                        send({
                            'scriptPath':'/api/json/isset_face_source_account_link',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                let  dataTemp
                                    ,data;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    if(isset($d('dialog_source_link_input_text'))){

                                        $s('dialog_source_link_input_text').border='1px solid #ff0000';

                                        page_object.action.admin.action.face_item_data.action.add_face.box.isset_source_account_link=true;

                                    }

                                }
                                else{

                                    if(data['isset_source_account_link']){

                                        $s('dialog_source_link_input_text').border='1px solid #ff0000';

                                        page_object.action.admin.action.face_item_data.action.add_face.box.isset_source_account_link=true;

                                    }
                                    else{

                                        $s('dialog_source_link_input_text').border='1px solid #1ACC00';

                                        page_object.action.admin.action.face_item_data.action.add_face.box.isset_source_account_link=false;

                                    }

                                }

                            }
                        });

                        page_object.action.admin.action.face_item_data.action.add_face.box.source_account_link.remove_timeout();

                    }
                },
                'upload':{
                    'init':function(){

                        $d('dialog_face_image_file').onchange=page_object.action.admin.action.face_item_data.action.add_face.box.upload.on_change_file;

                        $d('dialog_face_image_file').click();

                    },
                    'on_change_file':function(dialog_index){

                        let  data
                            ,list           =[]
                            ,i
                            ,file_list      =this.files
                            ,list_item      ={};

                        if(file_list.length===0)
                            return false;

                        $s('dialog_face_image_label').display               ='block';
                        $d('dialog_face_image_label').innerHTML             ='Загрузка 1%';

                        $s('dialog_face_image_progress_block').display      ='block';
                        $s('dialog_face_image_progress_line').width         ='1%';

                        $d('dialog_face_image_choose').innerHTML            ='Подождите';
                        $d('dialog_face_image_choose').onclick              =function(){};

                        $d('dialog_face_image_item').display                ='none';
                        $d('dialog_face_image_item').innerHTML              ='';

                        $d('dialog_face_coords_list').innerHTML             ='';

                        $d('dialog_face_image_choose').setAttribute('class','dialog_face_image_choose_disable');
                        $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label');

                        for(i=0;i<file_list.length;i++){

                            list_item={
                                'file_ID':null,
                                'remove':false,
                                'file':file_list[i]
                            };

                            list.push(list_item);

                        }

                        data={
                            'on_progress':page_object.action.admin.action.face_item_data.action.add_face.box.upload.on_progress.init,
                            'on_uploaded':page_object.action.admin.action.face_item_data.action.add_face.box.upload.on_uploaded.init,
                            'on_complete':page_object.action.admin.action.face_item_data.action.add_face.box.upload.on_complete.init,
                            'file_type':'image',
                            'list':list
                        };

                        page_object.upload.reset();

                        page_object.upload.init(dialog_index,data);

                        page_object.dialog.action.resize();

                    },
                    'on_progress':{
                        'init':function(){

                            if(isset($d('dialog_face_image_progress_line'))){

                                let perc=(page_object.upload.data['list'][page_object.upload.file_item['index']]['percent']*100).toFixed(1);

                                if((page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_index']+2)<page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_len'])
                                    $d('dialog_face_image_label').innerHTML='Загрузка '+perc+'%';
                                else
                                    $d('dialog_face_image_label').innerHTML='Обработка изображения';

                                $s('dialog_face_image_progress_line').width=perc+'%';

                            }

                        }
                    },
                    'on_uploaded':{
                        'init':function(){

                            if(isset($d('dialog_face_image_progress_line')))
                                page_object.action.admin.action.face_item_data.action.add_face.box.face.init();

                        }
                    },
                    'on_complete':{
                        'init':function(){}
                    }
                },
                'face':{
                    'face_index':null,
                    'image_ID':null,
                    'image_data':null,
                    'file_ID':null,
                    'face_len':0,
                    'face_list':null,
                    'face_delta_x':1.6,
                    'face_delta_y':1.3,
                    'face_coords_list':null,
                    'face_image_min_w':70,
                    'face_image_min_h':70,
                    'init':function(){

                        if(isset($d('dialog_face_image_progress_block'))){

                            $s('dialog_face_image_progress_block').display  ='none';
                            $s('dialog_face_image_progress_line').width     =0;

                        }

                        if(isset($d('dialog_face_image_label')))
                            $d('dialog_face_image_label').innerHTML='Поиск лиц...';

                        page_object.dialog.action.resize();

                        var post='';

                        post+='image_ID='+uniEncode(page_object.upload.data['list'][page_object.upload.file_item['index']]['image_ID']);
                        post+='&file_ID='+uniEncode(page_object.upload.data['list'][page_object.upload.file_item['index']]['file_ID']);

                        send({
                            'scriptPath':'/api/json/get_faces_coords',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                let  data
                                    ,dataTemp;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    if(isset($d('dialog_face_image_file'))){

                                        $d('dialog_face_image_choose').setAttribute('class','dialog_face_image_choose');

                                        $d('dialog_face_image_choose').innerHTML        ='Загрузить изображение';
                                        $d('dialog_face_image_choose').onclick          =page_object.action.admin.action.face_item_data.action.add_face.box.upload.init;

                                    }

                                    if(isset($d('dialog_face_image_label'))){

                                        $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label_red');

                                        $d('dialog_face_image_label').innerHTML=stripSlashes(data['error']['data']['info']);

                                        $d('dialog_face_image_file').value='';

                                    }

                                }
                                else{

                                    page_object.action.admin.action.face_item_data.action.add_face.box.face.file_ID         =data['data']['file_ID'];
                                    page_object.action.admin.action.face_item_data.action.add_face.box.face.image_ID        =data['data']['image_ID'];
                                    page_object.action.admin.action.face_item_data.action.add_face.box.face.image_data      =data['data']['image'];
                                    page_object.action.admin.action.face_item_data.action.add_face.box.face.face_len        =data['data']['face_len'];
                                    page_object.action.admin.action.face_item_data.action.add_face.box.face.face_list       =data['data']['face_list'];

                                    if(data['data']['face_len']===0){

                                        if(isset($d('dialog_face_image_label'))){

                                            $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label_red');
                                            $d('dialog_face_image_label').innerHTML='Лица не найдены =/';

                                        }

                                        if(isset($d('dialog_face_image_choose'))){

                                            $d('dialog_face_image_choose').onclick=page_object.action.admin.action.face_item_data.action.add_face.box.upload.init;

                                            $d('dialog_face_image_choose').innerHTML='Загрузить изображение';
                                            $d('dialog_face_image_choose').setAttribute('class','dialog_face_image_choose')

                                        }

                                        if(isset($d('dialog_face_image_file')))
                                            $d('dialog_face_image_file').value='';

                                        return true;

                                    }

                                    if(isset($d('dialog_face_image_block'))){

                                        let  image_data     =data['data']['image']
                                            ,image_link     ='/'+image_data['image_dir']+'/'+image_data['image_item_ID_list']['large']['ID']
                                            ,box_width      =page_object.action.admin.action.face_item_data.action.add_face.box.width
                                            ,el;

                                        el=addElement({
                                            'tag':'img',
                                            'src':image_link,
                                            'width':box_width
                                        });

                                        el.onload           =page_object.action.admin.action.face_item_data.action.add_face.box.face.show_face_list;
                                        // el.onprogress       =function(e){
                                        //
                                        //     e=e||window.event;
                                        //
                                        //     $d('dialog_face_image_label').innerHTML='Загрузка изображения '+parseInt((e.loaded/e.total)*100)+'%';
                                        //
                                        // };
                                        // el.onloadstart      =function(){
                                        //
                                        //     $d('dialog_face_image_label').innerHTML='Загрузка изображения 0%';
                                        //
                                        // };

                                    }

                                    if(isset($d('dialog_face_image_label'))){

                                        $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label');
                                        $d('dialog_face_image_label').innerHTML='Загрузка изображения...';

                                    }

                                }

                            }
                        });

                    },
                    'show_face_list':function(){

                        if(isset($d('dialog_face_image_block')))
                            $s('dialog_face_image_block').display='block';

                        if(isset($d('dialog_face_image_label'))){

                            $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label');
                            $d('dialog_face_image_label').innerHTML='Выберите лицо';

                        }

                        if(isset($d('dialog_face_image_item'))){

                            $d('dialog_face_image_item').innerHTML='';
                            $d('dialog_face_image_item').appendChild(this);

                        }

                        if(isset($d('dialog_face_coords_list'))){

                            $d('dialog_face_coords_list').innerHTML='';

                            let  image_data             =page_object.action.admin.action.face_item_data.action.add_face.box.face.image_data
                                ,face_delta_x           =page_object.action.admin.action.face_item_data.action.add_face.box.face.face_delta_x
                                ,face_delta_y           =page_object.action.admin.action.face_item_data.action.add_face.box.face.face_delta_y
                                ,image_w                =image_data['image_item_ID_list']['large']['width']
                                ,image_h                =image_data['image_item_ID_list']['large']['height']
                                ,box_width              =page_object.action.admin.action.face_item_data.action.add_face.box.width
                                ,face_list              =page_object.action.admin.action.face_item_data.action.add_face.box.face.face_list
                                ,face_len               =page_object.action.admin.action.face_item_data.action.add_face.box.face.face_len
                                ,face_image_min_w       =page_object.action.admin.action.face_item_data.action.add_face.box.face.face_image_min_w
                                ,face_image_min_h       =page_object.action.admin.action.face_item_data.action.add_face.box.face.face_image_min_h
                                ,face_index
                                ,face_w
                                ,face_h
                                ,face_x
                                ,face_y
                                ,face_w_new
                                ,face_h_new
                                ,face_x_new
                                ,face_y_new
                                ,image_w_new
                                ,image_h_new
                                ,coords
                                ,face_check_len         =0
                                ,is_face_checked        =false
                                ,is_isset               =false
                                ,perc                   =parseInt(box_width)/parseInt(image_w)
                                ,el;

                            page_object.action.admin.action.face_item_data.action.add_face.box.face.face_coords_list=[];

                            if(face_len===1)
                                if(!is_null(face_list[0]['face_ID']))
                                    if(face_list[0]['confidence']>70){

                                        if(isset($d('dialog_face_image_label'))){

                                            $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label_red');
                                            $d('dialog_face_image_label').innerHTML='Лицо уже сущевтвует в системе #'+face_list[0]['face_ID'];

                                        }

                                        if(isset($d('dialog_face_image_block')))
                                            $s('dialog_face_image_block').display='none';

                                        is_isset=true;

                                    }

                            if(!is_isset){

                                for(face_index in face_list){

                                    is_face_checked     =false;

                                    coords              =face_list[face_index]['coords'];

                                    face_x              =coords['left'];
                                    face_y              =coords['top'];
                                    // face_w              =coords['right']-coords['left'];
                                    // face_h              =coords['bottom']-coords['top'];
                                    face_w              =coords['width'];
                                    face_h              =coords['height'];

                                    face_y              -=Math.ceil(face_h/3);
                                    face_h              +=Math.ceil(face_h/3);

                                    if(
                                          // face_w>face_image_min_w
                                        // &&face_h>face_image_min_h
                                        true
                                    ){

                                        face_check_len++;

                                        face_w      =Math.ceil(face_w*perc);
                                        face_h      =Math.ceil(face_h*perc);

                                        face_x      =Math.ceil(face_x*perc);
                                        face_y      =Math.ceil(face_y*perc);

                                        face_w_new  =face_w*face_delta_x;
                                        face_h_new  =face_h*face_delta_y;

                                        face_x_new  =face_x-Math.ceil((face_w_new-face_w)/2);
                                        face_y_new  =face_y-Math.ceil((face_h_new-face_h)/2);

                                        if(face_x_new<0)
                                            face_x_new=0;

                                        if(face_y_new<0)
                                            face_y_new=0;

                                        image_w_new     =Math.ceil(image_w*perc);
                                        image_h_new     =Math.ceil(image_h*perc);

                                        if(face_x_new+face_w_new>image_w_new)
                                            face_w_new=image_w_new-face_x_new;

                                        if(face_y_new+face_h_new>image_h_new)
                                            face_h_new=image_h_new-face_y_new;

                                        page_object.action.admin.action.face_item_data.action.add_face.box.face.face_coords_list.push({
                                            'x':Math.ceil(face_x_new/perc),
                                            'y':Math.ceil(face_y_new/perc),
                                            'w':Math.ceil(face_w_new/perc),
                                            'h':Math.ceil(face_h_new/perc),
                                            'face_token':face_list[face_index]['face_token']
                                        });

                                        el=addElement({
                                            'tag':'div',
                                            'id':'dialog_face_image_item_'+face_index,
                                            'class':'dialog_face_image_item',
                                            'style':'width: '+face_w_new+'px; height: '+face_h_new+'px; margin: '+face_y_new+'px 0 0 '+face_x_new+'px;'
                                        });

                                        if(!is_null(face_list['face_ID'])){

                                            el.setAttribute('title','Уже существует в системе');
                                            el.setAttribute('class','dialog_face_image_item_isset');

                                        }
                                        else
                                            el.onclick=page_object.action.admin.action.face_item_data.action.add_face.box.face.change_face;

                                        $d('dialog_face_coords_list').appendChild(el);

                                    }

                                }

                                if(face_check_len===0){

                                    if(isset($d('dialog_face_image_block')))
                                        $s('dialog_face_image_block').display='none';

                                    if(isset($d('dialog_face_image_item')))
                                        $d('dialog_face_image_item').innerHTML='';

                                    if(isset($d('dialog_face_coords_list')))
                                        $d('dialog_face_coords_list').innerHTML='';

                                    if(isset($d('dialog_face_image_label'))){

                                        $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label_red');

                                        if(face_len===1)
                                            $d('dialog_face_image_label').innerHTML='Слишком маленькое лицо на изображении';
                                        else
                                            $d('dialog_face_image_label').innerHTML='Слишком маленькие лица на изображении';

                                    }

                                    if(isset($d('dialog_face_image_file')))
                                        $d('dialog_face_image_file').value='';

                                }

                            }

                        }

                        if(isset($d('dialog_face_image_choose'))){

                            $d('dialog_face_image_choose').onclick=page_object.action.admin.action.face_item_data.action.add_face.box.upload.init;

                            $d('dialog_face_image_choose').innerHTML='Загрузить изображение';
                            $d('dialog_face_image_choose').setAttribute('class','dialog_face_image_choose')

                        }

                        if(isset($d('dialog_face_image_file')))
                            $d('dialog_face_image_file').value='';

                        page_object.dialog.action.resize();

                    },
                    'change_face':function(){

                        let  face_index             =this.id.split('_')[4]
                            ,dialog_index           =page_object.action.admin.action.face_item_data.action.add_face.box.dialog_index
                            ,el;

                        page_object.action.admin.action.face_item_data.action.add_face.box.face.face_index=face_index;

                        if(isset($d('dialog_face_image')))
                            $s('dialog_face_image').display='none';

                        if(isset($d('dialog_face_data')))
                            $s('dialog_face_data').display='block';
                        
                        el=addElement({
                            'tag':'div',
                            'id':'dialog_submit_add_'+dialog_index,
                            'class':'dialog_submit_add',
                            'inner':'Добавить'
                        });

                        $d('dialog_controls_'+dialog_index).insertBefore(el,$d('dialog_submit_cancel_'+dialog_index));
                        
                        el.onclick=page_object.action.admin.action.face_item_data.action.add_face.add.init;

                        if(isset($d('dialog_face_image_change')))
                            $d('dialog_face_image_change').onclick=page_object.action.admin.action.face_item_data.action.add_face.box.face.change_image;

                        page_object.dialog.action.resize();

                    },
                    'change_image':function(){

                        if(isset($d('dialog_face_image')))
                            $s('dialog_face_image').display='block';

                        if(isset($d('dialog_face_data')))
                            $s('dialog_face_data').display='none';

                        let dialog_index=page_object.action.admin.action.face_item_data.action.add_face.box.dialog_index;

                        if(isset($d('dialog_submit_add_'+dialog_index)))
                            removeElement($d('dialog_submit_add_'+dialog_index));

                        page_object.dialog.action.resize();

                    }
                }
            },
            'add':{
                'init':function(){
                    
                    let  is_error       =false
                        ,dialog_index   =page_object.action.admin.action.face_item_data.action.add_face.box.dialog_index;

                    if(empty($v('dialog_name_input_text'))){

                        $s('dialog_name_input_text').borderColor='#ff0000';

                        is_error=true;

                    }

                    if(empty($v('dialog_age_input_text'))){

                        $s('dialog_age_input_text').borderColor='#ff0000';

                        is_error=true;

                    }

                    if(empty($v('dialog_city_input_text'))){

                        $s('dialog_city_input_text').borderColor='#ff0000';

                        is_error=true;

                    }

                    if(page_object.action.admin.action.face_item_data.action.add_face.box.isset_source_account_link){

                        $s('dialog_source_link_input_text').borderColor='#ff0000';

                        is_error=true;

                    }

                    if(!is_error)
                        page_object.action.admin.action.face_item_data.action.add_face.add.send(dialog_index);

                },
                'send':function(dialog_index){

                    var  lang_obj           =page_object.action.admin.content[page_object.lang]
                        ,coords_list        =page_object.action.admin.action.face_item_data.action.add_face.box.face.face_coords_list
                        ,coords_index       =page_object.action.admin.action.face_item_data.action.add_face.box.face.face_index;

                    if(isset($d('dialog_submit_add_'+dialog_index))){

                        $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_add_'+dialog_index).innerHTML    =lang_obj['wait'];
                        $d('dialog_submit_add_'+dialog_index).onclick      =function(){};

                    }

                    if(isset($d('dialog_face_image_change'))){

                        $d('dialog_face_image_change').setAttribute('class','dialog_face_image_choose_disable');

                        $d('dialog_face_image_change').onclick=function(){};

                    }

                    if(isset($d('dialog_submit_cancel_'+dialog_index))){

                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    }

                    let post='';

                    post+='file_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.add_face.box.face.file_ID);
                    post+='&image_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.add_face.box.face.image_ID);
                    post+='&coords='+uniEncode(jsonEncode(coords_list));
                    // post+='&coords='+uniEncode(jsonEncode(coords_list[coords_index]));
                    post+='&face_token='+uniEncode(coords_list[coords_index]['face_token']);

                    post+='&name='+uniEncode($d('dialog_name_input_text').value);
                    post+='&source_ID='+uniEncode($d('dialog_source_input_select').value);
                    post+='&source_account_link='+uniEncode($d('dialog_source_link_input_text').value);
                    post+='&city_name='+uniEncode($d('dialog_city_input_text').value);
                    post+='&age='+uniEncode($d('dialog_age_input_text').value);
                    post+='&info='+uniEncode($d('dialog_note_input_text').value);

                    send({
                        // 'scriptPath':'/api/json/add_face',
                        'scriptPath':'/api/json/add_face_to_face_plus_plus',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if($d('dialog_submit_add_'+dialog_index)){

                                    $d('dialog_submit_add_'+dialog_index).onclick=function(){

                                        page_object.action.admin.action.face_item_data.action.add_face.add.init();

                                    };
                                    $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_add');
                                    $d('dialog_submit_add_'+dialog_index).innerHTML=lang_obj['add'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                                if(isset($d('dialog_face_image_change'))){

                                    $d('dialog_face_image_change').setAttribute('class','dialog_face_image_choose');

                                    $d('dialog_face_image_change').onclick=page_object.action.admin.action.face_item_data.action.add_face.box.face.change_image;

                                }

                                if(isset($d('dialog_face_image_label'))){

                                    $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label_red');

                                    if(isset(data['error']['data']))
                                        $d('dialog_face_image_label').innerHTML=stripSlashes(data['error']['data']['info']);
                                    else
                                        $d('dialog_face_image_label').innerHTML='Неизвестная ошибка';

                                }

                                if(isset($d('dialog_face_image_file')))
                                    $d('dialog_face_image_file').value='';

                                page_object.action.admin.action.face_item_data.action.add_face.box.face.change_image();

                            }
                            else
                                page_object.action.admin.action.face_item_data.action.add_face.add.prepare(dialog_index,data);

                        }
                    });

                },
                'prepare':function(dialog_index,face_data){

                    var  face_index    =0
                        ,el
                        ,inner;

                    page_object.action.admin.data['face_data']['face_list'].unshift(face_data['data']);

                    inner=page_object.action.admin.create.face.create.get_face_item_data_row(face_index);

                    page_object.action.admin.action.face_item_data.action.rename_face.move_down();

                    el=addElement({
                        'tag':'div',
                        'id':'face_item_'+face_index,
                        'class':'face_item',
                        'style':'opacity: 0; height: 0px;',
                        'inner':inner
                    });

                    if(page_object.action.admin.data['face_data']['face_list'].length===1)
                        $d('face_list').appendChild(el);
                    else
                        $d('face_list').insertBefore(el,$d('face_item_1'));

                    page_object.action.admin.action.face_item_data.action.init();

                    page_object.dialog.action.un_show.init(dialog_index,true);

                    setTimeout(function(){

                        if(isset($d('face_item_'+face_index))){

                            $s('face_item_'+face_index).opacity     =1;
                            $s('face_item_'+face_index).height      ='150px';

                        }

                    },500);

                }
            }
        },
        'edit_face':{
            'init':function(){

                let  list           =page_object.action.admin.data['face_data']['face_list']
                    ,index;

                for(index in list)
                    if(isset($d('face_item_edit_'+index)))
                        $d('face_item_edit_'+index).onclick=page_object.action.admin.action.face_item_data.action.edit_face.box.init;

            },
            'box':{
                'face_index':null,
                'dialog_index':null,
                'width':400,
                'isset_source_account_link':false,
                'init':function(){

                    page_object.action.admin.action.face_item_data.action.edit_face.box.face_index                  =null;
                    page_object.action.admin.action.face_item_data.action.edit_face.box.dialog_index                =null;
                    page_object.action.admin.action.face_item_data.action.edit_face.box.isset_source_account_link   =null;

                    let  inner              =''
                        ,face_index         =this.id.split('_')[3]
                        ,data               =page_object.action.admin.data['face_data']
                        ,face_list          =data['face_list']
                        ,face_data          =face_list[face_index]
                        ,source_list        =data['source_list']
                        ,source_index
                        ,face_data_type_ID
                        ,type_list          =data['face_data_type_list']
                        ,type_index;

                    page_object.action.admin.action.face_item_data.action.edit_face.box.face_index=face_index;

                    inner+='<div id="dialog_face_data" class="dialog_row_block">';

                    inner+='<div class="dialog_row">';

                    inner+='<div id="dialog_source_label" class="dialog_row_label">';
                    inner+='<span>Источник</span>';
                    inner+='<span class="dialog_red_field">*</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_source_input_block" class="dialog_row_input">';

                    inner+='<select id="dialog_source_input_select" class="dialog_row_input_text">';

                    for(source_index in source_list){

                        inner+='<option value="'+source_list[source_index]['ID']+'"'+(parseInt(source_list[source_index]['ID'])===parseInt(face_data['source_ID'])?' selected="selected"':'')+'>'+stripSlashes(source_list[source_index]['name'])+'</option>';

                        if(parseInt(source_list[source_index]['ID'])===parseInt(face_data['source_ID']))
                            face_data_type_ID=source_list[source_index]['rang'];

                    }

                    inner+='</select>';

                    inner+='</div>';

                    inner+='</div>';

                    inner+='<div id="dialog_face_data" class="dialog_row_block">';

                    inner+='<div class="dialog_row">';

                    inner+='<div id="dialog_source_label" class="dialog_row_label">';
                    inner+='<span>Уровень</span>';
                    inner+='<span class="dialog_red_field">*</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_source_input_block" class="dialog_row_input">';

                    inner+='<select id="dialog_level_input_select" class="dialog_row_input_text">';

                    for(type_index in type_list)
                        inner+='<option value="'+type_list[type_index]['ID']+'"'+(type_list[type_index]['ID']===parseInt(face_data['face_data_type_ID']||(empty(face_data['face_data_type_ID'])&&parseInt(type_list[type_index]['ID'])===parseInt(face_data_type_ID)))?' selected="selected"':'')+'>'+stripSlashes(type_list[type_index]['name'])+'</option>';

                    inner+='</select>';

                    inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="dialog_row">';

                    inner+='<div id="dialog_source_link_label" class="dialog_row_label">';
                    inner+='<span>Ссылка</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_source_link_input_block" class="dialog_row_input">';
                    inner+='<input type="text" id="dialog_source_link_input_text" class="dialog_row_input_text" value="'+(empty(face_data['source_account_link'])?'':stripSlashes(face_data['source_account_link']))+'" />';
                    inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="dialog_row">';

                    inner+='<div id="dialog_name_label" class="dialog_row_label">';
                    inner+='<span>Имя</span>';
                    inner+='<span class="dialog_red_field">*</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_name_input_block" class="dialog_row_input">';
                    inner+='<input type="text" id="dialog_name_input_text" class="dialog_row_input_text" value="'+(empty(face_data['name'])?'':stripSlashes(face_data['name']))+'" />';
                    inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="dialog_row">';

                    inner+='<div id="dialog_age_label" class="dialog_row_label">';
                    inner+='<span>Возраст</span>';
                    inner+='<span class="dialog_red_field">*</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_age_input_block" class="dialog_row_input">';
                    inner+='<input type="number" id="dialog_age_input_text" class="dialog_row_input_text" value="'+(empty(face_data['age'])?'':stripSlashes(face_data['age']))+'" min="16" max="60" />';
                    inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="dialog_row">';

                    inner+='<div id="dialog_city_label" class="dialog_row_label">';
                    inner+='<span>Город</span>';
                    inner+='<span class="dialog_red_field">*</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_city_input_block" class="dialog_row_input">';
                    inner+='<input type="text" id="dialog_city_input_text" class="dialog_row_input_text" value="'+(empty(face_data['city'])?'':stripSlashes(face_data['city']))+'" />';
                    inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="dialog_row">';

                    inner+='<div id="dialog_note_label" class="dialog_row_label">';
                    inner+='<span>Примечание</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_note_input_block" class="dialog_row_input">';
                    inner+='<textarea id="dialog_note_input_text" class="dialog_row_textarea">'+(empty(face_data['info'])?'':stripSlashes(face_data['info']))+'</textarea>';
                    inner+='</div>';


                    inner+='</div>';

                    if(page_object.show_info_dev){

                        inner+='<div class="dialog_row">';

                        inner+='<div id="dialog_note_dev_label" class="dialog_row_label">';
                        inner+='<span>Примечание dev</span>';
                        inner+='</div>';
                        inner+='<div id="dialog_note_dev_input_block" class="dialog_row_input">';
                        inner+='<textarea id="dialog_note_dev_input_text" class="dialog_row_textarea">'+(empty(face_data['info_dev'])?'':stripSlashes(face_data['info_dev']))+'</textarea>';
                        inner+='</div>';

                        inner+='</div>';

                    }

                    inner+='</div>';

                    page_object.action.admin.action.face_item_data.action.edit_face.box.dialog_index=page_object.dialog.init({
                        'title':'Редактирование профиля',
                        'w':page_object.action.admin.action.face_item_data.action.edit_face.box.width,
                        'inner':inner,
                        'save':function(){

                            page_object.action.admin.action.face_item_data.action.edit_face.edit.init(page_object.action.admin.action.face_item_data.action.edit_face.box.dialog_index);
                            
                        },
                        'on_create':function(){

                            $d('dialog_name_input_text').onkeyup                =page_object.action.admin.action.face_item_data.action.edit_face.box.check;
                            $d('dialog_age_input_text').onkeyup                 =page_object.action.admin.action.face_item_data.action.edit_face.box.check;
                            $d('dialog_source_link_input_text').onkeyup         =page_object.action.admin.action.face_item_data.action.edit_face.box.check;
                            $d('dialog_city_input_text').onkeyup                =page_object.action.admin.action.face_item_data.action.edit_face.box.check;

                            ymaps.ready(function(){

                                ymaps.modules.require('SuggestView',function(SuggestView){

                                    var suggestView=new SuggestView('dialog_city_input_text',{container:all});

                                    suggestView.events.add('select',function(event){

                                        // page_object.action.admin.action.place_item_info.click.yandex.geocoder.init('dialog_city_input_text');

                                    });

                                });

                            });

                        },
                        'cancel':true
                    });

                },
                'check':function(){

                    let  id=this.id;

                    switch(id){

                        case'dialog_name_input_text':{

                            if(!empty($v(id)))
                                this.style.border='1px solid #1ACC00';

                            break;

                        }

                        case'dialog_city_input_text':{

                            if(!empty($v(id)))
                                this.style.border='1px solid #1ACC00';

                            break;

                        }

                        case'dialog_age_input_text':{

                            if(!empty($v(id)))
                                if(parseInt($v(id))>=18)
                                    this.style.border='1px solid #1ACC00';
                                else
                                    this.style.border='1px solid #ff0000';

                            break;

                        }

                        case'dialog_source_link_input_text':{

                            if(!empty($v(id)))
                                page_object.action.admin.action.face_item_data.action.edit_face.box.source_account_link.init();
                            else
                                this.style.border='1px solid #ff0000';

                            break;

                        }

                        default:{

                            if(!empty($v(id)))
                                $s(id).borderColor='';

                            break;

                        }

                    }

                },
                'source_account_link':{
                    'timeout_event':null,
                    'timeout':300,
                    'init':function(){

                        if(empty($v('dialog_source_link_input_text')))
                            return false;

                        page_object.action.admin.action.face_item_data.action.edit_face.box.source_account_link.restart_timeout();

                    },
                    'remove_timeout':function(){

                        if(isset(page_object.action.admin.action.face_item_data.action.edit_face.box.source_account_link.timeout_event))
                            clearTimeout(page_object.action.admin.action.face_item_data.action.edit_face.box.source_account_link.timeout_event);

                    },
                    'restart_timeout':function(){

                        page_object.action.admin.action.face_item_data.action.edit_face.box.source_account_link.remove_timeout();

                        page_object.action.admin.action.face_item_data.action.edit_face.box.source_account_link.timeout_event=setTimeout(page_object.action.admin.action.face_item_data.action.edit_face.box.source_account_link.send,page_object.action.admin.action.face_item_data.action.edit_face.box.source_account_link.timeout);

                    },
                    'send':function(){

                        let  post               =''
                            ,face_index         =page_object.action.admin.action.face_item_data.action.edit_face.box.face_index
                            ,data               =page_object.action.admin.data['face_data']
                            ,face_list          =data['face_list']
                            ,face_data          =face_list[face_index];

                        post+='source_account_link='+uniEncode($v('dialog_source_link_input_text'));
                        post+='&face_ID='+uniEncode(face_data['ID']);

                        send({
                            'scriptPath':'/api/json/isset_face_source_account_link',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                let  dataTemp
                                    ,data;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    if(isset($d('dialog_source_link_input_text'))){

                                        $s('dialog_source_link_input_text').border='1px solid #ff0000';

                                        page_object.action.admin.action.face_item_data.action.edit_face.box.isset_source_account_link=true;

                                    }

                                }
                                else{

                                    if(data['isset_source_account_link']){

                                        $s('dialog_source_link_input_text').border='1px solid #ff0000';

                                        page_object.action.admin.action.face_item_data.action.edit_face.box.isset_source_account_link=true;

                                    }
                                    else{

                                        $s('dialog_source_link_input_text').border='1px solid #1ACC00';

                                        page_object.action.admin.action.face_item_data.action.edit_face.box.isset_source_account_link=false;

                                    }

                                }

                            }
                        });

                        page_object.action.admin.action.face_item_data.action.edit_face.box.source_account_link.remove_timeout();

                    }
                },
            },
            'edit':{
                'init':function(){

                    let  is_error       =false
                        ,dialog_index   =page_object.action.admin.action.face_item_data.action.edit_face.box.dialog_index;

                    if(empty($v('dialog_name_input_text'))){

                        $s('dialog_name_input_text').borderColor='#ff0000';

                        is_error=true;

                    }

                    if(empty($v('dialog_age_input_text'))){

                        $s('dialog_age_input_text').borderColor='#ff0000';

                        is_error=true;

                    }

                    if(empty($v('dialog_city_input_text'))){

                        $s('dialog_city_input_text').borderColor='#ff0000';

                        is_error=true;

                    }

                    if(page_object.action.admin.action.face_item_data.action.edit_face.box.isset_source_account_link){

                        $s('dialog_source_link_input_text').borderColor='#ff0000';

                        is_error=true;

                    }

                    if(!is_error)
                        page_object.action.admin.action.face_item_data.action.edit_face.edit.send(dialog_index);

                },
                'send':function(dialog_index){

                    var  lang_obj           =page_object.action.admin.content[page_object.lang]
                        ,face_index         =page_object.action.admin.action.face_item_data.action.edit_face.box.face_index
                        ,data               =page_object.action.admin.data['face_data']
                        ,face_list          =data['face_list']
                        ,face_data          =face_list[face_index];

                    if(isset($d('dialog_submit_save_'+dialog_index))){

                        $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_save_'+dialog_index).innerHTML    =lang_obj['wait'];
                        $d('dialog_submit_save_'+dialog_index).onclick      =function(){};

                    }

                    if(isset($d('dialog_submit_cancel_'+dialog_index))){

                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    }

                    let post='';

                    post+='face_ID='+uniEncode(face_data['ID']);

                    post+='&name='+uniEncode($d('dialog_name_input_text').value);
                    post+='&source_ID='+uniEncode($d('dialog_source_input_select').value);
                    post+='&level='+uniEncode($d('dialog_level_input_select').value);
                    post+='&source_account_link='+uniEncode($d('dialog_source_link_input_text').value);
                    post+='&city='+uniEncode($d('dialog_city_input_text').value);
                    post+='&age='+uniEncode($d('dialog_age_input_text').value);
                    post+='&info='+uniEncode($d('dialog_note_input_text').value);

                    send({
                        'scriptPath':'/api/json/edit_face',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if($d('dialog_submit_save_'+dialog_index)){

                                    $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                        page_object.action.admin.action.face_item_data.action.edit_face.edit.init();

                                    };
                                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                    $d('dialog_submit_save_'+dialog_index).innerHTML=lang_obj['save'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                            }
                            else
                                page_object.action.admin.action.face_item_data.action.edit_face.edit.prepare(dialog_index,data);

                        }
                    });

                },
                'prepare':function(dialog_index,face_data){

                    let face_index=page_object.action.admin.action.face_item_data.action.edit_face.box.face_index;

                    face_data=face_data['data'];

                    page_object.action.admin.data['face_data']['face_list'][face_index]=face_data;

                    $d('face_item_source_'+face_index).innerHTML='<table width="100%" cellpadding="0" cellspacing="0"><tbody><tr><td width="100%" height="150"><a target="_blank" href="'+face_data['source_account_link']+'"'+(face_data['source_account_link'].length>20?(' title="'+stripSlashes(face_data['source_account_link'])+'"'):'')+'>'+stripSlashes(empty(face_data['source_link_number'])?'-':('#'+face_data['source_link_number']))+'</a></td></tr></tbody></table>';
                    $d('face_item_name_'+face_index).innerHTML='<table width="100%" cellpadding="0" cellspacing="0"><tbody><tr><td width="100%" height="150">'+stripSlashes(empty(face_data['name'])?'-':face_data['name'])+'</a></td></tr></tbody></table>';
                    $d('face_item_age_'+face_index).innerHTML='<table width="100%" cellpadding="0" cellspacing="0"><tbody><tr><td width="100%" height="150">'+stripSlashes(empty(face_data['age'])?'-':face_data['age'])+'</td></tr></tbody></table>';
                    $d('face_item_city_'+face_index).innerHTML='<table width="100%" cellpadding="0" cellspacing="0"><tbody><tr><td width="100%" height="150">'+stripSlashes(empty(face_data['city'])?'-':face_data['city'])+'</td></tr></tbody></table>';

                    page_object.action.admin.action.face_item_data.action.init();

                    page_object.dialog.action.un_show.init(dialog_index,true);

                }
            }
        },
        'edit_photo':{
            'init':function(){

                let  list           =page_object.action.admin.data['face_data']['face_list']
                    ,index;

                for(index in list)
                    if(isset($d('face_item_photo_'+index)))
                        $d('face_item_photo_'+index).onclick=page_object.action.admin.action.face_item_data.action.edit_photo.box.init;

            },
            'box':{
                'dialog_index':null,
                'image_list':null,
                'image_index':null,
                'face_ID':null,
                'image_ID':null,
                'face_index':null,
                'width':840,
                'init':function(){

                    page_object.action.admin.action.face_item_data.action.edit_photo.box.face_ID        =null;
                    page_object.action.admin.action.face_item_data.action.edit_photo.box.image_ID       =null;
                    page_object.action.admin.action.face_item_data.action.edit_photo.box.face_index     =null;
                    page_object.action.admin.action.face_item_data.action.edit_photo.box.dialog_index   =null;
                    page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list     =null;

                    let  inner              =''
                        ,face_index         =this.id.split('_')[3]
                        ,data               =page_object.action.admin.data['face_data']
                        ,face_list          =data['face_list']
                        ,face_data          =face_list[face_index];

                    page_object.action.admin.action.face_item_data.action.edit_photo.box.face_index     =face_index;
                    page_object.action.admin.action.face_item_data.action.edit_photo.box.face_ID        =face_list[face_index]['ID'];

                    inner+='<div class="dialog_row">';

                        inner+='<div id="dialog_face_image_load">Получение списка изображений...</div>';

                        inner+='<div id="dialog_face_add_image">Добавить лицо</div>';

                        inner+='<div id="dialog_face_item_header">';

                            inner+='<div class="face_header_col" style="width: 50px;">ID</div>';
                            inner+='<div class="face_header_col">Фото</div>';
                            inner+='<div class="face_header_col" style="width: 100px;">Обложка</div>';
                            inner+='<div class="face_header_col">Размер</div>';
                            inner+='<div class="face_header_col">Место</div>';
                            inner+='<div class="face_header_col">Дата</div>';
                            inner+='<div class="face_header_col" style="width: 30px"></div>';

                        inner+='</div>';

                        inner+='<div id="dialog_face_image_list"></div>';

                    inner+='</div>';

                    page_object.action.admin.action.face_item_data.action.edit_photo.box.dialog_index=page_object.dialog.init({
                        'title':'Редактирование фото: '+stripSlashes(face_data['name'])+' #'+face_data['ID'],
                        'w':page_object.action.admin.action.face_item_data.action.edit_photo.box.width,
                        'inner':inner,
                        'save':function(){

                            page_object.action.admin.action.face_item_data.action.edit_photo.edit.init(page_object.action.admin.action.face_item_data.action.edit_face.box.dialog_index);

                        },
                        'on_create':function(){

                            page_object.action.admin.action.face_item_data.action.edit_photo.box.load();

                        },
                        'cancel':true
                    });

                },
                'load':function(){

                    var  dialog_index       =page_object.action.admin.action.face_item_data.action.edit_photo.box.dialog_index
                        ,face_ID            =page_object.action.admin.action.face_item_data.action.edit_photo.box.face_ID
                        ,lang_obj           =page_object.action.admin.content[page_object.lang]
                        ,post               ='face_ID='+uniEncode(face_ID);

                    if(isset($d('dialog_submit_save_'+dialog_index))){

                        $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_save_'+dialog_index).innerHTML    =lang_obj['wait'];
                        $d('dialog_submit_save_'+dialog_index).onclick      =function(){};

                    }

                    if(isset($d('dialog_submit_cancel_'+dialog_index))){

                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    }

                    send({
                        'scriptPath':'/api/json/get_face_image_list',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if($d('dialog_submit_save_'+dialog_index)){

                                    $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                        page_object.action.admin.action.face_item_data.action.edit_photo.edit.init();

                                    };
                                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                    $d('dialog_submit_save_'+dialog_index).innerHTML=lang_obj['save'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                            }
                            else
                                page_object.action.admin.action.face_item_data.action.edit_photo.box.prepare(dialog_index,data['data']);

                        }
                    });

                },
                'prepare':function(dialog_index,data){

                    let  image_list         =data['image_list']
                        ,image_index
                        ,image_ID           =data['image_ID']
                        ,inner              ='';

                    page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list=image_list;

                    for(image_index in image_list){

                        inner+='<div id="dialog_face_item_'+image_index+'" class="dialog_face_item_row">';

                        if(parseInt(image_ID)===parseInt(image_list[image_index]['image_ID'])){

                            page_object.action.admin.action.face_item_data.action.edit_photo.box.image_index    =image_index;
                            page_object.action.admin.action.face_item_data.action.edit_photo.box.image_ID       =parseInt(image_ID);

                        }

                        inner+=page_object.action.admin.create.face.create.get_face_item_image_row(image_ID,image_list,image_index);

                        inner+='</div>';

                    }

                    if(isset($d('dialog_face_image_list')))
                        $d('dialog_face_image_list').innerHTML=inner;

                    if(page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list.length<4){

                        $s('dialog_face_image_list').overflow   ='hidden';
                        $s('dialog_face_image_list').height     ='auto';

                        if(page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list.length===1)
                            if(isset($d('dialog_face_item_controls_0')))
                                $s('dialog_face_item_controls_0').display='none';

                        page_object.dialog.action.resize();

                    }

                    if(isset($d('dialog_face_add_image'))){

                        $s('dialog_face_add_image').display='inline-block';

                        page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.init();

                    }

                    if(isset($d('dialog_face_item_header')))
                        $s('dialog_face_item_header').display='block';

                    if(isset($d('dialog_face_image_load')))
                        $d('dialog_face_image_load').innerHTML='';

                    for(image_index in image_list)
                        if(isset($d('dialog_face_item_remove_'+image_index)))
                            $d('dialog_face_item_remove_'+image_index).onclick=page_object.action.admin.action.face_item_data.action.edit_photo.box.remove.init;

                    page_object.action.admin.action.face_item_data.action.edit_photo.box.checkbox.init();
                    page_object.action.admin.action.face_item_data.action.edit_photo.box.image.init();
                    page_object.action.admin.action.face_item_data.action.edit_photo.box.remove.init();

                    page_object.dialog.action.resize();

                },
                'checkbox':{
                    'init':function(){

                        let  image_index
                            ,image_list     =page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list;

                        for(image_index in image_list)
                            if(isset($d('dialog_face_item_checkbox_item_'+image_index)))
                                $d('dialog_face_item_checkbox_item_'+image_index).onclick=page_object.action.admin.action.face_item_data.action.edit_photo.box.checkbox.check;

                    },
                    'check':function(){

                        let  image_index
                            ,image_current_index    =parseInt(this.id.split('_')[5])
                            ,image_list             =page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list
                            ,image_ID               =image_list[image_current_index]['image_ID'];

                        for(image_index in image_list)
                            if(isset($d('dialog_face_item_checkbox_item_'+image_index)))
                                $d('dialog_face_item_checkbox_item_'+image_index).setAttribute('class','dialog_checkbox');

                        page_object.action.admin.action.face_item_data.action.edit_photo.box.image_index    =image_current_index;
                        page_object.action.admin.action.face_item_data.action.edit_photo.box.image_ID       =image_ID;

                        this.setAttribute('class','dialog_checkbox_active');

                    }
                },
                'image':{
                    'image_ID':null,
                    'image_index':null,
                    'dialog_index':null,
                    'init':function(){

                        let  image_index
                            ,image_list     =page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list;

                        for(image_index in image_list)
                            if(isset($d('dialog_face_item_image_block_'+image_index)))
                                $d('dialog_face_item_image_block_'+image_index).onclick=page_object.action.admin.action.face_item_data.action.edit_photo.box.image.open;

                    },
                    'open':function(){

                        var  index              =this.id.split('_')[5]
                            ,image_list         =page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list
                            ,image_data         =image_list[index]
                            ,link               ='/'+image_data['image']['image_dir']+'/'+image_data['image']['image_item_ID_list']['middle']['ID']
                            // ,link               ='/'+image_data['image']['image_dir']+'/'+image_data['image']['image_item_ID_list']['large']['ID']
                            ,inner              ='';

                        page_object.action.admin.action.face_item_data.action.edit_photo.box.image.image_ID     =image_data['image_ID'];
                        page_object.action.admin.action.face_item_data.action.edit_photo.box.image.image_index  =parseInt(index);

                        inner+='<div id="dialog_image" class="dialog_row" style="text-align: center; max-height: calc(100vh - 200px); max-width: calc(100vw - 200px);">';
                            inner+='Подождите...';
                        inner+='</div>';

                        if(empty(image_data['faceset_ID'])){

                            inner+='<div id="find_face_block" class="dialog_row" style="display: none">';
                                inner+='<div id="dialog_face_find_face" class="dialog_submit_save" style="position: relative; display: inline-block;">Найти лица</div>';
                            inner+='</div>';

                        }

                        page_object.action.admin.action.face_item_data.action.edit_photo.box.image.dialog_index=page_object.dialog.init({
                            'w':600,
                            'inner':inner,
                            'on_create':function(){

                                var el=new Image();

                                trace(link);

                                el.src      =link;
                                el.onload   =page_object.action.admin.action.face_item_data.action.edit_photo.box.image.load;

                                if(isset($d('dialog_face_find_face')))
                                    $d('dialog_face_find_face').onclick=page_object.action.admin.action.face_item_data.action.edit_photo.box.face.init;

                            },
                            'cancel':true
                        });

                    },
                    'load':function(){

                        if(isset($d('dialog_image'))){

                            let  link           =this.src
                                ,w              =this.width
                                ,h              =this.height
                                ,screen_w       =600
                                ,screen_h       =winSize.winHeight-200
                                ,percent        =w/h
                                ,temp_w         =screen_w
                                ,temp_h         =temp_w/percent;

                            trace('percent:');
                            trace(percent);
                            trace('screen_h: '+screen_h);
                            trace(temp_w+' x '+temp_h);

                            if(temp_h>screen_h){

                                temp_h  =screen_h;
                                temp_w  =temp_h*percent

                            }

                            trace(temp_w+' x '+temp_h);

                            // if(temp_w===600)
                            $d('dialog_image').innerHTML='<div id="dialog_image_container"><div id="dialog_face_coords_list"></div><img id="dialog_image_src" src="'+link+'" width="'+temp_w+'" style="opacity: 0" /></div>';
                            // else
                            //     $d('dialog_image').innerHTML='<img id="dialog_image_src" src="'+link+'" height="100%" style="opacity: 0" />';

                            setTimeout(function(){

                                $s('dialog_image_src').opacity      =1;
                                $s('dialog_image').width            =elementSize.width($d('dialog_image'))+'px';
                                $s('dialog_image').height           =elementSize.height($d('dialog_image'))+'px';

                            },40);

                        }

                        if(isset($d('find_face_block')))
                            $s('find_face_block').display='block';

                    }
                },
                'remove':{
                    'init':function(){

                        let  image_index
                            ,image_list     =page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list;

                        for(image_index in image_list)
                            if(isset($d('dialog_face_item_remove_'+image_index)))
                                $d('dialog_face_item_remove_'+image_index).onclick=page_object.action.admin.action.face_item_data.action.edit_photo.box.remove.box;

                    },
                    'box':function(){

                        var  image_index        =this.id.split('_')[4]
                            ,image_list         =page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list
                            ,image_data         =image_list[image_index]
                            ,inner              ='';

                        inner+='<div class="dialog_row">';
                        inner+='Удалить фото #'+image_data['image_ID']+'?';
                        inner+='</div>';

                        var dialog_index=page_object.dialog.init({
                            'title':'Удаление фото',
                            'inner':inner,
                            'remove':function(){

                                page_object.action.admin.action.face_item_data.action.edit_photo.box.remove.prepare(dialog_index,image_index);

                            },
                            'cancel':true
                        });

                    },
                    'prepare':function(dialog_index,image_index){

                        let is_checkbox_active=$d('dialog_face_item_checkbox_item_'+image_index).getAttribute('class')==='dialog_checkbox_active';

                        if(isset($d('dialog_face_item_'+image_index)))
                            removeElement($d('dialog_face_item_'+image_index));

                        page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list.splice(image_index,1);

                        page_object.action.admin.action.face_item_data.action.rename_image.move_up(image_index);

                        page_object.action.admin.action.face_item_data.action.edit_photo.box.checkbox.init();
                        page_object.action.admin.action.face_item_data.action.edit_photo.box.image.init();
                        page_object.action.admin.action.face_item_data.action.edit_photo.box.remove.init();

                        if(page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list.length<4){

                            $s('dialog_face_image_list').overflow   ='hidden';
                            $s('dialog_face_image_list').height     ='auto';

                            if(page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list.length===1)
                                $s('dialog_face_item_controls_0').display='none';

                            page_object.dialog.action.resize();

                        }

                        if(is_checkbox_active)
                            if(isset($d('dialog_face_item_checkbox_item_0')))
                                $d('dialog_face_item_checkbox_item_0').click();

                        page_object.dialog.action.un_show.init(dialog_index,true);

                    }
                },
                'face':{
                    'face_index':null,
                    'image_ID':null,
                    'image_data':null,
                    'file_ID':null,
                    'face_len':0,
                    'face_list':null,
                    'face_delta_x':1.6,
                    'face_delta_y':1.3,
                    'face_coords_list':null,
                    'face_image_min_w':70,
                    'face_image_min_h':70,
                    'init':function(){

                        if(isset($d('dialog_face_find_face'))){

                            $d('dialog_face_find_face').setAttribute('class','dialog_submit_disable');

                            $d('dialog_face_find_face').onclick     =function(){};
                            $d('dialog_face_find_face').innerHTML   ='Поиск лиц...';

                        }

                        var post='';

                        post+='image_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.edit_photo.box.image.image_ID);

                        send({
                            'scriptPath':'/api/json/get_faces_coords',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                let  data
                                    ,dataTemp;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    if(isset($d('dialog_face_find_face'))){

                                        $d('dialog_face_find_face').setAttribute('class','dialog_submit_save');

                                        $d('dialog_face_find_face').onclick     =page_object.action.admin.action.face_item_data.action.edit_photo.box.face.init;
                                        $d('dialog_face_find_face').innerHTML   ='Найти лица';

                                    }

                                }
                                else{

                                    page_object.action.admin.action.face_item_data.action.edit_photo.box.face.file_ID         =data['data']['file_ID'];
                                    page_object.action.admin.action.face_item_data.action.edit_photo.box.face.image_ID        =data['data']['image_ID'];
                                    page_object.action.admin.action.face_item_data.action.edit_photo.box.face.image_data      =data['data']['image'];
                                    page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_len        =data['data']['face_len'];
                                    page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_list       =data['data']['face_list'];

                                    if(data['data']['face_len']===0){

                                        if(isset($d('dialog_face_find_face'))){

                                            $d('dialog_face_find_face').innerHTML='<b style="color: #ff0000;">Лица не найдены =/</b>';

                                            setTimeout(function(){

                                                $d('dialog_face_find_face').setAttribute('class','dialog_submit_save');

                                                $d('dialog_face_find_face').onclick     =page_object.action.admin.action.face_item_data.action.edit_photo.box.face.init;
                                                $d('dialog_face_find_face').innerHTML   ='Найти лица';

                                            },3000);

                                        }

                                        return true;

                                    }

                                    if(isset($d('dialog_face_find_face')))
                                        $d('dialog_face_find_face').innerHTML='Выберите лицо';

                                    page_object.action.admin.action.face_item_data.action.edit_photo.box.face.show_face_list();

                                }

                            }
                        });

                    },
                    'show_face_list':function(){

                        if(isset($d('dialog_face_coords_list'))){

                            $d('dialog_face_coords_list').innerHTML='';

                            let  image_data             =page_object.action.admin.action.face_item_data.action.edit_photo.box.face.image_data
                                ,face_delta_x           =page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_delta_x
                                ,face_delta_y           =page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_delta_y
                                ,image_w                =image_data['image_item_ID_list']['middle']['width']
                                ,image_h                =image_data['image_item_ID_list']['middle']['height']
                                ,box_width              =elementSize.width($d('dialog_image_container'))
                                // ,box_width              =page_object.action.admin.action.face_item_data.action.add_face.box.width
                                ,face_list              =page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_list
                                ,face_len               =page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_len
                                ,face_image_min_w       =page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_image_min_w
                                ,face_image_min_h       =page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_image_min_h
                                ,face_index
                                ,face_w
                                ,face_h
                                ,face_x
                                ,face_y
                                ,face_w_new
                                ,face_h_new
                                ,face_x_new
                                ,face_y_new
                                ,image_w_new
                                ,image_h_new
                                ,coords
                                ,face_check_len         =0
                                ,is_face_checked        =false
                                ,is_isset               =false
                                ,perc                   =parseInt(box_width)/parseInt(image_w)
                                ,el;

                            page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_coords_list=[];

                            for(face_index in face_list){

                                is_face_checked     =false;

                                coords              =face_list[face_index]['coords'];

                                face_x              =coords['left'];
                                face_y              =coords['top'];
                                // face_w              =coords['right']-coords['left'];
                                // face_h              =coords['bottom']-coords['top'];
                                face_w              =coords['width'];
                                face_h              =coords['height'];

                                face_y              -=Math.ceil(face_h/3);
                                face_h              +=Math.ceil(face_h/3);

                                if(
                                      // face_w>face_image_min_w
                                    // &&face_h>face_image_min_h
                                    true
                                ){

                                    face_check_len++;

                                    face_w      =Math.ceil(face_w*perc);
                                    face_h      =Math.ceil(face_h*perc);

                                    face_x      =Math.ceil(face_x*perc);
                                    face_y      =Math.ceil(face_y*perc);

                                    face_w_new  =face_w*face_delta_x;
                                    face_h_new  =face_h*face_delta_y;

                                    face_x_new  =face_x-Math.ceil((face_w_new-face_w)/2);
                                    face_y_new  =face_y-Math.ceil((face_h_new-face_h)/2);

                                    if(face_x_new<0)
                                        face_x_new=0;

                                    if(face_y_new<0)
                                        face_y_new=0;

                                    image_w_new     =Math.ceil(image_w*perc);
                                    image_h_new     =Math.ceil(image_h*perc);

                                    if(face_x_new+face_w_new>image_w_new)
                                        face_w_new=image_w_new-face_x_new;

                                    if(face_y_new+face_h_new>image_h_new)
                                        face_h_new=image_h_new-face_y_new;

                                    page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_coords_list.push({
                                        'x':Math.ceil(face_x_new/perc),
                                        'y':Math.ceil(face_y_new/perc),
                                        'w':Math.ceil(face_w_new/perc),
                                        'h':Math.ceil(face_h_new/perc),
                                        'face_token':face_list[face_index]['face_token']
                                    });

                                    el=addElement({
                                        'tag':'div',
                                        'id':'dialog_face_image_item_'+face_index,
                                        'class':'dialog_face_image_item',
                                        'style':'width: '+face_w_new+'px; height: '+face_h_new+'px; margin: '+face_y_new+'px 0 0 '+face_x_new+'px;'
                                    });

                                    el.onclick=page_object.action.admin.action.face_item_data.action.edit_photo.box.face.change_face;

                                    $d('dialog_face_coords_list').appendChild(el);

                                }

                            }

                        }

                        if(isset($d('dialog_face_find_face'))){

                            // $d('dialog_face_find_face').onclick=page_object.action.admin.action.face_item_data.action.add_face.box.upload.init;

                            $d('dialog_face_find_face').innerHTML='Выберите лицо';
                            // $d('dialog_face_find_face').setAttribute('class','dialog_submit_save')

                        }

                        page_object.dialog.action.resize();

                    },
                    'change_face':function(){

                        let  face_index             =this.id.split('_')[4]
                            ,face_list              =$d('dialog_face_coords_list').getElementsByTagName('div')
                            ,index
                            ,dialog_index           =page_object.action.admin.action.face_item_data.action.add_face.box.dialog_index
                            ,el
                            ,is_active              =false;

                        for(index in face_list)
                            if(isObject(face_list[index])){

                                trace(face_index+' === '+index);

                                if(parseInt(face_index)===parseInt(index)){

                                    trace(face_index+' === '+index);

                                    if(this.getAttribute('class')==='dialog_face_image_item_active'){

                                        this.setAttribute('class','dialog_face_image_item');

                                        page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_index=null;

                                    }
                                    else{

                                        is_active=true;

                                        this.setAttribute('class','dialog_face_image_item_active');

                                        page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_index=face_index;

                                    }

                                }
                                else
                                    face_list[index].setAttribute('class','dialog_face_image_item');

                            }

                        if(isset($d('dialog_face_find_face'))){

                            if(is_active){

                                $d('dialog_face_find_face').onclick=page_object.action.admin.action.face_item_data.action.edit_photo.box.add.init;
                                // $d('dialog_face_find_face').onclick=page_object.action.admin.action.face_item_data.action.add_face.box.upload.init;

                                $d('dialog_face_find_face').innerHTML='Загрузить лицо';
                                $d('dialog_face_find_face').setAttribute('class','dialog_submit_save')

                            }
                            else{

                                $d('dialog_face_find_face').onclick=function(){};

                                $d('dialog_face_find_face').innerHTML='Выберите лицо';
                                $d('dialog_face_find_face').setAttribute('class','dialog_submit_disable')

                            }

                        }

                    }
                },
                'add':{
                    'init':function(){

                        page_object.action.admin.action.face_item_data.action.edit_photo.box.add.send();

                    },
                    'send':function(){
    
                        var  lang_obj           =page_object.action.admin.content[page_object.lang]
                            ,dialog_index       =page_object.action.admin.action.face_item_data.action.edit_photo.box.image.dialog_index
                            ,coords_list        =page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_coords_list
                            ,coords_index       =page_object.action.admin.action.face_item_data.action.edit_photo.box.face.face_index;
    
                        if(isset($d('dialog_face_find_face'))){
    
                            $d('dialog_face_find_face').setAttribute('class','dialog_submit_disable');
    
                            $d('dialog_face_find_face').innerHTML   =lang_obj['wait'];
                            $d('dialog_face_find_face').onclick     =function(){};
    
                        }
    
                        let post='';
    
                        post+='face_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.edit_photo.box.face_ID);
                        post+='&file_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.edit_photo.box.face.file_ID);
                        post+='&image_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.edit_photo.box.face.image_ID);
                        post+='&coords='+uniEncode(jsonEncode(coords_list));
                        post+='&face_token='+uniEncode(coords_list[coords_index]['face_token']);
    
                        send({
                            // 'scriptPath':'/api/json/add_face',
                            'scriptPath':'/api/json/edit_face_image',
                            'postData':post,
                            'onComplete':function(j,worktime){
    
                                let  dataTemp
                                    ,data;
    
                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);
    
                                if(isset(data['error'])){
    
                                    if($d('dialog_face_find_face'+dialog_index)){
    
                                        $d('dialog_face_find_face'+dialog_index).onclick=function(){
    
                                            page_object.action.admin.action.face_item_data.action.edit_photo.box.add.init();
    
                                        };
                                        $d('dialog_face_find_face'+dialog_index).setAttribute('class','dialog_submit_add');
                                        $d('dialog_face_find_face'+dialog_index).innerHTML='Загрузить лицо';
    
                                    }
    
                                }
                                else
                                    page_object.action.admin.action.face_item_data.action.edit_photo.box.add.prepare(dialog_index,data);
    
                            }
                        });
    
                    },
                    'prepare':function(dialog_index,face_data){
    
                        // var  image_index=page_object.action.admin.action.face_item_data.action.edit_photo.box.image_index
                        var  image_index        =page_object.action.admin.action.face_item_data.action.edit_photo.box.image.image_index
                            ,face_index         =page_object.action.admin.action.face_item_data.action.edit_photo.box.face_index
                            ,el
                            ,inner;

                        page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list[image_index]=face_data['data'];
    
                        inner=page_object.action.admin.create.face.create.get_face_item_image_row(page_object.action.admin.action.face_item_data.action.edit_photo.box.image_ID,page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list,image_index);

                        $d('dialog_face_item_'+image_index).innerHTML=inner;

                        if(isset($d('face_item_image_len_'+face_index)))
                            $d('face_item_image_len_'+face_index).innerHTML=face_data['data']['image_facekit_len']+'/'+face_data['data']['image_len'];

                        setTimeout(function(){
    
                            if(isset($d('dialog_face_item_'+image_index))){
    
                                $s('dialog_face_item_'+image_index).opacity     =1;
                                $s('dialog_face_item_'+image_index).height      ='150px';
    
                            }

                            let  image_list=page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list
                                ,index;

                            page_object.dialog.action.resize();

                            for(index in image_list)
                                if(isset($d('dialog_face_item_remove_'+index)))
                                    $d('dialog_face_item_remove_'+index).onclick=page_object.action.admin.action.face_item_data.action.edit_photo.box.remove.init;

                            page_object.action.admin.action.face_item_data.action.edit_photo.box.checkbox.init();
                            page_object.action.admin.action.face_item_data.action.edit_photo.box.image.init();
                            page_object.action.admin.action.face_item_data.action.edit_photo.box.remove.init();

                            if(page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list.length<4){

                                $s('dialog_face_image_list').overflow   ='hidden';
                                $s('dialog_face_image_list').height     =$d('dialog_face_image_list').scrollHeight+'px';

                                if(page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list.length===1)
                                    if(isset($d('dialog_face_item_controls_0')))
                                        $s('dialog_face_item_controls_0').display='none';

                            }
                            else{

                                $s('dialog_face_image_list').overflowY  ='auto';
                                $s('dialog_face_image_list').height     ='500px';

                            }

                            page_object.dialog.action.resize();
    
                        },500);

                        page_object.dialog.action.un_show.init(dialog_index,true);
    
                    }
                }
            },
            'edit':{
                'init':function(){

                    page_object.action.admin.action.face_item_data.action.edit_photo.edit.send();

                },
                'send':function(){

                    let  image_list         =page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list
                        ,image_index
                        ,lang_obj           =page_object.action.admin.content[page_object.lang]
                        ,dialog_index       =page_object.action.admin.action.face_item_data.action.edit_photo.box.dialog_index
                        ,face_ID            =page_object.action.admin.action.face_item_data.action.edit_photo.box.face_ID
                        ,face_index         =page_object.action.admin.action.face_item_data.action.edit_photo.box.face_index
                        ,image_ID           =page_object.action.admin.action.face_item_data.action.edit_photo.box.image_ID
                        ,image_ID_list      =[]
                        ,post               ='';

                    for(image_index in image_list)
                        image_ID_list.push(image_list[image_index]['image_ID']);

                    post+='face_ID='+uniEncode(face_ID);
                    post+='&image_ID='+uniEncode(image_ID);
                    post+='&image_ID_list='+uniEncode(jsonEncode(image_ID_list));

                    if(isset($d('dialog_submit_save_'+dialog_index))){

                        $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_save_'+dialog_index).innerHTML    =lang_obj['wait'];
                        $d('dialog_submit_save_'+dialog_index).onclick      =function(){};

                    }

                    if(isset($d('dialog_submit_cancel_'+dialog_index))){

                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    }

                    send({
                        'scriptPath':'/api/json/save_face_image_list',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if($d('dialog_submit_save_'+dialog_index)){

                                    $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                        page_object.action.admin.action.face_item_data.action.edit_photo.edit.init();

                                    };
                                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                    $d('dialog_submit_save_'+dialog_index).innerHTML=lang_obj['save'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                            }
                            else
                                page_object.action.admin.action.face_item_data.action.edit_photo.edit.prepare(data['data']);

                        }
                    });

                },
                'prepare':function(data){

                    let  dialog_index       =page_object.action.admin.action.face_item_data.action.edit_photo.box.dialog_index
                        ,face_index         =page_object.action.admin.action.face_item_data.action.edit_photo.box.face_index
                        ,image_link
                        ,image_object       =new Image()
                        ,func;

                    if(isset($d('face_item_image_block_'+face_index)))
                        $s('face_item_image_block_'+face_index).opacity=0;

                    if(isset($d('face_item_image_background_'+face_index)))
                        $s('face_item_image_background_'+face_index).opacity=1;

                    if(!empty(data['image'])){

                        image_link='/'+data['image']['image_dir']+'/'+data['image']['image_item_ID_list']['small']['ID'];

                        if(isset($d('face_item_image_block_'+face_index))){

                            $s('face_item_image_block_'+face_index).background          ='url('+image_link+') 50% 50% no-repeat';
                            $s('face_item_image_block_'+face_index).backgroundSize      ='cover';

                        }

                        setTimeout(function(){

                            if(isset($d('face_item_image_block_'+face_index)))
                                $s('face_item_image_block_'+face_index).opacity=1;

                            if(isset($d('face_item_image_background_'+face_index)))
                                $s('face_item_image_background_'+face_index).opacity=0;


                        },300);

                        image_object.src            =image_link;
                        func                        =new Function("if(isset($d('dialog_face_item_image_block_"+face_index+"'))){$s('dialog_face_item_image_block_"+face_index+"').opacity=1;$s('dialog_face_item_image_background_"+face_index+"').opacity=0;}");
                        image_object.onload         =func;

                    }

                    if(isset($d('face_item_image_len_'+face_index)))
                        $d('face_item_image_len_'+face_index).innerHTML=data['facekit_image_len']+'/'+data['image_len'];

                    page_object.action.admin.data['face_data']['face_list'][face_index]['image_ID']             =data['image_ID'];
                    page_object.action.admin.data['face_data']['face_list'][face_index]['image_len']            =data['image_len'];
                    page_object.action.admin.data['face_data']['face_list'][face_index]['facekit_image_len']    =data['facekit_image_len'];
                    page_object.action.admin.data['face_data']['face_list'][face_index]['image']                =data['image'];

                    page_object.dialog.action.un_show.init(dialog_index,true);

                }
            },
            'add_photo':{
                'init':function(){

                    if(isset($d('dialog_face_add_image')))
                        $d('dialog_face_add_image').onclick=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.init;

                },
                'box':{
                    'dialog_index':null,
                    'width':400,
                    'isset_source_account_link':false,
                    'init':function(){

                        let  inner  ='';

                        inner+='<div id="dialog_face_image" class="dialog_row_block">';

                        inner+='<div id="dialog_face_image_block" style="margin: 0 0 0 0">';
                        inner+='<div id="dialog_face_image_item"></div>';
                        inner+='<div id="dialog_face_coords_list"></div>';
                        inner+='</div>';

                        inner+='<div id="dialog_face_image_label"></div>';

                        inner+='<div id="dialog_face_image_progress_block">';
                        inner+='<div id="dialog_face_image_progress_line"></div>';
                        inner+='</div>';

                        inner+='<div id="dialog_face_image_choose" class="dialog_face_image_choose">Загрузить изображение</div>';
                        inner+='<input type="file" id="dialog_face_image_file" style="display: none" />';

                        inner+='</div>';

                        page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.dialog_index=page_object.dialog.init({
                            'title':'Добавление нового лица',
                            'w':page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.width,
                            'inner':inner,
                            'on_create':function(){

                                $d('dialog_face_image_choose').onclick=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.upload.init;

                            },
                            'cancel':true
                        });

                    },
                    'upload':{
                        'init':function(){

                            $d('dialog_face_image_file').onchange=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.upload.on_change_file;

                            $d('dialog_face_image_file').click();

                        },
                        'on_change_file':function(dialog_index){

                            let  data
                                ,list           =[]
                                ,i
                                ,file_list      =this.files
                                ,list_item      ={};

                            if(file_list.length===0)
                                return false;

                            $s('dialog_face_image_label').display               ='block';
                            $d('dialog_face_image_label').innerHTML             ='Загрузка 1%';

                            $s('dialog_face_image_progress_block').display      ='block';
                            $s('dialog_face_image_progress_line').width         ='1%';

                            $d('dialog_face_image_choose').innerHTML            ='Подождите';
                            $d('dialog_face_image_choose').onclick              =function(){};

                            $d('dialog_face_image_item').display                ='none';
                            $d('dialog_face_image_item').innerHTML              ='';

                            $d('dialog_face_coords_list').innerHTML             ='';

                            $d('dialog_face_image_choose').setAttribute('class','dialog_face_image_choose_disable');
                            $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label');

                            for(i=0;i<file_list.length;i++){

                                list_item={
                                    'file_ID':null,
                                    'remove':false,
                                    'file':file_list[i]
                                };

                                list.push(list_item);

                            }

                            data={
                                'on_progress':page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.upload.on_progress.init,
                                'on_uploaded':page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.upload.on_uploaded.init,
                                'on_complete':page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.upload.on_complete.init,
                                'file_type':'image',
                                'list':list
                            };

                            page_object.upload.reset();

                            page_object.upload.init(dialog_index,data);

                            page_object.dialog.action.resize();

                        },
                        'on_progress':{
                            'init':function(){

                                if(isset($d('dialog_face_image_progress_line'))){

                                    let perc=(page_object.upload.data['list'][page_object.upload.file_item['index']]['percent']*100).toFixed(1);

                                    if((page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_index']+2)<page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_len'])
                                        $d('dialog_face_image_label').innerHTML='Загрузка '+perc+'%';
                                    else
                                        $d('dialog_face_image_label').innerHTML='Обработка изображения';

                                    $s('dialog_face_image_progress_line').width=perc+'%';

                                }

                            }
                        },
                        'on_uploaded':{
                            'init':function(){

                                if(isset($d('dialog_face_image_progress_line')))
                                    page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.init();

                            }
                        },
                        'on_complete':{
                            'init':function(){}
                        }
                    },
                    'face':{
                        'face_index':null,
                        'image_ID':null,
                        'image_data':null,
                        'file_ID':null,
                        'face_len':0,
                        'face_list':null,
                        'face_delta_x':1.6,
                        'face_delta_y':1.3,
                        'face_coords_list':null,
                        'face_image_min_w':70,
                        'face_image_min_h':70,
                        'init':function(){

                            if(isset($d('dialog_face_image_progress_block'))){

                                $s('dialog_face_image_progress_block').display  ='none';
                                $s('dialog_face_image_progress_line').width     =0;

                            }

                            if(isset($d('dialog_face_image_label')))
                                $d('dialog_face_image_label').innerHTML='Поиск лиц...';

                            page_object.dialog.action.resize();

                            var post='';

                            post+='image_ID='+uniEncode(page_object.upload.data['list'][page_object.upload.file_item['index']]['image_ID']);
                            post+='&file_ID='+uniEncode(page_object.upload.data['list'][page_object.upload.file_item['index']]['file_ID']);

                            send({
                                'scriptPath':'/api/json/get_faces_coords',
                                'postData':post,
                                'onComplete':function(j,worktime){

                                    let  data
                                        ,dataTemp;

                                    dataTemp=j.responseText;
                                    trace(dataTemp);
                                    data=jsonDecode(dataTemp);
                                    trace(data);
                                    trace_worktime(worktime,data);

                                    if(isset(data['error'])){

                                        if(isset($d('dialog_face_image_file'))){

                                            $d('dialog_face_image_choose').setAttribute('class','dialog_face_image_choose');

                                            $d('dialog_face_image_choose').innerHTML        ='Загрузить изображение';
                                            $d('dialog_face_image_choose').onclick          =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.upload.init;

                                        }

                                        if(isset($d('dialog_face_image_label'))){

                                            $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label_red');

                                            $d('dialog_face_image_label').innerHTML=stripSlashes(data['error']['info']);

                                            $d('dialog_face_image_file').value='';

                                        }

                                    }
                                    else{

                                        page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.file_ID         =data['data']['file_ID'];
                                        page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.image_ID        =data['data']['image_ID'];
                                        page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.image_data      =data['data']['image'];
                                        page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_len        =data['data']['face_len'];
                                        page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_list       =data['data']['face_list'];

                                        if(data['data']['face_len']===0){

                                            let dialog_index=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.dialog_index;

                                            if(isset($d('dialog_face_image_label'))){

                                                $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label_red');
                                                $d('dialog_face_image_label').innerHTML='Лица не найдены =/';

                                            }

                                            if(isset($d('dialog_face_image_choose'))){

                                                $d('dialog_face_image_choose').onclick=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.upload.init;

                                                $d('dialog_face_image_choose').innerHTML='Загрузить изображение';
                                                $d('dialog_face_image_choose').setAttribute('class','dialog_face_image_choose')

                                            }

                                            // upload image if not faces

                                            if(!isset($d('dialog_submit_add_'+dialog_index))){

                                                var el=addElement({
                                                    'tag':'div',
                                                    'id':'dialog_submit_add_'+dialog_index,
                                                    'class':'dialog_submit_add',
                                                    'inner':'Добавить'
                                                });

                                                $d('dialog_controls_'+dialog_index).insertBefore(el,$d('dialog_submit_cancel_'+dialog_index));

                                                el.onclick=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.add.init;

                                            }

                                            if(isset($d('dialog_face_image_block'))){

                                                let  image_data     =data['data']['image']
                                                    ,image_link     ='/'+image_data['image_dir']+'/'+image_data['image_item_ID_list']['middle']['ID']
                                                    ,box_width      =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.width
                                                    ,el;

                                                el=addElement({
                                                    'tag':'img',
                                                    'src':image_link,
                                                    'width':box_width
                                                });

                                                el.onload=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.show_face_list;

                                            }

                                            if(isset($d('dialog_face_image_label'))){

                                                $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label');
                                                $d('dialog_face_image_label').innerHTML='Загрузка изображения...';

                                            }



                                            // if(isset($d('dialog_face_image_file')))
                                            //     $d('dialog_face_image_file').value='';

                                            return true;

                                        }

                                        if(isset($d('dialog_face_image_block'))){

                                            let  image_data     =data['data']['image']
                                                ,image_link     ='/'+image_data['image_dir']+'/'+image_data['image_item_ID_list']['middle']['ID']
                                                ,box_width      =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.width
                                                ,el;

                                            el=addElement({
                                                'tag':'img',
                                                'src':image_link,
                                                'width':box_width
                                            });

                                            el.onload           =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.show_face_list;

                                        }

                                        if(isset($d('dialog_face_image_label'))){

                                            $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label');
                                            $d('dialog_face_image_label').innerHTML='Загрузка изображения...';

                                        }

                                    }

                                }
                            });

                        },
                        'show_face_list':function(){

                            if(isset($d('dialog_face_image_block')))
                                $s('dialog_face_image_block').display='block';

                            if(isset($d('dialog_face_image_label'))){

                                $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label');
                                $d('dialog_face_image_label').innerHTML='Выберите лицо';

                            }

                            if(isset($d('dialog_face_image_item'))){

                                $d('dialog_face_image_item').innerHTML='';
                                $d('dialog_face_image_item').appendChild(this);

                            }

                            if(isset($d('dialog_face_coords_list'))){

                                $d('dialog_face_coords_list').innerHTML='';

                                let  image_data             =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.image_data
                                    ,face_delta_x           =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_delta_x
                                    ,face_delta_y           =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_delta_y
                                    ,image_w                =image_data['image_item_ID_list']['middle']['width']
                                    ,image_h                =image_data['image_item_ID_list']['middle']['height']
                                    // ,box_width              =elementSize.width($d('root_search_face_image_block_container'))
                                    ,box_width              =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.width
                                    ,face_list              =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_list
                                    ,face_len               =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_len
                                    ,face_index
                                    ,face_w
                                    ,face_h
                                    ,face_x
                                    ,face_y
                                    ,face_w_new
                                    ,face_h_new
                                    ,face_x_new
                                    ,face_y_new
                                    ,image_w_new
                                    ,image_h_new
                                    ,coords
                                    ,face_check_len         =0
                                    ,perc                   =parseInt(box_width)/parseInt(image_w)
                                    ,el;

                                page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_coords_list=[];

                                for(face_index in face_list){

                                    coords              =face_list[face_index]['coords'];

                                    face_x              =coords['left'];
                                    face_y              =coords['top'];
                                    // face_w              =coords['right']-coords['left'];
                                    // face_h              =coords['bottom']-coords['top'];
                                    face_w              =coords['width'];
                                    face_h              =coords['height'];

                                    face_y              -=Math.ceil(face_h/3);
                                    face_h              +=Math.ceil(face_h/3);

                                    face_check_len++;

                                    face_w      =Math.ceil(face_w*perc);
                                    face_h      =Math.ceil(face_h*perc);

                                    face_x      =Math.ceil(face_x*perc);
                                    face_y      =Math.ceil(face_y*perc);

                                    face_w_new  =face_w*face_delta_x;
                                    face_h_new  =face_h*face_delta_y;

                                    face_x_new  =face_x-Math.ceil((face_w_new-face_w)/2);
                                    face_y_new  =face_y-Math.ceil((face_h_new-face_h)/2);

                                    if(face_x_new<0)
                                        face_x_new=0;

                                    if(face_y_new<0)
                                        face_y_new=0;

                                    image_w_new     =Math.ceil(image_w*perc);
                                    image_h_new     =Math.ceil(image_h*perc);

                                    if(face_x_new+face_w_new>image_w_new)
                                        face_w_new=image_w_new-face_x_new;

                                    if(face_y_new+face_h_new>image_h_new)
                                        face_h_new=image_h_new-face_y_new;

                                    page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_coords_list.push({
                                        'x':Math.ceil(face_x_new/perc),
                                        'y':Math.ceil(face_y_new/perc),
                                        'w':Math.ceil(face_w_new/perc),
                                        'h':Math.ceil(face_h_new/perc),
                                        'face_token':face_list[face_index]['face_token']
                                    });

                                    el=addElement({
                                        'tag':'div',
                                        'id':'dialog_face_image_item_'+face_index,
                                        'class':'dialog_face_image_item',
                                        'style':'width: '+face_w_new+'px; height: '+face_h_new+'px; margin: '+face_y_new+'px 0 0 '+face_x_new+'px;'
                                    });

                                    el.onclick=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.change_face;

                                    $d('dialog_face_coords_list').appendChild(el);

                                }

                                // if(face_len===1)
                                //     if(!is_null(face_list[0]['face_ID']))
                                //         if(face_list[0]['confidence']>70){
                                //
                                //             if(isset($d('dialog_face_image_label'))){
                                //
                                //                 $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label_red');
                                //                 $d('dialog_face_image_label').innerHTML='Лицо уже сущевтвует в системе #'+face_list[0]['face_ID'];
                                //
                                //             }
                                //
                                //             if(isset($d('dialog_face_image_block')))
                                //                 $s('dialog_face_image_block').display='none';
                                //
                                //             is_isset=true;
                                //
                                //         }
                                //
                                // if(!is_isset){
                                //
                                //     for(face_index in face_list){
                                //
                                //         is_face_checked     =false;
                                //
                                //         coords              =face_list[face_index]['coords'];
                                //
                                //         face_x              =coords['left'];
                                //         face_y              =coords['top'];
                                //         // face_w              =coords['right']-coords['left'];
                                //         // face_h              =coords['bottom']-coords['top'];
                                //         face_w              =coords['width'];
                                //         face_h              =coords['height'];
                                //
                                //         face_y              -=Math.ceil(face_h/3);
                                //         face_h              +=Math.ceil(face_h/3);
                                //
                                //         if(
                                //             // face_w>face_image_min_w
                                //         // &&face_h>face_image_min_h
                                //             true
                                //         ){
                                //
                                //             face_check_len++;
                                //
                                //             face_w      =Math.ceil(face_w*perc);
                                //             face_h      =Math.ceil(face_h*perc);
                                //
                                //             face_x      =Math.ceil(face_x*perc);
                                //             face_y      =Math.ceil(face_y*perc);
                                //
                                //             face_w_new  =face_w*face_delta_x;
                                //             face_h_new  =face_h*face_delta_y;
                                //
                                //             face_x_new  =face_x-Math.ceil((face_w_new-face_w)/2);
                                //             face_y_new  =face_y-Math.ceil((face_h_new-face_h)/2);
                                //
                                //             if(face_x_new<0)
                                //                 face_x_new=0;
                                //
                                //             if(face_y_new<0)
                                //                 face_y_new=0;
                                //
                                //             image_w_new     =Math.ceil(image_w*perc);
                                //             image_h_new     =Math.ceil(image_h*perc);
                                //
                                //             if(face_x_new+face_w_new>image_w_new)
                                //                 face_w_new=image_w_new-face_x_new;
                                //
                                //             if(face_y_new+face_h_new>image_h_new)
                                //                 face_h_new=image_h_new-face_y_new;
                                //
                                //             page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_coords_list.push({
                                //                 'x':Math.ceil(face_x_new/perc),
                                //                 'y':Math.ceil(face_y_new/perc),
                                //                 'w':Math.ceil(face_w_new/perc),
                                //                 'h':Math.ceil(face_h_new/perc),
                                //                 'face_token':face_list[face_index]['face_token']
                                //             });
                                //
                                //             el=addElement({
                                //                 'tag':'div',
                                //                 'id':'dialog_face_image_item_'+face_index,
                                //                 'class':'dialog_face_image_item',
                                //                 'style':'width: '+face_w_new+'px; height: '+face_h_new+'px; margin: '+face_y_new+'px 0 0 '+face_x_new+'px;'
                                //             });
                                //
                                //             if(!is_null(face_list['face_ID'])){
                                //
                                //                 el.setAttribute('title','Уже существует в системе');
                                //                 el.setAttribute('class','dialog_face_image_item_isset');
                                //
                                //             }
                                //             else
                                //                 el.onclick=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.change_face;
                                //
                                //             $d('dialog_face_coords_list').appendChild(el);
                                //
                                //         }
                                //
                                //     }
                                //
                                //     if(face_check_len===0){
                                //
                                //         if(isset($d('dialog_face_image_block')))
                                //             $s('dialog_face_image_block').display='none';
                                //
                                //         if(isset($d('dialog_face_image_item')))
                                //             $d('dialog_face_image_item').innerHTML='';
                                //
                                //         if(isset($d('dialog_face_coords_list')))
                                //             $d('dialog_face_coords_list').innerHTML='';
                                //
                                //         if(isset($d('dialog_face_image_label'))){
                                //
                                //             $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label_red');
                                //
                                //             if(face_len===1)
                                //                 $d('dialog_face_image_label').innerHTML='Слишком маленькое лицо на изображении';
                                //             else
                                //                 $d('dialog_face_image_label').innerHTML='Слишком маленькие лица на изображении';
                                //
                                //         }
                                //
                                //         if(isset($d('dialog_face_image_file')))
                                //             $d('dialog_face_image_file').value='';
                                //
                                //     }
                                //
                                // }

                            }

                            if(isset($d('dialog_face_image_choose'))){

                                $d('dialog_face_image_choose').onclick=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.upload.init;

                                $d('dialog_face_image_choose').innerHTML='Загрузить изображение';
                                $d('dialog_face_image_choose').setAttribute('class','dialog_face_image_choose')

                            }

                            if(isset($d('dialog_face_image_file')))
                                $d('dialog_face_image_file').value='';

                            page_object.dialog.action.resize();

                        },
                        'change_face':function(){

                            let  face_index             =parseInt(this.id.split('_')[4])
                                ,dialog_index           =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.dialog_index
                                ,el;

                            if(this.getAttribute('class')==='dialog_face_image_item_active'){

                                page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_index=null;

                                if(isset($d('dialog_face_image_label'))){

                                    $s('dialog_face_image_label').display       ='block';
                                    $d('dialog_face_image_label').innerHTML     ='Выберите лицо';

                                }

                                this.setAttribute('class','dialog_face_image_item');

                                if(isset($d('dialog_submit_add_'+dialog_index)))
                                    removeElement($d('dialog_submit_add_'+dialog_index));

                            }
                            else{

                                page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_index=face_index;

                                let  face_list=$d('dialog_face_coords_list').getElementsByTagName('div')
                                    ,face_item_index;

                                if(isset($d('dialog_face_image_label')))
                                    $s('dialog_face_image_label').display='none';

                                for(face_item_index in face_list)
                                    if(isObject(face_list[face_item_index]))
                                        face_list[face_item_index].setAttribute('class','dialog_face_image_item');

                                this.setAttribute('class','dialog_face_image_item_active');

                                if(!isset($d('dialog_submit_add_'+dialog_index))){

                                    el=addElement({
                                        'tag':'div',
                                        'id':'dialog_submit_add_'+dialog_index,
                                        'class':'dialog_submit_add',
                                        'inner':'Добавить'
                                    });

                                    $d('dialog_controls_'+dialog_index).insertBefore(el,$d('dialog_submit_cancel_'+dialog_index));

                                    el.onclick=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.add.init;
                                    
                                }

                                if(isset($d('dialog_face_image_change')))
                                    $d('dialog_face_image_change').onclick=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.change_image;

                            }

                            page_object.dialog.action.resize();

                        },
                        'change_image':function(){

                            if(isset($d('dialog_face_image')))
                                $s('dialog_face_image').display='block';

                            if(isset($d('dialog_face_data')))
                                $s('dialog_face_data').display='none';

                            let dialog_index=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.dialog_index;

                            if(isset($d('dialog_submit_add_'+dialog_index)))
                                removeElement($d('dialog_submit_add_'+dialog_index));

                            page_object.dialog.action.resize();

                        }
                    }
                },
                'add':{
                    'init':function(){

                        page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.add.send();

                    },
                    'send':function(){
    
                        var  lang_obj           =page_object.action.admin.content[page_object.lang]
                            ,dialog_index       =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.dialog_index
                            ,coords_list        =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_coords_list
                            ,coords_index       =page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.face_index;
    
                        if(isset($d('dialog_submit_add_'+dialog_index))){
    
                            $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_disable');
    
                            $d('dialog_submit_add_'+dialog_index).innerHTML    =lang_obj['wait'];
                            $d('dialog_submit_add_'+dialog_index).onclick      =function(){};
    
                        }
    
                        if(isset($d('dialog_face_image_change'))){
    
                            $d('dialog_face_image_change').setAttribute('class','dialog_face_image_choose_disable');
    
                            $d('dialog_face_image_change').onclick=function(){};
    
                        }
    
                        if(isset($d('dialog_submit_cancel_'+dialog_index))){
    
                            $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');
    
                            $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};
    
                        }
    
                        let post='';
    
                        post+='face_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.edit_photo.box.face_ID);
                        post+='&file_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.file_ID);
                        post+='&image_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.image_ID);
                        post+='&coords='+uniEncode(jsonEncode(coords_list));
                        post+='&face_token='+(coords_list.length>0?uniEncode(coords_list[coords_index]['face_token']):'');
    
                        send({
                            'scriptPath':'/api/json/add_face_image',
                            'postData':post,
                            'onComplete':function(j,worktime){
    
                                let  dataTemp
                                    ,data;
    
                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);
    
                                if(isset(data['error'])){
    
                                    if($d('dialog_submit_add_'+dialog_index)){
    
                                        $d('dialog_submit_add_'+dialog_index).onclick=function(){
    
                                            page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.add.init();
    
                                        };
                                        $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_add');
                                        $d('dialog_submit_add_'+dialog_index).innerHTML=lang_obj['add'];
    
                                    }
    
                                    if($d('dialog_submit_cancel_'+dialog_index)){
    
                                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){
    
                                            page_object.dialog.action.un_show.init(dialog_index,true);
    
                                        };
                                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');
    
                                    }
    
                                    if(isset($d('dialog_face_image_change'))){
    
                                        $d('dialog_face_image_change').setAttribute('class','dialog_face_image_choose');
    
                                        $d('dialog_face_image_change').onclick=page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.change_image;
    
                                    }
    
                                    if(isset($d('dialog_face_image_label'))){
    
                                        $d('dialog_face_image_label').setAttribute('class','dialog_face_image_label_red');
    
                                        $d('dialog_face_image_label').innerHTML=stripSlashes(data['error']['data']['info']);
    
                                    }
    
                                    if(isset($d('dialog_face_image_file')))
                                        $d('dialog_face_image_file').value='';
    
                                    page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.box.face.change_image();
    
                                }
                                else
                                    page_object.action.admin.action.face_item_data.action.edit_photo.add_photo.add.prepare(dialog_index,data);
    
                            }
                        });
    
                    },
                    'prepare':function(dialog_index,face_data){
    
                        var  image_index=0
                            ,el
                            ,inner;

                        page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list.unshift(face_data['data']);
    
                        inner=page_object.action.admin.create.face.create.get_face_item_image_row(page_object.action.admin.action.face_item_data.action.edit_photo.box.image_ID,page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list,0)
    
                        page_object.action.admin.action.face_item_data.action.rename_image.move_down();

                        el=addElement({
                            'tag':'div',
                            'id':'dialog_face_item_'+image_index,
                            'class':'dialog_face_item_row',
                            'style':'opacity: 0; height: 0;',
                            'inner':inner
                        });
    
                        if(page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list.length===1)
                            $d('dialog_face_image_list').appendChild(el);
                        else
                            $d('dialog_face_image_list').insertBefore(el,$d('dialog_face_item_1'));
    
                        setTimeout(function(){
    
                            if(isset($d('dialog_face_item_'+image_index))){
    
                                $s('dialog_face_item_'+image_index).opacity     =1;
                                $s('dialog_face_item_'+image_index).height      ='150px';
    
                            }

                            let  image_list=page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list
                                ,index;

                            page_object.dialog.action.resize();

                            for(index in image_list)
                                if(isset($d('dialog_face_item_remove_'+index)))
                                    $d('dialog_face_item_remove_'+index).onclick=page_object.action.admin.action.face_item_data.action.edit_photo.box.remove.init;

                            page_object.action.admin.action.face_item_data.action.edit_photo.box.checkbox.init();
                            page_object.action.admin.action.face_item_data.action.edit_photo.box.image.init();
                            page_object.action.admin.action.face_item_data.action.edit_photo.box.remove.init();

                            if(page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list.length<4){

                                $s('dialog_face_image_list').overflow   ='hidden';
                                $s('dialog_face_image_list').height     =$d('dialog_face_image_list').scrollHeight+'px';

                                if(page_object.action.admin.action.face_item_data.action.edit_photo.box.image_list.length===1)
                                    if(isset($d('dialog_face_item_controls_0')))
                                        $s('dialog_face_item_controls_0').display='none';

                            }
                            else{

                                $s('dialog_face_image_list').overflowY  ='auto';
                                $s('dialog_face_image_list').height     ='500px';

                            }

                            page_object.dialog.action.resize();
    
                        },500);

                        page_object.dialog.action.un_show.init(dialog_index,true);
    
                    }
                }
            }
        },
        'edit_video':{
            'init':function(){

                let  list           =page_object.action.admin.data['face_data']['face_list']
                    ,index;

                for(index in list)
                    if(isset($d('face_item_video_'+index)))
                        $d('face_item_video_'+index).onclick=page_object.action.admin.action.face_item_data.action.edit_video.box.init;

            },
            'box':{
                'dialog_index':null,
                'video_list':null,
                'video_index':null,
                'face_ID':null,
                'video_ID':null,
                'face_index':null,
                'width':840,
                'init':function(){

                    page_object.action.admin.action.face_item_data.action.edit_video.box.face_ID        =null;
                    page_object.action.admin.action.face_item_data.action.edit_video.box.video_ID       =null;
                    page_object.action.admin.action.face_item_data.action.edit_video.box.face_index     =null;
                    page_object.action.admin.action.face_item_data.action.edit_video.box.dialog_index   =null;
                    page_object.action.admin.action.face_item_data.action.edit_video.box.video_list     =null;

                    let  inner              =''
                        ,face_index         =this.id.split('_')[3]
                        ,data               =page_object.action.admin.data['face_data']
                        ,face_list          =data['face_list']
                        ,face_data          =face_list[face_index];

                    page_object.action.admin.action.face_item_data.action.edit_video.box.face_index     =face_index;
                    page_object.action.admin.action.face_item_data.action.edit_video.box.face_ID        =face_list[face_index]['ID'];

                    inner+='<div class="dialog_row">';

                        inner+='<div id="dialog_face_video_load">Получение списка изображений...</div>';

                        // if(page_object.link.link_list[1]==='root')
                        inner+='<div id="dialog_face_add_video">Добавить видео</div>';

                        inner+='<div id="dialog_face_item_header">';

                            inner+='<div class="face_header_col" style="width: 50px;">ID</div>';
                            inner+='<div class="face_header_col" style="width: 580px;">Видео</div>';
                            // inner+='<div class="face_header_col">Размер</div>';
                            // inner+='<div class="face_header_col">Место</div>';
                            inner+='<div class="face_header_col">Дата</div>';
                            inner+='<div class="face_header_col" style="width: 30px"></div>';

                        inner+='</div>';

                        inner+='<div id="dialog_face_video_list" style="max-height: calc(100vh - 200px)"></div>';

                    inner+='</div>';

                    page_object.action.admin.action.face_item_data.action.edit_video.box.dialog_index=page_object.dialog.init({
                        'title':'Редактирование видео: '+stripSlashes(face_data['name'])+' #'+face_data['ID'],
                        'w':page_object.action.admin.action.face_item_data.action.edit_video.box.width,
                        'inner':inner,
                        'save':function(){

                            page_object.action.admin.action.face_item_data.action.edit_video.edit.init(page_object.action.admin.action.face_item_data.action.edit_face.box.dialog_index);

                        },
                        'on_create':function(){

                            page_object.action.admin.action.face_item_data.action.edit_video.box.load();

                        },
                        'cancel':true
                    });

                },
                'load':function(){

                    var  dialog_index       =page_object.action.admin.action.face_item_data.action.edit_video.box.dialog_index
                        ,face_ID            =page_object.action.admin.action.face_item_data.action.edit_video.box.face_ID
                        ,lang_obj           =page_object.action.admin.content[page_object.lang]
                        ,post               ='face_ID='+uniEncode(face_ID);

                    if(isset($d('dialog_submit_save_'+dialog_index))){

                        $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_save_'+dialog_index).innerHTML    =lang_obj['wait'];
                        $d('dialog_submit_save_'+dialog_index).onclick      =function(){};

                    }

                    if(isset($d('dialog_submit_cancel_'+dialog_index))){

                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    }

                    send({
                        'scriptPath':'/api/json/get_face_video_list',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if($d('dialog_submit_save_'+dialog_index)){

                                    $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                        page_object.action.admin.action.face_item_data.action.edit_video.edit.init();

                                    };
                                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                    $d('dialog_submit_save_'+dialog_index).innerHTML=lang_obj['save'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                            }
                            else
                                page_object.action.admin.action.face_item_data.action.edit_video.box.prepare(dialog_index,data['data']);

                        }
                    });

                },
                'prepare':function(dialog_index,data){

                    let  video_list         =data['video_list']
                        ,video_index
                        ,video_ID           =data['video_ID']
                        ,inner              ='';

                    page_object.action.admin.action.face_item_data.action.edit_video.box.video_list=video_list;

                    for(video_index in video_list){

                        inner+='<div id="dialog_face_item_'+video_index+'" class="dialog_face_item_row" style="height: auto;">';

                        if(parseInt(video_ID)===parseInt(video_list[video_index]['video_ID'])){

                            page_object.action.admin.action.face_item_data.action.edit_video.box.video_index    =video_index;
                            page_object.action.admin.action.face_item_data.action.edit_video.box.video_ID       =parseInt(video_ID);

                        }

                        inner+=page_object.action.admin.create.face.create.get_face_item_video_row(video_ID,video_list,video_index);

                        inner+='</div>';

                    }

                    if(isset($d('dialog_face_video_list')))
                        $d('dialog_face_video_list').innerHTML=inner;

                    if(isset($d('dialog_face_add_video'))){

                        $s('dialog_face_add_video').display='inline-block';

                        page_object.action.admin.action.face_item_data.action.edit_video.add_video.init();

                    }

                    if(page_object.action.admin.action.face_item_data.action.edit_video.box.video_list.length<2){

                        $s('dialog_face_video_list').overflow   ='hidden';
                        $s('dialog_face_video_list').height     ='auto';

                        page_object.dialog.action.resize();

                    }

                    if(isset($d('dialog_face_item_header')))
                        $s('dialog_face_item_header').display='block';

                    if(isset($d('dialog_face_video_load')))
                        $d('dialog_face_video_load').innerHTML='';

                    for(video_index in video_list)
                        if(isset($d('dialog_face_item_remove_'+video_index)))
                            $d('dialog_face_item_remove_'+video_index).onclick=page_object.action.admin.action.face_item_data.action.edit_video.box.remove.init;

                    page_object.action.admin.action.face_item_data.action.edit_video.box.remove.init();

                    page_object.dialog.action.resize();

                },
                'remove':{
                    'init':function(){

                        let  video_index
                            ,video_list     =page_object.action.admin.action.face_item_data.action.edit_video.box.video_list;

                        for(video_index in video_list)
                            if(isset($d('dialog_face_item_remove_'+video_index)))
                                $d('dialog_face_item_remove_'+video_index).onclick=page_object.action.admin.action.face_item_data.action.edit_video.box.remove.box;

                    },
                    'box':function(){

                        var  video_index        =this.id.split('_')[4]
                            ,video_list         =page_object.action.admin.action.face_item_data.action.edit_video.box.video_list
                            ,video_data         =video_list[video_index]
                            ,inner              ='';

                        inner+='<div class="dialog_row">';
                        inner+='Удалить видео #'+video_data['video_ID']+'?';
                        inner+='</div>';

                        var dialog_index=page_object.dialog.init({
                            'title':'Удаление видео',
                            'inner':inner,
                            'remove':function(){

                                page_object.action.admin.action.face_item_data.action.edit_video.box.remove.prepare(dialog_index,video_index);

                            },
                            'cancel':true
                        });

                    },
                    'prepare':function(dialog_index,video_index){

                        if(isset($d('dialog_face_item_'+video_index)))
                            removeElement($d('dialog_face_item_'+video_index));

                        page_object.action.admin.action.face_item_data.action.edit_video.box.video_list.splice(video_index,1);

                        page_object.action.admin.action.face_item_data.action.rename_video.move_up(video_index);

                        page_object.action.admin.action.face_item_data.action.edit_video.box.remove.init();

                        if(page_object.action.admin.action.face_item_data.action.edit_video.box.video_list.length<2){

                            $s('dialog_face_video_list').overflow   ='hidden';
                            $s('dialog_face_video_list').height     ='auto';

                            page_object.dialog.action.resize();

                        }

                        page_object.dialog.action.un_show.init(dialog_index,true);

                    }
                }
            },
            'edit':{
                'init':function(){

                    page_object.action.admin.action.face_item_data.action.edit_video.edit.send();

                },
                'send':function(){

                    let  video_list         =page_object.action.admin.action.face_item_data.action.edit_video.box.video_list
                        ,video_index
                        ,lang_obj           =page_object.action.admin.content[page_object.lang]
                        ,dialog_index       =page_object.action.admin.action.face_item_data.action.edit_video.box.dialog_index
                        ,face_ID            =page_object.action.admin.action.face_item_data.action.edit_video.box.face_ID
                        ,face_index         =page_object.action.admin.action.face_item_data.action.edit_video.box.face_index
                        ,video_ID           =page_object.action.admin.action.face_item_data.action.edit_video.box.video_ID
                        ,video_ID_list      =[]
                        ,post               ='';

                    for(video_index in video_list)
                        video_ID_list.push(video_list[video_index]['video_ID']);

                    post+='face_ID='+uniEncode(face_ID);
                    post+='&video_ID='+uniEncode(video_ID);
                    post+='&video_ID_list='+uniEncode(jsonEncode(video_ID_list));

                    if(isset($d('dialog_submit_save_'+dialog_index))){

                        $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_save_'+dialog_index).innerHTML    =lang_obj['wait'];
                        $d('dialog_submit_save_'+dialog_index).onclick      =function(){};

                    }

                    if(isset($d('dialog_submit_cancel_'+dialog_index))){

                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    }

                    send({
                        'scriptPath':'/api/json/save_face_video_list',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if($d('dialog_submit_save_'+dialog_index)){

                                    $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                        page_object.action.admin.action.face_item_data.action.edit_video.edit.init();

                                    };
                                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                    $d('dialog_submit_save_'+dialog_index).innerHTML=lang_obj['save'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                            }
                            else
                                page_object.action.admin.action.face_item_data.action.edit_video.edit.prepare(data['data']);

                        }
                    });

                },
                'prepare':function(data){

                    let  dialog_index       =page_object.action.admin.action.face_item_data.action.edit_video.box.dialog_index
                        ,face_index         =page_object.action.admin.action.face_item_data.action.edit_video.box.face_index;

                    page_object.action.admin.data['face_data']['face_list'][face_index]['video_len']=data['video_len'];

                    if(isset($d('face_item_video_'+face_index))){

                        if(parseInt(data['video_len'])===0)
                            $d('face_item_video_'+face_index).setAttribute('class','face_item_video_disable');
                        else
                            $d('face_item_video_'+face_index).setAttribute('class','face_item_video');

                    }

                    page_object.dialog.action.un_show.init(dialog_index,true);

                }
            },
            'add_video':{
                'init':function(){

                    if(isset($d('dialog_face_add_video')))
                        $d('dialog_face_add_video').onclick=page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.init;

                },
                'box':{
                    'dialog_index':null,
                    'width':600,
                    'isset_source_account_link':false,
                    'init':function(){

                        let  inner  ='';

                        inner+='<div id="dialog_face_video" class="dialog_row_block">';

                        inner+='<div id="dialog_face_video_block" style="margin: 0 0 0 0">';
                        inner+='<div id="dialog_face_video_item"></div>';
                        inner+='</div>';

                        inner+='<div id="dialog_face_video_label"></div>';

                        inner+='<div id="dialog_face_video_progress_block">';
                        inner+='<div id="dialog_face_video_progress_line"></div>';
                        inner+='</div>';

                        inner+='<div id="dialog_face_video_choose" class="dialog_face_video_choose">Загрузить видео</div>';
                        inner+='<input type="file" id="dialog_face_video_file" style="display: none" accept=".mp4" />';

                        inner+='</div>';

                        page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.dialog_index=page_object.dialog.init({
                            'title':'Добавление нового видео',
                            'w':page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.width,
                            'inner':inner,
                            'on_create':function(){

                                if(isset($d('dialog_face_video_choose')))
                                    $d('dialog_face_video_choose').onclick=page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.upload.init;

                            },
                            'cancel':true
                        });

                    },
                    'upload':{
                        'init':function(){

                            $d('dialog_face_video_file').onchange=page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.upload.on_change_file;

                            $d('dialog_face_video_file').click();

                        },
                        'on_change_file':function(dialog_index){

                            let  data
                                ,list           =[]
                                ,i
                                ,file_list      =this.files
                                ,list_item      ={};

                            if(file_list.length===0)
                                return false;

                            $s('dialog_face_video_label').display               ='block';
                            $d('dialog_face_video_label').innerHTML             ='Загрузка 1%';

                            $s('dialog_face_video_progress_block').display      ='block';
                            $s('dialog_face_video_progress_line').width         ='1%';

                            $d('dialog_face_video_choose').innerHTML            ='Подождите';
                            $d('dialog_face_video_choose').onclick              =function(){};

                            $d('dialog_face_video_item').display                ='none';
                            $d('dialog_face_video_item').innerHTML              ='';

                            $d('dialog_face_video_choose').setAttribute('class','dialog_face_video_choose_disable');
                            $d('dialog_face_video_label').setAttribute('class','dialog_face_video_label');

                            for(i=0;i<file_list.length;i++){

                                list_item={
                                    'file_ID':null,
                                    'remove':false,
                                    'file':file_list[i]
                                };

                                list.push(list_item);

                            }

                            data={
                                'on_progress':page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.upload.on_progress.init,
                                'on_uploaded':page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.upload.on_uploaded.init,
                                'on_complete':page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.upload.on_complete.init,
                                'file_type':'video',
                                'list':list
                            };

                            page_object.upload.reset();

                            page_object.upload.init(dialog_index,data);

                            page_object.dialog.action.resize();

                        },
                        'on_progress':{
                            'init':function(){

                                if(isset($d('dialog_face_video_progress_line'))){

                                    let perc=(page_object.upload.data['list'][page_object.upload.file_item['index']]['percent']*100).toFixed(1);

                                    if((page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_index']+2)<page_object.upload.data['list'][page_object.upload.file_item['index']]['chunk_len'])
                                        $d('dialog_face_video_label').innerHTML='Загрузка '+perc+'%';
                                    else
                                        $d('dialog_face_video_label').innerHTML='Обработка изображения';

                                    $s('dialog_face_video_progress_line').width=perc+'%';

                                }

                            }
                        },
                        'on_uploaded':{
                            'init':function(index){

                                if(isset($d('dialog_face_video_progress_line')))
                                    page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.prepare.init(index);

                            }
                        },
                        'on_complete':{
                            'init':function(){}
                        }
                    },
                    'prepare':{
                        'file_ID':null,
                        'video_ID':null,
                        'video_path':null,
                        'init':function(index){

                            let  data           =page_object.upload.data['list'][index]
                                ,video_path     =data['video_path']
                                ,dialog_index   =page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.dialog_index
                                ,el
                                ,inner          ='';

                            if(isset($d('dialog_face_video_progress_block'))){

                                $s('dialog_face_video_progress_block').display  ='none';
                                $s('dialog_face_video_progress_line').width     =0;

                            }

                            if(isset($d('dialog_face_video_label')))
                                $d('dialog_face_video_label').innerHTML='';

                            page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.prepare.file_ID         =data['file_ID'];
                            page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.prepare.video_ID        =data['video_ID'];
                            page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.prepare.video_path      =data['video_path'];

                            inner+='<video height="320" controls=""><source src="/'+video_path+'"></video>';

                            if(isset($d('dialog_face_video_item')))
                                $d('dialog_face_video_item').innerHTML=inner;

                            if(!isset($d('dialog_submit_add_'+dialog_index))){

                                el=addElement({
                                    'tag':'div',
                                    'id':'dialog_submit_add_'+dialog_index,
                                    'class':'dialog_submit_add',
                                    'inner':'Добавить видео в профиль'
                                });

                                $d('dialog_controls_'+dialog_index).insertBefore(el,$d('dialog_submit_cancel_'+dialog_index));

                                el.onclick=page_object.action.admin.action.face_item_data.action.edit_video.add_video.add.init;

                            }

                            if(isset($d('dialog_face_video_choose'))){

                                $d('dialog_face_video_choose').onclick      =page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.change_video;
                                $d('dialog_face_video_choose').innerHTML    ='Загрузить другое видео';

                                $d('dialog_face_video_choose').setAttribute('class','dialog_face_video_choose');

                            }

                            page_object.dialog.action.resize();

                        }
                    },
                    'change_video':function(){

                        if(isset($d('dialog_face_video')))
                            $s('dialog_face_video').display='block';

                        if(isset($d('dialog_face_data')))
                            $s('dialog_face_data').display='none';

                        let dialog_index=page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.dialog_index;

                        if(isset($d('dialog_submit_add_'+dialog_index)))
                            removeElement($d('dialog_submit_add_'+dialog_index));

                        page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.upload.init();

                        page_object.dialog.action.resize();

                    }
                },
                'add':{
                    'init':function(){

                        page_object.action.admin.action.face_item_data.action.edit_video.add_video.add.send();

                    },
                    'send':function(){
    
                        var  lang_obj           =page_object.action.admin.content[page_object.lang]
                            ,dialog_index       =page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.dialog_index;
    
                        if(isset($d('dialog_submit_add_'+dialog_index))){
    
                            $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_disable');
    
                            $d('dialog_submit_add_'+dialog_index).innerHTML    =lang_obj['wait'];
                            $d('dialog_submit_add_'+dialog_index).onclick      =function(){};
    
                        }
    
                        if(isset($d('dialog_face_video_change'))){
    
                            $d('dialog_face_video_change').setAttribute('class','dialog_face_video_choose_disable');
    
                            $d('dialog_face_video_change').onclick=function(){};
    
                        }
    
                        if(isset($d('dialog_submit_cancel_'+dialog_index))){
    
                            $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');
    
                            $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};
    
                        }
    
                        let post='';
    
                        post+='face_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.edit_video.box.face_ID);
                        post+='&file_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.prepare.file_ID);
                        post+='&video_ID='+uniEncode(page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.prepare.video_ID);
    
                        send({
                            'scriptPath':'/api/json/add_face_video',
                            'postData':post,
                            'onComplete':function(j,worktime){
    
                                let  dataTemp
                                    ,data;
    
                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);
    
                                if(isset(data['error'])){
    
                                    if($d('dialog_submit_add_'+dialog_index)){
    
                                        $d('dialog_submit_add_'+dialog_index).onclick=function(){
    
                                            page_object.action.admin.action.face_item_data.action.edit_video.add_video.add.init();
    
                                        };

                                        $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_add');

                                        $d('dialog_submit_add_'+dialog_index).innerHTML='Добавить видео в профиль';
    
                                    }
    
                                    if($d('dialog_submit_cancel_'+dialog_index)){
    
                                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){
    
                                            page_object.dialog.action.un_show.init(dialog_index,true);
    
                                        };
                                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');
    
                                    }
    
                                    if(isset($d('dialog_face_video_change'))){
    
                                        $d('dialog_face_video_change').setAttribute('class','dialog_face_video_choose');
    
                                        $d('dialog_face_video_change').onclick=page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.face.change_video;
    
                                    }
    
                                    if(isset($d('dialog_face_video_label'))){
    
                                        $d('dialog_face_video_label').setAttribute('class','dialog_face_video_label_red');
    
                                        $d('dialog_face_video_label').innerHTML=stripSlashes(data['error']['data']['info']);
    
                                    }
    
                                    if(isset($d('dialog_face_video_file')))
                                        $d('dialog_face_video_file').value='';
    
                                    page_object.action.admin.action.face_item_data.action.edit_video.add_video.box.face.change_video();
    
                                }
                                else
                                    page_object.action.admin.action.face_item_data.action.edit_video.add_video.add.prepare(dialog_index,data);
    
                            }
                        });
    
                    },
                    'prepare':function(dialog_index,face_data){
    
                        var  video_index=0
                            ,el
                            ,inner;

                        page_object.action.admin.action.face_item_data.action.edit_video.box.video_list.unshift(face_data['data']);

                        inner=page_object.action.admin.create.face.create.get_face_item_video_row(page_object.action.admin.action.face_item_data.action.edit_video.box.video_ID,page_object.action.admin.action.face_item_data.action.edit_video.box.video_list,0)

                        page_object.action.admin.action.face_item_data.action.rename_video.move_down();

                        el=addElement({
                            'tag':'div',
                            'id':'dialog_face_item_'+video_index,
                            'class':'dialog_face_item_row',
                            'style':'opacity: 0; height: 0;',
                            'inner':inner
                        });
    
                        if(page_object.action.admin.action.face_item_data.action.edit_video.box.video_list.length===1)
                            $d('dialog_face_video_list').appendChild(el);
                        else
                            $d('dialog_face_video_list').insertBefore(el,$d('dialog_face_item_1'));

                        setTimeout(function(){
    
                            if(isset($d('dialog_face_item_'+video_index))){
    
                                $s('dialog_face_item_'+video_index).opacity     =1;
                                $s('dialog_face_item_'+video_index).height      ='auto';
    
                            }

                            let  video_list=page_object.action.admin.action.face_item_data.action.edit_video.box.video_list
                                ,index;

                            page_object.dialog.action.resize();

                            for(index in video_list)
                                if(isset($d('dialog_face_item_remove_'+index)))
                                    $d('dialog_face_item_remove_'+index).onclick=page_object.action.admin.action.face_item_data.action.edit_video.box.remove.init;

                            page_object.action.admin.action.face_item_data.action.edit_video.box.remove.init();

                            if(page_object.action.admin.action.face_item_data.action.edit_video.box.video_list.length<4){

                                $s('dialog_face_video_list').overflow   ='hidden';
                                $s('dialog_face_video_list').height     =$d('dialog_face_video_list').scrollHeight+'px';

                            }
                            else{

                                $s('dialog_face_video_list').overflowY  ='auto';
                                $s('dialog_face_video_list').height     ='500px';

                            }

                            page_object.dialog.action.resize();

                        },500);

                        page_object.dialog.action.un_show.init(dialog_index,true);
    
                    }
                }
            }
        },
        'remove_face':{
            'init':function(){

                let  list           =page_object.action.admin.data['face_data']['face_list']
                    ,index;

                for(index in list)
                    if(isset($d('face_item_remove_'+index)))
                        $d('face_item_remove_'+index).onclick=page_object.action.admin.action.face_item_data.action.remove_face.box.init;

            },
            'box':{

                'init':function(){

                    let  inner          =''
                        ,index          =this.id.split('_')[3]
                        ,data           =page_object.action.admin.data['face_data']
                        ,face_data      =data['face_list'][index];

                    inner+='<div class="dialog_row">';
                        inner+='Удалить лицо #'+face_data['ID']+'?';
                    inner+='</div>';

                    var dialog_index=page_object.dialog.init({
                        'title':'Удаление лица',
                        'inner':inner,
                        'remove':function(){

                            page_object.action.admin.action.face_item_data.action.remove_face.remove.init(dialog_index,index);

                        },
                        'on_create':function(){

                        },
                        'cancel':true
                    });

                }
            },
            'remove':{
                'init':function(dialog_index,index){
                    
                    page_object.action.admin.action.face_item_data.action.remove_face.remove.send(dialog_index,index);

                },
                'send':function(dialog_index,index){

                    var lang_obj=page_object.action.admin.content[page_object.lang];

                    if(isset($d('dialog_submit_remove_'+dialog_index))){

                        $d('dialog_submit_remove_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_remove_'+dialog_index).innerHTML    =lang_obj['wait'];
                        $d('dialog_submit_remove_'+dialog_index).onclick      =function(){};

                    }

                    if(isset($d('dialog_submit_cancel_'+dialog_index))){

                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    }

                    let  post       =''
                        ,list       =page_object.action.admin.data['face_data']['face_list']
                        ,data       =list[index];

                    post+='face_ID='+uniEncode(data['ID']);

                    send({
                        'scriptPath':'/api/json/remove_face',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if($d('dialog_submit_remove_'+dialog_index)){

                                    $d('dialog_submit_remove_'+dialog_index).onclick=function(){

                                        page_object.action.admin.action.face_item_data.action.remove_face.remove.init(dialog_index,index);

                                    };
                                    $d('dialog_submit_remove_'+dialog_index).setAttribute('class','dialog_submit_remove');
                                    $d('dialog_submit_remove_'+dialog_index).innerHTML=lang_obj['remove'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                            }
                            else
                                page_object.action.admin.action.face_item_data.action.remove_face.remove.prepare(dialog_index,index);

                        }
                    });

                },
                'prepare':function(dialog_index,index){

                    if(isset($d('face_item_'+index)))
                        removeElement($d('face_item_'+index));

                    page_object.action.admin.data['face_data']['face_list'].splice(index,1);

                    page_object.action.admin.action.face_item_data.action.rename_face.move_up(index);

                    page_object.action.admin.action.face_item_data.action.init();

                    page_object.dialog.action.un_show.init(dialog_index,true);

                }
            }
        },
        'scroll':{
            'event':null,
            'is_work':false,
            'init':function(){

                if(isset( page_object.action.admin.action.face_item_data.action.scroll.event))
                     page_object.action.admin.action.face_item_data.action.scroll.remove_listener();

                 page_object.action.admin.action.face_item_data.action.scroll.event=listener.set(window,'scroll',function(event){

                     page_object.action.admin.action.face_item_data.action.scroll.send(event);

                });

            },
            'send':function(e){

                if(isset($d('face_item')))
                    if(!page_object.action.admin.action.face_item_data.action.scroll.is_work){

                         page_object.action.admin.action.face_item_data.action.scroll.is_work=true;

                        let  scroll_position        =get_document_scroll_position()
                            ,win_height             =winSize.winHeight
                            ,doc_height             =document.documentElement.scrollHeight
                            ,delta                  =doc_height-win_height-scroll_position;

                        if(delta>10){

                             page_object.action.admin.action.face_item_data.action.scroll.is_work=false;

                            return false;

                        }

                        let  face_list              =page_object.action.admin.data['face_data']['face_list']
                            ,face_len               =face_list.length
                            ,face_data
                            ,post                   ='';

                        if(face_list.length===0)
                            return false;

                        if(!empty(face_len))
                            post+='&face_len='+uniEncode(face_len);

                        // face_data=face_list[face_list.length-1];
                        //
                        // post+='face_ID='+uniEncode(face_data['ID']);

                        if(page_object.action.admin.action.face_item_data.action.filter.is_open){

                            if(!empty($v('face_filter_source')))
                                post+='&source_ID='+uniEncode($v('face_filter_source'));

                            if(!empty($v('face_filter_level')))
                                post+='&level='+uniEncode($v('face_filter_level'));

                            if(!empty($v('face_filter_face_ID')))
                                post+='&face_ID='+uniEncode($v('face_filter_face_ID'));

                            if(!empty($v('face_filter_city'))){

                                let  city_list  =$v('face_filter_city').split(',')
                                    ,city_len   =city_list.length;

                                if(city_len>0)
                                    post+='&city='+uniEncode(city_list[city_len-1].trim());

                            }

                            if(!empty($v('face_filter_link')))
                                post+='&source_link='+uniEncode($v('face_filter_link'));

                            if(!empty($v('face_filter_image_sort_count'))){

                                post+='&sort_by=image_count';
                                post+='&sort_direction='+uniEncode(parseInt($v('face_filter_image_sort_count'))===1?'asc':'desc');

                            }

                            if($d('face_filter_check_fpp').getAttribute('class')==='face_filter_check_active')
                                post+='&is_uploaded_image=1';

                            if($d('face_filter_check_face_count').getAttribute('class')==='face_filter_check_active')
                                post+='&is_not_one_face=1';

                            if($d('face_filter_check_not_city').getAttribute('class')==='face_filter_check_active')
                                post+='&is_not_city=1';

                            if($d('face_filter_check_not_age').getAttribute('class')==='face_filter_check_active')
                                post+='&is_not_age=1';

                        }
                        else{

                            let  city                   =get_url_param('city')
                                ,source_ID              =get_url_param('source_ID')
                                ,source_link            =get_url_param('source_link')
                                ,is_uploaded_image      =get_url_param('is_uploaded_image')
                                ,sort_by                =get_url_param('sort_by')
                                ,sort_direction         =get_url_param('sort_direction')
                                ,is_not_one_face        =get_url_param('is_not_one_face');

                            if(!empty(city))
                                post+='&city='+uniEncode(city);

                            if(!empty(source_ID))
                                post+='&source_ID='+uniEncode(source_ID);

                            if(!empty(source_link))
                                post+='&source_link='+uniEncode(source_link);

                            if(!empty(sort_by))
                                post+='&sort_by='+uniEncode(sort_by);

                            if(!empty(sort_direction))
                                post+='&sort_direction='+uniEncode(sort_direction);

                            if(!empty(is_uploaded_image))
                                post+='&is_uploaded_image='+uniEncode(is_uploaded_image);

                            if(!empty(is_not_one_face))
                                post+='&is_not_one_face='+uniEncode(is_not_one_face);

                        }

                        send({
                            'scriptPath':'/api/json/get_face_list',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                let  dataTemp
                                    ,data;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    trace('ERROR LOAD');

                                     page_object.action.admin.action.face_item_data.action.scroll.is_work=false;

                                }
                                else{

                                     page_object.action.admin.action.face_item_data.action.scroll.prepare(data);

                                }

                            }
                        });

                    }

            },
            'prepare':function(data){

                data=data['data'];

                if(data['face_list'].length>0){

                    let  face_list      =page_object.action.admin.data['face_data']['face_list']
                        ,face_index     =face_list.length
                        ,index
                        ,face_len
                        ,el
                        ,func
                        ,num            =0
                        ,inner;

                    page_object.action.admin.data['face_data']['face_list']=page_object.action.admin.data['face_data']['face_list'].concat(data['face_list']);

                    face_len=page_object.action.admin.data['face_data']['face_list'].length;

                    for(index=face_index;index<face_len;index++){

                        inner=page_object.action.admin.create.face.create.get_face_item_data_row(index);

                        el=addElement({
                            'tag':'div',
                            'id':'face_item_'+index,
                            'class':'face_item',
                            'inner':inner
                        });

                        $d('face_list').appendChild(el);

                    }

                }

                page_object.action.admin.action.face_item_data.action.init();

                page_object.action.admin.action.face_item_data.action.scroll.is_work=false;

            },
            'remove_listener':function(){

                listener.remove( page_object.action.admin.action.face_item_data.action.scroll.event);

            }
        },
    },
    'resize':function(){}
};