page_object.action.admin.action.users={
    'show':function(){

        if(isset($d('users_content'))){

            page_object.action.admin.position.users_content.o       =1;
            page_object.action.admin.position.users_content.h       =$d('users_list').scrollHeight+page_object.action.admin.position.users_list.m.t+page_object.action.admin.position.users_list.m.b+2;
            page_object.action.admin.position.users_content.y       =page_object.action.admin.position.header.h+page_object.action.admin.position.users_content.m.t;

            page_object.action.admin.position.users_list.o          =1;
            page_object.action.admin.position.users_list.y          =page_object.action.admin.position.users_list.m.t;

            page_object.action.admin.position.users_add.o           =1;
            page_object.action.admin.position.users_add.w           =elementSize.width($d('users_add'));
            page_object.action.admin.position.users_add.x           =page_object.action.admin.position.users_list.x+page_object.action.admin.position.users_list.w-page_object.action.admin.position.users_add.w;

            $s('users_content').width       =page_object.action.admin.position.users_content.w+'px';
            $s('users_content').height      =page_object.action.admin.position.users_content.h+'px';
            $s('users_content').opacity     =page_object.action.admin.position.users_content.o;
            $s('users_content').transform   ='translate(0,'+page_object.action.admin.position.users_content.y+'px)';

            $s('users_list').width          =page_object.action.admin.position.users_list.w+'px';
            $s('users_list').height         =page_object.action.admin.position.users_list.h+'px';
            $s('users_list').opacity        =page_object.action.admin.position.users_list.o;
            $s('users_list').transform      ='translate('+page_object.action.admin.position.users_list.x+'px,'+page_object.action.admin.position.users_list.y+'px)';

            $s('users_add').opacity         =page_object.action.admin.position.users_add.o;
            // $s('users_add').transform       ='translate('+page_object.action.admin.position.users_add.x+'px,'+page_object.action.admin.position.users_add.y+'px)';

            // $s('all').height                =(page_object.action.admin.position.users_content.h+page_object.action.admin.position.header.h)+'px';

        }

        setTimeout(page_object.link.preload.un_show,300);

    },
    'un_show':function(remove){

        if(isset($d('users_content'))){

            page_object.action.admin.position.users_content.o       =1;
            page_object.action.admin.position.users_content.h       =0;

            $s('users_content').opacity     =page_object.action.admin.position.users_content.o;
            $s('users_list').height         =page_object.action.admin.position.users_content.h+'px';
            $s('users_content').height      =page_object.action.admin.position.users_content.h+'px';

        }

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.admin.action.users.remove,300);

    },
    'remove':function(){

        if(isset($d('users_content')))
            removeElement($d('users_content'));

    },
    'click':{
        'edit':{
            'index':null,
            'init':function(){

                let  index              =this.id.split('_')[4]
                    ,lang_obj           =page_object.action.admin.content[page_object.lang]
                    ,data               =page_object.action.admin.data
                    ,user_data          =data['list'][index]
                    ,inner              ='';

                page_object.action.admin.action.users.click.edit.index=index;

                inner+='<div id="dialog_login_block" class="dialog_row">';
                inner+='<div id="dialog_login_label" class="dialog_row_label">';
                inner+='<span>'+stripSlashes(lang_obj['users_login'])+'</span>';
                inner+='</div>';
                inner+='<div id="dialog_login_input_block" class="dialog_row_input">';
                inner+='<input type="text" id="dialog_login_input_text" class="dialog_row_input_text" value="'+stripSlashes(empty(user_data['login'])?'':user_data['login'])+'" />';
                inner+='</div>';
                inner+='</div>';

                inner+='<div id="dialog_email_block" class="dialog_row">';
                inner+='<div id="dialog_email_label" class="dialog_row_label">';
                inner+='<span>'+stripSlashes(lang_obj['users_email'])+'</span>';
                inner+='</div>';
                inner+='<div id="dialog_email_input_block" class="dialog_row_input">';
                inner+='<input type="text" id="dialog_email_input_text" class="dialog_row_input_text" value="'+stripSlashes(empty(user_data['email'])?'':user_data['email'])+'" />';
                inner+='</div>';
                inner+='</div>';

                inner+='<div id="dialog_name_block" class="dialog_row">';
                inner+='<div id="dialog_name_label" class="dialog_row_label">';
                inner+='<span>'+stripSlashes(lang_obj['users_name'])+'</span>';
                inner+='</div>';
                inner+='<div id="dialog_name_input_block" class="dialog_row_input">';
                inner+='<input type="text" id="dialog_name_input_text" class="dialog_row_input_text" value="'+stripSlashes(empty(user_data['name'])?'':user_data['name'])+'" />';
                inner+='</div>';
                inner+='</div>';

                inner+='<div id="dialog_surname_block" class="dialog_row">';
                inner+='<div id="dialog_surname_label" class="dialog_row_label">';
                inner+='<span>'+stripSlashes(lang_obj['users_surname'])+'</span>';
                inner+='</div>';
                inner+='<div id="dialog_surname_input_block" class="dialog_row_input">';
                inner+='<input type="text" id="dialog_surname_input_text" class="dialog_row_input_text" value="'+stripSlashes(empty(user_data['surname'])?'':user_data['surname'])+'" />';
                inner+='</div>';
                inner+='</div>';

                inner+='<div id="dialog_museum_block" class="dialog_row">';

                    inner+='<div id="dialog_admin_check" class="dialog_admin_check'+(user_data['is_root']?'_active':'')+'">';
                        inner+='<div class="dialog_admin_check_checkbox"></div>';
                        inner+='<div class="dialog_admin_check_name"><table cellpadding="0" cellspacing="0"><tr><td valign="middle" height="22">'+stripSlashes(lang_obj['users_admin'])+'</td></tr></table></div>';
                    inner+='</div>';

                inner+='</div>';

                var dialog_index=page_object.dialog.init({
                    'title':lang_obj['dialog_user_create_title'],
                    'inner':inner,
                    'save':function(){

                        page_object.action.admin.action.users.click.edit.send(dialog_index);

                    },
                    'on_create':function(){

                        page_object.action.admin.action.users.click.edit.check_login.success         =true;
                        page_object.action.admin.action.users.click.edit.check_email.success         =true;
                        page_object.action.admin.action.users.click.edit.check_name.success          =true;
                        page_object.action.admin.action.users.click.edit.check_surname.success       =true;

                        $d('dialog_email_input_text').onkeydown         =page_object.action.admin.action.users.click.edit.keydown;
                        $d('dialog_email_input_text').onkeyup           =page_object.action.admin.action.users.click.edit.check_email.init;

                        $d('dialog_login_input_text').onkeydown         =page_object.action.admin.action.users.click.edit.keydown;
                        $d('dialog_login_input_text').onkeyup           =page_object.action.admin.action.users.click.edit.check_login.init;

                        $d('dialog_name_input_text').onkeydown          =page_object.action.admin.action.users.click.edit.keydown;
                        $d('dialog_name_input_text').onkeyup            =page_object.action.admin.action.users.click.edit.check_name.init;

                        $d('dialog_surname_input_text').onkeydown       =page_object.action.admin.action.users.click.edit.keydown;
                        $d('dialog_surname_input_text').onkeyup         =page_object.action.admin.action.users.click.edit.check_surname.init;

                        $d('dialog_admin_check').onclick                =page_object.action.admin.action.users.click.edit.admin_check;

                    },
                    'cancel':true
                });
            },
            'keydown':function(){

                this.style.borderColor='#1b93db';

            },
            'check_name':{
                'success':false,
                'timeout':null,
                'init':function(){

                    if(isset(page_object.action.admin.action.users.click.edit.check_name.timeout))
                        clearTimeout(page_object.action.admin.action.users.click.edit.check_name.timeout);

                    page_object.action.admin.action.users.click.edit.check_name.timeout=setTimeout(page_object.action.admin.action.users.click.edit.check_name.check,500);

                },
                'check':function(){

                    if($d('dialog_name_input_text').value.length>=2&&$d('dialog_name_input_text').value.length<=32){

                        page_object.action.admin.action.users.click.edit.check_name.success=true;

                        $s('dialog_name_input_text').borderColor='#84cf30';

                    }
                    else{

                        page_object.action.admin.action.users.click.edit.check_name.success=false;

                        $s('dialog_name_input_text').borderColor='#e14141';

                    }

                }
            },
            'check_surname':{
                'success':false,
                'timeout':null,
                'init':function(){

                    if(isset(page_object.action.admin.action.users.click.edit.check_surname.timeout))
                        clearTimeout(page_object.action.admin.action.users.click.edit.check_surname.timeout);

                    page_object.action.admin.action.users.click.edit.check_surname.timeout=setTimeout(page_object.action.admin.action.users.click.edit.check_surname.check,500);

                },
                'check':function(){

                    if($d('dialog_surname_input_text').value.length>=2&&$d('dialog_surname_input_text').value.length<=32){

                        page_object.action.admin.action.users.click.edit.check_surname.success=true;

                        $s('dialog_surname_input_text').borderColor='#84cf30';

                    }
                    else{

                        page_object.action.admin.action.users.click.edit.check_surname.success=false;

                        $s('dialog_surname_input_text').borderColor='#e14141';

                    }

                }
            },
            'check_email':{
                'success':false,
                'timeout':null,
                'init':function(){

                    if(isset(page_object.action.admin.action.users.click.edit.check_email.timeout))
                        clearTimeout(page_object.action.admin.action.users.click.edit.check_email.timeout);

                    page_object.action.admin.action.users.click.edit.check_email.timeout=setTimeout(page_object.action.admin.action.users.click.edit.check_email.send,500);

                },
                'send':function(){

                    if(isEmail($d('dialog_email_input_text').value)){

                        let  index              =page_object.action.admin.action.users.click.edit.index
                            ,data               =page_object.action.admin.data
                            ,user_ID            =data['list'][index]['ID'];

                        send({
                            'scriptPath':'/api/json/isset_user_email',
                            'postData':'user_ID='+uniEncode(user_ID)+'&email='+uniEncode($d('dialog_email_input_text').value),
                            'onComplete':function(j,worktime){

                                let  dataTemp
                                    ,data;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    page_object.action.admin.action.users.click.edit.check_email.success=false;

                                    $s('dialog_email_input_text').borderColor='#e14141';

                                }
                                else if(data['isset_email']){

                                    page_object.action.admin.action.users.click.edit.check_email.success=false;

                                    $s('dialog_email_input_text').borderColor='#e14141';

                                }
                                else{

                                    page_object.action.admin.action.users.click.edit.check_email.success=true;

                                    $s('dialog_email_input_text').borderColor='#84cf30';

                                }

                            }
                        });

                        return true;

                    }
                    else{

                        $s('dialog_email_input_text').borderColor='#e14141';

                        page_object.action.admin.action.users.click.edit.check_email.success=false;

                        return false;

                    }

                }
            },
            'check_login':{
                'success':false,
                'timeout':null,
                'init':function(){

                    if(isset(page_object.action.admin.action.users.click.edit.check_login.timeout))
                        clearTimeout(page_object.action.admin.action.users.click.edit.check_login.timeout);

                    page_object.action.admin.action.users.click.edit.check_login.timeout=setTimeout(page_object.action.admin.action.users.click.edit.check_login.send,500);

                },
                'send':function(){

                    if($d('dialog_login_input_text').value.length>=3&&$d('dialog_login_input_text').value.length<=32){

                        let  index              =page_object.action.admin.action.users.click.edit.index
                            ,data               =page_object.action.admin.data
                            ,user_ID            =data['list'][index]['ID'];

                        send({
                            'scriptPath':'/api/json/isset_user_login',
                            'postData':'user_ID='+uniEncode(user_ID)+'&login='+uniEncode($d('dialog_login_input_text').value),
                            'onComplete':function(j,worktime){

                                let  dataTemp
                                    ,data;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    page_object.action.admin.action.users.click.edit.check_login.success=false;

                                    $s('dialog_login_input_text').borderColor='#e14141';

                                }
                                else if(data['isset_login']){

                                    page_object.action.admin.action.users.click.edit.check_login.success=false;

                                    $s('dialog_login_input_text').borderColor='#e14141';

                                }
                                else{

                                    page_object.action.admin.action.users.click.edit.check_login.success=true;

                                    $s('dialog_login_input_text').borderColor='#84cf30';

                                }

                            }
                        });

                    }
                    else{

                        $s('dialog_login_input_text').borderColor='#e14141';

                        page_object.action.admin.action.users.click.edit.check_login.success=false;

                        return false;

                    }

                }
            },
            'send':function(dialog_index){

                let  post               =''
                    ,error              =false
                    ,index              =page_object.action.admin.action.users.click.edit.index
                    ,data               =page_object.action.admin.data
                    ,user_ID            =data['list'][index]['ID']
                    ,lang_obj           =page_object.action.admin.content[page_object.lang];

                post+='user_ID='+uniEncode(user_ID);
                post+='&is_root='+uniEncode($d('dialog_admin_check').getAttribute('class')==='dialog_admin_check_active'?1:0);

                if(
                      page_object.action.admin.action.users.click.edit.check_login.success
                    &&page_object.action.admin.action.users.click.edit.check_email.success
                    &&page_object.action.admin.action.users.click.edit.check_name.success
                    &&page_object.action.admin.action.users.click.edit.check_surname.success
                    &&!error
                ){
                    
                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');

                    $d('dialog_submit_save_'+dialog_index).innerHTML    =lang_obj['wait'];
                    $d('dialog_submit_save_'+dialog_index).onclick      =function(){};

                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    post+='&user_login='+uniEncode($d('dialog_login_input_text').value);
                    post+='&user_email='+uniEncode($d('dialog_email_input_text').value);
                    post+='&user_name='+uniEncode($d('dialog_name_input_text').value);
                    post+='&user_surname='+uniEncode($d('dialog_surname_input_text').value);

                    send({
                        'scriptPath':'/api/json/update_admin',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                $s('dialog_login_input_text').borderColor   ='#e14141';
                                $s('dialog_email_input_text').borderColor   ='#e14141';

                                $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                    page_object.action.admin.action.users.click.edit.send(dialog_index);

                                };
                                $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                    page_object.dialog.action.un_show.init(dialog_index,true);

                                };

                                $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                lang_obj=page_object.dialog.content[page_object.lang];

                                $d('dialog_submit_save_'+dialog_index).innerHTML=lang_obj['save'];

                                page_object.dialog.create.set_action();

                            }
                            else{

                                page_object.dialog.action.un_show.init(dialog_index,true);

                                page_object.action.admin.action.users.click.edit.create(data);

                            }

                        }
                    });

                }
                else{

                    if(!page_object.action.admin.action.users.click.edit.check_login.success)
                        $s('dialog_login_input_text').borderColor='#e14141';

                    if(!page_object.action.admin.action.users.click.edit.check_email.success)
                        $s('dialog_email_input_text').borderColor='#e14141';

                    if(!page_object.action.admin.action.users.click.edit.check_name.success)
                        $s('dialog_name_input_text').borderColor='#e14141';

                    if(!page_object.action.admin.action.users.click.edit.check_surname.success)
                        $s('dialog_surname_input_text').borderColor='#e14141';

                }

            },
            'create':function(data){

                let index=page_object.action.admin.action.users.click.edit.index;

                if(isset($d('users_row_'+index))){

                    page_object.action.admin.data['list'][index]=data['user'];

                    if(isset($d('users_header_login_'+index)))
                        $d('users_header_login_'+index).innerHTML='<span>'+stripSlashes(data['user']['login'])+'</span>';

                    if(isset($d('users_header_name_'+index)))
                        $d('users_header_name_'+index).innerHTML='<span>'+stripSlashes(data['user']['name'])+'</span>';

                    if(isset($d('users_header_surname_'+index)))
                        $d('users_header_surname_'+index).innerHTML='<span>'+stripSlashes(data['user']['surname'])+'</span>';

                    if(isset($d('users_header_email_'+index)))
                        $d('users_header_email_'+index).innerHTML='<span>'+stripSlashes(data['user']['email'])+'</span>';

                    if(isset($d('users_header_admin_check_'+index)))
                        $d('users_header_admin_check_'+index).setAttribute('class','users_header_admin_check'+(data['user']['is_root']?'_active':''));

                    setTimeout(function(){

                        page_object.action.admin.action.users.click.edit.set_action(index);
                        page_object.action.admin.action.users.resize();

                    },40);

                }

            },
            'set_action':function(i){

                if(isset($d('users_header_admin_check_'+i)))
                    $d('users_header_admin_check_'+i).onclick=page_object.action.admin.action.users.click.admin.init;

                if(isset($d('users_header_block_check_'+i)))
                    $d('users_header_block_check_'+i).onclick=page_object.action.admin.action.users.click.block.init;

                if(isset($d('users_header_edit_check_'+i)))
                    $d('users_header_edit_check_'+i).onclick=page_object.action.admin.action.users.click.edit.init;

                if(isset($d('users_header_remove_check_'+i)))
                    $d('users_header_remove_check_'+i).onclick=page_object.action.admin.action.users.click.remove.init;

            },
            'admin_check':function(){

                if(this.getAttribute('class')==='dialog_admin_check')
                    $d('dialog_admin_check').setAttribute('class','dialog_admin_check_active');
                else
                    $d('dialog_admin_check').setAttribute('class','dialog_admin_check');

                setTimeout(page_object.dialog.action.resize,300);

            }
        },
        'add':{
            'init':function(){

                let  lang_obj           =page_object.action.admin.content[page_object.lang]
                    ,inner              ='';

                inner+='<div id="dialog_login_block" class="dialog_row">';
                inner+='<div id="dialog_login_label" class="dialog_row_label">';
                inner+='<span>'+stripSlashes(lang_obj['users_login'])+'</span>';
                inner+='</div>';
                inner+='<div id="dialog_login_input_block" class="dialog_row_input">';
                inner+='<input type="text" id="dialog_login_input_text" class="dialog_row_input_text" value="" />';
                inner+='</div>';
                inner+='</div>';

                inner+='<div id="dialog_email_block" class="dialog_row">';
                inner+='<div id="dialog_email_label" class="dialog_row_label">';
                inner+='<span>'+stripSlashes(lang_obj['users_email'])+'</span>';
                inner+='</div>';
                inner+='<div id="dialog_email_input_block" class="dialog_row_input">';
                inner+='<input type="text" id="dialog_email_input_text" class="dialog_row_input_text" value="" />';
                inner+='</div>';
                inner+='</div>';

                inner+='<div id="dialog_name_block" class="dialog_row">';
                inner+='<div id="dialog_name_label" class="dialog_row_label">';
                inner+='<span>'+stripSlashes(lang_obj['users_name'])+'</span>';
                inner+='</div>';
                inner+='<div id="dialog_name_input_block" class="dialog_row_input">';
                inner+='<input type="text" id="dialog_name_input_text" class="dialog_row_input_text" value="" />';
                inner+='</div>';
                inner+='</div>';

                inner+='<div id="dialog_surname_block" class="dialog_row">';
                inner+='<div id="dialog_surname_label" class="dialog_row_label">';
                inner+='<span>'+stripSlashes(lang_obj['users_surname'])+'</span>';
                inner+='</div>';
                inner+='<div id="dialog_surname_input_block" class="dialog_row_input">';
                inner+='<input type="text" id="dialog_surname_input_text" class="dialog_row_input_text" value="" />';
                inner+='</div>';
                inner+='</div>';

                inner+='<div id="dialog_museum_block" class="dialog_row">';

                    inner+='<div id="dialog_admin_check" class="dialog_admin_check">';
                        inner+='<div class="dialog_admin_check_checkbox"></div>';
                        inner+='<div class="dialog_admin_check_name"><table cellpadding="0" cellspacing="0"><tr><td valign="middle" height="22">'+stripSlashes(lang_obj['users_admin'])+'</td></tr></table></div>';
                    inner+='</div>';

                inner+='</div>';

                var dialog_index=page_object.dialog.init({
                    'title':lang_obj['dialog_user_create_title'],
                    'inner':inner,
                    'create':function(){

                        page_object.action.admin.action.users.click.add.send(dialog_index);

                    },
                    'on_create':function(){

                        page_object.action.admin.action.users.click.add.check_login.success         =false;
                        page_object.action.admin.action.users.click.add.check_email.success         =false;
                        page_object.action.admin.action.users.click.add.check_name.success          =false;
                        page_object.action.admin.action.users.click.add.check_surname.success       =false;

                        $d('dialog_email_input_text').onkeydown         =page_object.action.admin.action.users.click.add.keydown;
                        $d('dialog_email_input_text').onkeyup           =page_object.action.admin.action.users.click.add.check_email.init;

                        $d('dialog_login_input_text').onkeydown         =page_object.action.admin.action.users.click.add.keydown;
                        $d('dialog_login_input_text').onkeyup           =page_object.action.admin.action.users.click.add.check_login.init;

                        $d('dialog_name_input_text').onkeydown          =page_object.action.admin.action.users.click.add.keydown;
                        $d('dialog_name_input_text').onkeyup            =page_object.action.admin.action.users.click.add.check_name.init;

                        $d('dialog_surname_input_text').onkeydown       =page_object.action.admin.action.users.click.add.keydown;
                        $d('dialog_surname_input_text').onkeyup         =page_object.action.admin.action.users.click.add.check_surname.init;

                        $d('dialog_admin_check').onclick                =page_object.action.admin.action.users.click.add.admin_check;

                    },
                    'cancel':true
                });

            },
            'keydown':function(){

                this.style.borderColor='#1b93db';

            },
            'check_name':{
                'success':false,
                'timeout':null,
                'init':function(){

                    if(isset(page_object.action.admin.action.users.click.add.check_name.timeout))
                        clearTimeout(page_object.action.admin.action.users.click.add.check_name.timeout);

                    page_object.action.admin.action.users.click.add.check_name.timeout=setTimeout(page_object.action.admin.action.users.click.add.check_name.check,500);

                },
                'check':function(){

                    if($d('dialog_name_input_text').value.length>=2&&$d('dialog_name_input_text').value.length<=32){

                        page_object.action.admin.action.users.click.add.check_name.success=true;

                        $s('dialog_name_input_text').borderColor='#84cf30';

                    }
                    else{

                        page_object.action.admin.action.users.click.add.check_name.success=false;

                        $s('dialog_name_input_text').borderColor='#e14141';

                    }

                }
            },
            'check_surname':{
                'success':false,
                'timeout':null,
                'init':function(){

                    if(isset(page_object.action.admin.action.users.click.add.check_surname.timeout))
                        clearTimeout(page_object.action.admin.action.users.click.add.check_surname.timeout);

                    page_object.action.admin.action.users.click.add.check_surname.timeout=setTimeout(page_object.action.admin.action.users.click.add.check_surname.check,500);

                },
                'check':function(){

                    if($d('dialog_surname_input_text').value.length>=2&&$d('dialog_surname_input_text').value.length<=32){

                        page_object.action.admin.action.users.click.add.check_surname.success=true;

                        $s('dialog_surname_input_text').borderColor='#84cf30';

                    }
                    else{

                        page_object.action.admin.action.users.click.add.check_surname.success=false;

                        $s('dialog_surname_input_text').borderColor='#e14141';

                    }

                }
            },
            'check_email':{
                'success':false,
                'timeout':null,
                'init':function(){

                    if(isset(page_object.action.admin.action.users.click.add.check_email.timeout))
                        clearTimeout(page_object.action.admin.action.users.click.add.check_email.timeout);

                    page_object.action.admin.action.users.click.add.check_email.timeout=setTimeout(page_object.action.admin.action.users.click.add.check_email.send,500);

                },
                'send':function(){

                    if(isEmail($d('dialog_email_input_text').value)){

                        send({
                            'scriptPath':'/api/json/isset_user_email',
                            'postData':'email='+uniEncode($d('dialog_email_input_text').value),
                            'onComplete':function(j,worktime){

                                let  dataTemp
                                    ,data;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    page_object.action.admin.action.users.click.add.check_email.success=false;

                                    $s('dialog_email_input_text').borderColor='#e14141';

                                }
                                else if(data['isset_email']){

                                    page_object.action.admin.action.users.click.add.check_email.success=false;

                                    $s('dialog_email_input_text').borderColor='#e14141';

                                }
                                else{

                                    page_object.action.admin.action.users.click.add.check_email.success=true;

                                    $s('dialog_email_input_text').borderColor='#84cf30';

                                }

                            }
                        });

                        return true;

                    }
                    else{

                        $s('dialog_email_input_text').borderColor='#e14141';

                        page_object.action.admin.action.users.click.add.check_email.success=false;

                        return false;

                    }

                }
            },
            'check_login':{
                'success':false,
                'timeout':null,
                'init':function(){

                    if(isset(page_object.action.admin.action.users.click.add.check_login.timeout))
                        clearTimeout(page_object.action.admin.action.users.click.add.check_login.timeout);

                    page_object.action.admin.action.users.click.add.check_login.timeout=setTimeout(page_object.action.admin.action.users.click.add.check_login.send,500);

                },
                'send':function(){

                    if($d('dialog_login_input_text').value.length>=3&&$d('dialog_login_input_text').value.length<=32){

                        send({
                            'scriptPath':'/api/json/isset_user_login',
                            'postData':'login='+uniEncode($d('dialog_login_input_text').value),
                            'onComplete':function(j,worktime){

                                let  dataTemp
                                    ,data;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    page_object.action.admin.action.users.click.add.check_login.success=false;

                                    $s('dialog_login_input_text').borderColor='#e14141';

                                }
                                else if(data['isset_login']){

                                    page_object.action.admin.action.users.click.add.check_login.success=false;

                                    $s('dialog_login_input_text').borderColor='#e14141';

                                }
                                else{

                                    page_object.action.admin.action.users.click.add.check_login.success=true;

                                    $s('dialog_login_input_text').borderColor='#84cf30';

                                }

                            }
                        });

                    }
                    else{

                        $s('dialog_login_input_text').borderColor='#e14141';

                        page_object.action.admin.action.users.click.add.check_login.success=false;

                        return false;

                    }

                }
            },
            'send':function(dialog_index){

                let  post               =''
                    ,error              =false
                    ,lang_obj           =page_object.action.admin.content[page_object.lang];

                post+='&is_root='+uniEncode(($d('dialog_admin_check').getAttribute('class')==='dialog_admin_check_active'?1:0));

                if(
                      page_object.action.admin.action.users.click.add.check_login.success
                    &&page_object.action.admin.action.users.click.add.check_email.success
                    &&page_object.action.admin.action.users.click.add.check_name.success
                    &&page_object.action.admin.action.users.click.add.check_surname.success
                    &&!error
                ){

                    $d('dialog_submit_create_'+dialog_index).setAttribute('class','dialog_submit_disable');

                    $d('dialog_submit_create_'+dialog_index).innerHTML    =lang_obj['wait'];
                    $d('dialog_submit_create_'+dialog_index).onclick      =function(){};

                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    post+='&user_login='+uniEncode($d('dialog_login_input_text').value);
                    post+='&user_email='+uniEncode($d('dialog_email_input_text').value);
                    post+='&user_name='+uniEncode($d('dialog_name_input_text').value);
                    post+='&user_surname='+uniEncode($d('dialog_surname_input_text').value);

                    send({
                        'scriptPath':'/api/json/add_admin',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                $s('dialog_login_input_text').borderColor='#e14141';
                                $s('dialog_email_input_text').borderColor='#e14141';

                                $d('dialog_submit_create_'+dialog_index).onclick=function(){

                                    page_object.action.admin.action.users.click.add.send(dialog_index);

                                };
                                $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                    page_object.dialog.action.un_show.init(dialog_index,true);

                                };
                                $d('dialog_submit_create_'+dialog_index).setAttribute('class','dialog_submit_create');
                                $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                lang_obj=page_object.dialog.content[page_object.lang];

                                $d('dialog_submit_create_'+dialog_index).innerHTML=lang_obj['create'];

                                page_object.dialog.create.set_action();

                            }
                            else{

                                page_object.dialog.action.un_show.init(dialog_index,true);

                                page_object.action.admin.action.users.click.add.create(data);

                            }

                        }
                    });

                }
                else{

                    $d('dialog_submit_create_'+dialog_index).onclick=function(){

                        page_object.action.admin.action.users.click.add.send(dialog_index);

                    };
                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                    };
                    $d('dialog_submit_create_'+dialog_index).setAttribute('class','dialog_submit_create');
                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                    if(!page_object.action.admin.action.users.click.add.check_login.success)
                        $s('dialog_login_input_text').borderColor='#e14141';

                    if(!page_object.action.admin.action.users.click.add.check_email.success)
                        $s('dialog_email_input_text').borderColor='#e14141';

                    if(!page_object.action.admin.action.users.click.add.check_name.success)
                        $s('dialog_name_input_text').borderColor='#e14141';

                    if(!page_object.action.admin.action.users.click.add.check_surname.success)
                        $s('dialog_surname_input_text').borderColor='#e14141';

                }

            },
            'create':function(data){

                page_object.action.admin.data['list'].push(data['user']);

                let  inner                  =''
                    ,i                      =page_object.action.admin.data['list'].length-1
                    ,el
                    ,style                  =''
                    ,user_list_w            =page_object.action.admin.position.users_list.w
                    ,user_list_h            =page_object.action.admin.position.users_list.table.row
                    ,y                      =i*user_list_h;

                style+='width: '+user_list_w+'px; ';
                style+='height: '+user_list_h+'px; ';
                style+='transform: translate(0,'+y+'px); ';

                inner+=page_object.action.admin.create.users.get_row(i);

                el=addElement({
                    'tag':'div',
                    'id':'users_row_'+i,
                    'class':'users_row',
                    'inner':inner,
                    'style':style
                });

                $d('users_list').appendChild(el);

                setTimeout(function(){

                    page_object.action.admin.action.users.click.add.set_action(i);
                    page_object.action.admin.action.users.resize();

                },40);

            },
            'set_action':function(i){

                if(isset($d('users_header_admin_check_'+i)))
                    $d('users_header_admin_check_'+i).onclick=page_object.action.admin.action.users.click.admin.init;

                if(isset($d('users_header_block_check_'+i)))
                    $d('users_header_block_check_'+i).onclick=page_object.action.admin.action.users.click.block.init;

                if(isset($d('users_header_edit_check_'+i)))
                    $d('users_header_edit_check_'+i).onclick=page_object.action.admin.action.users.click.edit.init;

                if(isset($d('users_header_remove_check_'+i)))
                    $d('users_header_remove_check_'+i).onclick=page_object.action.admin.action.users.click.remove.init;

            },
            'admin_check':function(){

                if(this.getAttribute('class')==='dialog_admin_check')
                    $d('dialog_admin_check').setAttribute('class','dialog_admin_check_active');
                else
                    $d('dialog_admin_check').setAttribute('class','dialog_admin_check');

                setTimeout(page_object.dialog.action.resize,300);

            }
        },
        'block':{
            'init':function(){

                let  index      =this.id.split('_')[4]
                    ,user_ID    =page_object.action.admin.data['list'][index]['ID'];

                page_object.action.admin.data['list'][index]['is_block']=!page_object.action.admin.data['list'][index]['is_block'];

                if(isset($d('users_header_block_check_'+index)))
                    $d('users_header_block_check_'+index).setAttribute('class','users_header_block_check'+(page_object.action.admin.data['list'][index]['is_block']?'_active':''));

                send({
                    'scriptPath':'/api/json/block_admin',
                    'postData':'user_ID='+uniEncode(user_ID),
                    'onComplete':function(j,worktime){

                        let  dataTemp
                            ,data;

                        dataTemp=j.responseText;
                        trace(dataTemp);
                        data=jsonDecode(dataTemp);
                        trace(data);
                        trace_worktime(worktime,data);

                        if(!isset(data['error'])){

                            page_object.action.admin.data['list'][index]['is_block']=data['user']['is_block'];

                            if(isset($d('users_header_block_check_'+index)))
                                $d('users_header_block_check_'+index).setAttribute('class','users_header_block_check'+(data['user']['is_block']?'_active':''))

                        }

                    }
                });

            }
        },
        'remove':{
            'init':function(){

                let  index          =parseInt(this.id.split('_')[4])
                    ,user_ID        =page_object.action.admin.data['list'][index]['ID']
                    ,user_login     =page_object.action.admin.data['list'][index]['login']
                    ,lang_obj       =page_object.action.admin.content[page_object.lang];

                var dialog_index=page_object.dialog.init({
                    'title':lang_obj['dialog_user_remove']+' '+user_login+'?',
                    'remove':function(){

                        page_object.action.admin.action.users.click.remove.send(dialog_index,index,user_ID);

                    },
                    'cancel':true
                });

            },
            'send':function(dialog_index,index,user_ID){

                let lang_obj=page_object.action.admin.content[page_object.lang];

                $d('dialog_submit_remove_'+dialog_index).setAttribute('class','dialog_submit_disable');

                $d('dialog_submit_remove_'+dialog_index).innerHTML    =lang_obj['wait'];
                $d('dialog_submit_remove_'+dialog_index).onclick      =function(){};

                $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                send({
                    'scriptPath':'/api/json/remove_admin',
                    'postData':'user_ID='+uniEncode(user_ID),
                    'onComplete':function(j,worktime){

                        let  dataTemp
                            ,data;

                        dataTemp=j.responseText;
                        trace(dataTemp);
                        data=jsonDecode(dataTemp);
                        trace(data);
                        trace_worktime(worktime,data);

                        if(isset(data['error'])){

                            $d('dialog_submit_remove_'+dialog_index).setAttribute('class','dialog_submit_remove');
                            $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                            lang_obj=page_object.dialog.content[page_object.lang];

                            $d('dialog_submit_remove_'+dialog_index).innerHTML=lang_obj['remove'];

                            page_object.dialog.create.set_action();

                        }
                        else{

                            page_object.action.admin.action.users.click.remove.remove(index);

                            page_object.dialog.action.un_show.init(dialog_index,true);

                        }

                    }
                });

            },
            'remove':function(index){

                let  list           =page_object.action.admin.data['list']
                    ,list_len       =list.length
                    ,i
                    ,i_old;

                if(index===0)
                    page_object.action.admin.data['list']=list.slice(1);
                else if((list_len-1)===index)
                    page_object.action.admin.data['list']=list.slice(0,index);
                else{

                    let  list_left      =list.slice(0,index)
                        ,list_right     =list.slice((index+1),list_len);

                    list=list_left.concat(list_right);

                    page_object.action.admin.data['list']=list;

                }

                list=page_object.action.admin.data['list'];

                if(isset($d('users_row_'+index))){

                    $s('users_row_'+index).height           =0;
                    $s('users_row_'+index).opacity          =0;

                    setTimeout(function(){

                        removeElement($d('users_row_'+index));

                        for(i=index;i<list.length;i++){

                            i_old=i+1;

                            if(isset($d('users_row_'+i_old))){

                                $d('users_row_'+i_old).setAttribute('id','users_row_'+i);

                                $d('users_header_login_'+i_old).setAttribute('id','users_header_login_'+i);
                                $d('users_header_surname_'+i_old).setAttribute('id','users_header_surname_'+i);
                                $d('users_header_name_'+i_old).setAttribute('id','users_header_name_'+i);
                                $d('users_header_email_'+i_old).setAttribute('id','users_header_email_'+i);
                                $d('users_header_admin_'+i_old).setAttribute('id','users_header_admin_'+i);

                                if(isset($d('users_header_admin_check_'+i_old)))
                                    $d('users_header_admin_check_'+i_old).setAttribute('id','users_header_admin_check_'+i);

                                $d('users_header_date_create_'+i_old).setAttribute('id','users_header_date_create_'+i);
                                $d('users_header_date_online_'+i_old).setAttribute('id','users_header_date_online_'+i);
                                $d('users_header_controls_'+i_old).setAttribute('id','users_header_controls_'+i);

                                if(isset($d('users_header_block_check_'+i_old)))
                                    $d('users_header_block_check_'+i_old).setAttribute('id','users_header_block_check_'+i);

                                if(isset($d('users_header_remove_check_'+i_old)))
                                    $d('users_header_remove_check_'+i_old).setAttribute('id','users_header_remove_check_'+i);

                            }

                        }

                        setTimeout(page_object.action.admin.action.users.resize,40);

                    },300);

                }

            }
        },
        'admin':{
            'init':function(){

                var  index      =this.id.split('_')[4]
                    ,user_ID    =page_object.action.admin.data['list'][index]['ID'];

                page_object.action.admin.data['list'][index]['is_root']=!page_object.action.admin.data['list'][index]['is_root'];

                if(isset($d('users_header_admin_check_'+index)))
                    $d('users_header_admin_check_'+index).setAttribute('class','users_header_admin_check'+(page_object.action.admin.data['list'][index]['is_root']?'_active':''));

                send({
                    'scriptPath':'/api/json/update_admin_access',
                    'postData':'user_ID='+uniEncode(user_ID),
                    'onComplete':function(j,worktime){

                        let  dataTemp
                            ,data;

                        dataTemp=j.responseText;
                        trace(dataTemp);
                        data=jsonDecode(dataTemp);
                        trace(data);
                        trace_worktime(worktime,data);

                        if(!isset(data['error'])){

                            page_object.action.admin.data['list'][index]['is_root']=data['user']['is_root'];

                            if(isset($d('users_header_admin_check_'+index)))
                                $d('users_header_admin_check_'+index).setAttribute('class','users_header_admin_check'+(data['user']['is_root']?'_active':''));

                        }

                    }
                });

            }
        }
    },
    'resize':function(){

        if(isset($d('users_content'))){

            page_object.action.admin.create.users.position.init();

            let  proportion             =page_object.action.admin.position['users_list']['table']
                ,user_list_w            =page_object.action.admin.position.users_list.w
                ,users_row_h            =proportion['row']
                ,users_login_w          =Math.ceil(user_list_w*proportion['login']/100)
                ,users_name_w           =Math.ceil(user_list_w*proportion['name']/100)
                ,users_surname_w        =Math.ceil(user_list_w*proportion['surname']/100)
                ,users_email_w          =Math.ceil(user_list_w*proportion['email']/100)
                ,users_admin_w          =Math.ceil(user_list_w*proportion['admin']/100)
                ,users_bookkeep_w       =Math.ceil(user_list_w*proportion['bookkeep']/100)
                ,users_museum_w         =Math.ceil(user_list_w*proportion['museum']/100)
                ,users_date_create_w    =Math.ceil(user_list_w*proportion['date_create']/100)
                ,users_date_online_w    =Math.ceil(user_list_w*proportion['date_online']/100)
                ,users_controls_w       =Math.ceil(user_list_w*proportion['controls']/100)
                ,users_login_x          =0
                ,users_name_x           =users_login_x+users_login_w
                ,users_surname_x        =users_name_x+users_name_w
                ,users_email_x          =users_surname_x+users_surname_w
                ,users_admin_x          =users_email_x+users_email_w
                ,users_bookkeep_x       =users_admin_x+users_admin_w
                ,users_museum_x         =users_bookkeep_x+users_bookkeep_w
                ,users_date_create_x    =users_museum_x+users_museum_w
                ,users_date_online_x    =users_date_create_x+users_date_create_w
                ,users_controls_x       =users_date_online_x+users_date_online_w
                ,list                   =page_object.action.admin.data['list']
                ,i
                ,perc
                ,y                      =0;

            $s('users_header').width                        =user_list_w+'px';

            $s('users_header_login').width                  =users_login_w+'px';
            $s('users_header_surname').width                =users_surname_w+'px';
            $s('users_header_name').width                   =users_name_w+'px';
            $s('users_header_email').width                  =users_email_w+'px';
            $s('users_header_admin').width                  =users_admin_w+'px';
            $s('users_header_date_create').width            =users_date_create_w+'px';
            $s('users_header_date_online').width            =users_date_online_w+'px';
            $s('users_header_controls').width               =users_controls_w+'px';

            $s('users_header_login').height                 =users_row_h+'px';
            $s('users_header_surname').height               =users_row_h+'px';
            $s('users_header_name').height                  =users_row_h+'px';
            $s('users_header_email').height                 =users_row_h+'px';
            $s('users_header_admin').height                 =users_row_h+'px';
            $s('users_header_date_create').height           =users_row_h+'px';
            $s('users_header_date_online').height           =users_row_h+'px';
            $s('users_header_controls').height              =users_row_h+'px';

            $s('users_header_login').transform              ='translate('+users_login_x+'px,0)';
            $s('users_header_surname').transform            ='translate('+users_surname_x+'px,0)';
            $s('users_header_name').transform               ='translate('+users_name_x+'px,0)';
            $s('users_header_email').transform              ='translate('+users_email_x+'px,0)';
            $s('users_header_admin').transform              ='translate('+users_admin_x+'px,0)';
            $s('users_header_date_create').transform        ='translate('+users_date_create_x+'px,0)';
            $s('users_header_date_online').transform        ='translate('+users_date_online_x+'px,0)';
            $s('users_header_controls').transform           ='translate('+users_controls_x+'px,0)';

            for(i=0;i<list.length;i++){

                y       +=users_row_h;
                perc    =i/2;

                $s('users_row_'+i).width                        =user_list_w+'px';
                $s('users_row_'+i).height                       =users_row_h+'px';
                $s('users_row_'+i).transform                    ='translate(0,'+y+'px)';

                $s('users_header_login_'+i).width               =users_login_w+'px';
                $s('users_header_surname_'+i).width             =users_surname_w+'px';
                $s('users_header_name_'+i).width                =users_name_w+'px';
                $s('users_header_email_'+i).width               =users_email_w+'px';
                $s('users_header_admin_'+i).width               =users_admin_w+'px';
                $s('users_header_date_create_'+i).width         =users_date_create_w+'px';
                $s('users_header_date_online_'+i).width         =users_date_online_w+'px';
                $s('users_header_controls_'+i).width            =users_controls_w+'px';

                $s('users_header_login_'+i).height              =users_row_h+'px';
                $s('users_header_surname_'+i).height            =users_row_h+'px';
                $s('users_header_name_'+i).height               =users_row_h+'px';
                $s('users_header_email_'+i).height              =users_row_h+'px';
                $s('users_header_admin_'+i).height              =users_row_h+'px';
                $s('users_header_date_create_'+i).height        =users_row_h+'px';
                $s('users_header_date_online_'+i).height        =users_row_h+'px';
                $s('users_header_controls_'+i).height           =users_row_h+'px';

                $s('users_header_login_'+i).transform           ='translate('+users_login_x+'px,0)';
                $s('users_header_surname_'+i).transform         ='translate('+users_surname_x+'px,0)';
                $s('users_header_name_'+i).transform            ='translate('+users_name_x+'px,0)';
                $s('users_header_email_'+i).transform           ='translate('+users_email_x+'px,0)';
                $s('users_header_admin_'+i).transform           ='translate('+users_admin_x+'px,0)';
                $s('users_header_date_create_'+i).transform     ='translate('+users_date_create_x+'px,0)';
                $s('users_header_date_online_'+i).transform     ='translate('+users_date_online_x+'px,0)';
                $s('users_header_controls_'+i).transform        ='translate('+users_controls_x+'px,0)';

            }

            page_object.action.admin.position.users_content.h   =y+users_row_h+page_object.action.admin.position.header.h+page_object.action.admin.position.users_list.m.b;
            page_object.action.admin.position.users_list.h      =y+users_row_h;

            $s('users_list').width          =page_object.action.admin.position.users_list.w+'px';
            $s('users_list').height         =page_object.action.admin.position.users_list.h+'px';

            $s('users_content').width       =page_object.action.admin.position.users_content.w+'px';
            $s('users_content').height      =page_object.action.admin.position.users_content.h+'px';

            // $s('all').height                =(page_object.action.admin.position.users_content.h+page_object.action.admin.position.header.h)+'px';

        }

    }
};