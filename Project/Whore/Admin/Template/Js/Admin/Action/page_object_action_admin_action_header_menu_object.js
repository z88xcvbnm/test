page_object.action.admin.action.header_menu={
    'show':function(){

        if(isset($d('header_menu'))){

            page_object.action.admin.position.header_menu.o=1;

            $s('header_menu').opacity=page_object.action.admin.position.header_menu.o;

        }

    },
    'un_show':function(remove){

        if(isset($d('header_menu'))){

            page_object.action.admin.position.header_menu.o=0;

            $s('header_menu').opacity=page_object.action.admin.position.header_menu.o;

        }

        if(isset(remove))
            if(remove)
                setTimeout(page_object.action.admin.action.header_menu.remove,300);

    },
    'remove':function(){

        if(isset($d('header_menu')))
            removeElement($d('header_menu'));

    },
    'resize':function(){

        if(isset($d('header'))){

            page_object.action.admin.position.header.w=Math.ceil(winSize.winWidth *.62);

            $s('header').width=page_object.action.admin.position.header_menu.w+'px';

        }

    },
    'click':function(){

        let  el_ID          =this.id
            ,key            =implode('_',el_ID.split('_').slice(2))
            ,lang_obj       =page_object.content[page_object.lang]
            ,link           ='/admin/'+page_object.action.admin.data['user']['login']+'/'+key
            ,func;

        func=function(){

            page_object.action.admin.action.header_menu.set_active(key);

            setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

        };

        page_object.action.admin.action.page_check_edit.init(func);

        return true;

    },
    'set_active':function(key){

        let  list               =$d('header_menu').getElementsByTagName('div')
            ,i
            ,el_x
            ,el_w
            ,isset_active       =false;

        if(!isset(key))
            if(isset(page_object.link.link_list[2]))
                key=page_object.link.link_list[2];

        for(i=0;i<list.length;i++)
            if(isObject(list[i])){

                if(list[i].getAttribute('id')===('header_menu_'+key)){

                    if(isset($d('header_menu_'+key)))
                        if($d('header_menu_'+key).getAttribute('class')!=='header_menu_item_disable'){

                            el_w            =elementSize.width($d('header_menu_'+key));
                            el_x            =getElementPosition($d('header_menu_'+key))['left'];
                            isset_active    =true;

                            $s('header_menu_line_active').transform     ='translate('+el_x+'px,0)';
                            $s('header_menu_line_active').width         =el_w+'px';

                            $d('header_menu_'+key).setAttribute('class','header_menu_item_active');

                        }

                }
                else if(list[i].getAttribute('class')!=='header_menu_item_disable')
                    list[i].setAttribute('class','header_menu_item');

            }

        if(isset($d('header_menu_line_active')))
            $s('header_menu_line_active').opacity=isset_active?1:0;

    }
};