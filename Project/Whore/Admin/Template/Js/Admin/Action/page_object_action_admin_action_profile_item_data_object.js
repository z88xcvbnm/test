page_object.action.admin.action.profile_item_data={
    'show':function(){

        page_object.action.admin.action.profile_item_block.show();

    },
    'un_show':function(remove){

        page_object.action.admin.action.profile_item_block.un_show(remove);

    },
    'action':{
        'init':function(){

            page_object.action.admin.action.profile_item_data.action.email_profile.init();
            page_object.action.admin.action.profile_item_data.action.profile_none.init();
            page_object.action.admin.action.profile_item_data.action.add_profile.init();
            page_object.action.admin.action.profile_item_data.action.edit_profile.init();
            page_object.action.admin.action.profile_item_data.action.balance.init();
            page_object.action.admin.action.profile_item_data.action.remove_profile.init();
            page_object.action.admin.action.profile_item_data.action.scroll.init();
            page_object.action.admin.action.profile_item_data.action.search_history.init();
            page_object.action.admin.action.profile_item_data.action.search.init();

        },
        'profile_none':{
            'init':function(){

                let profile_list=page_object.action.admin.data['data']['profile_list'];

                trace(profile_list.length);

                if(profile_list.length===0){

                    trace('ok');

                    $s('profile_none').opacity     =.4;
                    $s('profile_block').opacity    =0;

                }
                else{

                    $s('profile_none').opacity     =0;
                    $s('profile_block').opacity    =1;

                }

            }
        },
        'rename_profile':{
            'move_down':function(){

                let  list           =page_object.action.admin.data['data']['profile_list']
                    ,list_len       =list.length
                    ,index_from
                    ,index_to;

                for(index_to=list_len;index_to>0;index_to--){

                    index_from=parseInt(index_to)-1;

                    page_object.action.admin.action.profile_item_data.action.rename_profile.rename_index(index_from,index_to);

                }

            },
            'move_up':function(index){

                let  list           =page_object.action.admin.data['data']['profile_list']
                    ,list_len       =list.length
                    ,index_from
                    ,index_to;

                for(index_to=index;index_to<list_len;index_to++){

                    index_from=parseInt(index_to)+1;

                    page_object.action.admin.action.profile_item_data.action.rename_profile.rename_index(index_from,index_to);

                }

            },
            'rename_index':function(index_from,index_to){

                if(isset($d('profile_item_'+index_from))){

                    $d('profile_item_'+index_from).setAttribute('id','profile_item_'+index_to);

                    $d('profile_item_number_'+index_from).setAttribute('id','profile_item_number_'+index_to);
                    $d('profile_item_login_'+index_from).setAttribute('id','profile_item_login_'+index_to);
                    $d('profile_item_surname_'+index_from).setAttribute('id','profile_item_surname_'+index_to);
                    $d('profile_item_name_'+index_from).setAttribute('id','profile_item_name_'+index_to);
                    $d('profile_item_balance_'+index_from).setAttribute('id','profile_item_balance_'+index_to);
                    $d('profile_item_check_'+index_from).setAttribute('id','profile_item_check_'+index_to);
                    $d('profile_item_date_'+index_from).setAttribute('id','profile_item_date_'+index_to);

                    $d('profile_item_controls_'+index_from).setAttribute('id','profile_item_controls_'+index_to);
                    $d('profile_item_info_'+index_from).setAttribute('id','profile_item_info_'+index_to);
                    $d('profile_item_wallet_'+index_from).setAttribute('id','profile_item_wallet_'+index_to);
                    $d('profile_item_edit_'+index_from).setAttribute('id','profile_item_edit_'+index_to);
                    $d('profile_item_remove_'+index_from).setAttribute('id','profile_item_remove_'+index_to);

                }

            }
        },
        'add_profile':{
            'dialog_index':null,
            'init':function(){

                if(isset($d('profile_add')))
                    $d('profile_add').onclick=page_object.action.admin.action.profile_item_data.action.add_profile.box;

            },
            'box':function(){

                let inner='';

                inner+='<div id="reg_block" class="dialog_row">';

                inner+='<div class="dialog_row_label">';
                inner+='<span>Login</span>';
                inner+='<span class="dialog_red_field">*</span>';
                inner+='</div>';
                inner+='<div class="dialog_row_input">';
                inner+='<input type="text" id="dialog_login_input_text" class="dialog_row_input_text" value="" />';
                inner+='</div>';

                inner+='</div>';

                inner+='<div class="dialog_row">';

                inner+='<div class="dialog_row_label">';
                inner+='<span>Email</span>';
                inner+='<span class="dialog_red_field">*</span>';
                inner+='</div>';
                inner+='<div class="dialog_row_input">';
                inner+='<input type="text" id="dialog_email_input_text" class="dialog_row_input_text" value="" />';
                inner+='</div>';

                inner+='</div>';

                inner+='<div class="dialog_row">';

                inner+='<div class="dialog_row_label">';
                inner+='<span>Фамилия</span>';
                inner+='</div>';
                inner+='<div class="dialog_row_input">';
                inner+='<input type="text" id="dialog_surname_input_text" class="dialog_row_input_text" value="" />';
                inner+='</div>';

                inner+='</div>';

                inner+='<div class="dialog_row">';

                inner+='<div class="dialog_row_label">';
                inner+='<span>Имя</span>';
                inner+='</div>';
                inner+='<div class="dialog_row_input">';
                inner+='<input type="text" id="dialog_name_input_text" class="dialog_row_input_text" value="" />';
                inner+='</div>';

                inner+='</div>';

                page_object.action.admin.action.profile_item_data.action.add_profile.dialog_index=page_object.dialog.init({
                    'title':'Добавление пользователя',
                    'inner':inner,
                    'add':function(){

                        page_object.action.admin.action.profile_item_data.action.add_profile.send.init(page_object.action.admin.action.profile_item_data.action.add_profile.dialog_index);

                    },
                    'on_create':function(){

                        page_object.action.admin.action.profile_item_data.action.add_profile.input.init();

                    },
                    'on_cancel':function(){

                        let  lang_obj       =page_object.content[page_object.lang]
                            ,link           ='/';

                        setUrl(lang_obj['title']+' | '+lang_obj['loading'],link);

                    }
                });
                    
            },
            'input':{
                'init':function(){

                    if(isset($d('dialog_login_input_text')))
                        $d('dialog_login_input_text').onkeyup=page_object.action.admin.action.profile_item_data.action.add_profile.input.check_login.init;

                    if(isset($d('dialog_email_input_text')))
                        $d('dialog_email_input_text').onkeyup=page_object.action.admin.action.profile_item_data.action.add_profile.input.check_email.init;

                },
                'check_email':{
                    'success':true,
                    'timeout':null,
                    'init':function(){

                        if(isset(page_object.action.admin.action.profile_item_data.action.add_profile.input.check_email.timeout))
                            clearTimeout(page_object.action.admin.action.profile_item_data.action.add_profile.input.check_email.timeout);

                        page_object.action.admin.action.profile_item_data.action.add_profile.input.check_email.timeout=setTimeout(page_object.action.admin.action.profile_item_data.action.add_profile.input.check_email.send,500);

                    },
                    'send':function(){

                        if(isEmail($d('dialog_email_input_text').value)){

                            send({
                                'scriptPath':'/api/json/isset_user_email',
                                'postData':'email='+uniEncode($d('dialog_email_input_text').value),
                                'onComplete':function(j,worktime){

                                    var  dataTemp
                                        ,data;

                                    dataTemp=j.responseText;
                                    trace(dataTemp);
                                    data=jsonDecode(dataTemp);
                                    trace(data);
                                    trace_worktime(worktime,data);

                                    if(isset(data['error'])){

                                        page_object.action.admin.action.profile_item_data.action.add_profile.input.check_email.success=false;

                                        $s('dialog_email_input_text').borderColor='#e14141';

                                    }
                                    else if(data['isset_email']){

                                        page_object.action.admin.action.profile_item_data.action.add_profile.input.check_email.success=false;

                                        $s('dialog_email_input_text').borderColor='#e14141';

                                    }
                                    else{

                                        page_object.action.admin.action.profile_item_data.action.add_profile.input.check_email.success=true;

                                        $s('dialog_email_input_text').borderColor='#84cf30';

                                    }

                                }
                            });

                            return true;

                        }
                        else{

                            $s('dialog_email_input_text').borderColor='#e14141';

                            page_object.action.admin.action.profile_item_data.action.add_profile.input.check_email.success=false;

                            return false;

                        }

                    }
                },
                'check_login':{
                    'success':true,
                    'timeout':null,
                    'init':function(){

                        if(isset(page_object.action.admin.action.profile_item_data.action.add_profile.input.check_login.timeout))
                            clearTimeout(page_object.action.admin.action.profile_item_data.action.add_profile.input.check_login.timeout);

                        page_object.action.admin.action.profile_item_data.action.add_profile.input.check_login.timeout=setTimeout(page_object.action.admin.action.profile_item_data.action.add_profile.input.check_login.send,500);

                    },
                    'send':function(){

                        if($d('dialog_login_input_text').value.length>=3&&$d('dialog_login_input_text').value.length<=32){

                            send({
                                'scriptPath':'/api/json/isset_user_login',
                                'postData':'login='+uniEncode($d('dialog_login_input_text').value),
                                'onComplete':function(j,worktime){

                                    var  dataTemp
                                        ,data;

                                    dataTemp=j.responseText;
                                    trace(dataTemp);
                                    data=jsonDecode(dataTemp);
                                    trace(data);
                                    trace_worktime(worktime,data);

                                    if(isset(data['error'])){

                                        page_object.action.admin.action.profile_item_data.action.add_profile.input.check_login.success=false;

                                        $s('dialog_login_input_text').borderColor='#e14141';

                                    }
                                    else if(data['isset_login']){

                                        page_object.action.admin.action.profile_item_data.action.add_profile.input.check_login.success=false;

                                        $s('dialog_login_input_text').borderColor='#e14141';

                                    }
                                    else{

                                        page_object.action.admin.action.profile_item_data.action.add_profile.input.check_login.success=true;

                                        $s('dialog_login_input_text').borderColor='#84cf30';

                                    }

                                }
                            });

                        }
                        else{

                            $s('dialog_login_input_text').borderColor='#e14141';

                            page_object.action.admin.action.profile_item_data.action.add_profile.input.check_login.success=false;

                            return false;

                        }

                    }
                }
            },
            'send':{
                'init':function(){

                    let is_error=false;

                    if(!page_object.action.admin.action.profile_item_data.action.add_profile.input.check_login.success){

                        is_error=true;

                        $s('dialog_login_input_text').borderColor='#ff0000';

                    }

                    if(!page_object.action.admin.action.profile_item_data.action.add_profile.input.check_email.success){

                        is_error=true;

                        $s('dialog_email_input_text').borderColor='#ff0000';

                    }

                    if(!is_error)
                        page_object.action.admin.action.profile_item_data.action.add_profile.send.submit();

                },
                'submit':function(){

                    let  dialog_index   =page_object.action.admin.action.profile_item_data.action.add_profile.dialog_index
                        ,lang_obj       =page_object.dialog.content[page_object.lang]
                        ,post           ='';

                    if(isset($d('dialog_submit_add_'+dialog_index))){

                        $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_add_'+dialog_index).innerHTML    =lang_obj['wait'];
                        $d('dialog_submit_add_'+dialog_index).onclick      =function(){};

                    }

                    if(isset($d('dialog_submit_cancel_'+dialog_index))){

                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    }

                    post+='login='+uniEncode($v('dialog_login_input_text'));
                    post+='&email='+uniEncode($v('dialog_email_input_text'));
                    post+='&name='+uniEncode($v('dialog_surname_input_text'));
                    post+='&surname='+uniEncode($v('dialog_name_input_text'));

                    send({
                        'scriptPath':'/api/json/add_profile_invite',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if(isset($d('dialog_login_input_text')))
                                    $s('dialog_login_input_text').borderColor='#ff0000';

                                if(isset($d('dialog_email_input_text')))
                                    $s('dialog_email_input_text').borderColor='#ff0000';

                                if($d('dialog_submit_add_'+dialog_index)){

                                    $d('dialog_submit_add_'+dialog_index).onclick=function(){

                                        page_object.action.admin.action.profile_item_data.action.add_profile.send.init(page_object.action.admin.action.profile_item_data.action.add_profile.dialog_index);

                                    };
                                    $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_add');
                                    $d('dialog_submit_add_'+dialog_index).innerHTML=lang_obj['add'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                            }
                            else
                                page_object.action.admin.action.profile_item_data.action.add_profile.send.prepare(dialog_index,data);

                        }
                    });

                },
                'prepare':function(dialog_index,profile_data){

                    var  profile_index    =0
                        ,el
                        ,inner;

                    page_object.action.admin.data['data']['profile_list'].unshift(profile_data['data']['user']);

                    inner=page_object.action.admin.create.profile.create.get_profile_item_data_row(profile_index);

                    page_object.action.admin.action.profile_item_data.action.rename_profile.move_down();

                    el=addElement({
                        'tag':'div',
                        'id':'profile_item_'+profile_index,
                        'class':'profile_item',
                        'style':'opacity: 0; height: 0px;',
                        'inner':inner
                    });

                    if(page_object.action.admin.data['data']['profile_list'].length===1)
                        $d('profile_list').appendChild(el);
                    else
                        $d('profile_list').insertBefore(el,$d('profile_item_1'));

                    setTimeout(function(){

                        if(isset($d('profile_item_'+profile_index))){

                            $s('profile_item_'+profile_index).opacity     =1;
                            $s('profile_item_'+profile_index).height      ='40px';

                        }

                    },500);

                    page_object.action.admin.action.profile_item_data.action.init();
                    page_object.dialog.action.un_show.init(dialog_index,true);

                }
            },
            'un_show':function(){

                if(!empty(page_object.action.admin.action.profile_item_data.action.add_profile.dialog_index))
                    page_object.dialog.action.un_show.init(page_object.action.admin.action.profile_item_data.action.add_profile.dialog_index,true);

            }
        },
        'edit_profile':{
            'init':function(){

                let  list           =page_object.action.admin.data['data']['profile_list']
                    ,index;

                for(index in list)
                    if(isset($d('profile_item_edit_'+index)))
                        $d('profile_item_edit_'+index).onclick=page_object.action.admin.action.profile_item_data.action.edit_profile.box.init;

            },
            'box':{
                'dialog_index':null,
                'user_ID':null,
                'init':function(){

                    let  inner                  =''
                        ,profile_index          =this.id.split('_')[3]
                        ,data                   =page_object.action.admin.data['data']
                        ,profile_list           =data['profile_list']
                        ,profile_data           =profile_list[profile_index];

                    page_object.action.admin.action.profile_item_data.action.edit_profile.box.profile_index     =profile_index;
                    page_object.action.admin.action.profile_item_data.action.edit_profile.box.user_ID           =profile_data['ID'];

                    inner+='<div id="reg_block" class="dialog_row">';

                    inner+='<div class="dialog_row_label">';
                    inner+='<span>Login</span>';
                    inner+='<span class="dialog_red_field">*</span>';
                    inner+='</div>';
                    inner+='<div class="dialog_row_input">';
                    inner+='<input type="text" id="dialog_login_input_text" class="dialog_row_input_text" value="'+(empty(profile_data['login'])?'':profile_data['login'])+'" />';
                    inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="dialog_row">';

                    inner+='<div class="dialog_row_label">';
                    inner+='<span>Email</span>';
                    inner+='<span class="dialog_red_field">*</span>';
                    inner+='</div>';
                    inner+='<div class="dialog_row_input">';
                    inner+='<input type="text" id="dialog_email_input_text" class="dialog_row_input_text" value="'+(empty(profile_data['email'])?'':profile_data['email'])+'" />';
                    inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="dialog_row">';

                    inner+='<div class="dialog_row_label">';
                    inner+='<span>Фамилия</span>';
                    inner+='</div>';
                    inner+='<div class="dialog_row_input">';
                    inner+='<input type="text" id="dialog_surname_input_text" class="dialog_row_input_text" value="'+(empty(profile_data['surname'])?'':profile_data['surname'])+'" />';
                    inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="dialog_row">';

                    inner+='<div class="dialog_row_label">';
                    inner+='<span>Имя</span>';
                    inner+='</div>';
                    inner+='<div class="dialog_row_input">';
                    inner+='<input type="text" id="dialog_name_input_text" class="dialog_row_input_text" value="'+(empty(profile_data['name'])?'':profile_data['name'])+'" />';
                    inner+='</div>';

                    inner+='</div>';

                    page_object.action.admin.action.profile_item_data.action.edit_profile.box.dialog_index=page_object.dialog.init({
                        'title':'Редактирование пользователя',
                        'w':page_object.action.admin.action.profile_item_data.action.edit_profile.box.width,
                        'inner':inner,
                        'save':function(){
                            
                            page_object.action.admin.action.profile_item_data.action.edit_profile.edit.init();
                            
                        },
                        'on_create':function(){

                            page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.init();

                        },
                        'cancel':true
                    });

                },
                'input':{
                    'init':function(){

                        if(isset($d('dialog_login_input_text')))
                            $d('dialog_login_input_text').onkeyup=page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_login.init;

                        if(isset($d('dialog_email_input_text')))
                            $d('dialog_email_input_text').onkeyup=page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_email.init;

                    },
                    'check_email':{
                        'success':true,
                        'timeout':null,
                        'init':function(){

                            if(isset(page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_email.timeout))
                                clearTimeout(page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_email.timeout);

                            page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_email.timeout=setTimeout(page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_email.send,500);

                        },
                        'send':function(){

                            if(isEmail($d('dialog_email_input_text').value)){

                                let post='';

                                post+='user_ID='+uniEncode(page_object.action.admin.action.profile_item_data.action.edit_profile.box.user_ID);
                                post+='&email='+uniEncode($d('dialog_email_input_text').value);

                                send({
                                    'scriptPath':'/api/json/isset_user_email',
                                    'postData':post,
                                    'onComplete':function(j,worktime){

                                        var  dataTemp
                                            ,data;

                                        dataTemp=j.responseText;
                                        trace(dataTemp);
                                        data=jsonDecode(dataTemp);
                                        trace(data);
                                        trace_worktime(worktime,data);

                                        if(isset(data['error'])){

                                            page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_email.success=false;

                                            $s('dialog_email_input_text').borderColor='#e14141';

                                        }
                                        else if(data['isset_email']){

                                            page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_email.success=false;

                                            $s('dialog_email_input_text').borderColor='#e14141';

                                        }
                                        else{

                                            page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_email.success=true;

                                            $s('dialog_email_input_text').borderColor='#84cf30';

                                        }

                                    }
                                });

                                return true;

                            }
                            else{

                                $s('dialog_email_input_text').borderColor='#e14141';

                                page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_email.success=false;

                                return false;

                            }

                        }
                    },
                    'check_login':{
                        'success':true,
                        'timeout':null,
                        'init':function(){

                            if(isset(page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_login.timeout))
                                clearTimeout(page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_login.timeout);

                            page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_login.timeout=setTimeout(page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_login.send,500);

                        },
                        'send':function(){

                            if($d('dialog_login_input_text').value.length>=3&&$d('dialog_login_input_text').value.length<=32){

                                let post='';

                                post+='user_ID='+uniEncode(page_object.action.admin.action.profile_item_data.action.edit_profile.box.user_ID);
                                post+='&login='+uniEncode($d('dialog_login_input_text').value);

                                send({
                                    'scriptPath':'/api/json/isset_user_login',
                                    'postData':post,
                                    'onComplete':function(j,worktime){

                                        var  dataTemp
                                            ,data;

                                        dataTemp=j.responseText;
                                        trace(dataTemp);
                                        data=jsonDecode(dataTemp);
                                        trace(data);
                                        trace_worktime(worktime,data);

                                        if(isset(data['error'])){

                                            page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_login.success=false;

                                            $s('dialog_login_input_text').borderColor='#e14141';

                                        }
                                        else if(data['isset_login']){

                                            page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_login.success=false;

                                            $s('dialog_login_input_text').borderColor='#e14141';

                                        }
                                        else{

                                            page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_login.success=true;

                                            $s('dialog_login_input_text').borderColor='#84cf30';

                                        }

                                    }
                                });

                            }
                            else{

                                $s('dialog_login_input_text').borderColor='#e14141';

                                page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_login.success=false;

                                return false;

                            }

                        }
                    }
                },
                'send':{
                    'init':function(){

                        let is_error=false;

                        if(!page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_login.success){

                            is_error=true;

                            $s('dialog_login_input_text').borderColor='#ff0000';

                        }

                        if(!page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_email.success){

                            is_error=true;

                            $s('dialog_email_input_text').borderColor='#ff0000';

                        }

                        if(!is_error)
                            page_object.action.admin.action.profile_item_data.action.edit_profile.send.submit();

                    },
                    'submit':function(){

                        let  dialog_index   =page_object.action.admin.action.profile_item_data.action.edit_profile.dialog_index
                            ,lang_obj       =page_object.dialog.content[page_object.lang]
                            ,post           ='';

                        if(isset($d('dialog_submit_add_'+dialog_index))){

                            $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_disable');

                            $d('dialog_submit_add_'+dialog_index).innerHTML    =lang_obj['wait'];
                            $d('dialog_submit_add_'+dialog_index).onclick      =function(){};

                        }

                        if(isset($d('dialog_submit_cancel_'+dialog_index))){

                            $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                            $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                        }

                        post+='login='+uniEncode($v('dialog_login_input_text'));
                        post+='&email='+uniEncode($v('dialog_email_input_text'));
                        post+='&name='+uniEncode($v('dialog_surname_input_text'));
                        post+='&surname='+uniEncode($v('dialog_name_input_text'));

                        send({
                            'scriptPath':'/api/json/add_profile_invite',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                let  dataTemp
                                    ,data;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    if(isset($d('dialog_login_input_text')))
                                        $s('dialog_login_input_text').borderColor='#ff0000';

                                    if(isset($d('dialog_email_input_text')))
                                        $s('dialog_email_input_text').borderColor='#ff0000';

                                    if($d('dialog_submit_add_'+dialog_index)){

                                        $d('dialog_submit_add_'+dialog_index).onclick=function(){

                                            page_object.action.admin.action.profile_item_data.action.edit_profile.send.init(page_object.action.admin.action.profile_item_data.action.edit_profile.dialog_index);

                                        };
                                        $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_add');
                                        $d('dialog_submit_add_'+dialog_index).innerHTML=lang_obj['add'];

                                    }

                                    if($d('dialog_submit_cancel_'+dialog_index)){

                                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                            page_object.dialog.action.un_show.init(dialog_index,true);

                                        };
                                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                    }

                                }
                                else
                                    page_object.action.admin.action.profile_item_data.action.edit_profile.send.prepare(dialog_index,data);

                            }
                        });

                    }
                }
            },
            'edit':{
                'init':function(){

                    let is_error=false;

                    if(!page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_login.success){

                        is_error=true;

                        $s('dialog_login_input_text').borderColor='#ff0000';

                    }

                    if(!page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.check_email.success){

                        is_error=true;

                        $s('dialog_email_input_text').borderColor='#ff0000';

                    }

                    if(!is_error)
                        page_object.action.admin.action.profile_item_data.action.edit_profile.edit.submit();

                },
                'submit':function(){

                    let  dialog_index   =page_object.action.admin.action.profile_item_data.action.edit_profile.box.dialog_index
                        ,lang_obj       =page_object.dialog.content[page_object.lang]
                        ,post           ='';

                    if(isset($d('dialog_submit_save_'+dialog_index))){

                        $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_save_'+dialog_index).innerHTML    =lang_obj['wait'];
                        $d('dialog_submit_save_'+dialog_index).onclick      =function(){};

                    }

                    if(isset($d('dialog_submit_cancel_'+dialog_index))){

                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    }

                    post+='&user_ID='+uniEncode(page_object.action.admin.action.profile_item_data.action.edit_profile.box.user_ID);
                    post+='&user_login='+uniEncode($v('dialog_login_input_text'));
                    post+='&user_email='+uniEncode($v('dialog_email_input_text'));
                    post+='&user_name='+uniEncode($v('dialog_surname_input_text'));
                    post+='&user_surname='+uniEncode($v('dialog_name_input_text'));

                    send({
                        'scriptPath':'/api/json/update_profile',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if(isset($d('dialog_login_input_text')))
                                    $s('dialog_login_input_text').borderColor='#ff0000';

                                if(isset($d('dialog_email_input_text')))
                                    $s('dialog_email_input_text').borderColor='#ff0000';

                                if($d('dialog_submit_save_'+dialog_index)){

                                    $d('dialog_submit_save_'+dialog_index).onclick=function(){

                                        page_object.action.admin.action.profile_item_data.action.edit_profile.edit.init();

                                    };
                                    $d('dialog_submit_save_'+dialog_index).setAttribute('class','dialog_submit_save');
                                    $d('dialog_submit_save_'+dialog_index).innerHTML=lang_obj['save'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                            }
                            else
                                page_object.action.admin.action.profile_item_data.action.edit_profile.edit.prepare(data);

                        }
                    });

                },
                'prepare':function(profile_data){

                    let  profile_index      =page_object.action.admin.action.profile_item_data.action.edit_profile.box.profile_index
                        ,dialog_index       =page_object.action.admin.action.profile_item_data.action.edit_profile.box.dialog_index
                        ,inner;

                    page_object.action.admin.data['data']['profile_list'][profile_index]=profile_data['user'];

                    inner=page_object.action.admin.create.profile.create.get_profile_item_data_row(profile_index);

                    $d('profile_item_'+profile_index).innerHTML=inner;

                    page_object.action.admin.action.profile_item_data.action.init();

                    page_object.dialog.action.un_show.init(dialog_index,true);

                }
            }
        },
        'remove_profile':{
            'init':function(){

                let  list           =page_object.action.admin.data['data']['profile_list']
                    ,index;

                for(index in list)
                    if(isset($d('profile_item_remove_'+index)))
                        $d('profile_item_remove_'+index).onclick=page_object.action.admin.action.profile_item_data.action.remove_profile.box.init;

            },
            'box':{
                'init':function(){

                    let  inner          =''
                        ,index          =this.id.split('_')[3]
                        ,data           =page_object.action.admin.data['data']
                        ,profile_data      =data['profile_list'][index];

                    inner+='<div class="dialog_row">';
                        inner+='Удалить пользователя #'+profile_data['ID']+'?';
                    inner+='</div>';

                    var dialog_index=page_object.dialog.init({
                        'title':'Удаление пользователя',
                        'inner':inner,
                        'remove':function(){

                            page_object.action.admin.action.profile_item_data.action.remove_profile.remove.init(dialog_index,index);

                        },
                        'on_create':function(){

                        },
                        'cancel':true
                    });

                }
            },
            'remove':{
                'init':function(dialog_index,index){
                    
                    page_object.action.admin.action.profile_item_data.action.remove_profile.remove.send(dialog_index,index);

                },
                'send':function(dialog_index,index){

                    var lang_obj=page_object.action.admin.content[page_object.lang];

                    if(isset($d('dialog_submit_remove_'+dialog_index))){

                        $d('dialog_submit_remove_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_remove_'+dialog_index).innerHTML    =lang_obj['wait'];
                        $d('dialog_submit_remove_'+dialog_index).onclick      =function(){};

                    }

                    if(isset($d('dialog_submit_cancel_'+dialog_index))){

                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    }

                    let  post       =''
                        ,list       =page_object.action.admin.data['data']['profile_list']
                        ,data       =list[index];

                    post+='user_ID='+uniEncode(data['ID']);

                    send({
                        'scriptPath':'/api/json/remove_profile',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if($d('dialog_submit_remove_'+dialog_index)){

                                    $d('dialog_submit_remove_'+dialog_index).onclick=function(){

                                        page_object.action.admin.action.profile_item_data.action.remove_profile.remove.init(dialog_index,index);

                                    };
                                    $d('dialog_submit_remove_'+dialog_index).setAttribute('class','dialog_submit_remove');
                                    $d('dialog_submit_remove_'+dialog_index).innerHTML=lang_obj['remove'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                            }
                            else
                                page_object.action.admin.action.profile_item_data.action.remove_profile.remove.prepare(dialog_index,index);

                        }
                    });

                },
                'prepare':function(dialog_index,index){

                    if(isset($d('profile_item_'+index)))
                        removeElement($d('profile_item_'+index));

                    page_object.action.admin.data['data']['profile_list'].splice(index,1);

                    page_object.action.admin.action.profile_item_data.action.rename_profile.move_up(index);

                    page_object.action.admin.action.profile_item_data.action.init();

                    page_object.dialog.action.un_show.init(dialog_index,true);

                }
            }
        },
        'balance':{
            'init':function(){

                let  list           =page_object.action.admin.data['data']['profile_list']
                    ,index;

                for(index in list)
                    if(isset($d('profile_item_wallet_'+index)))
                        $d('profile_item_wallet_'+index).onclick=page_object.action.admin.action.profile_item_data.action.balance.box.init;

            },
            'box':{
                'dialog_index':null,
                'user_ID':null,
                'init':function(){

                    let  inner                  =''
                        ,profile_index          =this.id.split('_')[3]
                        ,data                   =page_object.action.admin.data['data']
                        ,profile_list           =data['profile_list']
                        ,profile_data           =profile_list[profile_index];

                    page_object.action.admin.action.profile_item_data.action.balance.box.profile_index     =profile_index;
                    page_object.action.admin.action.profile_item_data.action.balance.box.user_ID           =profile_data['ID'];

                    inner+='<div id="reg_block" class="dialog_row">';

                    inner+='<div class="dialog_row_label">';
                    inner+='<span>Действие</span>';
                    inner+='<span class="dialog_red_field">*</span>';
                    inner+='</div>';
                    inner+='<div class="dialog_row_input">';
                    inner+='<select id="dialog_action_input_select" class="dialog_row_input_text">';
                        inner+='<option value="0">Выберите действие</option>';
                        inner+='<option value="add_hand_money">Пополнить баланс</option>';
                        inner+='<option value="remove_hand_money">Списать средства</option>';
                    inner+='</select>';
                    inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="dialog_row">';

                    inner+='<div class="dialog_row_label">';
                    inner+='<span>Сумма, RUB</span>';
                    inner+='<span class="dialog_red_field">*</span>';
                    inner+='</div>';
                    inner+='<div class="dialog_row_input">';
                    inner+='<input type="number" id="dialog_balance_input_text" class="dialog_row_input_text" value="1" min="1" />';
                    inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="dialog_row">';

                    inner+='<div class="dialog_row_label">';
                    inner+='<span>Примечание</span>';
                    inner+='</div>';
                    inner+='<div class="dialog_row_input">';
                    inner+='<textarea id="dialog_info_input_text" class="dialog_row_textarea"></textarea>';
                    inner+='</div>';

                    inner+='</div>';

                    page_object.action.admin.action.profile_item_data.action.balance.box.dialog_index=page_object.dialog.init({
                        'title':'Пополнение баланса',
                        'w':page_object.action.admin.action.profile_item_data.action.edit_profile.box.width,
                        'inner':inner,
                        'add':function(){

                            page_object.action.admin.action.profile_item_data.action.balance.add.init();

                        },
                        'on_create':function(){

                            page_object.action.admin.action.profile_item_data.action.edit_profile.box.input.init();

                        },
                        'cancel':true
                    });

                }
            },
            'add':{
                'init':function(){

                    let  is_error=false;

                    if(empty($v('dialog_action_input_select'))){

                        $s('dialog_action_input_select').borderColor='#ff0000';

                        is_error=true;

                    }

                    if(parseInt($v('dialog_balance_input_text'))<1){

                        $s('dialog_login_input_text').borderColor='#ff0000';

                        is_error=true;

                    }

                    if(!is_error)
                        page_object.action.admin.action.profile_item_data.action.balance.add.send();

                },
                'send':function(){

                    let  dialog_index   =page_object.action.admin.action.profile_item_data.action.balance.dialog_index
                        ,user_ID        =page_object.action.admin.action.profile_item_data.action.balance.box.user_ID
                        ,lang_obj       =page_object.dialog.content[page_object.lang]
                        ,post           ='';

                    if(isset($d('dialog_submit_add_'+dialog_index))){

                        $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_add_'+dialog_index).innerHTML    =lang_obj['wait'];
                        $d('dialog_submit_add_'+dialog_index).onclick      =function(){};

                    }

                    if(isset($d('dialog_submit_cancel_'+dialog_index))){

                        $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                        $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                    }

                    post+='user_ID='+uniEncode(user_ID);
                    post+='&action='+uniEncode($v('dialog_action_input_select'));
                    post+='&balance='+uniEncode($v('dialog_balance_input_text'));
                    post+='&info='+uniEncode($v('dialog_info_input_text'));

                    send({
                        'scriptPath':'/api/json/update_profile_balance',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  dataTemp
                                ,data;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if(isset($d('dialog_login_input_text')))
                                    $s('dialog_login_input_text').borderColor='#ff0000';

                                if(isset($d('dialog_email_input_text')))
                                    $s('dialog_email_input_text').borderColor='#ff0000';

                                if($d('dialog_submit_add_'+dialog_index)){

                                    $d('dialog_submit_add_'+dialog_index).onclick=function(){

                                        page_object.action.admin.action.profile_item_data.action.edit_profile.send.init(page_object.action.admin.action.profile_item_data.action.edit_profile.dialog_index);

                                    };
                                    $d('dialog_submit_add_'+dialog_index).setAttribute('class','dialog_submit_add');
                                    $d('dialog_submit_add_'+dialog_index).innerHTML=lang_obj['add'];

                                }

                                if($d('dialog_submit_cancel_'+dialog_index)){

                                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){

                                        page_object.dialog.action.un_show.init(dialog_index,true);

                                    };
                                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_cancel');

                                }

                            }
                            else
                                page_object.action.admin.action.profile_item_data.action.balance.add.prepare(data);

                        }
                    });

                },
                'prepare':function(profile_data){

                    let  profile_index      =page_object.action.admin.action.profile_item_data.action.balance.box.profile_index
                        ,dialog_index       =page_object.action.admin.action.profile_item_data.action.balance.box.dialog_index
                        ,inner;

                    page_object.action.admin.data['data']['profile_list'][profile_index]['balance']=profile_data['data']['balance'];;

                    inner=page_object.action.admin.create.profile.create.get_profile_item_data_row(profile_index);

                    $d('profile_item_'+profile_index).innerHTML=inner;

                    page_object.action.admin.action.profile_item_data.action.init();

                    page_object.dialog.action.un_show.init(dialog_index,true);

                }
            }
        },
        'scroll':{
            'event':null,
            'is_work':false,
            'init':function(){

                if(isset( page_object.action.admin.action.profile_item_data.action.scroll.event))
                     page_object.action.admin.action.profile_item_data.action.scroll.remove_listener();

                 page_object.action.admin.action.profile_item_data.action.scroll.event=listener.set(window,'scroll',function(event){

                     page_object.action.admin.action.profile_item_data.action.scroll.send(event);

                });

            },
            'send':function(e){

                if(isset($d('profile_item')))
                    if(!page_object.action.admin.action.profile_item_data.action.scroll.is_work){

                         page_object.action.admin.action.profile_item_data.action.scroll.is_work=true;

                        let  scroll_position        =get_document_scroll_position()
                            ,win_height             =winSize.winHeight
                            ,doc_height             =document.documentElement.scrollHeight
                            ,delta                  =doc_height-win_height-scroll_position;

                        if(delta>10){

                             page_object.action.admin.action.profile_item_data.action.scroll.is_work=false;

                            return false;

                        }

                        let  profile_list                =page_object.action.admin.data['data']['profile_list']
                            ,profile_data
                            ,city                       =get_url_param('city')
                            ,post                       ='';

                        if(profile_list.length===0)
                            return false;

                        profile_data=profile_list[profile_list.length-1];

                        post+='user_ID='+uniEncode(profile_data['ID']);

                        if(!empty(city))
                            post+='&city='+uniEncode(city);

                        if(isset($d('profile_search_input')))
                            if(!empty($v('profile_search_input')))
                                post+='&search='+uniEncode($v('profile_search_input'));

                        send({
                            'scriptPath':'/api/json/get_profile_list',
                            'postData':post,
                            'onComplete':function(j,worktime){

                                let  dataTemp
                                    ,data;

                                dataTemp=j.responseText;
                                trace(dataTemp);
                                data=jsonDecode(dataTemp);
                                trace(data);
                                trace_worktime(worktime,data);

                                if(isset(data['error'])){

                                    trace('ERROR LOAD');

                                     page_object.action.admin.action.profile_item_data.action.scroll.is_work=false;

                                }
                                else{

                                     page_object.action.admin.action.profile_item_data.action.scroll.prepare(data);

                                }

                            }
                        });

                    }

            },
            'prepare':function(data){

                data=data['data'];

                if(data['profile_list'].length>0){

                    let  profile_list      =page_object.action.admin.data['data']['profile_list']
                        ,profile_index     =profile_list.length
                        ,index
                        ,profile_len
                        ,el
                        ,func
                        ,num            =0
                        ,inner;

                    page_object.action.admin.data['data']['profile_list']=page_object.action.admin.data['data']['profile_list'].concat(data['profile_list']);

                    profile_len=page_object.action.admin.data['data']['profile_list'].length;

                    for(index=profile_index;index<profile_len;index++){

                        inner=page_object.action.admin.create.profile.create.get_profile_item_data_row(index);

                        el=addElement({
                            'tag':'div',
                            'id':'profile_item_'+index,
                            'class':'profile_item',
                            'inner':inner
                        });

                        $d('profile_list').appendChild(el);

                    }

                }

                page_object.action.admin.action.profile_item_data.action.scroll.is_work=false;

                page_object.action.admin.action.profile_item_data.action.init();

            },
            'remove_listener':function(){

                listener.remove( page_object.action.admin.action.profile_item_data.action.scroll.event);

            }
        },
        'search':{
            'timeout':null,
            'init':function(){

                if(isset($d('profile_search_input')))
                    $d('profile_search_input').onkeyup=page_object.action.admin.action.profile_item_data.action.search.start;

            },
            'reset':function(){

                if(isset(page_object.action.admin.action.profile_item_data.action.search.timeout))
                    clearTimeout(page_object.action.admin.action.profile_item_data.action.search.timeout);

                page_object.action.admin.action.profile_item_data.action.search.timeout=null;

            },
            'start':function(){

                page_object.action.admin.action.profile_item_data.action.search.reset();

                page_object.action.admin.action.profile_item_data.action.search.timeout=setTimeout(page_object.action.admin.action.profile_item_data.action.search.send,500);

            },
            'send':function(){

                let post='';

                post+='search='+uniEncode($v('profile_search_input'));

                send({
                    'scriptPath':'/api/json/get_profile_list',
                    'postData':post,
                    'onComplete':function(j,worktime){

                        var  dataTemp
                            ,data;

                        dataTemp=j.responseText;
                        trace(dataTemp);
                        data=jsonDecode(dataTemp);
                        trace(data);
                        trace_worktime(worktime,data);

                        if(isset(data['error'])){

                            trace('ERROR LOAD');

                        }
                        else{

                            if(isset($d('profile_list'))){

                                $s('profile_list').opacity  =0;
                                $s('profile_list').height   =0;

                            }

                            setTimeout(function(){

                                page_object.action.admin.action.profile_item_data.action.search.prepare(data);

                            },300);

                        }

                    }
                });


            },
            'prepare':function(data){

                if(isset($d('profile_list'))){

                    $d('profile_list').innerHTML    ='';
                    $s('profile_list').height       ='auto';

                    page_object.action.admin.data['data']['profile_list']=data['data']['profile_list'];

                    let  profile_list       =page_object.action.admin.data['data']['profile_list']
                        ,profile_len        =profile_list.length
                        ,index
                        ,inner
                        ,el;

                    for(index=0;index<profile_len;index++){

                        inner=page_object.action.admin.create.profile.create.get_profile_item_data_row(index);

                        el=addElement({
                            'tag':'div',
                            'id':'profile_item_'+index,
                            'class':'profile_item',
                            'inner':inner
                        });

                        $d('profile_list').appendChild(el);

                    }

                    $s('profile_list').opacity=1;

                    page_object.action.admin.action.profile_item_data.action.init();

                }

            }
        },
        'search_history':{
            'dialog_index':null,
            'list':[],
            'init':function(){

                let  list       =page_object.action.admin.data['data']['profile_list']
                    ,index;

                for(index in list)
                    if(isset($d('profile_item_info_'+index)))
                        $d('profile_item_info_'+index).onclick=page_object.action.admin.action.profile_item_data.action.search_history.history.init;

            },
            'history':{
                'init':function(){

                    let  list               =page_object.action.admin.data['data']['profile_list']
                        ,index              =parseInt(this.id.split('_')[3])
                        ,profile_data       =list[index]
                        ,post               ='';

                    post+='user_ID='+uniEncode(profile_data['ID']);

                    send({
                        'scriptPath':'/api/json/get_profile_face_search_history',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  data
                                ,dataTemp;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                            }
                            else{

                                page_object.action.admin.action.profile_item_data.action.search_history.history.prepare(data['data']);

                            }

                        }
                    });

                },
                'get_search_history_row':function(index){

                    let  list                   =page_object.action.admin.action.profile_item_data.action.search_history.list
                        ,face_data              =list[index]
                        ,face_index
                        ,image_source_link      =empty(face_data['image'])?null:('/'+face_data['image']['image_dir']+'/'+face_data['image']['image_item_ID_list']['preview']['ID'])
                        ,image_dest_link
                        ,name                   =''
                        ,percent
                        ,percent_min            =100
                        ,percent_max            =0
                        ,inner                  ='';

                    if(OS.isMobile){

                        inner+='<div class="search_history_item_date_row">'+(empty(face_data['date_create'])?'-':(get_date(face_data['date_create'])+' '+get_time(face_data['date_create'])))+'</div>';

                        inner+='<div class="search_history_item_mobile_row">';

                            inner+='<div class="search_history_item_mobile_col">';

                                inner+='<div class="search_history_item_image" style="background: url('+image_source_link+') 50% 50% no-repeat; background-size: cover;"></div>';

                            inner+='</div>';

                            inner+='<div class="search_history_item_mobile_col_arrow"></div>';

                            inner+='<div class="search_history_item_mobile_col">';

                        if(face_data['percent']>9000){

                            image_dest_link='/'+face_data['face_list'][0]['image']['image_dir']+'/'+face_data['face_list'][0]['image']['image_item_ID_list']['preview']['ID'];

                            if(face_data['percent']>10000)
                                percent=(parseInt(face_data['percent'])/10000).toFixed(2);
                            else
                                percent=(parseInt(face_data['percent'])/100).toFixed(2);

                            if(!empty(face_data['name']))
                                name+=face_data['name'];

                            if(!empty(face_data['age']))
                                name+=(!empty(name)?', ':'')+face_data['age'];

                            if(!empty(face_data['city']))
                                name+=(!empty(name)?', ':'')+face_data['city'];

                            if(name.length>24)
                                name=name.substr(0,24)+'...';

                            inner+='<div class="search_history_item_info">'+stripSlashes(name)+'</div>';
                            inner+='<div class="search_history_item_image'+(empty(name)?'':'_right')+'" style="background: url('+image_dest_link+') 50% 50% no-repeat; background-size: cover;">';
                                inner+='<div class="search_history_item_image_percent">'+percent+'%</div>';
                            inner+='</div>';

                        }
                        else
                            inner+='<div class="search_history_item_info_image_empty"><table><tr><td valign="middle" style="background: url(/show/image/style/admin/system/nun_30.png) 50% 50% no-repeat; background-size: 80%;">Совпадений<br />не найдено</td></tr></table></div>';

                            inner+='</div>';

                        inner+='</div>';

                        if(face_data['face_list'].length>0){

                            for(face_index in face_data['face_list']){

                                if(face_data['face_list'][face_index]['percent']>10000)
                                    percent=(parseInt(face_data['face_list'][face_index]['percent'])/10000).toFixed(2);
                                else
                                    percent=(parseInt(face_data['face_list'][face_index]['percent'])/100).toFixed(2);

                                if(percent_min>percent)
                                    percent_min=percent;

                                if(percent_max<percent)
                                    percent_max=percent;

                            }

                            if(!empty(percent_min)&&!empty(percent_max))
                                inner+='<div class="search_history_item_mobile_row" style="text-align: left;">Похожие девушки, совпадение от '+percent_max+'% до '+percent_min+'%:</div>';
                            else
                                inner+='<div class="search_history_item_mobile_row" style="text-align: left;">Похожие девушки:</div>';

                            inner+='<div class="search_history_item_mobile_row">';

                            inner+='<div class="search_history_item_profile_list">';

                            for(face_index in face_data['face_list']){

                                name='';

                                if(!empty(face_data['face_list'][face_index]['name']))
                                    name+=face_data['face_list'][face_index]['name'];

                                if(!empty(face_data['face_list'][face_index]['age']))
                                    name+=(!empty(name)?', ':'')+face_data['face_list'][face_index]['age'];

                                if(!empty(face_data['face_list'][face_index]['city']))
                                    name+=(!empty(name)?', ':'')+face_data['face_list'][face_index]['city'];

                                image_dest_link='/'+face_data['face_list'][face_index]['image']['image_dir']+'/'+face_data['face_list'][face_index]['image']['image_item_ID_list']['preview']['ID'];

                                inner+='<div id="search_history_item_profile_pdf_'+index+'_'+face_index+'" class="search_history_item_profile_pdf">';

                                    if(face_data['face_list'][face_index]['percent']>10000)
                                        percent=(parseInt(face_data['face_list'][face_index]['percent'])/10000).toFixed(2);
                                    else
                                        percent=(parseInt(face_data['face_list'][face_index]['percent'])/100).toFixed(2);

                                    inner+='<div class="search_history_item_profile_pdf_image" style="background: url('+image_dest_link+') 50% 50% no-repeat; background-size: cover;"></div>';
                                    inner+='<div id="search_history_item_profile_pdf_info_'+index+'_'+face_index+'" class="search_history_item_profile_pdf_info"><table><tr><td>'+name+' - <i>'+percent+'%</i></td></tr></table></div>';
                                    inner+='<div id="search_history_item_profile_pdf_load_'+index+'_'+face_index+'" class="search_history_item_profile_pdf_load">Загрузка...</div>';

                                inner+='</div>';

                            }

                                inner+='</div>';

                            inner+='</div>';

                        }

                    }
                    else{

                        inner+='<div class="search_history_item_col" style="width: 50px;"><table><tr><td>'+face_data['ID']+'</td></tr></table></div>';
                        inner+='<div class="search_history_item_col"><table><tr><td>'+(empty(face_data['date_create'])?'-':(get_date(face_data['date_create'])+'<br />'+get_time(face_data['date_create'])))+'</td></tr></table></div>';
                        inner+='<div class="search_history_item_col">';

                        if(face_data['percent']>10000)
                            percent=(parseInt(face_data['percent'])/10000).toFixed(2);
                        else
                            percent=(parseInt(face_data['percent'])/100).toFixed(2);

                            inner+='<div class="search_history_item_image" style="background: url('+image_source_link+') 50% 50% no-repeat; background-size: cover;"></div>';
                            inner+='<div class="search_history_item_info">'+parseInt(face_data['image']['image_item_ID_list']['large']['file_size']/1024)+'kb '+face_data['image']['image_item_ID_list']['large']['width']+'x'+face_data['image']['image_item_ID_list']['large']['height']+'</div>';

                        inner+='</div>';
                        inner+='<div class="search_history_item_col">';

                        if(face_data['percent']>9000){

                            image_dest_link='/'+face_data['face_list'][0]['image']['image_dir']+'/'+face_data['face_list'][0]['image']['image_item_ID_list']['preview']['ID'];

                            if(!empty(face_data['name']))
                                name+=face_data['name'];

                            if(!empty(face_data['age']))
                                name+=(!empty(name)?', ':'')+face_data['age'];

                            if(!empty(face_data['city']))
                                name+=(!empty(name)?', ':'')+face_data['city'];

                            inner+='<div class="search_history_item_image" style="background: url('+image_dest_link+') 50% 50% no-repeat; background-size: cover;">';
                                inner+='<div class="search_history_item_image_percent">'+percent+'%</div>';
                            inner+='</div>';
                            inner+='<div class="search_history_item_info">'+stripSlashes(name)+'</div>';

                        }
                        else
                            inner+='<div class="search_history_item_info"><table><tr><td style="background: url(/show/image/style/admin/system/nun_30.png) 50% 50% no-repeat; background-size: 80%;">Совпадений<br />не найдено</td></tr></table></div>';

                        inner+='</div>';
                        inner+='<div class="search_history_item_col" style="width: 400px">';

                        if(face_data['face_list'].length>0){

                            inner+='<div class="search_history_item_profile_list">';

                            for(face_index in face_data['face_list']){

                                name='';

                                if(!empty(face_data['face_list'][face_index]['name']))
                                    name+=face_data['face_list'][face_index]['name'];

                                if(!empty(face_data['face_list'][face_index]['age']))
                                    name+=(!empty(name)?', ':'')+face_data['face_list'][face_index]['age'];

                                if(!empty(face_data['face_list'][face_index]['city']))
                                    name+=(!empty(name)?', ':'')+face_data['face_list'][face_index]['city'];

                                image_dest_link='/'+face_data['face_list'][face_index]['image']['image_dir']+'/'+face_data['face_list'][face_index]['image']['image_item_ID_list']['preview']['ID'];

                                inner+='<div id="search_history_item_profile_pdf_'+index+'_'+face_index+'" class="search_history_item_profile_pdf">';

                                    if(face_data['face_list'][face_index]['percent']>10000)
                                        percent=(parseInt(face_data['face_list'][face_index]['percent'])/10000).toFixed(2);
                                    else
                                        percent=(parseInt(face_data['face_list'][face_index]['percent'])/100).toFixed(2);

                                    inner+='<div class="search_history_item_profile_pdf_image" style="background: url('+image_dest_link+') 50% 50% no-repeat; background-size: cover;"></div>';
                                    inner+='<div id="search_history_item_profile_pdf_info_'+index+'_'+face_index+'" class="search_history_item_profile_pdf_info"><table><tr><td>'+name+' - <i>'+percent+'%</i></td></tr></table></div>';
                                    inner+='<div id="search_history_item_profile_pdf_load_'+index+'_'+face_index+'" class="search_history_item_profile_pdf_load">Загрузка...</div>';

                                inner+='</div>';

                            }

                                inner+='</div>';

                        }
                        else
                            inner+='<div class="search_history_item_info"><table><tr><td>Список пуст</td></tr></table></div>';

                        inner+='</div>';

                    }

                    return inner;

                },
                'prepare':function(data){

                    page_object.action.admin.action.profile_item_data.action.search_history.list=data['face_search_history_list'];

                    let  inner      =''
                        ,list       =data['face_search_history_list']
                        ,index;

                    inner+='<div id="search_history_block" style="max-height: calc(100vh - 200px); overflow-y: auto">';

                    if(list.length===0){

                        inner+='<div id="search_history_label_none">Список пуст</div>';

                    }
                    else{

                        if(OS.isMobile){

                            inner+='<div id="search_history_header" style="height: 30px; padding: 3px 0 0 0;">ИСТОРИЯ ЗАПРОСОВ</div>';

                        }
                        else{

                            inner+='<div id="search_history_header">';
                                inner+='<div class="search_history_header_col" style="width: 50px;">ID</div>';
                                inner+='<div class="search_history_header_col">Данные запроса</div>';
                                inner+='<div class="search_history_header_col">Исходное изображение</div>';
                                inner+='<div class="search_history_header_col">Результат запроса</div>';
                                inner+='<div class="search_history_header_col" style="width: 400px">Похожие анкеты</div>';
                            inner+='</div>';

                        }

                            inner+='<div id="search_history_list">';

                        for(index in list){

                            inner+='<div id="search_history_item_'+index+'" class="search_history_item">';

                                inner+=page_object.action.admin.action.profile_item_data.action.search_history.history.get_search_history_row(index);

                            inner+='</div>';

                        }

                            inner+='</div>';

                    }

                        inner+='</div>';

                    page_object.action.admin.action.profile_item_data.action.search_history.dialog_index=page_object.dialog.init({
                        'title':'История запросов пользователя',
                        'w':1100,
                        'inner':inner,
                        'on_create':function(){

                            page_object.action.admin.action.profile_item_data.action.search_history.pdf.init();

                        },
                        'cancel':true
                    });

                }
            },
            'un_show':function(){

                if(isset($d('logo')))
                    $d('logo').click();

            },
            'pdf':{
                'init':function(){

                    let  face_list      =page_object.action.admin.action.profile_item_data.action.search_history.list
                        ,search_index
                        ,face_index;

                    for(search_index in face_list)
                        for(face_index in face_list[search_index]['face_list']){

                        if(isset($d('search_history_item_profile_pdf_'+search_index+'_'+face_index)))
                            $d('search_history_item_profile_pdf_'+search_index+'_'+face_index).onclick=page_object.action.admin.action.profile_item_data.action.search_history.pdf.create;

                    }

                    if(isset($d('settings_close_button')))
                        $d('settings_close_button').onclick=page_object.action.admin.action.profile_item_data.action.search_history.un_show;

                },
                'create':function(){

                    var  face_list          =page_object.action.admin.action.profile_item_data.action.search_history.list
                        ,key_list           =this.id.split('_')
                        ,search_index       =key_list[5]
                        ,face_index         =key_list[6]
                        ,search_data        =face_list[search_index]
                        ,face_data          =search_data['face_list'][face_index]
                        ,post               ='';

                    post+='face_token='+uniEncode(face_data['face_token']);
                    post+='&face_search_multi_token='+uniEncode(search_data['face_search_multi_token']);

                    if(isset($d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index))){

                        $d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).innerHTML   ='Загрузка...';
                        $s('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).opacity     =1;

                    }

                    if(isset($d('search_history_item_profile_pdf_info_'+search_index+'_'+face_index)))
                        $s('search_history_item_profile_pdf_info_'+search_index+'_'+face_index).opacity=0;

                    send({
                        'scriptPath':'/api/json/create_face_pdf_file_combo',
                        'postData':post,
                        'onComplete':function(j,worktime){

                            let  data
                                ,dataTemp;

                            dataTemp=j.responseText;
                            trace(dataTemp);
                            data=jsonDecode(dataTemp);
                            trace(data);
                            trace_worktime(worktime,data);

                            if(isset(data['error'])){

                                if(isset($d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index))){

                                    $d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).innerHTML='Ошибка доступа!';

                                    setTimeout(function(){

                                        $s('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).opacity=0;

                                        setTimeout(function(){

                                            $d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).innerHTML='Загрузка...';

                                        },400);

                                    },3000);

                                }

                                if(isset($d('search_history_item_profile_pdf_info_'+search_index+'_'+face_index)))
                                    $s('search_history_item_profile_pdf_info_'+search_index+'_'+face_index).opacity=1;

                            }
                            else{

                                if(isset($d('search_history_item_profile_pdf_load_'+search_index+'_'+face_index)))
                                    $s('search_history_item_profile_pdf_load_'+search_index+'_'+face_index).opacity=0;

                                if(isset($d('search_history_item_profile_pdf_info_'+search_index+'_'+face_index)))
                                    $s('search_history_item_profile_pdf_info_'+search_index+'_'+face_index).opacity=1;

                                window.open('/show/pdf/find_face/'+data['data']['hash'],"_blank");

                            }

                        }
                    });

                }
            },
        },
        'email_profile':{
            'dialog_index':null,
            'init':function(){

                let  list       =page_object.action.admin.data['data']['profile_list']
                    ,index;

                for(index in list)
                    if(isset($d('profile_item_feedback_'+index)))
                        $d('profile_item_feedback_'+index).onclick=page_object.action.admin.action.profile_item_data.action.email_profile.box;

            },
            'box':function(){

                var  inner      =''
                    ,list       =page_object.action.admin.data['data']['profile_list']
                    ,index      =this.id.split('_')[3]
                    ,data       =list[index];

                inner+='<div id="dialog_email_block" class="dialog_row">';
                    inner+='<div id="dialog_email_label" class="dialog_row_label">';
                        inner+='<span>Email</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_email_input_block" class="dialog_row_input">';
                        inner+='<input type="text" id="dialog_email_input_text" class="dialog_row_input_text" value="'+stripSlashes(data['email'])+'" />';
                    inner+='</div>';
                inner+='</div>';

                inner+='<div id="dialog_content_block" class="dialog_row">';
                    inner+='<div id="dialog_content_label" class="dialog_row_label">';
                        inner+='<span>Сообщение</span>';
                    inner+='</div>';
                    inner+='<div id="dialog_content_input_block" class="dialog_row_input">';
                        inner+='<textarea id="dialog_content_input_text" class="dialog_row_textarea"></textarea>';
                    inner+='</div>';
                inner+='</div>';

                page_object.action.admin.action.profile_item_data.action.email_profile.dialog_index=page_object.dialog.init({
                    'title':'Обратная связь с пользователем '+uniEncode(data['login']),
                    'inner':inner,
                    'send':function(){

                        page_object.action.admin.action.profile_item_data.action.email_profile.send(page_object.action.admin.action.profile_item_data.action.email_profile.dialog_index);

                    },
                    'on_create':function(){

                        if(isset($d('dialog_email_input_text')))
                            $d('dialog_email_input_text').onkeyup=page_object.action.admin.action.profile_item_data.action.email_profile.input.init;

                        if(isset($d('dialog_content_input_text')))
                            $d('dialog_content_input_text').onkeyup=page_object.action.admin.action.profile_item_data.action.email_profile.input.init;

                    }
                });

            },
            'input':{
                'init':function(){

                    switch(this.id){

                        case'dialog_email_input_text':{

                            if(empty(this.value))
                                this.style.borderColor='#ff0000';
                            else if(!isEmail(this.value))
                                this.style.borderColor='#ff0000';
                            else
                                this.style.borderColor='';

                            break;

                        }

                        case'dialog_content_input_text':{

                            if(empty(this.value))
                                this.style.borderColor='#ff0000';
                            else
                                this.style.borderColor='';

                            break;

                        }

                    }

                }
            },
            'send':function(dialog_index){

                if(isset($d('dialog_submit_send_'+dialog_index))){

                    $d('dialog_submit_send_'+dialog_index).setAttribute('class','dialog_submit_disable');

                    $d('dialog_submit_send_'+dialog_index).innerHTML    ='Подождите';
                    $d('dialog_submit_send_'+dialog_index).onclick      =function(){};

                }

                if(isset($d('dialog_submit_cancel_'+dialog_index))){

                    $d('dialog_submit_cancel_'+dialog_index).setAttribute('class','dialog_submit_disable');

                    $d('dialog_submit_cancel_'+dialog_index).onclick=function(){};

                }

                var post='';

                post+='&email='+uniEncode($v('dialog_email_input_text').toLowerCase());
                post+='&content='+uniEncode($v('dialog_content_input_text'));

                send({
                    'scriptPath':'/api/json/send_email_to_profile',
                    'postData':post,
                    'onComplete':function(j,worktime){

                        var  data
                            ,dataTemp;

                        dataTemp=j.responseText;
                        trace(dataTemp);
                        data=jsonDecode(dataTemp);
                        trace(data);
                        trace_worktime(worktime,data);

                        page_object.dialog.action.un_show.init(page_object.action.admin.action.profile_item_data.action.email_profile.dialog_index,true);

                        if(!isset(data['error'])){

                            var inner='';

                            inner+='<div class="dialog_row" style="text-align: center">Ваше сообщение отправлено</div>';

                            var dialog_index=page_object.dialog.init({
                                'title':'Информация',
                                'inner':inner,
                                'ok':function(){

                                    page_object.dialog.action.un_show.init(dialog_index,true);

                                }
                            });

                        }

                    }
                });

            }
        }
    },
    'resize':function(){}
};