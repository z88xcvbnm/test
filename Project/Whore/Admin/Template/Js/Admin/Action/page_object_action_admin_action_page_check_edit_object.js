page_object.action.admin.action.page_check_edit={
    'callback':null,
    'un_callback':null,
    'isset':false,
    'init':function(callback,un_callback){

        if(isset(callback))
            page_object.action.admin.action.page_check_edit.callback=callback;
        else
            page_object.action.admin.action.page_check_edit.callback=null;

        if(isset(un_callback))
            page_object.action.admin.action.page_check_edit.un_callback=un_callback;
        else
            page_object.action.admin.action.page_check_edit.un_callback=null;

        page_object.action.admin.action.page_check_edit.isset=false;

        if(isset(page_object.action.admin.data['excursion']))
            if(page_object.action.admin.data['excursion']['edit'])
                page_object.action.admin.action.page_check_edit.check.excursion.init();

        if(isset(page_object.action.admin.data['place']))
            if(page_object.action.admin.data['place']['edit'])
                page_object.action.admin.action.page_check_edit.check.place.init();

        if(isset(page_object.action.admin.data['object'])&&isset(page_object.action.admin.data['edit']))
            if(page_object.action.admin.data['edit'])
                page_object.action.admin.action.page_check_edit.check.location_content.init();

        if(isset(page_object.action.admin.data['action'])&&isset(page_object.action.admin.data['edit']))
            if(page_object.action.admin.data['action']=='admin_content_about_localization_item'&&page_object.action.admin.data['edit'])
                page_object.action.admin.action.page_check_edit.check.about.init();

        if(!page_object.action.admin.action.page_check_edit.isset){

            if(isset(page_object.action.admin.action.page_check_edit.callback))
                page_object.action.admin.action.page_check_edit.callback();

        }

    },
    'check':{
        'excursion':{
            'init':function(){

                var  inner          =''
                    ,lang_obj       =page_object.action.admin.content[page_object.lang];

                page_object.action.admin.action.page_check_edit.isset=true;

                inner+='<div class="dialog_row_none" style="text-align: center">'+stripSlashes(lang_obj['dialog_page_check_edit_save'])+'</div>';

                var dialog_index=page_object.dialog.init({
                    'title':lang_obj['dialog_information'],
                    'inner':inner,
                    'save':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.callback))
                                page_object.action.admin.action.excursion_item.click.save.init(page_object.action.admin.action.page_check_edit.callback);

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;

                        },600);

                    },
                    'no_save':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.callback))
                                page_object.action.admin.action.page_check_edit.callback();

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;

                        },600);

                    },
                    'cancel':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.un_callback))
                                page_object.action.admin.action.page_check_edit.un_callback();

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;

                        },600);

                    }
                });

            }
        },
        'place':{
            'init':function(){

                var  inner          =''
                    ,lang_obj       =page_object.action.admin.content[page_object.lang];

                page_object.action.admin.action.page_check_edit.isset=true;

                inner+='<div class="dialog_row_none" style="text-align: center">'+stripSlashes(lang_obj['dialog_page_check_edit_save'])+'</div>';

                var dialog_index=page_object.dialog.init({
                    'title':lang_obj['dialog_information'],
                    'inner':inner,
                    'save':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.callback))
                                page_object.action.admin.action.place_item.click.save.init(page_object.action.admin.action.page_check_edit.callback);

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;

                        },600);

                    },
                    'no_save':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.callback))
                                page_object.action.admin.action.page_check_edit.callback();

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;

                        },600);

                    },
                    'cancel':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.un_callback))
                                page_object.action.admin.action.page_check_edit.un_callback();

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;

                        },600);

                    }
                });

            }
        },
        'location_content':{
            'init':function(){

                var  inner          =''
                    ,lang_obj       =page_object.action.admin.content[page_object.lang];

                page_object.action.admin.action.page_check_edit.isset=true;

                inner+='<div class="dialog_row_none" style="text-align: center">'+stripSlashes(lang_obj['dialog_page_check_edit_save'])+'</div>';

                var dialog_index=page_object.dialog.init({
                    'title':lang_obj['dialog_information'],
                    'inner':inner,
                    'save':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.callback))
                                page_object.action.admin.action.location_content.click.save.init(page_object.action.admin.action.page_check_edit.callback);

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;

                        },600);

                    },
                    'no_save':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.callback))
                                page_object.action.admin.action.page_check_edit.callback();

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;


                        },600);

                    },
                    'cancel':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.un_callback))
                                page_object.action.admin.action.page_check_edit.un_callback();

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;

                        },600);

                    }
                });

            }
        },
        'about':{
            'init':function(){

                var  inner          =''
                    ,lang_obj       =page_object.action.admin.content[page_object.lang];

                page_object.action.admin.action.page_check_edit.isset=true;

                inner+='<div class="dialog_row_none" style="text-align: center">'+stripSlashes(lang_obj['dialog_page_check_edit_save'])+'</div>';

                var dialog_index=page_object.dialog.init({
                    'title':lang_obj['dialog_information'],
                    'inner':inner,
                    'save':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.callback))
                                page_object.action.admin.action.about.click.save.init(page_object.action.admin.action.page_check_edit.callback);

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;

                        },600);

                    },
                    'no_save':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.callback))
                                page_object.action.admin.action.page_check_edit.callback();

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;


                        },600);

                    },
                    'cancel':function(){

                        page_object.dialog.action.un_show.init(dialog_index,true);

                        setTimeout(function(){

                            if(isset(page_object.action.admin.action.page_check_edit.un_callback))
                                page_object.action.admin.action.page_check_edit.un_callback();

                            page_object.action.admin.action.page_check_edit.callback        =null;
                            page_object.action.admin.action.page_check_edit.un_callback     =null;

                        },600);

                    }
                });

            }
        }
    }
};