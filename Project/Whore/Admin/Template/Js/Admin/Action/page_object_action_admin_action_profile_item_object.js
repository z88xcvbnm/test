page_object.action.admin.action.profile_item={
    'show':function(){

        if(isset($d('profile_item'))){

            page_object.action.admin.position.profile_item.o=1;

            $s('profile_item').opacity=page_object.action.admin.position.profile_item.o;

            page_object.action.admin.action.profile_item.resize();
            page_object.action.admin.action.profile_content.resize();

        }

        setTimeout(page_object.link.preload.un_show,200);

    },
    'un_show':function(remove){

        if(isset($d('profile_item'))){

            page_object.action.admin.position.profile_item.o=0;

            $s('profile_item').opacity=page_object.action.admin.position.profile_item.o;

            if(isset(remove))
                if(remove)
                    setTimeout(page_object.action.admin.action.profile_item.remove,300);

        }

    },
    'action':{
        'init':function(){}
    },
    'resize':function(){}
};