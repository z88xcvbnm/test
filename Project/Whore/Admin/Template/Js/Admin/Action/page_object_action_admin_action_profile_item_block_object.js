page_object.action.admin.action.profile_item_block={
    'show':function(){

        if(isset($d('profile_item_block'))){

            page_object.action.admin.position.profile_item_block.o      =1;
            page_object.action.admin.position.profile_item_block.y      =0;

            $s('profile_item_block').opacity        =page_object.action.admin.position.profile_item_block.o;
            $s('profile_item_block').transform      ='translate('+page_object.action.admin.position.profile_item_block.x+'px,'+page_object.action.admin.position.profile_item_block.y+'px)';

        }

    },
    'un_show':function(remove){

        if(isset($d('profile_item_block'))){

            page_object.action.admin.position.profile_item_block.o  =0;
            page_object.action.admin.position.profile_item_block.y  =-page_object.action.admin.position.profile_item_block.h;

            $s('profile_item_block').opacity        =page_object.action.admin.position.profile_item_block.o;
            $s('profile_item_block').transform      ='translate('+page_object.action.admin.position.profile_item_block.x+'px,'+page_object.action.admin.position.profile_item_block.y+'px)';

            if(isset(remove))
                if(remove)
                    setTimeout(page_object.action.admin.action.profile_item_block.remove,300);

        }

    },
    'remove':function(){

        if(isset($d('profile_item_block')))
            removeElement($d('profile_item_block'));

    },
    'action':{
        'init':function(){

        },
    },
    'resize':function(){}
};