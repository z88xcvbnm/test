page_object.action.admin.create.profile={
    'init':function(){

        page_object.action.admin.create.profile.position.init();
        page_object.action.admin.create.profile.create.init();
        page_object.action.admin.create.profile.set_action();
        page_object.action.admin.create.profile.show();

    },
    'position':{
        'init':function(){

            page_object.action.admin.create.profile.position.profile_content();
            page_object.action.admin.create.profile.position.profile_item();
            page_object.action.admin.create.profile.position.profile_item_block();

        },
        'profile_content':function(){

            page_object.action.admin.position.profile_content.w     =winSize.winWidth;
            page_object.action.admin.position.profile_content.h     =winSize.winHeight-page_object.action.admin.position.header.h;
            page_object.action.admin.position.profile_content.x     =0;
            page_object.action.admin.position.profile_content.y     =0;
            page_object.action.admin.position.profile_content.o     =0;

        },
        'profile_item':function(){

            page_object.action.admin.position.profile_item.x    =0;
            page_object.action.admin.position.profile_item.w    =page_object.action.admin.position.profile_content.w;
            page_object.action.admin.position.profile_item.h    =page_object.action.admin.position.profile_content.h;
            page_object.action.admin.position.profile_item.y    =0;
            page_object.action.admin.position.profile_item.o    =0;

        },
        'profile_item_block':function(){

            page_object.action.admin.position.profile_item_block.x          =0;
            page_object.action.admin.position.profile_item_block.w          =page_object.action.admin.position.profile_content.w;
            page_object.action.admin.position.profile_item_block.h          =winSize.winHeight-page_object.action.admin.position.header.h;
            page_object.action.admin.position.profile_item_block.y          =0;
            page_object.action.admin.position.profile_item_block.o          =0;

        }
    },
    'create':{
        'init':function(){

            page_object.action.admin.create.profile.create.profile_content();
            page_object.action.admin.create.profile.create.profile_item();
            page_object.action.admin.create.profile.create.profile_item_block();
            page_object.action.admin.create.profile.create.profile_item_data();

        },
        'get_profile_item_data_row':function(profile_index){

            let  data                   =page_object.action.admin.data['data']
                ,profile_list           =data['profile_list']
                ,profile_data           =profile_list[profile_index]
                ,inner                  ='';

            inner+='<div id="profile_item_number_'+profile_index+'" class="profile_item_col" style="width: 50px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="40">'+stripSlashes(empty(profile_data['ID'])?'-':profile_data['ID'])+'</td></tr></table></div>';
            inner+='<div id="profile_item_login_'+profile_index+'" class="profile_item_col"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="40">'+stripSlashes(empty(profile_data['login'])?'-':profile_data['login'])+'</td></tr></table></div>';
            inner+='<div id="profile_item_email_'+profile_index+'" class="profile_item_col"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="40">'+stripSlashes(empty(profile_data['email'])?'-':profile_data['email'])+'</td></tr></table></div>';
            inner+='<div id="profile_item_surname_'+profile_index+'" class="profile_item_col"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="40">'+stripSlashes(empty(profile_data['surname'])?'-':profile_data['surname'])+'</td></tr></table></div>';
            inner+='<div id="profile_item_name_'+profile_index+'" class="profile_item_col" style="width: 150px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="40">'+stripSlashes(empty(profile_data['name'])?'-':profile_data['name'])+'</td></tr></table></div>';
            inner+='<div id="profile_item_balance_'+profile_index+'" class="profile_item_col" style="width: 140px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="40">'+stripSlashes(is_null(profile_data['balance'])?'-':profile_data['balance'])+'</td></tr></table></div>';
            inner+='<div id="profile_item_check_'+profile_index+'" class="profile_item_col" style="width: 140px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="40">'+stripSlashes(profile_data['is_email_confirmed']?'Да':'Нет')+'</td></tr></table></div>';
            inner+='<div id="profile_item_date_'+profile_index+'" class="profile_item_col" style="width: 105px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="40">'+(empty(profile_data['date_online'])?'-':stripSlashes(empty(profile_data['date_online'])?'-':get_date_time(profile_data['date_online'])))+'</td></tr></table></div>';

            inner+='<div id="profile_item_controls_'+profile_index+'" class="profile_item_col" style="width: 140px">';
                inner+='<div id="profile_item_feedback_'+profile_index+'" class="profile_item_feedback"></div>';
                inner+='<div id="profile_item_info_'+profile_index+'" class="profile_item_info"'+(empty(profile_data['percent'])?'':' title="Max percent: '+profile_data['percent']+'%"')+''+(empty(profile_data['percent'])?'':' style="border: 1px solid '+(parseFloat(profile_data['percent'])>=90?'#ff0000':(parseFloat(profile_data['percent'])>=85?'#FFDA00':'#00FF10'))+';"')+'></div>';
                inner+='<div id="profile_item_wallet_'+profile_index+'" class="profile_item_wallet"></div>';
                inner+='<div id="profile_item_edit_'+profile_index+'" class="profile_item_edit"></div>';
                inner+='<div id="profile_item_remove_'+profile_index+'" class="profile_item_remove"></div>';
            inner+='</div>';

            return inner;

        },
        'profile_content':function(){

            if(isset($d('profile_content')))
                return false;

            let  el
                ,inner      =''
                ,style      ='';

            // style+='width: '+page_object.action.admin.position.profile_content.w+'px; ';
            style+='min-height: '+page_object.action.admin.position.profile_content.h+'px; ';
            style+='transform: translate('+page_object.action.admin.position.profile_content.x+'px,'+page_object.action.admin.position.profile_content.y+'px); ';
            style+='opacity: '+page_object.action.admin.position.profile_content.o+'; ';

            el=addElement({
                'tag':'div',
                'id':'profile_content',
                'inner':inner,
                'style':style
            });

            $d('all').appendChild(el);

            return true;

        },
        'profile_item':function(){

            if(isset($d('profile_item')))
                return false;

            let  el
                ,inner                  =''
                ,style                  ='';

            // style+='width: '+page_object.action.admin.position.profile_item.w+'px; ';
            style+='min-height: '+page_object.action.admin.position.profile_item.h+'px; ';
            style+='transform: translate('+page_object.action.admin.position.profile_item.x+'px,'+page_object.action.admin.position.profile_item.y+'px); ';
            style+='opacity: '+page_object.action.admin.position.profile_item.o+'; ';

            el=addElement({
                'tag':'div',
                'id':'profile_item',
                'inner':inner,
                'style':style
            });

            $d('profile_content').appendChild(el);

        },
        'profile_item_block':function(){

            if(isset($d('profile_item_block')))
                return false;

            let  el
                ,inner                  =''
                ,style                  ='';

            // style+='width: '+page_object.action.admin.position.profile_item_block.w+'px; ';
            style+='min-height: '+page_object.action.admin.position.profile_item_block.h+'px; ';
            style+='transform: translate('+page_object.action.admin.position.profile_item_block.x+'px,'+page_object.action.admin.position.profile_item_block.y+'px); ';
            style+='opacity: '+page_object.action.admin.position.profile_item_block.o+'; ';

            inner+='';

            el=addElement({
                'tag':'div',
                'id':'profile_item_block',
                'inner':inner,
                'style':style
            });

            $d('profile_item').appendChild(el);

        },
        'profile_item_data':function(){

            let  data               =page_object.action.admin.data['data']
                ,profile_list       =data['profile_list']
                ,profile_index
                ,func
                ,inner          ='';

            inner+='<div id="profile_block_container" style="min-height: '+page_object.action.admin.position.profile_item_block.h+'px;">';

            inner+='<div id="profile_controls">';

                inner+='<div id="profile_add">Добавить пользователя</div>';
                inner+='<div id="profile_search">';
                    inner+='<input type="text" id="profile_search_input" value="" />';
                inner+='</div>';

            inner+='</div>';

            inner+='<div id="profile_none">Список пользователей пуст</div>';
            inner+='<div id="profile_block">';

            inner+='<div id="profile_header">';
            inner+='<div class="profile_header_col" style="width: 50px;">ID</div>';
            inner+='<div class="profile_header_col">Login</div>';
            inner+='<div class="profile_header_col">Email</div>';
            inner+='<div class="profile_header_col">Фамилия</div>';
            inner+='<div class="profile_header_col" style="width: 150px;">Имя</div>';
            inner+='<div class="profile_header_col" style="width: 140px;">Баланс, RUB</div>';
            inner+='<div class="profile_header_col" style="width: 140px;">Подтвержден email</div>';
            inner+='<div class="profile_header_col" style="width: 105px;">Дата online</div>';
            inner+='<div class="profile_header_col" style="width: 140px"></div>';
            inner+='</div>';

            inner+='<div id="profile_list">';

            for(profile_index in profile_list){

                inner+='<div id="profile_item_'+profile_index+'" class="profile_item">';

                inner+=page_object.action.admin.create.profile.create.get_profile_item_data_row(profile_index);

                inner+='</div>';

            }

            inner+='</div>';
            inner+='</div>';

            inner+='</div>';

            $d('profile_item_block').innerHTML=inner;

        }
    },
    'set_action':function(){

        page_object.action.admin.action.profile_item.action.init();
        page_object.action.admin.action.profile_item_data.action.init();

    },
    'show':function(){

        setTimeout(page_object.action.admin.action.profile_content.show,40);
        setTimeout(page_object.action.admin.action.profile_item.show,300);
        setTimeout(page_object.action.admin.action.profile_item_block.show,800);
        setTimeout(page_object.link.preload.un_show,1000);

    }
};
