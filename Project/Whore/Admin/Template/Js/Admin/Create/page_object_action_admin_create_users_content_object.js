page_object.action.admin.create.users={
    'init':function(){

        page_object.action.admin.create.users.position.init();
        page_object.action.admin.create.users.create();
        page_object.action.admin.create.users.set_action();
        page_object.action.admin.create.users.show();

    },
    'position':{
        'init':function(){

            page_object.action.admin.create.users.position.users_content();
            page_object.action.admin.create.users.position.users_list();
            page_object.action.admin.create.users.position.users_add();

        },
        'users_content':function(){

            page_object.action.admin.position.users_content.w       =winSize.winWidth;
            page_object.action.admin.position.users_content.y       =page_object.action.admin.create.header.h;
            page_object.action.admin.position.users_content.h       =0;
            page_object.action.admin.position.users_content.o       =0;

        },
        'users_list':function(){

            page_object.action.admin.position.users_list.w          =page_object.action.admin.position.users_content.w-page_object.action.admin.position.users_list.m.l-page_object.action.admin.position.users_list.m.r;

            if(page_object.action.admin.position.users_list.w>page_object.action.admin.position.users_list.w_max)
                page_object.action.admin.position.users_list.w=page_object.action.admin.position.users_list.w_max;

            page_object.action.admin.position.users_list.x          =Math.ceil((page_object.action.admin.position.users_content.w-page_object.action.admin.position.users_list.w)/2);
            page_object.action.admin.position.users_list.y          =20;
            page_object.action.admin.position.users_list.h          =0;
            page_object.action.admin.position.users_list.o          =0;

        },
        'users_add':function(){

            page_object.action.admin.position.users_add.x           =page_object.action.admin.position.users_list.x+page_object.action.admin.position.users_list.w-page_object.action.admin.position.users_add.w;
            page_object.action.admin.position.users_add.o           =0

        }
    },
    'get_row':function(i){

        let  inner                  =''
            ,list                   =page_object.action.admin.data['list']
            ,proportion             =page_object.action.admin.position['users_list']['table']
            ,user_list_w            =page_object.action.admin.position.users_list.w
            ,user_list_h            =page_object.action.admin.position.users_list.table.row
            ,users_login_w          =Math.ceil(user_list_w*proportion['login']/100)
            ,users_name_w           =Math.ceil(user_list_w*proportion['name']/100)
            ,users_surname_w        =Math.ceil(user_list_w*proportion['surname']/100)
            ,users_email_w          =Math.ceil(user_list_w*proportion['email']/100)
            ,users_admin_w          =Math.ceil(user_list_w*proportion['admin']/100)
            ,users_bookkeep_w       =Math.ceil(user_list_w*proportion['bookkeep']/100)
            ,users_museum_w         =Math.ceil(user_list_w*proportion['museum']/100)
            ,users_date_create_w    =Math.ceil(user_list_w*proportion['date_create']/100)
            ,users_date_online_w    =Math.ceil(user_list_w*proportion['date_online']/100)
            ,users_controls_w       =Math.ceil(user_list_w*proportion['controls']/100)
            ,users_login_x          =0
            ,users_name_x           =users_login_x+users_login_w
            ,users_surname_x        =users_name_x+users_name_w
            ,users_email_x          =users_surname_x+users_surname_w
            ,users_admin_x          =users_email_x+users_email_w
            ,users_bookkeep_x       =users_admin_x+users_admin_w
            ,users_museum_x         =users_bookkeep_x+users_bookkeep_w
            ,users_date_create_x    =users_museum_x+users_museum_w
            ,users_date_online_x    =users_date_create_x+users_date_create_w
            ,users_controls_x       =users_date_online_x+users_date_online_w;

        inner+='<div id="users_header_login_'+i+'" class="users_row_item" style="width: '+users_login_w+'px; height: '+user_list_h+'px; transform: translate('+users_login_x+'px,0);">';
        inner+='<span>'+stripSlashes(empty(list[i]['login'])?'-':list[i]['login'])+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_surname_'+i+'" class="users_row_item" style="width: '+users_surname_w+'px; height: '+user_list_h+'px; transform: translate('+users_surname_x+'px,0);">';
        inner+='<span>'+stripSlashes(empty(list[i]['surname'])?'-':list[i]['surname'])+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_name_'+i+'" class="users_row_item" style="width: '+users_name_w+'px; height: '+user_list_h+'px; transform: translate('+users_name_x+'px,0);">';
        inner+='<span>'+stripSlashes(empty(list[i]['name'])?'-':list[i]['name'])+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_email_'+i+'" class="users_row_item" style="width: '+users_email_w+'px; height: '+user_list_h+'px; transform: translate('+users_email_x+'px,0);">';
        inner+='<span>'+stripSlashes(empty(list[i]['email'])?'-':list[i]['email'])+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_admin_'+i+'" class="users_row_item" style="width: '+users_admin_w+'px; height: '+user_list_h+'px; transform: translate('+users_admin_x+'px,0);">';

        if(page_object.action.admin.data['user']['login']!==list[i]['login'])
            inner+='<div id="users_header_admin_check_'+i+'" class="users_header_admin_check'+(list[i]['is_root']?'_active':'')+'"></div>';

        inner+='</div>';

        inner+='<div id="users_header_date_create_'+i+'" class="users_row_item" style="width: '+users_date_create_w+'px; height: '+user_list_h+'px; transform: translate('+users_date_create_x+'px,0);">';
        inner+='<span>'+(list[i]['date_create']===0?'-':(get_date(list[i]['date_create'])))+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_date_online_'+i+'" class="users_row_item" style="width: '+users_date_online_w+'px; height: '+user_list_h+'px; transform: translate('+users_date_online_x+'px,0);">';
        inner+='<span>'+(list[i]['date_online']===0?'-':(get_date_time(list[i]['date_online'])))+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_controls_'+i+'" class="users_row_item" style="width: '+users_controls_w+'px; height: '+user_list_h+'px; transform: translate('+users_controls_x+'px,0); border: none; ">';

        if(page_object.action.admin.data['user']['login']!==list[i]['login']){

            inner+='<div id="users_header_edit_check_'+i+'" class="users_header_edit_check"></div>';
            inner+='<div id="users_header_block_check_'+i+'" class="users_header_block_check'+(list[i]['is_block']?'_active':'')+'"></div>';
            inner+='<div id="users_header_remove_check_'+i+'" class="users_header_remove_check'+(list[i]['is_remove']?'_active':'')+'"></div>';

        }

        inner+='</div>';

        return inner;

    },
    'create':function(){

        let  el
            ,inner                  =''
            ,style                  =''
            ,style_list             =''
            ,style_add              =''
            ,lang_obj               =page_object.action.admin.content[page_object.lang]
            ,proportion             =page_object.action.admin.position['users_list']['table']
            ,user_list_w            =page_object.action.admin.position.users_list.w
            ,user_list_h            =page_object.action.admin.position.users_list.table.row
            ,users_login_w          =Math.ceil(user_list_w*proportion['login']/100)
            ,users_name_w           =Math.ceil(user_list_w*proportion['name']/100)
            ,users_surname_w        =Math.ceil(user_list_w*proportion['surname']/100)
            ,users_email_w          =Math.ceil(user_list_w*proportion['email']/100)
            ,users_admin_w          =Math.ceil(user_list_w*proportion['admin']/100)
            ,users_bookkeep_w       =Math.ceil(user_list_w*proportion['bookkeep']/100)
            ,users_museum_w         =Math.ceil(user_list_w*proportion['museum']/100)
            ,users_date_create_w    =Math.ceil(user_list_w*proportion['date_create']/100)
            ,users_date_online_w    =Math.ceil(user_list_w*proportion['date_online']/100)
            ,users_controls_w       =Math.ceil(user_list_w*proportion['controls']/100)
            ,users_login_x          =0
            ,users_name_x           =users_login_x+users_login_w
            ,users_surname_x        =users_name_x+users_name_w
            ,users_email_x          =users_surname_x+users_surname_w
            ,users_admin_x          =users_email_x+users_email_w
            ,users_bookkeep_x       =users_admin_x+users_admin_w
            ,users_museum_x         =users_bookkeep_x+users_bookkeep_w
            ,users_date_create_x    =users_museum_x+users_museum_w
            ,users_date_online_x    =users_date_create_x+users_date_create_w
            ,users_controls_x       =users_date_online_x+users_date_online_w;

        style_list+='width: '+user_list_w+'px; ';
        style_list+='transform: translate('+page_object.action.admin.position.users_list.x+'px,'+page_object.action.admin.position.users_list.y+'px); ';

        style_add+='transform: translate('+page_object.action.admin.position.users_add.x+'px,'+page_object.action.admin.position.users_add.y+'px); ';
        style_add+='opacity: '+page_object.action.admin.position.users_add.o+'; ';

        inner+='<div id="users_add">'+stripSlashes(lang_obj['users_add'])+'</div>';

        inner+='<div id="users_list" style="'+style_list+'">';

        inner+='<div id="users_header" style="width: '+user_list_w+'px; height: '+user_list_h+'px; ">';

        inner+='<div id="users_header_login" class="users_header_item" style="width: '+users_login_w+'px; height: '+user_list_h+'px; transform: translate('+users_login_x+'px,0);">';
        inner+='<span>'+stripSlashes(lang_obj['users_login'])+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_surname" class="users_header_item" style="width: '+users_surname_w+'px; height: '+user_list_h+'px; transform: translate('+users_surname_x+'px,0);">';
        inner+='<span>'+stripSlashes(lang_obj['users_surname'])+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_name" class="users_header_item" style="width: '+users_name_w+'px; height: '+user_list_h+'px; transform: translate('+users_name_x+'px,0);">';
        inner+='<span>'+stripSlashes(lang_obj['users_name'])+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_email" class="users_header_item" style="width: '+users_email_w+'px; height: '+user_list_h+'px; transform: translate('+users_email_x+'px,0);">';
        inner+='<span>'+stripSlashes(lang_obj['users_email'])+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_admin" class="users_header_item" style="width: '+users_admin_w+'px; height: '+user_list_h+'px; transform: translate('+users_admin_x+'px,0); text-align: center; ">';
        inner+='<span>'+stripSlashes(lang_obj['users_admin'])+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_date_create" class="users_header_item" style="width: '+users_date_create_w+'px; height: '+user_list_h+'px; transform: translate('+users_date_create_x+'px,0); border: none; ">';
        inner+='<span>'+stripSlashes(lang_obj['users_date_create'])+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_date_online" class="users_header_item" style="width: '+users_date_online_w+'px; height: '+user_list_h+'px; transform: translate('+users_date_online_x+'px,0);">';
        inner+='<span>'+stripSlashes(lang_obj['users_date_online'])+'</span>';
        inner+='</div>';

        inner+='<div id="users_header_controls" class="users_header_item" style="width: '+users_controls_w+'px; height: '+user_list_h+'px; transform: translate('+users_controls_x+'px,0);"></div>';

        inner+='</div>';

        let  list           =page_object.action.admin.data['list']
            ,i
            ,y              =0
            ,perc;

        for(i=0;i<list.length;i++){

            y               +=user_list_h;
            perc            =i/2;

            inner+='<div id="users_row_'+i+'" class="users_row" style="width: '+user_list_w+'px; height: '+user_list_h+'px; transform: translate(0,'+y+'px);">';

            inner+=page_object.action.admin.create.users.get_row(i);

            inner+='</div>';

        }

        inner+='</div>';

        page_object.action.admin.position.users_list.h=y+user_list_h;

        style+='width: '+page_object.action.admin.position.users_content.w+'px; ';
        style+='transform: translate(0,'+page_object.action.admin.position.users_content.y+'px);';

        el=addElement({
            'tag':'div',
            'id':'users_content',
            'inner':inner,
            'style':style
        });

        $d('all').appendChild(el);

    },
    'set_action':function(){

        let  list=page_object.action.admin.data['list']
            ,i;

        for(i=0;i<list.length;i++){

            if(isset($d('users_header_admin_check_'+i)))
                $d('users_header_admin_check_'+i).onclick=page_object.action.admin.action.users.click.admin.init;

            if(isset($d('users_header_block_check_'+i)))
                $d('users_header_block_check_'+i).onclick=page_object.action.admin.action.users.click.block.init;

            if(isset($d('users_header_edit_check_'+i)))
                $d('users_header_edit_check_'+i).onclick=page_object.action.admin.action.users.click.edit.init;

            if(isset($d('users_header_remove_check_'+i)))
                $d('users_header_remove_check_'+i).onclick=page_object.action.admin.action.users.click.remove.init;

        }

        $d('users_add').onclick=page_object.action.admin.action.users.click.add.init;

    },
    'show':function(){

        setTimeout(page_object.action.admin.action.users.show,300);

    }
};