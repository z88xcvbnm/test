page_object.action.admin.create.face={
    'init':function(){
        
        page_object.action.admin.create.face.position.init();
        page_object.action.admin.create.face.create.init();
        page_object.action.admin.create.face.set_action();
        page_object.action.admin.create.face.show();
    
    },
    'position':{
        'init':function(){
            
            page_object.action.admin.create.face.position.face_content();
            page_object.action.admin.create.face.position.face_item();
            page_object.action.admin.create.face.position.face_item_block();

        },
        'face_content':function(){

            page_object.action.admin.position.face_content.w    =winSize.winWidth;
            page_object.action.admin.position.face_content.h    =winSize.winHeight-page_object.action.admin.position.header.h;
            page_object.action.admin.position.face_content.x    =0;
            page_object.action.admin.position.face_content.y    =0;
            page_object.action.admin.position.face_content.o    =0;

        },
        'face_item':function(){

            page_object.action.admin.position.face_item.x   =0;
            page_object.action.admin.position.face_item.w   =page_object.action.admin.position.face_content.w;
            page_object.action.admin.position.face_item.h   =page_object.action.admin.position.face_content.h;
            page_object.action.admin.position.face_item.y   =0;
            page_object.action.admin.position.face_item.o   =0;

        },
        'face_item_block':function(){

            page_object.action.admin.position.face_item_block.x      =0;
            page_object.action.admin.position.face_item_block.w      =page_object.action.admin.position.face_content.w;
            page_object.action.admin.position.face_item_block.h      =winSize.winHeight-page_object.action.admin.position.header.h;
            page_object.action.admin.position.face_item_block.y      =0;
            page_object.action.admin.position.face_item_block.o      =0;

        }
    },
    'create':{
        'init':function(){
            
            page_object.action.admin.create.face.create.face_content();
            page_object.action.admin.create.face.create.face_item();
            page_object.action.admin.create.face.create.face_item_block();
            page_object.action.admin.create.face.create.face_item_data();

        },
        'get_face_item_image_row':function(image_ID,image_list,image_index){

            let  image_data         =image_list[image_index]
                ,image_link
                ,image_link_full
                ,image_link_list    =[
                    '/Project/Whore/Admin/Template/Images/Avatar/whore_image_empty_0.png',
                    '/Project/Whore/Admin/Template/Images/Avatar/whore_image_empty_1.png'
                ]
                ,image_object       =new Image()
                ,func
                ,inner              ='';

            if(!empty(image_data['image'])){

                image_link                  ='/'+image_data['image']['image_dir']+'/'+image_data['image']['image_item_ID_list']['small']['ID'];
                image_link_full             =window.location.origin+'/'+image_data['image']['image_dir']+'/'+image_data['image']['image_item_ID_list']['large']['ID'];

                image_object.src            =image_link;
                func                        =new Function("if(isset($d('dialog_face_item_image_block_"+image_index+"'))){$s('dialog_face_item_image_block_"+image_index+"').opacity=1;$s('dialog_face_item_image_background_"+image_index+"').opacity=0;}");
                image_object.onload         =func;

            }

            inner+='<div id="dialog_face_item_number_'+image_index+'" class="face_item_col" style="width: 50px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150" align="center">';
                inner+='<a href="https://yandex.ru/images/search?source=collections&&url='+uniEncode(image_link_full)+'&rpt=imageview" target="_blank">';
                    inner+=stripSlashes(empty(image_data['image_ID'])?'':image_data['image_ID']);
                inner+='</a>';
            inner+='</td></tr></table></div>';

            inner+='<div id="dialog_face_item_image_'+image_index+'" class="face_item_col">';

                inner+='<div id="dialog_face_item_image_background_'+image_index+'" class="face_item_image_background" style="background: url('+image_link_list[0]+') 50% 50% no-repeat; background-size: cover;"></div>';
                inner+='<div id="dialog_face_item_image_block_'+image_index+'" class="face_item_image_block"'+(empty(image_link)?'':(' style="background: url('+image_link+') 50% 50% no-repeat; opacity: 0; background-size: cover;"'))+'></div>';

                inner+='<div id="dialog_face_item_image_len_'+image_index+'" class="face_item_image_len"'+(empty(image_data['faceset_ID'])?(image_data['face_count']>1?'':' style="display: none"'):'')+'>'+(empty(image_data['faceset_ID'])?(image_data['face_count']>1?'?!':''):'+')+'</div>';

            inner+='</div>';

            inner+='<div id="dialog_face_item_checkbox_'+image_index+'" class="face_item_col" style="width: 100px">';
                inner+='<div id="dialog_face_item_checkbox_item_'+image_index+'" class="dialog_checkbox'+(parseInt(image_ID)===parseInt(image_data['image_ID'])?'_active':'')+'"></div>';
            inner+='</div>';
            
            inner+='<div id="dialog_face_item_image_size_'+image_index+'" class="face_item_col"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150" align="center">'+stripSlashes(image_data['image']['image_item_ID_list']['large']['width']+'x'+image_data['image']['image_item_ID_list']['large']['height']+'px')+'</td></tr></table></div>';

            inner+='<div id="dialog_face_item_size_'+image_index+'" class="face_item_col"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150" align="center">'+(empty(image_data['image']['image_item_ID_list']['large']['file_size'])?'-':stripSlashes((image_data['image']['image_item_ID_list']['large']['file_size']/(1024*1024)).toFixed(2))+'M')+'</td></tr></table></div>';

            inner+='<div id="dialog_face_item_date_'+image_index+'" class="face_item_col"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150" align="center">'+(empty(image_data['date_create'])?'-':stripSlashes(get_date(image_data['date_create'])))+'</td></tr></table></div>';

            inner+='<div id="dialog_face_item_controls_'+image_index+'" class="face_item_col" style="width: 30px">';
                inner+='<div id="dialog_face_item_remove_'+image_index+'" class="face_item_remove"></div>';
            inner+='</div>';

            return inner;

        },
        'get_face_item_video_row':function(video_ID,video_list,video_index){

            let  video_data         =video_list[video_index]
                ,video_link
                ,video_link_full
                ,video_link_list    =[
                    '/Project/Whore/Admin/Template/Images/Avatar/whore_image_empty_0.png',
                    '/Project/Whore/Admin/Template/Images/Avatar/whore_image_empty_1.png'
                ]
                ,video_object       =new Image()
                ,func
                ,inner              ='';

            if(!empty(video_data['video'])){

                video_link                  ='/'+video_data['video']['video_path'];
                video_link_full             =window.location.origin+'/'+video_data['video']['video_path'];

                video_object.src            =video_link;
                func                        =new Function("if(isset($d('dialog_face_item_video_block_"+video_index+"'))){$s('dialog_face_item_video_block_"+video_index+"').opacity=1;$s('dialog_face_item_video_background_"+video_index+"').opacity=0;}");
                video_object.onload         =func;

            }

            inner+='<div id="dialog_face_item_number_'+video_index+'" class="face_item_col" style="width: 50px; height: 320px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="320" align="center">';
                inner+=stripSlashes(empty(video_data['video_ID'])?'':video_data['video_ID']);
            inner+='</td></tr></table></div>';

            inner+='<div id="dialog_face_item_video_'+video_index+'" class="face_item_col" style="width: 580px; height: 320px; text-align: center">';

                inner+='<video height="320" controls>';
                    inner+='<source src="'+video_link+'" />';
                inner+='</video>';

            inner+='</div>';
            
            // inner+='<div id="dialog_face_item_video_size_'+video_index+'" class="face_item_col"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150" align="center">'+stripSlashes(video_data['video']['video_item_ID_list']['large']['width']+'x'+video_data['video']['video_item_ID_list']['large']['height']+'px')+'</td></tr></table></div>';

            // inner+='<div id="dialog_face_item_size_'+video_index+'" class="face_item_col"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150" align="center">'+(empty(video_data['video']['video_item_ID_list']['large']['file_size'])?'-':stripSlashes((video_data['video']['video_item_ID_list']['large']['file_size']/(1024*1024)).toFixed(2))+'M')+'</td></tr></table></div>';

            inner+='<div id="dialog_face_item_date_'+video_index+'" class="face_item_col" style="height: 320px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="320" align="center">'+(empty(video_data['date_create'])?'-':stripSlashes(get_date(video_data['date_create'])))+'</td></tr></table></div>';

            inner+='<div id="dialog_face_item_controls_'+video_index+'" class="face_item_col" style="width: 30px; height: 320px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="250" align="center">';
                inner+='<div id="dialog_face_item_remove_'+video_index+'" class="face_item_remove"></div>';
            inner+='</td></tr></table></div>';

            return inner;

        },
        'get_face_item_data_row':function(face_index){

            let  data               =page_object.action.admin.data['face_data']
                ,face_list          =data['face_list']
                ,face_data          =face_list[face_index]
                ,image_link
                ,image_link_list    =[
                    '/Project/Whore/Admin/Template/Images/Avatar/whore_image_empty_0.png',
                    '/Project/Whore/Admin/Template/Images/Avatar/whore_image_empty_1.png'
                ]
                ,image_object       =new Image()
                ,func
                ,inner              ='';

            if(!empty(face_data['image'])){

                image_link='/'+face_data['image']['image_dir']+'/'+face_data['image']['image_item_ID_list']['small']['ID'];

                image_object.src            =image_link;
                func                        =new Function("if(isset($d('face_item_image_block_"+face_index+"'))){$s('face_item_image_block_"+face_index+"').opacity=1;$s('face_item_image_background_"+face_index+"').opacity=0;}");
                image_object.onload         =func;
                // image_object.oncomplete     =func;

            }
            // else
            //     image_link=image_link_list[Math.ceil(Math.random()*(image_link_list.length))-1];

            inner+='<div id="face_item_number_'+face_index+'" class="face_item_col" style="width: 50px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150">'+stripSlashes(empty(face_data['ID'])?'':face_data['ID'])+'</td></tr></table></div>';

            inner+='<div id="face_item_image_'+face_index+'" class="face_item_col">';
                inner+='<div id="face_item_image_background_'+face_index+'" class="face_item_image_background" style="background: url('+image_link_list[0]+') 50% 50% no-repeat; background-size: cover;"></div>';
                inner+='<div id="face_item_image_block_'+face_index+'" class="face_item_image_block"'+(empty(image_link)?'':(' style="background: url('+image_link+') 50% 50% no-repeat; opacity: 0; background-size: cover;"'))+'></div>';
                inner+='<div id="face_item_image_len_'+face_index+'" class="face_item_image_len">'+stripSlashes(face_data['image_facekit_len']+'/'+face_data['image_len'])+'</div>';
                // inner+='<div id="face_item_image_len_'+face_index+'" class="face_item_image_len"'+(face_data['image_len']<=1?' style="display: none"':'')+'>'+stripSlashes(face_data['image_facekit_len']+'/'+face_data['image_len'])+'</div>';
            inner+='</div>';

            inner+='<div id="face_item_source_'+face_index+'" class="face_item_col" style="width: 240px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150">';

            if(empty(face_data['source_account_link']))
                inner+='<span>-</span>';
            else
                // inner+='<a target="_blank" href="'+face_data['source_account_link']+'"'+(face_data['source_account_link'].length>20?(' title="'+stripSlashes(face_data['source_account_link'])+'"'):'')+'>'+stripSlashes((face_data['source_account_link'].length>20?(face_data['source_account_link'].substr(0,20)+'...'):face_data['source_account_link']))+'</a>';
                inner+='<a target="_blank" href="'+face_data['source_account_link']+'"'+(face_data['source_account_link'].length>20?(' title="'+stripSlashes(face_data['source_account_link'])+'"'):'')+'>'+stripSlashes(empty(face_data['source_link_number'])?'-':('#'+face_data['source_link_number']))+'</a>';

            inner+='</td></tr></table></div>';

            inner+='<div id="face_item_name_'+face_index+'" class="face_item_col"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150">'+stripSlashes(empty(face_data['name'])?'-':face_data['name'])+'</td></tr></table></div>';
            inner+='<div id="face_item_age_'+face_index+'" class="face_item_col" style="width: 60px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150">'+stripSlashes(empty(face_data['age'])?'-':face_data['age'])+'</td></tr></table></div>';
            inner+='<div id="face_item_city_'+face_index+'" class="face_item_col" style="width: 290px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150">'+stripSlashes(empty(face_data['city'])?'-':face_data['city'])+'</td></tr></table></div>';
            inner+='<div id="face_item_date_'+face_index+'" class="face_item_col" style="width: 140px;"><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="100%" height="150">'+(empty(face_data['date_create'])?'-':stripSlashes(get_date(face_data['date_create'])))+'</td></tr></table></div>';

            inner+='<div id="face_item_controls_'+face_index+'" class="face_item_col" style="width: 100px">';
                inner+='<div id="face_item_edit_'+face_index+'" class="face_item_edit"></div>';
                inner+='<div id="face_item_photo_'+face_index+'" class="face_item_photo"></div>';
                inner+='<div id="face_item_video_'+face_index+'" class="face_item_video'+(parseInt(face_data['video_len'])===0?'_disable':'')+'"></div>';
                inner+='<div id="face_item_remove_'+face_index+'" class="face_item_remove"></div>';
            inner+='</div>';

            return inner;

        },
        'face_content':function(){

            if(isset($d('face_content')))
                return false;

            let  el
                ,inner      =''
                ,style      ='';

            // style+='width: '+page_object.action.admin.position.face_content.w+'px; ';
            style+='min-height: '+page_object.action.admin.position.face_content.h+'px; ';
            style+='transform: translate('+page_object.action.admin.position.face_content.x+'px,'+page_object.action.admin.position.face_content.y+'px); ';
            style+='opacity: '+page_object.action.admin.position.face_content.o+'; ';

            el=addElement({
                'tag':'div',
                'id':'face_content',
                'inner':inner,
                'style':style
            });

            $d('all').appendChild(el);

            return true;
        
        },
        'face_item':function(){

            if(isset($d('face_item')))
                return false;

            let  el
                ,inner                  =''
                ,style                  ='';

            // style+='width: '+page_object.action.admin.position.face_item.w+'px; ';
            style+='min-height: '+page_object.action.admin.position.face_item.h+'px; ';
            style+='transform: translate('+page_object.action.admin.position.face_item.x+'px,'+page_object.action.admin.position.face_item.y+'px); ';
            style+='opacity: '+page_object.action.admin.position.face_item.o+'; ';

            el=addElement({
                'tag':'div',
                'id':'face_item',
                'inner':inner,
                'style':style
            });

            $d('face_content').appendChild(el);

        },
        'face_item_block':function(){

            if(isset($d('face_item_block')))
                return false;

            let  el
                ,inner                  =''
                ,style                  ='';

            // style+='width: '+page_object.action.admin.position.face_item_block.w+'px; ';
            style+='min-height: '+page_object.action.admin.position.face_item_block.h+'px; ';
            style+='transform: translate('+page_object.action.admin.position.face_item_block.x+'px,'+page_object.action.admin.position.face_item_block.y+'px); ';
            style+='opacity: '+page_object.action.admin.position.face_item_block.o+'; ';

            inner+='';

            el=addElement({
                'tag':'div',
                'id':'face_item_block',
                'inner':inner,
                'style':style
            });

            $d('face_item').appendChild(el);

        },
        'face_item_data':function(){

            let  data           =page_object.action.admin.data['face_data']
                ,face_list      =data['face_list']
                ,face_index
                ,source_list    =data['source_list']
                ,source_index
                ,func
                ,inner          ='';

            inner+='<div id="face_block_container" style="min-height: calc(100vh - 90px);">';

            inner+='<div id="face_controls">';
                inner+='<div id="face_add">Добавить лицо</div>';
                inner+='<div id="face_filter">Фильтры</div>';
            inner+='</div>';

            inner+='<div id="face_filter_list">';

                inner+='<div class="face_filter_row">';

                    inner+='<div class="face_filter_col">';
                        inner+='<div class="face_filter_label">Источник:</div>';
                        inner+='<div class="face_filter_select">';
                            inner+='<select id="face_filter_source" class="face_input_select">';
                                inner+='<option value="0">Выберите источник</option>';

            for(source_index in source_list)
                inner+='<option value="'+source_list[source_index]['ID']+'">'+stripSlashes(source_list[source_index]['name'])+'</option>';

                            inner+='</select>';
                        inner+='</div>';
                    inner+='</div>';

                    inner+='<div class="face_filter_col">';
                        inner+='<div class="face_filter_label">Face ID:</div>';
                        inner+='<div class="face_filter_select">';
                            inner+='<input id="face_filter_face_ID" class="face_input" value="" />';
                        inner+='</div>';
                    inner+='</div>';

                inner+='</div>';

                inner+='<div class="face_filter_row">';

                    inner+='<div class="face_filter_col">';
                        inner+='<div class="face_filter_label">Ссылка на профиль:</div>';
                        inner+='<div class="face_filter_select">';
                            inner+='<input id="face_filter_link" class="face_input" value="" />';
                        inner+='</div>';
                    inner+='</div>';

                    inner+='<div class="face_filter_col">';
                        inner+='<div class="face_filter_label">Город:</div>';
                        inner+='<div class="face_filter_select">';
                            inner+='<input id="face_filter_city" class="face_input" value="" />';
                        inner+='</div>';
                    inner+='</div>';

                inner+='</div>';

                inner+='<div class="face_filter_row">';

                    inner+='<div class="face_filter_col">';
                        inner+='<div class="face_filter_label">Сортировать фото по:</div>';
                        inner+='<div class="face_filter_select">';
                            inner+='<select id="face_filter_image_sort_count" class="face_input_select">';
                                inner+='<option value="0">Выберите вариант</option>';
                                inner+='<option value="1">По возрастанию</option>';
                                inner+='<option value="2">По убыванию</option>';
                            inner+='</select>';
                        inner+='</div>';
                    inner+='</div>';

                    inner+='<div class="face_filter_col">';
                        inner+='<div class="face_filter_label">Уровень:</div>';
                        inner+='<div class="face_filter_select">';
                            inner+='<select id="face_filter_level" class="face_input_select">';
                                inner+='<option value="0">Выберите уровень</option>';
                                inner+='<option value="1">Шлюхи</option>';
                                inner+='<option value="2">Эскорт</option>';
                                inner+='<option value="3">Содержанки</option>';
                                inner+='<option value="4">Начинающие</option>';
                            inner+='</select>';
                        inner+='</div>';
                    inner+='</div>';

                inner+='</div>';

                inner+='<div class="face_filter_row" style="text-align: right">';

                    inner+='<div class="face_filter_col">';

                        inner+='<div id="face_filter_check_fpp" class="face_filter_check">';
                            inner+='<div class="face_filter_check_box"></div>';
                            inner+='<div class="face_filter_check_label">Отправленные в FPP:</div>';
                        inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="face_filter_col">';

                        inner+='<div id="face_filter_check_not_age" class="face_filter_check">';
                            inner+='<div class="face_filter_check_box"></div>';
                            inner+='<div class="face_filter_check_label">Без возраста:</div>';
                        inner+='</div>';

                    inner+='</div>';

                inner+='</div>';

                inner+='<div class="face_filter_row" style="text-align: right">';

                    inner+='<div class="face_filter_col">';

                        inner+='<div id="face_filter_check_face_count" class="face_filter_check">';
                            inner+='<div class="face_filter_check_box"></div>';
                            inner+='<div class="face_filter_check_label">Несколько лиц на фото: </div>';
                        inner+='</div>';

                    inner+='</div>';

                    inner+='<div class="face_filter_col">';

                        inner+='<div id="face_filter_check_not_city" class="face_filter_check">';
                            inner+='<div class="face_filter_check_box"></div>';
                            inner+='<div class="face_filter_check_label">Без города: </div>';
                        inner+='</div>';

                    inner+='</div>';

                inner+='</div>';

            inner+='</div>';

            inner+='<div id="face_none">Список лиц пуст</div>';
            inner+='<div id="face_block">';

            inner+='<div id="face_header">';
                inner+='<div class="face_header_col" style="width: 50px;">ID</div>';
                inner+='<div class="face_header_col">Фото</div>';
                inner+='<div class="face_header_col" style="width: 240px;">Исходник</div>';
                inner+='<div class="face_header_col">Имя</div>';
                inner+='<div class="face_header_col" style="width: 60px;">Возраст</div>';
                inner+='<div class="face_header_col" style="width: 290px;">Место</div>';
                inner+='<div class="face_header_col" style="width: 140px">Дата</div>';
                inner+='<div class="face_header_col" style="width: 100px"></div>';
            inner+='</div>';

            inner+='<div id="face_list">';

            for(face_index in face_list){

                inner+='<div id="face_item_'+face_index+'" class="face_item">';

                inner+=page_object.action.admin.create.face.create.get_face_item_data_row(face_index);

                inner+='</div>';

            }

            inner+='</div>';
            inner+='</div>';

            inner+='</div>';

            $d('face_item_block').innerHTML=inner;

        }
    },
    'set_action':function(){

        page_object.action.admin.action.face_item.action.init();
        page_object.action.admin.action.face_item_data.action.init();

    },
    'show':function(){

        setTimeout(page_object.action.admin.action.face_content.show,40);
        setTimeout(page_object.action.admin.action.face_item.show,300);
        setTimeout(page_object.action.admin.action.face_item_block.show,800);
        setTimeout(page_object.link.preload.un_show,1000);

    }
};
