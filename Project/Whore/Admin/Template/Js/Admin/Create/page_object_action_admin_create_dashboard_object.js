page_object.action.admin.create.dashboard={
    'init':function(){
        
        page_object.action.admin.create.dashboard.position.init();
        page_object.action.admin.create.dashboard.create.init();
        page_object.action.admin.create.dashboard.set_action();
        page_object.action.admin.create.dashboard.show();
    
    },
    'position':{
        'init':function(){
            
            page_object.action.admin.create.dashboard.position.dashboard_content();
            
        },
        'dashboard_content':function(){

            page_object.action.admin.position.dashboard_content.w    =winSize.winWidth;
            page_object.action.admin.position.dashboard_content.h    =winSize.winHeight-page_object.action.admin.position.header.h;
            page_object.action.admin.position.dashboard_content.x    =0;
            page_object.action.admin.position.dashboard_content.y    =0;
            page_object.action.admin.position.dashboard_content.o    =0;

        },
    },
    'create':{
        'init':function(){
            
            page_object.action.admin.create.dashboard.create.dashboard_content();
            
        },
        'dashboard_content':function(){

            let  el
                ,data       =page_object.action.admin.data['dashboard']
                ,inner      =''
                ,style      ='';

            style+='width: '+page_object.action.admin.position.dashboard_content.w+'px; ';
            style+='height: '+page_object.action.admin.position.dashboard_content.h+'px; ';
            style+='transform: translate('+page_object.action.admin.position.dashboard_content.x+'px,'+page_object.action.admin.position.dashboard_content.y+'px); ';
            style+='opacity: '+page_object.action.admin.position.dashboard_content.o+'; ';

            inner+='<div class="dashboard_stats_block" style="min-height: '+page_object.action.admin.position.dashboard_content.h+'px;">';
                inner+='<div class="dashboard_stats_label">Поступлений за текущий месяц: <b>'+data['transaction_last_month']+' руб.</b> <i style="font-size: 12px;">(всего: '+data['transaction']+' руб.)</i></div>';
                inner+='<div class="dashboard_stats_label">База профилей: <b>'+data['face_count']['all']+'</b> <i style="font-size: 12px;">(+ '+data['face_count']['month']+' за 30 дней)</i></div>';
                inner+='<div class="dashboard_stats_label">База фото: <b>'+(data['face_image_count']['all'])+'</b> <i style="font-size: 12px;">(+'+(data['face_image_count']['month'])+' за 30 дней)</i></div>';
                // inner+='<div class="dashboard_stats_label">База фото: <b>'+(data['facekit_image_count']['all']+' / '+data['face_image_count']['all'])+'</b> <i style="font-size: 12px;">(+ '+(data['facekit_image_count']['month']+' / '+data['face_image_count']['month'])+' за 30 дней)</i></div>';
                inner+='<div class="dashboard_stats_label">Количество пользователей: <b>'+data['profile_count']['all']+'</b> <i style="font-size: 12px;">(+ '+data['profile_count']['month']+' за 30 дней)</i></div>';
                inner+='<div class="dashboard_stats_label">Количество Faceset: <b>'+data['faceset_count']+'</b></div>';
                inner+='<div class="dashboard_stats_label">Время последнего обновления: <b>'+(empty(data['face_image_last_date_update'])?'Нет данных':get_date_time(data['face_image_last_date_update']))+'</b></div>';
                inner+='<div class="dashboard_stats_label">';
                    inner+='<div id="user_reg_stats">Статистика регистраций пользователей</div>';
                inner+='</div>';

                if(page_object.action.admin.data['user']['is_root']){

                    inner+='<div class="dashboard_stats_label">';
                        inner+='<div id="user_city_stats">Статистика городов</div>';
                    inner+='</div>';

                    inner+='<div class="dashboard_stats_label">';
                        inner+='<div id="user_parsing_stats">Статистика парсинга</div>';
                    inner+='</div>';

                }

            inner+='</div>';

            el=addElement({
                'tag':'div',
                'id':'dashboard_content',
                'inner':inner,
                'style':style
            });

            $d('all').appendChild(el);

            return true;
        
        },
    },
    'set_action':function(){

        page_object.action.admin.action.dashboard_content.action.init();

        return true;

    },
    'show':function(){

        setTimeout(page_object.action.admin.action.dashboard_content.show,40);

    }
};
