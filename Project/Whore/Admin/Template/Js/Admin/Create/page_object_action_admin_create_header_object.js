page_object.action.admin.create.header={
    'init':function(){

        if(isset($d('header'))&&!isset($d('header_profile'))){

            page_object.action.admin.action.header.un_show(true);

            setTimeout(page_object.action.admin.create.header.create.init,300);

        }
        else
            page_object.action.admin.create.header.create.init();

    },
    'position':{
        'header_menu':function(){

            page_object.action.admin.position.header_menu.w=Math.ceil(winSize.winWidth*.63);

        },
        'header':function(){

            page_object.action.admin.position.header.w      =winSize.winWidth;
            page_object.action.admin.position.header.x      =0;
            page_object.action.admin.position.header.y      =-page_object.action.admin.position.header.h;

        },
        'header_profile':function(){

            if(isset($d('header_profile'))){

                page_object.action.admin.position.header_profile.o      =0;
                page_object.action.admin.position.header_profile.w      =elementSize.width($d('header_profile'));
                page_object.action.admin.position.header_profile.x      =winSize.winWidth-page_object.action.admin.position.header_profile.m.r-page_object.action.admin.position.header_profile.w;

                $s('header_profile').opacity            =page_object.action.admin.position.header_profile.o;
                // $s('header_profile').transform          ='translate('+page_object.action.admin.position.header_profile.x+'px,'+page_object.action.admin.position.header_profile.y+'px)';
                $s('header_profile').height             =page_object.action.admin.position.header_profile.h+'px';

            }

        }
    },
    'create':{
        'init':function(){

            if(isset($d('header')))
                return false;

            page_object.action.admin.create.header.position.header();
            page_object.action.admin.create.header.position.header_menu();
            page_object.action.admin.create.header.create.header();
            page_object.action.admin.create.header.create.footer();
            page_object.action.admin.create.header.set_action();
            page_object.action.admin.create.header.show();

            return true;

        },
        'header':function(){

            let  el
                ,inner                      =''
                ,header_style               =''
                ,header_menu_style          =''
                ,profile_style              =''
                ,lang_object                =page_object.action.admin.content[page_object.lang]
                ,data                       =page_object.action.admin.data['user']
                ,image_ID                   =data['image_ID']
                ,login                      =data['login']
                ,profile_image              =(image_ID===0)?'/Project/Whore/Admin/Template/Images/Avatar/admin_icon.png':'/'+data['image_dir']+'/'+data['image_item_ID_list']['preview']['ID'];

            inner+='<div id="logo"></div>';

            header_menu_style+='transform: translate('+page_object.action.admin.position.header_menu.x+'px,'+page_object.action.admin.position.header_menu.y+'px); ';
            //header_menu_style+='width: '+page_object.action.admin.position.header_menu.w+'px; ';
            header_menu_style+='opacity: '+page_object.action.admin.position.header_menu.o+'; ';

            inner+='<div id="header_menu" style="'+header_menu_style+'">';

                inner+='<div id="header_menu_face" class="header_menu_item" style="margin-left: 0;">Лица</div>';
                inner+='<div id="header_menu_profile" class="header_menu_item" style="margin-left: 0;">Пользователи</div>';
                // inner+='<div id="header_menu_financial" class="header_menu_item" style="margin-left: 0;">Финансы</div>';
                // inner+='<div id="header_menu_source" class="header_menu_item" style="margin-left: 0;">Источники</div>';

                if(page_object.action.admin.data['user']['is_root'])
                    inner+='<div id="header_menu_users" class="header_menu_item">Администраторы</div>';

            inner+='</div>';

            inner+='<div id="header_menu_line_active" class="header_menu_line_active"></div>';

            inner+='<div id="header_profile" class="header_profile" style="'+profile_style+'">';
            inner+='<div id="header_profile_image" style="background: url('+profile_image+') 50% 50% no-repeat; background-size: cover;"></div>';
            inner+='<div id="header_profile_login">'+stripSlashes(login)+'</div>';
            inner+='</div>';

            header_style+='transform: translate('+page_object.action.admin.position.header.x+'px,'+page_object.action.admin.position.header.y+'px); ';
            // header_style+='width: '+page_object.action.admin.position.header.w+'px; ';
            header_style+='height: '+page_object.action.admin.position.header.h+'px; ';

            el=addElement({
                'tag':'div',
                'id':'header',
                'inner':inner,
                'style':header_style
            });

            $d('all').appendChild(el);

        },
        'footer':function(){

            let  el;

            el=addElement({
                'tag':'div',
                'id':'footer',
                'inner':'© 2019 Shluham.net',
                'style':'opacity: 0'
            });

            $d('all').appendChild(el);

        }
    },
    'set_action':function(){

        let  list=$d('header_menu').getElementsByTagName('div')
            ,i;

        for(i=0;i<list.length;i++)
            if(isObject(list[i]))
                list[i].onclick=page_object.action.admin.action.header_menu.click;

        $d('header_profile').onclick=page_object.action.admin.action.header_profile.click;

        if(isset(page_object.link.link_list[2]))
            page_object.action.admin.action.header_menu.set_active(page_object.link.link_list[2]);

        if(isset($d('logo')))
            $d('logo').onclick=page_object.action.admin.action.header.logo.click;

    },
    'show':function(){

        setTimeout(page_object.action.admin.action.header.show,40);
        setTimeout(page_object.action.admin.action.header_menu.show,400);
        setTimeout(page_object.action.admin.create.header.position.header_profile,40);
        setTimeout(page_object.action.admin.action.header_profile.show,400);

    }
};
