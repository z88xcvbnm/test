<?php

namespace Project\Whore\Admin\Route\Prepare;

use Core\Action\Logout\UserLogout\UserLogoutSystemAction;
use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Action\User\Admin\LogoutUserAdminAction;
use Project\Whore\Admin\Route\Page\ConfirmEmail\RouteConfirmEmailAdminWhorePage;
use Project\Whore\Admin\Route\Page\Invite\RouteInviteAdminWhorePage;
use Project\Whore\Admin\Route\Page\Payment\RouteConfirmPaymentAdminWhorePage;
use Project\Whore\Admin\Route\Page\Pdf\RoutePdfFaceDownloadAdminWhorePage;
use Project\Whore\Admin\Route\Page\RecoveryPassword\RouteRecoveryPasswordAdminWhorePage;
use Project\Whore\Admin\Route\Page\NotFound\RouteNotFoundAdminWhorePage;
use Project\Whore\Admin\Route\Redirect\Admin\Login\Dashboard\RouteAdminLoginDashboardRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\Profile\Login\Face\RouteProfileLoginFaceRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\RouteRootRedirectAdminWhore;

class RoutePrepareLinkLen2AdminWhorePage{

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin(){

        if(User::is_login())
            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            ){

                $user_login=UserLogin::get_user_login_default();

                if($user_login==Url::$list[1])
                    return RouteAdminLoginDashboardRedirectAdminWhore::init();

            }

        return RouteRootRedirectAdminWhore::init();

    }

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_pdf(){

        if(User::is_login())
            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            ){
                switch(Url::$list[1]){

                    case'face_download':
                        return RoutePdfFaceDownloadAdminWhorePage::init();

                }

            }

        return RouteRootRedirectAdminWhore::init();

    }

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile(){

        if(User::is_login())
            if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            ){

                $user_login=UserLogin::get_user_login_default();

                if($user_login==Url::$list[1])
                    return RouteProfileLoginFaceRedirectAdminWhore::init();

            }

        return RouteRootRedirectAdminWhore::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_confirm_email(){

        if(User::is_login())
            LogoutUserAdminAction::init();

        return RouteConfirmEmailAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_invite(){

        if(User::is_login())
            UserLogoutSystemAction::init();

        return RouteInviteAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_confirm_payment(){

        return RouteConfirmPaymentAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_recovery_password(){

        if(User::is_login())
            UserLogoutSystemAction::init();

        return RouteRecoveryPasswordAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set(){

        switch(Url::$list[0]){

            case'admin':
                return self::prepare_admin();

            case'profile':
                return self::prepare_profile();

            case'registration_invite':
                return self::prepare_invite();

            case'recovery_password':
                return self::prepare_recovery_password();

            case'confirm_email':
                return self::prepare_confirm_email();

            case'confirm_payment':
                return self::prepare_confirm_payment();

            case'pdf':
                return self::prepare_pdf();

            default:
                return RouteNotFoundAdminWhorePage::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}