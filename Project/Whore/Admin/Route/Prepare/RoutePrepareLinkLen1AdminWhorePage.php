<?php

namespace Project\Whore\Admin\Route\Prepare;

use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Project\Whore\Admin\Route\Page\About\RouteAboutAdminWhorePage;
use Project\Whore\Admin\Route\Page\City\RouteCityAdminWhorePage;
use Project\Whore\Admin\Route\Page\PayPalPayment\RoutePayPalPaymentAdminWhorePage;
use Project\Whore\Admin\Route\Page\Source\RouteSourceAdminWhorePage;
use Project\Whore\Admin\Route\Page\Auth\RouteAuthAdminWhorePage;
use Project\Whore\Admin\Route\Page\Reg\RouteRegAdminWhorePage;
use Project\Whore\Admin\Route\Page\NotFound\RouteNotFoundAdminWhorePage;
use Project\Whore\Admin\Route\Page\PhpInfo\RoutePhpInfoAdminWhorePage;
use Project\Whore\Admin\Route\Page\Term\RouteTermAdminWhorePage;
use Project\Whore\Admin\Route\Redirect\Admin\Login\Dashboard\RouteAdminLoginDashboardRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\Profile\Login\Face\RouteProfileLoginFaceRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\RouteRootRedirectAdminWhore;

class RoutePrepareLinkLen1AdminWhorePage{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return RouteAdminLoginDashboardRedirectAdminWhore::init();

        }

        return RouteRootRedirectAdminWhore::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_paypal_payment(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
                ||UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return RoutePayPalPaymentAdminWhorePage::init();

        }

        return RouteRootRedirectAdminWhore::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_pdf(){

        return RouteRootRedirectAdminWhore::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile(){

        if(User::is_login()){

            if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return RouteProfileLoginFaceRedirectAdminWhore::init();

        }

        return RouteRootRedirectAdminWhore::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_about(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return RouteAdminLoginDashboardRedirectAdminWhore::init();
            else if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return RouteProfileLoginFaceRedirectAdminWhore::init();

        }

        return RouteAboutAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_source(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return RouteAdminLoginDashboardRedirectAdminWhore::init();
            else if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return RouteProfileLoginFaceRedirectAdminWhore::init();

        }

        return RouteSourceAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_term(){

        if(User::is_login()){

            if(
                UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return RouteAdminLoginDashboardRedirectAdminWhore::init();
            else if(
                UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return RouteProfileLoginFaceRedirectAdminWhore::init();

        }

        return RouteTermAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_city(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return RouteAdminLoginDashboardRedirectAdminWhore::init();
            else if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return RouteProfileLoginFaceRedirectAdminWhore::init();

        }

        return RouteCityAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_reg(){

        if(User::is_login()){

            if(
                UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return RouteAdminLoginDashboardRedirectAdminWhore::init();
            else if(
                UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return RouteProfileLoginFaceRedirectAdminWhore::init();

        }

        return RouteRegAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_auth(){

        if(User::is_login()){

            if(
                UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return RouteAdminLoginDashboardRedirectAdminWhore::init();
            else if(
                UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return RouteProfileLoginFaceRedirectAdminWhore::init();

        }

        return RouteAuthAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set(){

        switch(Url::$list[0]){

            case'login':
            case'auth':
                return self::prepare_auth();

            case'reg':
                return self::prepare_reg();

            case'source':
                return self::prepare_source();

            case'about':
                return self::prepare_about();

            case'term':
                return self::prepare_term();

            case'city':
                return self::prepare_city();

            case'admin':
                return self::prepare_admin();

            case'paypal_payment_2':
                return self::prepare_paypal_payment();

            case'profile':
                return self::prepare_profile();

            case'phpinfo':
                return RoutePhpInfoAdminWhorePage::init();

            case'pdf':
                return self::prepare_pdf();

            default:
                return RouteNotFoundAdminWhorePage::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}