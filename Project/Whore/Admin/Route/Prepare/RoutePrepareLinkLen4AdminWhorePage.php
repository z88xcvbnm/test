<?php

namespace Project\Whore\Admin\Route\Prepare;

use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Route\Page\Admin\Login\Face\RouteAdminLoginFaceAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\Financial\Settings\RouteAdminLoginFinancialSettingsAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\Financial\Stats\RouteAdminLoginFinancialStatsAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\Financial\Transaction\RouteAdminLoginFinancialTransactionAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\Profile\ProfileList\RouteAdminLoginProfileListAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\Source\SourceList\RouteAdminLoginSourceListAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\Source\SourceParsing\RouteAdminLoginSourceParsingAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\User\RouteAdminLoginUserAdminWhorePage;
use Project\Whore\Admin\Route\Redirect\Admin\Login\Financial\RouteAdminLoginFinancialRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\Admin\Login\Profile\ProfileRequest\RouteAdminLoginProfileRequestRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\Admin\Login\Profile\RouteAdminLoginProfileRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\Admin\Login\RouteAdminLoginRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\Auth\RouteAuthRedirectAdminWhore;
use Project\Whore\Admin\Route\Page\NotFound\RouteNotFoundAdminWhorePage;

class RoutePrepareLinkLen4AdminWhorePage{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin_login_financial(){

        if(empty(Url::$list[3]))
            return RouteAdminLoginFaceAdminWhorePage::init();

        switch(Url::$list[3]){

            case'settings':
                return RouteAdminLoginFinancialSettingsAdminWhorePage::init();

            case'stats':
                return RouteAdminLoginFinancialStatsAdminWhorePage::init();

            case'transaction':
                return RouteAdminLoginFinancialTransactionAdminWhorePage::init();

        }

        return RouteAdminLoginFinancialRedirectAdminWhore::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin_login_profile(){

        switch(Url::$list[3]){

            case'list':
                return RouteAdminLoginProfileListAdminWhorePage::init();

            case'request':
                return RouteAdminLoginProfileRequestRedirectAdminWhore::init();

        }

        return RouteAdminLoginProfileRedirectAdminWhore::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin_login_source(){

        switch(Url::$list[3]){

            case'list':
                return RouteAdminLoginSourceListAdminWhorePage::init();

            case'request':
                return RouteAdminLoginSourceParsingAdminWhorePage::init();

        }

        return RouteAdminLoginProfileRedirectAdminWhore::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin_login(){

        switch(Url::$list[2]){

            case'financial':
                return self::prepare_admin_login_financial();

            case'profile':
                return self::prepare_admin_login_profile();

            case'source':
                return self::prepare_admin_login_source();

            case'users':
                return RouteAdminLoginUserAdminWhorePage::init();

            default:
                return RouteNotFoundAdminWhorePage::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin(){

        if(!User::is_login())
            return RouteAuthRedirectAdminWhore::init();

        if(
              UserAccess::$is_root
            ||UserAccess::$is_admin
        ){

            $user_login=UserLogin::get_user_login_default();

            if($user_login==Url::$list[1])
                return self::prepare_admin_login();

        }

        return RouteAdminLoginRedirectAdminWhore::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set(){

        switch(Url::$list[0]){

            case'admin':
                return self::prepare_admin();

            default:
                return RouteNotFoundAdminWhorePage::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}