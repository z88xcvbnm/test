<?php

namespace Project\Whore\Admin\Route\Prepare;

use Core\Module\Url\Url;
use Project\Whore\Admin\Route\Page\NotFound\RouteNotFoundAdminWhorePage;

class RoutePrepareLinkLen6AdminWhorePage{

    /**
     * @return bool
     */
    public  static function set(){

        switch(Url::$list[0]){

            default:
                return RouteNotFoundAdminWhorePage::init();

        }

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}