<?php

namespace Project\Whore\Admin\Route\Prepare;

use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Project\Whore\Admin\Route\Page\RouteRootAdminWhorePage;
use Project\Whore\Admin\Route\Redirect\Admin\Login\RouteAdminLoginRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\Profile\Login\RouteProfileLoginRedirectAdminWhore;

class RoutePrepareLinkLen0AdminWhorePage{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return RouteAdminLoginRedirectAdminWhore::init();
            else if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return RouteProfileLoginRedirectAdminWhore::init();

        }

        return RouteRootAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}