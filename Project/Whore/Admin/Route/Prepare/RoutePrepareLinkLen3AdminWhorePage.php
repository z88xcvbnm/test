<?php

namespace Project\Whore\Admin\Route\Prepare;

use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserLogin;

use Project\Whore\Admin\Route\Page\Admin\Login\Dashboard\RouteAdminLoginDashboardAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\Face\RouteAdminLoginFaceAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\Financial\RouteAdminLoginFinancialAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\Profile\RouteAdminLoginProfileAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\Settings\RouteAdminLoginSettingsAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\Source\RouteAdminLoginSourceAdminWhorePage;
use Project\Whore\Admin\Route\Page\Admin\Login\User\RouteAdminLoginUserAdminWhorePage;

use Project\Whore\Admin\Route\Page\Profile\Login\Billing\RouteProfileLoginBillingAdminWhorePage;
use Project\Whore\Admin\Route\Page\Profile\Login\City\RouteProfileLoginCityAdminWhorePage;
use Project\Whore\Admin\Route\Page\Profile\Login\Face\RouteProfileLoginFaceAdminWhorePage;
use Project\Whore\Admin\Route\Page\Profile\Login\Payment\RouteProfileLoginPaymentAdminWhorePage;
use Project\Whore\Admin\Route\Page\Profile\Login\SearchHistory\RouteProfileLoginSearchHistoryAdminWhorePage;
use Project\Whore\Admin\Route\Page\Profile\Login\Settings\RouteProfileLoginSettingsAdminWhorePage;
use Project\Whore\Admin\Route\Page\Profile\Login\Feedback\RouteProfileLoginFeedbackAdminWhorePage;

use Project\Whore\Admin\Route\Redirect\Auth\RouteAuthRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\RouteRootRedirectAdminWhore;
use Project\Whore\Admin\Route\Page\NotFound\RouteNotFoundAdminWhorePage;

class RoutePrepareLinkLen3AdminWhorePage{

    /**
     * @return bool
     */
    private static function prepare_admin_login_face(){

        return RouteAdminLoginFaceAdminWhorePage::init();

    }

    /**
     * @return bool
     */
    private static function prepare_admin_login_dashboard(){

        return RouteAdminLoginDashboardAdminWhorePage::init();

    }

    /**
     * @return bool
     */
    private static function prepare_admin_login_financial(){

        return RouteAdminLoginFinancialAdminWhorePage::init();

    }

    /**
     * @return bool
     */
    private static function prepare_admin_login_profile(){

        return RouteAdminLoginProfileAdminWhorePage::init();

    }

    /**
     * @return bool
     */
    private static function prepare_admin_login_source(){

        return RouteAdminLoginSourceAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin_login_users(){

        if(UserAccess::$is_root)
            return RouteAdminLoginUserAdminWhorePage::init();

        return RouteRootRedirectAdminWhore::init();

    }

    /**
     * @return bool
     */
    private static function prepare_admin_login_settings(){

        return RouteAdminLoginSettingsAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin(){

        if(!User::is_login())
            return RouteAuthRedirectAdminWhore::init();
        
        if(
              UserAccess::$is_root
            ||UserAccess::$is_admin
        ){

            $user_login=UserLogin::get_user_login_default();

            if($user_login!=Url::$list[1])
                return RouteAuthRedirectAdminWhore::init();

        }

        switch(Url::$list[2]){

            case'dashboard':
                return self::prepare_admin_login_dashboard();

            case'face':
                return self::prepare_admin_login_face();

            case'financial':
                return self::prepare_admin_login_financial();

            case'source':
                return self::prepare_admin_login_source();

            case'profile':
                return self::prepare_admin_login_profile();

            case'users':
                return self::prepare_admin_login_users();

            case'settings':
                return self::prepare_admin_login_settings();

            default:
                return RouteNotFoundAdminWhorePage::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile_login_face(){

        return RouteProfileLoginFaceAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile_login_city(){

        return RouteProfileLoginCityAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile_login_payment(){

        return RouteProfileLoginPaymentAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile_login_billing(){

        return RouteProfileLoginBillingAdminWhorePage::init();

    }

    /**
     * @return bool
     */
    private static function prepare_profile_login_search_history(){

        return RouteProfileLoginSearchHistoryAdminWhorePage::init();

    }

    /**
     * @return bool
     */
    private static function prepare_profile_login_settings(){

        return RouteProfileLoginSettingsAdminWhorePage::init();

    }

    /**
     * @return bool
     */
    private static function prepare_profile_login_feedback(){

        return RouteProfileLoginFeedbackAdminWhorePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile(){

        if(!User::is_login())
            return RouteAuthRedirectAdminWhore::init();

        if(
              UserAccess::$is_profile
            ||UserAccess::$is_profile_wallet
        ){

            $user_login=UserLogin::get_user_login_default();

            if($user_login!=Url::$list[1])
                return RouteAuthRedirectAdminWhore::init();

        }

        switch(Url::$list[2]){

            case'face':
                return self::prepare_profile_login_face();

            case'city':
                return self::prepare_profile_login_city();

            case'billing':
                return self::prepare_profile_login_billing();

            case'payment':
                return self::prepare_profile_login_payment();

            case'search_history':
                return self::prepare_profile_login_search_history();

            case'settings':
                return self::prepare_profile_login_settings();

            case'feedback':
                return self::prepare_profile_login_feedback();

            default:
                return RouteNotFoundAdminWhorePage::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set(){

        switch(Url::$list[0]){

            case'admin':
                return self::prepare_admin();

            case'profile':
                return self::prepare_profile();

            default:
                return RouteNotFoundAdminWhorePage::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}