<?php

namespace Project\Whore\Admin\Route\Prepare;

use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Project\Whore\Admin\Route\Page\NotFound\RouteNotFoundAdminWhorePage;
use Project\Whore\Admin\Route\Redirect\Admin\RouteAdminRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\Auth\RouteAuthRedirectAdminWhore;

class RoutePrepareLinkLen5AdminWhorePage{


    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin(){

        if(!User::is_login())
            return RouteAuthRedirectAdminWhore::init();

        if(
              UserAccess::$is_root
            ||UserAccess::$is_admin
        )
            switch(Url::$list[2]){
    
                default:
                    return RouteNotFoundAdminWhorePage::init();
    
            }
        
        return RouteAdminRedirectAdminWhore::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set(){

        switch(Url::$list[0]){

            case'admin':
                return self::prepare_admin();

            default:
                return RouteNotFoundAdminWhorePage::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}