<?php

namespace Project\Whore\Admin\Route\FileDownload;

use Core\Module\Exception\ParametersException;
use Core\Module\Url\Url;
use Project\Whore\Admin\File\Css\CssDownload;
use Project\Whore\Admin\File\Image\ImageContentDownload;
use Project\Whore\Admin\File\Image\ImageStyleDownload;
use Project\Whore\Admin\File\Js\JsDownload;
use Project\Whore\Admin\File\Video\VideoContentDownload;

class RouteAllAdminWhoreFileDownload{

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_show_css(){

        if(count(Url::$list)<3){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is not exists'
            ];

            throw new ParametersException($error);

        }

        return CssDownload::init();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_show_js(){

        if(count(Url::$list)<3){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is not exists'
            ];

            throw new ParametersException($error);

        }

        return JsDownload::init();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_download_image_content(){

        if(!isset(Url::$list[3])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is not exists'
            ];

            throw new ParametersException($error);

        }

        return ImageContentDownload::init(Url::$list[3]);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_download_video_content(){

        if(!isset(Url::$list[3])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is not exists'
            ];

            throw new ParametersException($error);

        }

        return VideoContentDownload::init(Url::$list[3]);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_show_image_style(){

        if(count(Url::$list)<6){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Link length is not correct'
            ];

            throw new ParametersException($error);

        }

        return ImageStyleDownload::init();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_show_image_content(){

        if(!isset(Url::$list[3])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is not exists'
            ];

            throw new ParametersException($error);

        }

        return ImageContentDownload::init(Url::$list[3]);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_show_video_content(){

        if(!isset(Url::$list[3])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is not exists'
            ];

            throw new ParametersException($error);

        }

        return VideoContentDownload::init(Url::$list[3]);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_download_image(){

        if(!isset(Url::$list[2])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is not exists'
            ];

            throw new ParametersException($error);

        }

        switch(Url::$list[2]){

            case'content':
                return self::prepare_download_image_content();

            default:{

                $error=[
                    'title'     =>ParametersException::$title,
                    'info'      =>'Hash is not exists'
                ];

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_download_video(){

        if(!isset(Url::$list[2])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is not exists'
            ];

            throw new ParametersException($error);

        }

        switch(Url::$list[2]){

            case'content':
                return self::prepare_download_video_content();

            default:{

                $error=[
                    'title'     =>ParametersException::$title,
                    'info'      =>'Hash is not exists'
                ];

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_show_image(){

        if(!isset(Url::$list[2])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is not exists'
            ];

            throw new ParametersException($error);

        }

        switch(Url::$list[2]){

            case'content':
                return self::prepare_show_image_content();

            case'style':
                return self::prepare_show_image_style();

            default:{

                $error=[
                    'title'     =>ParametersException::$title,
                    'info'      =>'Hash is not exists'
                ];

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_show_video(){

        if(!isset(Url::$list[2])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is not exists'
            ];

            throw new ParametersException($error);

        }

        switch(Url::$list[2]){

            case'content':
                return self::prepare_show_video_content();

            default:{

                $error=[
                    'title'     =>ParametersException::$title,
                    'info'      =>'Hash is not exists'
                ];

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_download(){

        if(isset(Url::$list[1]))
            switch(Url::$list[1]){

                case'image':
                    return self::prepare_download_image();

                case'video':
                    return self::prepare_download_video();

            }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_show(){

        if(isset(Url::$list[1]))
            switch(Url::$list[1]){

                case'image':
                    return self::prepare_show_image();

                case'video':
                    return self::prepare_show_video();

                case'css':
                    return self::prepare_show_css();

                case'js':
                    return self::prepare_show_js();

            }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(count(Url::$list)>0)
            switch(Url::$list[0]){

                case'download':
                    return self::prepare_download();

                case'show':
                    return self::prepare_show();

            }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}