<?php

namespace Project\Whore\Admin\Route\FileDownload;

use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseNotFound;
use Core\Module\Url\Url;
use Core\Module\User\User;

class RouteAdminWhoreFileDownload{

    /** @var bool */
    private static $isset_route=false;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_route_authorization(){

        return RouteAuthorizationAdminWhoreFileDownload::init();

    }

    /**
     * @return bool
     */
    private static function init_route_guest(){

        return RouteGuestAdminWhoreFileDownload::init();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_route_all(){

        return RouteAllAdminWhoreFileDownload::init();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(count(Url::$list)<3)
            return ResponseNotFound::init();

        if(User::is_login())
            self::$isset_route=self::init_route_authorization();

        if(!self::$isset_route)
            self::$isset_route=self::init_route_guest();

        if(!self::$isset_route)
            self::$isset_route=self::init_route_all();

        if(!self::$isset_route){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Action is not exits'
            ];

            throw new ParametersException($error);

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}