<?php

namespace Project\Whore\Admin\Route\Page\NotFound;

use Project\Whore\Admin\Page\NotFound\NotFoundAdminWhorePage;

class RouteNotFoundAdminWhorePage{

    /**
     * @return bool
     */
    private static function set(){

        return NotFoundAdminWhorePage::init();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}