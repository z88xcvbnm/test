<?php

namespace Project\Whore\Admin\Route\Page\Profile\Login\Payment;

use Project\Whore\Admin\Action\Face\GetFaceStatsForRootAction;
use Project\Whore\Admin\Template\Dom\Profile\RootProfileWhoreDom;

class RouteProfileLoginPaymentAdminWhorePage{

    /** @var int */
    public  static $face_len                    =0;

    /** @var int */
    public  static $face_image_indexed_len      =0;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_root_stats(){

        $r=GetFaceStatsForRootAction::init();

        self::$face_len                     =$r['face_len'];
        self::$face_image_indexed_len       =$r['face_image_indexed_len'];

        return true;

    }

    /**
     * @return bool
     */
    private static function init_page(){

        return RootProfileWhoreDom::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_root_stats();

        return self::init_page();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}