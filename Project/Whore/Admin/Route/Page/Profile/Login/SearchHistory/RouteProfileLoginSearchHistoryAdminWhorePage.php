<?php

namespace Project\Whore\Admin\Route\Page\Profile\Login\SearchHistory;

use Project\Whore\Admin\Template\Dom\Profile\RootProfileWhoreDom;

class RouteProfileLoginSearchHistoryAdminWhorePage{

    /**
     * @return bool
     */
    private static function init_page(){

        return RootProfileWhoreDom::init();

    }

    /**
     * @return bool
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}