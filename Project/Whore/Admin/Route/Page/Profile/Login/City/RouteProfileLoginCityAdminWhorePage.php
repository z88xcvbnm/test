<?php

namespace Project\Whore\Admin\Route\Page\Profile\Login\City;

use Project\Whore\Admin\Template\Dom\Profile\City\ProfileCityWhoreDom;
use Project\Whore\All\Module\Face\FaceCity;

class RouteProfileLoginCityAdminWhorePage{

    /** @var array */
    public  static $city_list               =[];

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_city_list(){

        self::$city_list=FaceCity::get_face_city_stats_list(100);

        return true;

    }

    /**
     * @return bool
     */
    private static function init_page(){

        return ProfileCityWhoreDom::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_city_list();

        return self::init_page();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}