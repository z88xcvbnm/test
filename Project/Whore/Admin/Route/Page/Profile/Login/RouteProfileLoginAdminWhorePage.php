<?php

namespace Project\Whore\Admin\Route\Page\Profile\Login;

use Core\Module\Url\Url;
use Project\Whore\Admin\Route\Redirect\Auth\RouteAuthRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\Profile\Login\RouteProfileLoginRedirectAdminWhore;

class RouteProfileLoginAdminWhorePage{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        switch(count(Url::$list)){

            case 0:
                return RouteProfileLoginRedirectAdminWhore::init();

            default:
                return RouteAuthRedirectAdminWhore::init();

        }

    }

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::set();

    }

}