<?php

namespace Project\Whore\Admin\Route\Page\Profile\Login\Settings;

use Project\Whore\Admin\Template\Dom\Root\RootRootWhoreDom;

class RouteProfileLoginSettingsAdminWhorePage{

    /**
     * Init root admin Whore page
     */
    private static function init_page(){

        return RootRootWhoreDom::init();

    }

    /**
     * Prepare
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * Init root page
     */
    public  static function init(){

        return self::set();

    }

}