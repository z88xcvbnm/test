<?php

namespace Project\Whore\Admin\Route\Page\Pdf;

use Project\Whore\Admin\Template\Dom\Pdf\PdfFaceDownloadWhoreDom;

class RoutePdfFaceDownloadAdminWhorePage{

    /**
     * @return bool
     */
    private static function set(){

        return PdfFaceDownloadWhoreDom::init();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}