<?php

namespace Project\Whore\Admin\Route\Page\Payment;

use Core\Module\Url\Url;
use Project\Whore\Admin\Action\Payment\ConfirmPaymentAction;
use Project\Whore\Admin\Route\Redirect\RouteRootRedirectAdminWhore;
use Project\Whore\Admin\Template\Dom\Payment\RootConfirmPaymentWhoreDom;
use Project\Whore\Admin\Template\Dom\Root\RootRootWhoreDom;

class RouteConfirmPaymentAdminWhorePage{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_confirm_payment(){

        return ConfirmPaymentAction::init(Url::$list[1]);

    }

    /**
     * @return bool
     */
    private static function init_page(){

        return RootConfirmPaymentWhoreDom::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        switch(count(Url::$list)){

            case 2:{

                switch(Url::$list[0]){

                    case'confirm_payment':{

                        if(empty(Url::$list[1]))
                            return RouteRootRedirectAdminWhore::init();

                        self::prepare_confirm_payment();

                        return self::init_page();

                    }

                    default:
                        return RouteRootRedirectAdminWhore::init();

                }

            }

            default:
                return RouteRootRedirectAdminWhore::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}