<?php

namespace Project\Whore\Admin\Route\Page\PayPalPayment;

use Project\Whore\Admin\Template\Dom\Root\PayPalPayment\RootPayPalPaymentWhoreDom;

class RoutePayPalPaymentAdminWhorePage{

    /**
     * @return bool
     */
    private static function init_page(){

        return RootPayPalPaymentWhoreDom::init();

    }

    /**
     * @return bool
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}