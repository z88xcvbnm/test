<?php

namespace Project\Whore\Admin\Route\Page\RecoveryPassword;

use Core\Module\Url\Url;
use Project\Whore\Admin\Route\Redirect\RouteRootRedirectAdminWhore;
use Project\Whore\Admin\Template\Dom\Root\RootRootWhoreDom;

class RouteRecoveryPasswordAdminWhorePage{

    /**
     * @return bool
     */
    private static function init_page(){

        return RootRootWhoreDom::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        switch(count(Url::$list)){

            case 2:{

                switch(Url::$list[0]){

                    case'recovery_password':{

                        if(empty(Url::$list[1]))
                            return RouteRootRedirectAdminWhore::init();

                        return self::init_page();

                    }

                    default:
                        return RouteRootRedirectAdminWhore::init();

                }

            }

            default:
                return RouteRootRedirectAdminWhore::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}