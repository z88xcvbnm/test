<?php

namespace Project\Whore\Admin\Route\Page\Term;

use Project\Whore\Admin\Template\Dom\Root\Term\RootTermWhoreDom;

class RouteTermAdminWhorePage{

    /**
     * @return bool
     */
    private static function init_page(){

        return RootTermWhoreDom::init();

    }

    /**
     * @return bool
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}