<?php

namespace Project\Whore\Admin\Route\Page\PhpInfo;

use Project\Whore\Admin\Page\PhpInfo\PhpInfoAdminWhorePage;

class RoutePhpInfoAdminWhorePage{

    /**
     * @return bool
     */
    private static function set(){

        return PhpInfoAdminWhorePage::init();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}