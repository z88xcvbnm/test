<?php

namespace Project\Whore\Admin\Route\Page\Reg;

use Project\Whore\Admin\Template\Dom\Root\RootRootWhoreDom;

class RouteRegAdminWhorePage{

    /**
     * @return bool
     */
    private static function init_page(){

        return RootRootWhoreDom::init();

    }

    /**
     * @return bool
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}