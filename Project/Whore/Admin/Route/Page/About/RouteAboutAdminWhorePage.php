<?php

namespace Project\Whore\Admin\Route\Page\About;

use Project\Whore\Admin\Template\Dom\Root\About\RootAboutWhoreDom;

class RouteAboutAdminWhorePage{

    /**
     * @return bool
     */
    private static function init_page(){

        return RootAboutWhoreDom::init();

    }

    /**
     * @return bool
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}