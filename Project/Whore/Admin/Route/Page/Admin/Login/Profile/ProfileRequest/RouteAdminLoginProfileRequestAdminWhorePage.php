<?php

namespace Project\Whore\Admin\Route\Page\Admin\Login\Profile\ProfileRequest;

use Project\Whore\Admin\Template\Dom\Admin\RootAdminWhoreDom;

class RouteAdminLoginProfileRequestAdminWhorePage{

    /**
     * @return bool
     */
    private static function init_page(){

        return RootAdminWhoreDom::init();

    }

    /**
     * @return bool
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}