<?php

namespace Project\Whore\Admin\Route\Page\Admin\Login;

use Core\Module\Url\Url;
use Project\Whore\Admin\Route\Redirect\Admin\Login\RouteAdminLoginRedirectAdminWhore;
use Project\Whore\Admin\Route\Redirect\Auth\RouteAuthRedirectAdminWhore;

class RouteAdminLoginAdminWhorePage{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        switch(count(Url::$list)){

            case 0:
                return RouteAdminLoginRedirectAdminWhore::init();

            default:
                return RouteAuthRedirectAdminWhore::init();

        }

    }

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::set();

    }

}