<?php

namespace Project\Whore\Admin\Route\Page\Admin\Login\Settings;

use Project\Whore\Admin\Template\Dom\Admin\RootAdminWhoreDom;

class RouteAdminLoginSettingsAdminWhorePage{

    /**
     * Init root admin Whore page
     */
    private static function init_page(){

        return RootAdminWhoreDom::init();

    }

    /**
     * Prepare
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * Init root page
     */
    public  static function init(){

        return self::set();

    }

}