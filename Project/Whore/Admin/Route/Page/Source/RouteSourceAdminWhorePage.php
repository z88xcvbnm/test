<?php

namespace Project\Whore\Admin\Route\Page\Source;

use Project\Whore\Admin\Template\Dom\Root\Source\RootSourceWhoreDom;

class RouteSourceAdminWhorePage{

    /**
     * @return bool
     */
    private static function init_page(){

        return RootSourceWhoreDom::init();

    }

    /**
     * @return bool
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}