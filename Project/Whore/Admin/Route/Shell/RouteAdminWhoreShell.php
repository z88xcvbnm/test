<?php

namespace Project\Whore\Admin\Route\Shell;

use Core\Module\Exception\ParametersException;
use Core\Module\Url\Url;
use Project\Whore\Admin\Api\Source\CheckModelZoneLastIDApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingModelZoneApi;
use Project\Whore\Admin\Shell\Source\AddFaceTokenListToFacePlusPlusShell;
use Project\Whore\Admin\Shell\Source\GetSourceCheck50OldParsingEmilyDatesLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceCheck50OldParsingJuliaDatesLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceCheckParsingDosugDuplicateLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceCheckParsingEmilyDatesLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceCheckParsingJuliaDatesLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceCheckParsingZolushkaProjectHeightAndWeightLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingAtolinLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingDosugFirmLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingDosugLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingDosugLoginLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingEmilyDatesLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingEscortNewsLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingFavoritkaLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingFeiKievLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingGorchizaLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingIntimCityLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingJuliaDatesLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingLoveLamaLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingLovesssLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingModelZoneNewLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingModelZoneVideoLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingParamoursLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingProstitutkiKievaInfoLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingProstitutkiKievaLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingProstitutkiPiteraGoodLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingProstitutkiRedLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingProstitutkiSPbLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingProstitutkiSPbLocalToLocalShellOnlyPage;
use Project\Whore\Admin\Shell\Source\GetSourceParsingSoderjankiOnlineLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingSoderjantkiRFLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingWhores777LocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingYescortLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\GetSourceParsingZolushkaProjectLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\RemoveDuplicateImageFromModelZoneLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\TestTelegramShell;
use Project\Whore\Admin\Shell\Source\TrigDuplicateFaceLocalToLocalShell;
use Project\Whore\Admin\Shell\Source\UpdateFaceCityStatsShell;
use Project\Whore\Admin\Shell\Source\UpdateFaceDataInfoHeightAndWeightShell;
use Project\Whore\Admin\Shell\Source\UpdateFaceDataInfoShell;
use Project\Whore\Admin\Shell\Mailing\MailingShell;

class RouteAdminWhoreShell{

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShluhamNetException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        if(!isset(Url::$shell_action)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Shell action is not exists'
            ];

            throw new ParametersException($error);

        }

        switch(Url::$shell_action){

            case'parsing_source_model_zone':
                return GetSourceParsingModelZoneApi::init();

            case'parsing_atolin':
                return GetSourceParsingAtolinLocalToLocalShell::init();

            case'parsing_lovesss':
                return GetSourceParsingLovesssLocalToLocalShell::init();

            case'parsing_soderjanki_rf':
                return GetSourceParsingSoderjantkiRFLocalToLocalShell::init();

            case'parsing_model_zone':
                return GetSourceParsingModelZoneNewLocalToLocalShell::init();

            case'parsing_whores777':
                return GetSourceParsingWhores777LocalToLocalShell::init();

            case'parsing_prostitutki_spb':
                return GetSourceParsingProstitutkiSPbLocalToLocalShell::init();

            case'parsing_prostitutki_spb_only_page':
                return GetSourceParsingProstitutkiSPbLocalToLocalShellOnlyPage::init();

            case'parsing_prostitutki_red':
                return GetSourceParsingProstitutkiRedLocalToLocalShell::init();

            case'parsing_prostitutki_pitera_good':
                return GetSourceParsingProstitutkiPiteraGoodLocalToLocalShell::init();

            case'parsing_escort_news':
                return GetSourceParsingEscortNewsLocalToLocalShell::init();

            case'parsing_dosug':
                return GetSourceParsingDosugLocalToLocalShell::init();

            case'parsing_dosug_firm':
                return GetSourceParsingDosugFirmLocalToLocalShell::init();

            case'parsing_gorchiza':
                return GetSourceParsingGorchizaLocalToLocalShell::init();

            case'parsing_fei_kieva':
                return GetSourceParsingFeiKievLocalToLocalShell::init();

            case'parsing_prostitutki_kieva':
                return GetSourceParsingProstitutkiKievaLocalToLocalShell::init();

            case'parsing_prostitutki_kieva_info':
                return GetSourceParsingProstitutkiKievaInfoLocalToLocalShell::init();

            case'parsing_julia_dates':
                return GetSourceParsingJuliaDatesLocalToLocalShell::init();

            case'parsing_emily_dates':
                return GetSourceParsingEmilyDatesLocalToLocalShell::init();

            case'check_parsing_julia_dates':
                return GetSourceCheckParsingJuliaDatesLocalToLocalShell::init();

            case'check_parsing_emily_dates':
                return GetSourceCheckParsingEmilyDatesLocalToLocalShell::init();

            case'check_parsing_50_old_julia_dates':
                return GetSourceCheck50OldParsingJuliaDatesLocalToLocalShell::init();

            case'check_parsing_50_old_emily_dates':
                return GetSourceCheck50OldParsingEmilyDatesLocalToLocalShell::init();

            case'parsing_soderjanki_online':
                return GetSourceParsingSoderjankiOnlineLocalToLocalShell::init();

            case'parsing_intim_city':
                return GetSourceParsingIntimCityLocalToLocalShell::init();

            case'parsing_zolushka_project':
                return GetSourceParsingZolushkaProjectLocalToLocalShell::init();

            case'parsing_favoritka':
                return GetSourceParsingFavoritkaLocalToLocalShell::init();

            case'parsing_paramours':
                return GetSourceParsingParamoursLocalToLocalShell::init();

            case'parsing_lovelama':
                return GetSourceParsingLoveLamaLocalToLocalShell::init();

            case'parsing_model_zone_video':
                return GetSourceParsingModelZoneVideoLocalToLocalShell::init();

            case'parsing_yescort':
                return GetSourceParsingYescortLocalToLocalShell::init();

            case'send_face_token_to_face_plus_plus':
                return AddFaceTokenListToFacePlusPlusShell::init();

            case'remove_duplicate_image_from_model_zone':
                return RemoveDuplicateImageFromModelZoneLocalToLocalShell::init();

            case'trig_duplicate_face':
                return TrigDuplicateFaceLocalToLocalShell::init();

            case'check_model_zone_last_id':
                return CheckModelZoneLastIDApi::init();

            case'update_face_city_stats':
                return UpdateFaceCityStatsShell::init();

            case'update_face_data_info':
                return UpdateFaceDataInfoShell::init();

            case'get_source_check_parsing_zolushka_project_height_and_weight':
                return GetSourceCheckParsingZolushkaProjectHeightAndWeightLocalToLocalShell::init();

            case'update_face_data_info_height_and_weight':
                return UpdateFaceDataInfoHeightAndWeightShell::init();

            case'get_source_check_parsing_dosug_duplicate':
                return GetSourceCheckParsingDosugDuplicateLocalToLocalShell::init();

            case'get_source_parsing_dosug_login':
                return GetSourceParsingDosugLoginLocalToLocalShell::init();

            case'test_telegram':
                return TestTelegramShell::init();

            case'mailing':
                return MailingShell::init();

            default:{

                $error=[
                    'title'     =>ParametersException::$title,
                    'info'      =>'Action is not valid'
                ];

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShluhamNetException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        return self::set();

    }

}