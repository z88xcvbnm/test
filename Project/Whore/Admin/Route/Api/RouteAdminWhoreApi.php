<?php

namespace Project\Whore\Admin\Route\Api;

use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseNotFound;
use Core\Module\Url\Url;
use Core\Module\User\User;

class RouteAdminWhoreApi{

    /** @var bool */
    private static $isset_route=false;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\ShluhamNetException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Mpdf\Src\MpdfException
     * @throws \ImagickException
     */
    private static function init_route_authorization(){

        return RouteAuthorizationAdminWhoreApi::init();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     * @throws \ImagickException
     */
    private static function init_route_guest(){

        return RouteGuestAdminWhoreApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function init_route_all(){

        return RouteAllAdminWhoreApi::init();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\ShluhamNetException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     * @throws \Core\Module\Mpdf\Src\MpdfException
     * @throws \ImagickException
     */
    private static function set(){

        if(count(Url::$list)<3)
            return ResponseNotFound::init();

        if(User::is_login())
            self::$isset_route=self::init_route_authorization();

        if(!self::$isset_route)
            self::$isset_route=self::init_route_guest();

        if(!self::$isset_route)
            self::$isset_route=self::init_route_all();

        if(!self::$isset_route){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Action is not exits'
            ];

            throw new ParametersException($error);

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\ShluhamNetException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     * @throws \Core\Module\Mpdf\Src\MpdfException
     * @throws \ImagickException
     */
    public  static function init(){

        return self::set();

    }

}