<?php

namespace Project\Whore\Admin\Route\Api;

use Core\Module\Url\Url;
use Project\Whore\Admin\Api\Auth\UserAuth\UserAuthApi;
use Project\Whore\Admin\Api\File\UploadFileFromLinkApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi;
use Project\Whore\Admin\Api\Token\GetTokenApi;
use Project\Whore\Admin\Api\User\Admin\InviteRegistrationUserAdminApi;
use Project\Whore\Admin\Api\User\Admin\RecoveryPasswordUserAdminApi;
use Project\Whore\Admin\Api\User\Data\SaveUserRecoveryPasswordApi;
use Project\Whore\Admin\Api\User\IssetData\IssetUserEmailApi;
use Project\Whore\Admin\Api\User\IssetData\IssetUserLoginApi;
use Project\Whore\Admin\Api\User\Profile\AddProfileApi;

class RouteGuestAdminWhoreApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     * @throws \ImagickException
     */
    private static function set(){

        if(count(Url::$list)>=3)
            switch(Url::$list[2]){

                case'save_user_settings':
                case'create_admin_password':
                    return InviteRegistrationUserAdminApi::init();

                case'forgot_password':
                    return RecoveryPasswordUserAdminApi::init();

                case'reset_password':
                    return SaveUserRecoveryPasswordApi::init();

                case'get_trigger':
                case'get_route_page':
                    return GetRoutePageApi::init();

                case'get_token':
                    return GetTokenApi::init();

                case'auth':
                case'auth_admin':
                case'user_auth':
                    return UserAuthApi::init();

                case'isset_user_login':
                    return IssetUserLoginApi::init();

                case'isset_user_email':
                    return IssetUserEmailApi::init();

                case'reg':
                case'add_profile':
                    return AddProfileApi::init();

                case'upload_file_from_link':
                    return UploadFileFromLinkApi::init();

            }

        return false;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     * @throws \ImagickException
     */
    public  static function init(){

        return self::set();

    }

}