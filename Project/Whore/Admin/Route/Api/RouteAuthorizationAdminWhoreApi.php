<?php

namespace Project\Whore\Admin\Route\Api;

use Core\Module\Url\Url;
use Project\Whore\Admin\Api\Face\AddFaceApi;
use Project\Whore\Admin\Api\Face\AddFaceToFacePlusPlusApi;
use Project\Whore\Admin\Api\Face\AddFaceVideoApi;
use Project\Whore\Admin\Api\Face\AddImageToFacePlusPlusApi;
use Project\Whore\Admin\Api\Face\CreatePdfFileApi;
use Project\Whore\Admin\Api\Face\CreatePdfFileComboApi;
use Project\Whore\Admin\Api\Face\EditFaceDataApi;
use Project\Whore\Admin\Api\Face\EditImageToFacePlusPlusApi;
use Project\Whore\Admin\Api\Face\GetFaceImageListApi;
use Project\Whore\Admin\Api\Face\GetFaceListApi;
use Project\Whore\Admin\Api\Face\GetFacesCoordsFacePlusPlusApi;
use Project\Whore\Admin\Api\Face\GetFaceSearchComboResultApi;
use Project\Whore\Admin\Api\Face\GetFaceSearchComboResultWithFaceTriggerApi;
use Project\Whore\Admin\Api\Face\GetFaceSearchHistoryApi;
use Project\Whore\Admin\Api\Face\GetFaceVideoListApi;
use Project\Whore\Admin\Api\Face\GetProfileFaceImageListWithFaceTriggerApi;
use Project\Whore\Admin\Api\Face\GetProfileFaceSearchHistoryApi;
use Project\Whore\Admin\Api\Face\IssetFaceSourceLinkApi;
use Project\Whore\Admin\Api\Face\RemoveFaceApi;
use Project\Whore\Admin\Api\Face\SaveFaceImageListApi;
use Project\Whore\Admin\Api\Face\SaveFaceVideoListApi;
use Project\Whore\Admin\Api\Face\SearchFaceApi;
use Project\Whore\Admin\Api\Face\SearchFacePartApi;
use Project\Whore\Admin\Api\Face\SearchFacePartComboApi;
use Project\Whore\Admin\Api\Face\SearchFacePartComboWithTrigFaceApi;
use Project\Whore\Admin\Api\Faceset\GetFaceSearchPartsLenApi;
use Project\Whore\Admin\Api\Faceset\GetFaceSearchPartsLenComboApi;
use Project\Whore\Admin\Api\Faceset\SyncFacesetListApi;
use Project\Whore\Admin\Api\Feedback\FeedbackApi;
use Project\Whore\Admin\Api\File\UploadFileChunkApi;
use Project\Whore\Admin\Api\Image\SetImageItemHashLinkApi;
use Project\Whore\Admin\Api\Logout\UserLogout\UserLogoutApi;
use Project\Whore\Admin\Api\Profile\SendEmailToProfileApi;
use Project\Whore\Admin\Api\Source\AddFaceTokenListToFacePlusPlusApi;
use Project\Whore\Admin\Api\Source\GetAuthPageLovesssLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetLogInLovesssLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetModelZoneProfileDataLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingAtolinLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingDosugLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingEmilyDatesLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingEscortNewsLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingFavoritkaLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingFeiKievLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingGorchizaLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingIntimCityLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingJuliaDatesLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingJuliaDatesTestLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingLoveLamaLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingLovesssLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingLubovnikiRFLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingModelZoneApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingModelZoneFromBackupApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingModelZoneLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingModelZoneLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingModelZoneNewLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingModelZoneWithOneImageLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingParamoursLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingProstitutkiKievaInfoLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingProstitutkiKievaLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingProstitutkiPiteraGoodLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingProstitutkiRedLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingProstitutkiSPbLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingSoderjankiOnlineLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingSoderjantkiRFLocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingWhores777LocalToLocalApi;
use Project\Whore\Admin\Api\Source\GetSourceParsingZolushkaProjectLocalToLocalApi;
use Project\Whore\Admin\Api\Source\ReplaceWeightHeightInInfoApi;
use Project\Whore\Admin\Api\User\AddAdminApi;
use Project\Whore\Admin\Api\User\Admin\BlockUserAdminApi;
use Project\Whore\Admin\Api\User\Admin\EditUserAdminApi;
use Project\Whore\Admin\Api\User\Admin\RemoveUserAdminApi;
use Project\Whore\Admin\Api\User\Admin\SaveUserAvatarApi;
use Project\Whore\Admin\Api\User\Admin\SetToAdminUserAdminApi;
use Project\Whore\Admin\Api\User\BlockAdminApi;
use Project\Whore\Admin\Api\User\Data\SaveUserEmailApi;
use Project\Whore\Admin\Api\User\Data\SaveUserLoginApi;
use Project\Whore\Admin\Api\User\Data\SaveUserNameApi;
use Project\Whore\Admin\Api\User\Data\SaveUserPasswordApi;
use Project\Whore\Admin\Api\User\Profile\AddProfileBalanceTransactionApi;
use Project\Whore\Admin\Api\User\Profile\AddProfileInviteApi;
use Project\Whore\Admin\Api\User\Profile\GetProfileListApi;
use Project\Whore\Admin\Api\User\Profile\RemoveProfileApi;
use Project\Whore\Admin\Api\User\Profile\UpdateProfileBalanceApi;
use Project\Whore\Admin\Api\User\RemoveAdminApi;
use Project\Whore\Admin\Api\User\RemoveProfileAccountApi;
use Project\Whore\Admin\Api\User\UpdateAdminAccessApi;
use Project\Whore\Admin\Api\User\UpdateAdminApi;
use Project\Whore\Admin\Api\Face\GetProfileFaceImageListApi;
use Project\Whore\Admin\Api\User\UpdateProfileApi;

class RouteAuthorizationAdminWhoreApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\ShluhamNetException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Mpdf\Src\MpdfException
     * @throws \ImagickException
     */
    private static function set(){

        if(count(Url::$list)>=3)
            switch(Url::$list[2]){

                case'add_payment':
                    return AddProfileBalanceTransactionApi::init();

                case'get_face_search_history':
                    return GetFaceSearchHistoryApi::init();

                case'get_profile_face_search_history':
                    return GetProfileFaceSearchHistoryApi::init();

                case'set_image_item_hash_link':
                    return SetImageItemHashLinkApi::init();

                case'update_profile_balance':
                    return UpdateProfileBalanceApi::init();

                case'update_profile':
                    return UpdateProfileApi::init();

                case'remove_profile':
                    return RemoveProfileApi::init();

                case'add_profile_invite':
                    return AddProfileInviteApi::init();

                case'get_profile_list':
                    return GetProfileListApi::init();

                case'create_face_pdf_file':
                    return CreatePdfFileApi::init();

                case'create_face_pdf_file_combo':
                    return CreatePdfFileComboApi::init();

                case'search_face':
                    return SearchFaceApi::init();

                case'get_face_search_parts_len':{

//                    \Config::$is_debug=true;

                    return GetFaceSearchPartsLenApi::init();

                }

                case'get_face_search_parts_len_combo':{

//                    \Config::$is_debug=true;

                    return GetFaceSearchPartsLenComboApi::init();

                }

                case'get_face_search_combo_result':{

//                    if(empty($_POST['is_test']))
//                        return GetFaceSearchComboResultApi::init();
//
//                    \Config::$is_debug=true;

                    return GetFaceSearchComboResultWithFaceTriggerApi::init();

                }

                case'search_face_part':{

//                    \Config::$is_debug=true;

                    return SearchFacePartApi::init();

                }

                case'search_face_part_combo':{

//                    if(empty($_POST['is_test']))
//                        return SearchFacePartComboApi::init();
//
//                    \Config::$is_debug=true;

                    return SearchFacePartComboWithTrigFaceApi::init();

                }

                case'feedback':
                    return FeedbackApi::init();

                case'send_email_to_profile':
                    return SendEmailToProfileApi::init();

                case'add_face':
                    return AddFaceApi::init();

                case'add_face_to_face_plus_plus':
                    return AddFaceToFacePlusPlusApi::init();

                case'edit_face':
                    return EditFaceDataApi::init();

                case'sync_faceset_list':
                    return SyncFacesetListApi::init();

                case'add_face_token_list_to_face_plus_plus':
                    return AddFaceTokenListToFacePlusPlusApi::init();

                case'add_face_image':
                    return AddImageToFacePlusPlusApi::init();

                case'add_face_video':
                    return AddFaceVideoApi::init();

                case'edit_face_image':
                    return EditImageToFacePlusPlusApi::init();

                case'get_profile_face_image_list':{

//                    if(empty($_POST['is_test']))
//                        return GetProfileFaceImageListApi::init();
//
//                    \Config::$is_debug=true;

                    return GetProfileFaceImageListWithFaceTriggerApi::init();


                }

                case'get_faces_coords':
                    return GetFacesCoordsFacePlusPlusApi::init();

                case'get_face_list':
                    return GetFaceListApi::init();

                case'get_face_image_list':
                    return GetFaceImageListApi::init();

                case'get_face_video_list':
                    return GetFaceVideoListApi::init();

                case'save_face_image_list':
                    return SaveFaceImageListApi::init();

                case'save_face_video_list':
                    return SaveFaceVideoListApi::init();

                case'remove_face':
                    return RemoveFaceApi::init();

                case'isset_face_source_account_link':
                    return IssetFaceSourceLinkApi::init();

                case'get_source_parsing_model_zone':
                    return GetSourceParsingModelZoneApi::init();

                case'get_source_parsing_model_zone_localhost':
                    return GetSourceParsingModelZoneLocalApi::init();

                case'get_source_parsing_model_zone_localhost_to_localhost':
                    return GetSourceParsingModelZoneLocalToLocalApi::init();

                case'get_source_parsing_model_zone_profile_data_localhost_to_localhost':
                    return GetModelZoneProfileDataLocalToLocalApi::init();

                case'get_source_parsing_model_zone_new_localhost_to_localhost':
                    return GetSourceParsingModelZoneNewLocalToLocalApi::init();

                case'get_source_parsing_dosug_localhost_to_localhost':
                    return GetSourceParsingDosugLocalToLocalApi::init();

                case'get_source_parsing_escort_news_localhost_to_localhost':
                    return GetSourceParsingEscortNewsLocalToLocalApi::init();

                case'get_source_parsing_atolin_localhost_to_localhost':
                    return GetSourceParsingAtolinLocalToLocalApi::init();

                case'get_auth_page_lovesss_localhost_to_localhost':
                    return GetAuthPageLovesssLocalToLocalApi::init();

                case'get_login_lovesss_localhost_to_localhost':
                    return GetLogInLovesssLocalToLocalApi::init();

                case'get_source_parsing_lovesss_localhost_to_localhost':
                    return GetSourceParsingLovesssLocalToLocalApi::init();

                case'get_source_parsing_soderjantki_localhost_to_localhost':
                    return GetSourceParsingSoderjantkiRFLocalToLocalApi::init();

                case'get_source_parsing_whores777_localhost_to_localhost':
                    return GetSourceParsingWhores777LocalToLocalApi::init();

                case'get_source_parsing_prostitutki_spb_localhost_to_localhost':
                    return GetSourceParsingProstitutkiSPbLocalToLocalApi::init();

                case'get_source_parsing_prostitutki_red_localhost_to_localhost':
                    return GetSourceParsingProstitutkiRedLocalToLocalApi::init();

                case'get_source_parsing_prostitutki_pitera_good_localhost_to_localhost':
                    return GetSourceParsingProstitutkiPiteraGoodLocalToLocalApi::init();

                case'get_source_parsing_gorchiza_localhost_to_localhost':
                    return GetSourceParsingGorchizaLocalToLocalApi::init();

                case'get_source_parsing_prostitutki_kieva_localhost_to_localhost':
                    return GetSourceParsingProstitutkiKievaLocalToLocalApi::init();

                case'get_source_parsing_prostitutki_kieva_info_localhost_to_localhost':
                    return GetSourceParsingProstitutkiKievaInfoLocalToLocalApi::init();

                case'get_source_parsing_fei_kieva_localhost_to_localhost':
                    return GetSourceParsingFeiKievLocalToLocalApi::init();

                case'get_source_parsing_julia_dates_localhost_to_localhost':
                    return GetSourceParsingJuliaDatesLocalToLocalApi::init();

                case'get_source_parsing_julia_dates_test_localhost_to_localhost':
                    return GetSourceParsingJuliaDatesTestLocalToLocalApi::init();

                case'get_source_parsing_emily_dates_localhost_to_localhost':
                    return GetSourceParsingEmilyDatesLocalToLocalApi::init();

                case'get_source_parsing_lubovniki_localhost_to_localhost':
                    return GetSourceParsingLubovnikiRFLocalToLocalApi::init();

                case'get_source_parsing_model_zone_with_one_image_localhost_to_localhost':
                    return GetSourceParsingModelZoneWithOneImageLocalToLocalApi::init();

                case'get_source_parsing_soderganki_online_localhost_to_localhost':
                    return GetSourceParsingSoderjankiOnlineLocalToLocalApi::init();

                case'get_source_parsing_intim_city_localhost_to_localhost':
                    return GetSourceParsingIntimCityLocalToLocalApi::init();

                case'get_source_parsing_zolushka_project_localhost_to_localhost':
                    return GetSourceParsingZolushkaProjectLocalToLocalApi::init();

                case'get_source_parsing_favoritka_localhost_to_localhost':
                    return GetSourceParsingFavoritkaLocalToLocalApi::init();

                case'get_source_parsing_paramours_localhost_to_localhost':
                    return GetSourceParsingParamoursLocalToLocalApi::init();

                case'get_source_parsing_lovelama_localhost_to_localhost':
                    return GetSourceParsingLoveLamaLocalToLocalApi::init();

                case'get_source_parsing_model_zone_backup':
                    return GetSourceParsingModelZoneFromBackupApi::init();

                case'replace_weight_and_height_in_info':
                    return ReplaceWeightHeightInInfoApi::init();

                case'add_admin':
                    return AddAdminApi::init();

                case'update_admin':
                    return UpdateAdminApi::init();

                case'update_admin_access':
                    return UpdateAdminAccessApi::init();

                case'block_admin':
                    return BlockAdminApi::init();

                case'remove_admin':
                    return RemoveAdminApi::init();

                case'remove_profile_account':
                    return RemoveProfileAccountApi::init();

                case'set_admin':
                case'edit_admin':
                    return EditUserAdminApi::init();

                case'set_user_image':
                    return SaveUserAvatarApi::init();

                case'set_user_password':
                    return SaveUserPasswordApi::init();

                case'set_user_name':
                    return SaveUserNameApi::init();

                case'set_user_email':
                    return SaveUserEmailApi::init();

                case'set_user_login':
                    return SaveUserLoginApi::init();

                case'remove_user':
                    return RemoveUserAdminApi::init();

                case'block_user':
                    return BlockUserAdminApi::init();

                case'set_user_to_admin':
                    return SetToAdminUserAdminApi::init();

                case'user_logout':
                    return UserLogoutApi::init();

                case'upload_file_chunk':
                    return UploadFileChunkApi::init();

            }

        return false;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\ShluhamNetException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Mpdf\Src\MpdfException
     * @throws \ImagickException
     */
    public  static function init(){

        return self::set();

    }

}