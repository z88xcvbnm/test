<?php

namespace Project\Whore\Admin\Route\Api;

use Core\Module\Exception\ParametersException;
use Core\Module\Url\Url;
use Core\Module\User\User;
use Project\Whore\Admin\Api\Face\UpdateCountryAndCityApi;
use Project\Whore\Admin\Api\Image\SetImageItemHashLinkApi;
use Project\Whore\Admin\Api\PayPal\PayPalPaymentCheckApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi;
use Project\Whore\Admin\Api\Token\GetTokenApi;
use Project\Whore\Admin\Api\User\IssetData\IssetUserEmailApi;
use Project\Whore\Admin\Api\User\IssetData\IssetUserLoginApi;
use Project\Whore\All\Api\Face\AddFaceDataApi;
use Project\Whore\All\Api\Face\AddFaceImageApi;
use Project\Whore\All\Api\Face\GetFaceIdApi;
use Project\Whore\All\Api\Face\GetFaceIDListApi;
use Project\Whore\All\Api\Face\UpdateFaceToPublicApi;

class RouteAllAdminWhoreApi{

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        if(count(Url::$list)>=3){

            switch(Url::$list[2]){

                case'set_image_item_hash_link':
                    return SetImageItemHashLinkApi::init();

                case'get_trigger':
                case'get_route_page':
                    return GetRoutePageApi::init();

                case'get_token':
                    return GetTokenApi::init();

                case'isset_user_login':
                    return IssetUserLoginApi::init();

                case'isset_user_email':
                    return IssetUserEmailApi::init();

                case'update_country_and_city':
                    return UpdateCountryAndCityApi::init();

                case'paypal_payment_check':
                    return PayPalPaymentCheckApi::init();

            }

            if(!empty($_POST['key_hash']))
                if($_POST['key_hash']=='domino777'){

                    User::$user_ID=-1;

                    switch(Url::$list[2]){

                        case'add_face_data':
                            return AddFaceDataApi::init();

                        case'add_face_image':
                            return AddFaceImageApi::init();

                        case'get_face_id':
                            return GetFaceIdApi::init();

                        case'update_face_to_public':
                            return UpdateFaceToPublicApi::init();

                        case'get_face_id_list':
                            return GetFaceIDListApi::init();

                    }

                }

        }

        return false;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        return self::set();

    }

}