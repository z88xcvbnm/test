<?php

namespace Project\Whore\Admin\Route;

use Core\Module\Url\Url;
use Project\Whore\Admin\Page\NotFound\NotFoundAdminWhorePage;
use Project\Whore\Admin\Route\Prepare\RoutePrepareLinkLen0AdminWhorePage;
use Project\Whore\Admin\Route\Prepare\RoutePrepareLinkLen1AdminWhorePage;
use Project\Whore\Admin\Route\Prepare\RoutePrepareLinkLen2AdminWhorePage;
use Project\Whore\Admin\Route\Prepare\RoutePrepareLinkLen3AdminWhorePage;
use Project\Whore\Admin\Route\Prepare\RoutePrepareLinkLen4AdminWhorePage;
use Project\Whore\Admin\Route\Prepare\RoutePrepareLinkLen5AdminWhorePage;
use Project\Whore\Admin\Route\Prepare\RoutePrepareLinkLen6AdminWhorePage;

class RouteAdminWhorePage{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        switch(count(Url::$list)){

            case 0:
                return RoutePrepareLinkLen0AdminWhorePage::init();

            case 1:
                return RoutePrepareLinkLen1AdminWhorePage::init();

            case 2:
                return RoutePrepareLinkLen2AdminWhorePage::init();

            case 3:
                return RoutePrepareLinkLen3AdminWhorePage::init();

            case 4:
                return RoutePrepareLinkLen4AdminWhorePage::init();

            case 5:
                return RoutePrepareLinkLen5AdminWhorePage::init();

            case 6:
                return RoutePrepareLinkLen6AdminWhorePage::init();

            default:
                return NotFoundAdminWhorePage::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}