<?php

namespace Project\Whore\Admin\Route\Redirect;

use Core\Module\Header\HeaderRedirect;
use Core\Module\User\User;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Action\Link\Admin\AdminLinkDefaultAction;
use Project\Whore\Admin\Page\NotFound\NotFoundAdminWhorePage;
use Project\Whore\Admin\Route\Redirect\Auth\RouteAuthRedirectAdminWhore;

class RouteRootDefaultRedirectAdminWhore{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function redirect(){

        if(User::is_login()){

            $user_login=UserLogin::get_user_login_default();

            if(empty($user_login))
                return RouteAuthRedirectAdminWhore::init();
            else{

                $link_default=AdminLinkDefaultAction::get_admin_link_default();

                if(empty($link_default))
                    return NotFoundAdminWhorePage::init();

                return HeaderRedirect::init(NULL,$link_default);

            }

        }

        return RouteAuthRedirectAdminWhore::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        return self::redirect();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}