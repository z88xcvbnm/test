<?php

namespace Project\Whore\Admin\Route\Redirect\Admin\Login\Settings;

use Core\Module\Header\HeaderRedirect;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Config\PageConfigAdminWhore;
use Project\Whore\Admin\Route\Redirect\RouteRootRedirectAdminWhore;

class RouteAdminLoginSettingsRedirectAdminWhore{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function redirect(){

        $user_login=UserLogin::get_user_login_default();

        if(empty($user_login))
            return RouteRootRedirectAdminWhore::init();

        return HeaderRedirect::init(NULL,'/admin/'.$user_login.'/'.PageConfigAdminWhore::$root_admin_settings_page_default);

    }

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::redirect();

    }

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::set();

    }

}