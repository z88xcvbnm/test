<?php

namespace Project\Whore\Admin\Content;

class ContentAdminWhoreEn{

    /** @var array */
    public  static $content_list=array(
        'project_name_title'                    =>'Shluham.NET',
        'root_term_page_title'                  =>'Terms',
        'root_about_page_title'                 =>'About',
        'registration_page_title'               =>'Registration',
        'invite_page_title'                     =>'Registration',
        'recovery_password_page_title'          =>'Recovery password',
        'admin_page_title'                      =>'Admin page',
        'root_page_title'                       =>'Root page',
        'auth_page_title'                       =>'Authorization',
        'reg_page_title'                        =>'Registration',
        'look_page_title'                       =>'Look',
        'stats_page_title'                      =>'Statistics',
        'company_page_title'                    =>'Face',
        'profile_page_title'                    =>'Profile',
        'users_page_title'                      =>'Administrators',
        'dashboard_page_title'                  =>'Dashboard',
        'settings_page_title'                   =>'Settings',
        'noname_page_title'                     =>'Noname',
        'loading'                               =>'Loading'
    );

    /** @var string */
    public  static $content_default     ='Undefined';

    /**
     * @param string|NULL $key
     * @return mixed|string
     */
    public  static function get_content(string $key=NULL){

        if(empty($key))
            return self::$content_default;

        if(empty(self::$content_list[$key]))
            return self::$content_default;

        return self::$content_list[$key];

    }

}