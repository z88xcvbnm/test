<?php

namespace Project\Whore\Admin\Content;

use Core\Module\Exception\PathException;
use Core\Module\Lang\Lang;

class ContentAdminWhore{

    /** @var null */
    private static $class_name;

    /** @var string */
    private static $lang_key_default    ='en';

    /** @var array */
    private static $lang_list           =array(
        'ru',
        'en'
    );

    /**
     * @return null|string
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_classname(){

        if(!empty(self::$class_name))
            return self::$class_name;

        Lang::$lang_key='ru';

        if(array_search(Lang::$lang_key,self::$lang_list)!==false){

            $file_path='Project/Whore/Admin/Content/ContentAdminWhore'.ucfirst(Lang::$lang_key).'.php';

            if(file_exists($file_path))
                $class_name='\Project\Whore\Admin\Content\ContentAdminWhore'.ucfirst(Lang::$lang_key);

        }

        if(empty($class_name)){

            $file_path      ='Project/Whore/Admin/Content/ContentAdminWhore'.ucfirst(self::$lang_key_default).'.php';
            $class_name     ='\Project\Whore\Admin\Content\ContentAdminWhore'.ucfirst(self::$lang_key_default);

            if(!file_exists($file_path)){

                $error=array(
                    'title'     =>'System problem',
                    'info'      =>'Error file is not exists'
                );

                throw new PathException($error);

            }

        }

        self::$class_name=$class_name;

        return $class_name;

    }

    /**
     * @param string|NULL $key
     * @return string
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_content(string $key=NULL){

        $class_name=self::get_classname();

        return $class_name::get_content($key);

    }

    /**
     * @return string
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_project_name(){

        return self::get_content('project_name_title');

    }

    /**
     * @param string|NULL $key
     * @return string
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_page_title(string $key=NULL){

        $project_title      =self::get_project_name();
        $page_title         =self::get_content($key);

        return $project_title.' | '.$page_title;

    }

    /**
     * @param string|NULL $title
     * @return string
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_page_custom_title(string $title=NULL){

        $project_title      =self::get_project_name();
        $page_title         =empty($title)?'':$title;

        return $project_title.' | '.$page_title;

    }

}