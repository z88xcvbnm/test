<?php

namespace Project\Whore\Admin\Content;

class ContentAdminWhoreRu{

    /** @var array */
    public  static $content_list=array(
        'project_name_title'                    =>'Shluham.NET',
        'root_term_page_title'                  =>'Термины и определения в эскортном бизнесе',
        'root_city_page_title'                  =>'Статистика городов по количеству шлюх',
        'root_about_page_title'                 =>'О проекте',
        'root_source_page_title'                =>'Источники анкет эскортниц, проституток и содержанок',
        'admin_page_title'                      =>'Админка',
        'registration_page_title'               =>'Регистрация',
        'invite_page_title'                     =>'Регистрация',
        'recovery_password_page_title'          =>'Восстановление пароля',
        'root_page_title'                       =>'Проверка репутации девушки по фотографии',
        'auth_page_title'                       =>'Авторизация',
        'reg_page_title'                        =>'Регистрация',
        'face_page_title'                       =>'Лица',
        'profile_page_title'                    =>'Проверка репутации девушки по фотографии',
        'profile_list_page_title'               =>'Пользователи',
        'profile_request_page_title'            =>'Пользовательские запросы',
        'financial_page_title'                  =>'Финансы',
        'financial_settings_page_title'         =>'Настройки',
        'financial_stats_title'                 =>'Статистика',
        'financial_transaction_title'           =>'Статистика',
        'source_page_title'                     =>'Источники анкет эскортниц, проституток и содержанок',
        'source_list_page_title'                =>'Список реусурсов',
        'source_parsing_page_title'             =>'Парсинг реусурсов',
        'users_page_title'                      =>'Администраторы',
        'dashboard_page_title'                  =>'Dashboard',
        'settings_page_title'                   =>'Настройки',
        'feedback_page_title'                   =>'Обратная связь',
        'noname_page_title'                     =>'Без названия',
        'loading'                               =>'Загрузка',
        'profile_face_page_title'               =>'Поиск лиц',
        'profile_city_page_title'               =>'Статистика городов по количеству шлюх',
        'profile_billing_page_title'            =>'Биллинг',
        'profile_payment_page_title'            =>'Пополнение баланса',
        'profile_search_history_page_title'     =>'История посика лиц',
        'confirm_email_page_title'              =>'Подтверждение email',
        'root_description'                      =>'Shluham.net (шлюхам нет) или shkuram.net (шкурам нет) - сайт для online проверки репутации девушки по фотографии ее лица с помощью биометрического алгоритма',
        'city_description'                      =>'Shluham.net (шлюхам нет) или shkuram.net (шкурам нет) - сайт для online проверки репутации девушки по фотографии ее лица с помощью биометрического алгоритма'
    );

    /** @var string */
    public  static $content_default='Undefined';

    /**
     * @param string|NULL $key
     * @return mixed|string
     */
    public  static function get_content(string $key=NULL){

        if(empty($key))
            return self::$content_default;

        if(empty(self::$content_list[$key]))
            return self::$content_default;

        return self::$content_list[$key];

    }

}