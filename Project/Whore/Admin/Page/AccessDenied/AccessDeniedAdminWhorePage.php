<?php

namespace Project\Whore\Admin\Page\NotFound;

use Project\Whore\Admin\Template\Dom\System\NotFound\NotFoundAdminWhoreDom;

class AccessDeniedAdminWhorePage{

    /**
     * @return bool
     */
    private static function init_page(){

        return NotFoundAdminWhoreDom::init();

    }

    /**
     * @return bool
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}