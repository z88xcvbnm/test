<?php

namespace Project\Whore\Admin\Page\PhpInfo;

use Project\Whore\Admin\Template\Dom\PhpInfo\PhpInfoAdminWhoreDom;

class PhpInfoAdminWhorePage{

    /**
     * @return bool
     */
    private static function init_page(){

        return PhpInfoAdminWhoreDom::init();

    }

    /**
     * @return bool
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}