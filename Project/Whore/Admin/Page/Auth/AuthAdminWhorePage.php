<?php

namespace Project\Whore\Admin\Page\Auth;

use Project\Whore\Admin\Route\Page\Auth\RouteAuthAdminWhorePage;

class AuthAdminWhorePage{

    /**
     * @return bool
     */
    private static function init_page(){

        return RouteAuthAdminWhorePage::init();

    }

    /**
     * @return bool
     */
    private static function set(){

        return self::init_page();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}