<?php

namespace Project\Whore\Admin\Config;

use Project\Whore\Admin\Content\ContentAdminWhore;

class PageConfigProfileWhore{

    /** @var string */
    public static $root_page_default                            ='profile';

    /** @var string */
    public  static $root_profile_face_page_default              ='face';

    /** @var string */
    public  static $root_profile_settings_page_default          ='settings';

    /**
     * @return mixed
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_page_title_default(){

        return ContentAdminWhore::get_project_name();

    }

}