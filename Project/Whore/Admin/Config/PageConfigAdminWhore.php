<?php

namespace Project\Whore\Admin\Config;

use Project\Whore\Admin\Content\ContentAdminWhore;

class PageConfigAdminWhore{

    /** @var string */
    public static $root_page_default                                        ='admin';

    /** @var string */
    public  static $root_admin_page_default                                 ='dashboard';

    /** @var string */
    public  static $root_admin_dashboard_page_default                       ='dashboard';

    /** @var string */
    public  static $root_admin_face_page_default                            ='face';

    /** @var string */
    public  static $root_admin_profile_page_default                         ='profile';

    /** @var string */
    public  static $root_admin_profile_request_page_default                 ='request';

    /** @var string */
    public  static $root_admin_profile_list_page_default                    ='list';

    /** @var string */
    public  static $root_admin_financial_page_default                       ='financial';

    /** @var string */
    public  static $root_admin_financial_transaction_page_default           ='transaction';

    /** @var string */
    public  static $root_admin_financial_stats_page_default                 ='stats';

    /** @var string */
    public  static $root_admin_financial_settings_page_default              ='settings';

    /** @var string */
    public  static $root_admin_source_page_default                          ='source';

    /** @var string */
    public  static $root_admin_source_list_page_default                     ='list';

    /** @var string */
    public  static $root_admin_source_parsing_page_default                  ='parsing';

    /** @var string */
    public  static $root_admin_users_page_default                           ='users';

    /** @var string */
    public  static $root_admin_settings_page_default                        ='settings';

    /** @var string */
    public  static $root_admin_invite_page_default                          ='invite';

    /** @var string */
    public  static $root_unreg_page_default                                 ='auth';

    /** @var string */
    public  static $page_title_key_default                                  ='users_page_title';

    /** @var string */
    public  static $page_content_title_key_default                          ='sight_page_title';

    /**
     * @return mixed
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_page_title_default(){

        return ContentAdminWhore::get_project_name();

    }

    /**
     * @return string
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_page_content_title_default(){

        return ContentAdminWhore::get_page_title(self::$page_content_title_key_default);

    }

}