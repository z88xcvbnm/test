<?php

namespace Project\Whore\Admin\Config;

use Core\Module\Exception\ParametersException;

class JsConfigProfileWhore{

    /**
     * @var array
     */
    private static $list=[
        'auth'=>[
            '/show/js/auth/init.js'
        ],
        'profile'=>[
            '/show/js/profile/init.js'
        ],
        'maps'=>'https=>//api-maps.yandex.ru/2.1/?lang=ru_RU'
    ];

    /**
     * @param string|NULL $group_key
     * @param string|NULL $script_name
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_script_path(string $group_key=NULL,string $script_name=NULL){

        if(empty($group_key)&&empty($script_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        if(!empty($group_key)&&empty($script_name)){

            if(isset(self::$list[$group_key]))
                return self::$list[$group_key];

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Group key is not exists'
            );

            throw new ParametersException($error);

        }
        else if(empty($group_key)&&!empty($script_name)){

            if(isset(self::$list[$script_name]))
                return self::$list[$script_name];

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Script name is not exists'
            );

            throw new ParametersException($error);

        }
        else{

            $error_info_list=[];

            if(!isset(self::$list[$group_key]))
                $error_info_list[]='Group key is not exists';

            if(!isset(self::$list[$group_key][$script_name]))
                $error_info_list[]='';

            if(count($error_info_list)>0){

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>$error_info_list
                );

                throw new ParametersException($error);

            }

            return self::$list[$group_key][$script_name];

        }

    }

}