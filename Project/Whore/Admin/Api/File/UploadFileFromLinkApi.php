<?php

namespace Project\Whore\Admin\Api\File;

use Core\Module\Date\Date;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\File\File;
use Core\Module\File\FileConfig;
use Core\Module\File\FileParametersCash;
use Core\Module\Image\Image;
use Core\Module\Image\ImageUploadedPrepare;
use Core\Module\OsServer\OsServer;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;

class UploadFileFromLinkApi{

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $image_ID;

    /** @var string */
    private static $file_content_type;

    /** @var string */
    private static $file_extension;

    /** @var int */
    private static $image_link;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$file_ID          =NULL;
        self::$image_ID         =NULL;
        self::$image_link       =NULL;

        return true;

    }

    /**
     * @param string|NULL $file_extension
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_file(string $file_extension=NULL){

        return File::add_file_without_params($file_extension,true,false,false,self::$image_link);

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_hash
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_file(int $file_ID=NULL,string $file_hash=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_content_type=NULL,string $file_extension=NULL){

        return File::update_file($file_ID,$file_hash,1,1,$file_size,$file_size,$file_mime_type,$file_content_type,$file_extension,true,false,true);

    }

    /**
     * @param string|NULL $file_path
     * @return array
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_prepare_file(string $file_path=NULL){

        if(empty($file_path))
            return NULL;

        $image=@file_get_contents($file_path);

        if($image===false)
            return NULL;

        $file_name          =basename($file_path);
        $file_extension     =File::get_file_extension_from_file_name($file_name);

        $file_ID            =self::add_file($file_extension);

        if(empty($file_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new PhpException($error);

        }

        $file_dest_path=File::get_file_path_from_file_ID($file_ID,true);

        File::get_file_dir($file_ID,NULL,Date::get_date_time_full());
        FileParametersCash::add_file_parameter_in_cash($file_ID,'file_extension',$file_extension);

        if(file_put_contents($file_dest_path,$image)===false){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not save'
            ];

            throw new PhpException($error);

        }

        $file_extension     =File::get_file_extension_from_file_path($file_dest_path);
        $file_mime_type     =File::get_file_mime_type_from_path($file_dest_path);

        if(empty($file_extension)&&!empty($file_mime_type)){

            $file_extension=isset(FileConfig::$file_mime_type_list[$file_mime_type])?FileConfig::$file_mime_type_list[$file_mime_type][0]:NULL;

            if(empty($file_extension)){

                $error=[
                    'title'     =>PhpException::$title,
                    'info'      =>'File extension is empty'
                ];

                throw new PhpException($error);

            }

            File::update_file_extension($file_ID,$file_extension);

        }

        if(filesize($file_dest_path)<=16000){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'File size is very little'
            ];

            throw new ParametersValidationException($error);

        }

        if(!OsServer::$is_windows){

            $temp_image_path='Temp/ModelZoneParsing/img_'.time();

            try{

                $img=new \Imagick($file_dest_path);
                $img->readImage($file_dest_path);
                $img->writeImage($temp_image_path);

            }
            catch (\ImagickException $e){

                $error=[
                    'title'     =>ParametersValidationException::$title,
                    'info'      =>'File is not valid'
                ];

                throw new ParametersValidationException($error);

            }

            unset($img);

            unlink($file_dest_path);

            copy($temp_image_path,$file_dest_path);

            unlink($temp_image_path);

        }

        $image_object=Image::get_image_object_from_file_path($file_dest_path);

        if(empty($image_object)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'File object is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        $size       =getimagesize($file_dest_path,$im_info);
        $sum        =$size[0]*$size[1];

        if($sum!=0){

            $file_size  =filesize($file_dest_path);
            $cof        =$file_size/$sum;

        }
        else
            $cof=0;

        if($cof<.01){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image size is not correct'
            ];

            throw new ParametersValidationException($error);

        }

        switch(exif_imagetype($file_dest_path)){

            case 1:
            case 2:
            case 3:
            case 7:
            case 8:
                break;

            default:
                return NULL;

        }

        $file_mime_type         =File::get_file_mime_type_from_path($file_dest_path);
        $file_content_type      =File::get_file_content_type_from_path($file_dest_path);
        $file_size              =filesize($file_dest_path);
        $data_list              =[
            User::$user_ID,
            $file_size,
            $file_mime_type,
            $file_content_type,
            time(),
            rand(0,time())
        ];
        $file_hash              =Hash::get_sha1_encode(implode(':',$data_list));

        if($file_size==0){

            File::remove_file_ID($file_ID);

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'File size is not correct'
            ];

            throw new ParametersValidationException($error);

        }

        if(!self::update_file($file_ID,$file_hash,$file_size,$file_mime_type,$file_content_type,$file_extension)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not update'
            ];

            throw new PhpException($error);

        }

        return[
            'file_ID'               =>$file_ID,
            'file_content_type'     =>$file_content_type,
            'file_extension'        =>$file_extension
        ];

    }

    /**
     * @param int|NULL $file_ID
     * @param string $file_content_type
     * @param string|NULL $file_extension
     * @param string|NULL $image_link
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_image(int $file_ID=NULL,string $file_content_type='image',string $file_extension=NULL,string $image_link=NULL){

        return ImageUploadedPrepare::init($file_ID,$file_content_type,$file_extension,$image_link);

    }

    /**
     * @param array|NULL $file_data
     * @return mixed
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_prepare_image(array $file_data=NULL){

        if(empty($file_data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File data is empty'
            ];

            throw new PhpException($error);

        }

        $file_ID                            =$file_data['file_ID'];
        $file_extension                     =$file_data['file_extension'];

        $image_data                         =self::prepare_image($file_ID,'image',$file_extension,self::$image_link);

        $image_path                         =$image_data['image_dir'].'/'.$image_data['image_item_ID_list']['large'];
        $image_data['file_path']            =$image_path;
        $image_data['file_size']            =filesize($image_path);
        $image_data['file_content_type']    ='image';
        $image_data['file_extension']       =$file_extension;
        $image_data['file_mime_type']       =File::get_file_mime_type_from_path($image_path);
        $image_data['image_resolution']     =Image::get_image_pixel_size_from_file_path($image_path);

        return $image_data;

    }

    /**
     * @param string|NULL $image_link
     * @return string
     */
    private static function get_prepare_image_link(string $image_link=NULL){

        return str_replace(' ','%20',$image_link);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set_file(){

        $file_data=self::get_prepare_file(self::$image_link);

        if(empty($file_data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not prepare'
            ];

            throw new PhpException($error);

        }

        self::$file_ID                  =$file_data['file_ID'];
        self::$file_content_type        =$file_data['file_content_type'];
        self::$file_extension           =$file_data['file_extension'];

        $image_data=self::get_prepare_image($file_data);

        if(empty($image_data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Image was not prepare'
            ];

            throw new PhpException($error);

        }

        self::$image_ID=$image_data['image_ID'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'file_ID'           =>self::$file_ID,
                'image_ID'          =>self::$image_ID,
                'link'              =>self::$image_link
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        self::set_file();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        if(empty($_POST['image_link'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Link is empty'
            ];

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$image_link=self::get_prepare_image_link($_POST['image_link']);

        return self::set();

    }

}