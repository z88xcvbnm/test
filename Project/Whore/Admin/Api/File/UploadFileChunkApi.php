<?php

namespace Project\Whore\Admin\Api\File;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\File\FileChunkUploadAction;

class UploadFileChunkApi{

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $file_chunk_index;

    /** @var int */
    private static $file_chunk_count;

    /** @var int */
    private static $file_chunk_size;

    /** @var int */
    private static $file_size;

    /** @var string */
    private static $file_mime_type;

    /** @var string */
    private static $file_content_type;

    /** @var string */
    private static $file_extension;

    /** @var string */
    private static $file_chunk;

    /** @var array */
    private static $data;

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_file_chunk(){

        self::$data=FileChunkUploadAction::init(self::$file_ID,self::$file_chunk_index,self::$file_chunk_count,self::$file_chunk_size,self::$file_size,self::$file_mime_type,self::$file_content_type,self::$file_extension,self::$file_chunk);

        if(empty(self::$data)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'File chunk was not uploaded'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        return ResponseSuccess::init(self::$data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        if(self::prepare_file_chunk())
            return self::set_return();

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        if(
              !isset($_POST['file_chunk_index'])
            ||!isset($_POST['file_chunk_count'])
            ||!isset($_POST['file_size'])
            ||!isset($_POST['file_chunk'])
            ||!isset($_POST['file_extension'])
        ){

            $error_info_list=[];

            if(!isset($_POST['file_chunk_index']))
                $error_info_list['file_chunk_index']='File chunk index is not exists';

            if(!isset($_POST['file_chunk_count']))
                $error_info_list['file_chunk_count']='File chunk count is not exists';

            if(!isset($_POST['file_chunk_size']))
                $error_info_list['file_chunk_size']='File chunk size is not exists';

            if(!isset($_POST['file_size']))
                $error_info_list['file_size']='File size is not exists';

            if(!isset($_POST['file_mime_type']))
                $error_info_list['file_mime_type']='File mime type is not exists';

            if(!isset($_POST['file_content_type']))
                $error_info_list['file_content_type']='File content type is not exists';

            if(!isset($_POST['file_extension']))
                $error_info_list['file_extension']='File extension is not exists';

            if(!isset($_POST['file_chunk']))
                $error_info_list['file_chunk']='File chunk is not exists';

            if(count($error_info_list)>0){

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>$error_info_list
                );

                throw new ParametersException($error);

            }

            return false;

        }

        self::$file_ID              =empty($_POST['file_ID'])?NULL:$_POST['file_ID'];
        self::$file_chunk_index     =$_POST['file_chunk_index'];
        self::$file_chunk_count     =$_POST['file_chunk_count'];
        self::$file_size            =$_POST['file_size'];
        self::$file_chunk_size      =$_POST['file_chunk_size'];
        self::$file_mime_type       =$_POST['file_mime_type'];
        self::$file_content_type    =$_POST['file_content_type'];
        self::$file_extension       =mb_strtolower($_POST['file_extension'],'utf-8');
        self::$file_chunk           =$_POST['file_chunk'];

        return self::set();

    }

}