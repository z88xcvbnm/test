<?php

namespace Project\Whore\Admin\Api\Logout\UserLogout;

use Core\Action\Logout\UserLogout\UserLogoutSystemAction;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Response\ResponseUnauthorized;

class UserLogoutApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(UserLogoutSystemAction::init())
            ResponseSuccess::init([]);
        else{

            $data=array(
                'title'     =>'Parameters problem',
                'info'      =>'User already logouted'
            );

            ResponseUnauthorized::init($data);

            return false;

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}