<?php

namespace Project\Whore\Admin\Api\Profile;

use Core\Module\Email\EmailSend;
use Core\Module\Email\EmailValidation;
use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Url\Url;

class SendEmailToProfileApi{

    /** @var string */
    private static $email;

    /** @var string */
    private static $content;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_feedback(){

        $inner      =[];

        $inner[]    =nl2br(self::$content);
        $inner[]    ='';
        $inner[]    ='С уважением,';
        $inner[]    ='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

        return EmailSend::init(array(self::$email),'Обратная связь '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::send_feedback();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['email']))
            $error_info_list['email']='Email is empty';
        else if(!EmailValidation::is_valid_email($_POST['email']))
            $error_info_list['email']='Email is not valid';

        if(empty($_POST['content']))
            $error_info_list['content']='Content is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$email                =mb_strtolower($_POST['email'],'utf-8');
        self::$content              =$_POST['content'];

        return self::set();

    }

}