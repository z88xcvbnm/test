<?php

namespace Project\Whore\Admin\Api\Token;

use Core\Action\Token\GetTokenSystemAction;
use Core\Module\Response\ResponseAccessDenied;
use Core\Module\Response\ResponseSuccess;

class GetTokenApi{

    /**
     * Prepare get token
     */
    private static function set(){

        $token=GetTokenSystemAction::init();

        if(empty($token))
            ResponseAccessDenied::init('Token is empty');

        $data=array(
            'token'=>$token
        );

        ResponseSuccess::init($data);

        return true;

    }

    /**
     * Init get token
     */
    public  static function init(){

        return self::set();

    }

}