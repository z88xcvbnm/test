<?php

namespace Project\Whore\Admin\Api\Feedback;

use Core\Action\Feedback\FeedbackSystemAction;
use Core\Module\Email\EmailValidation;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\OsServer\OsServer;
use Core\Module\ReCaptcha\ReCaptcha;
use Core\Module\Response\ResponseSuccess;

class FeedbackApi{

    /** @var string */
    private static $email;

    /** @var string */
    private static $content;

    /** @var string */
    private static $re_captcha_token;

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_re_captcha(){

        if(\Config::$is_need_recaptcha&&!OsServer::$is_windows)
            return ReCaptcha::is_valid_re_captcha_token(self::$re_captcha_token,'feedback');

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_feedback(){

        if(!FeedbackSystemAction::init(self::$email,self::$content)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'FeedbackApi was not send'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::check_re_captcha()){

            self::send_feedback();

            return self::set_return();

        }

        return false;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['email']))
            $error_info_list['email']='Email is empty';
        else if(!EmailValidation::is_valid_email($_POST['email']))
            $error_info_list['email']='Email is not valid';

        if(empty($_POST['content']))
            $error_info_list['content']='Content is empty';

        if(\Config::$is_need_recaptcha&&!OsServer::$is_windows)
            if(empty($_POST['re_captcha_token']))
                $error_info_list[]='ReCaptcha token is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$email                =mb_strtolower($_POST['email'],'utf-8');
        self::$content              =$_POST['content'];
        self::$re_captcha_token     =empty($_POST['re_captcha_token'])?NULL:$_POST['re_captcha_token'];

        return self::set();

    }

}