<?php

namespace Project\Whore\Admin\Api\User;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserBalance;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Core\Module\User\UserEmailCheck;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Action\User\Admin\LogoutUserAdminAction;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;

class UpdateProfileApi{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_login_ID;

    /** @var int */
    private static $user_email_ID;

    /** @var int */
    private static $user_data_ID;

    /** @var string */
    private static $user_login;

    /** @var string */
    private static $user_password;

    /** @var string */
    private static $user_name;

    /** @var string */
    private static $user_surname;

    /** @var string */
    private static $user_email;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_access(){

        if(
              !UserAccess::$is_root
            &&!UserAccess::$is_admin
        ){

            LogoutUserAdminAction::init();

            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user(){

        if(!User::isset_user_ID(self::$user_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'User ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_login(){

        if(!UserLogin::remove_user_login_from_user_ID(self::$user_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User login is empty'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_email(){

        if(!UserEmail::remove_user_email(self::$user_ID,self::$user_email)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User email is empty'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_data(){

        if(!UserData::remove_user_data(self::$user_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User data is empty'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_data(){

        self::$user_data_ID=UserData::add_user_data(self::$user_ID,self::$user_name,self::$user_surname,NULL);

        if(empty(self::$user_data_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User data was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_email(){

        self::$user_email_ID=UserEmail::add_user_email(self::$user_ID,self::$user_email);

        if(empty(self::$user_email_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User email was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_login(){

        self::$user_login_ID=UserLogin::add_user_login(self::$user_ID,self::$user_login,self::$user_password);

        if(empty(self::$user_login_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User login was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_user_login(){

        $data=UserLogin::get_user_login_data(self::$user_ID);

        if(empty($data))
            return self::add_user_login();

        if($data['login']==self::$user_login)
            return true;

        self::$user_password=$data['password'];

        self::remove_user_login();

        return self::add_user_login();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_user_email(){

        $user_email=UserEmail::get_user_email_last(self::$user_ID);

        if(empty($user_email))
            return self::add_user_email();

        if($user_email==self::$user_email)
            return true;

        self::remove_user_email();

        return self::add_user_email();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_user_data(){

        $data=UserData::get_user_data(self::$user_ID);

        if(empty($data))
            return self::add_user_data();

        if(
            $data['name']==self::$user_name
            &&$data['surname']==self::$user_surname
        )
            return true;

        self::remove_user_data();

        return self::add_user_data();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'   =>true,
            'user'      =>[
                'ID'                        =>self::$user_ID,
                'login'                     =>self::$user_login,
                'image'                     =>[],
                'email'                     =>self::$user_email,
                'name'                      =>self::$user_name,
                'surname'                   =>self::$user_surname,
                'balance'                   =>UserBalance::get_user_balance(self::$user_ID),
                'is_email_confirmed'        =>UserEmailCheck::is_user_email_confirmed(self::$user_ID),
                'date_online'               =>User::get_date_online(self::$user_ID)
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_user())
            if(self::is_valid_access()){

                self::prepare_user_login();
                self::prepare_user_email();
                self::prepare_user_data();

                return self::set_return();

            }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['user_ID']))
            $error_info_list[]='User ID is empty';

        if(empty($_POST['user_login']))
            $error_info_list[]='User login is empty';

        if(empty($_POST['user_email']))
            $error_info_list[]='User email is empty';

        if(empty($_POST['user_name']))
            $error_info_list[]='User name is empty';

        if(empty($_POST['user_surname']))
            $error_info_list[]='User surname is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$user_ID                  =(int)$_POST['user_ID'];
        self::$user_login               =$_POST['user_login'];
        self::$user_email               =$_POST['user_email'];
        self::$user_name                =$_POST['user_name'];
        self::$user_surname             =$_POST['user_surname'];

        return self::set();

    }

}