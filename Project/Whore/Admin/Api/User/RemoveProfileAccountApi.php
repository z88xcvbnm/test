<?php

namespace Project\Whore\Admin\Api\User;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserEmail;
use Core\Module\User\UserRemove;
use Project\Whore\Admin\Action\User\Admin\LogoutUserAdminAction;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;

class RemoveProfileAccountApi{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_remove_ID;

    /**
     * @return bool
     */
    private static function set_user_ID(){

        self::$user_ID=User::$user_ID;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_access(){

        if(
              !UserAccess::$is_profile_wallet
            &&!UserAccess::$is_profile
        ){

            LogoutUserAdminAction::init();

            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_access_level_for_remove(){

        if(UserAccess::$is_root)
            return true;

        $user_access_type_name_list=UserAccess::get_user_access_type_name_list(self::$user_ID);

        if(array_search('root',$user_access_type_name_list)!==false){

            LogoutUserAdminAction::init();

            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user(){

        if(!User::isset_user_ID(self::$user_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'User ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_remove(){

        self::$user_remove_ID=UserRemove::add_user_remove(User::$user_ID,self::$user_ID,NULL,NULL);

        if(empty(self::$user_remove_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User remove was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user(){

        UserEmail::remove_user_email_to_unshow(self::$user_ID);

        if(!User::remove_user_ID(self::$user_ID,true)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_user_ID();

        if(self::is_valid_access())
            if(self::isset_user())
                if(self::isset_user_access_level_for_remove()){

                    self::remove_user();
                    self::add_user_remove();

                    return self::set_return();

                }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}