<?php

namespace Project\Whore\Admin\Api\User\Data;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\User\Data\UserSavePasswordAction;

class SaveUserPasswordApi{

    /** @var string */
    private static $password_old;

    /** @var string */
    private static $password_new;

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function update(){

        if(!UserSavePasswordAction::init(self::$password_old,self::$password_new)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Password was not updated'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_return(){

        ResponseSuccess::init([]);

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     */
    public  static function set(){

        if(self::update())
            return self::set_return();

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['pas_old']))
            $error_info_list['password_old']='Old password is empty';

        if(empty($_POST['pas']))
            $error_info_list['password_new']='New password is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$password_new     =$_POST['pas'];
        self::$password_old     =$_POST['pas_old'];

        return self::set();

    }

}