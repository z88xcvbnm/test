<?php

namespace Project\Whore\Admin\Api\User\Data;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\User\Data\UserSaveLoginAction;

class SaveUserLoginApi{

    /** @var string */
    private static $login;

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update(){

        if(!UserSaveLoginAction::init(self::$login)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Login was not updated'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set(){

        if(self::update())
            return self::set_return();

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(empty($_POST['login'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Login is empty'
            );

            throw new ParametersException($error);

        }

        self::$login=mb_strtolower($_POST['login'],'utf-8');

        return self::set();

    }

}