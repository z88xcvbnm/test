<?php

namespace Project\Whore\Admin\Api\User\Data;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\User\Data\UserSaveEmailAction;

class SaveUserEmailApi{

    /** @var string */
    private static $email;

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function update(){

        if(!UserSaveEmailAction::init(self::$email)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User email was not updated'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_return(){

        ResponseSuccess::init([]);

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     */
    public  static function set(){

        if(self::update())
            return self::set_return();

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    public  static function init(){

        if(empty($_POST['email'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User email is empty'
            );

            throw new ParametersException($error);

        }

        self::$email=$_POST['email'];

        return self::set();

    }

}