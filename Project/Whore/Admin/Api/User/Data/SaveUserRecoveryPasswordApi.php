<?php

namespace Project\Whore\Admin\Api\User\Data;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\OsServer\OsServer;
use Core\Module\ReCaptcha\ReCaptcha;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\User\Data\UserSaveRecoveryPasswordAction;

class SaveUserRecoveryPasswordApi{

    /** @var string */
    private static $login;

    /** @var string */
    private static $hash;

    /** @var string */
    private static $password;

    /** @var string */
    private static $re_captcha_token;

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare(){

        if(\Config::$is_need_recaptcha&&!OsServer::$is_windows)
            ReCaptcha::is_valid_re_captcha_token(self::$re_captcha_token,'reset_password');

        $data=UserSaveRecoveryPasswordAction::init(self::$hash,self::$password);

        if(empty($data)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Password was not updated'
            );

            throw new PhpException($error);

        }

        self::$login=$data['login'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=array(
            'login'=>self::$login
        );

        ResponseSuccess::init($data);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::prepare())
            return self::set_return();

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['hash']))
            $error_info_list['hash']='Hash is empty';

        if(empty($_POST['pas']))
            $error_info_list['password']='Password is empty';

        if(\Config::$is_need_recaptcha&&!OsServer::$is_windows)
            if(empty($_POST['re_captcha_token']))
                $error_info_list[]='ReCaptcha token is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$hash                 =$_POST['hash'];
        self::$password             =$_POST['pas'];
        self::$re_captcha_token     =empty($_POST['re_captcha_token'])?NULL:$_POST['re_captcha_token'];

        return self::set();

    }

}