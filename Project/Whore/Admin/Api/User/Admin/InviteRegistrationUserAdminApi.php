<?php

namespace Project\Whore\Admin\Api\User\Admin;

use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseAccessDenied;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Token\Token;
use Core\Module\User\UserAccess;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Action\User\Admin\UserAdminInviteRegistrationAction;
use Project\Whore\Admin\Config\PageConfigAdminWhore;
use Project\Whore\Admin\Config\PageConfigProfileWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;
use Project\Whore\All\Action\Auth\User\UserAuthProjectAction;

class InviteRegistrationUserAdminApi{

    /** @var string */
    private static $hash;

    /** @var string */
    private static $login;

    /** @var string */
    private static $email;

    /** @var string */
    private static $name;

    /** @var string */
    private static $surname;

    /** @var string */
    private static $password;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare(){

        if(!UserAdminInviteRegistrationAction::init(self::$hash,self::$login,self::$email,self::$name,self::$surname,self::$password)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User data was not updated'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function redirect_admin(){

        $user_login=UserLogin::get_user_login_default();

        $data=array(
            'token'         =>Token::$token_hash,
            'type'          =>'admin',
            'title'         =>ContentAdminWhore::get_page_title('admin_page_title'),
            'redirect'      =>'/admin/'.$user_login.'/'.PageConfigAdminWhore::$root_admin_page_default
        );

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function redirect_profile(){

        $user_login=UserLogin::get_user_login_default();

        $data=array(
            'token'         =>Token::$token_hash,
            'type'          =>'profile',
            'title'         =>ContentAdminWhore::get_page_title('profile_face_page_title'),
            'redirect'      =>'/'.PageConfigProfileWhore::$root_page_default.'/'.$user_login.'/'.PageConfigProfileWhore::$root_profile_face_page_default
        );

        return ResponseSuccess::init($data);

    }

    /**
     * @return string|null
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_login(){

        return UserLogin::get_user_login_default();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     */
    private static function set_return(){

        $r=UserAuthProjectAction::init(self::$login,self::$password);

        if(!empty($r))
            if($r['success'])
                if(
                    UserAccess::$is_root
                    ||UserAccess::$is_admin
                ){

                    self::set_user_login();

                    return self::redirect_admin();

                }
                else if(
                    UserAccess::$is_profile
                    ||UserAccess::$is_profile_wallet
                ){

                    self::set_user_login();

                    return self::redirect_profile();

                }

        return ResponseAccessDenied::init();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     */
    private static function set(){

        if(self::prepare())
            return self::set_return();

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['hash']))
            $error_info_list['hash']='Hash is empty';

        if(empty($_POST['login']))
            $error_info_list['login']='Login is empty';

        if(empty($_POST['email']))
            $error_info_list['email']='Email is empty';

        if(empty($_POST['name']))
            $error_info_list['name']='Name is empty';

        if(empty($_POST['surname']))
            $error_info_list['surname']='Surname is empty';

        if(empty($_POST['password']))
            $error_info_list['password']='Password is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$hash         =$_POST['hash'];
        self::$login        =mb_strtolower($_POST['login'],'utf-8');
        self::$email        =mb_strtolower($_POST['email'],'utf-8');
        self::$name         =$_POST['name'];
        self::$surname      =$_POST['surname'];
        self::$password     =$_POST['password'];

        return self::set();

    }

}