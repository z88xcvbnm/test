<?php

namespace Project\Whore\Admin\Api\User\Admin;

use Core\Module\Exception\ParametersException;
use Core\Module\OsServer\OsServer;
use Core\Module\ReCaptcha\ReCaptcha;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\User\Admin\UserAdminRecoveryPasswordAction;

class RecoveryPasswordUserAdminApi{

    /** @var string */
    private static $email;

    /** @var string */
    private static $link;

    /** @var string */
    private static $re_captcha_token;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare(){

        if(\Config::$is_need_recaptcha&&!OsServer::$is_windows)
            ReCaptcha::is_valid_re_captcha_token(self::$re_captcha_token,'recovery_password');

        $r=UserAdminRecoveryPasswordAction::init(self::$email);

        if(empty($r)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>array(
                    'recovery_password'=>'Recovery password had problem for your profile'
                )
            );

            throw new ParametersException($error);

        }

        if(isset($r['link']))
            self::$link=$r['link'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        if(OsServer::$is_windows)
            $data['link']=self::$link;

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::prepare())
            return self::set_return();

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['email']))
            $error_info_list[]='Email is empty';

        if(\Config::$is_need_recaptcha&&!OsServer::$is_windows)
            if(empty($_POST['re_captcha_token']))
                $error_info_list[]='ReCaptcha token is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$email                =mb_strtolower($_POST['email'],'utf-8');
        self::$re_captcha_token      =empty($_POST['re_captcha_token'])?NULL:$_POST['re_captcha_token'];

        return self::set();

    }

}