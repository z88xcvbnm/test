<?php

namespace Project\Whore\Admin\Api\User\Admin;

use Core\Module\Error\ErrorCashContent;
use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\User\Admin\AddUserAdminAction;

class AddUserAdminApi{

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(!isset($_POST['email'])||!isset($_POST['login'])||!isset($_POST['surname'])||!isset($_POST['name'])){

            $error_info_list=[];

            if(empty($_POST['email']))
                $error_info_list['email']='Email is empty';

            if(empty($_POST['login']))
                $error_info_list['login']='Login is empty';

            if(empty($_POST['surname']))
                $error_info_list['surname']='Surname is empty';

            if(empty($_POST['name']))
                $error_info_list['name']='Name is empty';

            if(count($error_info_list)>0){

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>$error_info_list
                );

                throw new ParametersException($error);

            }

        }

        $is_admin       =isset($_POST['admin'])?(bool)$_POST['admin']:false;
        $is_bookkeep    =isset($_POST['bookkeep'])?(bool)$_POST['bookkeep']:false;
        $is_museum      =isset($_POST['museum'])?(bool)$_POST['museum']:false;
        $is_manager     =isset($_POST['manager'])?(bool)$_POST['manager']:false;
        $firm_ID        =isset($_POST['firm_ID'])?$_POST['firm_ID']:NULL;
        $place_ID_list  =isset($_POST['place_ID_list'])?$_POST['place_ID_list']:[];

        $user_data=AddUserAdminAction::init(mb_strtolower($_POST['email'],'utf-8'),mb_strtolower($_POST['login'],'utf-8'),$_POST['surname'],$_POST['name'],$is_admin,$is_manager,$is_bookkeep,$is_museum,$firm_ID,$place_ID_list);

        if($user_data===false){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>ErrorCashContent::get_error_list()
            );

            throw new ParametersException($error);

        }

        ResponseSuccess::init($user_data);

        return true;

    }

}