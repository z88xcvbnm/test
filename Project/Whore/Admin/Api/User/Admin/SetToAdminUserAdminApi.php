<?php

namespace Project\Whore\Admin\Api\User\Admin;

use Core\Module\Error\ErrorCashContent;
use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\User\Admin\SetToAdminUserAdminAction;

class SetToAdminUserAdminApi{

    /**
     * @return bool
     * @throws ParametersException
     */
    public  static function init(){

        if(!isset($_POST['user_ID'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $user_access_data=SetToAdminUserAdminAction::init();

        if(empty($user_access_data)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>ErrorCashContent::get_error_list()
            );

            throw new ParametersException($error);

        }

        ResponseSuccess::init($user_access_data);

        return true;

    }

}