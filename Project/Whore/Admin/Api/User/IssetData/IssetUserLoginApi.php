<?php

namespace Project\Whore\Admin\Api\User\IssetData;

use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\All\Action\User\IssetData\IssetUserLoginAction;

class IssetUserLoginApi{

    /** @var string */
    private static $login;

    /** @var string */
    private static $hash;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        $data=array(
            'isset_login'=>IssetUserLoginAction::init(self::$login,self::$hash)
        );

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(empty($_POST['login'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Login is empty'
            ];

            throw new ParametersException($error);

        }

        self::$login        =mb_strtolower($_POST['login'],'utf-8');
        self::$hash         =empty($_POST['hash'])?NULL:$_POST['hash'];

        return self::set();

    }

}