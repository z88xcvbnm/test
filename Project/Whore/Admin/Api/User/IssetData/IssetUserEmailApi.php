<?php

namespace Project\Whore\Admin\Api\User\IssetData;

use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\All\Action\User\IssetData\IssetUserEmailAction;

class IssetUserEmailApi{

    /** @var string */
    private static $email;

    /** @var string */
    private static $hash;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        $data=array(
            'isset_email'=>IssetUserEmailAction::init(self::$email,self::$hash)
        );

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(empty($_POST['email'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Email is empty'
            ];

            throw new ParametersException($error);

        }

        self::$email        =$_POST['email'];
        self::$hash         =empty($_POST['hash'])?NULL:$_POST['hash'];

        return self::set();

    }

}