<?php

namespace Project\Whore\Admin\Api\User;

use Core\Action\User\Admin\AddAdminSystemAction;
use Core\Module\Date\Date;
use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\UserAccess;
use Project\Whore\Admin\Action\User\Admin\LogoutUserAdminAction;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;

class AddAdminApi{

    /** @var int */
    private static $user_ID;

    /** @var string */
    private static $user_login;

    /** @var string */
    private static $user_name;

    /** @var string */
    private static $user_surname;

    /** @var string */
    private static $user_email;

    /** @var string */
    private static $user_access_type_name;

    /** @var string */
    private static $user_hash_type_name;

    /** @var string */
    private static $link;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_access(){

        if(
              !UserAccess::$is_root
            &&!UserAccess::$is_admin
        ){

            LogoutUserAdminAction::init();

            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        }

        return true;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user(){

        $user_data=AddAdminSystemAction::init(
            self::$user_login,
            self::$user_email,
            self::$user_name,
            self::$user_surname,
            'registration_invite',
            self::$user_access_type_name,
            true
        );

        if(empty($user_data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User was not add'
            ];

            throw new PhpException($error);

        }

        self::$user_ID=$user_data['user_ID'];

        if(!empty($user_data['link']))
            self::$link=$user_data['link'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        // https://whore/registration_invite/33c60017ae4495b725bd6aaf35ba882069ef980b

        $data=[
            'success'   =>true,
            'user'      =>[
                'ID'            =>self::$user_ID,
                'login'         =>self::$user_login,
                'image'         =>[],
                'email'         =>self::$user_email,
                'name'          =>self::$user_name,
                'surname'       =>self::$user_surname,
                'is_root'       =>self::$user_hash_type_name=='root',
                'is_block'      =>false,
                'is_remove'     =>false,
                'date_create'   =>Date::get_timestamp(),
                'date_online'   =>NULL
            ]
        ];

        if(!empty(self::$link))
            $data['link']=self::$link;

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::is_valid_access()){

            self::add_user();

            return self::set_return();

        }

        return false;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['user_login']))
            $error_info_list[]='User login is empty';

        if(empty($_POST['user_email']))
            $error_info_list[]='User email is empty';

        if(empty($_POST['user_name']))
            $error_info_list[]='User name is empty';

        if(empty($_POST['user_surname']))
            $error_info_list[]='User surname is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$user_login               =$_POST['user_login'];
        self::$user_email               =$_POST['user_email'];
        self::$user_name                =$_POST['user_name'];
        self::$user_surname             =$_POST['user_surname'];
        self::$user_access_type_name    =empty($_POST['is_root'])?'admin':'root';

        return self::set();

    }

}