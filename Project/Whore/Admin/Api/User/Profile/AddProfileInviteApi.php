<?php

namespace Project\Whore\Admin\Api\User\Profile;

use Core\Action\User\Profile\AddProfileSystemAction;
use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseSuccess;

class AddProfileInviteApi{

    /** @var int */
    private static $user_ID;

    /** @var string */
    private static $email;

    /** @var string */
    private static $login;

    /** @var string */
    private static $name;

    /** @var string */
    private static $surname;

    /** @var string */
    private static $link;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user(){

        $r=AddProfileSystemAction::init(self::$email,self::$login,self::$surname,self::$name,['profile'],'registration_invite');

        if(empty($r)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User was not add'
            ];

            throw new ParametersException($error);

        }

        self::$user_ID      =$r['user_ID'];
        self::$link         =empty($r['link'])?NULL:$r['link'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'user'  =>[
                    'ID'                        =>self::$user_ID,
                    'login'                     =>self::$login,
                    'email'                     =>self::$email,
                    'surname'                   =>self::$surname,
                    'name'                      =>self::$name,
                    'balance'                   =>0,
                    'date_online'               =>NULL,
                    'is_email_confirmed'        =>false
                ]
            ]
        ];

        if(!empty(self::$link))
            $data['data']['link']=self::$link;

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_user();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['email']))
            $error_info_list[]='Email is not valid';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$email        =mb_strtolower($_POST['email'],'utf-8');
        self::$login        =empty(mb_strtolower($_POST['login'],'utf-8'))?NULL:$_POST['login'];
        self::$surname      =empty($_POST['surname'])?NULL:$_POST['surname'];
        self::$name         =empty($_POST['name'])?NULL:$_POST['name'];

        return self::set();

    }

}