<?php

namespace Project\Whore\Admin\Api\User\Profile;

use Core\Action\Crypto\CryptoCompareCurrencyAction;
use Core\Module\Email\EmailSend;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\OsServer\OsServer;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserEmail;
use Core\Module\User\UserWallet;
use Core\Module\User\UserWalletDonate;
use Core\Module\Wallet\WalletAddress;
use Core\Module\Wallet\WalletName;
use Core\Module\Wallet\WalletValidation;

class AddProfileBalanceTransactionApi{

    /** @var int */
    private static $wallet_name_ID;

    /** @var int */
    private static $user_wallet_ID;

    /** @var int */
    private static $target_wallet_ID;

    /** @var int */
    private static $user_wallet_donate_ID;

    /** @var string */
    private static $user_email;

    /** @var string */
    private static $wallet_name;

    /** @var string */
    private static $user_wallet_address;

    /** @var string */
    private static $target_wallet_address;

    /** @var float */
    private static $transaction_sum;

    /** @var float */
    private static $transaction_sum_rub;

    /** @var array */
    private static $transaction_sum_rub_list=[
        100,
        500,
        1000,
        3000,
        5000,
        10000
    ];

    /** @var float */
    private static $transaction_currency;

    /** @var string */
    private static $hash;

    /** @var string */
    private static $link;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_transaction_sum_rub(){

        if(array_search(self::$transaction_sum_rub,self::$transaction_sum_rub_list)===false){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Transaction sum is not valid'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_user_wallet_address(){

        if(!WalletValidation::is_valid_wallet_address(self::$wallet_name,self::$user_wallet_address)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>(self::$wallet_name=='sberbank'?'Name':'Wallet').' address is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_wallet_name_ID(){

        self::$wallet_name_ID=WalletName::get_wallet_name_ID_from_key(self::$wallet_name);

        if(empty(self::$wallet_name_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Wallet name is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_wallet_ID(){

        self::$user_wallet_ID=UserWallet::get_user_wallet_ID(User::$user_ID,self::$user_wallet_address);

        if(empty(self::$user_wallet_ID))
            self::$user_wallet_ID=UserWallet::add_user_wallet(User::$user_ID,self::$wallet_name_ID,self::$user_wallet_address,0,0,false);

        if(empty(self::$user_wallet_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User wallet was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_target_wallet_ID(){

        self::$target_wallet_ID=WalletAddress::get_wallet_address_ID_from_address(self::$target_wallet_address);

        if(empty(self::$target_wallet_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User wallet was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_transaction_currency(){

        if(
              self::$wallet_name=='sberbank'
            ||self::$wallet_name=='qiwi'
        ){

            self::$transaction_currency     =1;
            self::$transaction_sum          =self::$transaction_sum_rub;

            return true;

        }

        $r=CryptoCompareCurrencyAction::init('RUB',[self::$wallet_name]);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Problem with get currency'
            ];

            throw new PhpException($error);

        }

        foreach($r['currency_list'] as $value){

            self::$transaction_currency=$value;

            break;

        }

        self::$transaction_sum=round(self::$transaction_sum_rub*self::$transaction_currency,5);

        return true;

    }

    /**
     * @return bool
     */
    private static function set_hash(){

        $list=[
            User::$user_ID,
            self::$wallet_name_ID,
            self::$user_wallet_ID,
            self::$target_wallet_ID,
            self::$user_wallet_address,
            self::$target_wallet_address,
            self::$transaction_sum,
            self::$transaction_currency,
            self::$transaction_sum_rub,
            time(),
            rand(0,time())
        ];

        self::$hash=Hash::get_sha1_encode(implode(':',$list));

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_email(){

        self::$user_email=UserEmail::get_user_email_last(User::$user_ID);

        if(empty(self::$user_email)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User email is empty'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_wallet_donate_ID(){

        self::$user_wallet_donate_ID=UserWalletDonate::add_user_wallet_donate(
            User::$user_ID,
            self::$wallet_name_ID,
            self::$user_wallet_ID,
            self::$target_wallet_ID,
            self::$user_wallet_address,
            self::$target_wallet_address,
            self::$transaction_sum,
            self::$transaction_currency,
            self::$transaction_sum_rub,
            self::$hash
        );

        if(empty(self::$user_wallet_donate_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User wallet donate ID was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_notification(){

        $inner  =[];
        $link   =\Config::$http_type.'://'.Url::$host.'/confirm_payment/'.self::$hash;

        self::$link=$link;

        $inner[]='Пользователь: '.self::$user_email;
        $inner[]='Способ пополнения: '.mb_strtoupper(self::$wallet_name);
        $inner[]='Сумма пополнения: '.self::$transaction_sum.' '.mb_strtoupper(self::$wallet_name);
//        $inner[]='Кошелек пользователя: '.self::$user_wallet_address;
        $inner[]='Кошелек пользователя: '.(self::$wallet_name=='sberbank'?'...':'').self::$user_wallet_address;
        $inner[]='Кошелек пополнения: '.self::$target_wallet_address;
        $inner[]='Сумма к зачислению: '.self::$transaction_sum_rub.' ₽';

        $inner[]='Начислить сумму на счет: <a href="'.$link.'">'.$link.'</a>';

        return EmailSend::init(array(\Config::$email_payment),'Пополнение счета '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        if(OsServer::$is_windows){

            $data['link']=self::$link;
//            $data['info']=(self::$wallet_name=='sberbank'?'...':'').self::$user_wallet_address;

        }

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::is_valid_transaction_sum_rub()){

            self::set_wallet_name_ID();

            if(self::is_valid_user_wallet_address()){

                self::set_user_wallet_ID();
                self::set_target_wallet_ID();
                self::set_transaction_currency();
                self::set_user_email();
                self::set_hash();
                self::set_user_wallet_donate_ID();

                self::send_notification();

                return self::set_return();

            }

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['wallet_name']))
            $error_info_list[]='Wallet name is empty';

        if(empty($_POST['user_wallet_address']))
            $error_info_list[]='User wallet address is empty';

        if(empty($_POST['target_wallet_address']))
            $error_info_list[]='Target wallet address is empty';

        if(empty($_POST['transaction_sum_rub']))
            $error_info_list[]='Transaction sum RUB is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$wallet_name                  =mb_strtolower($_POST['wallet_name'],'utf-8');
        self::$user_wallet_address          =$_POST['user_wallet_address'];
        self::$target_wallet_address        =$_POST['target_wallet_address'];
        self::$transaction_sum_rub          =(float)$_POST['transaction_sum_rub'];

        return self::set();

    }

}