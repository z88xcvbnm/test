<?php

namespace Project\Whore\Admin\Api\User\Profile;

use Core\Action\User\User\AddUserSystemAction;
use Core\Module\Exception\ParametersException;
use Core\Module\OsServer\OsServer;
use Core\Module\ReCaptcha\ReCaptcha;
use Core\Module\Response\ResponseSuccess;

class AddProfileApi{

    /** @var int */
    private static $user_ID;

    /** @var string */
    private static $email;

    /** @var string */
    private static $login;

    /** @var string */
    private static $password;

    /** @var string */
    private static $link;

    /** @var string */
    private static $re_captcha_token;

    /** @var string */
    private static $promo_code;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException     */
    private static function set_user(){

//        \Config::$is_debug=true;

        if(\Config::$is_need_recaptcha&&!OsServer::$is_windows)
            ReCaptcha::is_valid_re_captcha_token(self::$re_captcha_token,'reg');

        $r=AddUserSystemAction::init(self::$email,self::$login,self::$password,['profile'],'confirm_email',self::$promo_code);

        if(empty($r)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User was not add'
            ];

            throw new ParametersException($error);

        }

        self::$user_ID      =$r['user_ID'];
        self::$login        =$r['user_login'];
        self::$password     =$r['user_email'];
        self::$link         =empty($r['link'])?NULL:$r['link'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'user'  =>[
                    'login'         =>self::$login,
                    'is_profile'    =>true
                ]
            ]
        ];

        if(!empty(self::$link))
            $data['data']['link']=self::$link;

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_user();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['email']))
            $error_info_list[]='Email is not valid';

        if(empty($_POST['login']))
            $error_info_list[]='Login is empty';

        if(empty($_POST['password']))
            $error_info_list[]='Password is empty';

        if(\Config::$is_need_recaptcha&&!OsServer::$is_windows)
            if(empty($_POST['re_captcha_token']))
                $error_info_list[]='ReCaptcha token is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$email                =mb_strtolower(trim($_POST['email']),'utf-8');
        self::$login                =mb_strtolower(trim($_POST['login']),'utf-8');
        self::$password             =trim($_POST['password']);
        self::$re_captcha_token     =empty($_POST['re_captcha_token'])?NULL:$_POST['re_captcha_token'];
        self::$promo_code           =empty($_POST['promo_code'])?NULL:$_POST['promo_code'];

        return self::set();

    }

}