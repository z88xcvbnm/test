<?php

namespace Project\Whore\Admin\Api\User\Profile;

use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\User\Profile\GetProfileListAction;

class GetProfileListApi{

    /** @var int */
    private static $user_ID;

    /** @var string */
    private static $search;

    /** @var array */
    private static $profile_list    =[];

    /** @var int */
    private static $start           =0;

    /** @var int */
    private static $len             =20;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_profile_list(){

        $r=GetProfileListAction::init(self::$start,self::$len,self::$user_ID,self::$search);

        if(count($r)==0)
            return false;

        self::$profile_list=array_values($r['user_list']);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'profile_list'=>self::$profile_list
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_profile_list();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::$user_ID      =empty($_POST['user_ID'])?NULL:(int)$_POST['user_ID'];
        self::$search       =empty($_POST['search'])?NULL:$_POST['search'];

        return self::set();

    }

}