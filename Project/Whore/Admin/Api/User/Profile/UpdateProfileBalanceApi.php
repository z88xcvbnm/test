<?php

namespace Project\Whore\Admin\Api\User\Profile;

use Core\Module\Email\EmailSend;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserBalance;
use Core\Module\User\UserBalanceTransaction;
use Core\Module\User\UserEmail;

class UpdateProfileBalanceApi{

    /** @var int */
    private static $user_ID;

    /** @var string */
    private static $email;

    /** @var float */
    private static $balance;

    /** @var string */
    private static $info;

    /** @var string */
    private static $transaction_action;

    /** @var int */
    private static $user_balance_transaction_ID;

    /** @var array */
    private static $transaction_action_list=[
        'add_hand_money',
        'remove_hand_money'
    ];

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user(){

        if(!User::isset_user_ID(self::$user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is not exists'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_email(){

        self::$email=UserEmail::get_user_email_last(self::$user_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_action(){

        if(array_search(self::$transaction_action,self::$transaction_action_list)===false){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Action is not exists'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_balance(){

        switch(self::$transaction_action){

            case'add_hand_money':{

                if(!UserBalance::add_money_user_balance(self::$user_ID,self::$balance)){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'User balance was not update'
                    ];

                    throw new PhpException($error);

                }

                return true;

            }

            case'remove_hand_money':{

                if(!UserBalance::remove_money_user_balance(self::$user_ID,self::$balance)){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'User balance was not update'
                    ];

                    throw new PhpException($error);

                }

                return true;

            }

            default:{

                $error=[
                    'title'     =>ParametersValidationException::$title,
                    'info'      =>'Transaction action is not exists'
                ];

                throw new ParametersValidationException($error);

            }

        }

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_balance(){

        self::$balance=UserBalance::get_user_balance(self::$user_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_balance_transaction(){

        self::$user_balance_transaction_ID=UserBalanceTransaction::add_user_balance_transaction(self::$user_ID,NULL,NULL,NULL,NULL,self::$transaction_action,self::$balance,self::$info);

        if(empty(self::$user_balance_transaction_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User balance transaction was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_balance_in_user_balance_transaction(){

        if(!UserBalanceTransaction::update_balance_in_user_balance_transaction(self::$user_balance_transaction_ID,self::$balance)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Balance was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_notification(){

        if(self::$transaction_action=='remove_hand_money')
            return true;

        $inner  =[];

        $inner[]='Добрый день!';
        $inner[]='';
        $inner[]='На Ваш счёт начисленно '.self::$balance.' ₽';
        $inner[]='';
        $inner[]='Основание: '.self::$info;
        $inner[]='';
        $inner[]='С уважением,';
        $inner[]='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

        return EmailSend::init(array(self::$email),'Пополнение счета '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'transaction_ID'    =>self::$user_balance_transaction_ID,
                'balance'           =>UserBalance::get_user_balance(self::$user_ID),
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_user())
            if(self::isset_action()){

                self::add_user_balance_transaction();
                self::update_user_balance();
                self::set_user_email();
                self::update_balance_in_user_balance_transaction();

                self::send_notification();

                self::set_user_balance();

                return self::set_return();

            }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['user_ID']))
            $error_info_list[]='User ID is empty';

        if(empty($_POST['balance']))
            $error_info_list[]='Balance is empty';
        else if($_POST['balance']<=0)
            $error_info_list[]='Balance not valid';

        if(empty($_POST['action']))
            $error_info_list[]='Action is not valid';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$user_ID              =(int)$_POST['user_ID'];
        self::$balance              =(float)$_POST['balance'];
        self::$transaction_action   =$_POST['action'];
        self::$info                 =empty($_POST['info'])?NULL:$_POST['info'];

        return self::set();

    }

}