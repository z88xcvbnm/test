<?php

namespace Project\Whore\Admin\Api\Auth\UserAuth;

use Core\Module\Exception\ParametersException;
use Core\Module\Os\Os;
use Core\Module\OsServer\OsServer;
use Core\Module\ReCaptcha\ReCaptcha;
use Core\Module\Response\ResponseAccessDenied;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Token\Token;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserBalance;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Config\PageConfigProfileWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;
use Project\Whore\All\Action\Auth\User\UserAuthProjectAction;
use Project\Whore\Admin\Config\PageConfigAdminWhore;

class UserAuthApi{

    /** @var string */
    private static $login;

    /** @var string */
    private static $password;

    /** @var string */
    private static $re_captcha_token;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_user_balance_table(){

        if(!UserBalance::isset_user_balance(User::$user_ID))
            UserBalance::add_user_balance(User::$user_ID);

        return true;

    }

    /**
     * @return string|null
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_login(){

        return UserLogin::get_user_login_default();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function redirect_admin(){

        $user_login=UserLogin::get_user_login_default();

        $data=array(
            'token'         =>Token::$token_hash,
            'type'          =>'admin',
            'title'         =>ContentAdminWhore::get_page_title('admin_page_title'),
            'redirect'      =>'/admin/'.$user_login.'/'.PageConfigAdminWhore::$root_admin_page_default
        );

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function redirect_profile(){

        $user_login=UserLogin::get_user_login_default();

        $data=array(
            'token'         =>Token::$token_hash,
            'type'          =>'profile',
            'title'         =>ContentAdminWhore::get_page_title('profile_face_page_title'),
            'redirect'      =>'/'.PageConfigProfileWhore::$root_page_default.'/'.$user_login.'/'.PageConfigProfileWhore::$root_profile_face_page_default
        );

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     */
    private static function set(){

        if(\Config::$is_need_recaptcha&&!OsServer::$is_windows)
            ReCaptcha::is_valid_re_captcha_token(self::$re_captcha_token,'auth');

        $r=UserAuthProjectAction::init(self::$login,self::$password);

        if(!empty($r))
            if($r['success'])
                if(
                      UserAccess::$is_root
                    ||UserAccess::$is_admin
                ){

                    self::set_user_login();

                    return self::redirect_admin();

                }
                else if(
                      UserAccess::$is_profile
                    ||UserAccess::$is_profile_wallet
                ){

                    self::set_user_login();

                    self::check_user_balance_table();

                    return self::redirect_profile();

                }

        return ResponseAccessDenied::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Exception\UnknownException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['login']))
            $error_info_list[]='Login is empty';

        if(empty($_POST['password']))
            $error_info_list[]='Password is empty';

        if(\Config::$is_need_recaptcha&&!OsServer::$is_windows)
            if(empty($_POST['re_captcha_token']))
                $error_info_list[]='ReCaptcha token is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$login                =mb_strtolower(trim($_POST['login']),'utf-8');
        self::$password             =trim($_POST['password']);
        self::$re_captcha_token     =empty($_POST['re_captcha_token'])?NULL:$_POST['re_captcha_token'];

        return self::set();

    }

}