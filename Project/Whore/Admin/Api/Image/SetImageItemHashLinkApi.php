<?php

namespace Project\Whore\Admin\Api\Image;

use Core\Module\Db\Db;
use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\DbQueryException;
use Core\Module\Response\ResponseSuccess;

class SetImageItemHashLinkApi{

    /** @var int */
    private static $image_len=0;

    /**
     * @param int|NULL $start
     * @param int|NULL $len
     * @return bool
     * @throws DbQueryException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_image_item_hash_link(int $start=NULL,int $len=NULL){

        $q=[
            'select'=>[
                'id',
                'image_id',
                'image_type',
                'date_create'
            ],
            'table'=>'_image_item',
            'where'=>[
                'type'=>0
            ],
            'order'=>[
                [
                    'column'    =>'id',
                    'direction' =>'desc'
                ]
            ],
            'limit'=>[
                $start,
                $len
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return 0;

        $list=[];

        foreach($r as $row)
            $list[$row['id']]=Hash::get_sha1_encode(Dir::get_dir_from_date(DirConfig::$dir_image,$row['date_create']).'/'.$row['image_id'].'/'.$row['id'].'.'.$row['image_type']);

        foreach($list as $image_item_ID=>$hash_link){

            $q=[
                'table'     =>'_image_item',
                'set'       =>[
                    'hash_link'=>$hash_link
                ],
                'where'     =>[
                    'id'=>$image_item_ID
                ]
            ];

            if(!Db::update($q)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'Image item was not update'
                ];

                throw new DbQueryException($error);

            }

        }

        return count($list);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'   =>true,
            'data'      =>[
                'image_len'=>self::$image_len
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        $start      =0;
        $len        =100;

        do{

            $image_len              =self::prepare_image_item_hash_link($start,$len);
            self::$image_len        +=$image_len;
            $start                  +=$len;

            if($image_len==0)
                break;

        }while($image_len!=0);

        return self::set_return();

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}