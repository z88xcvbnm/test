<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Core\Module\OsServer\OsServer;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Sort\Sort;
use Core\Module\User\User;
use Core\Module\User\UserBalance;
use Core\Module\User\UserBalanceTransaction;
use Project\Whore\Admin\Action\Image\ImagePathListAction;
use Project\Whore\Admin\Action\Video\VideoPathListAction;
use Project\Whore\All\Action\FacePlusPlus\SearchFaceTokenInFacesetPartsFacePlusPlusAction;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Face\FaceSearch;
use Project\Whore\All\Module\Face\FaceSearchMulti;
use Project\Whore\All\Module\Face\FaceVideo;
use Project\Whore\All\Module\Price\PriceConfig;
use Project\Whore\All\Module\Source\Source;

class SearchFacePartComboApi{

    /** @var int */
    private static $face_search_multi_ID;

    /** @var int */
    private static $face_search_ID;

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $image_ID;

    /** @var int */
    private static $user_balance_transaction_ID;

    /** @var string */
    private static $face_search_multi_token;

    /** @var string */
    private static $face_search_token;

    /** @var string */
    private static $face_token;

    /** @var array */
    private static $source_rang_list        =[];

    /** @var int */
    private static $face_len_max            =20;

    /** @var int */
    private static $percent_min             =75;

    /** @var float */
    private static $balance;

    /** @var float */
    private static $percent;

    /** @var int */
    private static $faceset_index;

    /** @var int */
    private static $faceset_len;

    /** @var array */
    private static $face_coords_list        =[];

    /** @var array */
    private static $face_ID_list            =[];

    /** @var array */
    private static $face_list               =[];

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_search_multi_ID(){

        self::$face_search_multi_ID=FaceSearchMulti::get_face_search_multi_ID(self::$face_search_multi_token);

        if(empty(self::$face_search_multi_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face search token is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_search_token(){

        $r=FaceSearch::get_face_search_data_from_face_search_token_with_multi_ID(self::$face_search_multi_ID,self::$face_search_token);

        if(empty($r)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face search token is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        if(User::$user_ID!=$r['user_ID']){

            $error=[
                'title'     =>AccessDeniedException::$title,
                'info'      =>'Access denied'
            ];

            throw new AccessDeniedException($error);

        }

        if($r['faceset_len']==$r['faceset_index']){

            self::set_return();

            return false;

        }

        self::$face_search_ID       =$r['ID'];
        self::$file_ID              =$r['file_ID'];
        self::$image_ID             =$r['image_ID'];
        self::$face_token           =$r['face_token'];
        self::$face_coords_list     =empty($r['face_coords'])?NULL:Json::decode($r['face_coords']);
        self::$faceset_len          =(int)$r['faceset_len'];
        self::$faceset_index        =(int)$r['faceset_index'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_list(){

        self::$faceset_index++;

        $r=SearchFaceTokenInFacesetPartsFacePlusPlusAction::init(self::$face_search_ID,self::$file_ID,self::$image_ID,self::$face_token,self::$faceset_index,self::$faceset_len,self::$face_coords_list,self::$face_len_max,self::$face_search_multi_ID);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Problem with search'
            ];

            throw new PhpException($error);

        }

        if(!isset($r['face_search_ID']))
            return true;

        self::$face_search_ID       =$r['face_search_ID'];
        self::$face_list            =Sort::sort_with_key('percent',$r['face_token_list'],'desc');

        return true;

    }

    /**
     * @return bool
     */
    private static function set_percent(){

        self::$percent=0;

        if(count(self::$face_list)==0)
            return true;

        foreach(self::$face_list as $row)
            if(self::$percent<$row['percent']){

                self::$face_ID      =$row['face_ID'];
                self::$percent      =$row['percent'];

            }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_face_ID_list(){

        if(count(self::$face_list)==0)
            return true;

        foreach(self::$face_list as $row)
            self::$face_ID_list[]=$row['face_ID'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_list(){

        $r=FaceImage::get_image_ID_list_from_face_ID_list(self::$face_ID_list);

        if(count($r)==0)
            return true;

        $image_list     =ImagePathListAction::get_image_path_list(array_keys($r),true,true);

        if(count($image_list)==0)
            return true;

        foreach(self::$face_list as $index=>$face_row)
            if(isset($image_list[$face_row['image_ID']]))
                self::$face_list[$index]['image']=$image_list[$face_row['image_ID']];

        foreach(self::$face_list as $index=>$face_row)
            if(!isset($face_row['image']))
                unset(self::$face_list[$index]);

        if(count(self::$face_list)==0)
            return true;

        $list=[];

        foreach(self::$face_list as $row){

            $face_ID=$row['face_ID'];

            if(!isset($list[$face_ID]))
                $list[$face_ID]=[
                    'face_ID'           =>$face_ID,
                    'find_len'          =>0,
                    'percent'           =>0,
                    'image_percent'     =>[],
                    'image_len'         =>0,
                    'image'             =>$row['image']
                ];

            if(floatval($list[$face_ID]['percent'])<$row['percent'])
                $list[$face_ID]['percent']=$row['percent'];

            $list[$face_ID]['find_len']++;
            $list[$face_ID]['image_percent'][$row['image_ID']]=$row['percent'];

        }

        self::$face_list    =Sort::sort_with_key('percent',$list,'desc');
        $list               =[];
        $face_index         =0;

        foreach(self::$face_list as $index=>$row){

            $percent=round(self::$face_list[$index]['percent']/100,2);

            self::$face_list[$index]['percent']=$percent;

        }

        foreach(self::$face_list as $row)
            if($row['percent']>=self::$percent_min){

                $list[]=$row;

                $face_index++;

                if($face_index>=self::$face_len_max)
                    break;

            }

        self::$face_list=$list;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_total_image_list(){

        if(count(self::$face_list)==0)
            return false;

        $face_ID_list=[];

        foreach(self::$face_list as $row)
            $face_ID_list[]=$row['face_ID'];

        $image_len_list=FaceImage::get_image_len_list($face_ID_list);

        foreach($image_len_list as $face_ID=>$len)
            foreach(self::$face_list as $index=>$row)
                if($face_ID==$row['face_ID'])
                    self::$face_list[$index]['image_len']=$len;

        self::$face_list                        =array_values(self::$face_list);

        $face_ID                                =self::$face_list[0]['face_ID'];
        $image_ID_list                          =FaceImage::get_image_ID_list($face_ID);

        self::$face_list[0]['image_list']       =array_values(ImagePathListAction::get_image_path_list($image_ID_list,true,true));

        $video_ID_list                          =FaceVideo::get_video_ID_list($face_ID);

        if(count($video_ID_list)>0)
            self::$face_list[0]['video_list']=array_values(VideoPathListAction::get_video_path_list($video_ID_list,true,true));
        else
            self::$face_list[0]['video_list']=[];

        return true;

    }

    /**
     * @param string|NULL $info
     * @return array
     */
    private static function get_info_list(string $info=NULL){

        if(empty($info))
            return[];

        $row_list=mb_split("\n",strip_tags($info));

        if(count($row_list)==0)
            return[];

        $list=[];

        foreach($row_list as $row){

            $is_added       =false;
            $label_list     =mb_split('\:',$row);
            $label          ='';
            $value          ='';

            if(count($label_list)===2){

                if(trim($label_list[0])=='Other')
                    break;

                if(!$is_added){

                    $label      =$label_list[0];
                    $value      =$label_list[1];

                    switch($label){

                        case'Цели знакомства':{

                            $is_added=false;

                            break;
                        }

                        default:{

                            $is_added=true;

                            break;

                        }

                    }

                }

            }
            else if(!empty(trim($label_list[0]))){

                $label      ='О себе';
                $value      =trim($label_list[0]);

                $is_added=true;

            }

            if($is_added){

                $body=mb_split('\/',$value);

                if(count($body)==3)
                    $value=$body[0].'/'.$body[1].'/'.$body[2];

                $body=mb_split('\-',$value);

                if(count($body)==3)
                    $value=$body[0].'/'.$body[1].'/'.$body[2];

                if(isset($list[$label]))
                    $list[$label]['value'].="\n".$value;
                else
                    $list[$label]=[
                        'label'     =>$label,
                        'value'     =>$value
                    ];

            }

        }

        return array_values($list);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_source_rang_list(){

        $r=Source::get_source_rang_list();

        if(count($r)==0)
            return false;


        foreach($r as $source_ID=>$row)
            self::$source_rang_list[$source_ID]=(int)$row['rang'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_data(){

        $r=FaceData::get_face_data_list_from_face_ID_list(self::$face_ID_list);

        if(count($r)==0)
            return true;

        foreach(self::$face_list as $index=>$row){

            $face_ID=$row['face_ID'];

            if(isset($r[$face_ID])){

                self::$face_list[$index]['rang']                    =$r[$face_ID]['face_data_type_ID'];
//                self::$face_list[$index]['rang']                    =isset(self::$source_rang_list[$r[$face_ID]['source_ID']])?self::$source_rang_list[$r[$face_ID]['source_ID']]:NULL;
                self::$face_list[$index]['name']                    =$r[$face_ID]['name'];
                self::$face_list[$index]['age']                     =$r[$face_ID]['age'];
                self::$face_list[$index]['city']                    =$r[$face_ID]['city'];
                self::$face_list[$index]['info']                    =empty($r[$face_ID]['info'])?[]:self::get_info_list($r[$face_ID]['info']);
                self::$face_list[$index]['source_account_link']     =$r[$face_ID]['source_account_link'];

            }

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_faceset_index(){

//        self::$faceset_index++;

        $type=self::$faceset_index==self::$faceset_len?0:NULL;

        if(!FaceSearch::update_face_search_face_index(self::$face_search_ID,self::$faceset_index,$type)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face search had not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_search(){

        if(self::$faceset_index!=self::$faceset_len)
            return true;

        $type=self::$faceset_index==self::$faceset_len?'0':NULL;

        if(!FaceSearch::update_face_search(self::$face_search_ID,self::$face_ID,self::$user_balance_transaction_ID,self::$face_coords_list,self::$face_token,count(self::$face_coords_list),self::$percent,PriceConfig::$search_price,NULL,NULL,NULL,$type)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face search was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

//        if(self::$faceset_index==self::$faceset_len)
//            $data=[
//                'success'           =>true,
//                'balance'           =>UserBalance::get_user_balance(User::$user_ID),
//                'search_token'      =>self::$face_search_ID,
//                'face_list'         =>array_values(self::$face_list)
//            ];
//        else
        $data=[
            'success'   =>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::set_face_search_multi_ID())
            if(self::isset_face_search_token()){

                self::set_source_rang_list();
                self::set_face_list();

                if(self::$faceset_index==self::$faceset_len){

                    self::set_face_ID_list();
                    self::set_face_image_list();
    //                    self::set_face_data();
                    self::set_face_total_image_list();

                    self::set_percent();
                    self::update_face_search();

                }

                self::update_faceset_index();

                return self::set_return();

            }

        return false;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

//        if(OsServer::$is_windows)
//            \Config::$is_debug=true;

        $error_info_list=[];

        if(empty($_POST['face_search_multi_token']))
            $error_info_list[]='File search multi token is empty';

        if(empty($_POST['face_search_token']))
            $error_info_list[]='File search token is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$face_search_multi_token      =$_POST['face_search_multi_token'];
        self::$face_search_token            =$_POST['face_search_token'];

        return self::set();

    }

}