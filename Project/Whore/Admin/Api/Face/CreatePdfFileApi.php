<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Dir\Dir;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Mpdf\Src\Mpdf;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\Admin\Action\Image\ImagePathListAction;
use Project\Whore\Admin\Content\ContentAdminWhore;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Face\FaceSearch;
use Project\Whore\All\Module\Face\FaceSearchItem;
use Project\Whore\All\Module\Face\FaceSearchPdf;

class CreatePdfFileApi{

    /** @var int */
    private static $face_search_pdf_ID;

    /** @var int */
    private static $face_search_ID;

    /** @var int */
    private static $face_search_item_ID;

    /** @var int */
    private static $face_data_type_ID;

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $face_data_ID;

    /** @var int */
    private static $source_image_ID;

    /** @var int */
    private static $result_image_ID;

    /** @var array */
    private static $source_image;

    /** @var array */
    private static $result_image;

    /** @var string */
    private static $face_search_token;

    /** @var string */
    private static $face_token;

    /** @var string */
    private static $face_search_pdf_token;

    /** @var array */
    private static $pdf_content_list            =[];

    /** @var array */
    private static $image_list                  =[];

    /** @var array */
    private static $content_list=[];

    /** @var string */
    private static $pdf_file_path;

    /** @var float */
    private static $percent;

    /** @var string */
    private static $name;

    /** @var int */
    private static $age;

    /** @var string */
    private static $city;

    /** @var string */
    private static $source_account_link;

    /** @var string */
    private static $info;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$face_search_pdf_ID               =NULL;
        self::$face_search_ID                   =NULL;
        self::$face_ID                          =NULL;
        self::$face_search_token                =NULL;
        self::$face_search_pdf_token            =NULL;
        self::$face_token                       =NULL;
        self::$pdf_file_path                    =NULL;
        self::$percent                          =NULL;
        self::$name                             =NULL;
        self::$age                              =NULL;
        self::$city                             =NULL;
        self::$source_account_link              =NULL;
        self::$info                             =NULL;
        self::$source_image_ID                  =NULL;
        self::$source_image                     =NULL;
        self::$pdf_content_list                 =[];
        self::$image_list                       =[];
        self::$content_list                     =[];

        return true;

    }

    /**
     * @param string|NULL $info
     * @return array
     */
    private static function get_info_list(string $info=NULL){

        if(empty($info))
            return[];

        $row_list=mb_split("\n",$info);

        if(count($row_list)==0)
            return[];

        $list=[];

        foreach($row_list as $row){

            $is_added       =false;
            $label_list     =mb_split('\:',$row);
            $label          ='';
            $value          ='';

            if(count($label_list)===2){

                if(trim($label_list[0])=='Other')
                    break;

                if(!$is_added){

                    $label      =$label_list[0];
                    $value      =$label_list[1];

                    switch($label){

                        case'Цели знакомства':{

                            $is_added=false;

                            break;
                        }

                        default:{

                            $is_added=true;

                            break;

                        }

                    }

                }

            }
            else if(!empty(trim($label_list[0]))){

                $label      ='О себе';
                $value      =trim($label_list[0]);

                $is_added=true;

            }

            if($is_added){

                $body=mb_split('\/',$value);

                if(count($body)==3)
                    $value=$body[0].'/'.$body[1].'/'.$body[2];

                $body=mb_split('\-',$value);

                if(count($body)==3)
                    $value=$body[0].'/'.$body[1].'/'.$body[2];

                $list[]=[
                    'label'     =>$label,
                    'value'     =>$value
                ];

            }

        }

        return $list;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_search_pdf(){

        self::$face_search_pdf_ID=FaceSearchPdf::add_face_search_pdf(self::$face_search_ID,self::$face_search_item_ID,self::$face_ID,self::$face_data_ID,self::$face_search_pdf_token);

        if(empty(self::$face_search_pdf_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face search PDF was not create'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_search_pdf_to_public(){

        if(!FaceSearchPdf::update_face_search_pdf_to_public(self::$face_search_pdf_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face search PDF was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function create_pdf_path(){

        self::$pdf_file_path=DirConfigProject::$dir_pdf;

        Dir::create_dir(self::$pdf_file_path);

        self::$pdf_file_path.='/'.self::$face_search_pdf_ID.'.pdf';

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Mpdf\Src\MpdfException
     */
    private static function create_pdf(){

        $config=[
            'debug'             =>false,
            'debugfonts'        =>false,
            'showImageErrors'   =>false,
            'format'            =>'A4',
            'margin_left'       =>13,
            'margin_right'      =>13,
            'margin_top'        =>13,
            'margin_bottom'     =>13,
            'default_font'      =>'Arial'
        ];

        $mpdf       =new Mpdf($config);
        $content    =implode('',self::$content_list);

        $mpdf->WriteHTML($content,2);
        $mpdf->Output(self::$pdf_file_path,'F');

        return true;

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function create_pdf_content(){

        $project_name=ContentAdminWhore::get_project_name();

        $list=[];

        $list[]='<!DOCTYPE html>';
        $list[]='<html>';
        $list[]='<head>';
        
        $list[]='<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
        
//        $list[]='<title>'.$project_name.'</title>';
        
        $list[]='</head>';
        
        $list[]='<body>';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; color: #222222; font-family: Arial, sans-serif">';

        $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; height: 2000px">';

        $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; padding: 0 0 5px 0; border-bottom: 1px solid #5585bf; margin: 0 0 20px 0;">';
        $list[]='<span style="font-size: 30px; color: #e9c2c1">'.$project_name.':</span>';
        $list[]='<span style="font-size: 22px; color: #17365d;"> результат поиска по лицам</span>';
        $list[]='</div>';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden;">';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; font-size: 20px; color: #376092">';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 50%; float: left; overflow: hidden; text-align: center">Исходное изображение</div>';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 50%; float: left; overflow: hidden; text-align: center">Обнаруженное совпадение ('.(round(self::$percent,2)/100).'%) </div>';
        
        $list[]='</div>';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden;">';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">';
        
        $list[]='<img src="/'.self::$source_image['image_dir'].'/'.self::$source_image['image_item_ID_list']['large']['ID'].'" width="100%" />';
        
        $list[]='</div>';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">';
        
        $list[]='<img src="/'.self::$result_image['image_dir'].'/'.self::$result_image['image_item_ID_list']['large']['ID'].'" width="100%" />';
        
        $list[]='</div>';
        
        $list[]='</div>';
        
        $list[]='</div>';

        $face_type              ='';
        $face_type_colour       ='';

        switch(self::$face_data_type_ID){

            case 1:{

                $face_type          ='проституция';
                $face_type_colour   ='#ff0000';

                break;

            }

            case 2:{

                $face_type          ='эскорт';
                $face_type_colour   ='#ff8100';

                break;

            }

            case 3:{

                $face_type          ='содержанка';
                $face_type_colour   ='#0062ff';

                break;

            }

            case 4:{

                $face_type          ='начинающая';
                $face_type_colour   ='#158400';

                break;

            }

        }
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; color: #17365d; font-size: 25px; padding: 0 0 5px 0; border-bottom: 1px solid #5585bf; margin: 0 0 10px 0">Анкетные данные'.(empty($face_type)?'':(' <i style="color: '.$face_type_colour.'">('.$face_type.')</i>')).'</div>';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; font-size: 14px; font-style: italic; color: #8c8c8c; margin: 0 0 10px 0;">';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 49%; float: left; padding: 0 1% 0 0; overflow: hidden;">Основная информация:</div>';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 49%; float: left; padding: 0 0 0 1%; overflow: hidden;">Дополнительная информация:</div>';
        
        $list[]='</div>';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; font-size: 14px;">';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 48%; float: left; overflow: hidden; text-align: center; padding: 0 1% 0 1%">';
        
        $list[]='<table width="100%" cellpadding="1" cellspacing="0" border="0">';

        if(!empty(self::$name)){

            $list[]='<tr>';
            $list[]='<td width="30%" align="left">Имя:</td>';
            $list[]='<td width="70%" align="left" style="font-weight: bold">'.self::$name.'</td>';
            $list[]='</tr>';

        }

        if(!empty(self::$age)){

            $list[]='<tr>';
            $list[]='<td width="30%" align="left">Возраст:</td>';
            $list[]='<td width="70%" align="left" style="font-weight: bold">'.self::$age.'</td>';
            $list[]='</tr>';

        }

        if(!empty(self::$city)){

            $list[]='<tr>';
            $list[]='<td width="30%" align="left">Город:</td>';
            $list[]='<td width="70%" align="left" style="font-weight: bold">'.self::$city.'</td>';
            $list[]='</tr>';

        }

        if(!empty(self::$source_account_link)){

            $list[]='<tr>';
            $list[]='<td width="30%" align="left">Источник:</td>';
            $list[]='<td width="70%" align="left" style="font-weight: bold">'.self::$source_account_link.'</td>';
            $list[]='</tr>';

        }
        
        $list[]='</table>';
        
        $list[]='</div>';
        
        $list[]='<div style="position: relative; top: 0; left: 0; width: 48%; float: left; overflow: hidden; text-align: center; padding: 0 1% 0 1%">';

        $social_list    =[];
        $info_list      =[];

        if(empty(self::$info))
            $list[]='<i>Данные отсутствуют</i>';
        else{

            $r=self::get_info_list(self::$info);

            $sort_list=[
                'Instagram'     =>0,
                'VK'            =>1,
                'Facebook'      =>2,
                'Twitter'       =>3
            ];

            foreach($r as $row)
                switch($row['label']){

                    case'Instagram':
                    case'VK':
                    case'Facebook':
                    case'Twitter':{

                        $social_list[$sort_list[$row['label']]]=$row;

                        break;

                    }

                    default:{

                        $info_list[]=$row;

                        break;

                    }

                }

            sort($social_list);

            $social_list=array_values($social_list);

            if(count($info_list)==0)
                $list[]='<i>Данные отсутствуют</i>';
            else{

                $list[]='<table width="100%" cellpadding="1" cellspacing="0" border="0">';

                foreach($info_list as $row){

                    $list[]='<tr>';
                    $list[]='<td width="30%" align="left">'.$row['label'].':</td>';
                    $list[]='<td width="70%" align="left" style="font-weight: bold">'.$row['value'].'</td>';
                    $list[]='</tr>';

                }

                $list[]='</table>';

            }

        }
        
        $list[]='</div>';
        
        $list[]='</div>';

        if(count($social_list)>0){

            $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; font-size: 14px; font-style: italic; color: #8c8c8c; margin: 10px 0 0 0;">Социальные сети:</div>';

            $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; font-size: 12px; font-style: italic; color: #338dcd; margin: 0 0 10px 0;">';

            $list[]='<table width="100%" cellspacing="0" cellpadding="0"><tr>';

            $n=0;

            foreach($social_list as $row)
                switch($row['label']){

                    case'Instagram':{

                        $list[]='<td width="25%">';

                        $list[]='<table width="100%" cellspacing="0" cellpadding="10"><tr>';

                        $list[]='<td><img src="/Project/Whore/Admin/Template/Images/Social/instagram.png" width="40" style="position: relative; top: 0; left: 0; float: left; overflow: hidden;" /></td>';
                        $list[]='<td valign="middle" style="font-style: italic;">'.$row['value'].'</div>';

                        $list[]='</tr></table>';

                        $list[]='</td>';

                        break;

                    }

                    case'VK':{

                        $list[]='<td width="25%">';

                            $list[]='<table width="100%" cellspacing="0" cellpadding="10"><tr>';

                                $list[]='<td><img src="/Project/Whore/Admin/Template/Images/Social/vk.png" width="40" style="position: relative; top: 0; left: 0; float: left; overflow: hidden;" /></td>';
                                $list[]='<td valign="middle" style="font-style: italic;">'.$row['value'].'</div>';

                            $list[]='</tr></table>';

                        $list[]='</td>';

                        break;

                    }

                    case'Facebook':{

                        $list[]='<td width="25%">';

                        $list[]='<table width="100%" cellspacing="0" cellpadding="10"><tr>';

                        $list[]='<td><img src="/Project/Whore/Admin/Template/Images/Social/facebook.png" width="40" style="position: relative; top: 0; left: 0; float: left; overflow: hidden;" /></td>';
                        $list[]='<td valign="middle" style="font-style: italic;">'.$row['value'].'</div>';

                        $list[]='</tr></table>';

                        $list[]='</td>';

                        break;

                    }

                    case'Twitter':{

                        $list[]='<td width="25%">';

                        $list[]='<table width="100%" cellspacing="0" cellpadding="10"><tr>';

                        $list[]='<td><img src="/Project/Whore/Admin/Template/Images/Social/twitter.png" width="40" style="position: relative; top: 0; left: 0; float: left; overflow: hidden;" /></td>';
                        $list[]='<td valign="middle" style="font-style: italic;">'.$row['value'].'</div>';

                        $list[]='</tr></table>';

                        $list[]='</td>';

                        break;

                    }

                }

            for($i=$n;$i<count($social_list);$i++)
                $list[]='<td width="25%"></td>';

            $list[]='</tr></table>';

            $list[]='</div>';

        }

        $list[]='</div>';

        if(count(self::$image_list)>1){

            $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden; color: #17365d; font-size: 25px; padding: 0 0 5px 0; border-bottom: 1px solid #5585bf; margin: 0 0 10px 0">Дополнительные фотографии</div>';

            $n=0;

            foreach(self::$image_list as $row){

                $image_link=$row['image_dir'].'/'.$row['image_item_ID_list']['large']['ID'];

                if(file_exists($image_link)){

                    if($n==0)
                        $list[]='<div style="position: relative; top: 0; left: 0; width: 100%; overflow: hidden;">';

//                    $image_link=$image_link;

                    $list[]='<div style="position: relative; top: 0; left: 0; width: 46%; float: left; overflow: hidden; text-align: center; padding: 10px 2% 10px 2%; vertical-align: middle;">';

                    $list[]='<img src="/'.$image_link.'" width="100%" />';

                    $list[]='</div>';

                    if($n==1)
                        $list[]='</div>';

                    $n++;

                    if($n==2)
                        $n=0;

                }

            }

        }
        
        $list[]='</body>';
        
        $list[]='</html>';

        self::$content_list=$list;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_search_ID(){

        self::$face_search_ID=self::$face_search_token;

        if(empty(self::$face_search_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face search ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_search_data(){

        $r=FaceSearch::get_face_search_data(self::$face_search_ID);

        if(empty($r)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face search data is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        self::$source_image_ID=$r['image_ID'];

        if(empty(self::$source_image_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Source image ID is empty'
            ];

            throw new PhpException($error);

        }

        self::$source_image=ImagePathAction::get_image_path_data(self::$source_image_ID,true,true,true);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_data_ID(){

        self::$face_data_ID=FaceData::get_face_data_ID(self::$face_ID);

        if(empty(self::$face_data_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face data ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_percent(){

        self::$percent=FaceSearch::get_face_search_percent(self::$face_search_ID)*100;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_search_item_ID(){

        $r=FaceSearchItem::get_face_search_item_data(self::$face_search_ID,self::$face_ID);

        if(empty($r)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face search item is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        self::$face_search_item_ID          =$r['face_search_item_ID'];
        self::$result_image_ID              =$r['image_ID'];

        if(!empty(self::$result_image_ID))
            self::$result_image=ImagePathAction::get_image_path_data(self::$result_image_ID,true,true,true);

//        self::$percent                      =$r['percent'];
//
//        if(self::$percent>10000)
//            self::$percent=round(self::$percent/100,2);
//        else if(self::$percent>100)
//            self::$percent=round(self::$percent,2);
//        else
//            self::$percent=round(self::$percent*100,2);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_ID(){

        self::$face_ID=self::$face_token;

        if(empty(self::$face_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_search_pdf_ID(){

        self::$face_search_pdf_ID=FaceSearchPdf::get_face_search_pdf_ID(self::$face_search_ID,self::$face_ID);

        return true;

    }

    /**
     * @return bool
     */
    private static function set_face_search_pdf_token(){

        $data=[
            User::$user_ID,
            self::$face_search_ID,
            self::$face_search_item_ID,
            self::$face_ID,
            self::$face_data_ID,
            time(),
            rand(0,time())
        ];

        self::$face_search_pdf_token=Hash::get_sha1_encode(implode(':',$data));

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_data(){

        $r=FaceData::get_face_data_from_face_data_ID(self::$face_data_ID);

        if(empty($r)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face data is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        self::$name                     =$r['name'];
        self::$age                      =$r['age'];
        self::$city                     =$r['city'];
        self::$info                     =$r['info'];
        self::$source_account_link      =$r['source_account_link'];
        self::$face_data_type_ID        =$r['face_data_type_ID'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_list(){

        $image_ID_list=FaceImage::get_image_ID_list(self::$face_ID);

        if(count($image_ID_list)==0)
            return true;

        self::$image_list=array_values(ImagePathListAction::get_image_path_list($image_ID_list,true,true,true));

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'hash'          =>self::$face_search_pdf_token,
                'pdf_path'      =>self::$pdf_file_path
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Mpdf\Src\MpdfException
     */
    private static function set(){

        if(self::set_face_search_ID())
            if(self::set_face_percent())
                if(self::set_face_ID())
                    if(self::set_face_search_item_ID())
                        if(self::set_face_data_ID()){

                            self::set_face_search_pdf_ID();
                            self::set_face_search_pdf_token();
                            self::add_face_search_pdf();

                            self::set_face_data();
                            self::set_face_search_data();
                            self::set_image_list();

                            self::create_pdf_path();

                            self::create_pdf_content();

                            self::create_pdf();

                            self::update_face_search_pdf_to_public();

                            return self::set_return();

                        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \Core\Module\Mpdf\Src\MpdfException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['face_token']))
            $error_info_list[]='Face token is empty';

        if(empty($_POST['face_search_token']))
            $error_info_list[]='Face search token is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$face_token           =$_POST['face_token'];
        self::$face_search_token    =$_POST['face_search_token'];

        return self::set();

    }

}