<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceVideo;

class SaveFaceVideoListApi{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $video_ID;

    /** @var array */
    private static $video_ID_list=[];

    /** @var int */
    private static $video_len;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_ID(){

        if(!Face::isset_face_ID(self::$face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is not exists'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_video_ID_list(){

        if(!FaceVideo::remove_face_video_except_video_ID_list(self::$face_ID,self::$video_ID_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face video was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_video_len(){

        self::$video_len=FaceVideo::get_video_len(self::$face_ID);

        if(!FaceData::update_face_data_video_len(self::$face_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_except_video_ID_list(){

//        $except_video_ID_list=FaceVideo::get_except_video_ID_list(self::$face_ID,self::$video_ID_list);

//        if(count($except_video_ID_list)==0)
//            return true;

        self::remove_video_ID_list();

//        $video_list=VideoPathListAction::get_video_path_list($except_video_ID_list,true,true,true);
//
//        foreach($video_list as $video_ID=>$video_row){
//
//            $dir_path       =$video_row['video_dir'];
//            $dir_list       =mb_split('\/',$dir_path);
//
//            if(count($dir_list)==6)
//                if(!Dir::remove_dir($dir_path)){
//
//                    $error=[
//                        'title'     =>PhpException::$title,
//                        'info'      =>'Dir was not remove'
//                    ];
//
//                    throw new PhpException($error);
//
//                }
//
//        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_video_ID_list(){

        self::$video_ID_list=FaceVideo::get_video_ID_list_from_video_ID_list(self::$face_ID,self::$video_ID_list);

        if(count(self::$video_ID_list)==0)
            return false;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'video_len'=>self::$video_len
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_face_ID()){

            self::set_video_ID_list();
            self::set_except_video_ID_list();

            self::set_video_len();

            return self::set_return();

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['face_ID']))
            $error_info_list[]='Face ID is empty';

        if(empty($_POST['video_ID_list']))
            $error_info_list[]='Video ID list is empty';
        else if(!Json::is_json($_POST['video_ID_list']))
            $error_info_list[]='Video ID list is not JSON';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$face_ID          =(int)$_POST['face_ID'];
        self::$video_ID         =empty($_POST['video_ID'])?NULL:(int)$_POST['video_ID'];
        self::$video_ID_list    =Json::decode($_POST['video_ID_list']);

        return self::set();

    }

}