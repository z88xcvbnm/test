<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Encrypt\Hash;
use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Core\Module\OsServer\OsServer;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Sort\Sort;
use Core\Module\User\User;
use Core\Module\User\UserBalance;
use Core\Module\User\UserBalanceTransaction;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\Admin\Action\Image\ImagePathListAction;
use Project\Whore\Admin\Action\Video\VideoPathListAction;
use Project\Whore\All\Action\FacePlusPlus\SearchFaceTokenInFacesetPartsFacePlusPlusAction;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Face\FaceSearch;
use Project\Whore\All\Module\Face\FaceSearchItem;
use Project\Whore\All\Module\Face\FaceSearchMulti;
use Project\Whore\All\Module\Face\FaceSearchMultiItem;
use Project\Whore\All\Module\Face\FaceSearchMultiItemPart;
use Project\Whore\All\Module\Face\FaceTrigItem;
use Project\Whore\All\Module\Face\FaceVideo;
use Project\Whore\All\Module\Price\PriceConfig;
use Project\Whore\All\Module\Source\Source;

class GetFaceSearchComboResultWithFaceTriggerApi{

    /** @var int */
    private static $face_search_multi_ID;

    /** @var string */
    private static $face_search_multi_token;

    /** @var array */
    private static $source_rang_list        =[];

    /** @var array */
    private static $image_list=[];

    /** @var int */
    private static $face_ID;

    /** @var float */
    private static $percent;

    /** @var float */
    private static $percent_mode;

    /** @var int */
    private static $face_len_max            =6;

    /** @var int */
    private static $percent_min             =75;

    /** @var array */
    private static $face_trig_ID_list       =[];

    /** @var array */
    private static $face_trigger_list       =[];

    /** @var array */
    private static $source_list             =[];

    /** @var array */
    private static $source_image_list       =[];

    /** @var array */
    private static $face_ID_list            =[];

    /** @var array */
    private static $face_list               =[];

    /** @var bool */
    private static $is_test                 =false;

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_search_multi(){

        self::$face_search_multi_ID=FaceSearchMulti::get_face_search_multi_ID_with_user_ID(User::$user_ID,self::$face_search_multi_token,[0,2]);

        if(empty(self::$face_search_multi_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face search token is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_source_rang_list(){

        $r=Source::get_source_rang_list();

        if(count($r)==0)
            return false;

        foreach($r as $source_ID=>$row)
            self::$source_rang_list[$source_ID]=(int)$row['rang'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_source_image_list(){

        $image_ID_list=FaceSearch::get_image_ID_list(self::$face_search_multi_ID);

        if(count($image_ID_list)==0)
            return false;

        self::$source_list          =$image_ID_list;
        self::$source_image_list    =ImagePathListAction::get_image_path_list(array_values($image_ID_list),true,true,false);

        foreach($image_ID_list as $face_search_ID=>$image_ID)
            if(isset(self::$source_image_list[$image_ID]))
                self::$source_image_list[$image_ID]['face_search_ID']=$face_search_ID;

        foreach($image_ID_list as $image_ID)
            self::$image_list[$image_ID]=0;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_list(){

        $r=FaceSearchItem::get_face_search_item_list_from_face_search_multi_ID(self::$face_search_multi_ID);

        if(count($r)==0)
            return true;

        foreach($r as $row)
            self::$face_ID_list[]=$row['face_ID'];

        self::$face_trig_ID_list=FaceTrigItem::get_face_ID_list_from_face_ID_list(self::$face_ID_list);

        foreach(self::$face_trig_ID_list as $face_trig_ID=>$face_ID_list)
            foreach($face_ID_list as $face_ID)
                self::$face_trigger_list[$face_ID]=$face_trig_ID;

        $list=[];

        foreach($r as $row){

//            self::$face_ID_list[]=$row['face_ID'];

            $face_ID        =$row['face_ID'];
            $face_trig_ID   =empty(self::$face_trigger_list[$face_ID])?NULL:self::$face_trigger_list[$face_ID];

            if(!isset($list[$row['face_ID']])){

                $list[$row['face_ID']]=[
                    'face_search_ID'        =>$row['face_search_ID'],
                    'face_search_item_ID'   =>$row['face_search_item_ID'],
                    'face_image_ID'         =>$row['face_image_ID'],
                    'face_ID'               =>$row['face_ID'],
                    'face_trig_ID'          =>$face_trig_ID,
                    'face_ID_list'          =>[
                        $row['face_ID']
                    ],
                    'image_ID'              =>$row['image_ID'],
                    'percent'               =>ceil($row['percent']),
                    'percent_mode'          =>0,
                    'source_image_list'     =>self::$image_list,
                    'image_list'            =>[]
                ];

            }
            else{

                $percent=(int)$row['percent'];

                if((int)$list[$row['face_ID']]['percent']<(int)$percent){

                    $list[$row['face_ID']]['face_search_ID']        =$row['face_search_ID'];
                    $list[$row['face_ID']]['face_search_item_ID']   =$row['face_search_item_ID'];
                    $list[$row['face_ID']]['face_image_ID']         =$row['face_image_ID'];
                    $list[$row['face_ID']]['image_ID']              =$row['image_ID'];
                    $list[$row['face_ID']]['percent']               =$percent;

                }

            }

            $percent_cof=0;

            if($row['percent']<=8500)
                $percent_cof=.5;
            else if(
                  $row['percent']>8500
                &&$row['percent']<=9000
            )
                $percent_cof=1;
            else if(
                  $row['percent']>9000
                &&$row['percent']<=9500
            )
                $percent_cof=1.5;
            else if($row['percent']>9500)
                $percent_cof=2;

            $percent_mode=$percent_cof*$row['percent'];

            $list[$row['face_ID']]['image_list'][]=[
                'face_search_ID'        =>$row['face_search_ID'],
                'face_search_item_ID'   =>$row['face_search_item_ID'],
                'face_image_ID'         =>$row['face_image_ID'],
                'image_ID'              =>$row['image_ID'],
                'percent'               =>$row['percent'],
                'percent_mode'          =>$percent_mode
            ];

            $list[$row['face_ID']]['percent_mode']                                                      +=$percent_mode;
            $list[$row['face_ID']]['source_image_list'][self::$source_list[$row['face_search_ID']]]     +=$percent_mode;

        }

        $face_trig_list=[];

        foreach($list as $face_ID=>$row){

            $face_trig_ID=$row['face_trig_ID'];

            if(!isset($face_trig_list[$face_trig_ID]))
                $face_trig_list[$face_trig_ID]=$row;
            else
                $face_trig_list[$face_trig_ID]['face_ID_list'][]=$row['face_ID'];

        }

        $list=[];

        foreach($face_trig_list as $row)
            $list[$row['face_ID']]=$row;

        self::$face_list    =Sort::sort_with_key('percent',$list,'desc');
        self::$face_list    =array_slice(self::$face_list,0,5);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_search_multi(){

        if(count(self::$face_list)==0)
            return true;

        foreach(self::$face_list as $index=>$row){

//            self::$face_ID_list[]=$row['face_ID'];

            arsort($row['source_image_list']);

            $source_image_ID=key($row['source_image_list']);

            if($index==0)
                FaceSearchMulti::update_result(self::$face_search_multi_ID,$source_image_ID,$row['face_ID'],$row['image_ID'],$row['source_image_list'],$row['percent'],$row['percent_mode']);

            $source_image_ID=NULL;

            if(count($row['source_image_list'])>0)
                $source_image_ID=key($row['source_image_list']);

            $data=[
                User::$user_ID,
                self::$face_search_multi_ID,
                self::$face_search_multi_token,
                $row['face_ID'],
                time(),
                rand(0,time())
            ];

            self::$face_list[$index]['image']                       =ImagePathAction::get_image_path_data($row['image_ID'],true,true,false);
            self::$face_list[$index]['token']                       =Hash::get_sha1_encode(implode(':',$data));
            self::$face_list[$index]['face_search_multi_item_ID']   =FaceSearchMultiItem::add_face_search_multi_item(self::$face_search_multi_ID,$row['face_search_ID'],$row['face_search_item_ID'],$source_image_ID,$row['face_ID'],$row['image_ID'],$row['face_image_ID'],$row['percent'],$row['percent_mode'],count($row['image_list']),self::$face_list[$index]['token']);

            foreach($row['image_list'] as $image_row){

                $source_image_ID=self::$source_list[$image_row['face_search_ID']];

                FaceSearchMultiItemPart::add_face_search_multi_item_part(self::$face_search_multi_ID,self::$face_list[$index]['face_search_multi_item_ID'],$image_row['face_search_ID'],$image_row['face_search_item_ID'],$source_image_ID,$row['face_ID'],$image_row['image_ID'],$image_row['face_image_ID'],$image_row['percent'],$image_row['percent_mode']);

            }

        }

        return true;

    }

    /**
     * @param string|NULL $info
     * @return array
     */
    private static function get_info_list(string $info=NULL){

        if(empty($info))
            return[];

        $row_list=mb_split("\n",strip_tags($info));

        if(count($row_list)==0)
            return[];

        $list=[];

        foreach($row_list as $row){

            $is_added       =false;
            $label_list     =mb_split('\:',$row);
            $label          ='';
            $value          ='';

            if(count($label_list)===2){

                if(trim($label_list[0])=='Other')
                    break;

                if(!$is_added){

                    $label      =$label_list[0];
                    $value      =$label_list[1];

                    switch($label){

                        case'Цели знакомства':{

                            $is_added=false;

                            break;
                        }

                        default:{

                            $is_added=true;

                            break;

                        }

                    }

                }

            }
            else if(!empty(trim($label_list[0]))){

                $label      ='О себе';
                $value      =trim($label_list[0]);

                $is_added=true;

            }

            if($is_added){

                $body=mb_split('\/',$value);

                if(count($body)==3)
                    $value=$body[0].'/'.$body[1].'/'.$body[2];

                $body=mb_split('\-',$value);

                if(count($body)==3)
                    $value=$body[0].'/'.$body[1].'/'.$body[2];

                if(isset($list[$label]))
                    $list[$label]['value'].="\n".$value;
                else
                    $list[$label]=[
                        'label'     =>$label,
                        'value'     =>$value
                    ];

            }

        }

        return array_values($list);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_data(){

//        self::$face_ID_list=array_slice(self::$face_ID_list,0,5);

        self::$face_ID_list=[];

        foreach(self::$face_list as $row)
            self::$face_ID_list[]=$row['face_ID'];

        self::$face_ID_list=array_slice(self::$face_ID_list,0,5);

        $r=FaceData::get_face_data_list_from_face_ID_list(self::$face_ID_list);

        if(count($r)==0)
            return true;

        foreach(self::$face_list as $index=>$row){

            $face_ID=$row['face_ID'];

            if(isset($r[$face_ID])){

                self::$face_list[$index]['rang']                        =$r[$face_ID]['face_data_type_ID'];
                self::$face_list[$index]['name']                        =$r[$face_ID]['name'];
                self::$face_list[$index]['age']                         =$r[$face_ID]['age'];
                self::$face_list[$index]['city']                        =$r[$face_ID]['city'];
                self::$face_list[$index]['info']                        =empty($r[$face_ID]['info'])?[]:self::get_info_list($r[$face_ID]['info']);
                self::$face_list[$index]['source_account_link']         =$r[$face_ID]['source_account_link'];
                self::$face_list[$index]['source_account_link_list']    =FaceData::get_face_source_link_list_from_face_ID_list(self::$face_list[$index]['face_ID_list']);

            }

        }

        return true;

    }/**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_total_image_list(){

        if(count(self::$face_list)==0)
            return false;

        $face_ID_list=[];

        foreach(self::$face_list as $row)
            $face_ID_list[]=$row['face_ID'];

        $image_len_list=FaceImage::get_image_len_list($face_ID_list);

        foreach($image_len_list as $face_ID=>$len)
            foreach(self::$face_list as $index=>$row)
                if($face_ID==$row['face_ID'])
                    self::$face_list[$index]['image_len']=$len;

//        foreach(self::$face_list as $index=>$row){
//
//            self::$face_list[$index]['image_len']=FaceTrigItem::get
//
//
//        }

        self::$face_list    =array_values(self::$face_list);
        $face_ID            =self::$face_list[0]['face_ID'];

        if(empty(self::$face_list[0]['face_trig_ID']))
            $image_ID_list=FaceImage::get_image_ID_list($face_ID);
        else{

            $face_trig_ID       =self::$face_list[0]['face_trig_ID'];
            $image_ID           =self::$face_list[0]['image_ID'];
            $face_ID_list       =FaceTrigItem::get_face_ID_list_from_face_trig_ID($face_trig_ID);
            $image_ID_list      =FaceImage::get_unical_image_ID_list_from_face_ID_list($face_ID_list);

            if(array_search($image_ID,$image_ID_list)===false)
                $image_ID_list[]=$image_ID;

        }

        self::$face_list[0]['image_list']       =array_values(ImagePathListAction::get_image_path_list($image_ID_list,true,true));

        $video_ID_list                          =FaceVideo::get_video_ID_list($face_ID);

        if(count($video_ID_list)>0)
            self::$face_list[0]['video_list']=array_values(VideoPathListAction::get_video_path_list($video_ID_list,true,true));
        else
            self::$face_list[0]['video_list']=[];

        return true;

    }

    /**
     * @return bool
     */
    private static function set_percent(){

        self::$percent=0;

        if(count(self::$face_list)==0)
            return true;

        foreach(self::$face_list as $row)
            if(self::$percent<$row['percent']){

                self::$face_ID          =$row['face_ID'];
                self::$percent          =$row['percent'];
                self::$percent_mode     =$row['percent_mode'];

            }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'           =>true,
            'data'              =>[
                'search_token'      =>self::$face_search_multi_token,
                'face_list'         =>array_values(self::$face_list)
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_face_search_multi()){

            self::set_source_rang_list();
            self::set_source_image_list();

            self::set_list();
            self::prepare_face_search_multi();

            self::set_face_data();
            self::set_face_total_image_list();
            self::set_percent();

            return self::set_return();

        }

        return false;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['face_search_multi_token']))
            $error_info_list[]='File search multi token is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$is_test                      =!empty($_POST['is_test']);
        self::$face_search_multi_token      =$_POST['face_search_multi_token'];

        return self::set();

    }

}