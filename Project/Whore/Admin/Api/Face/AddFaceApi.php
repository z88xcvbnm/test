<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Date\Date;
use Core\Module\Dir\Dir;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Geo\CityLocalization;
use Core\Module\Image\Image;
use Core\Module\Image\ImageConvert;
use Core\Module\Json\Json;
use Core\Module\Lang\LangConfig;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\All\Action\FaceKit\UploadImageFaceKitAction;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Source\Source;

class AddFaceApi{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $file_ID;
    
    /** @var int */
    private static $image_ID;

    /** @var int */
    private static $face_data_ID;

    /** @var int */
    private static $face_image_ID;

    /** @var int */
    private static $facekit_image_ID;

    /** @var int */
    private static $source_ID;

    /** @var int */
    private static $city_ID;

    /** @var string */
    private static $name;

    /** @var int */
    private static $age;

    /** @var string */
    private static $city_name;

    /** @var string */
    private static $info;

    /** @var string */
    private static $source_account_link;
    
    /** @var array */
    private static $coords;

    /** @var string */
    private static $image_path;

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_source_ID(){

        if(!Source::isset_source_ID(self::$source_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Source ID is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_ID(){
        
        if(!Image::isset_image_ID(self::$image_ID)){
            
            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image ID is empty'
            ];

            throw new ParametersValidationException($error);
            
        }
        
        return true;
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_coords(){

        $error_info_list=[];

        if(!isset(self::$coords['x']))
            $error_info_list[]='Coords OX is not exists';

        if(!isset(self::$coords['y']))
            $error_info_list[]='Coords OY is not exists';

        if(!isset(self::$coords['w']))
            $error_info_list[]='Width is not exists';

        if(!isset(self::$coords['h']))
            $error_info_list[]='Height is not exists';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function upload_to_facekit(){
        
        $r=UploadImageFaceKitAction::init(self::$face_ID,self::$image_path);
        
        if(empty($r)){
            
            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Kit error'
            ];

            throw new PhpException($error);
            
        }
        
        self::$facekit_image_ID=$r['facekit_image_ID'];

//        if(file_exists(self::$image_path))
//            unlink(self::$image_path);
        
        return true;
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face(){

        self::$face_ID=Face::add_face();

        if(empty(self::$face_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_image(){

        self::$face_image_ID=FaceImage::add_face_image(self::$face_ID,self::$file_ID,self::$image_ID,self::$facekit_image_ID,NULL,self::$coords);

        if(empty(self::$face_image_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face image was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_data(){

        self::$face_data_ID=FaceData::add_face_data(self::$face_ID,self::$city_ID,self::$image_ID,self::$source_ID,self::$source_account_link,self::$name,self::$age,self::$city_name,self::$info,1,1);

        if(empty(self::$face_data_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face(){

        if(!Face::update_face(self::$face_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set_face_image(){

        $image_object=ImagePathAction::get_image_path_data(self::$image_ID,true,true);

        if(empty($image_object)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Image is not exists'
            ];

            throw new ParametersException($error);

        }

        $image_path     =$image_object['image_dir'].'/'.$image_object['image_item_ID_list']['large']['ID'];
        $image_result   =ImageConvert::crop_image_from_file_path($image_path,self::$coords['x'],self::$coords['y'],self::$coords['w'],self::$coords['h'],self::$coords['w'],self::$coords['h']);

        return ImageConvert::save_image($image_result,self::$image_path,'jpg');

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_city_ID(){

        self::$city_ID=CityLocalization::get_city_ID(self::$city_name,LangConfig::$lang_ID_default);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_path(){

        if(!file_exists(DirConfigProject::$dir_face_temp))
            Dir::create_dir(DirConfigProject::$dir_face_temp);

        self::$image_path=DirConfigProject::$dir_face_temp.'/'.self::$image_ID.'.jpg';

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $link_number_list=empty(self::$source_account_link)?NULL:mb_split('\/',self::$source_account_link);
        
        $data=[
            'success'   =>true,
            'data'      =>[
                'ID'                    =>self::$face_ID,
                'image_ID'              =>self::$image_ID,
                'image'                 =>ImagePathAction::get_image_path_data(self::$image_ID,true,true),
                'image_len'             =>1,
                'image_facekit_len'     =>1,
                'city_ID'               =>self::$city_ID,
                'source_ID'             =>self::$source_ID,
                'city'                  =>self::$city_name,
                'source_account_link'   =>self::$source_account_link,
                'source_link_number'    =>empty($link_number_list)?NULL:end($link_number_list),
                'name'                  =>self::$name,
                'age'                   =>self::$age,
                'info'                  =>self::$info,
                'date_create'           =>Date::get_timestamp()
            ]
        ];

        return ResponseSuccess::init($data);
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        if(self::is_valid_coords())
            if(self::isset_source_ID())
                if(self::isset_image_ID()){

//                    self::set_city_ID();
                    self::set_image_path();
                    self::add_face();

                    self::set_face_image();
                    self::upload_to_facekit();

                    self::update_face();
                    self::add_face_image();
                    self::add_face_data();

                    return self::set_return();

                }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['image_ID']))
            $error_info_list[]='Image ID is empty';

        if(empty($_POST['name']))
            $error_info_list[]='Name is empty';

        if(empty($_POST['source_ID']))
            $error_info_list[]='Source ID is empty';

        if(empty($_POST['city_name']))
            $error_info_list[]='City is empty';

        if(empty($_POST['coords']))
            $error_info_list[]='Coords are empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$file_ID                  =empty($_POST['file_ID'])?NULL:$_POST['file_ID'];
        self::$image_ID                 =(int)$_POST['image_ID'];
        self::$coords                   =Json::decode($_POST['coords']);
        self::$source_ID                =(int)$_POST['source_ID'];
        self::$city_name                =$_POST['city_name'];
        self::$name                     =$_POST['name'];
        self::$source_account_link      =empty($_POST['source_account_link'])?NULL:$_POST['source_account_link'];
        self::$age                      =empty($_POST['age'])?NULL:$_POST['age'];
        self::$info                     =empty($_POST['note'])?NULL:$_POST['note'];

        return self::set();

    }
    
}