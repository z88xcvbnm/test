<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Exception\PhpException;
use Core\Module\Geo\City;
use Core\Module\Geo\CityLocalization;
use Core\Module\Geo\CountryLocalization;
use Core\Module\Lang\LangConfig;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\All\Module\Face\FaceData;

class UpdateCountryAndCityApi{

    /** @var int */
    private static $len=500;

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_data(){

        $n=0;

        do{

            $face_data_list=FaceData::get_face_data_with_empty_city_ID(self::$len);

            if(count($face_data_list)==0)
                return true;

            foreach($face_data_list as $row){

                $face_ID            =$row['face_ID'];
                $face_data_ID       =$row['ID'];
                $country_ID         =NULL;
                $city_ID            =NULL;
                $country            =NULL;
                $city_source        =$row['city'];
                $city               =$row['city'];

                if(!empty($city)){

                    $city_list=mb_split('\,',$city);

                    if(count($city_list)==1){

                        $country_ID=CountryLocalization::get_country_ID_from_name(mb_strtolower(trim($city_list[0]),'utf-8'));

                        if(empty($country_ID)){

                            $city_ID=CityLocalization::get_city_ID_from_name(mb_strtolower(trim($city_list[0]),'utf-8'));

                            if(!empty($city_ID))
                                $country_ID=City::get_country_ID($city_ID);

                        }
                        else{

                            $city_ID=CityLocalization::get_city_ID_from_name(mb_strtolower(trim($city_list[0]),'utf-8'));

                            if(!empty($city_ID))
                                $country_ID=City::get_country_ID($city_ID);

                        }

                    }
                    else{

                        $country        =mb_strtolower(trim($city_list[0]),'utf-8');
                        $city           =mb_strtolower(trim(end($city_list)),'utf-8');

                        switch($city){

                            case'spb':
                            case'спб':
                            case'saint petersburg':
                            case'st.petersburg':
                            case'st. petersburg':
                            case'st petersburg':{

                                $city='Санкт-Петербург';

                                break;

                            }

                            case'мск':
                            case'msk':{

                                $city='Москва';

                                break;

                            }

                        }

                        if(!empty($country))
                            $country_ID=CountryLocalization::get_country_ID_from_name(mb_strtolower($country,'utf-8'));

                        if(!empty($city))
                            $city_ID=CityLocalization::get_city_ID_from_name(mb_strtolower($city,'utf-8'));

                        if(empty($country_ID)&&!empty($city_ID))
                            $country_ID=City::get_country_ID($city_ID);

                    }

                }

                if(!empty($city_ID))
                    $city=CityLocalization::get_city_name($city_ID,LangConfig::$lang_ID_default);
                else
                    $city=NULL;

                if(!empty($country_ID))
                    $country=CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);
                else
                    $country=NULL;

                $city_result=$country.(empty($city)?'':(', '.$city));

                $data=[
                    'face_ID'           =>$face_ID,
                    'face_data_ID'      =>$face_data_ID,
                    'country_ID'        =>$country_ID,
                    'city_ID'           =>$city_ID,
                    'country'           =>$country,
                    'city'              =>$city,
                    'city_result'       =>$city_result,
                    'city_source'       =>$city_source
                ];

                if(
                      !empty($data['country_ID'])
                    &&!empty($data['city_ID'])
                ){

                    if(!FaceData::update_face_data_city_and_country($data['face_ID'],$data['country_ID'],$data['city_ID'],$data['city_result'])){

                        $error=[
                            'title'     =>PhpException::$title,
                            'info'      =>'Face data was not update'
                        ];

                        throw new PhpException($error);

                    }

                }
                else if(!empty($data['country_ID'])){

                    if(!FaceData::update_face_data_city_and_country($data['face_ID'],$data['country_ID'],$data['city_ID'],$data['city_source'])){

                        $error=[
                            'title'     =>PhpException::$title,
                            'info'      =>'Face data was not update'
                        ];

                        throw new PhpException($error);

                    }

                }
                else{

                    if(!FaceData::update_face_data_city_and_country($data['face_ID'],$data['country_ID'],$data['city_ID'],$data['city_source'])){

                        $error=[
                            'title'     =>PhpException::$title,
                            'info'      =>'Face data was not update'
                        ];

                        throw new PhpException($error);

                    }

                }

            }

        }while(true);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::prepare_face_data();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}