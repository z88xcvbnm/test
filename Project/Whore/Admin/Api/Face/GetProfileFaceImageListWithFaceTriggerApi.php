<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\Admin\Action\Image\ImagePathListAction;
use Project\Whore\Admin\Action\Video\VideoPathListAction;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Face\FaceSearch;
use Project\Whore\All\Module\Face\FaceSearchMulti;
use Project\Whore\All\Module\Face\FaceTrigItem;
use Project\Whore\All\Module\Face\FaceVideo;

class GetProfileFaceImageListWithFaceTriggerApi{

    /** @var int */
    private static $face_search_ID;

    /** @var int */
    private static $face_ID;

    /** @var array */
    private static $image_list      =[];

    /** @var array */
    private static $video_list      =[];

    /** @var bool */
    private static $is_test         =false;

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_search_multi_with_user(){

        if(!FaceSearchMulti::isset_face_search_multi_ID_with_user_ID(self::$face_search_ID,User::$user_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face search token is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

//    /**
//     * @return bool
//     * @throws ParametersValidationException
//     * @throws \Core\Module\Exception\DbParametersException
//     * @throws \Core\Module\Exception\DbQueryException
//     * @throws \Core\Module\Exception\DbQueryParametersException
//     * @throws \Core\Module\Exception\DbValidationValueException
//     * @throws \Core\Module\Exception\ParametersException
//     * @throws \Core\Module\Exception\PhpException
//     * @throws \Core\Module\Exception\SystemException
//     */
//    private static function isset_face_search_with_user(){
//
//        if(!FaceSearch::isset_face_search_with_user(self::$face_search_ID,User::$user_ID)){
//
//            $error=[
//                'title'     =>ParametersValidationException::$title,
//                'info'      =>'Face ID is not exists'
//            ];
//
//            throw new ParametersValidationException($error);
//
//        }
//
//        return true;
//
//    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_ID(){

        if(!Face::isset_face_ID(self::$face_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_list(){

        $face_trig_ID=FaceTrigItem::get_face_trig_ID(self::$face_ID);

        if(empty($face_trig_ID)){

            $image_ID_list=FaceImage::get_image_ID_list(self::$face_ID);

            if(count($image_ID_list)==0)
                return true;

        }
        else{

            $image_ID       =FaceData::get_image_ID(self::$face_ID);
            $face_ID_list   =FaceTrigItem::get_face_ID_list_from_face_trig_ID($face_trig_ID);
            $image_ID_list  =FaceImage::get_unical_image_ID_list_from_face_ID_list($face_ID_list);

//            echo"\n\n";
//            echo $image_ID."\n";

            if(!empty($image_ID))
                if(array_search($image_ID,$image_ID_list)===false)
                    $image_ID_list[]=$image_ID;

//            print_r($image_ID_list);
//            echo"\n\n";


        }

        self::$image_list=ImagePathListAction::get_image_path_list($image_ID_list,true,true);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_video_list(){

        $r=FaceVideo::get_video_ID_list(self::$face_ID);

        if(count($r)==0)
            return true;

        self::$video_list=VideoPathListAction::get_video_path_list($r,true,true);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'video_list'    =>array_values(self::$video_list),
                'image_list'    =>array_values(self::$image_list),
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_face_search_multi_with_user())
            if(self::isset_face_ID()){

                self::set_face_image_list();
                self::set_face_video_list();

                return self::set_return();

            }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['search_token']))
            $error_info_list[]='Search token is empty';

        if(empty($_POST['face_ID']))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$is_test              =!empty($_POST['is_test']);
        self::$face_search_ID       =$_POST['search_token'];
        self::$face_ID              =(int)$_POST['face_ID'];

        return self::set();

    }

}