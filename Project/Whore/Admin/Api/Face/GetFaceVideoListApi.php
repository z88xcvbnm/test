<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Video\VideoPathListAction;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceVideo;

class GetFaceVideoListApi{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $video_ID;

    /** @var array */
    private static $video_list=[];

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_ID(){

        if(!Face::isset_face_ID(self::$face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is not exists'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_video_list(){

        $r=FaceVideo::get_video_data_list(self::$face_ID);

        if(empty($r))
            return true;

        $video_ID_list=[];

        foreach($r as $row){

            $video_ID=$row['video_ID'];

            if(!empty($video_ID))
                self::$video_list[$video_ID]=$row;

        }

        if(count(self::$video_list)==0)
            return true;

        $video_list=VideoPathListAction::get_video_path_list(array_keys(self::$video_list),true,true,false);

//        print_r($video_list);exit;

        if(count($video_list)==0)
            return true;

        foreach($video_list as $video_ID=>$video_object)
            if(isset(self::$video_list[$video_ID]))
                self::$video_list[$video_ID]['video']=$video_object;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'video_list'    =>array_values(self::$video_list)
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_face_ID()){

            self::set_video_list();

            return self::set_return();

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(empty($_POST['face_ID'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        self::$face_ID=(int)$_POST['face_ID'];

        return self::set();

    }

}