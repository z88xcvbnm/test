<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Image\ImagePathListAction;
use Project\Whore\All\Module\Face\FaceData;

class GetFaceListApi{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $start           =0;

    /** @var int */
    private static $len             =30;

    /** @var array */
    private static $face_list       =[];

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_list(){

        if(!empty($_POST['face_len']))
            self::$start=(int)$_POST['face_len'];

        $r=FaceData::get_face_data_list(self::$face_ID,self::$start,self::$len);

        if(count($r)==0)
            return true;

        $image_ID_list=[];

        foreach($r as $row){

            $face_ID                =$row['face_ID'];
            $source_link_number     =NULL;

            if(!empty($row['image_ID']))
                $image_ID_list[$face_ID]=$row['image_ID'];

            $link_number_list=empty($row['source_account_link'])?NULL:mb_split('\/',$row['source_account_link']);

            if(empty($source_link_number))
                $source_link_number=$link_number_list[count($link_number_list)-1];

            self::$face_list[$face_ID]=[
                'ID'                        =>$face_ID,
                'city_ID'                   =>$row['city_ID'],
                'image_ID'                  =>$row['image_ID'],
                'image'                     =>NULL,
                'source_ID'                 =>$row['source_ID'],
                'face_data_type_ID'         =>$row['face_data_type_ID'],
                'link_ID'                   =>$row['link_ID'],
                'source_account_link'       =>$row['source_account_link'],
                'source_link_number'        =>empty($source_link_number)?NULL:$source_link_number,
                'name'                      =>$row['name'],
                'age'                       =>$row['age'],
                'city'                      =>$row['city'],
                'info'                      =>$row['info'],
                'info_dev'                  =>$row['info_dev'],
                'image_len'                 =>$row['image_len'],
                'image_facekit_len'         =>$row['image_facekit_len'],
                'video_len'                 =>$row['video_len'],
                'date_create'               =>$row['date_create']
            ];

        }

        if(count($image_ID_list)==0)
            return true;

        $image_path_list=ImagePathListAction::get_image_path_list(array_values($image_ID_list),true,true);

        if(count($image_path_list)==0)
            return true;

        foreach(self::$face_list as $face_ID=>$face_row)
            if(isset($image_path_list[$face_row['image_ID']]))
                self::$face_list[$face_ID]['image']=$image_path_list[$face_row['image_ID']];

        foreach(self::$face_list as $face_ID=>$face_row)
            if(empty(self::$face_list[$face_ID]['image_ID']))
                unset(self::$face_list[$face_ID]);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'face_list'     =>array_values(self::$face_list)
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_face_list();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::$face_ID=empty($_POST['face_ID'])?NULL:(int)$_POST['face_ID'];

        return self::set();

    }

}