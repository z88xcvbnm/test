<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\All\Module\Face\FaceData;

class IssetFaceSourceLinkApi{

    /** @var int */
    private static $face_ID;

    /** @var string */
    private static $source_account_link;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_source_link(){

        return FaceData::isset_source_account_link(self::$source_account_link,self::$face_ID);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'                       =>true,
            'isset_source_account_link'     =>self::isset_face_source_link()
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(empty($_POST['source_account_link'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source account link is empty'
            ];

            throw new ParametersException($error);

        }

        self::$face_ID                  =empty($_POST['face_ID'])?NULL:(int)$_POST['face_ID'];
        self::$source_account_link      =trim($_POST['source_account_link']);

        return self::set();

    }

}