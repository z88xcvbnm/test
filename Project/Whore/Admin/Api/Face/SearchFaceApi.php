<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Image\Image;
use Core\Module\Image\ImageFace;
use Core\Module\Json\Json;
use Core\Module\OsServer\OsServer;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Sort\Sort;
use Core\Module\User\User;
use Core\Module\User\UserBalance;
use Project\Whore\Admin\Action\Image\ImagePathListAction;
use Project\Whore\All\Action\FacePlusPlus\SearchFaceTokenInFacesetFacePlusPlusAction;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Price\PriceConfig;

class SearchFaceApi{

    /** @var int */
    private static $face_search_ID;

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $image_ID;

    /** @var string */
    private static $face_token;

    /** @var int */
    private static $face_len_max            =6;

    /** @var int */
    private static $percent_min             =75;

    /** @var array */
    private static $face_coords_list        =[];

    /** @var array */
    private static $face_ID_list            =[];

    /** @var array */
    private static $face_list               =[];

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image(){

        if(!Image::isset_image_ID(self::$image_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_face_token(){

        if(!ImageFace::isset_image_face(self::$image_ID,self::$face_token)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face token is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_user_balance(){

        $balance=(int)UserBalance::get_user_balance(User::$user_ID);

        if($balance<PriceConfig::$search_price){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Your balance is low'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_list(){

        $r=SearchFaceTokenInFacesetFacePlusPlusAction::init(self::$file_ID,self::$image_ID,self::$face_token,self::$face_coords_list,self::$face_len_max);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Problem with search'
            ];

            throw new PhpException($error);

        }

        self::$face_search_ID       =$r['face_search_ID'];
        self::$face_list            =Sort::sort_with_key('percent',$r['face_token_list'],'desc');

        return true;

    }

    /**
     * @return bool
     */
    private static function set_face_ID_list(){

        if(count(self::$face_list)==0)
            return true;

        foreach(self::$face_list as $row)
            self::$face_ID_list[]=$row['face_ID'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_list(){

        $r=FaceImage::get_image_ID_list_from_face_ID_list(self::$face_ID_list);

        if(count($r)==0)
            return true;

        $image_list=ImagePathListAction::get_image_path_list(array_keys($r),true,true);

        if(count($image_list)==0)
            return true;

        foreach(self::$face_list as $index=>$face_row)
            if(isset($image_list[$face_row['image_ID']]))
                self::$face_list[$index]['image']=$image_list[$face_row['image_ID']];

        foreach(self::$face_list as $index=>$face_row)
            if(!isset($face_row['image']))
                unset(self::$face_list[$index]);

        if(count(self::$face_list)==0)
            return true;

        $list=[];

        foreach(self::$face_list as $row){

            $face_ID=$row['face_ID'];

            if(!isset($list[$face_ID]))
                $list[$face_ID]=[
                    'face_ID'           =>$face_ID,
                    'find_len'          =>0,
                    'percent'           =>0,
                    'image_percent'     =>[],
                    'image_len'         =>0,
                    'image'             =>$row['image']
                ];

            if(floatval($list[$face_ID]['percent'])<$row['percent'])
                $list[$face_ID]['percent']=$row['percent'];

            $list[$face_ID]['find_len']++;
            $list[$face_ID]['image_percent'][$row['image_ID']]=$row['percent'];

        }

        self::$face_list    =Sort::sort_with_key('percent',$list,'desc');
        $list               =[];
        $face_index         =0;

        foreach(self::$face_list as $index=>$row){

            $percent=round(self::$face_list[$index]['percent']/100,2);

            self::$face_list[$index]['percent']=$percent;

        }

        foreach(self::$face_list as $row)
            if($row['percent']>=self::$percent_min){

                $list[]=$row;

                $face_index++;

                if($face_index>=self::$face_len_max)
                    break;

            }

        self::$face_list=$list;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_total_image_list(){

        if(count(self::$face_list)==0)
            return false;

        $face_ID_list=[];

        foreach(self::$face_list as $row)
            $face_ID_list[]=$row['face_ID'];

        $image_len_list=FaceImage::get_image_len_list($face_ID_list);

        foreach($image_len_list as $face_ID=>$len)
            foreach(self::$face_list as $index=>$row)
                if($face_ID==$row['face_ID'])
                    self::$face_list[$index]['image_len']=$len;

        self::$face_list                        =array_values(self::$face_list);

        $face_ID                                =self::$face_list[0]['face_ID'];
        $image_ID_list                          =FaceImage::get_image_ID_list($face_ID);

        self::$face_list[0]['image_list']       =array_values(ImagePathListAction::get_image_path_list($image_ID_list,true,true));

        return true;

    }

    private static function set_face_image_len(){



    }

    /**
     * @param string|NULL $info
     * @return array
     */
    private static function get_info_list(string $info=NULL){

        if(empty($info))
            return[];

        $row_list=mb_split("\n",$info);

        if(count($row_list)==0)
            return[];

        $list=[];

        foreach($row_list as $row){

            $is_added       =false;
            $label_list     =mb_split('\:',$row);
            $label          ='';
            $value          ='';

            if(count($label_list)===2){

                if(!$is_added){

                    $label      =$label_list[0];
                    $value      =$label_list[1];

                    $is_added=true;

                }

            }
            else{

                $label      ='О себе';
                $value      =$label_list[0];

                $is_added=true;

            }

            if($is_added){

                $body=mb_split('\/',$value);

                if(count($body)==3)
                    $value=$body[0].'/'.$body[1].'/'.$body[2];

                $body=mb_split('\-',$value);

                if(count($body)==3)
                    $value=$body[0].'/'.$body[1].'/'.$body[2];

                $list[]=[
                    'label'     =>$label,
                    'value'     =>$value
                ];

            }

        }

        return $list;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_data(){

        $r=FaceData::get_face_data_list_from_face_ID_list(self::$face_ID_list);

        if(count($r)==0)
            return true;

        foreach(self::$face_list as $index=>$row){

            $face_ID=$row['face_ID'];

            if(isset($r[$face_ID])){

                self::$face_list[$index]['name']                    =$r[$face_ID]['name'];
                self::$face_list[$index]['age']                     =$r[$face_ID]['age'];
                self::$face_list[$index]['city']                    =$r[$face_ID]['city'];
                self::$face_list[$index]['info']                    =empty($r[$face_ID]['info'])?[]:self::get_info_list($r[$face_ID]['info']);
                self::$face_list[$index]['source_account_link']     =$r[$face_ID]['source_account_link'];

            }

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'balance'       =>UserBalance::get_user_balance(User::$user_ID),
            'search_token'  =>self::$face_search_ID,
            'face_list'     =>array_values(self::$face_list)
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::check_user_balance())
            if(self::isset_image())
                if(self::isset_image_face_token()){

                    self::set_face_list();
                    self::set_face_ID_list();
                    self::set_face_image_list();
                    self::set_face_data();
                    self::set_face_total_image_list();

                    return self::set_return();

                }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(OsServer::$is_windows)
            \Config::$is_debug=true;

        $error_info_list=[];

        if(empty($_POST['file_ID']))
            $error_info_list[]='File ID is empty';

        if(empty($_POST['image_ID']))
            $error_info_list[]='Image ID is empty';

        if(empty($_POST['face_token']))
            $error_info_list[]='Face token is empty';

        if(empty($_POST['face_coords']))
            $error_info_list[]='Face coords is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$file_ID              =(int)$_POST['file_ID'];
        self::$image_ID             =(int)$_POST['image_ID'];
        self::$face_token           =$_POST['face_token'];
        self::$face_coords_list     =Json::decode($_POST['face_coords']);

        return self::set();

    }

}