<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Date\Date;
use Core\Module\Dir\Dir;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Geo\CityLocalization;
use Core\Module\Image\Image;
use Core\Module\Image\ImageConvert;
use Core\Module\Json\Json;
use Core\Module\Lang\LangConfig;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\All\Action\FacePlusPlus\AddFaceTokenToFacesetFacePlusPlusAction;
use Project\Whore\All\Action\FacePlusPlus\EditFaceTokenToFacesetFacePlusPlusAction;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Source\Source;

class EditImageToFacePlusPlusApi{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $faceset_ID;

    /** @var int */
    private static $face_image_ID;
    
    /** @var int */
    private static $image_ID;

    /** @var string */
    private static $face_token;

    /** @var string */
    private static $faceset_token;
    
    /** @var array */
    private static $coords;

    /** @var string */
    private static $image_path;

    /** @var int */
    private static $time_used;

    /** @var int */
    private static $image_len;

    /** @var int */
    private static $facekit_image_len;

    /** @var string */
    private static $file_log_path;

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_ID(){

        if(!Face::isset_face_ID(self::$face_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_file_log_path(){

        self::$file_log_path=DirConfigProject::$dir_parsing_log;

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/AddFaceToFacePlusPlus';

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/model_zone_'.time().'.log';

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_ID(){
        
        if(!Image::isset_image_ID(self::$image_ID)){
            
            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image ID is empty'
            ];

            throw new ParametersValidationException($error);
            
        }
        
        return true;
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_token_to_face_plus_plus(){

        $image_list=[
            [
                'file_ID'           =>self::$file_ID,
                'image_ID'          =>self::$image_ID,
                'coords'            =>self::$coords,
                'faceset_ID'        =>self::$faceset_ID,
                'face_token'        =>self::$face_token
            ]
        ];

        if(!empty(self::$file_log_path))
            file_put_contents(self::$file_log_path,"-> Image list: \n".print_r($image_list,true)."\n",FILE_APPEND);

        $r=EditFaceTokenToFacesetFacePlusPlusAction::init(self::$face_ID,$image_list,true,self::$file_log_path,true);

        if(empty($r)){
            
            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face Kit error'
            ];

            throw new PhpException($error);
            
        }

        self::$faceset_ID       =$r['faceset_ID'];
        self::$time_used        =$r['time_used'];
        
        return true;
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_data(){

        self::$image_len                =FaceImage::get_image_len(self::$face_ID);
        self::$facekit_image_len        =FaceImage::get_face_plus_plus_image_len(self::$face_ID);

        if(!FaceData::update_face_data_face_plus_plus_image_len(self::$face_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_path(){

        if(!file_exists(DirConfigProject::$dir_face_temp))
            Dir::create_dir(DirConfigProject::$dir_face_temp);

        self::$image_path=DirConfigProject::$dir_face_temp.'/'.self::$image_ID.'.jpg';

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_ID(){

        self::$face_image_ID=FaceImage::get_facekit_image_ID(self::$face_ID,self::$image_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){
        
        $data=[
            'success'       =>true,
            'time_used'     =>self::$time_used,
            'data'          =>[
                'ID'                    =>self::$face_image_ID,
                'faceset_ID'            =>self::$faceset_ID,
                'image_ID'              =>self::$image_ID,
                'image'                 =>ImagePathAction::get_image_path_data(self::$image_ID,true,true),
                'image_len'             =>self::$image_len,
                'image_facekit_len'     =>self::$facekit_image_len,
                'faceset_token'         =>self::$faceset_token,
                'date_create'           =>Date::get_timestamp()
            ]
        ];

        return ResponseSuccess::init($data);
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_file_log_path();

        if(self::isset_face_ID())
            if(self::isset_image_ID()){

                self::set_image_path();

                self::add_face_token_to_face_plus_plus();

                self::update_face_data();

                self::set_face_image_ID();

                return self::set_return();

            }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['face_ID']))
            $error_info_list[]='Face ID is empty';

        if(empty($_POST['image_ID']))
            $error_info_list[]='Image ID is empty';

        if(empty($_POST['face_token']))
            $error_info_list[]='Face token is empty';

        if(empty($_POST['coords']))
            $error_info_list[]='Coords are empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$face_ID                  =(int)$_POST['face_ID'];
        self::$file_ID                  =empty($_POST['file_ID'])?NULL:$_POST['file_ID'];
        self::$image_ID                 =(int)$_POST['image_ID'];
        self::$coords                   =Json::decode($_POST['coords']);
        self::$face_token               =$_POST['face_token'];

        return self::set();

    }
    
}