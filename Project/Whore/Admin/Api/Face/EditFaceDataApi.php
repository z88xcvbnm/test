<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Date\Date;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Geo\City;
use Core\Module\Geo\CityLocalization;
use Core\Module\Geo\CountryLocalization;
use Core\Module\Lang\LangConfig;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceDataType;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Source\Source;

class EditFaceDataApi{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $face_data_ID;

    /** @var int */
    private static $face_data_type_ID;

    /** @var int */
    private static $image_ID;

    /** @var string */
    private static $link_ID;

    /** @var string */
    private static $date_create;

    /** @var int */
    private static $source_ID;

    /** @var int */
    private static $city_ID;

    /** @var string */
    private static $source_account_link;

    /** @var string */
    private static $name;

    /** @var string */
    private static $age;

    /** @var string */
    private static $city;

    /** @var string */
    private static $info;

    /** @var int */
    private static $image_len;

    /** @var int */
    private static $facekit_image_len;

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_ID(){

        if(!Face::isset_face_ID(self::$face_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_data_type_ID(){

        if(empty(self::$face_data_type_ID))
            return self::set_face_data_type_ID();

        return FaceDataType::isset_face_data_type_ID(self::$face_data_type_ID);

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_source_ID(){

        if(!Source::isset_source_ID(self::$source_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Source ID is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_data_type_ID(){

        self::$face_data_type_ID=Source::get_source_rang(self::$source_ID);

        return true;

    }

    /**
     * @return bool
     */
    private static function set_link_ID(){

        if(empty(self::$source_account_link))
            return true;

        $list=mb_split('\/',self::$source_account_link);

        if(count($list)==0)
            return true;

        for($i=count($list)-1;$i>=0;$i--)
            if(!empty($list[$i])){

                self::$link_ID=trim($list[$i]);

                return true;

            }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_city(){

        if(empty(self::$city))
            return true;

        $country_ID     =NULL;
        $city_ID        =NULL;
        $city           =NULL;
        $country        =NULL;

        $list           =mb_split(',',self::$city);

        if(count($list)<2)
            return true;

        $city       =trim(end($list));
        $city_ID    =CityLocalization::get_city_ID_from_name(mb_strtolower($city,'utf-8'));

        if(!empty($city_ID))
            $city=CityLocalization::get_city_name($city_ID,LangConfig::$lang_ID_default);

        if(!empty($list[0])){

            $country        =trim($list[0]);
            $country_ID     =CountryLocalization::get_country_ID_from_name(mb_strtolower($country,'utf-8'));

            if(!empty($country_ID))
                $country=CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);

        }

        if(!empty($city_ID)){

            if(empty($country_ID)){

                $country_ID     =City::get_country_ID($city_ID);
                $country        =CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);

            }

        }

        if(!empty($city_ID))
            self::$city_ID=$city_ID;

        if(!empty($country)&&!empty($city))
            self::$city=$country.', '.$city;
        else if(empty($city))
            self::$city=$country;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_face_data(){

        if(!FaceData::remove_face_data_from_face_ID(self::$face_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_data(){

        self::$image_len            =FaceImage::get_image_len(self::$face_ID);
        self::$facekit_image_len    =FaceImage::get_face_plus_plus_image_len(self::$face_ID);

        self::$face_data_ID=FaceData::add_face_data(self::$face_ID,self::$city_ID,self::$image_ID,self::$source_ID,self::$source_account_link,self::$name,self::$age,self::$city,self::$info,self::$image_len,self::$facekit_image_len,NULL,0,self::$link_ID,self::$face_data_type_ID);

        if(empty(self::$face_data_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data is empty'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_ID_from_face_data(){

        $r=FaceData::get_image_ID_and_date_create(self::$face_ID);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data is not exists'
            ];

            throw new PhpException($error);

        }

        self::$image_ID         =$r['image_ID'];
        self::$date_create      =$r['date_create'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $link_number_list   =empty(self::$source_account_link)?NULL:mb_split('\/',self::$source_account_link);
        $age                =empty(self::$age)?NULL:(int)self::$age+ceil((strtotime(self::$date_create)-Date::get_timestamp())/(365*86400));

        $data=[
            'success'=>true,
            'data'      =>[
                'ID'                    =>self::$face_ID,
                'image_ID'              =>self::$image_ID,
                'image'                 =>ImagePathAction::get_image_path_data(self::$image_ID,true,true),
                'image_len'             =>self::$image_len,
                'image_facekit_len'     =>self::$facekit_image_len,
                'city_ID'               =>self::$city_ID,
                'source_ID'             =>self::$source_ID,
                'link_ID'               =>self::$link_ID,
                'face_data_type_ID'     =>self::$face_data_type_ID,
                'city'                  =>self::$city,
                'source_account_link'   =>self::$source_account_link,
                'source_link_number'    =>empty($link_number_list)?NULL:end($link_number_list),
                'name'                  =>self::$name,
                'age'                   =>$age,
                'info'                  =>self::$info,
                'date_create'           =>strtotime(self::$date_create)
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_face_ID())
            if(self::isset_source_ID())
                if(self::isset_face_data_type_ID()){

                    self::set_city();
                    self::set_link_ID();
                    self::set_image_ID_from_face_data();
                    self::remove_face_data();
                    self::add_face_data();

                    return self::set_return();

                }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['face_ID']))
            $error_info_list[]='Face ID is empty';

        if(empty($_POST['source_ID']))
            $error_info_list[]='Source ID is empty';

        if(empty($_POST['source_account_link']))
            $error_info_list[]='Source account link is empty';

        if(empty($_POST['name']))
            $error_info_list[]='Name is empty';

        if(empty($_POST['age']))
            $error_info_list[]='Age is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$face_ID                  =(int)$_POST['face_ID'];
        self::$source_ID                =(int)$_POST['source_ID'];
        self::$face_data_type_ID        =empty($_POST['level'])?NULL:(int)$_POST['level'];
        self::$source_account_link      =$_POST['source_account_link'];
        self::$name                     =$_POST['name'];
        self::$age                      =(int)$_POST['age'];
        self::$city                     =empty($_POST['city'])?NULL:$_POST['city'];
        self::$info                     =empty($_POST['info'])?NULL:$_POST['info'];

        return self::set();

    }

}