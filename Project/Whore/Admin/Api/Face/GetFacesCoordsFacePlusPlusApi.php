<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Image\Image;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserBalance;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\All\Action\FacePlusPlus\GetFacesCoordsFacePlusPlusAction;
use Project\Whore\All\Module\Price\PriceConfig;

class GetFacesCoordsFacePlusPlusApi{

    /** @var int */
    private static $file_ID;
    
    /** @var int */
    private static $image_ID;

    /** @var int */
    private static $face_len=0;

    /** @var int */
    private static $time_used;
    
    /** @var array */
    private static $face_list;

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_user_balance(){

        if(
              UserAccess::$is_root
            ||UserAccess::$is_admin
        )
            return true;

        $balance=(int)UserBalance::get_user_balance(User::$user_ID);

        if($balance<PriceConfig::$search_price){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Your balance is low'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_ID(){
        
        if(!Image::isset_image_ID(self::$image_ID)){
            
            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image ID is empty'
            ];

            throw new ParametersValidationException($error);
            
        }
        
        return true;
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_face_list(){
        
        $r=GetFacesCoordsFacePlusPlusAction::init(self::$image_ID);
        
        if(empty($r)){
            
            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face determine error'
            ];

            throw new PhpException($error);
            
        }

        self::$time_used    =empty($r['time_used'])?NULL:$r['time_used'];
        self::$face_len     =$r['face_len'];
        self::$face_list    =$r['face_list'];

        $list=[];

        foreach(self::$face_list as $row){

            $list[]=[
                'coords'=>$row['coords'],
                'face_token'=>$row['face_token']
            ];

        }

        self::$face_list=$list;
        
        return true;
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){
        
        $data=[
            'success'       =>true,
            'time_used'     =>self::$time_used,
            'data'          =>[
                'file_ID'       =>self::$file_ID,
                'image_ID'      =>self::$image_ID,
                'image'         =>ImagePathAction::get_image_path_data(self::$image_ID),
                'face_len'      =>self::$face_len,
                'face_list'     =>self::$face_list
            ]
        ];

        return ResponseSuccess::init($data);
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::check_user_balance())
            if(self::isset_image_ID()){

                self::get_face_list();

                return self::set_return();

            }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(empty($_POST['image_ID'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Image ID is empty'
            ];

            throw new ParametersException($error);

        }

        self::$file_ID      =empty($_POST['file_ID'])?NULL:(int)$_POST['file_ID'];
        self::$image_ID     =(int)$_POST['image_ID'];

        return self::set();

    }
    
}