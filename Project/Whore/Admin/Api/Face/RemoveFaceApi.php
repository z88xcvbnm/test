<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Dir\Dir;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\SystemException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Image\ImagePathListAction;
use Project\Whore\All\Action\FaceKit\RemoveImageFaceKitAction;
use Project\Whore\All\Action\FacePlusPlus\RemoveFaceTokenListFacePlusPlusAction;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceImage;

class RemoveFaceApi{

    /** @var int */
    private static $face_ID;

    /** @var array */
    private static $facekit_image_ID_list       =[];

    /** @var array */
    private static $face_token_list             =[];

    /** @var array */
    private static $image_ID_list               =[];

    /**
     * @return bool
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function isset_face_ID(){

        if(!Face::isset_face_ID(self::$face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face is not exists'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_token_list(){

        self::$face_token_list=FaceImage::get_face_plus_plus_data(self::$face_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_from_face_plus_plus(){

        if(count(self::$face_token_list)==0)
            return false;

        $r=RemoveFaceTokenListFacePlusPlusAction::init(self::$face_token_list);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face token list was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_face(){

        if(!Face::remove_face_ID(self::$face_ID,true)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_facekit_image_list(){

        if(count(self::$facekit_image_ID_list)==0)
            return true;

        $r=RemoveImageFaceKitAction::init(self::$facekit_image_ID_list);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Image was not remove from Face Kit'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function remove_image(){

//        return true;

        if(count(self::$image_ID_list)==0)
            return true;

        $image_list=ImagePathListAction::get_image_path_list(self::$image_ID_list,true,true,true);

        foreach($image_list as $row){

            $list=mb_split('/',$row['image_dir']);

            if(count($list)!=6){

                $error=[
                    'title'     =>PhpException::$title,
                    'info'      =>'Dir is not valid'
                ];

                throw new PhpException($error);

            }

            if(!empty($row['image_dir']))
                if(file_exists($row['image_dir']))
                    if(!Dir::remove_dir($row['image_dir'])){

                        $error=[
                            'title'     =>PhpException::$title,
                            'info'      =>'Dir is not exists'
                        ];

                        throw new PhpException($error);

                    }

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_facekit_image_ID_list(){

        self::$facekit_image_ID_list=FaceImage::get_facekit_image_ID_list(self::$face_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_ID_list(){

        self::$image_ID_list=FaceImage::get_image_ID_list(self::$face_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     */
    private static function set(){

        if(self::isset_face_ID()){

            self::set_face_token_list();
            self::remove_from_face_plus_plus();

            self::set_image_ID_list();
            self::remove_face();
            self::remove_image();

            return self::set_return();

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     */
    public  static function init(){

        if(empty($_POST['face_ID'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        self::$face_ID=(int)$_POST['face_ID'];

        return self::set();

    }

}