<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\Admin\Action\Face\GetFaceSearchHistoryAction;

class GetFaceSearchHistoryApi{

    /** @var int */
    private static $face_search_history_ID;

    /** @var array */
    private static $face_search_history_list    =[];

    /** @var int */
    private static $start                       =0;

    /** @var int */
    private static $len                         =10;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_search_list(){

        $r=GetFaceSearchHistoryAction::init(self::$face_search_history_ID,self::$start,self::$len,User::$user_ID);

        if(empty($r))
            return true;

        self::$face_search_history_list=$r['face_search_history_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'face_search_history_list'=>self::$face_search_history_list
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_face_search_list();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::$face_search_history_ID       =empty($_POST['face_search_history_ID'])?NULL:(int)$_POST['face_search_history_ID'];
        self::$start                        =empty($_POST['start'])?self::$start:(int)$_POST['start'];
        self::$len                          =empty($_POST['len'])?self::$len:(int)$_POST['len'];

        return self::set();

    }

}