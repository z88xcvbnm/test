<?php

namespace Project\Whore\Admin\Api\Face;

use Core\Module\Dir\Dir;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\Admin\Action\Image\ImagePathListAction;
use Project\Whore\All\Action\FacePlusPlus\RemoveFaceTokenListFacePlusPlusAction;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;

class SaveFaceImageListApi{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $image_ID;

    /** @var array */
    private static $image;

    /** @var array */
    private static $image_ID_list=[];

    /** @var int */
    private static $image_len;

    /** @var int */
    private static $facekit_image_len;

    /** @var array */
    private static $face_token_list=[];

    /** @var array */
    private static $image_list=[];

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_ID(){

        if(!Face::isset_face_ID(self::$face_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is not exists'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_from_face_plus_plus(){

        if(count(self::$face_token_list)==0)
            return false;

        $r=RemoveFaceTokenListFacePlusPlusAction::init(self::$face_token_list);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face token list was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_image_ID_list(){

        if(!FaceImage::remove_face_image_except_image_ID_list(self::$face_ID,self::$image_ID_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face image was not remove'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param array $image_ID_list
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_token_list(array $image_ID_list=[]){

        if(count($image_ID_list)==0)
            return true;

        self::$face_token_list=FaceImage::get_face_plus_plus_data(self::$face_ID,$image_ID_list);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_default(){

        if(count(self::$image_ID_list)==0){

            self::$image_ID=NULL;

            return true;

        }

        if(!empty(self::$image_ID))
            self::$image=ImagePathAction::get_image_path_data(self::$image_ID,true,true);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_len(){

        self::$image_len            =FaceImage::get_image_len(self::$face_ID);
        self::$facekit_image_len    =FaceImage::get_face_plus_plus_image_len(self::$face_ID);

        if(!FaceData::update_face_data_image_ID_face_plus_plus(self::$face_ID,self::$image_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face data was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_except_image_ID_list(){

        $except_image_ID_list=FaceImage::get_except_image_ID_list(self::$face_ID,self::$image_ID_list);

        if(count($except_image_ID_list)==0)
            return true;

        self::set_face_token_list($except_image_ID_list);
        self::remove_from_face_plus_plus();
        self::remove_image_ID_list();

        $image_list=ImagePathListAction::get_image_path_list($except_image_ID_list,true,true,true);

        foreach($image_list as $image_ID=>$image_row){

            $dir_path       =$image_row['image_dir'];
            $dir_list       =mb_split('\/',$dir_path);

            if(count($dir_list)==6)
                if(!Dir::remove_dir($dir_path)){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Dir was not remove'
                    ];

                    throw new PhpException($error);

                }

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_ID_list(){

        self::$image_ID_list=FaceImage::get_image_ID_list_from_image_ID_list(self::$face_ID,self::$image_ID_list);

        if(count(self::$image_ID_list)==0)
            return false;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'image_ID'              =>self::$image_ID,
                'image'                 =>self::$image,
                'image_len'             =>self::$image_len,
                'facekit_image_len'     =>self::$facekit_image_len,
                'image_list'            =>array_values(self::$image_list)
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_face_ID()){

            self::set_image_ID_list();
            self::set_except_image_ID_list();

            self::set_image_default();
            self::set_image_len();

            return self::set_return();

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['face_ID']))
            $error_info_list[]='Face ID is empty';

        if(empty($_POST['image_ID_list']))
            $error_info_list[]='Image ID list is empty';
        else if(!Json::is_json($_POST['image_ID_list']))
            $error_info_list[]='Image ID list is not JSON';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$face_ID          =(int)$_POST['face_ID'];
        self::$image_ID         =empty($_POST['image_ID'])?NULL:(int)$_POST['image_ID'];
        self::$image_ID_list    =Json::decode($_POST['image_ID_list']);

        return self::set();

    }

}