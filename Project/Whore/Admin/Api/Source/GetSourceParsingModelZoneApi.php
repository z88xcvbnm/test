<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Date\Date;
use Core\Module\Dir\Dir;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\File\File;
use Core\Module\File\FileParametersCash;
use Core\Module\Image\ImageConvert;
use Core\Module\Image\ImageUploadedPrepare;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\All\Action\FaceKit\GetFacesCoordsFaceKitAction;
use Project\Whore\All\Action\FaceKit\UploadImageFaceKitAction;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;

class GetSourceParsingModelZoneApi{

    /** @var int */
    private static $source_ID       =1;

    /** @var int */
    private static $start_ID        =1;

    /** @var int */
//    private static $finish_ID       =16306;
    private static $finish_ID       =16316;

    /** @var string */
    private static $url             ='https://model.zone/media';

    /** @var string */
    private static $dir_path        ='Temp/ModelZoneParsing';

    /**
     * @param string|NULL $source_account_link
     * @return int|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_face_ID_from_source_account_link(string $source_account_link=NULL){

        if(empty($source_account_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source account link is empty'
            ];

            throw new ParametersException($error);

        }

        return FaceData::get_face_ID_from_source_account_link($source_account_link);

    }

    /**
     * @param string|NULL $file_extension
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_file(string $file_extension=NULL){

        return File::add_file_without_params($file_extension,true,false,false);

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_hash
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_file(int $file_ID=NULL,string $file_hash=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_content_type=NULL,string $file_extension=NULL){

        return File::update_file($file_ID,$file_hash,1,1,$file_size,$file_size,$file_mime_type,$file_content_type,$file_extension,true,false,true);

    }

    /**
     * @param string|NULL $file_path
     * @return array|bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_file(string $file_path=NULL){

        if(empty($file_path))
            return false;

        $image=file_get_contents($file_path);

        if($image===false){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Image is empty'
            ];

            throw new PhpException($error);

        }

        $file_name          =basename($file_path);
        $file_extension     =File::get_file_extension_from_file_name($file_name);

        $file_ID=self::add_file($file_extension);

        if(empty($file_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new PhpException($error);

        }

        $file_dest_path=File::get_file_path_from_file_ID($file_ID,true);

        File::get_file_dir($file_ID,NULL,Date::get_date_for_db());
        FileParametersCash::add_file_parameter_in_cash($file_ID,'file_extension',$file_extension);

        if(file_put_contents($file_dest_path,$image)===false){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not save'
            ];

            throw new PhpException($error);

        }

        $file_mime_type         =File::get_file_mime_type_from_path($file_dest_path);
        $file_content_type      =File::get_file_content_type_from_path($file_dest_path);
        $file_size              =filesize($file_dest_path);
        $data_list              =[
            User::$user_ID,
            $file_size,
            $file_mime_type,
            $file_content_type,
            time(),
            rand(0,time())
        ];
        $file_hash              =Hash::get_sha1_encode(implode(':',$data_list));

        if(!self::update_file($file_ID,$file_hash,$file_size,$file_mime_type,$file_content_type,$file_extension)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not update'
            ];

            throw new PhpException($error);

        }

        return[
            'file_ID'               =>$file_ID,
            'file_content_type'     =>$file_content_type,
            'file_extension'        =>$file_extension
        ];

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_image(int $file_ID=NULL,string $file_content_type='image',string $file_extension=NULL){

        return ImageUploadedPrepare::init($file_ID,$file_content_type,$file_extension);

    }

    /**
     * @param array $image_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function search_face_in_image_list(array $image_list=[]){

        if(count($image_list)==0)
            return[];

        $face_len=0;

        foreach($image_list as $index=>$image_data){

            $r=GetFacesCoordsFaceKitAction::init($image_data['image_ID']);

            $image_list[$index]['face_len']     =0;
            $image_list[$index]['coords']       =[];

            if(count($r)>0)
                if($r['face_len']>0){

                    $image_list[$index]['face_len']     =$r['face_len'];
                    $image_list[$index]['face_list']    =$r['face_list'];
                    $face_list=[];

                    foreach($r['face_list'] as $face_index=>$face_data){

                        $coords         =$face_data['coords'];

//                        print_r($coords);

                        $perc_x         =.6;
                        $perc_y         =.3;
                        $w              =$coords['right']-$coords['left'];
                        $h              =$coords['bottom']-$coords['top'];
                        $w_delta        =ceil($w*$perc_x);
                        $h_delta        =ceil($h*$perc_y);
                        $x              =ceil($coords['left']-$w_delta/2);
                        $y              =ceil($coords['top']-$w_delta/2);
                        $w              +=$w_delta;
                        $h              +=$h_delta;

                        if($x<0)
                            $x=0;

                        if($y<0)
                            $y=0;

                        $face_list[]=[
                            'w'     =>$w,
                            'h'     =>$h,
                            'x'     =>$x,
                            'y'     =>$y
                        ];

//                        print_r($face_list);

                        $face_len++;

                    }

                    $image_list[$index]['coords']=$face_list;

                }

        }

        return $image_list;

    }

    /**
     * @param string|NULL $source_account_link
     * @return int|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face(string $source_account_link=NULL){

        $face_ID=self::get_face_ID_from_source_account_link($source_account_link);

        if(empty($face_ID))
            $face_ID=Face::add_face();

        return $face_ID;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @param string|NULL $name
     * @param string|NULL $source_account_link
     * @param int $image_len
     * @return int
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_data(int $face_ID=NULL,int $image_ID=NULL,string $name=NULL,string $source_account_link=NULL,int $image_len=0){

        $face_data_ID=FaceData::get_face_data_ID($face_ID);

        if(empty($face_data_ID))
            $face_data_ID=FaceData::add_face_data($face_ID,NULL,$image_ID,self::$source_ID,$source_account_link,$name,NULL,NULL,NULL,$image_len);

        return $face_data_ID;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_data_image(int $face_ID=NULL,int $image_ID=NULL){

        return FaceData::update_face_data_image_ID($face_ID,$image_ID);

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face(int $face_ID=NULL){

        return Face::update_face($face_ID);

    }

    /**
     * @param int|NULL $face_ID
     * @param array $image_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function upload_image_to_facekit_db(int $face_ID=NULL,array $image_list=[]){

        if(count($image_list)==0)
            return[];

        $image_ID=NULL;

        foreach($image_list as $image_index=>$image_data){

            $is_added=false;

            $image_list[$image_index]['face_image_ID']=NULL;

            if(count($image_data['coords'])==1){

                $image_path=$image_data['image_dir'].'/'.$image_data['image_item_ID_list']['large'];

                if(!file_exists(DirConfigProject::$dir_face_temp))
                    Dir::create_dir(DirConfigProject::$dir_face_temp);

                $image_temp_path    =DirConfigProject::$dir_face_temp.'/'.$image_data['image_ID'].'.jpg';
                $image_result       =ImageConvert::crop_image_from_file_path($image_path,$image_data['coords'][0]['x'],$image_data['coords'][0]['y'],$image_data['coords'][0]['w'],$image_data['coords'][0]['h'],$image_data['coords'][0]['w'],$image_data['coords'][0]['h']);

                ImageConvert::save_image($image_result,$image_temp_path,'jpg');

                $r=UploadImageFaceKitAction::init($face_ID,$image_temp_path,true);

                if(!empty($r)){

                    if(!empty($r['facekit_image_ID'])){

                        $facekit_image_ID=$r['facekit_image_ID'];

                        if(empty($image_ID))
                            $image_ID=$image_data['image_ID'];

                        $image_list[$image_index]['face_image_ID']=FaceImage::add_face_image($face_ID,$image_data['file_ID'],$image_data['image_ID'],$facekit_image_ID,$image_data['coords']);

                        $is_added=true;

                    }

                }

            }

            if(!$is_added)
                $image_list[$image_index]['face_image_ID']=FaceImage::add_face_image($face_ID,$image_data['file_ID'],$image_data['image_ID'],NULL,[]);

        }

        return[
            'image_ID'          =>empty($image_ID)?$image_list[0]['image_ID']:$image_ID,
            'image_list'        =>$image_list
        ];

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_content(){

        if(!file_exists(self::$dir_path))
            Dir::create_dir(self::$dir_path);

        $n=0;

        for($start_ID=self::$finish_ID;$start_ID>0;$start_ID--){

            $url        =self::$url.'/'.$start_ID;
            $r          =file_get_contents($url);

            file_put_contents('Temp/parsing_model_zone.log',$url."\nstart: ".Date::get_date_time_full()."\n",FILE_APPEND);

            if($r!==false){

                $body=preg_match_all('/<body.*\/body>/s',$r,$body_list);

                if($body!==false)
                    if(count($body_list[0])>0){

                        $r_name     =preg_match_all('/<h1.*>(.*)<\/h1>/s',$body_list[0][0],$name_list);
                        $name       =NULL;

                        if($r_name!==false)
                            if(count($name_list[0])>0)
                                $name=$name_list[1][0];

                        if(!empty($name)){

                            $links=preg_match_all('@((https?://)?([-\\w]+\\.[-\\w\\.]+)+\\w(:\\d+)?(/([-\\w/_\\.]*(\\?\\S+)?)?)*(jpg|jpeg|png|JPG|JPEG|PNG))@',$body_list[0][0],$list);

                            if($links!==false)
                                if(count($list[0])>0){

                                    $image_list=[];

                                    foreach($list[0] as $link){

                                        $file_data              =self::prepare_file($link);
                                        $file_ID                =$file_data['file_ID'];
                                        $file_content_type      =$file_data['file_content_type'];
                                        $file_extension         =$file_data['file_extension'];
                                        $image_data             =self::prepare_image($file_ID,'image',$file_extension);
                                        $image_list[]           =$image_data;

                                    }

                                    if(count($image_list)>0){

                                        $image_list         =self::search_face_in_image_list($image_list);
                                        $face_ID            =self::prepare_face($url);
                                        $face_data_ID       =self::prepare_face_data($face_ID,NULL,$name,$url,count($image_list));
                                        $image_data         =self::upload_image_to_facekit_db($face_ID,$image_list);

                                        self::update_face_data_image($face_ID,$image_data['image_ID']);
                                        self::update_face($face_ID);

                                    }

                                    file_put_contents('Temp/parsing_model_zone.log',"image_len: ".count($image_list)."\n",FILE_APPEND);

                                }

                        }

                    }

            }

            file_put_contents('Temp/parsing_model_zone.log',"finish: ".Date::get_date_time_full()."\n",FILE_APPEND);

//            $n++;
//
//            if($n>3)
//                exit;

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        self::get_content();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        return self::set();

    }

}