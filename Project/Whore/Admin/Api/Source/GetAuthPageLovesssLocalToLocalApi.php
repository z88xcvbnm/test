<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Cookie\CookieFile;
use Core\Module\Curl\CurlGet;
use Core\Module\Curl\CurlPost;
use Core\Module\Dir\Dir;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\ShluhamNetException;
use Core\Module\Response\ResponseSuccess;

class GetAuthPageLovesssLocalToLocalApi{

    /** @var string */
    private static $host            ='https://lovesss.ru';

    /** @var string */
    private static $link            ='https://lovesss.ru/login';
//    private static $link            ='http://folx.ru';

    /** @var string */
    private static $cookie_path     ='Temp/Parsing/Lovesss/reverse_ip.txt';

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_page(){

//        if(file_exists(self::$cookie_path))
//            unlink(self::$cookie_path);

        $r=CurlGet::init(self::$link,[],NULL,false,[],[],self::$cookie_path);
//        $r=CurlPost::init(self::$link,[],[],false,self::$cookie_path);

        if($r['status']==200){

            preg_match_all('/<img.*?src=\"\/captcha(.*?)\".*?class=\"frm-captcha-image\".*?\/>/is',$r['data'],$captcha_list);
            preg_match_all('/name=\"captcha\[(.*?)\]\"/is',$r['data'],$captcha_key_list);

            $cookie=CookieFile::get_cookie_list_from_file(self::$cookie_path);

            print_r($cookie);

            if(count($captcha_list)==2)
                if(count($captcha_list[1])>0){

                    $file_path='Temp/Lovesss_img';

                    Dir::create_dir($file_path);

                    $file_path  .='/'.time().'.jpg';
                    $link       =self::$host.'/captcha'.$captcha_list[1][0];

                    $r=CurlGet::init($link,[],NULL,false,[],[],self::$cookie_path);

                    file_put_contents($file_path,$r['data']);

                    sleep(5);

                    echo $captcha_key_list[1][0]."\n";
                    echo'<img src="/'.$file_path.'" />';

                    exit;

                }

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        self::get_page();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        return self::set();

    }

}