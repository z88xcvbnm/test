<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Dir\Dir;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\ShluhamNetException;
use Core\Module\File\File;
use Core\Module\File\FileParametersCash;
use Core\Module\Image\ImageUploadedPrepare;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\All\Action\FaceKit\GetFacesCoordsFaceKitAction;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class GetSourceParsingModelZoneLocalApi{

    /** @var int */
    private static $source_ID                               =1;

    /** @var int */
    private static $start_ID                                =1;

    /** @var int */
    private static $finish_ID                               =6353;

//    private static $finish_ID                               =14328;
//    private static $finish_ID                               =15275;
//    private static $finish_ID                               =16306;
//    private static $finish_ID                               =16316;
//    private static $finish_ID                               =16305;
//    private static $finish_ID                               =16283;

    /** @var string */
    private static $url                                     ='https://model.zone/media';

    /** @var string */
    private static $dir_path                                ='Temp/ModelZoneParsing';

    /** @var string */
    private static $url_api                                 ='http://shluham.net/api/json';

    /** @var string */
    private static $api_add_face_data_action                ='add_face_data';

    /** @var string */
    private static $api_add_face_image_action               ='add_face_image';

    /** @var string */
    private static $api_get_face_ID_action                  ='get_face_ID';

    /** @var string */
    private static $api_update_face_to_public_action        ='update_face_to_public';

    /** @var string */
    private static $key_hash                                ='domino777';

    /** @var string */
    private static $file_log_path                           ='';

    /**
     * @param string|NULL $source_account_link
     * @return mixed
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_get_face_ID(string $source_account_link=NULL){

        if(empty($source_account_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source account link is empty'
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_get_face_ID_action;
        $data       =[
            'source_account_link'   =>$source_account_link,
            'key_hash'              =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face ID was not give',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){

            file_put_contents(self::$file_log_path,"--> ERROR: ".Date::get_date_time_full()."\n",FILE_APPEND);

            return NULL;

        }

        if($r['status']!=200){

            file_put_contents(self::$file_log_path,"--> restart send get face ID: ".Date::get_date_time_full()."\n",FILE_APPEND);

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_get_face_ID($source_account_link);

        }

        if(isset($r['error'])){

            file_put_contents(self::$file_log_path,"--> ERROR: ".Date::get_date_time_full()."\n",FILE_APPEND);

            return NULL;

        }

        if(empty($r['data']))
            return NULL;

        if(!Json::is_json($r['data']))
            return NULL;

        $data=Json::decode($r['data']);

        if(isset($data['error'])){

            file_put_contents(self::$file_log_path,"--> ERROR: ".Date::get_date_time_full()."\n",FILE_APPEND);

            return NULL;

        }

        return $data['data']['face_ID'];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $source_ID
     * @param int|NULL $image_ID
     * @param int|NULL $city_ID
     * @param string|NULL $source_account_link
     * @param string|NULL $name
     * @param int|NULL $age
     * @param string|NULL $city
     * @param string|NULL $info
     * @return mixed
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_add_face_data(int $face_ID=NULL,int $source_ID=NULL,int $image_ID=NULL,int $city_ID=NULL,string $source_account_link=NULL,string $name=NULL,int $age=NULL,string $city=NULL,string $info=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($source_ID))
            $error_info_list[]='Source ID is empty';

        if(empty($source_account_link))
            $error_info_list[]='Source account link is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_add_face_data_action;
        $data       =[
            'face_ID'                   =>$face_ID,
            'source_ID'                 =>$source_ID,
            'image_ID'                  =>$image_ID,
            'city_ID'                   =>$city_ID,
            'source_account_link'       =>$source_account_link,
            'name'                      =>$name,
            'age'                       =>$age,
            'city'                      =>$city,
            'info'                      =>$info,
            'key_hash'                  =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face data was not add',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){

            file_put_contents(self::$file_log_path,"--> ERROR: ".Date::get_date_time_full()."\n",FILE_APPEND);

            return NULL;

        }

        if($r['status']!=200){

            file_put_contents(self::$file_log_path,"--> restart send add face data: ".Date::get_date_time_full()."\n",FILE_APPEND);

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_add_face_data($face_ID,$source_ID,$image_ID,$city_ID,$source_account_link,$name,$age,$city,$info);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $facekit_image_ID
     * @param int|NULL $file_size
     * @param string|NULL $file_content_type
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param string|NULL $file_path
     * @param array|NULL $face_coords
     * @return int|null
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_add_face_image(int $face_ID=NULL,int $facekit_image_ID=NULL,int $file_size=NULL,string $file_content_type=NULL,string $file_mime_type=NULL,string $file_extension=NULL,string $file_path=NULL,array $face_coords=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_add_face_image_action;
        $data       =[
            'face_ID'               =>$face_ID,
            'facekit_image_ID'      =>$facekit_image_ID,
            'file_size'             =>$file_size,
            'file_content_type'     =>$file_content_type,
            'file_mime_type'        =>$file_mime_type,
            'file_extension'        =>$file_extension,
            'image_base64'          =>base64_encode(file_get_contents($file_path)),
            'face_len'              =>count($face_coords),
            'face_coords'           =>empty($face_coords)?'[]':Json::encode($face_coords),
            'key_hash'              =>self::$key_hash,
            'time'                  =>time().'_'.rand(0,time())
        ];

        $r=CurlPost::init($url,[],$data);

        file_put_contents(self::$file_log_path,"--> ERROR: ".print_r($r,true)."\n",FILE_APPEND);

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face image was not add',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if($r['status']==500||$r['status']==500){

            file_put_contents(self::$file_log_path,"--> request: ".Date::get_date_time_full()."\n",FILE_APPEND);
            file_put_contents(self::$file_log_path,print_r($r,true)."\n",FILE_APPEND);
            file_put_contents(self::$file_log_path,print_r($data,true)."\n",FILE_APPEND);
            file_put_contents(self::$file_log_path,"--> restart send add face image: ".Date::get_date_time_full()."\n",FILE_APPEND);

            sleep(5);

            return self::send_add_face_image($face_ID,$facekit_image_ID,$file_size,$file_content_type,$file_mime_type,$file_extension,$file_path,$face_coords);

        }
        else if($r['status']!=200)
            return NULL;
        else{

            if(empty($r['data']))
                return NULL;

            if(!Json::is_json($r['data']))
                return NULL;

            $data=Json::decode($r['data']);

            if(isset($data['error'])){

                file_put_contents(self::$file_log_path,"--> ERROR: ".Date::get_date_time_full()."\n",FILE_APPEND);

                return NULL;

            }

            return $data['data']['image_ID'];

        }

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_update_face_to_public(int $face_ID=NULL,int $image_ID=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_update_face_to_public_action;
        $data       =[
            'face_ID'               =>$face_ID,
            'image_ID'              =>$image_ID,
            'key_hash'              =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face was not update to public',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){

            file_put_contents(self::$file_log_path,"--> ERROR: ".Date::get_date_time_full()."\n",FILE_APPEND);

            return NULL;

        }

        if($r['status']!=200){

            file_put_contents(self::$file_log_path,"--> restart send update face to public: ".Date::get_date_time_full()."\n",FILE_APPEND);

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_update_face_to_public($face_ID,$image_ID);

        }

        return true;

    }

    /**
     * @param string|NULL $source_account_link
     * @return int|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_face_ID_from_source_account_link(string $source_account_link=NULL){

        if(empty($source_account_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source account link is empty'
            ];

            throw new ParametersException($error);

        }

        return FaceData::get_face_ID_from_source_account_link($source_account_link);

    }

    /**
     * @param string|NULL $file_extension
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_file(string $file_extension=NULL){

        return File::add_file_without_params($file_extension,true,false,false);

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_hash
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_file(int $file_ID=NULL,string $file_hash=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_content_type=NULL,string $file_extension=NULL){

        return File::update_file($file_ID,$file_hash,1,1,$file_size,$file_size,$file_mime_type,$file_content_type,$file_extension,true,false,true);

    }

    /**
     * @param string|NULL $file_path
     * @return array|bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_file(string $file_path=NULL){

        if(empty($file_path))
            return false;

        $image=@file_get_contents($file_path);

        if($image===false)
            return NULL;

        $file_name          =basename($file_path);
        $file_extension     =File::get_file_extension_from_file_name($file_name);

        $file_ID=self::add_file($file_extension);

        if(empty($file_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new PhpException($error);

        }

        $file_dest_path=File::get_file_path_from_file_ID($file_ID,true);

        File::get_file_dir($file_ID,NULL,Date::get_date_time_full());
        FileParametersCash::add_file_parameter_in_cash($file_ID,'file_extension',$file_extension);

        if(file_put_contents($file_dest_path,$image)===false){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not save'
            ];

            throw new PhpException($error);

        }

        if(filesize($file_dest_path)<=2048){

            file_put_contents(self::$file_log_path,"BAD FILE: ".$file_path." ".Date::get_date_time_full()."\n",FILE_APPEND);

            return NULL;

        }

//        $size       =getimagesize($file_dest_path,$im_info);
//        $sum        =$size[0]*$size[1];
//
//        if($sum!=0){
//
//            $file_size  =filesize($file_dest_path);
//            $cof        =$file_size/$sum;
//
//        }
//        else
//            $cof=0;
//
//        file_put_contents(self::$file_log_path,"-> file cof: ".$cof." ".Date::get_date_time_full()."\n",FILE_APPEND);
//
//        if($cof<.01){
//
//            file_put_contents(self::$file_log_path,"BAD FILE SUM: ".$file_path." ".Date::get_date_time_full()."\n",FILE_APPEND);
//
//            return NULL;
//
//        }

        switch(exif_imagetype($file_dest_path)){

            case 1:
            case 2:
            case 3:
            case 7:
            case 8:
                break;

            default:
                return NULL;

        }

        $file_mime_type         =File::get_file_mime_type_from_path($file_dest_path);
        $file_content_type      =File::get_file_content_type_from_path($file_dest_path);
        $file_size              =filesize($file_dest_path);
        $data_list              =[
            User::$user_ID,
            $file_size,
            $file_mime_type,
            $file_content_type,
            time(),
            rand(0,time())
        ];
        $file_hash              =Hash::get_sha1_encode(implode(':',$data_list));

        if($file_size==0){

            File::remove_file_ID($file_ID);

            file_put_contents(self::$file_log_path,"--> file is not exists: ".$file_size." ".Date::get_date_time_full()."\n",FILE_APPEND);

            return NULL;

        }

        if(!self::update_file($file_ID,$file_hash,$file_size,$file_mime_type,$file_content_type,$file_extension)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not update'
            ];

            throw new PhpException($error);

        }

        return[
            'file_ID'               =>$file_ID,
            'file_content_type'     =>$file_content_type,
            'file_extension'        =>$file_extension
        ];

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_image(int $file_ID=NULL,string $file_content_type='image',string $file_extension=NULL){

        return ImageUploadedPrepare::init($file_ID,$file_content_type,$file_extension);

    }

    /**
     * @param array $image_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function search_face_in_image_list(array $image_list=[]){

        return $image_list;

        if(count($image_list)==0)
            return[];

        $face_len=0;

        foreach($image_list as $index=>$image_data){

            file_put_contents(self::$file_log_path,"-> start search faces on image_ID: ".$image_data['image_ID']." ".Date::get_date_time_full()."\n",FILE_APPEND);

            $r=GetFacesCoordsFaceKitAction::init($image_data['image_ID'],NULL,false,self::$file_log_path);

            file_put_contents(self::$file_log_path,"-> finish search faces on image_ID: ".$image_data['image_ID']." ".Date::get_date_time_full()."\n",FILE_APPEND);

            $image_list[$index]['face_len']     =0;
            $image_list[$index]['coords']       =[];

            if(count($r)>0){

                file_put_contents(self::$file_log_path,"--> image len: ".$r['face_len']." ".Date::get_date_time_full()."\n",FILE_APPEND);

                if($r['face_len']>0){

                    $image_list[$index]['face_len']     =$r['face_len'];
                    $image_list[$index]['face_list']    =$r['face_list'];
                    $face_list=[];

                    foreach($r['face_list'] as $face_index=>$face_data){

                        $coords         =$face_data['coords'];
                        $perc_x         =.6;
                        $perc_y         =.3;
                        $w              =$coords['right']-$coords['left'];
                        $h              =$coords['bottom']-$coords['top'];
                        $w_delta        =ceil($w*$perc_x);
                        $h_delta        =ceil($h*$perc_y);
                        $x              =ceil($coords['left']-$w_delta/2);
                        $y              =ceil($coords['top']-$w_delta/2);
                        $w              +=$w_delta;
                        $h              +=$h_delta;

                        if($x<0)
                            $x=0;

                        if($y<0)
                            $y=0;

                        $face_list[]=[
                            'w'     =>$w,
                            'h'     =>$h,
                            'x'     =>$x,
                            'y'     =>$y
                        ];

                        $face_len++;

                    }

                    $image_list[$index]['coords']=$face_list;

                }

            }
            else{

                file_put_contents(self::$file_log_path,"--> error result: ".print_r($r,true)." ".Date::get_date_time_full()."\n",FILE_APPEND);

            }

        }

        return $image_list;

    }

    /**
     * @param string|NULL $source_account_link
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face(string $source_account_link=NULL){

        $face_ID=self::get_face_ID_from_source_account_link($source_account_link);

        if(empty($face_ID))
            $face_ID=Face::add_face();
        
        $api_face_ID=self::send_get_face_ID($source_account_link);

        return[
            'face_ID'       =>$face_ID,
            'api_face_ID'   =>$api_face_ID
        ];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $api_face_ID
     * @param int|NULL $image_ID
     * @param string|NULL $name
     * @param string|NULL $source_account_link
     * @param int $image_len
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_data(int $face_ID=NULL,int $api_face_ID=NULL,int $image_ID=NULL,string $name=NULL,string $source_account_link=NULL,int $image_len=0){

        $face_data_ID=FaceData::get_face_data_ID($face_ID);

        if(empty($face_data_ID))
            $face_data_ID=FaceData::add_face_data($face_ID,NULL,$image_ID,self::$source_ID,$source_account_link,$name,NULL,NULL,NULL,$image_len);

        $api_face_data_ID=self::send_add_face_data($api_face_ID,self::$source_ID,$image_ID,NULL,$source_account_link,$name);

        return[
            'face_data_ID'          =>$face_data_ID,
            'api_face_data_ID'      =>$api_face_data_ID
        ];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $api_face_ID
     * @param int|NULL $image_ID
     * @param int|NULL $api_image_ID
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_data_image(int $face_ID=NULL,int $api_face_ID=NULL,int $image_ID=NULL,int $api_image_ID=NULL){

        self::send_update_face_to_public($api_face_ID,$api_image_ID);

        return FaceData::update_face_data_image_ID($face_ID,$image_ID);

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face(int $face_ID=NULL){

        return Face::update_face($face_ID);

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $api_face_ID
     * @param array $image_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function upload_image_to_facekit_db(int $face_ID=NULL,int $api_face_ID=NULL,array $image_list=[]){

        if(count($image_list)==0)
            return[];

        $image_ID       =NULL;
        $api_image_ID   =NULL;

        foreach($image_list as $image_index=>$image_data){

            file_put_contents(self::$file_log_path,"-> start prepare image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);

            $is_added=false;

            $image_list[$image_index]['face_image_ID']      =NULL;
            $facekit_image_ID                               =NULL;

//            if(count($image_data['coords'])==1){
//
//                $image_path=$image_data['image_dir'].'/'.$image_data['image_item_ID_list']['large'];
//
//                if(!file_exists(DirConfigProject::$dir_face_temp))
//                    Dir::create_dir(DirConfigProject::$dir_face_temp);
//
//                file_put_contents(self::$file_log_path,"--> start convert image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);
//
//                $image_temp_path    =DirConfigProject::$dir_face_temp.'/'.$image_data['image_ID'].'.jpg';
//                $image_result       =ImageConvert::crop_image_from_file_path($image_path,$image_data['coords'][0]['x'],$image_data['coords'][0]['y'],$image_data['coords'][0]['w'],$image_data['coords'][0]['h'],$image_data['coords'][0]['w'],$image_data['coords'][0]['h']);
//
//                ImageConvert::save_image($image_result,$image_temp_path,'jpg');
//
//                file_put_contents(self::$file_log_path,"--> finish convert image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);
//
//                file_put_contents(self::$file_log_path,"--> start upload image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);
////                $r=UploadImageFaceKitAction::init($face_ID,$image_temp_path,true);
//                $r=UploadImageFaceKitAction::init($api_face_ID,$image_temp_path,true,false,self::$file_log_path);
//
//                file_put_contents(self::$file_log_path,"--> finish upload image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);
//
//                if(!empty($r)){
//
//                    if(!empty($r['facekit_image_ID'])){
//
//                        $facekit_image_ID=$r['facekit_image_ID'];
//
//                        if(empty($image_ID))
//                            $image_ID=$image_data['image_ID'];
//
//                        file_put_contents(self::$file_log_path,"--> start add to local face image image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);
//
//                        $image_list[$image_index]['face_image_ID']=FaceImage::add_face_image($face_ID,$image_data['file_ID'],$image_data['image_ID'],$facekit_image_ID,$image_data['coords']);
//
//                        file_put_contents(self::$file_log_path,"--> added to local face image: ".$image_list[$image_index]['face_image_ID']." image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);
//                        file_put_contents(self::$file_log_path,"--> start add to server face image image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);
//
//                        $image_list[$image_index]['api_image_ID']=self::send_add_face_image($api_face_ID,$facekit_image_ID,$image_data['file_size'],$image_data['file_content_type'],$image_data['file_mime_type'],$image_data['file_extension'],$image_data['file_path'],$image_data['coords']);
//
//                        file_put_contents(self::$file_log_path,"--> added to server face image: ".$image_list[$image_index]['api_image_ID']." image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);
//
//                        $is_added=true;
//
//                    }
//
//                }
//
//            }

            if(!$is_added){

                file_put_contents(self::$file_log_path,"--> start add to local face image image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);

                $image_list[$image_index]['face_image_ID']          =FaceImage::add_face_image($face_ID,$image_data['file_ID'],$image_data['image_ID'],NULL,[]);

                file_put_contents(self::$file_log_path,"--> added to local face image: ".$image_list[$image_index]['face_image_ID']." image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);
                file_put_contents(self::$file_log_path,"--> start  add to server face image image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);

                $image_list[$image_index]['api_image_ID']           =self::send_add_face_image($api_face_ID,NULL,$image_data['file_size'],$image_data['file_content_type'],$image_data['file_mime_type'],$image_data['file_extension'],$image_data['file_path'],[]);

                file_put_contents(self::$file_log_path,"--> added to server face image: ".$image_list[$image_index]['api_image_ID']." image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);

            }

            file_put_contents(self::$file_log_path,"-> finish upload image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n",FILE_APPEND);

        }

        return[
            'image_ID'          =>empty($image_ID)?$image_list[0]['image_ID']:$image_ID,
            'api_image_ID'      =>count($image_list)==0?NULL:$image_list[0]['api_image_ID'],
            'image_list'        =>$image_list
        ];

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_content(){

        if(!file_exists(self::$dir_path))
            Dir::create_dir(self::$dir_path);

        self::$file_log_path=DirConfigProject::$dir_parsing_log;

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/ModelZone';

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/model_zone_'.time().'.log';

        $n=0;

        for($start_ID=self::$finish_ID;$start_ID>0;$start_ID--){

            $url        =self::$url.'/'.$start_ID;
            $r          =@file_get_contents($url);

            file_put_contents(self::$file_log_path,"\nstart: ".Date::get_date_time_full().' -> '.$url."\n",FILE_APPEND);

            if($r!==false){

                $body=preg_match_all('/<body.*\/body>/s',$r,$body_list);

                if($body!==false)
                    if(count($body_list[0])>0){

                        $r_name     =preg_match_all('/<h1.*>(.*)<\/h1>/s',$body_list[0][0],$name_list);
                        $name       =NULL;

                        if($r_name!==false)
                            if(count($name_list[0])>0)
                                $name=$name_list[1][0];

                        if(!empty($name)){

                            $links=preg_match_all('@((https?://)?([-\\w]+\\.[-\\w\\.]+)+\\w(:\\d+)?(/([-\\w/_\( \)\\.]*(\\?\\S+)?)?)*(jpg|jpeg|png|JPG|JPEG|PNG))@',$body_list[0][0],$list);
//                            $links=preg_match_all('@((https?://)?([-\\w]+\\.[-\\w\\.]+)+\\w(:\\d+)?(/([-\\w/_\\.]*(\\?\\S+)?)?)*(jpg|jpeg|png|JPG|JPEG|PNG))@',$body_list[0][0],$list);

//                            print_r($list);exit;

                            if($links!==false)
                                if(count($list[0])>0){

                                    file_put_contents(self::$file_log_path,"image start len: ".Date::get_date_time_full()."\n",FILE_APPEND);
                                    file_put_contents(self::$file_log_path,"image list: ".print_r($list[0],true)." ".Date::get_date_time_full()."\n",FILE_APPEND);

                                    $image_list=[];

                                    foreach($list[0] as $link){

                                        $link=str_replace(' ','%20',$link);

                                        file_put_contents(self::$file_log_path,"start prepare image: ".$link." ".Date::get_date_time_full()."\n",FILE_APPEND);

                                        $file_data                          =self::prepare_file($link);

                                        if(!empty($file_data)){

                                            $file_ID                            =$file_data['file_ID'];
                                            $file_content_type                  =$file_data['file_content_type'];
                                            $file_extension                     =$file_data['file_extension'];

                                            file_put_contents(self::$file_log_path,"-> start prepare image data: ".$link." ".Date::get_date_time_full()."\n",FILE_APPEND);

                                            $image_data                         =self::prepare_image($file_ID,'image',$file_extension);

                                            file_put_contents(self::$file_log_path,"-> finish prepare image data: ".$link." ".Date::get_date_time_full()."\n",FILE_APPEND);

                                            $image_path                         =$image_data['image_dir'].'/'.$image_data['image_item_ID_list']['large'];
                                            $image_data['file_path']            =$image_path;
                                            $image_data['file_size']            =filesize($image_path);
                                            $image_data['file_content_type']    ='image';
                                            $image_data['file_extension']       =$file_extension;
                                            $image_data['file_mime_type']       =File::get_file_mime_type_from_path($image_path);
                                            $image_list[]                       =$image_data;

                                            file_put_contents(self::$file_log_path,"complete prepare image: ".$link." ".Date::get_date_time_full()."\n",FILE_APPEND);

                                        }
                                        else
                                            file_put_contents(self::$file_log_path,"prepare image FAILED: ".$link." ".Date::get_date_time_full()."\n",FILE_APPEND);

                                    }

                                    if(count($image_list)>0){

                                        file_put_contents(self::$file_log_path,"image prepared len: ".Date::get_date_time_full()."\n",FILE_APPEND);

                                        file_put_contents(self::$file_log_path,"start search face on image list: ".Date::get_date_time_full()."\n",FILE_APPEND);

                                        $image_list         =self::search_face_in_image_list($image_list);

                                        file_put_contents(self::$file_log_path,"finish search faces: ".Date::get_date_time_full()."\n",FILE_APPEND);

                                        $face_data          =self::prepare_face($url);
                                        $face_ID            =$face_data['face_ID'];
                                        $api_face_ID        =$face_data['api_face_ID'];

                                        file_put_contents(self::$file_log_path,"added face ID: ".Date::get_date_time_full()."\n",FILE_APPEND);

                                        $face_data          =self::prepare_face_data($face_ID,$api_face_ID,NULL,$name,$url,count($image_list));
                                        $face_data_ID       =$face_data['face_data_ID'];
                                        $api_face_data_ID   =$face_data['api_face_data_ID'];

                                        file_put_contents(self::$file_log_path,"added face data ID: ".Date::get_date_time_full()."\n",FILE_APPEND);
                                        file_put_contents(self::$file_log_path,"start upload images to FaceKit: ".Date::get_date_time_full()."\n",FILE_APPEND);

                                        $image_data         =self::upload_image_to_facekit_db($face_ID,$api_face_ID,$image_list);

                                        file_put_contents(self::$file_log_path,"uploaded images to FaceKit: ".Date::get_date_time_full()."\n",FILE_APPEND);

                                        self::update_face_data_image($face_ID,$api_face_ID,$image_data['image_ID'],$image_data['api_image_ID']);

                                        file_put_contents(self::$file_log_path,"Updated face data: ".Date::get_date_time_full()."\n",FILE_APPEND);

                                        self::update_face($face_ID);

                                        file_put_contents(self::$file_log_path,"Finish: ".Date::get_date_time_full()."\n",FILE_APPEND);

                                    }

                                }

                        }

                    }

            }

//            $n++;
//
//            if($n>1)
//                exit;

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        self::get_content();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        return self::set();

    }

}