<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Db\Connect\DbConnect;
use Core\Module\Db\Db;
use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\ShluhamNetException;
use Core\Module\File\File;
use Core\Module\File\FileParametersCash;
use Core\Module\Image\ImageConvert;
use Core\Module\Image\ImageUploadedPrepare;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\All\Action\FaceKit\GetFacesCoordsFaceKitAction;
use Project\Whore\All\Action\FaceKit\UploadImageFaceKitAction;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class GetSourceParsingModelZoneFromBackupApi{

    /** @var int */
    private static $source_ID                               =1;

    /** @var int */
    private static $face_ID_start                           =336;

    /** @var string */
    private static $dir_path                                ='Temp/ModelZoneParsing';

    /** @var string */
    private static $url_api                                 ='http://shluham.net/api/json';

    /** @var string */
    private static $api_add_face_data_action                ='add_face_data';

    /** @var string */
    private static $api_add_face_image_action               ='add_face_image';

    /** @var string */
    private static $api_get_face_ID_action                  ='get_face_ID';

    /** @var string */
    private static $api_update_face_to_public_action        ='update_face_to_public';

    /** @var string */
    private static $api_get_face_ID_list_action             ='get_face_id_list';

    /** @var string */
    private static $key_hash                                ='domino777';

    /** @var array */
    private static $face_data_list                          =[];

    /** @var array */
    private static $image_list                              =[];

    /** @var string */
    private static $db_name                                 ='whore_backup';

    /** @var string */
    private static $dir_image                               ='Resource/ImageBackup';

    /** @var string */
    private static $file_log_path                           ='';

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function search_face_in_image_list(){

        if(count(self::$face_data_list)==0)
            return true;

        $face_len=0;

        foreach(self::$face_data_list as $face_ID=>$image_data){

            file_put_contents(self::$file_log_path,"\nstart: ".Date::get_date_time_full().' -> face_ID: '.$face_ID."\n",FILE_APPEND);
            file_put_contents(self::$file_log_path,"start search face on image: ".Date::get_date_time_full()."\n",FILE_APPEND);

            $r=GetFacesCoordsFaceKitAction::init(NULL,$image_data['image_path']);

            file_put_contents(self::$file_log_path,"find face len: ".$r['face_len']." -> ".Date::get_date_time_full()."\n",FILE_APPEND);

            self::$face_data_list[$face_ID]['face_len']     =0;
            self::$face_data_list[$face_ID]['coords']       =[];

            if(count($r)>0)
                if($r['face_len']>0){

                    self::$face_data_list[$face_ID]['face_len']     =$r['face_len'];
                    self::$face_data_list[$face_ID]['face_list']    =$r['face_list'];

                    $face_list=[];

                    foreach($r['face_list'] as $face_index=>$face_data){

                        $coords         =$face_data['coords'];
                        $perc_x         =.6;
                        $perc_y         =.3;
                        $w              =$coords['right']-$coords['left'];
                        $h              =$coords['bottom']-$coords['top'];
                        $w_delta        =ceil($w*$perc_x);
                        $h_delta        =ceil($h*$perc_y);
                        $x              =ceil($coords['left']-$w_delta/2);
                        $y              =ceil($coords['top']-$w_delta/2);
                        $w              +=$w_delta;
                        $h              +=$h_delta;

                        if($x<0)
                            $x=0;

                        if($y<0)
                            $y=0;

                        $face_list[]=[
                            'w'     =>$w,
                            'h'     =>$h,
                            'x'     =>$x,
                            'y'     =>$y
                        ];

                        $face_len++;

                    }

                    self::$face_data_list[$face_ID]['coords']=$face_list;

                    file_put_contents(self::$file_log_path,"start upload image to facekit: -> ".Date::get_date_time_full()."\n",FILE_APPEND);

                    $r=self::upload_image_to_facekit_db($image_data['face_ID'],[self::$face_data_list[$face_ID]]);

                    file_put_contents(self::$file_log_path,"uploaded image to facekit: -> ".Date::get_date_time_full()."\n",FILE_APPEND);
                    file_put_contents(self::$file_log_path,"start update face data: -> ".Date::get_date_time_full()."\n",FILE_APPEND);

                    self::send_update_face_to_public($face_ID,$r['api_image_ID']);

                    file_put_contents(self::$file_log_path,"finish update face data: -> ".Date::get_date_time_full()."\n",FILE_APPEND);

                }

            file_put_contents(self::$file_log_path,"finish: ".Date::get_date_time_full()." -> face_ID: ".$face_ID."\n\n",FILE_APPEND);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_update_face_to_public(int $face_ID=NULL,int $image_ID=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_update_face_to_public_action;
        $data       =[
            'face_ID'               =>$face_ID,
            'image_ID'              =>$image_ID,
            'key_hash'              =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if($r['status']!=200){

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            self::send_update_face_to_public($face_ID,$image_ID);

        }

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face was not update to public',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $facekit_image_ID
     * @param int|NULL $file_size
     * @param string|NULL $file_content_type
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param string|NULL $file_path
     * @param array|NULL $face_coords
     * @return mixed
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_add_face_image(int $face_ID=NULL,int $facekit_image_ID=NULL,int $file_size=NULL,string $file_content_type=NULL,string $file_mime_type=NULL,string $file_extension=NULL,string $file_path=NULL,array $face_coords=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_add_face_image_action;
        $data       =[
            'face_ID'               =>$face_ID,
            'facekit_image_ID'      =>$facekit_image_ID,
            'file_size'             =>$file_size,
            'file_content_type'     =>$file_content_type,
            'file_mime_type'        =>$file_mime_type,
            'file_extension'        =>$file_extension,
            'image_base64'          =>base64_encode(file_get_contents($file_path)),
            'face_len'              =>count($face_coords),
            'face_coords'           =>empty($face_coords)?'[]':Json::encode($face_coords),
            'key_hash'              =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face image was not add',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if($r['status']!=200){

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            self::send_add_face_image($face_ID,$facekit_image_ID,$file_size,$file_content_type,$file_mime_type,$file_extension,$file_path,$face_coords);

        }

        $data=Json::decode($r['data']);

        return $data['data']['image_ID'];

    }

    /**
     * @param int|NULL $api_face_ID
     * @param array $image_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function upload_image_to_facekit_db(int $api_face_ID=NULL,array $image_list=[]){

        if(count($image_list)==0)
            return[];

        $image_ID       =NULL;
        $api_image_ID   =NULL;

        foreach($image_list as $image_index=>$image_data){

            $is_added=false;

            $image_list[$image_index]['face_image_ID']      =NULL;
            $facekit_image_ID                               =NULL;

            if(count($image_data['coords'])==1){

                $image_path=$image_data['image_path'];

                if(!file_exists(DirConfigProject::$dir_face_temp))
                    Dir::create_dir(DirConfigProject::$dir_face_temp);

                $image_temp_path    =DirConfigProject::$dir_face_temp.'/'.$image_data['image_ID'].'.jpg';
                $image_result       =ImageConvert::crop_image_from_file_path($image_path,$image_data['coords'][0]['x'],$image_data['coords'][0]['y'],$image_data['coords'][0]['w'],$image_data['coords'][0]['h'],$image_data['coords'][0]['w'],$image_data['coords'][0]['h']);

                ImageConvert::save_image($image_result,$image_temp_path,'jpg');

//                print_r($image_list);exit;

                $r=UploadImageFaceKitAction::init($api_face_ID,$image_temp_path,true);

                if(!empty($r)){

                    if(!empty($r['facekit_image_ID'])){

                        $facekit_image_ID=$r['facekit_image_ID'];

                        if(empty($image_ID))
                            $image_ID=$image_data['image_ID'];

                        $image_list[$image_index]['api_image_ID']=self::send_add_face_image($api_face_ID,$facekit_image_ID,$image_data['file_size'],$image_data['file_content_type'],$image_data['file_mime_type'],$image_data['file_extension'],$image_data['image_path'],$image_data['coords']);

                        $is_added=true;

                    }

                }

//                if(file_exists($image_temp_path))
//                    unlink($image_temp_path);

            }

            if(!$is_added){

                $image_list[$image_index]['api_image_ID']=self::send_add_face_image($api_face_ID,NULL,$image_data['file_size'],$image_data['file_content_type'],$image_data['file_mime_type'],$image_data['file_extension'],$image_data['image_path'],[]);

            }

        }

        return[
            'image_ID'          =>empty($image_ID)?$image_list[0]['image_ID']:$image_ID,
            'api_image_ID'      =>count($image_list)==0?NULL:$image_list[0]['api_image_ID'],
            'image_list'        =>$image_list
        ];

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_ID_list(){

        $source_account_link_list=array_keys(self::$face_data_list);

        if(count($source_account_link_list)==0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source account link list is empty'
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_get_face_ID_list_action;
        $data       =[
            'source_account_link_list'      =>Json::encode($source_account_link_list),
            'key_hash'                      =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face ID was not give'
            ];

            throw new ShluhamNetException($error);

        }

        if($r['status']!=200){

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            self::set_face_ID_list();

        }

        $data=Json::decode($r['data']);

        if(count($data['data']['face_ID_list'])==0)
            return false;

        $list=[];

        foreach($data['data']['face_ID_list'] as $source_account_link=>$face_ID)
            if(isset(self::$face_data_list[$source_account_link])){

                $list[$source_account_link]=[
                    'face_ID'                   =>$face_ID,
                    'source_account_link'       =>$source_account_link,
                    'image_ID'                  =>self::$face_data_list[$source_account_link]
                ];

                self::$image_list[]=self::$face_data_list[$source_account_link];

            }

        self::$face_data_list=$list;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image(){

        $q=[
            'table'=>'_image_item',
            'select'=>[
                'id',
                'image_id',
                'image_type',
                'file_size',
                'file_mime_type',
                'file_extension',
                'date_create'
            ],
            'where'=>[
                'image_id'              =>self::$image_list,
                'resolution_type'       =>'large',
                'type'                  =>0
            ]
        ];

        $r=Db::select($q,self::$db_name);

        if(count($r)==0)
            return true;

        $list=[];

        foreach($r as $row){

            $image_dir          =Dir::get_dir_from_date(self::$dir_image,$row['date_create']);
            $image_ID_dir       =$image_dir.'/'.$row['image_id'];
            $image_path         =$image_ID_dir.'/'.$row['id'].'.jpg';

//            $list[$row['image_id']]=$image_path;
            $list[$row['image_id']]=[
                'image_path'            =>$image_path,
                'file_extension'        =>$row['file_extension'],
                'file_content_type'     =>'image',
                'file_size'             =>$row['file_size'],
                'file_mime_type'        =>$row['file_mime_type'],
                'date_create'           =>$row['date_create']
            ];

        }

        self::$image_list   =$list;
        $list               =[];

        foreach(self::$face_data_list as $source_account_link=>$row)
            if(isset(self::$image_list[$row['image_ID']])){

//                $row['image_path']=self::$image_list[$row['image_ID']];

                $row=array_merge($row,self::$image_list[$row['image_ID']]);

                $list[$row['face_ID']]=$row;

            }

        asort($list);

//        print_r($list);exit;

        self::$face_data_list=$list;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_data_list(){

        $q=[
            'table'=>'face_data',
            'select'=>[
                'face_id',
                'source_account_link',
                'image_id'
            ],
            'where'=>[
                'face_id'       =>[
                    'method'        =>'>=',
                    'value'         =>self::$face_ID_start
                ],
                'type'=>0
            ],
            'order'=>[
                [
                    'column'        =>'face_id',
                    'direction'     =>'asc'
                ]
            ]
        ];

        $r=Db::select($q,self::$db_name);

        if(count($r)==0)
            return true;

        foreach($r as $row)
            if(!empty($row['image_id']))
                self::$face_data_list[$row['source_account_link']]=$row['image_id'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_connect_to_backup_db(){

        $db_config=[
            'type'                      =>'postgresql',
            'host'                      =>'localhost',
            'login'                     =>'postgres',
            'password'                  =>'postgres',
            'name'                      =>'whore_backup'
        ];

        self::$db_name=$db_config['name'];

        DbConnect::init($db_config);

        return true;

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_log_path(){

        self::$file_log_path=DirConfigProject::$dir_parsing_log;

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/ModelZone';

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/model_zone_'.time().'.log';

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        \Config::$is_debug=true;

        self::set_log_path();
        self::set_connect_to_backup_db();
        self::set_face_data_list();
        self::set_face_ID_list();
        self::set_image();
        self::search_face_in_image_list();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        return self::set();

    }

}