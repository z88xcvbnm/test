<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Dir\Dir;
use Core\Module\Exception\PhpException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\All\Action\FacePlusPlus\AddFaceTokenToFacesetFacePlusPlusAction;
use Project\Whore\All\Action\FacePlusPlus\GetFacesCoordsFacePlusPlusAction;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;

class AddFaceTokenListToFacePlusPlusApi{

    /** @var array */
    private static $face_ID_list=[];

    /** @var string */
    private static $file_log_path;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$face_ID_list     =[];
        self::$file_log_path    =NULL;

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_file_log_path(){

        self::$file_log_path=DirConfigProject::$dir_parsing_log;

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/FacePlusPlus';

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/face_plu_plus_'.time().'.log';

        file_put_contents(self::$file_log_path,"Init\n",FILE_APPEND);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_ID_list(){

        file_put_contents(self::$file_log_path,"Start get face ID list\n",FILE_APPEND);

//        self::$face_ID_list=Face::get_face_ID_list();
        self::$face_ID_list=FaceData::get_face_ID_list_with_zero_image_facekit_len();

        file_put_contents(self::$file_log_path,"-> face len: ".count(self::$face_ID_list)." - done\n",FILE_APPEND);

        return true;

    }

    /**
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_image_list(){

        $n=0;

        file_put_contents(self::$file_log_path,"Start prepare face ID list\n",FILE_APPEND);

        foreach(self::$face_ID_list as $face_ID){

            file_put_contents(self::$file_log_path,"-> Start face ID: ".$face_ID."\n",FILE_APPEND);
            file_put_contents(self::$file_log_path,"--> Start get image list\n",FILE_APPEND);

            $image_list=FaceImage::get_face_plus_plus_data_without_faceset_ID($face_ID);

            file_put_contents(self::$file_log_path,"--> Have image len: ".count($image_list)." - done\n",FILE_APPEND);
            file_put_contents(self::$file_log_path,"--> ".print_r($image_list,true)."\n",FILE_APPEND);

            if(count($image_list)>0){

                file_put_contents(self::$file_log_path,"--> Start send image to Face Plus Plus\n",FILE_APPEND);

                $image_coords_list=[];

                foreach($image_list as $index=>$row){

                    file_put_contents(self::$file_log_path,"---> Get image path list\n",FILE_APPEND);

                    $image_path_list=ImagePathAction::get_image_path_data($row['image_ID'],true,true,true);

                    file_put_contents(self::$file_log_path,"----> ".print_r($image_path_list,true)."\n",FILE_APPEND);
                    file_put_contents(self::$file_log_path,"----> Done\n",FILE_APPEND);

                    if(!empty($image_path_list)){

                        $image_path=$image_path_list['image_dir'].'/'.$image_path_list['image_item_ID_list']['large']['ID'];

                        file_put_contents(self::$file_log_path,"----> image_path: ".$image_path."\n",FILE_APPEND);

                        if(file_exists($image_path)){

                            file_put_contents(self::$file_log_path,"----> Get faces coords\n",FILE_APPEND);

                            $get_coords_data=GetFacesCoordsFacePlusPlusAction::init($row['image_ID'],$image_path,false,self::$file_log_path);

                            file_put_contents(self::$file_log_path,"-----> done\n",FILE_APPEND);
//                            file_put_contents(self::$file_log_path,print_r($get_coords_data,true)."\n",FILE_APPEND);

                            if(!empty($get_coords_data)){

                                $image_list[$index]['face_len']     =$get_coords_data['face_len'];
                                $image_list[$index]['coords']       =$get_coords_data['face_list'];

                                $face_token=NULL;

                                if(count($get_coords_data['face_list'])==1)
                                    if(!empty($get_coords_data['face_list'][0]))
                                        if(!empty($get_coords_data['face_list'][0]['face_token'])){

                                            $face_token                         =$get_coords_data['face_list'][0]['face_token'];
                                            $image_list[$index]['face_token']   =$face_token;

                                        }

                                file_put_contents(self::$file_log_path,"-----> Start update face image\n",FILE_APPEND);

                                if(!FaceImage::update_face_image_face_token($face_ID,$row['image_ID'],$get_coords_data['face_list'],$face_token)){

                                    $error=[
                                        'title'     =>PhpException::$title,
                                        'info'      =>'Face image was not update'
                                    ];

                                    throw new PhpException($error);

                                }

                                $image_coords_list[]=$image_list[$index];

                                file_put_contents(self::$file_log_path,"------> done\n",FILE_APPEND);

                            }

                        }

                    }

                }

                if(count($image_coords_list)>0){

                    file_put_contents(self::$file_log_path,"-----> Start face token to Face++ \n",FILE_APPEND);

                    $add_face_token_data=AddFaceTokenToFacesetFacePlusPlusAction::init($face_ID,$image_coords_list,false,self::$file_log_path,true);

                    file_put_contents(self::$file_log_path,print_r($add_face_token_data,true)."\n",FILE_APPEND);

                    if(empty($add_face_token_data)){

                        $error=[
                            'title'     =>PhpException::$title,
                            'info'      =>'Face token was not add'
                        ];

                        throw new PhpException($error);

                    }

                    file_put_contents(self::$file_log_path,"-----> Done \n",FILE_APPEND);

                }

                file_put_contents(self::$file_log_path,"--> Done\n",FILE_APPEND);

            }

//            $n++;
//
//            if($n>10)
//                return true;

        }

        file_put_contents(self::$file_log_path,"Finish prepare face ID list\n",FILE_APPEND);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_file_log_path();
        self::set_face_ID_list();

        self::prepare_face_image_list();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::reset_data();

        return self::set();

    }

}