<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Curl\CurlGet;
use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Dir\Dir;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\ShluhamNetException;
use Core\Module\File\File;
use Core\Module\File\FileParametersCash;
use Core\Module\Geo\City;
use Core\Module\Geo\CityLocalization;
use Core\Module\Geo\CountryLocalization;
use Core\Module\Image\Image;
use Core\Module\Image\ImageUploadedPrepare;
use Core\Module\Json\Json;
use Core\Module\Lang\LangConfig;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class GetSourceParsingJuliaDatesTestLocalToLocalApi{

    /** @var int */
    private static $page;

    /** @var string */
    private static $host                ='https://juliadates.com';

    /** @var string  */
    private static $cookie_path         ='Temp/Parsing/JuliaDates/reverse_ip.txt';

    /** @var string */
    private static $x_csrf_token;

    /** @var string  */
    private static $csrf_token;

    /**
     * @return bool
     */
    private static function set_cookie_path(){

        self::$cookie_path=DIR_ROOT.'/'.self::$cookie_path;

        return true;

    }

    /**
     * @param string|NULL $data
     * @return string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_token(string $data=NULL){

        preg_match_all("/<meta\s*name=\"csrf\-token\"\s*content=\"(.*?)\"\s*>/is",$data,$token_list);

        if(count($token_list)!=2){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Meta is empty'
            ];

            throw new ParametersException($error);

        }

        if(count($token_list[1])==0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Token is empty in meta'
            ];

            throw new ParametersException($error);

        }

        return trim($token_list[1][0]);

    }

    /**
     * @param string|NULL $data
     * @return string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_token_from_input(string $data=NULL){

        preg_match_all("/<input\s*type=\"hidden\"\s*name=\"_csrf\-frontend\"\s*value=\"(.*?)\"\s*>/is",$data,$token_list);

        if(count($token_list)!=2){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Meta is empty'
            ];

            throw new ParametersException($error);

        }

        if(count($token_list[1])==0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Token is empty in meta'
            ];

            throw new ParametersException($error);

        }

        return trim($token_list[1][0]);

    }

    /**
     * @param string|NULL $data
     * @return array
     */
    public  static function get_cookie(string $data=NULL){

        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi',$data, $matches);

        $cookies=[];

        foreach($matches[1] as $item){

            parse_str($item,$cookie);

            $cookies=array_merge($cookies,$cookie);

        }

        return $cookies;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function open_root_host(){

        $curl=curl_init();

        $user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041107 Firefox/1.0";

        curl_setopt($curl,CURLOPT_USERAGENT, $user_agent);

        curl_setopt($curl,CURLOPT_HEADER,true);
        curl_setopt($curl,CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($curl,CURLOPT_COOKIESESSION,true);

        curl_setopt($curl,CURLOPT_COOKIEJAR,self::$cookie_path);
        curl_setopt($curl,CURLOPT_COOKIEFILE,self::$cookie_path);

        curl_setopt($curl,CURLOPT_URL,self::$host);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,true);

        $data                   =curl_exec($curl);
        $status                 =curl_getinfo($curl,CURLINFO_HTTP_CODE);
        $cookie_list            =self::get_cookie($data);
        self::$x_csrf_token     =self::get_token($data);

        curl_close($curl);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function open_login(){

        $curl=curl_init();

        $user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5)".
        " Gecko/20041107 Firefox/1.0";

        curl_setopt($curl,CURLOPT_USERAGENT, $user_agent);

        curl_setopt($curl,CURLOPT_URL,self::$host.'/login');

        curl_setopt($curl, CURLOPT_HEADER,true);
        curl_setopt($curl, CURLOPT_VERBOSE,true);
//        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($curl, CURLOPT_COOKIESESSION,true);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,true);

        curl_setopt($curl, CURLOPT_COOKIEJAR,realpath(self::$cookie_path));
        curl_setopt($curl, CURLOPT_COOKIEFILE,realpath(self::$cookie_path));

//        $header_list=[
//            'x-csrf-token: '.self::$x_csrf_token,
//            'x-requested-with: XMLHttpRequest',
//            'referer: https://juliadates.com/',
//            'accept-language: en,ru;q=0.9'
//        ];

        $header_list=[
            'accept-language: en,ru;q=0.9',
            'content-type: application/x-www-form-urlencoded; charset=UTF-8',
            'cookie: mf_user_id=933694384; _gcl_au=1.1.842147333.1560778246; _ym_uid=1560778246282156164; _ym_d=1560778246; _ga=GA1.1.1940653296.1560778247; _gid=GA1.1.409520453.1560778247; _ym_isad=1; f=40382e1d8bcf2312cd0ab9ebe6d25887; _ym_visorc_44308729=w',
            'origin: https://juliadates.com',
            'referer: https://juliadates.com/',
            'user-agent: '.$user_agent,
            'x-csrf-token: '.self::$x_csrf_token,
            'x-requested-with: XMLHttpRequest'
        ];

        curl_setopt($curl, CURLOPT_HTTPHEADER,$header_list);

//        $cookie_list=[
//            '_ym_isad'=>'1',
//            '_ym_visorc_44308729'=>'w',
//            '_ym_uid'=>'1560778246282156164',
//            '_gcl_au'=>'1.1.842147333.1560778246',
//            '_ym_d'=>'1560778246',
//            '__cfduid'=>'d38a291757b32f5e026f4b81a09cb6e511560778243',
//            'i'=>'RaOaX1PwMU0+pBiwFKl9XbpdCW+tqKOFbskoU1rFkMcuqM30RhVSaaJdicmEfUn+sOi0AiWH0K+aQW2PcAbPg1I+mvM=',
//            'yandexuid'=>'467604661560778245',
//            'yp'=>'1876138245.yrts.1560778245#1876138245.yrtsi.1560778245',
//            'f'=>'40382e1d8bcf2312cd0ab9ebe6d25887',
//            '_gid'=>'GA1.1.409520453.1560778247',
//            '_ga'=>'GA1.1.1940653296.1560778247',
//            '_gat_gtag_UA_97622185_1'=>'1',
//            'mf_user_id'=>'933694384',
//            'yabs-sid'=>'2412216371560778245',
//        ];

//        $cookie_list=[
//            'accept-language: en,ru;q=0.9',
//            'content-type: application/x-www-form-urlencoded; charset=UTF-8',
//            'cookie: __cfduid=d38a291757b32f5e026f4b81a09cb6e511560778243; mf_user_id=933694384; browser=3fe0e469f9f6ade7c6ef7d5d9e13eb2c1ed284d6fe7d5c640eeab3c5413fed8da%3A2%3A%7Bi%3A0%3Bs%3A7%3A%22browser%22%3Bi%3A1%3Bs%3A2%3A%22ok%22%3B%7D; _gcl_au=1.1.842147333.1560778246; _ym_uid=1560778246282156164; _ym_d=1560778246; _ga=GA1.1.1940653296.1560778247; _gid=GA1.1.409520453.1560778247; _ym_isad=1; f=40382e1d8bcf2312cd0ab9ebe6d25887; _ym_visorc_44308729=w; _csrf-frontend=977353bb0940333dd9ae8c4e52036d247aacf2e7eed4e9ed41e4ec24024d9f87a%3A2%3A%7Bi%3A0%3Bs%3A14%3A%22_csrf-frontend%22%3Bi%3A1%3Bs%3A32%3A%22SCxC3vvF9fJnKh9xg9w_Oo9zvuJs1_HD%22%3B%7D; advanced-frontend=kqpql95k5pid384ctuar05u26e',
//            'origin: https://juliadates.com',
//            'referer: https://juliadates.com/',
//            'user-agent: '.$user_agent,
//            'x-csrf-token: r8GCMGxuYqbulE8pxzPjQT3LE5s0BleRg5E7tzlDNTr8gvpzXxgU4NfyBUeMW9o5WvJkxHtpbuv15HHECBx9fg==',
//            'x-requested-with: XMLHttpRequest'
//        ];

        $data                   =curl_exec($curl);
        $status                 =curl_getinfo($curl,CURLINFO_HTTP_CODE);

        self::$csrf_token       =self::get_token_from_input($data);

        curl_close($curl);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function user_auth(){

        $curl=curl_init();

        $user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5)".
        " Gecko/20041107 Firefox/1.0";

        curl_setopt($curl,CURLOPT_USERAGENT, $user_agent);

        curl_setopt($curl,CURLOPT_URL,self::$host.'/login');

        curl_setopt($curl, CURLOPT_HEADER,true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_VERBOSE,true);
//        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($curl, CURLOPT_COOKIESESSION,true);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);

        curl_setopt($curl, CURLOPT_COOKIEJAR,realpath(self::$cookie_path));
        curl_setopt($curl, CURLOPT_COOKIEFILE,realpath(self::$cookie_path));

//        $header_list=[
//            'content-type: application/x-www-form-urlencoded; charset=UTF-8',
//            'x-csrf-token: '.self::$x_csrf_token,
//            'x-requested-with: XMLHttpRequest',
//            'referer: https://juliadates.com/',
//            'accept-encoding: gzip, deflate, br',
//            'accept-language: en,ru;q=0.9',
//            'content-length: 162'
//        ];

        $header_list=[
            ':authority: juliadates.com',
            ':method: POST',
            ':path: /login',
            ':scheme: https',
            'accept: */*',
            'accept-encoding: gzip, deflate, br',
            'accept-language: en,ru;q=0.9',
            'Accept-Language: en,ru;q=0.9',
            'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
//            'Cookie: mf_user_id=933694384; _gcl_au=1.1.842147333.1560778246; _ym_uid=1560778246282156164; _ym_d=1560778246; _ga=GA1.1.1940653296.1560778247; _gid=GA1.1.409520453.1560778247; _ym_isad=1; f=40382e1d8bcf2312cd0ab9ebe6d25887; _ym_visorc_44308729=w',
            'origin: https://juliadates.com',
            'referer: https://juliadates.com/',
            'User-Agent: '.$user_agent,
            'x-csrf-token: '.self::$x_csrf_token,
            'x-requested-with: XMLHttpRequest',
        ];

        print_r($header_list);

        curl_setopt($curl, CURLOPT_HTTPHEADER,$header_list);

        $post_list=[
            '_csrf-frontend'    =>self::$csrf_token,
            'username'          =>'serg.silinin@gmail.com',
            'password'          =>'Silin643534'
        ];

        print_r($post_list);

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$post_list);

        $data         =curl_exec($curl);
        $status       =curl_getinfo($curl);

        echo "\n\n\n";

        print_r($status);

        echo'---------------------------------------';

        echo $data;exit;

        self::$csrf_token=self::get_token($data);

        // meta: thqifKRwh1D3Z6Zr_vAT_rHnXJLZ1vxxKk_AhcX48Mf6QpNO3jnEZ7ot7iqrlXaJ2K8_572glxhvCYS1sM-njg==
        // x-csrf-token: thqifKRwh1D3Z6Zr_vAT_rHnXJLZ1vxxKk_AhcX48Mf6QpNO3jnEZ7ot7iqrlXaJ2K8_572glxhvCYS1sM-njg==

        // after login
        // x-csrf-token: thqifKRwh1D3Z6Zr_vAT_rHnXJLZ1vxxKk_AhcX48Mf6QpNO3jnEZ7ot7iqrlXaJ2K8_572glxhvCYS1sM-njg==
        // _csrf-frontend: Mtb802tNdk7inDesnMyJxzZSZYR0VBJ4Zx1oFqTBWtN-js3hEQQ1ea_Wf-3JqeywXxoG8RAieREiWywm0fYNmg==

        curl_close($curl);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        self::set_cookie_path();

        self::open_root_host();
        self::open_login();
        self::user_auth();


        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        if(empty($_POST['page'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Page is empty'
            ];

            throw new ParametersException($error);

        }

        self::$page=(int)$_POST['page'];

        return self::set();

    }

}