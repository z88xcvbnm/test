<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Response\ResponseSuccess;

class CheckModelZoneLastIDApi{

    /** @var string */
    private static $url                     ='https://model.zone/media';

    /** @var int */
    private static $model_zone_ID;

    /** @var int */
    private static $model_zone_ID_last;

    /** @var int */
    private static $added_profile_len       =0;

    /** @var int */
    private static $len                     =200;

    /**
     * @return bool
     */
    private static function prepare(){

        $n=0;

        do{

            $url        =self::$url.'/'.self::$model_zone_ID;
            $r          =@file_get_contents($url);

            if($r!==false){

                $body=preg_match_all('/<body.*\/body>/s',$r,$body_list);

                if($body!==false)
                    if(count($body_list[0])>0){

                        $links=preg_match_all('@((https?://)?([-\\w]+\\.[-\\w\\.]+)+\\w(:\\d+)?(/([-\\w/_\( \)\\.]*(\\?\\S+)?)?)*(jpg|jpeg|png|JPG|JPEG|PNG))@',$body_list[0][0],$list);

                        if($links!==false)
                            if(count($list[0])>0)
                                foreach($list[0] as $link){

                                    $link       =str_replace(' ','%20',$link);

                                    $data       =get_headers($link, true);
                                    $size       =isset($data['Content-Length'])?(int) $data['Content-Length']:0;

                                    if(!empty($size))
                                        if($size>192){

                                            $n=0;

                                            self::$model_zone_ID_last   =self::$model_zone_ID;
                                            self::$added_profile_len    ++;

                                            break;

                                        }

                                }

                    }

            }

            $n++;

            if($n>=self::$len)
                break;

            self::$model_zone_ID++;

        }while($n<self::$len);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'model_zone_ID'         =>self::$model_zone_ID,
                'added_profile_len'     =>self::$added_profile_len
            ]
        ];

        return ResponseSuccess::init($data);

    }

    private static function set(){

        return self::set_return();

    }

    public  static function init(){

        return self::set();

    }

}