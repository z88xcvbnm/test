<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Curl\CurlGet;
use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Dir\Dir;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\ShluhamNetException;
use Core\Module\File\File;
use Core\Module\File\FileParametersCash;
use Core\Module\Geo\City;
use Core\Module\Geo\CityLocalization;
use Core\Module\Geo\CountryLocalization;
use Core\Module\Image\Image;
use Core\Module\Image\ImageUploadedPrepare;
use Core\Module\Json\Json;
use Core\Module\Lang\LangConfig;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;
use Project\Whore\All\Module\Source\SourcePageEmpty;

class GetSourceParsingEmilyDatesLocalToLocalApi{

    /** @var int */
    private static $source_ID                               =18;

    /** @var int */
    private static $page                                    =1;

    /** @var array */
    private static $profile_index_list                      =[
        800000,
        1780000
    ];

    /** @var array */
    private static $profile_list=[
//        1137026,493580,757920,
//        1176119,
//        1288326,
//        1431584,691406,
        1309414,1667653,1746038,1045552,1443102,1091220,1658737,1338380,1303672,1454043,1779966,1775470,1780206,1780220,1780015,1772891,1780169,1779914,1779865,1779858,1779756,830743,570845,788484,503768,1275122,1049910,1307497,1687726,877590,1671205,1226750,1027368,1198921,1388304,783737,1767398,1235060,1062856,1026785,1478894,1682673,883135,1706288,781390,1026252,1729259,997480,1441895,1153830,716239,1516407,1094620,532000,1148709,1525500,1493336,1624598,988339,1133112,986900,783111,620453,601770,1179020,1740079,1281694,1746985,1773601,654486,1727584,1739059,1508611,1546091,1696101,1731968,1246666,1761059,1032257,1690004,1468121,1752167,1266159,1747648,1612649,1753427,1351845,990163,1585043,1585043,1234661,1704498,1723972,1690794,1604481,1540953,1736055,1454478,1521743,552348,1560394,612589,1726894,1511979,1744140,1765377,1464306,1559593,1102338,949389,1283540,990710,908880,1760416,1767471,922206,1090422,1580507,1279150,1532131,1756436,1283405,1643021,1242751,1599503,1673847,1739801,626171,978803,1535488,1007210,1731162,1673045,1774055,1738713,629086,1777292,1144411,1130975,1603057,1004391,1053059,1574540,1273723,1175105,1763503,1653951,1284425,1486246,523978,1044593,897258,1200319,971544,947231,1692744,1746999,1208309,323326,1325689,1446626,951661,903233,1280539,434417,802221,1745715,549574,1714450,595966,1511808,917747,1669902,1497719,1771856,1403553,750788,1611641,1755877,1756220,1000657,925646,460056,1699346,1721961,973234,769445,667298,1746165,915253,649187,953091,1729250,1748146,821915,1757497,1664095,1758254,1361571,1725131,1185059,1659414,1721884,1687938,1701272,732971,1370263,622492,1493465,1000764,1746074,1669562,1174753,976053,1028680,932717,1064684,1287908,725698,1384381,926733,1778874,622083,1742558,1470020,1554280,1335712,1031332,1376290,1503200,1630173,1723543,1534458,431572,1627021,1385806,1280222,1079959,1098359,1434398,1106041,1771393,1323487,1766287,552692,1741432,1770129,1744867,1423722,1572000,1743441,1767376,1746239,1028476,1434907,1753291,1572818,1745336,1480866,1276385,1165267,1756361,1280361,714463,621515,814833,614676,484237,788322,1489090,1769572,1486116,991332,600053,1734837,1471289,1696916,1167350,1602900,934446,1748809,586954,1766559,721782,880585,1627014,1077607,1668514,1731517,1712161,1548619,1713461,1669073,1769571,1423469,1716593,1487987,1306927,1668013,1324878,1498895,1645690,1383537,1098085,1755752,435688,1603205,870898,1733812,1368050,1594391,1113092,1170553,1758559,1543114,1751029,1575701,822850,1753935,1599994,1732323,873526,1776139,876948,658917,1445503,1293519,1446113,1086202,1772410,1398829,701066,1367015,1703265,1129744,1534906,1756500,1035508,1575666,953967,1588747,1697270,1340295,844665,1735166,1760620,1668380,1606215,310912,297276,1288947,1680655,1753202,1735936,950700,1706386,1162547,1149214,1727853,1638500,954183,683133,1535346,482450,507761,788500,1763732,1687497,430451,1526607,1410069,478424,768059,948975,1693231,1598766,1566934,1729370,1201553,1663193,1017173,1730196,1115328,1775023,1759323,1337014,1764032,1500883,1736762,1063675,1670318,1774731,1438277,938484,1209462,984606,605196,1708585,1589905,1146584,1292256,504416,1768085,1348619,950896,1235845,1773953,1339097,1515352,1471997,1775807,1617480,1646883,1754555,1777614,694470,1558065,1638138,652983,1461305,923861,1038019,1768870,484970,1468597,430341,1769858,651432,1753530,1411913,1069805,1497859,1753608,1769157,1770979,1763045,1092046,1380387,1322240,1298047,1158440,1556830,1723099,1081064,1739398,1723056,1758286,1642138,1763434,1773796,1579923,1612341,1688025,1738269,1475430,1651170,1512608,1003576,946196,1118771,1277129,1430793,1534842,1237749,784490,1396580,1175708,1688683,1281571,1605394,1393040,645178,966651,1396322,1767382,630325,1772204,1214800,1756713,1293908,1069864,1728235,1464460,1735205,1697869,1103036,1724657,1757734,1645607,1777685,1050068,1278241,1028815,1729813,1679925,1777609,1763380,963949,694174,1296106,1570690,1454839,1771587,1587418,1363267,1769626,1330703,1607713,1576252,1063356,903870,1257914,1469760,1191071,1565552,1067927,1387413,884788,1775610,1737608,1080150,662314,1748966,1714329,1501977,1373013,1732766,1663915,1773901,944955,1742805,1124971,1740909,1723468,1610498,1674731,1254459,1472582,1628700,461778,1054543,1726078,1713175,1290646,1726165,943774,1473535,1188868,1699683,1476136,1563990,1706508,1471963,1459713,1738887,1757221,1218632,954754,766381,1287729,1770159,1716822,1183334,1773303,1752079,1563643,654457,1149350,1770363,1695676,1526260,1060956,1601352,660449,1208338,1734031,1309242,1768674,947799,960687,1733973,960020,1470562,1211491,1549271,1011117,1610826,948569,1174016,316110,1517157,1312143,1420260,936099,673192,1723719,1768813,1519193,1439137,599121,1260181,1034673,1038030,1025558,1121146,1377807,383171,535224,1044140,1737233,1765951,1749926,474543,1427511,1108285,1273323,1273317,1455006,1523975,642926,1667699,1378092,1395804,955886,1698258,1590820,1592725,1756971,1584677,1759664,1168982,1306642,906378,1288184,1711531,1737908,1151785,1115767,1714876,315485,1748459,938283,1747464,1338651,1678954,1734432,1161036,701673,1736995,1773744,1671451,1564529,1644650,1772591,1769736,1769698,1772110,1604872,936064,1779168,1165637,1757636,693258,825060,1604080,1164452,1244106,1030588,1720021,1143465,665072,1611509,1173753,1505491,1779665,1133758,1737566,395488,1284019,1031689,465400,1770016,1292453,1014301,1685220,1116051,1371985,1610822,994020,292002,1766812,1078455,1248928,1286194,1581977,494290,1746786,1710071,1258762,1476072,1778094,1337105,1723307,1084504,1769945,603598,1141672,1593682,1281490,1715123,1524145,1718700,1756332,781049,1729258,1648487,1767778,803221,1585664,1669522,1045318,1213143,1234715,1778121,1293950,1006478,1748170,1554479,1491329,1275202,874400,1239377,1185991,1177134,1779093,1773697,1656840,464757,988113,1578994,1579474,1490037,1661786,1608744,1102954,1512015,1564877,1353550,1096110,1769681,1481779,788827,1398491,1284846,834132,1445952,1775631,1401604,588572,789977,1106933,1752916,1649607,1163602,1679679,950311,1771361,1599095,1763541,930644,1069028,1516204,1595193,1679220,1777606,800494,1061126,1638210,1727986,1272855,1687495,1671634,1494549,1684775,1776954,1178327,1004928,766898,1053308,849638,1774976,1522480,688200,1439451,1556773,1733581,985309,1696309,1662305,1735876,1680066,970187,1492018,1340994,633747,1297011,1469208,1171872,1719457,1589884,1311463,1497124,1614101,1327214,1769570,1138347,1161195,1129614,1721952,1746004,976629,1562655,1737670,1439824,1478557,1362911,1460288,1727999,1580001,779962,1212543,1115812,1683065,1098022,1263450,1746541,881898,1660506,1215474,1358967,1772426,1705783,1750678,1265424,818076,1593361,1526043,1571009,1561451,1084673,1200888,1584176,1055829,1771149,932552,1672257,1133266,1695291,1421595,1164930,1599474,1100435,1772430,1622975,1774246,1692481,1528027,1456479,1774873,1144410,1594854,1746977,1745488,1336147,1713937,1578435,1723321,701008,448599,910434,1703921,1639662,1255989,1556646,1486067,900138,578787,825841,1676215,1179387,1088586,1066659,1754961,1741547,1773222,1624284,936339,1188235,1610871,1674029,1470853,1131228,1459687,1751686,1690936,1569381,1745601,1767581,1024037,1479430,1608337,1607753,1677606,1123851,1772291,1719547,662589,1460797,1714415,1048193,1704152,1098684,799796,1769163,1762416,1266136,971148,1623207,1307977,1695719,1563057,1509194,1497953,1761759,630662,1503830,1605821,1300371,1397157,1761220,1655173,1100010,1681577,1507411,1701587,1088653,1038891,1535108,1709965,1270286,1666367,580616,1610342,1537263,1144995,638175,965410,1131031,360657,1759249,1405168,469799,1441877,1486890,1710499,507668,1647840,1763223,1613898,1625819,1416598,836545,1120076,1084237,1487204,478116,1553995,1183149,1369592,1754787,1590514,522837,1763890,1718474,1148429,1613047,1757558,1097683,1739153,1270629,1079708,959286,721458,1235967,1018361,1494746,1587917,1008704,855806,1266499,1454677,1145058,1749691,1671940,1286113,1690424,1545284,1633433,1199083,934846,713947,1622823,1511473,1749018,1743051,1095363,1737715,1716323,1406774,1083712,1677969,319081,1544403,1604854,1724222,1499518,1729407,1072931,1521252,437289,1328360,1587333,1190553,1418257,666411,840578,1684556,1544866,1613930,473184,1130195,970342,1033070,1778719,1715347,1709665,1778885,1749838,1778426,903152,1777083,1589484,562540,1778411,1353639,1080452,1567879,967614,1700343,981088,1753169,1690642,1742084,1710402,1764767,1620266,662367,941223,1779679,1509372,1748243,556052,1658469,1779407,987452,1571859,1756894,879486,961048,1729507,960119,1072282,965170,1520451,1384382,1549639,1779501,823025,1634820,1777613,518631,1439140,1738785,1505704,1070320,1534050,1574799,586363,1755994,1278436,1005515,1779523,1773821,1489470,1704898,1466430,1736897,1773620,1502856,1090573,1647812,1268458,1622621,1747749,1565180,1618075,487503,1435349,1534782,1671515,1083648,1737232,1753372,944290,1526442,1614862,1771069,1698992,798048,1302350,1500898,1614450,1778224,1771885,1230189,1331324,1566464,904274,1265815,1338451,1738607,874756,1044079,1475924,1544367,1572650,1003318,484926,1496258,1746823,1270783,831621,1563598,1777891,1404293,1517608,1742538,1778220,1325445,1444936,1719612,1449971,1386448,1506820,1431492,1668454,675521,675984,1649258,1763292,1756978,1492590,1579298,1193243,1640453,1460227,1082750,1778128,1418206,1636010,1312873,1456702,1753219,1082255,784857,1526363,1567134,758019,1003646,1359927,1711369,1114189,1761903,1159714,1778089,1151906,1756700,485049,450461,1741577,1015163,1778204,1657976,1773318,766905,1287482,1595631,1545771,472813,750962,1770510,1424499,1751393,555687,1777953,1069930,1399975,936918,1624158,1766769,1511579,1658425,1671624,1117505,1472315,617465,1388331,786943,483480,1338509,1723965,1762558,1755108,1754486,1708753,1708916,1165548,1487381,1284331,986348,1578284,932199,1191079,930766,1767498,318893,1398003,999588,1774605,1742397,1777170,1566734,709500,977418,1681252,1638438,1749634,1341173,1191649,1291438,1663043,1652227,1721918,1472707,1703097,1718186,967613,1515374,1575527,1247149,1695861,1620450,1762021,1616522,1775815,946740,1446874,812012,877170,1762571,1657148,1775563,1101496,1626597,1292292,1645181,1346544,1195513,1137153,563954,1106307,1757170,1607090,1723236,1427391,1688291,1351800,1296578,1276754,1413489,1774631,1263358,1156191,1387297,1775298,1326754,1481370,1746915,948333,915976,1464552,1489790,1133256,1750438,1512118,1414953,1737273,1004250,901274,1316888,1662419,1716210,1406011,1767742,1774297,1583655,1774204,889907,1079800,507841,555754,1486935,1492760,324748,707589,1265959,1379431,1431472,1648838,1774043,1742120,1494886,1715621,1630097,1595232,1773742,1667442,455881,1773570,1765719,1605302,1772829,1762664,1271337,1481288,1240112,1236564,1270615,1295266,1378307,1318686,1773187,822593,1454281,1773220,1597363,1629597,665307,1362471,1656614,1645919,1069045,1289432,1647259,659973,1126218,1362139,1618237,1769812,1668117,1419617,1737656,1644489,1722411,1771994,1701519,598846,1742844,1577277,1076001,1030177,1617453,922900,1632338,1200188,1326904,1112207,1275719,584908,1715011,955480,1771155,1617044,1014029,1770469,1730975,991521,1767969,1621758,1605935,1614187,767292,1152938,1508566,1670283,1065809,1619787,1592535,1137471,1770005,1739401,1194636,1395483,1540693,957067,1553828,1749921,1722094,1575643,1067628,1668829,1588893,469324,1768644,1108465,1768074,1685570,1529257,728416,670767,1092513,1663642,1767170,1192770,1549787,1060383,1539035,1557790,1537106,1594017,740725,953863,1753810,1096244,1282532,1477737,1608005,1691439,469167,1600125,1711908,1748022,1015279,641805,1687507,1766015,926140,1366539,1410409,1433757,1689438,1622566,1065585,1238433,1670127,1568539,1608459,1596496,1140592,984062,1755999,1511759,552269,537696,1476905,1764246,1747460,1682428,1722946,922971,1708222,1156920,1473297,1456865,1726411,1481808,1652029,1763010,1448118,1070120,1633651,1761860,950485,1246905,1723492,1080472,1702548,1498137,955856,1535135,1089470,947437,1121236,1218765,1585103,1467732,1671655,1612877,1497237,1762042,876981,1127215,1355680,1517560,1155757,1761672,1578344,1718378,1707747,1703234,1305417,1123971,1711718,1492930,1704111,1746838,1745878,1572308,1027705,1119021,1670309,1678787,552974,1491056,1056076,1739715,1499844,1212256,1744412,1703919,1452967,1730160,1536202,1751417,1754343,1753836,1623049,1532599,1582499,1582519,1111392,1682367,1697726,1242399,1678506,1618305,1683476,1628805,1748437,1595040,1637767,1633990,992876,1707348,1113617,1743988,1559946,1706870,1741513,1685160,650119,1439575,1643157,1302299,1658654,1600177,1499831,1192997,1743525,1009135,1717480,1670108,1282817,1668823,1055130,1408797,1643129,1725711,1173792,1740562,1436093,1644688,1293988,1712547,1565012,1676742,1358303,1732835,1617888,1468122,1242991,1040091,1711233,1712064,1680812,1708175,1645564,1548647,1160605,1056592,1571167,1534898,951177,1683172,1124953,1107948,1474034,623597,1718101,1034064,1405148,991184,1701359,1157418,1185065,1557131,1706509,1385965,1484583,1477785,1598123,1107414,1261144,505254,1078965,1763359,1603690,1711639,1098604,832159,1210592,1541813,968846,1552535,1088407,1648119,1687260,1770180,1740196,955169,1778210,1139206,1354144,1554035,1762667,1043464,1006277,1773257,1193942,1494787,1066701,1762573,948536,948536,1041403,1533143,925523,1574908,1666435,1194880,1266248,1686699,1755629,1671032,1773111,1484192,1352432,1415740,900103,1755786,1769923,435824,1256933,1606526,1775371,1141623,1759556,1408645,1724441,1522192,874115,1771826,991381,1772249,1523698,1112082,1751652,1583549,1628606,1751056,1115348,1174388,1748979,1292673,900072,1660296,1688013,1079456,1677309,1389189,1694841,1063484,1558479,1448889,1757465,1416471,1490294,1542132,1406109,305301,1435553,1062336,1687316,1117419,1072588,1433970,1769347,1769952,1770135,1584316,1637464,1722878,1766168,1520677,1757461,1641192,1605491,1710897,1239235,1566890,1046253,1220174,1724230,1030091,1688418,1694791,1765349,1545697,1770910,1054092,1753111,1159404,414333,1293115,1173685,1434374,877920,1767175,1569985,1728101,1261155,1600028,1603192,1648489,1048179,1506170,1164773,757135,905948,1529803,1516517,1658646,1298619,1769561,1394197,1010660,1749475,1749669,1493614,1757630,1697599,1664107,1046672,510604,1761010,1700463,653473,1406483,1749446,457073,1668643,1362683,1764760,1728654,562338,1745058,1765693,1771396,1115134,1348106,1756837,1739292,1550617,1467219,1542251,983397,1057646,1207133,1775535,1638773,1778031,1593351,1610492,1693009,947083,1549128,1209336,1262307,1776852,815524,1604925,1165690,1663998,1096643,1523638,1663467,1663482,1730256,1513125,1223613,1455820,1764583,1055854,1771996,1771933,1723159,1714201,1765712,1246076,1600882,1340747,1678453,1695731,1485317,1377465,1770125,914718,1773729,1747321,1767357,1758811,1742396,1570084,446412,1006212,1768854,1703018,310073,1564635,1472763,1643639,362281,1639564,1739056,1493487,391741,1615779,1538701,1773401,911181,1742027,1229944,1394232,1084569,1774604,1764262,1772159,945199,1761761,1460964,1665414,1640644,1767138,1712060,863018,1713123,547293,1732855,1688173,1775140,937704,476084,1769129,1055556,1170429,1708733,943076,1357686,1774168,1775019,1736861,658721,1693857,1530331,1640100,1742317,1740410,750274,1473660,1545001,904906,617514,1171230,1671065,1048070,1765654,1444816,1752631,1776067,1726321,1775940,1643847,1625552,1775558,1076689,1629487,1667676,1219722,904755,1336011,1444470,1420318,807626,1418266,1209107,1375757,1737574,653786,1774872,1419839,555452,1356250,1763419,1742784,1600974,1731269,1716999,1135191,1117942,1302119,1773415,1735168,395288,1133654,1570447,1621616,1091240,1405621,1587016,1364126,1774428,1291637,1025488,1457175,1176244,1734602,1688229,1652124,1089976,1770235,1454084,1756145,1685133,1772600,988094,1082380,1237214,1668064,1327413,1691416,1450858,1725721,867219,896698,1668987,1600330,1688431,1659566,1068068,1773450,1773238,683997,1773534,1203597,1128791,1167752,1756750,1677461,1574897,1670636,1751919,1734090,1699958,1496466,1412513,1728732,1772677,991274,1495020,1710230,1707442,1685337,1075655,1771576,1686260,1735570,1627910,1765906,1635555,1770622,1659723,1740161,1764750,1744171,1691842,1771423,1258011,1704761,901733,1686732,1594799,1544414,660709,1396496,1744153,1756549,1769481,1762406,1513678,1213997,1408254,1290323,1766043,1730370,1770233,1578768,1095109,1765845,1654496,795862,1691441,1454538,970327,1117203,1728812,1747885,1769967,850662,1770028,1118031,1629486,1704001,1769891,1769576,1741009,1731670,1754726,1769297,1654867,1736609,1763846,659762,1625824,930115,1435561,1757555,1496605,1752524,1214368,1764410,1754743,1744498,1152956,1700047,1129137,1621470,1689100,1089527,1758889,1488508,1668591,1741635,1342143,1096105,1598849,1356492,1668109,1735551,1422818,843290,1287784,1747059,1617074,1370978,1476968,1765655,1574685,1047098,1758251,1648836,1763714,1764950,894412,1471891,1303630,1740442,1443365,1606149,1624548,1406255,1765732,1763427,1646455,1128532,1397013,729092,1708844,1765185,695801,723209,1697769,1588024,1642286,1679500,1761304,1764842,1765078,1727590,1763687,1764857,1261616,1763367,1648748,954767,1763668,697856,1522712,1501174,1680090,1763585,1492095,1061628,1353815,1755431,1763333,1622736,1353250,868957,1687550,1447093,1716696,544409,1582361,1761452,1688723,1523825,1762452,1522455,1513811,1591555,1130908,1761696,1122518,1275165,607866,994716,1690970,1752743,912956,1666216,1602829,1570082,1689083,1753323,1686537,1497246,1272989,1662132,985764,1475067,1543881,1563409,1584283,1705683,1602921,1567810,1757467,1378595,1714439,1584160,1760162,1666885,985664,1468506,1717859,1689819,1323187,1509333,1014460,1759255,1034458,1747720,1480295,1715683,1648308,1570333,1044788,1302091,1565569,1732961,974050,1326171,1739579,1589848,1690156,1122670,1703902,986587,1096540,1686855,1410896,1169551,653515,935075,1755631,1732080,1724045,958659,1755282,1443401,1708663,1299864,1740227,922844,1646062,1358713,1501842,1142577,1228786,650324,1710687,1675062,1684406,1697014,1330913,1645214,1713154,1311225,1700633,1725405,1744692,1575747,651628,665814,672384,1656178,758998,1611532,1480251,1684122,1571582,1189842,1659319,1747525,1672195,1478777,1502659,1488010,1524120,1738655,1462735,703824,1419913,1502466,453169,1186735,1603021,1530100,1291872,956537,1604756,1619332,1587779,1567507,1070880,1135050,1405194,1139090,1037124,1331592,1614563,1077822,1647847,1722052,1729189,1745572,1443514,1741362,1573821,1597397,1679705,1055853,1372312,1581644,863928,1739029,1742883,644995,1719821,488409,1648096,1434769,1672179,1150449,1731483,1716889,707015,1735574,1650848,1255647,1409601,1712254,1741104,1628475,1478444,1676368,951659,1599689,1597395,1603620,845081,698280,1167979,1721209,1661365,1499783,1029286,1622807,1603757,1492402,1585284,1241302,1081539,1739097,454971,1608039,1531308,673255,1162627,1651226,1704975,645050,1693233,1514304,1129406,1384415,944580,627635,1257384,1687695,1385548,1646873,1717934,1732858,1729570,754348,1669895,1580501,1719863,1554378,1219899,1389291,945869,1146882
    ];

    /** @var array */
    private static $page_404_list                           =[];

    /** @var string */
    private static $token;

    /** @var string */
    private static $cookie;

    /** @var string */
    private static $x_token;

    /** @var string */
    private static $host                                    ='https://emilydates.com';

    /** @var string */
    private static $url_profile                             ='profile';

    /** @var string */
    private static $url_search                              ='page';

    /** @var string */
    private static $dir_path                                ='Temp/EmilyDates';

    /** @var string */
    private static $cookie_path                             ='Temp/Parsing/EmilyDates/reverse_ip.txt';

    /** @var string */
    private static $url_api                                 ='https://shluham.net/api/json';

    /** @var string */
    private static $api_add_face_data_action                ='add_face_data';

    /** @var string */
    private static $api_add_face_image_action               ='add_face_image';

    /** @var string */
    private static $api_get_face_ID_action                  ='get_face_ID';

    /** @var string */
    private static $api_update_face_to_public_action        ='update_face_to_public';

    /** @var string */
    private static $key_hash                                ='domino777';

    /** @var string */
    public  static $file_log_path                           ='';

    /** @var int  */
    private static $memory_usage                            =0;

    /** @var int */
    private static $image_min_len                           =1;

    /** @var int */
    private static $age_min                                 =0;

    /** @var int */
    private static $age_max                                 =50;

    /** @var int */
    private static $profile_index                           =0;

    /** @var int */
    private static $file_log_size_max                       =10485760;

    /** @var array */
    private static $sleep_profile_len_list                  =[100,200];

    /** @var array */
    private static $sleep_after_profile_list                =[40,60];

    /** @var array */
    private static $sleep_random_list                       =[10,40];

    /**
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function sleep_random(){

        $rand=rand(self::$sleep_random_list[0],self::$sleep_random_list[1]);

        self::add_to_log("SLEEP: ".$rand." ".Date::get_date_time_full()."\n");

        return sleep($rand);

    }

    /**
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function sleep_after_profile_uploaded(){

        $rand=rand(self::$sleep_after_profile_list[0],self::$sleep_after_profile_list[1]);

        self::add_to_log("SLEEP after parse profile: ".$rand." ".Date::get_date_time_full()."\n");

        return sleep($rand);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_log_path(){

        self::$dir_path=DIR_ROOT.'/'.self::$dir_path;

        if(!file_exists(self::$dir_path))
            Dir::create_dir(self::$dir_path);

        self::$file_log_path=DIR_ROOT.'/'.DirConfigProject::$dir_parsing_log;

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/EmilyDates';

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/emily_dates_'.time().'.log';

//        self::add_to_log('Created log path');

        return true;

    }

    /**
     * @param $data
     * @return bool|int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_to_log($data){

        $memory         =round((memory_get_usage()/(1024*1024)),3);
        $memory_peak    =round((memory_get_peak_usage()/(1024*1024)),3);

        if($memory>self::$memory_usage){

            $old_memory             =self::$memory_usage;
            self::$memory_usage     =$memory;

            self::add_to_log('############## -> Memory usage update from '.$old_memory.' to '.self::$memory_usage.'/'.$memory_peak.'mb'."\n");

        }

        echo print_r($data,true);

        if(file_exists(self::$file_log_path))
            if(filesize(self::$file_log_path)>self::$file_log_size_max)
                self::set_log_path();

        if(!empty($_POST['need_log']))
            return file_put_contents(self::$file_log_path,$data,FILE_APPEND);

        return false;

    }

    /**
     * @return bool
     */
    private static function set_cookie_path(){

        self::$cookie_path=DIR_ROOT.'/'.self::$cookie_path;

        return true;

    }

    /**
     * @param string|NULL $source_account_link
     * @return |null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_get_face_ID(string $source_account_link=NULL){

        if(empty($source_account_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source account link is empty'
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_get_face_ID_action;
        $data       =[
            'source_account_link'   =>$source_account_link,
            'key_hash'              =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face ID was not give',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if($r['status']!=200){

            self::add_to_log("--> restart send get face ID: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_get_face_ID($source_account_link);

        }

        if(isset($r['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if(empty($r['data']))
            return NULL;

        if(!Json::is_json($r['data']))
            return NULL;

        $data=Json::decode($r['data']);

        if(isset($data['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        return $data['data']['face_ID'];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $source_ID
     * @param int|NULL $image_ID
     * @param int|NULL $city_ID
     * @param string|NULL $source_account_link
     * @param string|NULL $name
     * @param int|NULL $age
     * @param string|NULL $city
     * @param string|NULL $info
     * @return bool|null
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_add_face_data(int $face_ID=NULL,int $source_ID=NULL,int $image_ID=NULL,int $city_ID=NULL,string $source_account_link=NULL,string $name=NULL,int $age=NULL,string $city=NULL,string $info=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($source_ID))
            $error_info_list[]='Source ID is empty';

        if(empty($source_account_link))
            $error_info_list[]='Source account link is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_add_face_data_action;
        $data       =[
            'face_ID'                   =>$face_ID,
            'source_ID'                 =>$source_ID,
            'image_ID'                  =>$image_ID,
            'city_ID'                   =>$city_ID,
            'source_account_link'       =>$source_account_link,
            'name'                      =>$name,
            'age'                       =>$age,
            'city'                      =>$city,
            'info'                      =>$info,
            'key_hash'                  =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face data was not add',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if($r['status']!=200){

            self::add_to_log("--> restart send add face data: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_add_face_data($face_ID,$source_ID,$image_ID,$city_ID,$source_account_link,$name,$age,$city,$info);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $facekit_image_ID
     * @param int|NULL $file_size
     * @param string|NULL $file_content_type
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param string|NULL $file_path
     * @param array|NULL $face_coords
     * @return |null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_add_face_image(int $face_ID=NULL,int $facekit_image_ID=NULL,int $file_size=NULL,string $file_content_type=NULL,string $file_mime_type=NULL,string $file_extension=NULL,string $file_path=NULL,array $face_coords=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_add_face_image_action;
        $data       =[
            'face_ID'               =>$face_ID,
            'facekit_image_ID'      =>$facekit_image_ID,
            'file_size'             =>$file_size,
            'file_content_type'     =>$file_content_type,
            'file_mime_type'        =>$file_mime_type,
            'file_extension'        =>$file_extension,
            'image_base64'          =>base64_encode(file_get_contents($file_path)),
            'face_len'              =>count($face_coords),
            'face_coords'           =>empty($face_coords)?'[]':Json::encode($face_coords),
            'key_hash'              =>self::$key_hash,
            'time'                  =>time().'_'.rand(0,time())
        ];

        $r=CurlPost::init($url,[],$data);

        self::add_to_log("--> ERROR: ".print_r($r,true)."\n");

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face image was not add',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if($r['status']==500||$r['status']==500){

            self::add_to_log("--> request: ".Date::get_date_time_full()."\n");
            self::add_to_log(print_r($r,true)."\n");
            self::add_to_log(print_r($data,true)."\n");
            self::add_to_log("--> restart send add face image: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_add_face_image($face_ID,$facekit_image_ID,$file_size,$file_content_type,$file_mime_type,$file_extension,$file_path,$face_coords);

        }
        else if($r['status']!=200)
            return NULL;
        else{

            if(empty($r['data']))
                return NULL;

            if(!Json::is_json($r['data']))
                return NULL;

            $data=Json::decode($r['data']);

            if(isset($data['error'])){

                self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

                return NULL;

            }

            return $data['data']['image_ID'];

        }

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return bool|null
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_update_face_to_public(int $face_ID=NULL,int $image_ID=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_update_face_to_public_action;
        $data       =[
            'face_ID'               =>$face_ID,
            'image_ID'              =>$image_ID,
            'key_hash'              =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face was not update to public',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if($r['status']!=200){

            self::add_to_log("--> restart send update face to public: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_update_face_to_public($face_ID,$image_ID);

        }

        return true;

    }

    /**
     * @param string|NULL $source_account_link
     * @return int|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_face_ID_from_source_account_link(string $source_account_link=NULL){

        if(empty($source_account_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source account link is empty'
            ];

            throw new ParametersException($error);

        }

        return FaceData::get_face_ID_from_source_account_link($source_account_link);

    }

    /**
     * @param string|NULL $file_extension
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_file(string $file_extension=NULL){

        return File::add_file_without_params($file_extension,true,false,false);

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_hash
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_file(int $file_ID=NULL,string $file_hash=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_content_type=NULL,string $file_extension=NULL){

        return File::update_file($file_ID,$file_hash,1,1,$file_size,$file_size,$file_mime_type,$file_content_type,$file_extension,true,false,true);

    }

    /**
     * @param string|NULL $file_path
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_file(string $file_path=NULL){

        if(empty($file_path))
            return NULL;

        self::add_to_log("start get file from url: ".$file_path." ".Date::get_date_time_full()."\n");

//        $image=@file_get_contents($file_path);

//        $r=CurlPost::init($file_path,[],UseragentStack::get_random_useragent());
//        $r=CurlPost::init($file_path,[],NULL,true);

        $image=file_get_contents($file_path);

//        $r=CurlPost::init($file_path,[],[],false,self::$cookie_path);

//        if($r['status']!=200)
//            return NULL;

//        $image=$r['data'];

        self::add_to_log("file downloaded: ".$file_path." ".Date::get_date_time_full()."\n");

        if($image===false)
            return NULL;

        self::add_to_log("try get file extension: ".$file_path." ".Date::get_date_time_full()."\n");

        $file_name          =basename($file_path);
        $query              =parse_url($file_path, PHP_URL_QUERY);
        $file_name          =str_replace('?'.$query,'',$file_name);
        $file_extension     =File::get_file_extension_from_file_name($file_name);

        self::add_to_log("got file extension: ".$file_path." ".Date::get_date_time_full()."\n");

        $file_ID=self::add_file($file_extension);

        self::add_to_log("create file row in DB: ".$file_path." ".Date::get_date_time_full()."\n");

        if(empty($file_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new PhpException($error);

        }

        $file_dest_path=File::get_file_path_from_file_ID($file_ID,true);

        File::get_file_dir($file_ID,NULL,Date::get_date_time_full());
        FileParametersCash::add_file_parameter_in_cash($file_ID,'file_extension',$file_extension);

        self::add_to_log("try put file on the drive: ".$file_path." ".Date::get_date_time_full()."\n");

//        $query              =parse_url($file_dest_path, PHP_URL_QUERY);
//        $file_dest_path     =str_replace('?'.$query,'',$file_dest_path);

        if(file_put_contents($file_dest_path,$image)===false){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not save'
            ];

            throw new PhpException($error);

        }

        self::add_to_log("check min file size: ".$file_path." ".Date::get_date_time_full()."\n");
        self::add_to_log("file size: ".filesize($file_dest_path)." ".Date::get_date_time_full()."\n");

        if(filesize($file_dest_path)<=16000){

            self::add_to_log("BAD FILE: ".$file_path." ".Date::get_date_time_full()."\n");

            return NULL;

        }

//        if(!OsServer::$is_windows){
//
//            self::add_to_log("try create temp image file: ".$file_path." ".Date::get_date_time_full()."\n");
//
//            $temp_image_path='Temp/ModelZoneParsing/img_'.time();
//
//            try{
//
//                self::add_to_log("try open Imagick: ".$file_path." ".Date::get_date_time_full()."\n");
//
//                $img=new \Imagick($file_dest_path);
//                self::add_to_log("read image with Imagick: ".$file_path." ".Date::get_date_time_full()."\n");
//                $img->readImage($file_dest_path);
//                self::add_to_log("try write image with Imagick: ".$file_path." ".Date::get_date_time_full()."\n");
//                $img->writeImage($temp_image_path);
//                self::add_to_log("temp image file created: ".$file_path." ".Date::get_date_time_full()."\n");
//
//            }
//            catch (\ImagickException $e){
//
//                self::add_to_log("create temp file ERROR: ".print_r($e)." ".Date::get_date_time_full()."\n");
//
//                return NULL;
//
//            }
//
//            unset($img);
//
//            unlink($file_dest_path);
//
//            copy($temp_image_path,$file_dest_path);
//
//            unlink($temp_image_path);
//
//        }

        self::add_to_log("try get image object from file path: ".$file_path." ".Date::get_date_time_full()."\n");

        $image_object=Image::get_image_object_from_file_path($file_dest_path);

        if(empty($image_object)){

            self::add_to_log("FILE ERROR: ".$file_path." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        $size       =getimagesize($file_dest_path,$im_info);
        $sum        =$size[0]*$size[1];

        if($sum!=0){

            $file_size  =filesize($file_dest_path);
            $cof        =$file_size/$sum;

        }
        else
            $cof=0;

        self::add_to_log("-> file cof: ".$cof." ".Date::get_date_time_full()."\n");

        if($cof<.01){

            self::add_to_log("BAD FILE SUM: ".$file_path." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        switch(exif_imagetype($file_dest_path)){

            case 1:
            case 2:
            case 3:
            case 7:
            case 8:
                break;

            default:
                return NULL;

        }

        $file_mime_type         =File::get_file_mime_type_from_path($file_dest_path);
        $file_content_type      =File::get_file_content_type_from_path($file_dest_path);
        $file_size              =filesize($file_dest_path);
        $data_list              =[
            User::$user_ID,
            $file_size,
            $file_mime_type,
            $file_content_type,
            time(),
            rand(0,time())
        ];
        $file_hash              =Hash::get_sha1_encode(implode(':',$data_list));

        if($file_size==0){

            File::remove_file_ID($file_ID);

            self::add_to_log("--> file is not exists: ".$file_size." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if(!self::update_file($file_ID,$file_hash,$file_size,$file_mime_type,$file_content_type,$file_extension)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not update'
            ];

            throw new PhpException($error);

        }

        return[
            'file_ID'               =>$file_ID,
            'file_content_type'     =>$file_content_type,
            'file_extension'        =>$file_extension
        ];

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_image(int $file_ID=NULL,string $file_content_type='image',string $file_extension=NULL){

        return ImageUploadedPrepare::init($file_ID,$file_content_type,$file_extension);

    }

    /**
     * @param array $image_list
     * @return array
     */
    private static function search_face_in_image_list(array $image_list=[]){

        return $image_list;

    }

    /**
     * @param string|NULL $source_account_link
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_link(string $source_account_link=NULL){

        $face_ID=self::get_face_ID_from_source_account_link($source_account_link);

        return !empty($face_ID);

    }

    /**
     * @param string|NULL $source_account_link
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face(string $source_account_link=NULL){

        $face_ID=self::get_face_ID_from_source_account_link($source_account_link);

        if(empty($face_ID))
            $face_ID=Face::add_face();

        return[
            'face_ID'=>$face_ID
        ];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @param string|NULL $name
     * @param string|NULL $source_account_link
     * @param int $image_len
     * @param int|NULL $age
     * @param string|NULL $city
     * @param string|NULL $info
     * @param int|NULL $city_ID
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_data(int $face_ID=NULL,int $image_ID=NULL,string $name=NULL,string $source_account_link=NULL,int $image_len=0,int $age=NULL,string $city=NULL,string $info=NULL,int $city_ID=NULL,string $link_ID=NULL){

        $face_data_ID=FaceData::get_face_data_ID($face_ID);

        if(empty($face_data_ID))
            $face_data_ID=FaceData::add_face_data($face_ID,$city_ID,$image_ID,self::$source_ID,$source_account_link,$name,$age,$city,$info,$image_len,0,NULL,2,$link_ID);

        return[
            'face_data_ID'=>$face_data_ID
        ];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_data_image(int $face_ID=NULL,int $image_ID=NULL){

        return FaceData::update_face_data_image_ID($face_ID,$image_ID);

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face(int $face_ID=NULL){

        return Face::update_face($face_ID);

    }

    /**
     * @param int|NULL $face_data_ID
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_data(int $face_data_ID=NULL){

        return FaceData::update_face_data_to_public($face_data_ID);

    }

    /**
     * @param int|NULL $face_ID
     * @param array $image_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function upload_image_to_facekit_db(int $face_ID=NULL,array $image_list=[]){

        if(count($image_list)==0)
            return[];

        $image_ID       =NULL;
        $api_image_ID   =NULL;

        foreach($image_list as $image_index=>$image_data){

            self::add_to_log("-> start prepare image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

            $is_added=false;

            $image_list[$image_index]['face_image_ID']      =NULL;
            $facekit_image_ID                               =NULL;

            if(!$is_added){

                self::add_to_log("--> start add to local face image image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

                $image_list[$image_index]['face_image_ID']=FaceImage::add_face_image($face_ID,$image_data['file_ID'],$image_data['image_ID'],NULL,NULL,[]);

                self::add_to_log("--> added to local face image: ".$image_list[$image_index]['face_image_ID']." image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

            }

            self::add_to_log("-> finish upload image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

        }

        return[
            'image_ID'          =>empty($image_ID)?$image_list[0]['image_ID']:$image_ID,
            'image_list'        =>$image_list
        ];

    }

    /**
     * @param string|NULL $data
     * @return array
     */
    private static function get_body_params(string $data=NULL){

        $list=[];

        preg_match_all('/(\d+)\s*см/is',$data,$param_list);

        if(count($param_list)>0)
            if(count($param_list[1])>0)
                if(!empty($param_list[1][0])){

                    $height=(int)$param_list[1][0];

                    if($height<=200&&$height>=155)
                        $list['height']=$height;

                }

        preg_match_all('/(\d+)\s*кг/is',$data,$param_list);

        if(count($param_list)>0)
            if(count($param_list[1])>0)
                if(!empty($param_list[1][0])){

                    $weight=(int)$param_list[1][0];

                    if($weight<=80&&$weight>=40)
                        $list['weight']=$weight;

                }

        preg_match_all('/\,\s*(с(?:о|\s*)\s*(?:первым|вторым|третьим|четвертым|пятым)\s*размером груди)/is',$data,$param_list);

        if(count($param_list)>0)
            if(count($param_list[1])>0)
                if(!empty($param_list[1][0]))
                    switch(trim($param_list[1][0])){

                        case'с первым размером груди':{

                            $list['boobs']=1;

                            break;

                        }

                        case'со вторым размером груди':{

                            $list['boobs']=2;

                            break;

                        }

                        case'с третьим размером груди':{

                            $list['boobs']=3;

                            break;

                        }

                        case'с четвертым размером груди':{

                            $list['boobs']=4;

                            break;

                        }

                        case'с пятым размером груди':{

                            $list['boobs']=5;

                            break;

                        }

                        case'с шестым размером груди':{

                            $list['boobs']=6;

                            break;

                        }

                    }

        return $list;

    }

    /**
     * @param string|NULL $profile_item_ID
     * @param string|NULL $name
     * @param int|NULL $age
     * @param int|NULL $weight
     * @param int|NULL $height
     * @param int|NULL $boobs
     * @param string|NULL $phone
     * @param int|NULL $price_1
     * @param int|NULL $price_2
     * @param int|NULL $price_3
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile_link(string $profile_item_ID=NULL){

        $profile_link=self::$host.'/'.self::$url_profile.'/'.$profile_item_ID;
//        $profile_link='https://emilydates.com/profile/1176119';

        if(self::isset_face_link($profile_link)){

            self::add_to_log("WARNING: Already exists".$profile_link." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        self::add_to_log("Set profile link: ".$profile_link." ".Date::get_date_time_full()."\n");

//        $r=CurlGet::init($profile_link,[],NULL,false,[],[],self::$cookie_path);
        $r=CurlPost::init($profile_link,[],[],false,self::$cookie_path);

        if($r['status']!=200)
            SourcePageEmpty::add_source_page_empty(self::$source_ID,$profile_link,$profile_item_ID,$r['status']);

        if($r['status']==504){

            self::add_to_log("Cloudflare restart profile link: ".$profile_link." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if($r['status']==404){

//            self::$page_404_list[]=$profile_item_ID;

            self::add_to_log("404 error: ".$profile_link." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        self::add_to_log("Got profile link status: ".$r['status']." ".Date::get_date_time_full()."\n");

        if($r['status']!=200){

            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            self::add_to_log("ERROR: ".$r['status']." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        $status     =$r['status'];
        $r          =$r['data'];

//        $r=mb_convert_encoding($r,'utf-8', "windows-1251");

        if($r!==false){

            $body=preg_match_all('/<body.*?>(.*?)<\/body>/is',$r,$body_list);

            if($body===false){

                self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                self::add_to_log("ERROR body is empty ".Date::get_date_time_full()."\n");

            }

            if($body!==false)
                if(count($body_list[1])>0){

                    $data=[
                        'id'            =>$profile_item_ID,
                        'link'          =>$profile_link,
                        'name'          =>NULL,
                        'age'           =>NULL,
                        'country_ID'    =>NULL,
                        'city_ID'       =>NULL,
                        'city'          =>NULL,
                        'info'          =>[],
                        'image_list'    =>[]
                    ];

                    preg_match_all('/<div\s*class=\"\s*pstrip__link\s*js_popup\-btn\s*\"\s*data\-url=.*?data\-dest=\"popup\s*small\"\s*data\-verif\-text.*?data\-src=\"(.*?)\"\s*>\s*<img/is',$body_list[1][0],$list);

                    if(count($list)==0){

                        self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        self::add_to_log("Image links is empty ".Date::get_date_time_full()."\n");

                        SourcePageEmpty::add_source_page_empty(self::$source_ID,$profile_link,$profile_item_ID,$status);

                        return NULL;

                    }

                    if(count($list[1])==0){

                        SourcePageEmpty::add_source_page_empty(self::$source_ID,$profile_link,$profile_item_ID,$status);

                        self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        self::add_to_log("List count !=2 ".Date::get_date_time_full()."\n");

                        return NULL;

                    }

                    $image_list=[];

                    foreach($list[1] as $link)
                        if(mb_strlen($link,'utf-8')>5){

                            $link=str_replace(' ','%20',$link);

                            $image_list[]=$link;

                        }

                    if(count($image_list)<self::$image_min_len){

                        self::add_to_log('Image count less than '.self::$image_min_len);

                        SourcePageEmpty::add_source_page_empty(self::$source_ID,$profile_link,$profile_item_ID,$status);

                        return NULL;

                    }

                    $data['image_list']=$image_list;

                    preg_match_all('/<div\s*class=\"profile\-info__main\"\s*>\s*<span\s*class=\"profile\-info__name\s*js_main\-info\-name\"\s*>(.*?)<\/span>\s*<span\s*class=\"profile\-info__age\s*js_main\-info\-age\"\s*>,\s*(\d+)<\/span>\s*<\/div>/is',$body_list[1][0],$param_list);

                    if(count($param_list)>0)
                        if(count($param_list[1])>0)
                            if(!empty($param_list[1][0])){

                                $data['name']   =trim($param_list[1][0]);
                                $data['age']    =(int)$param_list[2][0];

                                if($data['age']<self::$age_min||$data['age']>self::$age_max){

                                    self::add_to_log('Image is not needed count'."\n");

                                    SourcePageEmpty::add_source_page_empty(self::$source_ID,$profile_link,$profile_item_ID,$status);

                                    return NULL;

                                }

                            }

                    preg_match_all('/<div\s*class=\"profile\-info__location\s*js_main\-info\-location\"\s*>(.*?),\s*(.*?)<\/div>/is',$body_list[1][0],$param_list);

                    if(count($param_list)>0)
                        if(count($param_list[1])>0)
                            if(!empty($param_list[1][0])){

                                $country_ID     =NULL;
                                $city_ID        =NULL;
                                $city           =NULL;
                                $country        =NULL;

                                if(!empty($param_list[1][0])){

                                    $city       =trim($param_list[1][0]);
                                    $city_ID    =CityLocalization::get_city_ID_from_name(mb_strtolower($city,'utf-8'));
                                    $city       =CityLocalization::get_city_name($city_ID,LangConfig::$lang_ID_default);

                                }

                                if(!empty($param_list[2][0])){

                                    $country        =trim($param_list[2][0]);
                                    $country_ID     =CountryLocalization::get_country_ID_from_name(mb_strtolower($country,'utf-8'));
                                    $country        =CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);

                                }

                                if(!empty($country_ID))
                                    $data['country_ID']=$country_ID;

                                if(!empty($city_ID)){

                                    $data['city_ID']=$city_ID;

                                    if(empty($country_ID)){

                                        $country_ID     =City::get_country_ID($city_ID);
                                        $country        =CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);

                                        $data['country_ID']=$country_ID;

                                    }

                                }


                                if(!empty($country)&&!empty($city))
                                    $data['city']=$country.', '.$city;
                                else if(empty($city))
                                    $data['city']=$country;

                            }

//                    preg_match_all('/<h3\s*class=\"user\-infoblock__title\"\s*>Цели знакомства<\/h3>\s*<div\s*class=\"user\-infoblock__content\s*user\-infoblock__content\-\-text\"\s*>(.*?)<\/div>/is',$body_list[1][0],$param_list);

//                    $need_profile=false;
//
//                    if(count($param_list)>0)
//                        if(count($param_list[1])>0)
//                            if(!empty($param_list[1][0])){
//
//                                $temp_list=mb_split('\,',$param_list[1][0]);
//
//                                if(count($temp_list)>0)
//                                    foreach($temp_list as $row)
//                                        if(!empty($row))
//                                            switch(mb_strtolower(trim($row),'utf-8')){
//
//                                                case'найти спонсора':
//                                                case'путешествовать вместе':
//                                                case'провести вечер':{
//
//                                                    $need_profile=true;
//
//                                                    break;
//
//                                                }
//
//                                            }
//
//                            }
//
//                    if(!$need_profile){
//
//                        self::add_to_log('Drop add profile. Not good points for meet. '.$profile_link."\n");
//
//                        SourcePageEmpty::add_source_page_empty(self::$source_ID,$profile_link,$profile_item_ID,$status);
//
//                        return NULL;
//
//                    }

                    $about_list=[];

                    preg_match_all('/<h3\s*class=\"user\-infoblock__title\"\s*>О себе<\/h3>\s*<div\s*class=\"user\-infoblock__content\s*user\-infoblock__content\-\-text\"\s*>\s*<div\s*class=\"user\-infoblock__about\s*js_profile\-about\"\s*>(.*?)<button/is',$body_list[1][0],$param_list);

                    if(count($param_list)>0)
                        if(count($param_list[1])>0)
                            if(!empty($param_list[1][0]))
                                $about_list[]=html_entity_decode(str_replace('<br />',"\n",trim($param_list[1][0])));

                    preg_match_all('/<h3\s*class=\"user\-infoblock__title\"\s*>Внешность<\/h3>\s*<div\s*class=\"user\-infoblock__content\s*user\-infoblock__content\-\-text\"\s*>(.*?)<\/div>/is',$body_list[1][0],$param_list);

                    if(count($param_list)>0)
                        if(count($param_list[1])>0)
                            if(!empty($param_list[1][0]))
                                $about_list[]=html_entity_decode(str_replace('<br />',"\n",trim($param_list[1][0])));

                    preg_match_all('/<div\s*class=\"user\-infoblock\"\s*>\s*<h3\s*class=\"user\-infoblock__title\"\s*>Личная информация<\/h3>\s*<div\s*class=\"user\-infoblock__content\s*user\-infoblock__content\-\-text\"\s*>(.*?)<\/div>/is',$body_list[1][0],$param_list);

                    if(count($param_list)>0)
                        if(count($param_list[1])>0)
                            if(!empty($param_list[1][0]))
                                $about_list[]=html_entity_decode(str_replace('<br />',"\n",trim($param_list[1][0])));

                    if(count($about_list)>0){

                        $body_data      =implode("\n",$about_list);
                        $body_list      =self::get_body_params($body_data);

                        if(count($body_list)>0){

                            if(isset($body_list['height']))
                                $data['info'][]='Рост: '.$body_list['height'];

                            if(isset($body_list['weight']))
                                $data['info'][]='Вес: '.$body_list['weight'];

                            if(isset($body_list['boobs']))
                                $data['info'][]='Бюст: '.$body_list['boobs'];

                        }

                        $data['info'][]='О себе: '.$body_data;

                    }

                    self::add_to_log("Profile data ".Date::get_date_time_full()."\n");
                    self::add_to_log(print_r($data,true)."\n");

                    return $data;

                }

        }

        return NULL;

    }

    /**
     * @param array $image_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_image_list(array $image_list=[]){

        if(count($image_list)==0)
            return[];

        $list=[];

        foreach($image_list as $link){

            $file_data=self::prepare_file($link);

            if(!empty($file_data)){

                $file_ID                            =$file_data['file_ID'];
                $file_extension                     =$file_data['file_extension'];
                $image_data                         =self::prepare_image($file_ID,'image',$file_extension);
                $image_path                         =$image_data['image_dir'].'/'.$image_data['image_item_ID_list']['large'];

                print_r($image_path);

                echo"\n\n";
                echo $image_path."\n\n";

                $image_data['file_path']            =$image_path;
                $image_data['file_size']            =filesize($image_path);
                $image_data['file_content_type']    ='image';
                $image_data['file_extension']       =$file_extension;
                $image_data['file_mime_type']       =File::get_file_mime_type_from_path($image_path);
                $image_data['image_resolution']     =Image::get_image_pixel_size_from_file_path($image_path);

                $key            =$image_data['image_resolution']['width'].':'.$image_data['image_resolution']['height'].':'.$image_data['file_size'].':'.$image_data['file_mime_type'];
                $list[$key]     =$image_data;

            }

        }

        return array_values($list);

    }

    /**
     * @param array|NULL $profile_data
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_profile(array $profile_data=NULL){

        if(empty($profile_data))
            return NULL;

        if(count($profile_data['image_list'])==0)
            return NULL;

        $profile_data['image_list']=self::get_image_list($profile_data['image_list']);

        if(count($profile_data['image_list'])==0)
            return NULL;

        self::add_to_log("Start create face data ".Date::get_date_time_full()."\n");

        $image_list         =self::search_face_in_image_list($profile_data['image_list']);
        $face_data          =self::prepare_face($profile_data['link']);
        $face_ID            =$face_data['face_ID'];
        $face_data          =self::prepare_face_data($face_ID,NULL,$profile_data['name'],$profile_data['link'],count($profile_data['image_list']),$profile_data['age'],$profile_data['city'],implode("\n",$profile_data['info']),$profile_data['city_ID'],$profile_data['id']);
        $face_data_ID       =$face_data['face_data_ID'];
        $image_data         =self::upload_image_to_facekit_db($face_ID,$image_list);

        self::update_face_data_image($face_ID,$image_data['image_ID']);
        self::update_face($face_ID);
        self::update_face_data($face_data_ID);

        self::add_to_log("FINISH create face data ".Date::get_date_time_full()."\n");

        return $profile_data;

    }

    /**
     * @param string|NULL $data
     * @return string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_token(string $data=NULL){

        preg_match_all("/<meta\s*name=\"csrf\-token\"\s*content=\"(.*?)\"\s*>/is",$data,$token_list);

        if(count($token_list)!=2){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Meta is empty'
            ];

            throw new ParametersException($error);

        }

        if(count($token_list[1])==0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Token is empty in meta'
            ];

            throw new ParametersException($error);

        }

        return trim($token_list[1][0]);

    }

    /**
     * @param string|NULL $data
     * @return array
     */
    public  static function get_cookie(string $data=NULL){

        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi',$data, $matches);

        $cookies=[];

        foreach($matches[1] as $item){

            parse_str($item,$cookie);

            $cookies=array_merge($cookies,$cookie);

        }

        return $cookies;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function reset_connect(){

        self::add_to_log("Reset connect to server ".Date::get_date_time_full()."\n");
        self::add_to_log("Clean cookie ".Date::get_date_time_full()."\n");

        file_put_contents(self::$cookie_path,'');

        self::add_to_log("get default url ".Date::get_date_time_full()."\n");

        $r=CurlPost::init(self::$host,[],[],false,self::$cookie_path,false);
//
        self::$cookie   =self::get_cookie($r['data']);
        self::$x_token  =self::get_token($r['data']);

        $r=CurlPost::init(self::$host,[],[],false,self::$cookie_path,false,NULL,NULL,self::$host);

        self::$x_token=self::get_token($r['data']);

        self::add_to_log("continue ".Date::get_date_time_full()."\n");
//        self::add_to_log("content: ".print_r($r,true)." ".Date::get_date_time_full()."\n");

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_random_profile_index(){

        self::$profile_index=rand(self::$profile_index_list[0],self::$profile_index_list[1]);

        if(
              FaceData::isset_link_ID(self::$source_ID,self::$profile_index)
            ||SourcePageEmpty::isset_source_page_empty(self::$source_ID,self::$profile_index)
        )
            return self::set_random_profile_index();

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_profile_links(){

        libxml_use_internal_errors(true);

        self::$profile_index=0;

        do{

            if(!isset(self::$profile_list[self::$profile_index])){

                self::add_to_log("DONE with profile index: ".self::$profile_index." ".Date::get_date_time_full()."\n");

                exit;

            }


            $profile_item_ID        =self::$profile_list[self::$profile_index];
            $profile_data           =self::prepare_profile_link($profile_item_ID);

            if(empty($profile_data)){

                self::add_to_log("Empty profile data with index: ".self::$profile_index." ".Date::get_date_time_full()."\n");

            }

            if(!empty($profile_data)){

                self::add_to_log("Start prepare profile data with index: ".self::$profile_index." ".Date::get_date_time_full()."\n");

                self::prepare_profile($profile_data);

                self::add_to_log("Finish prepare profile data: ".self::$profile_index." ".Date::get_date_time_full()."\n");

            }

            self::sleep_random();

            self::$profile_index++;

        }while(true);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        self::set_log_path();
        self::set_cookie_path();
        self::get_profile_links();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        if(empty($_POST['page'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Page is empty'
            ];

            throw new ParametersException($error);

        }

        self::$page=(int)$_POST['page'];

        return self::set();

    }

}