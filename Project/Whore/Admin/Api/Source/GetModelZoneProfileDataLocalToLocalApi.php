<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Curl\CurlGet;
use Core\Module\Date\Date;
use Core\Module\Dir\Dir;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Geo\City;
use Core\Module\Geo\CityLocalization;
use Core\Module\Geo\CountryLocalization;
use Core\Module\Lang\LangConfig;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Useragent\UseragentStack;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\FaceData;
use function Sodium\add;

class GetModelZoneProfileDataLocalToLocalApi{

    /** @var int */
    private static $start_face_ID;

    /** @var array */
    private static $face_list                               =[];

    /** @var string */
    private static $dir_path                                ='Temp/ModelZoneParsingProfileInfo';

    /** @var int */
    private static $interation_current                      =1;

    /** @var int */
    private static $interation_len                          =100;

    /** @var string */
    private static $file_log_path                           ='';

    /** @var string */
    private static $header;

    /**
     * @return bool
     */
    private static function set_header(){

        self::$header=UseragentStack::get_random_useragent();

        return true;

    }

    /**
     * @param $data
     * @return bool|int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_to_log($data){

        if(!empty($_POST['need_log'])){

            if(filesize(self::$file_log_path)>10*1024*1024)
                self::set_log_path();

            return file_put_contents(self::$file_log_path,$data,FILE_APPEND);

        }

        return false;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_log_path(){

        if(!file_exists(self::$dir_path))
            Dir::create_dir(self::$dir_path);

        self::$file_log_path=DirConfigProject::$dir_parsing_log;

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/ModelZoneParsingProfileInfo';

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/model_zone_'.time().'.log';

        return true;

    }

    /**
     * @param array $list
     * @return null
     */
    private static function get_data(array $list=[]){

        if(count($list)==0)
            return NULL;

        if(count($list[1])==0)
            return NULL;

        return trim($list[1][0]);

    }

    /**
     * @param int|NULL $face_ID
     * @param string|NULL $link
     * @return null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_page(int $face_ID=NULL,string $link=NULL){

        if(empty($face_ID)||empty($link))
            return NULL;

        if(!empty($_POST['link'])){

            $link       =$_POST['link'];
            $face_ID    =FaceData::get_face_ID_from_source_account_link($link);

        }

//        $link='https://model.zone/media/b0663536001e0b4f1c17b7b296f07cdb';

        $r=CurlGet::init($link,[],self::$header);

//        print_r($r);exit;

        if($r['status']!=200){

            self::add_to_log('status: '.$r['status'].' link: '.$link);

            return NULL;

        }

        $country_ID                 =NULL;
        $city_ID                    =NULL;
        $name                       =NULL;
        $age                        =NULL;
        $address                    =NULL;
        $country                    =NULL;
        $city                       =NULL;
        $date                       =NULL;
        $day                        =NULL;
        $month                      =NULL;
        $year                       =NULL;
        $address                    =NULL;

        $height                     =NULL;
        $weight                     =NULL;
        $boobs                      =NULL;
        $body_top                   =NULL;
        $body_middle                =NULL;
        $body_bottom                =NULL;
        $body                       =NULL;
        $about                      =NULL;
        $money                      =NULL;
        $insta                      =NULL;
        $facebook                   =NULL;
        $vk                         =NULL;
        $boobs_type                 =NULL;
        $hobby_old                  =NULL;

        $eye_color                  =NULL;
        $hair_color                 =NULL;
        $hair_length                =NULL;
        $national                   =NULL;
        $skin                       =NULL;
        $nationality                =NULL;
        $visa                       =NULL;
        $education                  =NULL;
        $profession                 =NULL;
        $language                   =NULL;
        $smoke                      =NULL;
        $work                       =NULL;
        $children                   =NULL;
        $connection_list            =NULL;
        $tattoo                     =NULL;
        $zodiac                     =NULL;
        $beauty_contest             =NULL;
        $beauty_dignity             =NULL;
        $lang                       =NULL;

        $about_weight               =NULL;
        $about_height               =NULL;
        $about_age                  =NULL;
        $about_boobs_size           =NULL;
        $about_top_size             =NULL;
        $about_middle_size          =NULL;
        $about_bottom_size          =NULL;
        $about_visa                 =NULL;
        $about_tattoo               =NULL;
        $about_lang                 =NULL;
        $about_natural              =NULL;
        $about_grin_card            =NULL;
        $about_boobs_type           =NULL;
        $about_year                 =NULL;
        $about_month                =NULL;
        $about_date                 =NULL;
        $about_day                  =NULL;
        $about_hobby                =NULL;
        $about_photo                =NULL;

        preg_match_all('/<body.*?>(.*?)<\/body>/is',$r['data'],$data_list);

        if(count($data_list)>1){

            $r['data']=self::get_data($data_list);

            if(empty($name)){

                preg_match_all('/(?:имя|name):\s*(.*?),?(\d*)</iu',$r['data'],$name_list);

                $name=self::get_data($name_list);

                if(count($name_list)==3)
                    if(count($name_list[2])>0){

                        $age_temp=(int)$name_list[2][0];

                        if($age_temp>=16&&$age_temp<=60)
                            $age=$name_list[2][0];

                    }

            }

            if(empty($age)){

                preg_match_all('/(?:Возраст|Age)\D*(\d{2})\s*\S*</u',$r['data'],$age_list);
//                preg_match_all('/(?:Возраст|Age):\s*(\d{2})\s*\S*</u',$r['data'],$age_list);

                $age=self::get_data($age_list);

            }

            if(empty($height)){

                preg_match_all('/(?:рост|height)\D*(\d+(?:\.\d{2,3}|,\d{2,3})?)\s*\S*<?/iu',$r['data'],$height_list);
//                preg_match_all('/(?:рост|height):.*?(\d+(?:\.\d{2,3}|,\d{2,3})?)\s*\S*<?/iu',$r['data'],$height_list);

                $height=self::get_data($height_list);

            }

            if(empty($weight)){

                preg_match_all('/(?:вес|weight)\D*((?:\d+|\d+(?:\.|,)?\d*))(?:\s*вес|\s*weight)?\s*\S*</iu',$r['data'],$weight_list);
//                preg_match_all('/(?:вес|weight):.*?(\d+(?:\.|,)?\d*)(?:\s*вес|\s*weight)\s*\S*</iu',$r['data'],$weight_list);

                $weight=self::get_data($weight_list);

            }

            if(empty($boobs)){

                preg_match_all('/(?:бюст|bust)\D*(\d+(?:\.|,)?\d*)\s*\S*</iu',$r['data'],$boobs_list);
//                preg_match_all('/(?:бюст|bust):\s*(\d+(?:\.|,)?\d*)\s*\S*</iu',$r['data'],$boobs_list);

                $boobs=self::get_data($boobs_list);

            }

            if(empty($boobs_type)){

                preg_match_all('/(?:тип бюста|bust type):.*?(\w+)\s*\S*</iu',$r['data'],$boobs_type_list);

                $boobs_type=self::get_data($boobs_type_list);

    //            if(!empty($boobs_type)&&!empty($boobs))
    //                $boobs.=' ('.mb_strtolower($boobs_type,'utf-8').')';

            }

            if(empty($body_top)){

                preg_match_all('/(?:грудь|груди|breast)\D*(\d{1,3}(?:\.\d{1,2}|,\d{1,2})?)\s*\S*<?/iu',$r['data'],$body_top_list);
//                preg_match_all('/(?:грудь|груди|breast):.*?(\d{1,3}(?:\.\d{1,2}|,\d{1,2})?)\s*\S*<?/iu',$r['data'],$body_top_list);
//                preg_match_all('/(?:грудь|груди|breast)\s*.*?(\d+(?:\.|,)?\d*)\s*\S*<?/iu',$r['data'],$body_top_list);
    //            preg_match_all('/(?:грудь|breast).*?(\d+(?:\.|,)?\d*)\s*\S*</iu',$r['data'],$body_top_list);

                $body_top=self::get_data($body_top_list);

                if($body_top<=6)
                    $boobs=$body_top;

            }

            if(empty($body_middle)){

                preg_match_all('/(?:талия|breast)\D*(\d+(?:\.|,)?\d*)\s*\S*<?/iu',$r['data'],$body_middle_list);
//                preg_match_all('/(?:талия|breast):.*?(\d+(?:\.|,)?\d*)\s*\S*<?/iu',$r['data'],$body_middle_list);
    //            preg_match_all('/(?:талия|breast).*?(\d+(?:\.|,)?\d*)\s*\S*</iu',$r['data'],$body_middle_list);

                $body_middle=self::get_data($body_middle_list);

            }

            if(empty($body_bottom)){

                preg_match_all('/(?:б(?:е|ё)дра|breast)\D*(\d+)\s*\S*<?/iu',$r['data'],$body_bottom_list);
//                preg_match_all('/(?:б(?:е|ё)дра|breast):.*?(\d+)\s*\S*<?/iu',$r['data'],$body_bottom_list);
    //            preg_match_all('/(?:б(?:е|ё)дра|breast).*?(\d+(?:\.|,)?\d*)\s*\S*</iu',$r['data'],$body_bottom_list);

                $body_bottom=self::get_data($body_bottom_list);

            }

            if(empty($language)){

                $lang_temp_list=[];

                preg_match_all('/англ|engli|фран|испа|итал|финс|немец|deuts/iu',$r['data'],$lang_list);

                if(count($lang_list)>0)
                    foreach($lang_list[0] as $lang)
                        switch(mb_strtolower($lang,'utf-8')){

                            case'engli':
                            case'англ':{

                                $lang_temp_list['английский']='английский';

                                break;

                            }

                            case'фран':{

                                $lang_temp_list['французский']='французский';

                                break;

                            }

                            case'polish':{

                                $lang_temp_list['польский']='польский';

                                break;

                            }

                            case'испа':{

                                $lang_temp_list['испанский']='испанский';

                                break;

                            }

                            case'итал':{

                                $lang_temp_list['итальянский']='итальянский';

                                break;

                            }

                            case'немец':
                            case'deuts':{

                                $lang_temp_list['немецкий']='немецкий';

                                break;

                            }

                        }

                if(count($lang_temp_list)>0)
                    $language=implode(', ',$lang_temp_list);

            }

            if(empty($hair_color)){

                preg_match_all('/(?:цвет\s?волос|hair):.*?(\w+\s*\W?\s*\w*)\s*\S*</iu',$r['data'],$hair_color_list);

                $hair_color=self::get_data($hair_color_list);

            }

            if(empty($hair_length)){

                preg_match_all('/Длина\s*волос:.*?(\w+\s*\W?\s*\w*)\s*\S*</iu',$r['data'],$hair_length_list);

                $hair_length=self::get_data($hair_length_list);

            }

            if(empty($eye_color)){

                preg_match_all('/(?:цвет\s?глаз|eye|eyes|eyes color):.*?(\w+\s*\W?\s*\w*)\s*\S*</iu',$r['data'],$eye_color_list);

                $eye_color=self::get_data($eye_color_list);

            }

            if(empty($national)){

                preg_match_all('/(?:гражданство|national):.*?(\w+\s*\W?\s*\w*)\s*\S*</iu',$r['data'],$national_list);

                $national=self::get_data($national_list);

            }

            if(empty($nationality)){

                preg_match_all('/(?:национальность|nationality):.*?(\w+\s*\W?\s*\w*)\s*\S*</iu',$r['data'],$nationality_list);

                $nationality=self::get_data($nationality_list);

            }

            if(empty($skin)){

                preg_match_all('/(?:раса|race):.*?(\w+\s*\W?\s*\w*)\s*\S*</iu',$r['data'],$skin_list);

                $skin=self::get_data($skin_list);

            }

            if(empty($zodiac)){

                preg_match_all('/(?:зодиак|zodiac):.*?(\w+\s*\W?\s*\w*)\s*\S*</iu',$r['data'],$zodiac_list);

                $zodiac=self::get_data($zodiac_list);

            }

            if(empty($citizenship)){

                preg_match_all('/(?:гражданство|citizenship):.*?(\w+\s*\W?\s*\w*)\s*\S*</iu',$r['data'],$citizenship_list);

                $citizenship=self::get_data($citizenship_list);

            }

            if(empty($education)){

                preg_match_all('/(?:образование|education):.*?(\w+\s*\W?\s*\w*)\s*\S*</iu',$r['data'],$education_list);

                $education=self::get_data($education_list);

            }

            if(empty($smoke)){

                if(preg_match('/курение|smoke/iu',$r['data'])>0)
                    $smoke='Да';

            }

            if(empty($visa)){

                preg_match_all('/Визы:?\s*(.*?)\s*\S*</ui',$r['data'],$visa_list);

                $visa_line=self::get_data($visa_list);

                if(!empty($visa_line)){

                    preg_match_all('/\w+/ui',$visa_line,$visa_list);

                    if(count($visa_list[0])>0)
                        $visa=implode(', ',$visa_list[0]);

                }

            }

            if(empty($profession)){

                preg_match_all('/профессия:?\s*(.*?)\s*\S*</ui',$r['data'],$profession_list);

                $profession=self::get_data($profession_list);

            }

            if(empty($connection)){

                preg_match_all('/связи:\s*(.*?)\s*\S*</ui',$r['data'],$connection_list);

                $connection=self::get_data($connection_list);

            }

            if(empty($tattoo)){

                if(preg_match('/татуировки/ui',$r['data'])>0)
                    $tattoo='да';

            }

            if(empty($vk)){

                preg_match_all('/vk:\s*(?:(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])|(.*?))</iu',$r['data'],$vk_list);

                $vk=self::get_data($vk_list);

            }

            if(empty($insta)){

                preg_match_all('/instagram:\s*(?:(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])|(.*?))</iu',$r['data'],$insta_list);

                $insta=self::get_data($insta_list);

            }

            if(empty($facebook)){

                preg_match_all('/facebook:\s*(?:(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])|(.*?))</iu',$r['data'],$facebook_list);

                $facebook=self::get_data($facebook_list);

            }

            if(empty($work)){

                preg_match_all('/текущая\s*работа:.*?(.*?)\s*\S*</ui',$r['data'],$work_list);

                $work=self::get_data($work_list);

            }

            if(empty($money)){

                preg_match_all('/мин\S*\s*гонорар:.*?(\d+(?:\.\d+)?)\s*\S*</ui',$r['data'],$money_list);

                $money=self::get_data($money_list);

            }

            if(empty($hobby_old)){

                preg_match_all('/Занятия в прошлом:\s*(.*?)\s*</ui',$r['data'],$hobby_old_list);

                $hobby_old=self::get_data($hobby_old_list);

            }

            if(empty($beauty_contest)){

                preg_match_all('/конкурсы\s*красоты:.*?(.*?)</iu',$r['data'],$beauty_contest_list);

                $beauty_contest=self::get_data($beauty_contest_list);

            }

            if(empty($beauty_dignity)){

                preg_match_all('/титулы?:\s*(.*?)</iu',$r['data'],$beauty_dignity_list);

                $beauty_dignity=self::get_data($beauty_dignity_list);

            }

            if(empty($country)){

                preg_match_all('/(?:страна|country):.*?(\w+\s*\W?\s*\w*)\s*\S*</iu',$r['data'],$country_list);

                $country=trim(self::get_data($country_list));

                if(!empty($country)){

                    $country_ID=CountryLocalization::get_country_ID_from_name(mb_strtolower($country,'utf-8'));

                    if(empty($country_ID))
                        $country=NULL;
                    else
                        $country=CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);

                }

            }

            if(empty($city)){

                preg_match_all('/(?:город|city):.*?(\w+\s*\W?\s*\w*)\s*\S*</iu',$r['data'],$city_list);

                $city=trim(self::get_data($city_list));

                if(!empty($city)){

                    $city_lower=mb_strtolower($city,'utf-8');

                    switch($city_lower){

                        case'msk':
                        case'мск':{

                            $city='Москва';

                            break;

                        }

                        case'spb':
                        case'спб':
                        case'санктпетербург':
                        case'с-пб':{

                            $city='Санкт-Петербург';

                            break;

                        }

                    }

                    $city_ID=CityLocalization::get_city_ID_from_name(mb_strtolower($city,'utf-8'));

                    if(empty($city_ID)){

                        $country_ID_2=CountryLocalization::get_country_ID_from_name(mb_strtolower($city,'utf-8'));

                        if(!empty($country_ID_2)){

                            $country_ID     =$country_ID_2;
                            $country        =$city;

                        }
                        else
                            $city=NULL;

                    }
                    else
                        $city=CityLocalization::get_city_name($city_ID,LangConfig::$lang_ID_default);

                }

            }

            if(!empty($body_top)&&!empty($body_middle)&&!empty($body_bottom)){

                if(!empty($body_top))
                    $body.=$body_top;

                if(!empty($body_middle))
                    $body.=(empty($body)?'':'/').$body_middle;

                if(!empty($body_bottom))
                    $body.=(empty($body)?'':'/').$body_bottom;

            }

            if(empty($about)){

                preg_match_all('/О себе:?\s*(.*?)\s*\S*</iu',$r['data'],$about_list);

                $about=self::get_data($about_list);

                if(!empty($about)){

                    if(empty($date)){

                        if(empty($year)){

                            preg_match_all('/(\d{4})/iu',$about,$year_list);

                            $year=self::get_data($year_list);

                            if(!empty($year)){

                                $age_delta=Date::get_year_full()-(int)$year;

                                if($age_delta<16||$age_delta>60)
                                    $year=NULL;

                                if(!empty($year)){

                                    if(empty($age)){

                                        if($age_delta>=16&&$age_delta<=60)
                                            $age=$age_delta;

                                    }

                                    preg_match_all('/(\d{1,2}\S*\d{1,2}\S*'.$year.')/iu',$about,$date_list);

                                    $date=self::get_data($date_list);

                                    if(!empty($date)){

                                        preg_match_all('/(\d+)/iu',$date,$date_list);

                                        if(count($date_list)>1)
                                            if(count($date_list[1])==3){

                                                $day        =$date_list[1][0];
                                                $month      =$date_list[1][1];
                                                $year       =$date_list[1][2];

                                                $date       =$day.'.'.$month.'.'.$year;

                                            }

                                    }

                                }

                            }

                        }

                    }

                    if(empty($height)){

                        preg_match_all('/(?:^|\D)((?:\d{3}|\d{1}(?:\.|,)\d{1,2}))(?:\s*рост|\s*height)?(?:$|\D)/iu',$about,$height_list);
//                        preg_match_all('/(?:^|\D)(\d{1,3}?(?:\.\d{2,3}|,\d{2,3})?)(?:\s*рост|\s*height)?(?:$|\D)/iu',$about,$height_list);

                        if(count($height_list)>1)
                            if(count($height_list[1])>0)
                                foreach($height_list[1] as $height_row){

                                    $height_row=str_replace(',','.',$height_row);

                                    if(ceil($height_row)<3)
                                        $height_row=ceil($height_row*100);

                                    if(!empty($height_row)){

                                        if($height_row<150||$height_row>200)
                                            $height=NULL;
                                        else
                                            $height=$height_row;

                                        break;

                                    }

                                }

                    }

                    if(empty($body)){

                        preg_match_all('/((?:\d{2,3}[-]\d{2}[-]\d{2,3}|\d{2,3}[_]\d{2}[_]\d{2,3}|\d{2,3}[\/]\d{2}[\/]\d{2,3}|\d{2,3}\s+\d{2}\s+\d{2,3}))/iu',$about,$body_list);
    //                    preg_match_all('/(?:^|\D)(\d{2,3}\D\d{2}\D\d{2,3})(?:$|\D)/iu',$about,$body_list);

                        if(count($body_list)>1){

                            foreach($body_list[1] as $row){

                                preg_match_all('/(\d+)/ui',$row,$body_item_list);

                                if(count($body_item_list)>1)
                                    if(count($body_item_list[1])==3){

                                        $body_top_temp      =NULL;
                                        $body_middle_temp   =NULL;
                                        $body_bottom_temp   =NULL;

                                        if(
                                              (int)$body_item_list[1][0]>70
                                            &&(int)$body_item_list[1][0]<110
                                        )
                                            $body_top_temp=$body_item_list[1][0];

                                        if(
                                              (int)$body_item_list[1][1]>45
                                            &&(int)$body_item_list[1][1]<80
                                        )
                                            $body_middle_temp=$body_item_list[1][1];

                                        if(
                                              (int)$body_item_list[1][2]>70
                                            &&(int)$body_item_list[1][2]<120
                                        )
                                            $body_bottom_temp=$body_item_list[1][2];

                                        if(
                                              !empty($body_top_temp)
                                            &&!empty($body_middle_temp)
                                            &&!empty($body_bottom_temp)
                                        ){

                                            $body_top       =$body_top_temp;
                                            $body_middle    =$body_middle_temp;
                                            $body_bottom    =$body_bottom_temp;

                                            $body           =$body_top_temp.'/'.$body_middle_temp.'/'.$body_bottom_temp;

                                            break;

                                        }

                                    }

                            }

                        }

                    }

                    if(empty($weight)){

    //                    preg_match_all('/(?:^|\D|вес\s*\S*)(?!\.|,)(\d{2})(?:кг|kg)?(?:$|\D)/iu',$about,$weight_list);
                        preg_match_all('/(?:^|\D)(?:кг|kg|вес|weight)?\s*(\d?(?:\.|,)?\d{2}?)(?:кг|kg|вес|weight)?(?:$|\D)/iu',$about,$weight_list);

                        if(count($weight_list)>1)
                            foreach($weight_list[1] as $row){

                                $weight_temp=(int)$row;

                                if(
                                      $weight_temp>40
                                    &&$weight_temp<100
                                )
                                    if(
                                          $weight_temp!=$age
                                        &&$weight_temp!=$body_top
                                        &&$weight_temp!=$body_middle
                                        &&$weight_temp!=$body_bottom
                                        &&$weight_temp!=$height
                                    ){

//                                        echo $weight_temp.' != '.$age."\n";
//                                        echo $weight_temp.' != '.$body_top."\n";
//                                        echo $weight_temp.' != '.$body_middle."\n";
//                                        echo $weight_temp.' != '.$body_bottom."\n";
//                                        echo $weight_temp.' != '.$height."\n";

                                        $weight=$weight_temp;

                                        break;

                                    }

                            }

                    }

                    if(empty($age)){

//                        preg_match_all('/(\d{2,3})\s*(?:лет)?/iu',$about,$age_list);
                        preg_match_all('/(\d{2,3})(?:\s*лет)?(?!\s*(?:вес|kg|кг))/iu',$about,$age_list);

                        if(count($age_list)>1)
                            foreach($age_list[1] as $row){

                                $age_temp=(int)$row;

                                if(
                                      $age_temp>16
                                    &&$age_temp<60
                                )
                                    if(
                                          $age_temp!=$age
                                        &&$age_temp!=$body_top
                                        &&$age_temp!=$body_middle
                                        &&$age_temp!=$body_bottom
                                        &&$age_temp!=$height
                                        &&$age_temp!=$weight
                                    ){

                                        $age=$age_temp;

                                        break;

                                    }

                            }

                     }

                    if(empty($beauty_dignity)){

                        preg_match_all('/(мис.*?)(?:,|<|$)/iu',$about,$beauty_dignity_list);

                        if(count($beauty_dignity_list)>1){

                            $beauty_dignity_temp_list=[];

                            foreach($beauty_dignity_list[1] as $row)
                                $beauty_dignity_temp_list[]=$row;

                            if(count($beauty_dignity_temp_list)>0)
                                $beauty_dignity=implode(', ',$beauty_dignity_temp_list);

                        }

                    }

                    if(empty($boobs)){

//                        preg_match_all('/(?:размер|бюст|грудь|гр\.)?\s*(\d?\.?\d{1,3})\s*(?:размер)?/iu',$about,$boobs_list);
                        preg_match_all('/(?:размер|бюст|грудь|гр\.)?\s*\D((?:\d{1}|\d{1}(?:\.|,)\d{1}))(?:\D\s*(?:размер)?|$)/iu',$about,$boobs_list);

                        if(count($boobs_list)>1)
                            foreach($boobs_list[1] as $index=>$row){

                                $row=str_replace(',','.',$row);

                                $boobs_temp=ceil($row);

                                if($boobs_temp!=$row)
                                    if(preg_match('/размер|бюст|грудь/iu',$boobs_list[0][$index])!==false)
                                        $boobs_temp=$row;

                                if($boobs_temp==$row)
                                    if(
                                          $boobs_temp>0
                                        &&$boobs_temp<=6
                                    )
                                        if(
                                              $boobs_temp!=$age
                                            &&$boobs_temp!=$body_top
                                            &&$boobs_temp!=$body_middle
                                            &&$boobs_temp!=$body_bottom
                                            &&$boobs_temp!=$height
                                            &&$boobs_temp!=$weight
                                            &&$boobs_temp!=$age
                                        ){

                                            $boobs=$row;

                                            break;

                                        }

                            }

                    }

                    if(empty($boobs_type)){

                        preg_match_all('/(своя|натур(?:альная)?|имплант|силик(?:он)?)/iu',$about,$boobs_list);

                        $boobs_type=self::get_data($boobs_list);

                        switch($boobs_type){

                            case'силик':{

                                $boobs_type='имплант';

                                break;

                            }

                            case'своя':
                            case'натур':{

                                $boobs_type='натуральная';

                                break;

                            }

                        }

    //                    if(!empty($boobs_type)&&!empty($boobs))
    //                        $boobs.=' ('.$boobs_type.')';

                    }

                    if(empty($country)){

                        preg_match_all('/(\w{3,40})/u',$about,$country_list);

                        if(count($country_list)>0)
                            if(count($country_list[1])>0){

                                $country_list=CountryLocalization::get_country_ID_list_from_name_list($country_list[1]);

                                if(count($country_list)>0)
                                    foreach($country_list as $country_ID=>$country){

                                        $country=CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);

                                        break;

                                    }

                            }


                    }

                    if(empty($city)){

                        preg_match_all('/(\w{3,40})/u',$about,$city_list);

                        if(count($city_list)>0)
                            if(count($city_list[1])>0){

                                $city_list=CityLocalization::get_city_ID_list_from_name_list($city_list[1]);

                                if(count($city_list)>0)
                                    foreach($city_list as $city_ID=>$city){

                                        $city=CityLocalization::get_city_name($city_ID,LangConfig::$lang_ID_default);

                                        if(empty($country_ID)){

                                            $country_ID     =City::get_country_ID($city_ID);
                                            $country        =CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);

                                        }

                                        if(!empty($city))
                                            break;

                                    }

                            }


                    }

                    if(empty($language)){

                        preg_match_all('/(англ|испан|франц|итал|финс|немец|бельг|китайc|арабс|португ|хинди|японск|турецк|корейск)/iu',$about,$lang_list);

                        if(count($lang_list)>0)
                            if(count($lang_list[1])>0){

                                $lang_temp_list=[];

                                foreach($lang_list[1] as $lang){

                                    switch(mb_strtolower($lang,'utf-8')){

                                        case'англ':{

                                            $lang_temp_list[]='английский';

                                            break;

                                        }

                                        case'испан':{

                                            $lang_temp_list[]='испанский';

                                            break;

                                        }

                                        case'франц':{

                                            $lang_temp_list[]='французский';

                                            break;

                                        }

                                        case'итал':{

                                            $lang_temp_list[]='итальянский';

                                            break;

                                        }

                                        case'финс':{

                                            $lang_temp_list[]='финский';

                                            break;

                                        }

                                        case'немец':{

                                            $lang_temp_list[]='немецкий';

                                            break;

                                        }

                                        case'бельг':{

                                            $lang_temp_list[]='бельгийский';

                                            break;

                                        }

                                        case'китайc':{

                                            $lang_temp_list[]='китайский';

                                            break;

                                        }

                                        case'арабс':{

                                            $lang_temp_list[]='арабский';

                                            break;

                                        }

                                        case'португ':{

                                            $lang_temp_list[]='португальский';

                                            break;

                                        }

                                        case'хинди':{

                                            $lang_temp_list[]='хинди';

                                            break;

                                        }

                                        case'японск':{

                                            $lang_temp_list[]='японский';

                                            break;

                                        }

                                        case'турецк':{

                                            $lang_temp_list[]='турецкий';

                                            break;

                                        }

                                        case'корейск':{

                                            $lang_temp_list[]='корейский';

                                            break;

                                        }

                                    }

                                }

                                if(count($lang_temp_list)>0)
                                    $lang=implode(', ',$lang_temp_list);

                            }


                    }

                    if(empty($visa)){

                        preg_match_all('/(?:виза)?(шенген|америка|офэ|сша|английск|финск|британ)/iu',$about,$visa_list);

                        if(count($visa_list)>0)
                            if(count($visa_list[1])>0){

                                $visa_temp_list=[];

                                foreach($visa_list[1] as $visa){

                                    $visa_temp=mb_strtolower($visa,'utf-8');

                                    switch($visa_temp){

                                        case'америка':
                                        case'сша':{

                                            $visa_temp_list[]='американская';

                                            break;

                                        }

                                        case'британ':
                                        case'английск':{

                                            $visa_temp_list[]='английская';

                                            break;

                                        }

                                        case'финск':{

                                            $visa_temp_list[]='финская';

                                            break;

                                        }

                                        default:{

                                            $visa_temp_list[]=$visa_temp;

                                            break;

                                        }

                                    }

                                }

                                if(count($visa_temp_list)>0)
                                    $visa=implode(', ',$visa_temp_list);

                            }


                    }

                    if(empty($city)){

                        preg_match_all('/(мск|msk|spb|спб|c-пб|moscow|москва|санкт-петербург|st\.petersburg|saint-petersburg|е-бург)/iu',$about,$city_list);

                        $city_lower=mb_strtolower(self::get_data($city_list),'utf-8');

                        switch($city_lower){

                            case'е-бург':{

                                $city='Екатеринбург';

                                break;

                            }

                            case'moscow':
                            case'москва':
                            case'msk':
                            case'мск':{

                                $city='Москва';

                                break;

                            }

                            case'st.petersburg':
                            case'st. petersburg':
                            case'saint-petersburg':
                            case'spb':
                            case'спб':
                            case'санктпетербург':
                            case'с-пб':{

                                $city='Санкт-Петербург';

                                break;

                            }

                        }

                        if(!empty($city))
                            $city_ID=CityLocalization::get_city_ID_from_name(mb_strtolower($city,'utf-8'));

                    }

                }

            }

            if(empty($address)){

                if(!empty($country)&&!empty($city))
                    $address=$country.', '.$city;
                else if(!empty($country)&&empty($city)){

                    $country_lower=mb_strtolower($country,'utf-8');

                    switch($country_lower){

                        case'россия':{

                            $city='Москва';

                            break;

                        }

                        case'украина':{

                            $city='Киев';

                            break;

                        }

                    }

                    $address=$country;

                    if(!empty($city)){

                        $city_ID    =CityLocalization::get_city_ID_from_name(mb_strtolower($city,'utf-8'));
                        $address    .=', '.$city;

                    }

                }
                else if(empty($country)&&!empty($city)){

                    if(empty($city_ID))
                        $address=$city;
                    else{

                        $country_ID=City::get_country_ID($city_ID);

                        if(!empty($country_ID))
                            $country=CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);

                        $address=$country.', '.$city;

                    }

                }
                else
                    $address=NULL;

            }

    //        echo 'Имя: '.$name."\n";
    //        echo 'Возраст: '.$age."\n";
    //        echo 'country_ID: '.$country_ID."\n";
    //        echo 'city_ID: '.$city_ID."\n";
    //        echo 'Страна: '.$country."\n";
    //        echo 'Город: '.$city."\n";
    //        echo 'Адрес: '.$address."\n";

            $info_list=[];

            if(!empty($height))
                $info_list[]='Рост: '.$height;

            if(!empty($weight))
                $info_list[]='Вес: '.$weight;

            if(!empty($boobs))
                $info_list[]='Бюст: '.$boobs.(empty($boobs_type)?'':(' ('.mb_strtolower($boobs_type,'utf-8').')'));

            if(!empty($boobs_type)&&empty($boobs))
                $info_list[]='Тип бюста: '.$boobs_type;

            if(!empty($body))
                $info_list[]='Параметры: '.$body;

            if(!empty($tattoo))
                $info_list[]='Тату: '.$tattoo;

            if(!empty($money))
                $info_list[]='Мин. гонорар: '.$money;

            if(!empty($beauty_contest))
                $info_list[]='Конкурсы красоты: '.$beauty_contest;

            if(!empty($beauty_dignity))
                $info_list[]='Титулы: '.$beauty_dignity;

            if(
                  !empty($insta)
                ||!empty($facebook)
                ||!empty($vk)
            )
                $info_list[]='';

            if(!empty($insta))
                $info_list[]='Instagram: '.$insta;

            if(!empty($facebook))
                $info_list[]='Facebook: '.$facebook;

            if(!empty($vk))
                $info_list[]='VK: '.$vk;

            if(!empty($about)){

                $info_list[]    ='';
                $info_list[]    ='О себе: '.$about;

            }

            $info_list[]='';
            $info_list[]='Other:';

            if(!empty($year))
                $info_list[]='Год рождения: '.$year;

            if(!empty($date))
                $info_list[]='Дата рождения: '.$date;

            if(!empty($boobs))
                $info_list[]='Бюст: '.$boobs;

            if(!empty($boobs_type))
                $info_list[]='Тип бюста: '.$boobs_type;

            if(!empty($eye_color))
                $info_list[]='Цвет глаз: '.$eye_color;

            if(!empty($hair_color))
                $info_list[]='Цвет волос: '.$hair_color;

            if(!empty($hair_length))
                $info_list[]='Длина волос: '.$hair_length;

            if(!empty($national))
                $info_list[]='Граждаство: '.$national;

            if(!empty($nationality))
                $info_list[]='Национальность: '.$nationality;

            if(!empty($skin))
                $info_list[]='Раса: '.$skin;

            if(!empty($visa))
                $info_list[]='Визы: '.$visa;

            if(!empty($citizenship))
                $info_list[]='Гражданство: '.$citizenship;

            if(!empty($education))
                $info_list[]='Образование: '.$education;

            if(!empty($profession))
                $info_list[]='Профессия: '.$profession;

            if(!empty($language))
                $info_list[]='Языки: '.$language;

            if(!empty($hobby_old))
                $info_list[]='Занятия в прошлом: '.$hobby_old;

            if(!empty($connection))
                $info_list[]='Связи: '.$connection;

            if(!empty($work))
                $info_list[]='Текущая работа: '.$work;

            if(!empty($zodiac))
                $info_list[]='Зодиак: '.$zodiac;

            if(!empty($body_top))
                $info_list[]='Грудь: '.$body_top;

            if(!empty($body_middle))
                $info_list[]='Талия: '.$body_middle;

            if(!empty($body_bottom))
                $info_list[]='Бедра: '.$body_bottom;

            if(!empty($smoke))
                $info_list[]='Курение: '.$smoke;

            if(!empty($lang))
                $info_list[]='Языки: '.$lang;

            if(!empty($_POST['print_r'])){

                echo 'Face ID: '.$face_ID."\n";
                echo 'Name: '.$name."\n";
                echo 'Age: '.$age."\n";
                echo 'Address: '.$address."\n";
                print_r($info_list);

                exit;

            }

            if(empty($name))
                return false;

            if(!FaceData::update_face_data($face_ID,$country_ID,$city_ID,$name,$age,$address,implode("\n",$info_list))){

                $error=[
                    'title'     =>PhpException::$title,
                    'info'      =>'Face data was not update'
                ];

                throw new PhpException($error);

            }

            return true;

        }

        return false;

    }

//    /**
//     * @param int|NULL $face_ID
//     * @param string|NULL $link
//     * @return null
//     * @throws ParametersException
//     * @throws PhpException
//     * @throws \Core\Module\Exception\DbParametersException
//     * @throws \Core\Module\Exception\DbQueryException
//     * @throws \Core\Module\Exception\DbQueryParametersException
//     * @throws \Core\Module\Exception\DbValidationValueException
//     * @throws \Core\Module\Exception\PathException
//     * @throws \Core\Module\Exception\SystemException
//     */
//    private static function prepare_page(int $face_ID=NULL,string $link=NULL){
//
//        if(empty($face_ID)||empty($link))
//            return NULL;
//
//        if(!empty($_POST['link'])){
//
//            $link       =$_POST['link'];
//            $face_ID    =FaceData::get_face_ID_from_source_account_link($link);
//
//        }
//
////        $link='https://model.zone/media/b0663536001e0b4f1c17b7b296f07cdb';
//
//        $r=CurlGet::init($link,[],self::$header);
//
////        print_r($r);exit;
//
//        if($r['status']!=200){
//
//            self::add_to_log('status: '.$r['status'].' link: '.$link);
//
//            return NULL;
//
//        }
//
//        preg_match_all('/Имя:\s*([a-zA-Zа-яА-Я]+)/ui',$r['data'],$name_list);
//        preg_match_all('/Возраст:(.*?)(?:<br>|$)/is',$r['data'],$age_list);
//        preg_match_all('/(?:Рост|Рост)\s*?\(см\)\:*?\s*?(\d{3})\s*?/is',$r['data'],$height_list);
////        preg_match_all('/Рост\s*(?:\(см\)):(\d+)(?:<br>|$)/is',$r['data'],$height_list);
//        preg_match_all('/Бюст \(размер\):(.*?)(?:<br>|$)/is',$r['data'],$boobs_list);
//        preg_match_all('/Country:(.*?)(?:<br>|$)/is',$r['data'],$country_list);
//        preg_match_all('/Город:(.*?)(?:<br>|$)/is',$r['data'],$city_list);
//        preg_match_all('/Мин. гонорар:(.*?)(?:<br>|$)/is',$r['data'],$money_list);
//        preg_match_all('/О себе:(.*?)(?:<br>|$)/is',$r['data'],$about_list);
//
//        $country_ID         =NULL;
//        $city_ID            =NULL;
//
//        $about_weight       =NULL;
//        $about_height       =NULL;
//        $about_age          =NULL;
//        $about_boobs_size   =NULL;
//        $about_top_size     =NULL;
//        $about_middle_size  =NULL;
//        $about_bottom_size  =NULL;
//        $about_visa         =NULL;
//        $about_tattoo       =NULL;
//        $about_lang         =NULL;
//        $about_natural      =NULL;
//        $about_grin_card    =NULL;
//        $about_boobs_type   =NULL;
//        $about_year         =NULL;
//        $about_month        =NULL;
//        $about_date         =NULL;
//        $about_day          =NULL;
//        $about_hobby        =NULL;
//        $about_photo        =NULL;
//
//        $name               =self::get_data($name_list);
//        $age                =self::get_data($age_list);
//        $height             =self::get_data($height_list);
//        $boobs              =self::get_data($boobs_list);
//        $country            =self::get_data($country_list);
//        $city               =self::get_data($city_list);
//        $money              =self::get_data($money_list);
//        $about              =self::get_data($about_list);
//
//        if(!empty($country)){
//
//            $country_ID=CountryLocalization::get_country_ID_from_name(mb_strtolower($country,'utf-8'));
//
//            if(empty($country_ID))
//                $country=NULL;
//            else
//                $country=CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);
//
//        }
//
//        if(!empty($city)){
//
//            $city_ID=CityLocalization::get_city_ID_from_name(mb_strtolower($city,'utf-8'));
//
//            if(empty($city_ID)){
//
//                $country_ID_2=CountryLocalization::get_country_ID_from_name(mb_strtolower($city,'utf-8'));
//
//                if(!empty($country_ID_2)){
//
//                    $country    .=(empty($country)?'':', ').$city;
//                    $city       =NULL;
//
//                }
//                else
//                    $city=NULL;
//
//            }
//            else
//                $city=CityLocalization::get_city_name($city_ID,LangConfig::$lang_ID_default);
//
//        }
//
//        if(mb_strlen($age,'utf-8')>2){
//
//            if(mb_strlen($age,'utf-8')==4){
//
//                $delta=Date::get_year_full()-(int)$age;
//
//                if($delta<15)
//                    $age=NULL;
//
//            }
//
//        }
//
//        if(!empty($country))
//            switch($country){
//
//                case'Russia':{
//
//                    $country='Россия';
//
//                    break;
//
//                }
//
//            }
//
//        if(!empty($about)){
//
////            preg_match_all("/(\d+\S?\d?(?:-|\/)\d+\S?\d?(?:-|\/)\d+\S?\d?)/is",$about,$params_list);
//            preg_match_all("/(?:(\d+\S?(?!\.)\d?-\d+\S?\d?-(?:\d+))|(\d+\S?(?!\.)\d?\/\d+\S?\d?\/(?:\d+))|(\d+\S?(?!\.)\d?,\d+\S?\d?,(?:\d+)))/iu",$about,$params_list);
////            preg_match_all("/(\d+\S?(?!\.)\d?(?:-|\/|\.|,)\d+\S?\d?(?:-|\/|\.|,)(?:\d+))/iu",$about,$params_list);
////            preg_match_all("/(\d+\S?\d?(?:-|\/|\.|,)\d+\S?\d?(?:-|\/|\.|,)(?:\d+))/iu",$about,$params_list);
//
//            $params=self::get_data($params_list);
//
//            if(!empty($params)){
//
//                // get params 90-60-90
//
//                $params=str_replace(',','.',$params);
//
//                $need_next=true;
//
//                list($size_1,$size_2,$size_3)=mb_split('-|\.|\/|,',$params);
//
//                $size_1     =(float)$size_1;
//                $size_2     =(float)$size_2;
//                $size_3     =(float)$size_3;
//
//                if($size_3>1940){
//
//                    if($size_1>0&&$size_1<32)
//                        $about_day=$size_1;
//                    else if($size_1>0&&$size_1<13)
//                        $about_month=$size_2;
//
//                    if($size_2>0&&$size_2<13)
//                        $about_month=$size_2;
//                    else if($size_2>0&&$size_2<32)
//                        $about_day=$size_1;
//
//                    $about_year=$size_3;
//
//                    if(empty($about_day)||empty($about_month)||empty($about_year))
//                        $need_next=true;
//                    else
//                        $about_date=$about_day.'.'.$about_month.'.'.$about_year;
//
//                }
//
//                if($need_next){
//
//                    if($size_1>130&&$size_1<210)
//                        $about_height=$size_1;
//                    else if($size_1<130&&$size_1>65)
//                        $about_top_size=$size_1;
//                    else if($size_1>15&&$size_1<60)
//                        $about_age=$size_1;
//                    else if($size_1<10)
//                        $about_boobs_size=$size_1;
//                    else if($size_1>1940){
//
//                        $about_year     =$size_1;
//                        $about_age      =Date::get_year_full()-(int)$about_year;
//
//                    }
//
//                    if($size_2>130&&empty($about_height)&&$size_2<210)
//                        $about_height=$size_2;
//                    else if($size_2<110&&$size_2>50&&!empty($about_top_size))
//                        $about_middle_size=$size_2;
//                    else if($size_2>15&&$size_2<60&&empty($about_age)&&empty($about_top_size)&&empty($about_height))
//                        $about_age=$size_2;
//                    else if($size_2<10&&empty($about_boobs_size))
//                        $about_boobs_size=$size_2;
//                    else if($size_2>1940){
//
//                        $about_year     =$size_2;
//                        $about_age      =Date::get_year_full()-(int)$about_year;
//
//                    }
//
//                    if($size_3>130&&empty($about_height)&&$size_3<210)
//                        $about_height=$size_3;
//                    else if($size_3<130&&$size_3>70&&!empty($about_top_size)&&!empty($about_middle_size))
//                        $about_bottom_size=$size_3;
//                    else if($size_3>15&&$size_3<60&&empty($about_age))
//                        $about_age=$size_3;
//                    else if($size_3<10&&empty($about_boobs_size))
//                        $about_boobs_size=$size_3;
//                    else if($size_3>1940){
//
//                        $about_year     =$size_3;
//                        $about_age      =Date::get_year_full()-(int)$about_year;
//
//                    }
//
//                }
//
//            }
//
//            if(empty($about_hobby)){
//
//                preg_match_all("/((?:гимнас)(?:\w*))/iu",$about,$params_list);
//
//                if(count($params_list)>0)
//                    foreach($params_list[1] as $hobby_row)
//                        $about_hobby.=(empty($about_hobby)?'':', ').$hobby_row;
//
//            }
//
//            if(empty($about_photo)){
//
//                preg_match_all("/((?:ny)(?:\w*))/iu",$about,$params_list);
//
//                if(count($params_list)>0)
//                    foreach($params_list[1] as $photo_row)
//                        $about_photo.=(empty($about_photo)?'':', ').$photo_row;
//
//            }
//
//            if(
//                  empty($about_top_size)
//                &&empty($about_top_size)
//                &&empty($about_top_size)
//            ){
//
//                preg_match_all("/талия\s*(\d{2})(?:\D|\s|$)/is",$about,$params_list);
//
//                $params=(int)self::get_data($params_list);
//
//                if($params>50&&$params<90)
//                    $about_middle_size=$params;
//
//                preg_match_all("/б(?:е|ё)дра\s*(\d{2,3})(?:\D|\s|$)/is",$about,$params_list);
//
//                $params=(int)self::get_data($params_list);
//
//                if($params>80&&$params<100)
//                    $about_bottom_size=$params;
//
////                $about=str_replace($params,'',$about);
//
//            }
//
//            if(empty($about_tattoo)){
//
//                preg_match_all("/((?:без)\s*тату|татуировки)/iu",$about,$params_list);
//
//                $params=self::get_data($params_list);
//
//                if(!empty($params))
//                    $about_tattoo=$params;
//
//            }
//
//            if(empty($about_year)){
//
//                preg_match_all("/\s*(\d{4})\s*/is",$about,$params_list);
//
//                $params=(int)self::get_data($params_list);
//
//                if($params>1940&&$params<Date::get_year_full()-15){
//
//                    $about_year     =$params;
//                    $about_age      =Date::get_year_full()-$params;
//
//                }
//
//            }
//
//            if(empty($about_natural)){
//
//                preg_match_all("/((?:без|не)\s*(?:пласт|натурал)\w+)/iu",$about,$params_list);
//
//                $params=self::get_data($params_list);
//
//                if(!empty($params))
//                    $about_natural=$params;
//
////                $about=str_replace($params,'',$about);
//
//            }
//
//            if(empty($about_visa)){
//
//                preg_match_all("/виз(?:а|ы):?\s*([a-zA-Zа-яА-Я]+)/ui",$about,$params_list);
//
//                $params=self::get_data($params_list);
//
//                if(!empty($params))
//                    $about_visa=$params;
//
//                preg_match_all("/(шенге)/ui",$about,$params_list);
//
//                $params=self::get_data($params_list);
//
//                if(!empty($params))
//                    $about_visa.=(!empty($about_visa)?', ':'').'Шенген';
//
//                preg_match_all("/(сша|амер(?:\w+))/ui",$about,$params_list);
//
//                $params=self::get_data($params_list);
//
//                if(!empty($params))
//                    $about_visa.=(!empty($about_visa)?', ':'').'США';
//
//            }
//
//            if(empty($about_grin_card)){
//
//                preg_match_all("/((?:грин|green)(?:\w+|)\s*(?:карта|card))\S*?/ui",$about,$params_list);
//
//                $params=self::get_data($params_list);
//
//                if(!empty($params))
//                    $about_grin_card=$params;
//
////                $about=str_replace($params,'',$about);
//
//            }
//
//            if(empty($about_height)){
//
//                preg_match_all("/(?:рост|\s*)((?:\d{3}(?!\d)|\d{1}(?:\.|,)\s*(?:\d{1,2})))/is",$about,$params_list);
////                preg_match_all("/((?:рост|)(?:\d{3}\s+|\d{1}(?:\.|,)(?:\d{1,2})))/is",$about,$params_list);
//
////                $params=(float)self::get_data($params_list);
//
//                if(count($params_list)>0)
//                    foreach($params_list[1] as $params){
//
//                        $params=(float)$params;
//
//                        if($params<3)
//                            $params=(int)($params*100);
//
//                        if($params>150&&$params<200){
//
//                            $about_height=$params;
//
//                            break;
//
//                        }
//
//                    }
//
////                $about=str_replace($params,'',$about);
//
//            }
//
//            if(empty($about_weight)){
//
//                preg_match_all("/(?:вес|масса|вешу)?\s*(\d{2,3})(?:\s*|кг)/is",$about,$params_list);
//
//                foreach($params_list[1] as $weight)
//                    if($weight>40&&$weight<110){
//
//                        if(
//                              $about_top_size!=$weight
//                            &&$about_middle_size!=$weight
//                            && $about_bottom_size!=$weight
//                        ){
//
//                            $about_weight=$weight;
//
//                            break;
//
//                        }
//
//                    }
//
////                $about=str_replace($params,'',$about);
//
//            }
//
//            if(empty($about_age)){
//
//                preg_match_all("/(?:^|\s*)(\d{2,3})(?:\s*|$)/is",$about,$params_list);
////                preg_match_all("/(?:^|\D|\s)(\d{2})(?:\D|\s|$)/is",$about,$params_list);
//
////                $params=(int)self::get_data($params_list);
//
//                if(count($params_list[1])>0)
//                    foreach($params_list[1] as $params)
//                        if($params>=16&&$params<=50&&$about_weight!=$params&&$about_middle_size!=$params)
//                            $about_age=$params;
//
//            }
//
//            if(empty($city)){
//
//                preg_match_all("/([A-ZА-ЯЁ]{1}[a-zа-яёА-ЯЁ]+)/u",$about,$params_list);
//
//                if(count($params_list)>1)
//                    foreach($params_list[1] as $params)
//                        if(empty($country)){
//
//                            $country_ID=CountryLocalization::get_country_ID_from_name(mb_strtolower($params,'utf-8'));
//
//                            if(!empty($country_ID))
//                                $country=CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);
//
//                        }
//                        else{
//
//                            $city_ID=CityLocalization::get_city_ID_from_name(mb_strtolower($params,'utf-8'));
//
//                            if(!empty($city_ID))
//                                $city=CityLocalization::get_city_name($city_ID,LangConfig::$lang_ID_default);
//
//                        }
//
//            }
//
//            if(empty($boobs)){
//
//                preg_match_all("/(?:^|гр(?:\.)|грудь|\D|\s)\s*(\d{1}(?:,\d{1,2}|\.\d{1,2})*)\.?\+?(?!\.|,)?\s*(?:\D|\s|$)/iu",$about,$params_list);
////                preg_match_all("/(?:^|гр(?:\.)|грудь|\D|\s)\s*(\d{1}(?:,\d{1,2}|\.\d{1,2})*\+?(?!\.|,)\s*)(?:\D|\s|$)/iu",$about,$params_list);
////                preg_match_all("/(?:^|гр(?:\.)|грудь|\D|\s)\s*(\d{1}(?:,\d{1,2}|\.\d{1,2})*\+?(?!\.|,)\s*(?:своя|натур(?:\w+)|\s*))(?:\D|\s|$)/iu",$about,$params_list);
////                preg_match_all("/(?:^|гр(?:\.)|грудь|\D|\s)\s*(\d{1}(?!\.|\,)\+?\s*(?:своя|натур(?:\w+)|\s*))(?:\D|\s|$)/iu",$about,$params_list);
//
////                $params=self::get_data($params_list);
//
//                if(count($params_list[1])>0)
//                    foreach($params_list[1] as $boobs_size_row)
//                        if((int)$boobs_size_row>0&&(int)$boobs_size_row<7){
//
//                            $boobs=(float)str_replace(',','.',$boobs_size_row);
//
//                            break;
//
//                        }
//
//                if(!empty($boobs)){
//
//                    preg_match_all("/((?:своя|сво(?:e|ё)|натур(?:\w+)))/iu",$about,$params_list);
//
//                    $params=self::get_data($params_list);
//
//                    if(!empty($params))
//                        $about_boobs_type=mb_strtolower($params,'utf-8');
//
//                }
//
////                $about=str_replace($params,'',$about);
//
//            }
//
//        }
//
//        $info_list      =[];
//
//        if(!empty($name))
//            $name=$name;
//
//        if(!empty($age))
//            $age=(int)$age;
//        else if(!empty($about_age))
//            $age=$about_age;
//
//        if(!empty($about_date))
//            $info_list[]='Дата рождения: '.$about_date;
//        else if(!empty($about_year))
//            $info_list[]='Год рождения: '.$about_year;
//
//        if(!empty($height))
//            $info_list[]='Рост: '.$height;
//        else if(!empty($about_height))
//            $info_list[]='Рост: '.$about_height;
//
//        if(!empty($about_weight))
//            $info_list[]='Вес: '.$about_weight;
//
//        if(!empty($boobs))
//            $info_list[]='Грудь: '.$boobs;
//        else if($about_boobs_size)
//            $info_list[]='Бюст: '.$about_boobs_size;
//
//        if(!empty($about_boobs_type))
//            $info_list[]='Тип бюста: '.$about_boobs_type;
//
//        if(!empty($country))
//            $info_list[]='Страна: '.$country;
//
//        if(!empty($city))
//            $info_list[]='Город: '.$city;
//
//        $body_params_list=[];
//
//        if(!empty($about_top_size))
//            $body_params_list[]=$about_top_size;
//
//        if(!empty($about_middle_size))
//            $body_params_list[]=$about_middle_size;
//
//        if(!empty($about_bottom_size))
//            $body_params_list[]=$about_bottom_size;
//
//        if(count($body_params_list)>0)
//            $info_list[]='Параметры: '.implode('-',$body_params_list);
//
//        if(!empty($about_visa))
//            $info_list[]='Визы: '.$height;
//
//        if(!empty($about_tattoo))
//            $info_list[]='Тату: '.$about_tattoo;
//
////        if(!empty($about_natural))
////            echo 'Natural: '.$about_natural."\n";
//
//        if(!empty($about_grin_card))
//            $info_list[]='Гринкарта: есть';
//
//        if(!empty($about_hobby))
//            $info_list[]='Хобби: '.$about_hobby;
//
////        if(!empty($about_photo)){
////            echo 'Photo: '.$about_photo."\n";
////
////        }
//
//        if(!empty($money))
//            $info_list[]='Мин. гонорар: '.$money;
//
//        $info_list[]='';
//        $info_list[]='Source: '.$about;
//
//        $address=$country;
//
//        if(!empty($city))
//            $address.=(empty($address)?'':', ').$city;
//
////        echo $name."\n";
////        echo $age."\n";
////        echo $address."\n";
////        print_r($info_list);
//
//        if(!FaceData::update_face_data($face_ID,$city_ID,$name,$age,$address,implode("\n",$info_list))){
//
//            $error=[
//                'title'     =>PhpException::$title,
//                'info'      =>'Face data was not update'
//            ];
//
//            throw new PhpException($error);
//
//        }
//
//        return true;
//
//    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_list(){

        do{

            $face_list=FaceData::get_face_data_source_link_list(self::$start_face_ID,1,self::$interation_current,self::$interation_len);

            if(count($face_list)==0)
                return self::set_return();

            foreach($face_list as $row){

                $face_ID        =$row['face_ID'];
                $link           =$row['source_account_link'];

                self::prepare_page($face_ID,$link);

                self::$start_face_ID=$face_ID;

            }

//            self::$interation_current+=self::$interation_len;

        }while(true);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_log_path();
        self::set_header();

        self::set_face_list();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(empty($_POST['face_ID'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Face ID is empty'
            ];

            throw new ParametersException($error);

        }

        self::$start_face_ID=(int)$_POST['face_ID'];

        return self::set();

    }

}