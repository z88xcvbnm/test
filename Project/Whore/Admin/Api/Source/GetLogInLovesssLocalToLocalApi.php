<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Cookie\CookieFile;
use Core\Module\Curl\CurlGet;
use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Dir\Dir;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\ShluhamNetException;
use Core\Module\File\File;
use Core\Module\File\FileParametersCash;
use Core\Module\Image\Image;
use Core\Module\Image\ImageUploadedPrepare;
use Core\Module\Json\Json;
use Core\Module\OsServer\OsServer;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class GetLogInLovesssLocalToLocalApi{

    /** @var string */
    private static $link            ='https://lovesss.ru/login#anchor-login-form';

    /** @var string */
    private static $cookie_path     ='Temp/Parsing/Lovesss/reverse_ip.txt';

    /** @var string */
    private static $login;

    /** @var string */
    private static $password;

    /** @var string */
    private static $captcha_key;

    /** @var string */
    private static $re_captcha;

    /**
     * @var string
     */
    private static $url_referer     ='https://lovesss.ru/login';

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_page(){

        $post_list=[
            'login'     =>self::$login,
            'password'  =>self::$password,
            'submit'    =>NULL
        ];

        $post_list['captcha['.self::$captcha_key.']']=self::$re_captcha.'';

        $cookie_list=CookieFile::get_cookie_list_from_file(self::$cookie_path);

        print_r($post_list);

        $r=CurlGet::init(self::$link,[],NULL,false,$post_list,$cookie_list,self::$cookie_path,self::$url_referer);
//        $r=CurlPost::init(self::$link,[],$post_list,false,self::$cookie_path);

        $cookie=CookieFile::get_cookie_list_from_file(self::$cookie_path);

        print_r($cookie);

//        $r=CurlGet::init(self::$link,[],NULL,false,$post_list,[],self::$cookie_path);

        print_r($r);
        exit;

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::get_page();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        $error_info_list=[];

        if(empty($_POST['login']))
            $error_info_list[]='Login is empty';

        if(empty($_POST['password']))
            $error_info_list[]='Password is empty';

        if(empty($_POST['recaptcha_key']))
            $error_info_list[]='Key is empty';

        if(empty($_POST['recaptcha_value']))
            $error_info_list[]='Value is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        print_r($_POST);

        self::$login            =trim($_POST['login']);
        self::$password         =trim($_POST['password']);
        self::$captcha_key      =trim($_POST['recaptcha_key']);
        self::$re_captcha       =trim($_POST['recaptcha_value']);

        return self::set();

    }

}