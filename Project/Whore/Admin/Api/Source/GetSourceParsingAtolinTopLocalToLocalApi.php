<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Curl\CurlGet;
use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Dir\Dir;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\ShluhamNetException;
use Core\Module\File\File;
use Core\Module\File\FileParametersCash;
use Core\Module\Image\Image;
use Core\Module\Image\ImageUploadedPrepare;
use Core\Module\Json\Json;
use Core\Module\OsServer\OsServer;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\Useragent\UseragentStack;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;

class GetSourceParsingAtolinTopLocalToLocalApi{

    /** @var int */
    private static $source_ID                               =3;

    /** @var int */
    private static $page                                    =4;

    /** @var string */
    private static $host                                    ='https://atolin.ru';

    /** @var string */
    private static $url_profile                             ='anketa';

    /** @var string */
    private static $url_search                              ='anketa/search?AnketaSearch%5Bgender%5D%5B0%5D=0&AnketaSearch%5Bagefrom%5D=18&AnketaSearch%5Bageto%5D=50&AnketaSearch%5Blocation_id%5D=&page=';

    /** @var string */
    private static $dir_path                                ='Temp/Atolin';

    /** @var string */
    private static $url_api                                 ='https://shluham.net/api/json';

    /** @var string */
    private static $api_add_face_data_action                ='add_face_data';

    /** @var string */
    private static $api_add_face_image_action               ='add_face_image';

    /** @var string */
    private static $api_get_face_ID_action                  ='get_face_ID';

    /** @var string */
    private static $api_update_face_to_public_action        ='update_face_to_public';

    /** @var string */
    private static $key_hash                                ='domino777';

    /** @var string */
    public  static $file_log_path                           ='';

    /** @var int */
    private static $image_min_len                           =2;

    /** @var int */
    private static $age_min                                 =0;

    /** @var int */
    private static $age_max                                 =50;

    /** @var int */
    private static $profile_index                           =0;

    /** @var array */
    private static $sleep_profile_len_list                  =[50,100];

    /** @var array */
    private static $sleep_after_profile_list                =[400,600];

    /** @var array */
    private static $sleep_random_list                       =[5,15];

    /**
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function sleep_random(){

        $rand=rand(self::$sleep_random_list[0],self::$sleep_random_list[1]);

        self::add_to_log("SLEEP: ".$rand." ".Date::get_date_time_full()."\n");
        
        return sleep($rand);
        
    }

    /**
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function sleep_after_profile_uploaded(){

        $rand=rand(self::$sleep_after_profile_list[0],self::$sleep_after_profile_list[1]);

        self::add_to_log("SLEEP after parse profile: ".$rand." ".Date::get_date_time_full()."\n");

        return sleep($rand);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_log_path(){

        if(!file_exists(self::$dir_path))
            Dir::create_dir(self::$dir_path);

        self::$file_log_path=DirConfigProject::$dir_parsing_log;

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/Atolin';

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/atolin_'.time().'.log';

        return true;

    }

    /**
     * @param $data
     * @return bool|int
     */
    private static function add_to_log($data){

        if(!empty($_POST['need_log']))
            return file_put_contents(self::$file_log_path,$data,FILE_APPEND);

        return false;
        
    }

    /**
     * @param string|NULL $source_account_link
     * @return mixed
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_get_face_ID(string $source_account_link=NULL){

        if(empty($source_account_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source account link is empty'
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_get_face_ID_action;
        $data       =[
            'source_account_link'   =>$source_account_link,
            'key_hash'              =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face ID was not give',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){
            
            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if($r['status']!=200){

            self::add_to_log("--> restart send get face ID: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_get_face_ID($source_account_link);

        }

        if(isset($r['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if(empty($r['data']))
            return NULL;

        if(!Json::is_json($r['data']))
            return NULL;

        $data=Json::decode($r['data']);

        if(isset($data['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        return $data['data']['face_ID'];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $source_ID
     * @param int|NULL $image_ID
     * @param int|NULL $city_ID
     * @param string|NULL $source_account_link
     * @param string|NULL $name
     * @param int|NULL $age
     * @param string|NULL $city
     * @param string|NULL $info
     * @return mixed
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_add_face_data(int $face_ID=NULL,int $source_ID=NULL,int $image_ID=NULL,int $city_ID=NULL,string $source_account_link=NULL,string $name=NULL,int $age=NULL,string $city=NULL,string $info=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($source_ID))
            $error_info_list[]='Source ID is empty';

        if(empty($source_account_link))
            $error_info_list[]='Source account link is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_add_face_data_action;
        $data       =[
            'face_ID'                   =>$face_ID,
            'source_ID'                 =>$source_ID,
            'image_ID'                  =>$image_ID,
            'city_ID'                   =>$city_ID,
            'source_account_link'       =>$source_account_link,
            'name'                      =>$name,
            'age'                       =>$age,
            'city'                      =>$city,
            'info'                      =>$info,
            'key_hash'                  =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face data was not add',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if($r['status']!=200){

            self::add_to_log("--> restart send add face data: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_add_face_data($face_ID,$source_ID,$image_ID,$city_ID,$source_account_link,$name,$age,$city,$info);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $facekit_image_ID
     * @param int|NULL $file_size
     * @param string|NULL $file_content_type
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param string|NULL $file_path
     * @param array|NULL $face_coords
     * @return int|null
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_add_face_image(int $face_ID=NULL,int $facekit_image_ID=NULL,int $file_size=NULL,string $file_content_type=NULL,string $file_mime_type=NULL,string $file_extension=NULL,string $file_path=NULL,array $face_coords=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_add_face_image_action;
        $data       =[
            'face_ID'               =>$face_ID,
            'facekit_image_ID'      =>$facekit_image_ID,
            'file_size'             =>$file_size,
            'file_content_type'     =>$file_content_type,
            'file_mime_type'        =>$file_mime_type,
            'file_extension'        =>$file_extension,
            'image_base64'          =>base64_encode(file_get_contents($file_path)),
            'face_len'              =>count($face_coords),
            'face_coords'           =>empty($face_coords)?'[]':Json::encode($face_coords),
            'key_hash'              =>self::$key_hash,
            'time'                  =>time().'_'.rand(0,time())
        ];

        $r=CurlPost::init($url,[],$data);

        self::add_to_log("--> ERROR: ".print_r($r,true)."\n");

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face image was not add',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if($r['status']==500||$r['status']==500){

            self::add_to_log("--> request: ".Date::get_date_time_full()."\n");
            self::add_to_log(print_r($r,true)."\n");
            self::add_to_log(print_r($data,true)."\n");
            self::add_to_log("--> restart send add face image: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_add_face_image($face_ID,$facekit_image_ID,$file_size,$file_content_type,$file_mime_type,$file_extension,$file_path,$face_coords);

        }
        else if($r['status']!=200)
            return NULL;
        else{

            if(empty($r['data']))
                return NULL;

            if(!Json::is_json($r['data']))
                return NULL;

            $data=Json::decode($r['data']);

            if(isset($data['error'])){

                self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

                return NULL;

            }

            return $data['data']['image_ID'];

        }

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_update_face_to_public(int $face_ID=NULL,int $image_ID=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_update_face_to_public_action;
        $data       =[
            'face_ID'               =>$face_ID,
            'image_ID'              =>$image_ID,
            'key_hash'              =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face was not update to public',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if($r['status']!=200){

            self::add_to_log("--> restart send update face to public: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_update_face_to_public($face_ID,$image_ID);

        }

        return true;

    }

    /**
     * @param string|NULL $source_account_link
     * @return int|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_face_ID_from_source_account_link(string $source_account_link=NULL){

        if(empty($source_account_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source account link is empty'
            ];

            throw new ParametersException($error);

        }

        return FaceData::get_face_ID_from_source_account_link($source_account_link);

    }

    /**
     * @param string|NULL $file_extension
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_file(string $file_extension=NULL){

        return File::add_file_without_params($file_extension,true,false,false);

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_hash
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_file(int $file_ID=NULL,string $file_hash=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_content_type=NULL,string $file_extension=NULL){

        return File::update_file($file_ID,$file_hash,1,1,$file_size,$file_size,$file_mime_type,$file_content_type,$file_extension,true,false,true);

    }

    /**
     * @param string|NULL $file_path
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_file(string $file_path=NULL){

        if(empty($file_path))
            return NULL;

        self::add_to_log("start get file from url: ".$file_path." ".Date::get_date_time_full()."\n");

//        $image=@file_get_contents($file_path);

//        $r=CurlGet::init($file_path,[],UseragentStack::get_random_useragent());
        $r=CurlGet::init($file_path,[],NULL,true);

        if($r['status']!=200)
            return NULL;

        $image=$r['data'];

        self::add_to_log("file downloaded: ".$file_path." ".Date::get_date_time_full()."\n");

        if($image===false)
            return NULL;

        self::add_to_log("try get file extension: ".$file_path." ".Date::get_date_time_full()."\n");

        $file_name          =basename($file_path);
        $query              =parse_url($file_path, PHP_URL_QUERY);
        $file_name          =str_replace('?'.$query,'',$file_name);
        $file_extension     =File::get_file_extension_from_file_name($file_name);

        self::add_to_log("got file extension: ".$file_path." ".Date::get_date_time_full()."\n");

        $file_ID=self::add_file($file_extension);

        self::add_to_log("create file row in DB: ".$file_path." ".Date::get_date_time_full()."\n");

        if(empty($file_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new PhpException($error);

        }

        $file_dest_path=File::get_file_path_from_file_ID($file_ID,true);

        File::get_file_dir($file_ID,NULL,Date::get_date_time_full());
        FileParametersCash::add_file_parameter_in_cash($file_ID,'file_extension',$file_extension);

        self::add_to_log("try put file on the drive: ".$file_path." ".Date::get_date_time_full()."\n");

//        $query              =parse_url($file_dest_path, PHP_URL_QUERY);
//        $file_dest_path     =str_replace('?'.$query,'',$file_dest_path);

        if(file_put_contents($file_dest_path,$image)===false){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not save'
            ];

            throw new PhpException($error);

        }

        self::add_to_log("check min file size: ".$file_path." ".Date::get_date_time_full()."\n");
        self::add_to_log("file size: ".filesize($file_dest_path)." ".Date::get_date_time_full()."\n");

        if(filesize($file_dest_path)<=16000){

            self::add_to_log("BAD FILE: ".$file_path." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if(!OsServer::$is_windows){

            self::add_to_log("try create temp image file: ".$file_path." ".Date::get_date_time_full()."\n");

            $temp_image_path='Temp/ModelZoneParsing/img_'.time();

            try{

                self::add_to_log("try open Imagick: ".$file_path." ".Date::get_date_time_full()."\n");

                $img=new \Imagick($file_dest_path);
                self::add_to_log("read image with Imagick: ".$file_path." ".Date::get_date_time_full()."\n");
                $img->readImage($file_dest_path);
                self::add_to_log("try write image with Imagick: ".$file_path." ".Date::get_date_time_full()."\n");
                $img->writeImage($temp_image_path);
                self::add_to_log("temp image file created: ".$file_path." ".Date::get_date_time_full()."\n");

            }
            catch (\ImagickException $e){

                self::add_to_log("create temp file ERROR: ".print_r($e)." ".Date::get_date_time_full()."\n");

                return NULL;

            }

            unset($img);

            unlink($file_dest_path);

            copy($temp_image_path,$file_dest_path);

            unlink($temp_image_path);

        }

        self::add_to_log("try get image object from file path: ".$file_path." ".Date::get_date_time_full()."\n");

        $image_object=Image::get_image_object_from_file_path($file_dest_path);

        if(empty($image_object)){

            self::add_to_log("FILE ERROR: ".$file_path." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        $size       =getimagesize($file_dest_path,$im_info);
        $sum        =$size[0]*$size[1];

        if($sum!=0){

            $file_size  =filesize($file_dest_path);
            $cof        =$file_size/$sum;

        }
        else
            $cof=0;

        self::add_to_log("-> file cof: ".$cof." ".Date::get_date_time_full()."\n");

        if($cof<.01){

            self::add_to_log("BAD FILE SUM: ".$file_path." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        switch(exif_imagetype($file_dest_path)){

            case 1:
            case 2:
            case 3:
            case 7:
            case 8:
                break;

            default:
                return NULL;

        }

        $file_mime_type         =File::get_file_mime_type_from_path($file_dest_path);
        $file_content_type      =File::get_file_content_type_from_path($file_dest_path);
        $file_size              =filesize($file_dest_path);
        $data_list              =[
            User::$user_ID,
            $file_size,
            $file_mime_type,
            $file_content_type,
            time(),
            rand(0,time())
        ];
        $file_hash              =Hash::get_sha1_encode(implode(':',$data_list));

        if($file_size==0){

            File::remove_file_ID($file_ID);

            self::add_to_log("--> file is not exists: ".$file_size." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if(!self::update_file($file_ID,$file_hash,$file_size,$file_mime_type,$file_content_type,$file_extension)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not update'
            ];

            throw new PhpException($error);

        }

        return[
            'file_ID'               =>$file_ID,
            'file_content_type'     =>$file_content_type,
            'file_extension'        =>$file_extension
        ];

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_image(int $file_ID=NULL,string $file_content_type='image',string $file_extension=NULL){

        return ImageUploadedPrepare::init($file_ID,$file_content_type,$file_extension);

    }

    /**
     * @param array $image_list
     * @return array
     */
    private static function search_face_in_image_list(array $image_list=[]){

        return $image_list;

//        if(count($image_list)==0)
//            return[];
//
//        $face_len=0;
//
//        foreach($image_list as $index=>$image_data){
//
//            self::add_to_log("-> start search faces on image_ID: ".$image_data['image_ID']." ".Date::get_date_time_full()."\n");
//
//            $r=GetFacesCoordsFaceKitAction::init($image_data['image_ID'],NULL,false,self::$file_log_path);
//
//            self::add_to_log("-> finish search faces on image_ID: ".$image_data['image_ID']." ".Date::get_date_time_full()."\n");
//
//            $image_list[$index]['face_len']     =0;
//            $image_list[$index]['coords']       =[];
//
//            if(count($r)>0){
//
//                self::add_to_log("--> image len: ".$r['face_len']." ".Date::get_date_time_full()."\n");
//
//                if($r['face_len']>0){
//
//                    $image_list[$index]['face_len']     =$r['face_len'];
//                    $image_list[$index]['face_list']    =$r['face_list'];
//                    $face_list=[];
//
//                    foreach($r['face_list'] as $face_index=>$face_data){
//
//                        $coords         =$face_data['coords'];
//                        $perc_x         =.6;
//                        $perc_y         =.3;
//                        $w              =$coords['right']-$coords['left'];
//                        $h              =$coords['bottom']-$coords['top'];
//                        $w_delta        =ceil($w*$perc_x);
//                        $h_delta        =ceil($h*$perc_y);
//                        $x              =ceil($coords['left']-$w_delta/2);
//                        $y              =ceil($coords['top']-$w_delta/2);
//                        $w              +=$w_delta;
//                        $h              +=$h_delta;
//
//                        if($x<0)
//                            $x=0;
//
//                        if($y<0)
//                            $y=0;
//
//                        $face_list[]=[
//                            'w'     =>$w,
//                            'h'     =>$h,
//                            'x'     =>$x,
//                            'y'     =>$y
//                        ];
//
//                        $face_len++;
//
//                    }
//
//                    $image_list[$index]['coords']=$face_list;
//
//                }
//
//            }
//            else{
//
//                self::add_to_log("--> error result: ".print_r($r,true)." ".Date::get_date_time_full()."\n");
//
//            }
//
//        }
//
//        return $image_list;

    }

    /**
     * @param string|NULL $source_account_link
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_link(string $source_account_link=NULL){

        $face_ID=self::get_face_ID_from_source_account_link($source_account_link);

        return !empty($face_ID);

    }

    /**
     * @param string|NULL $source_account_link
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face(string $source_account_link=NULL){

        $face_ID=self::get_face_ID_from_source_account_link($source_account_link);

        if(empty($face_ID))
            $face_ID=Face::add_face();
        
        return[
            'face_ID'=>$face_ID
        ];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @param string|NULL $name
     * @param string|NULL $source_account_link
     * @param int $image_len
     * @param int|NULL $age
     * @param string|NULL $city
     * @param string|NULL $info
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_data(int $face_ID=NULL,int $image_ID=NULL,string $name=NULL,string $source_account_link=NULL,int $image_len=0,int $age=NULL,string $city=NULL,string $info=NULL){

        $face_data_ID=FaceData::get_face_data_ID($face_ID);

        if(empty($face_data_ID))
            $face_data_ID=FaceData::add_face_data($face_ID,NULL,$image_ID,self::$source_ID,$source_account_link,$name,$age,$city,$info,$image_len);

        return[
            'face_data_ID'=>$face_data_ID
        ];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_data_image(int $face_ID=NULL,int $image_ID=NULL){

        return FaceData::update_face_data_image_ID($face_ID,$image_ID);

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face(int $face_ID=NULL){

        return Face::update_face($face_ID);

    }

    /**
     * @param int|NULL $face_data_ID
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_data(int $face_data_ID=NULL){

        return FaceData::update_face_data_to_public($face_data_ID);

    }

    /**
     * @param int|NULL $face_ID
     * @param array $image_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function upload_image_to_facekit_db(int $face_ID=NULL,array $image_list=[]){

        if(count($image_list)==0)
            return[];

        $image_ID       =NULL;
        $api_image_ID   =NULL;

        foreach($image_list as $image_index=>$image_data){

            self::add_to_log("-> start prepare image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

            $is_added=false;

            $image_list[$image_index]['face_image_ID']      =NULL;
            $facekit_image_ID                               =NULL;

//            if(count($image_data['coords'])==1){
//
//                $image_path=$image_data['image_dir'].'/'.$image_data['image_item_ID_list']['large'];
//
//                if(!file_exists(DirConfigProject::$dir_face_temp))
//                    Dir::create_dir(DirConfigProject::$dir_face_temp);
//
//                self::add_to_log("--> start convert image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");
//
//                $image_temp_path    =DirConfigProject::$dir_face_temp.'/'.$image_data['image_ID'].'.jpg';
//                $image_result       =ImageConvert::crop_image_from_file_path($image_path,$image_data['coords'][0]['x'],$image_data['coords'][0]['y'],$image_data['coords'][0]['w'],$image_data['coords'][0]['h'],$image_data['coords'][0]['w'],$image_data['coords'][0]['h']);
//
//                ImageConvert::save_image($image_result,$image_temp_path,'jpg');
//
//                self::add_to_log("--> finish convert image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");
//
//                self::add_to_log("--> start upload image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");
////                $r=UploadImageFaceKitAction::init($face_ID,$image_temp_path,true);
//                $r=UploadImageFaceKitAction::init($api_face_ID,$image_temp_path,true,false,self::$file_log_path);
//
//                self::add_to_log("--> finish upload image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");
//
//                if(!empty($r)){
//
//                    if(!empty($r['facekit_image_ID'])){
//
//                        $facekit_image_ID=$r['facekit_image_ID'];
//
//                        if(empty($image_ID))
//                            $image_ID=$image_data['image_ID'];
//
//                        self::add_to_log("--> start add to local face image image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");
//
//                        $image_list[$image_index]['face_image_ID']=FaceImage::add_face_image($face_ID,$image_data['file_ID'],$image_data['image_ID'],$facekit_image_ID,$image_data['coords']);
//
//                        self::add_to_log("--> added to local face image: ".$image_list[$image_index]['face_image_ID']." image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");
//                        self::add_to_log("--> start add to server face image image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");
//
//                        $image_list[$image_index]['api_image_ID']=self::send_add_face_image($api_face_ID,$facekit_image_ID,$image_data['file_size'],$image_data['file_content_type'],$image_data['file_mime_type'],$image_data['file_extension'],$image_data['file_path'],$image_data['coords']);
//
//                        self::add_to_log("--> added to server face image: ".$image_list[$image_index]['api_image_ID']." image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");
//
//                        $is_added=true;
//
//                    }
//
//                }
//
//            }

            if(!$is_added){

                self::add_to_log("--> start add to local face image image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

                $image_list[$image_index]['face_image_ID']=FaceImage::add_face_image($face_ID,$image_data['file_ID'],$image_data['image_ID'],NULL,NULL,[]);

                self::add_to_log("--> added to local face image: ".$image_list[$image_index]['face_image_ID']." image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

            }

            self::add_to_log("-> finish upload image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

        }

        return[
            'image_ID'          =>empty($image_ID)?$image_list[0]['image_ID']:$image_ID,
            'image_list'        =>$image_list
        ];

    }

    /**
     * @param string|NULL $profile_item_ID
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile_link(string $profile_item_ID=NULL){

        $profile_link=self::$host.'/'.self::$url_profile.'/'.$profile_item_ID;
//        $profile_link='https://atolin.ru/anketa/5482616';

        if(self::isset_face_link($profile_link)){

            self::add_to_log("WARNING: Already exists".$profile_link." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        self::add_to_log("Set profile link: ".$profile_link." ".Date::get_date_time_full()."\n");

//        $r=CurlGet::init($profile_link,[],UseragentStack::get_random_useragent());
        $r=CurlGet::init($profile_link,[],NULL,true);

        self::add_to_log("Got profile link status: ".$r['status']." ".Date::get_date_time_full()."\n");

        if($r['status']!=200){

            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            self::add_to_log("ERROR: ".$r['status']." ".Date::get_date_time_full()."\n");

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Page had status: '.$r['status']
            ];

            throw new ParametersValidationException($error);

        }

        $r=$r['data'];

        if($r!==false){

            $body=preg_match_all('/<body.*\/body>/s',$r,$body_list);

            if($body===false){

                self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                self::add_to_log("ERROR body is empty ".Date::get_date_time_full()."\n");

            }

            if($body!==false)
                if(count($body_list[0])>0){

                    $data=[
                        'id'            =>$profile_item_ID,
                        'link'          =>$profile_link,
                        'name'          =>NULL,
                        'age'           =>NULL,
                        'city'          =>NULL,
                        'info'          =>[],
                        'image_list'    =>[]
                    ];

                    $links=preg_match_all('/<a.*?data-href=\"(.*?)\"/is',$body_list[0][0],$list);

                    if($links===false){

                        self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        self::add_to_log("Image links is empty ".Date::get_date_time_full()."\n");

                        return NULL;

                    }

                    if(count($list)!=2){

                        self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        self::add_to_log("List count !=2 ".Date::get_date_time_full()."\n");

                        return NULL;

                    }

                    if(count($list[1])==0){

                        self::add_to_log("Default image links list is empty ".Date::get_date_time_full()."\n");
                        self::add_to_log("Restart prepare data-big-image-url ".Date::get_date_time_full()."\n");

                        $links=preg_match_all('/<img.*?data-big-image-url=\"(.*?)\"/is',$body_list[0][0],$list);

                        if($links===false){

                            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                            self::add_to_log("Image links is empty ".Date::get_date_time_full()."\n");

                            return NULL;

                        }

                        if(count($list)!=2){

                            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                            self::add_to_log("List count !=2 ".Date::get_date_time_full()."\n");

                            return NULL;

                        }

                    }

                    if(count($list[1])<self::$image_min_len){

                        self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        self::add_to_log("Image links list count ".count($list[1])." < ".self::$image_min_len." ".Date::get_date_time_full()."\n");

                        return NULL;

                    }

                    $image_list=[];

                    foreach($list[1] as $link){

                        $link=str_replace(' ','%20',$link);

                        $image_list[]=self::$host.$link;

                    }

                    $data['image_list']=$image_list;

                    $name_r=preg_match_all('/<h1>(.*?)<\/h1>/is',$body_list[0][0],$name_list);

                    if($name_r!==false)
                        if(count($name_list)==2)
                            if(count($name_list[1])>0)
                                $data['name']=$name_list[1][0];

                    $info_r=preg_match_all('/<span>(.*?)<\/span>/is',$body_list[0][0],$info_list);

                    if(count($info_list)==2)
                        if(count($info_list[1])==0){

                            self::add_to_log("ERROR city and age is empty ".Date::get_date_time_full()."\n");
                            self::add_to_log("Restart search city and age ".Date::get_date_time_full()."\n");

                            $info_r=preg_match_all('/<div class=\"lg\">(.*?)<\/div>/is',$body_list[0][0],$info_list);

                            if(count($info_list)==2)
                                if(count($info_list[1])==0){

                                    self::add_to_log("ERROR city and age ".Date::get_date_time_full()."\n");
                                    self::add_to_log(print_r($info_list,true)."\n");
                                    self::add_to_log($body_list[0][0]."\n");

                                    $error=[
                                        'title'     =>ParametersValidationException::$title,
                                        'info'      =>'Problem with search city and age'
                                    ];

                                    throw new ParametersValidationException($error);

                                }

                        }

                    if($info_r!==false)
                        if(count($info_list)==2)
                            if(count($info_list[1])>0){

                                list($city,$age)=mb_split(',',$info_list[1][0]);

                                $data['city']       =$city;
                                $data['age']        =(int)$age;

                                if(
                                      $data['age']<self::$age_min
                                    ||$data['age']>self::$age_max
                                ){

                                    self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                    self::add_to_log("WARNING: Profile age is not correct ".Date::get_date_time_full()."\n");

                                    return NULL;

                                }

                            }

                    $info_r         =preg_match_all('/<p>(.*?)<\/p>/is',$body_list[0][0],$info_list);
                    $need_profile   =false;

                    if($info_r!==false)
                        if(count($info_list)==2)
                            if(count($info_list[1])>0){

                                $need_profile   =true;
                                $info           =[];

                                foreach($info_list[1] as $row){

                                    $span_r=preg_match_all('/<span class="blue">(.*?)<\/span>/is',$row,$span_list);

                                    if($span_r!==false)
                                        if(count($span_list)==2)
                                            if(count($span_list[1])>0)
                                                switch($span_list[1][0]){

                                                    case'Рост':{

                                                        preg_match('/(\d+) см/is',$row,$height);

                                                        if(count($height)>0)
                                                            $info[]='Рост: '.$height[0];

                                                        break;

                                                    }

                                                    case'Вес':{

                                                        preg_match('/(\d+) кг/is',$row,$weight);

                                                        if(count($weight)>0)
                                                            $info[]='Вес: '.$weight[0];

                                                        break;

                                                    }

                                                    case'Цели знакомства':{

                                                        preg_match_all('/(?:r>|r\/>)(.*?)(?=<|$)/is',$row,$target_list);

                                                        $temp_list=[];

                                                        if(count($target_list)==2)
                                                            foreach($target_list[1] as $target_item)
                                                                switch($target_item){

                                                                    case'ищу спонсора':
                                                                    case'совместное путешествие':
                                                                    case'провести вечер':{

                                                                        $need_profile   =true;
                                                                        $temp_list[]    =$target_item;

                                                                        break;

                                                                    }

                                                                }

                                                        if(count($temp_list)==0)
                                                            $need_profile=false;

                                                        break;

                                                    }

                                                    case'О себе':{

                                                        preg_match_all('/(?:<br>|<br\/>)(.*?)$/is',$row,$about_list);

                                                        if(count($about_list)==2)
                                                            foreach($about_list[1] as $about_item)
                                                                switch($about_item){

                                                                    case'Информация отсутствует':{

                                                                        break;

                                                                    }

                                                                    default:{

                                                                        $info[]='О себе: '.str_replace(['<br />','<br>','<br/>'],"\n",trim($about_item));

                                                                        break;

                                                                    }

                                                                }

                                                        break;

                                                    }

                                                }

                                }

                                $data['info']=$info;

                            }

                    if(!$need_profile){

                        self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        self::add_to_log("WARNING: Profile dropped. Not valid data ".Date::get_date_time_full()."\n");

                        return NULL;

                    }

                    self::add_to_log("Profile data ".Date::get_date_time_full()."\n");
                    self::add_to_log(print_r($data,true)."\n");

                    return $data;

                }

        }

        return NULL;

    }

    /**
     * @param array $image_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_image_list(array $image_list=[]){

        if(count($image_list)==0)
            return[];

        $list=[];

        foreach($image_list as $link){

            $file_data=self::prepare_file($link);

            if(!empty($file_data)){

                $file_ID                            =$file_data['file_ID'];
                $file_extension                     =$file_data['file_extension'];
                $image_data                         =self::prepare_image($file_ID,'image',$file_extension);
                $image_path                         =$image_data['image_dir'].'/'.$image_data['image_item_ID_list']['large'];
                $image_data['file_path']            =$image_path;
                $image_data['file_size']            =filesize($image_path);
                $image_data['file_content_type']    ='image';
                $image_data['file_extension']       =$file_extension;
                $image_data['file_mime_type']       =File::get_file_mime_type_from_path($image_path);
                $image_data['image_resolution']     =Image::get_image_pixel_size_from_file_path($image_path);

                $key            =$image_data['image_resolution']['width'].':'.$image_data['image_resolution']['height'].':'.$image_data['file_size'].':'.$image_data['file_mime_type'];
                $list[$key]     =$image_data;

            }

        }

        return array_values($list);

    }

    /**
     * @param array|NULL $profile_data
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_profile(array $profile_data=NULL){

        if(empty($profile_data))
            return NULL;

        if(count($profile_data['image_list'])==0)
            return NULL;

        $profile_data['image_list']=self::get_image_list($profile_data['image_list']);

        if(count($profile_data['image_list'])==0)
            return NULL;

        self::add_to_log("Start create face data ".Date::get_date_time_full()."\n");

        $image_list         =self::search_face_in_image_list($profile_data['image_list']);
        $face_data          =self::prepare_face($profile_data['link']);
        $face_ID            =$face_data['face_ID'];
        $face_data          =self::prepare_face_data($face_ID,NULL,$profile_data['name'],$profile_data['link'],count($profile_data['image_list']),$profile_data['age'],$profile_data['city'],implode("\n",$profile_data['info']));
        $face_data_ID       =$face_data['face_data_ID'];
        $image_data         =self::upload_image_to_facekit_db($face_ID,$image_list);

        self::update_face_data_image($face_ID,$image_data['image_ID']);
        self::update_face($face_ID);
        self::update_face_data($face_data_ID);

        self::add_to_log("FINISH create face data ".Date::get_date_time_full()."\n");

        return $profile_data;

    }

    /**
     * @param string|NULL $search_link
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_search_link(string $search_link=NULL){

        self::add_to_log("set search profile list: ".$search_link." ".Date::get_date_time_full()."\n");

//        $r=CurlGet::init($search_link,[],UseragentStack::get_random_useragent());
        $r=CurlGet::init($search_link,[],NULL,true);

        echo $r['data'];exit;

        self::add_to_log("got DOM from link: ".$search_link." ".Date::get_date_time_full()."\n");

        if($r['status']!=200){

            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            self::add_to_log("ERROR ".$search_link." ".Date::get_date_time_full()."\n");
            self::add_to_log("Header error: ".$r['status']." ".$search_link." ".Date::get_date_time_full()."\n");
            self::add_to_log("Content: ".$r['data']." ".$search_link." ".Date::get_date_time_full()."\n");

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Page had status: '.$r['status']
            ];

            throw new ParametersValidationException($error);

        }

        $r=$r['data'];

        if($r!==false){

            $body=preg_match_all('/<body.*\/body>/s',$r,$body_list);

            if($body===false){

                self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                self::add_to_log("ERROR: have not body ".$search_link." ".Date::get_date_time_full()."\n");

                return self::get_search_link($search_link);

            }

            if($body!==false)
                if(count($body_list[0])>0){

                    $profile_list_r=preg_match_all('/<div id=\"w0\" class=\"list-view\">(.*)<\/div>/s',$body_list[0][0],$profile_list);

                    if($profile_list_r===false){

                        self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        self::add_to_log("ERROR: have not DOM element with ID W0 ".$search_link." ".Date::get_date_time_full()."\n");

                        $error=[
                            'title'     =>ParametersValidationException::$title,
                            'info'      =>'have not DOM element with ID W0'
                        ];

                        throw new ParametersValidationException($error);

                    }

                    if($profile_list_r!==false){

                        if(count($profile_list[0])==0){

                            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                            self::add_to_log("ERROR: Profile list is empty ".$search_link." ".Date::get_date_time_full()."\n");

                            return self::get_search_link($search_link);

                        }

                        preg_match_all('/<div data-key=\"(.*?)\">(.*?)<\/div>/is',$profile_list[1][0],$profile_item_list);

                        if(count($profile_item_list)<2){

                            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                            self::add_to_log("Success: finish search list ".$search_link." ".Date::get_date_time_full()."\n");

                            $data=[
                                'success'=>true
                            ];

                            return ResponseSuccess::init($data);

                        }

                        if(count($profile_item_list[1])==0){

                            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                            self::add_to_log("ERROR: Profile list is empty. Maybe banned! ".$search_link." ".Date::get_date_time_full()."\n");

                            $error=[
                                'title'     =>ParametersValidationException::$title,
                                'info'      =>'Profile list is empty. Maybe banned!'
                            ];

                            throw new ParametersValidationException($error);

                        }

//                        echo'profile item list 111:'."\n";
//                        echo count($profile_item_list)."\n";
//                        print_r($profile_item_list);exit;

                        self::add_to_log("Have profile list count: ".count($profile_item_list[1])." ".Date::get_date_time_full()."\n");

                        foreach($profile_item_list[1] as $profile_index=>$profile_item){

                            self::add_to_log("Start prepare profile item with index: ".$profile_index." ".Date::get_date_time_full()."\n");

                            $profile_data=self::prepare_profile_link($profile_item);

                            if(empty($profile_data)){

                                self::add_to_log("Empty profile data with index: ".$profile_index." ".Date::get_date_time_full()."\n");

                            }

                            if(!empty($profile_data)){

                                self::add_to_log("Start prepare profile data with index: ".$profile_index." ".Date::get_date_time_full()."\n");

                                self::prepare_profile($profile_data);

                                self::add_to_log("Finish prepare profile data: ".$profile_index." ".Date::get_date_time_full()."\n");

                            }

                            self::sleep_random();

                            self::$profile_index++;

                            $profile_index_max=rand(self::$sleep_profile_len_list[0],self::$sleep_profile_len_list[1]);

                            if(self::$profile_index>=$profile_index_max){

                                self::$profile_index=0;

                                self::sleep_after_profile_uploaded();

                            }

                        }

                    }

                }

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_profile_links(){

        libxml_use_internal_errors(true);

        do{

            self::$profile_index=0;

            $search_link=self::$host.'/'.self::$url_search.self::$page;

            self::get_search_link($search_link);

            self::$page++;

        }while(true);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        self::set_log_path();
        self::get_profile_links();

        return self::set_return();

    }

    private static function get_rul($url)
    {
        // ensure PHP cURL library is installed
        if(function_exists('curl_init'))
        {
        $timestart=microtime(true);

//        $ip = '127.0.0.1';
//        $port = '9050';
//        $auth = 'rebootip111';
//        $command = 'signal NEWNYM';
//
//        $fp = fsockopen($ip,$port,$error_number,$err_string,10);
//
//        if(!$fp)
//        {
//          echo "ERROR: $error_number : $err_string";
//          return false;
//        }
//        else
//        {
//                fwrite($fp,"AUTHENTICATE \"".$auth."\"\n");
//                $received = fread($fp,512);
//                fwrite($fp,$command."\n");
//                $received = fread($fp,512);
//        }
//        fclose($fp);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROXY, "127.0.0.1:9050");
        curl_setopt($ch, CURLOPT_PROXYTYPE, 7);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        $response = curl_exec($ch);
        $error = curl_error($ch);
        print_r($response."\n");
        print_r($error."\n");
        }
        else // PHP cURL library not installed
        {
        echo 'Please install PHP cURL library';
        }
    }

    private static function tor_new_identity($tor_ip='127.0.0.1', $control_port='9050', $auth_code=''){
        $fp=fsockopen($tor_ip, $control_port, $errno, $errstr, 1);
        if (!$fp) return false;
        fputs($fp, "AUTHENTICATE $auth_code\r\n");
        $response = fread($fp, 1024);
        echo $response."\n";
//        list($code, $text) = explode(' ', $response, 2);
//        if ($code != '250') return false;
        fputs($fp, "signal NEWNYM\r\n");
        $response = fread($fp, 1024);
        echo $response."\n";
//        list($code, $text) = explode(' ', $response, 2);
//        if ($code != '250') return false;
        fclose($fp);
        return true;
    }

    private static  function CreateContext() {

        self::GetNewIdentity();

        $curent_proxy='127.0.0.1:81198';

        $opts       =array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept-language: en\r\n"."Cookie: yandex_gid=213\r\n",
                'user_agent'=>'Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.4; en-US; rv:1.9.2.28) Gecko/20120306 Firefox/3.6.28',
                'proxy'=>$curent_proxy,
                'request_fulluri'=>true
            )
        );
        $context    =stream_context_create($opts);

        return $context;

    }

    /**
     *
     */
    private static function GetNewIdentity() {

        if (self::tor_new_identity('127.0.0.1','9050',rand(111,99999999999))) {
            sleep(2);
        }
        else {
            self::GetNewIdentity();
        }
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        self::GetNewIdentity();
//        self::get_rul('http://ipinfo.io/');exit;

        if(empty($_REQUEST['page'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Page is empty'
            ];

            throw new ParametersException($error);

        }

        self::$page=(int)$_REQUEST['page'];

        return self::set();

    }

}