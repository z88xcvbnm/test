<?php

namespace Project\Whore\Admin\Api\Source;

use Core\Module\Response\ResponseSuccess;
use Project\Whore\All\Module\Face\FaceData;

class ReplaceWeightHeightInInfoApi{

    /** @var array */
    private static $source_ID_list=[17,18];

    /** @var int */
    private static $len=100;

    /** @var int */
    private static $face_data_len=0;

    /**
     * @param string $info
     * @return string|string[]|null
     */
    private static function prepare_replace(string $info=NULL){

        if(empty($info))
            return'';

        return preg_replace('/(\d+\s*(?:см|кг)(?:,\s*|\s*))/is','',$info);

    }

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare(){

        $offset=0;

        do{

            $r=FaceData::get_face_data_info_list(self::$source_ID_list,$offset,self::$len);

            if(count($r)==0)
                return true;

            foreach($r as $face_ID=>$info){

                $info=self::prepare_replace($info);

                FaceData::update_face_data_info($face_ID,$info);

                self::$face_data_len++;

            }

            $offset+=self::$len;

        }while(true);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'   =>true,
            'data'      =>[
                'face_data_len'=>self::$face_data_len
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::prepare();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}