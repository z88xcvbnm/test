<?php

namespace Project\Whore\Admin\Api\Faceset;

use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\File\File;
use Core\Module\Image\Image;
use Core\Module\Image\ImageFace;
use Core\Module\Json\Json;
use Core\Module\OsServer\OsServer;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\User\UserBalance;
use Project\Whore\All\Module\Face\FaceSearch;
use Project\Whore\All\Module\Face\Faceset;
use Project\Whore\All\Module\Price\PriceConfig;

class GetFaceSearchPartsLenApi{

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $image_ID;

    /** @var int */
    private static $face_search_ID;

    /** @var string */
    private static $face_search_token;

    /** @var string */
    private static $face_token;

    /** @var int */
    private static $faceset_len;

    /** @var array */
    private static $face_coords_list=[];

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_file_ID(){

        if(!File::isset_file(self::$file_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_ID(){

        if(!Image::isset_image_ID(self::$image_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image ID is empty'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_face_token(){

        if(!ImageFace::isset_image_face(self::$image_ID,self::$face_token)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face token is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_user_balance(){

        $balance=(int)UserBalance::get_user_balance(User::$user_ID);

        if($balance<PriceConfig::$search_price){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Your balance is low'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_faceset_token(){

        $data=[
            User::$user_ID,
            self::$file_ID,
            self::$image_ID,
            self::$face_token,
            time(),
            rand(0,time())
        ];

        self::$face_search_token=Hash::get_sha1_encode(implode(':',$data));

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_faceset_len(){

        self::$faceset_len=Faceset::get_faceset_len();

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_search_face(){

        self::$face_search_ID=FaceSearch::add_face_search(self::$file_ID,self::$image_ID,NULL,NULL,self::$face_coords_list,self::$face_token,NULL,NULL,NULL,self::$face_search_token,0,self::$faceset_len,2);

        if(empty(self::$face_search_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face search was not create'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'face_search_token'     =>self::$face_search_token,
                'parts_len'             =>self::$faceset_len
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::check_user_balance())
            if(self::isset_file_ID())
                if(self::isset_image_ID())
                    if(self::isset_image_face_token()){

                        self::set_faceset_len();
                        self::set_faceset_token();

                        self::add_search_face();

                        return self::set_return();

                    }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

//        if(OsServer::$is_windows)
//            \Config::$is_debug=true;

        $error_info_list=[];

        if(empty($_POST['file_ID']))
            $error_info_list[]='File ID is empty';

        if(empty($_POST['image_ID']))
            $error_info_list[]='Image ID is empty';

        if(empty($_POST['face_token']))
            $error_info_list[]='Face token is empty';

        if(empty($_POST['face_coords']))
            $error_info_list[]='Face coords is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$file_ID              =(int)$_POST['file_ID'];
        self::$image_ID             =(int)$_POST['image_ID'];
        self::$face_token           =$_POST['face_token'];
        self::$face_coords_list     =Json::decode($_POST['face_coords']);

        return self::set();

    }

}