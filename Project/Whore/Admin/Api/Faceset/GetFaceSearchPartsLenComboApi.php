<?php

namespace Project\Whore\Admin\Api\Faceset;

use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\File\File;
use Core\Module\Image\Image;
use Core\Module\Image\ImageFace;
use Core\Module\Json\Json;
use Core\Module\OsServer\OsServer;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\User\UserBalance;
use Core\Module\User\UserBalanceTransaction;
use Project\Whore\All\Module\Face\FaceSearch;
use Project\Whore\All\Module\Face\FaceSearchMulti;
use Project\Whore\All\Module\Face\Faceset;
use Project\Whore\All\Module\Price\PriceConfig;

class GetFaceSearchPartsLenComboApi{

    /** @var int */
    private static $face_search_multi_ID;

    /** @var int */
    private static $user_balance_transaction_ID;

    /** @var int */
    private static $image_len;

    /** @var float */
    private static $price;

    /** @var float */
    private static $user_balance;

    /** @var string */
    private static $face_search_multi_token;

    /** @var array */
    private static $face_search_token_list=[];

    /** @var array */
    private static $list=[];

    /** @var int */
    private static $faceset_len;

    /** @var bool */
    private static $is_test=false;

    /**
     * @param int|NULL $file_ID
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_file_ID(int $file_ID=NULL){

        if(empty($file_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new ParametersException($error);

        }

        if(!File::isset_file($file_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $image_ID
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_ID(int $image_ID=NULL){

        if(empty($image_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new ParametersException($error);

        }

        if(!Image::isset_image_ID($image_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Image ID is empty'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $face_token
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_image_face_token(int $image_ID=NULL,string $face_token=NULL){

        $error_info_list=[];

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($face_token))
            $error_info_list[]='Face token is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        if(!ImageFace::isset_image_face($image_ID,$face_token)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Face token is not exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_user_balance(){

        $balance        =(int)UserBalance::get_user_balance(User::$user_ID);
        $list_len       =count(self::$list);

        switch($list_len){

            case 0:{

                $total_price=0;

                break;

            }

            case 1:{

                $total_price=PriceConfig::$search_price;

                break;

            }

            case 2:
            case 3:{

                $total_price=2*PriceConfig::$search_price;

                break;

            }

            default:{

                $error=[
                    'title'     =>ParametersValidationException::$title,
                    'info'      =>'Face len is not valid'
                ];

                throw new ParametersValidationException($error);

                break;

            }

        }

        if($balance<$total_price){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Your balance is low'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_face_search_multi_token(){

        $data=[
            User::$user_ID,
            time(),
            rand(0,time())
        ];

        foreach(self::$list as $row){

            $data[]=$row['image_ID'];
            $data[]=$row['file_ID'];

        }

        self::$face_search_multi_token=Hash::get_sha1_encode(implode(':',$data));

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_faceset_len(){

        self::$faceset_len=Faceset::get_faceset_len();

//        if(self::$is_test)
//            self::$faceset_len=15;

        if(self::$faceset_len==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Facesets are empty'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_len(){

        self::$image_len=count(self::$list);

        if(self::$image_len==0){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Your balance is low'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_price(){

        self::$price=PriceConfig::get_price(self::$image_len);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_balance_transaction(){

        self::$user_balance_transaction_ID=UserBalanceTransaction::add_user_balance_transaction(User::$user_ID,NULL,NULL,NULL,NULL,'find_face_combo',self::$price);

        if(empty(self::$user_balance_transaction_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'balance transaction was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_balance(){

        if(!UserBalance::remove_money_user_balance(User::$user_ID,self::$price)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Balance was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_balance_in_user_balance_transaction(){

        if(!UserBalanceTransaction::update_balance_in_user_balance_transaction(self::$user_balance_transaction_ID,self::$user_balance)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Balance was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_balance(){

        self::$user_balance=UserBalance::get_user_balance(User::$user_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_user_balance(){

        self::add_user_balance_transaction();
        self::update_user_balance();
        self::set_user_balance();
        self::update_balance_in_user_balance_transaction();

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_search_multi(){

        self::$face_search_multi_ID=FaceSearchMulti::add_face_search_multi(self::$user_balance_transaction_ID,count(self::$list),self::$price,self::$face_search_multi_token);

        if(empty(self::$face_search_multi_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Face search multi was not create'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_face_search(){

        foreach(self::$list as $index=>$row){

            $data=[
                User::$user_ID,
                $row['file_ID'],
                $row['image_ID'],
                self::$user_balance_transaction_ID,
                $row['face_token'],
                time(),
                rand(0,time())
            ];

            $face_search_token                  =Hash::get_sha1_encode(implode(':',$data));
            self::$face_search_token_list[]     =$face_search_token;

            self::$list[$index]['face_search_ID']=FaceSearch::add_face_search($row['file_ID'],$row['image_ID'],NULL,self::$user_balance_transaction_ID,$row['face_coords_list'],$row['face_token'],NULL,NULL,NULL,$face_search_token,0,self::$faceset_len,2,self::$face_search_multi_ID);

            if(empty(self::$list[$index]['face_search_ID'])){

                $error=[
                    'title'     =>PhpException::$title,
                    'info'      =>'Face search was not create'
                ];

                throw new PhpException($error);

            }

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'face_search_multi_token'   =>self::$face_search_multi_token,
                'face_search_token_list'    =>self::$face_search_token_list,
                'parts_len'                 =>self::$faceset_len,
                'balance'                   =>self::$user_balance
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_image_data(){

        foreach(self::$list as $row){

            self::isset_file_ID($row['file_ID']);
            self::isset_image_ID($row['image_ID']);
            self::isset_image_face_token($row['image_ID'],$row['face_token']);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::check_user_balance())
            if(self::check_image_data()){

                self::set_image_len();
                self::set_price();
                self::prepare_user_balance();

                self::set_faceset_len();
                self::set_face_search_multi_token();

                self::add_face_search_multi();
                self::add_face_search();

                return self::set_return();

            }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

//        if(OsServer::$is_windows)
//            \Config::$is_debug=true;

        if(empty($_POST['list'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'List is empty'
            ];

            throw new ParametersException($error);

        }

        self::$is_test  =!empty($_POST['is_test']);
        self::$list     =Json::decode($_POST['list']);

        return self::set();

    }

}