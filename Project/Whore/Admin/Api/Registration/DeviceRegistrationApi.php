<?php

namespace Project\Whore\Admin\Api\Registration;

use Core\Module\Device\DeviceFirm;
use Core\Module\Device\DeviceModel;
use Core\Module\Device\DeviceToken;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Os\Os;

class DeviceRegistrationApi{

    /** @var int */
    private static $device_token_ID;

    /** @var int */
    private static $device_firm_ID;

    /** @var int */
    private static $device_model_ID;

    /** @var int */
    private static $os_ID;

    /** @var int */
    private static $os_version_ID;

    /** @var string */
    private static $device_type;

    /** @var string */
    private static $device_firm;

    /** @var string */
    private static $device_model;

    /** @var string */
    private static $device_hash;

    /** @var string */
    private static $device_token;

    /** @var string */
    private static $device_os_name;

    /** @var string */
    private static $device_os_version;

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_device_firm(){

        if(empty(self::$device_firm))
            return false;

        self::$device_firm_ID=DeviceFirm::get_device_firm_ID(self::$device_firm);

        if(empty(self::$device_firm_ID))
            self::$device_firm_ID=DeviceFirm::add_device_firm_name(self::$device_firm);

        if(empty(self::$device_firm_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device firm was not added'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_device_model(){

        if(empty(self::$device_model))
            return false;

        self::$device_model_ID=DeviceModel::get_device_model_ID(self::$device_model);

        if(empty(self::$device_model_ID))
            self::$device_model_ID=DeviceModel::add_device_model_name(self::$device_model);

        if(empty(self::$device_model_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device model was not added'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_device_hash(){

        if(empty(self::$device_hash))
            return false;

        if(empty(self::$device_token))
            self::$device_token_ID=DeviceToken::get_device_token_ID_from_device_hash(self::$device_hash);
        else
            self::$device_token_ID=DeviceToken::get_device_token_ID_from_device_hash_and_token(self::$device_hash,self::$device_token);

        if(empty(self::$device_token_ID))
            self::$device_token_ID=DeviceToken::add_device_token(self::$device_firm_ID,self::$device_model_ID,self::$os_ID,self::$os_version_ID,self::$device_hash,self::$device_token);

        if(empty(self::$device_token_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device token was not added'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function isset_device_token(){

        if(empty(self::$device_token))
            return false;

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_device_os_name(){

        if(empty(self::$device_os_name))
            return false;

        self::$os_ID=Os::get_os_ID(self::$device_os_name);

        if(empty(self::$os_ID))
            self::$os_ID=Os::add_os(self::$device_os_name);

        if(empty(self::$os_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'OS name was not added'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function isset_device_os_version(){

        return !empty(self::$device_os_version);

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_device_firm())
            if(self::isset_device_model())
                if(self::isset_device_os_name())
                    if(self::isset_device_os_version())
                        return true;

        return false;

    }

    /**
     * @param string|NULL $device_type
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $device_type=NULL){

        $error_info_list=[];

        if(empty($device_type))
            $error_info_list[]='Device type is empty';

        if(empty($_POST['device_firm']))
            $error_info_list[]='Device firm is empty';

        if(empty($_POST['device_model']))
            $error_info_list[]='Device model is empty';

        if(empty($_POST['device_hash']))
            $error_info_list[]='Device hash is empty';

        if(empty($_POST['device_token']))
            $error_info_list[]='Device token is empty';

        if(empty($_POST['device_os_name']))
            $error_info_list[]='Device OS name is empty';

        if(empty($_POST['device_os_version']))
            $error_info_list[]='Device OS version is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$device_type          =$device_type;
        self::$device_firm          =$_POST['device_firm'];
        self::$device_model         =$_POST['device_model'];
        self::$device_hash          =$_POST['device_hash'];
        self::$device_token         =$_POST['device_token'];
        self::$device_os_name       =$_POST['device_os_name'];
        self::$device_os_version    =$_POST['device_os_version'];

        return self::set();

    }

}