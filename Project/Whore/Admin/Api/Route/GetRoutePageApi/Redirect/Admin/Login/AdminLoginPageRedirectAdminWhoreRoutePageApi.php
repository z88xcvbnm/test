<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Admin\Login;

use Core\Module\Response\ResponseSuccess;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Config\PageConfigAdminWhore;

class AdminLoginPageRedirectAdminWhoreRoutePageApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_redirect(){

        $user_login=UserLogin::get_user_login_default();

        $data=array(
            'title'     =>PageConfigAdminWhore::get_page_title_default(),
            'redirect'  =>'/'.PageConfigAdminWhore::$root_page_default.'/'.$user_login.'/'.PageConfigAdminWhore::$root_admin_page_default
        );

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        return self::init_redirect();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}