<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Profile\Login\Settings;

use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Config\PageConfigAdminWhore;
use Project\Whore\Admin\Config\PageConfigProfileWhore;

class ProfileLoginSettingsPageRedirectAdminWhoreRoutePageApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_redirect(){

        if(User::is_login()){

            $user_login=UserLogin::get_user_login_default();

            $data=array(
                'title'     =>PageConfigAdminWhore::get_page_content_title_default(),
                'redirect'  =>'/'.PageConfigProfileWhore::$root_page_default.'/'.$user_login.'/'.PageConfigProfileWhore::$root_profile_settings_page_default
            );

            return ResponseSuccess::init($data);

        }

        return AuthPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::init_redirect();

    }

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::set();

    }

}