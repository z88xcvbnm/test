<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Prepare;

use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Project\Whore\Admin\Api\Route\GetRoutePageApi;

use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Auth\AuthPageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Reg\RegPageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Root\About\RootAboutPageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Root\Term\RootTermPageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Root\Source\RootSourcePageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Root\City\RootCityPageAdminWhoreRoutePageApi;

use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Root\RootPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Admin\AdminRootPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Profile\ProfileRootPageRedirectAdminWhoreRoutePageApi;

class PrepareLinkLen1AdminWhoreRoutePageApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_auth(){

        if(User::is_login()){

            if(
                UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return AdminRootPageRedirectAdminWhoreRoutePageApi::init();
            else if(
                UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return ProfileRootPageRedirectAdminWhoreRoutePageApi::init();

        }

        return AuthPageAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_reg(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return AdminRootPageRedirectAdminWhoreRoutePageApi::init();
            else if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return ProfileRootPageRedirectAdminWhoreRoutePageApi::init();

        }

        return RegPageAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_about(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return AdminRootPageRedirectAdminWhoreRoutePageApi::init();
            else if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return ProfileRootPageRedirectAdminWhoreRoutePageApi::init();

        }

        return RootAboutPageAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_source(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return AdminRootPageRedirectAdminWhoreRoutePageApi::init();
            else if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return ProfileRootPageRedirectAdminWhoreRoutePageApi::init();

        }

        return RootSourcePageAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_term(){

        if(User::is_login()){

            if(
                UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return AdminRootPageRedirectAdminWhoreRoutePageApi::init();
            else if(
                UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return ProfileRootPageRedirectAdminWhoreRoutePageApi::init();

        }

        return RootTermPageAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_city(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return AdminRootPageRedirectAdminWhoreRoutePageApi::init();
            else if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return ProfileRootPageRedirectAdminWhoreRoutePageApi::init();

        }

        return RootCityPageAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin(){

        if(User::is_login())
            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return AdminRootPageRedirectAdminWhoreRoutePageApi::init();

        return AuthPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile(){

        if(User::is_login())
            if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return ProfileRootPageRedirectAdminWhoreRoutePageApi::init();

        return AuthPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        switch(GetRoutePageApi::$list[0]){

            case'login':
            case'auth':
                return self::prepare_auth();

            case'reg':
                return self::prepare_reg();

            case'about':
                return self::prepare_about();

            case'term':
                return self::prepare_term();

            case'city':
                return self::prepare_city();

            case'source':
                return self::prepare_source();

            case'admin':
                return self::prepare_admin();

            case'profile':
                return self::prepare_profile();

            default:
                return RootPageRedirectAdminWhoreRoutePageApi::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}