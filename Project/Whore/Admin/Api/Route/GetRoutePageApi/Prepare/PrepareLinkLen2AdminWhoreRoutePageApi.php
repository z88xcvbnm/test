<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Prepare;

use Core\Action\Logout\UserLogout\UserLogoutSystemAction;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Api\Route\GetRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\ConfirmEmail\ConfirmEmailPageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\RecoveryPassword\RecoveryPasswordPageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Invite\InvitePageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Admin\AdminRootPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Profile\ProfileRootPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Root\RootPageRedirectAdminWhoreRoutePageApi;

class PrepareLinkLen2AdminWhoreRoutePageApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_auth(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return AdminRootPageRedirectAdminWhoreRoutePageApi::init();
            else if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return ProfileRootPageRedirectAdminWhoreRoutePageApi::init();

        }

        return AuthPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin_login(){

        $user_login=UserLogin::get_user_login_default();

        if($user_login==GetRoutePageApi::$list[1])
            return AdminRootPageRedirectAdminWhoreRoutePageApi::init();

        return AuthPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile_login(){

        $user_login=UserLogin::get_user_login_default();

        if($user_login==GetRoutePageApi::$list[1])
            return ProfileRootPageRedirectAdminWhoreRoutePageApi::init();

        return AuthPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return self::prepare_admin_login();

        }

        return AuthPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile(){

        if(User::is_login()){

            if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return self::prepare_profile_login();

        }

        return AuthPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_invite(){

        if(User::is_login())
            UserLogoutSystemAction::init();

        if(empty(GetRoutePageApi::$list[1]))
            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        return InvitePageAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_recovery_password(){

        if(User::is_login())
            UserLogoutSystemAction::init();

        if(empty(GetRoutePageApi::$list[1]))
            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        return RecoveryPasswordPageAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_confirm_email(){

        if(User::is_login())
            UserLogoutSystemAction::init();

        if(empty(GetRoutePageApi::$list[1]))
            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        return ConfirmEmailPageAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        switch(GetRoutePageApi::$list[0]){

            case'auth':
                return self::prepare_auth();

            case'admin':
                return self::prepare_admin();

            case'profile':
                return self::prepare_profile();

            case'registration_invite':
                return self::prepare_invite();

            case'recovery_password':
                return self::prepare_recovery_password();

            case'confirm_email':
                return self::prepare_confirm_email();

            default:
                return RootPageRedirectAdminWhoreRoutePageApi::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}