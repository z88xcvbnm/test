<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Prepare;

use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Root\RootPageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Admin\AdminRootPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Profile\ProfileRootPageRedirectAdminWhoreRoutePageApi;

class PrepareLinkLen0AdminWhoreRoutePageApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(User::is_login()){

            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            )
                return AdminRootPageRedirectAdminWhoreRoutePageApi::init();
            else if(
                  UserAccess::$is_profile
                ||UserAccess::$is_profile_wallet
            )
                return ProfileRootPageRedirectAdminWhoreRoutePageApi::init();

        }

        return RootPageAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}