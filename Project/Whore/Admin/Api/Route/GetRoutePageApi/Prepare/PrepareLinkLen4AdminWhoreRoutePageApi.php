<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Prepare;

use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Api\Route\GetRoutePageApi;

use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Admin\Login\Financial\Settings\AdminLoginFinancialSettingsPageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Admin\Login\Financial\Stats\AdminLoginFinancialStatsPageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Admin\Login\Financial\Transaction\AdminLoginFinancialTransactionPageAdminWhoreRoutePageApi;

use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Admin\Login\Profile\ProfileList\AdminLoginProfileListPageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Admin\Login\Profile\ProfileRequest\AdminLoginProfileRequestPageAdminWhoreRoutePageApi;

use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Admin\Login\Source\SourceList\AdminLoginSourceListPageAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Admin\Login\Source\SourceParsing\AdminLoginSourceParsingPageAdminWhoreRoutePageApi;

use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Admin\AdminRootPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Root\RootPageRedirectAdminWhoreRoutePageApi;

class PrepareLinkLen4AdminWhoreRoutePageApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin_login_financial(){

        if(!empty(GetRoutePageApi::$list[3]))
            switch(GetRoutePageApi::$list[3]){

                case'settings':
                    return AdminLoginFinancialSettingsPageAdminWhoreRoutePageApi::init();

                case'stats':
                    return AdminLoginFinancialStatsPageAdminWhoreRoutePageApi::init();

                case'transaction':
                    return AdminLoginFinancialTransactionPageAdminWhoreRoutePageApi::init();

            }

        return AdminRootPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin_login_profile(){

        if(!empty(GetRoutePageApi::$list[3]))
            switch(GetRoutePageApi::$list[3]){

                case'list':
                    return AdminLoginProfileListPageAdminWhoreRoutePageApi::init();

                case'request':
                    return AdminLoginProfileRequestPageAdminWhoreRoutePageApi::init();

            }

        return AdminRootPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin_login_source(){

        if(!empty(GetRoutePageApi::$list[3]))
            switch(GetRoutePageApi::$list[3]){

                case'list':
                    return AdminLoginSourceListPageAdminWhoreRoutePageApi::init();

                case'parsing':
                    return AdminLoginSourceParsingPageAdminWhoreRoutePageApi::init();

            }

        return AdminRootPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin_login(){

        switch(GetRoutePageApi::$list[2]){

            case'financial':
                return self::prepare_admin_login_financial();

            case'profile':
                return self::prepare_admin_login_profile();

            case'source':
                return self::prepare_admin_login_source();

            default:
                return AdminRootPageRedirectAdminWhoreRoutePageApi::init();

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_admin(){

        if(User::is_login())
            if(
                  UserAccess::$is_root
                ||UserAccess::$is_admin
            ){

                $user_login=UserLogin::get_user_login_default();

                if($user_login==GetRoutePageApi::$list[1])
                    return self::prepare_admin_login();

            }

        return AuthPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        switch(GetRoutePageApi::$list[0]){

            case'admin':
                return self::prepare_admin();

            default:{

                if(User::is_login())
                    return AdminRootPageRedirectAdminWhoreRoutePageApi::init();

                return RootPageRedirectAdminWhoreRoutePageApi::init();

            }

        }

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}