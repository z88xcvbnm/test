<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\ConfirmEmail;

use Core\Module\Email\EmailSend;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Core\Module\PromoCode\PromoCode;
use Core\Module\PromoCode\PromoCodeConfig;
use Core\Module\PromoCode\PromoCodeItem;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserBalance;
use Core\Module\User\UserBalanceTransaction;
use Core\Module\User\UserEmail;
use Core\Module\User\UserEmailCheck;
use Core\Module\User\UserHash;
use Core\Module\User\UserHashTypeConfig;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Root\RootPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Action\Face\GetFaceImageRandomListAction;
use Project\Whore\Admin\Action\Face\GetFaceStatsForRootAction;
use Project\Whore\Admin\Api\Route\GetRoutePageApi;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;

class ConfirmEmailPageAdminWhoreRoutePageApi{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_email_ID;

    /** @var int */
    private static $user_email_check_ID;

    /** @var int */
    private static $user_hash_ID;

    /** @var int */
    private static $user_hash_type_ID;

    /** @var string */
    private static $login;

    /** @var string */
    private static $email;

    /** @var string */
    private static $hash;

    /** @var string */
    private static $user_hash_type_name         ='confirm_email';

    /** @var int */
    private static $face_len                    =0;

    /** @var int */
    private static $face_image_indexed_len      =0;

    /** @var array */
    private static $face_list                   =[];

    /** @var array */
    private static $face_image_random_list      =[];

    /** @var array */
    private static $city_list                   =[];

    /** @var bool */
    private static $is_success                  =false;

    /** @var string */
    private static $promo_login                 =NULL;

    /** @var string */
    private static $promo_code                  =NULL;

    /** @var float */
    private static $money_bonus;

    /** @var bool */
    private static $is_bonus                    =false;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_hash(){

        $r=UserHash::get_user_hash_data_from_hash(self::$hash,self::$user_hash_type_ID,false);

        if(empty($r)){

            self::$is_success=false;

            return false;

        }

        self::$user_hash_ID     =$r['ID'];
        self::$user_ID          =$r['user_ID'];
        self::$promo_code       =$r['code'];
        self::$promo_login      =$r['login'];

        self::$email            =UserEmail::get_user_email_last(self::$user_ID);
        self::$login            =UserLogin::get_user_login(self::$user_ID);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_promo_code(){

        $r=PromoCode::get_promo_code_data(self::$promo_code);

        if(empty($r))
            return false;

        $promo_code_ID          =$r['ID'];
        self::$money_bonus      =$r['money'];
        $is_unique              =$r['is_unique'];
        $use_len                =$r['use_len'];
        $use_max_len            =$r['use_max_len'];

        if($use_len>=$use_max_len){

            $user_email     =UserEmail::get_user_email_last(self::$user_ID);
            $user_login     =UserLogin::get_user_login(self::$user_ID);

            $inner=[];

            $inner[]    ='Здравствуйте, '.$user_login.'!';
            $inner[]    ='';
            $inner[]    ='Спасибо за регистрацию. К сожалению ваш промокод не попал в число первых '.$use_max_len.' активированных кодов.';
            $inner[]    ='';
            $inner[]    ='С уважением,';
            $inner[]    ='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

            return EmailSend::init(array($user_email),'Начисление бонуса '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

        }

        PromoCodeItem::add_promo_code_item(self::$user_ID,$promo_code_ID,self::$promo_code,self::$money_bonus);

        if($is_unique)
            PromoCode::update_to_used($promo_code_ID);
        else
            PromoCode::update_use_len($promo_code_ID);

        UserBalanceTransaction::add_user_balance_transaction(self::$user_ID,NULL,NULL,NULL,NULL,'promo_code',self::$money_bonus,'Промокод: '.self::$promo_code,self::$money_bonus,NULL,NULL,NULL,self::$promo_code);
        UserBalance::add_money_user_balance(self::$user_ID,self::$money_bonus);

        self::$is_bonus=true;

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_promo_login(){

        $user_ID=UserLogin::get_user_ID_from_login(self::$promo_login);

        if(empty($user_ID))
            return false;

        $user_email     =UserEmail::get_user_email_last($user_ID);
        $user_login     =UserLogin::get_user_login($user_ID);

        if(empty($user_email))
            return false;

        self::$money_bonus=PromoCodeConfig::$user_reg_profit_money;

        UserBalanceTransaction::add_user_balance_transaction(self::$user_ID,NULL,NULL,NULL,NULL,'promo_login_use',self::$money_bonus,'Invite bonus with login: '.self::$promo_login,self::$money_bonus,NULL,NULL,NULL,self::$promo_login);
        UserBalance::add_money_user_balance(self::$user_ID,self::$money_bonus);

        $user_balance=UserBalance::get_user_balance($user_ID)+PromoCodeConfig::$user_invite_profit_money;

        UserBalanceTransaction::add_user_balance_transaction($user_ID,NULL,NULL,NULL,NULL,'promo_login_invite',PromoCodeConfig::$user_invite_profit_money,'Invite bonus with login: '.self::$login,$user_balance,NULL,NULL,NULL,self::$login);
        UserBalance::update_user_balance($user_ID,$user_balance);

        self::$login        =UserLogin::get_user_login(self::$user_ID);
        self::$is_bonus     =true;

        $inner=[];

        $inner[]    ='Здравствуйте, '.$user_login.'!';
        $inner[]    ='';
        $inner[]    ='Вам начислен бонус '.PromoCodeConfig::$user_invite_profit_money.' ₽ за приглашенного пользователя '.self::$login;
        $inner[]    ='';
        $inner[]    ='С уважением,';
        $inner[]    ='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

        return EmailSend::init(array($user_email),'Начисление бонуса '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_promo(){

        if(empty(self::$promo_login)&&empty(self::$promo_code))
            return false;

        if(!empty(self::$promo_code))
            return self::prepare_promo_code();

        if(!empty(self::$promo_login))
            return self::prepare_promo_login();

        return false;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_notification(){

        $inner=[];

        $inner[]='Здравствуйте, '.self::$login.'!';
        $inner[]='';
        $inner[]='Спасибо за регистрацию. Ваш аккаунт подтвержден.';

        if(self::$is_bonus){

            $inner[]='';
            $inner[]='Вам начислен бонус '.self::$money_bonus.' ₽';

        }

        $inner[]='';
        $inner[]='С уважением,';
        $inner[]='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

        return EmailSend::init(array(self::$email),'Подтверждение аккаунта '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_hash(){

        if(!UserHash::update_user_hash_date_use_from_user_hash_ID(self::$user_hash_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User hash was not update'
            ];

            throw new PhpException($error);

        }

        self::$is_success=true;

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_hash_type_ID(){

        self::$user_hash_type_ID=UserHashTypeConfig::get_user_hash_type_ID(self::$user_hash_type_name);

        if(empty(self::$user_hash_type_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User hash type ID is empty'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_date_email_check(){

        if(!User::update_user_date_email_check(self::$user_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User date email check was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_root_stats(){

        $r=GetFaceStatsForRootAction::init();

        self::$face_len                     =$r['face_len'];
        self::$city_list                    =$r['city_list'];
        self::$face_image_indexed_len       =$r['face_image_indexed_len'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_random_list(){

        $r=GetFaceImageRandomListAction::init();

        if(count($r)==0)
            return false;

        self::$face_image_random_list   =$r['face_image_list'];
        self::$face_list                =$r['face_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_email_ID(){

        self::$user_email_ID=UserEmail::get_user_email_ID(self::$user_ID);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_email_check(){

        self::$user_email_check_ID=UserEmailCheck::add_user_email_check(self::$user_ID,self::$user_email_ID,NULL,self::$hash);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=array(
            'action'        =>'confirm_email',
            'data'          =>[
                'success'                   =>self::$is_success,
                'face_len'                  =>self::$face_len,
                'face_image_indexed_len'    =>self::$face_image_indexed_len,
                'city_list'                 =>self::$city_list,
                'face_list'                 =>self::$face_list,
                'face_image_random_list'    =>self::$face_image_random_list
            ]
        );

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('confirm_email_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('confirm_email'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    page_object.action.confirm_email.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_user_hash_type_ID();

        if(!self::isset_user_hash()){

            self::set_root_stats();
            self::set_face_image_random_list();

            return self::init_page();

        }

        self::update_user_hash();
        self::set_user_email_ID();
        self::add_user_email_check();
        self::update_user_date_email_check();

        self::prepare_promo();
        self::set_root_stats();
        self::set_face_image_random_list();

        self::send_notification();

        return self::init_page();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(empty(GetRoutePageApi::$list[1]))
            return RootPageRedirectAdminWhoreRoutePageApi::init();

        self::$hash=GetRoutePageApi::$list[1];

        return self::set();

    }

}