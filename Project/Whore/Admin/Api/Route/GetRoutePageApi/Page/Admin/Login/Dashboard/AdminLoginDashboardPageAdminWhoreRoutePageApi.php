<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Admin\Login\Dashboard;

use Core\Module\Date\Date;
use Core\Module\Geo\CityLocalization;
use Core\Module\Json\Json;
use Core\Module\Lang\LangConfig;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Sort\Sort;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserWalletDonate;
use Project\Whore\Admin\Action\User\Data\UserPageDataAction;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Admin\AdminRootPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Face\Faceset;
use Project\Whore\All\Module\Source\Source;

class AdminLoginDashboardPageAdminWhoreRoutePageApi{

    /** @var int */
    private static $transaction                     =0;

    /** @var int */
    private static $transaction_month               =0;

    /** @var int */
    private static $faceset_len                     =0;

    /** @var int */
    private static $face_count                      =0;

    /** @var int */
    private static $face_month_count                =0;

    /** @var int */
    private static $profile_count                   =0;

    /** @var int */
    private static $profile_month_count             =0;

    /** @var int */
    private static $face_image_count                =0;

    /** @var int */
    private static $facekit_image_count             =0;

    /** @var int */
    private static $face_image_month_count          =0;

    /** @var int */
    private static $facekit_image_month_count       =0;

    /** @var array */
    private static $user_reg_stats_list             =[];

    /** @var int */
    private static $user_reg_stats_day_len          =30;

    /** @var array */
    private static $source_list                     =[];

    /** @var array */
    private static $parsing_list                    =[];

    /** @var array */
    private static $city_list                       =[];

    /** @var int */
    private static $face_image_last_date_update;

    /** @var int */
    private static $city_len                        =40;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_city_list(){

        $r=FaceData::get_face_data_city_len_list(self::$city_len);

        if(count($r)==0)
            return true;

        $city_name_list=CityLocalization::get_city_ID_list_from_city_ID_list(array_keys($r),LangConfig::$lang_ID_default);

        foreach($r as $city_ID=>$len)
            if(isset($city_name_list[$city_ID]))
                self::$city_list[]=[
                    'name'      =>$city_name_list[$city_ID],
                    'len'       =>$len
                ];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_source_list(){

        $r=Source::get_source_list();

        if(count($r)==0)
            return true;

        foreach($r as $row)
            self::$source_list['s'.$row['ID']]=$row['name'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_parsing_stats_list(){

        $source_day_len         =FaceData::get_parsing_date_stats();
        $source_all_len         =FaceData::get_parsing_all_stats();
        $source_hour_len        =FaceData::get_parsing_date_stats_in_one_hour();

        foreach($source_day_len as $source_ID=>$len)
            self::$parsing_list['s'.$source_ID]=[
                'all_len'       =>$source_all_len[$source_ID],
                'day_len'       =>isset($source_day_len[$source_ID])?$source_day_len[$source_ID]:0,
                'hour_len'      =>isset($source_hour_len[$source_ID])?$source_hour_len[$source_ID]:0
            ];

        foreach($source_all_len as $source_ID=>$len)
            if(!isset(self::$parsing_list[$source_ID]))
                self::$parsing_list['s'.$source_ID]=[
                    'all_len'       =>$source_all_len[$source_ID],
                    'day_len'       =>isset($source_day_len[$source_ID])?$source_day_len[$source_ID]:0,
                    'hour_len'      =>isset($source_hour_len[$source_ID])?$source_hour_len[$source_ID]:0
                ];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_reg_stats_list(){

        $user_reg_list          =User::get_user_reg_count(self::$user_reg_stats_day_len);
        $user_reg_email_list    =User::get_user_reg_count_confirmed_email(self::$user_reg_stats_day_len);
        $user_paid_list         =UserWalletDonate::get_user_reg_count_paid(self::$user_reg_stats_day_len);
        $face_data_list         =FaceData::get_user_reg_count_face_data_list(self::$user_reg_stats_day_len);

        for($index=0;$index<self::$user_reg_stats_day_len;$index++){

            $date=Date::get_date_for_db(Date::get_timestamp()-$index*24*3600+3*3600);

            self::$user_reg_stats_list[$date]=[
                'all'                   =>isset($user_reg_list[$date])?$user_reg_list[$date]:0,
                'confirmed_email'       =>isset($user_reg_email_list[$date])?$user_reg_email_list[$date]:0,
                'paid'                  =>isset($user_paid_list[$date]['profit'])?$user_paid_list[$date]['profit']:0,
                'paid_len'              =>isset($user_paid_list[$date]['len'])?$user_paid_list[$date]['len']:0,
                'paid_to_day'           =>UserWalletDonate::get_user_reg_count_paid_to_date($date),
                'face_len'              =>isset($face_data_list[$date])?$face_data_list[$date]:0
            ];

        }

        krsort(self::$user_reg_stats_list);

        return true;

    }

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_profile_len(){

        self::$profile_count=User::get_user_email_checked_all_len('profile');

    }

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_profile_30_days_len(){

        self::$profile_month_count=User::get_user_email_checked_all_30_days_len('profile');

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_faceset_len(){

        self::$faceset_len=Faceset::get_faceset_len();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_count(){

        self::$face_image_count=FaceImage::get_global_face_image_len();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_month_count(){

        self::$face_image_month_count=FaceImage::get_30_days_face_image_len();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_plus_plus_image_count(){

        self::$facekit_image_count=FaceImage::get_global_face_plus_plus_image_len();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_plus_plus_image_month_count(){

        self::$facekit_image_month_count=FaceImage::get_30_days_face_plus_plus_image_len();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_count(){

        self::$face_count=Face::get_face_count();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_month_count(){

        self::$face_month_count=Face::get_face_30_days_count();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_last_date_update(){

        self::$face_image_last_date_update=FaceData::get_last_date_update();
//        self::$face_image_last_date_update=FaceImage::get_last_date_update();

        return true;

    }

    /**
     * @return string
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_transaction_sum(){

        self::$transaction=UserWalletDonate::get_checked_sum();

        return true;

    }

    /**
     * @return string
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_transaction_sum_last_month(){

        self::$transaction_month=UserWalletDonate::get_checked_sum_last_month();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=[
            'user'          =>UserPageDataAction::get_data(),
            'action'        =>'admin_dashboard',
            'dashboard'     =>[
                'transaction'                       =>self::$transaction,
                'transaction_last_month'            =>self::$transaction_month,
                'source_list'                       =>self::$source_list,
                'user_stats'                        =>[
                    'reg'       =>self::$user_reg_stats_list,
                    'parsing'   =>self::$parsing_list,
                    'city'      =>self::$city_list
                ],
                'day_count'                         =>Date::get_last_day_in_month(),
                'faceset_count'                     =>self::$faceset_len,
                'face_image_last_date_update'       =>self::$face_image_last_date_update,
                'face_count'                        =>[
                    'all'       =>self::$face_count,
                    'month'     =>self::$face_month_count
                ],
                'profile_count'                     =>[
                    'all'       =>self::$profile_count,
                    'month'     =>self::$profile_month_count
                ],
                'face_image_count'                  =>[
                    'all'       =>self::$face_image_count,
                    'month'     =>self::$face_image_month_count
                ],
                'facekit_image_count'               =>[
                    'all'       =>self::$facekit_image_count,
                    'month'     =>self::$facekit_image_month_count
                ]
            ]
        ];

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('dashboard_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('admin'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    trace(data);
                    page_object.action.admin.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(!User::is_login())
            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        if(
              UserAccess::$is_root
            ||UserAccess::$is_admin
        ){

            self::set_city_list();

            self::set_source_list();
            self::set_parsing_stats_list();

            self::set_user_reg_stats_list();

            self::set_transaction_sum();
            self::set_transaction_sum_last_month();

            self::set_faceset_len();

            self::set_face_count();
            self::set_face_month_count();

            self::set_face_image_count();
            self::set_face_image_month_count();

            self::set_profile_len();
            self::set_profile_30_days_len();

//            self::set_facekit_image_count();
//            self::set_facekit_image_month_count();

            self::set_face_plus_plus_image_count();
            self::set_face_plus_plus_image_month_count();

            self::set_face_image_last_date_update();

            return self::init_page();

        }

        return AdminRootPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}