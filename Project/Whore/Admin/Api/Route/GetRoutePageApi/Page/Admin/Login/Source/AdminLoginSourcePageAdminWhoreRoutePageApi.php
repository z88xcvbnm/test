<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Admin\Login\Source;

use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Project\Whore\Admin\Action\User\Data\UserPageDataAction;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Admin\AdminRootPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;

class AdminLoginSourcePageAdminWhoreRoutePageApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=[
            'user'              =>UserPageDataAction::get_data(),
            'action'            =>'admin_source',
            'financial_data'    =>[]
        ];

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('source_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('admin'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    trace(data);
                    page_object.action.admin.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(!User::is_login())
            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        if(UserAccess::$is_root||UserAccess::$is_admin){

            return self::init_page();

        }

        return AdminRootPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}