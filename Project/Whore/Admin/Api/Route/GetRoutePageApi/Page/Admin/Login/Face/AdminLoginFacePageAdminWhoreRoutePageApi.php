<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Admin\Login\Face;

use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Project\Whore\Admin\Action\Face\GetFaceListAction;
use Project\Whore\Admin\Action\User\Data\UserPageDataAction;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Admin\AdminRootPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceDataType;
use Project\Whore\All\Module\Source\Source;

class AdminLoginFacePageAdminWhoreRoutePageApi{

    /** @var int */
    private static $face_ID;

    /** @var int */
    private static $model_zone_ID;

    /** @var int */
    private static $start                   =0;

    /** @var int */
//    private static $len             =1500;
    private static $len                     =30;

    /** @var array */
    private static $source_list             =[];

    /** @var array */
    private static $face_data_type_list     =[];

    /** @var array */
    private static $face_list               =[];

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_ID(){

        if(!empty(self::$model_zone_ID)){

            self::$face_ID=FaceData::get_face_ID_from_model_zone_ID(self::$model_zone_ID);

//            echo 'face_ID: '.self::$face_ID."\n";
//
//            echo Db::$query_last."\n";
//            print_r(Db::$value_last);

            if(!empty(self::$face_ID))
                self::$face_ID++;

//            echo 'face_ID: '.self::$face_ID."\n";

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_source_list(){

        self::$source_list=Source::get_source_list();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_data_type_list(){

        self::$face_data_type_list=FaceDataType::get_face_data_type_list();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_list(){

        $r=GetFaceListAction::init(self::$face_ID,self::$start,self::$len);

        self::$face_list=$r['face_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=[
            'user'              =>UserPageDataAction::get_data(),
            'action'            =>'admin_face',
            'face_data'         =>[
                'face_list'             =>array_values(self::$face_list),
                'source_list'           =>self::$source_list,
                'face_data_type_list'   =>self::$face_data_type_list
            ]
        ];

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('face_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('admin'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    trace(data);
                    page_object.action.admin.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(!User::is_login())
            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        if(
              UserAccess::$is_root
            ||UserAccess::$is_admin
        ){

            self::set_face_ID();
            self::set_face_list();
            self::set_source_list();
            self::set_face_data_type_list();

            return self::init_page();

        }

        return AdminRootPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(!empty($_POST['start']))
            self::$start=(int)$_POST['start'];

        if(!empty($_POST['len']))
            self::$len=(int)$_POST['len'];

        if(!empty($_POST['face_ID']))
            self::$face_ID=(int)$_POST['face_ID']+1;

        if(!empty($_POST['model_zone_ID']))
            self::$model_zone_ID=(int)$_POST['model_zone_ID'];

        return self::set();

    }

}