<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Root\Term;

use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Face\GetFaceImageRandomListAction;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;

class RootTermPageAdminWhoreRoutePageApi{

    /** @var string */
    private static $term                        ='';

    /** @var array */
    private static $face_list                   =[];

    /** @var array */
    private static $face_image_random_list      =[];

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_random_list(){

        $r=GetFaceImageRandomListAction::init();

        if(count($r)==0)
            return false;

        self::$face_image_random_list   =$r['face_image_list'];
        self::$face_list                =$r['face_list'];

        return true;

    }

    /**
     * @return bool
     */
    private static function set_term_content(){

        $list       =[];
        $list[]     ='<h1>Термины и определения в эскортном бизнесе</h1>';
        $list[]     ='<b>Эскорт</b> — услуга сопровождения клиента на встречах, деловых поездках и на отдыхе. В подавляющем большинстве случаев эскорт-услуги являются скрытой проституцией. Предоставление интим-услуг оговаривается сторонами явно и заранее.';
        $list[]     ='<b>Тема</b> - это единица измерения эскорт-услуг. По сути, это поездка к клиенту: на вечеринку, на дачу или в отель. Тема может длиться от нескольких часов до нескольких дней. Перед темой обязательно инструктируют, то есть рассказывают, кто будет на этой тусовке, как себя вести, что можно говорить, а что нельзя, с кем нужно спать.';
        $list[]     ='<b>Тур</b> - это поездка за границу на несколько дней, иногда недель, когда девочка каждый день работает с разными клиентами, о встречах с которыми ее менеджер договорился заранее. Клиентов может быть от нескольких за поездку, до нескольких за день. Зачастую туры связаны с групповым сексом. Наиболее популярные направления туров - ОАЭ, Турция, Италия, Лондон, Париж.';
        $list[]     ='<b>Содержание</b> - форма эскорта, при которой девочка приоритетно работает на одного клиента, который платит ей заранее оговоренную сумму вознаграждения ежемесячно или еженедельно. Конечно, содержание не исключает поездок на темы полностью, но по первому звонку клиента девушка должна приехать к нему.';
        $list[]     ='<b>Шлюхомиграция</b> - это последовательный переезд девушки с пониженнной социальной ответственностью во все более крупные города, продиктованный двумя основными факторами: испорченной репутацией в текущем городе проживания и погоней за более высокими гонорарами. Конечным пунктом миграции почти всех проституток является Москва, где высокий спрос на мохнатое золото, продиктованный высокой концентрацией капитала и есть возможность раствориться в большой массе людей.';
        $list[]     ='<b>Псевдонимы</b> - это выдуманные имена, которые эскортницы могут применять в своей работе. Чем ниже уровень проститутки и ее клиентов, тем вероятнее она будет использовать псевдоним. Дорогие эскортницы чаще всего работают под настоящими именами, т.к. их работа связана с поездками, где необходима покупка и оформление билетов, а так же они часто проходят проверки служб безопасности состоятельных клиентов. Псевдонимы они могут брать только в заграничных турах в страны, где русские женские имена звучат сложно или неблагозвучно.';
        $list[]     ='<b>Рабочая</b> - есть девушки которые занимаются эскортом изредка, предоставляя услуги ограниченному кругу симпатизирующих им состоятельных клиентов, а есть девушки, которые вписываются во все предлагаемые темы подряд, не уточняя подробностей и не брезгуя никем. Вот последние и есть рабочие лошади.';
        $list[]     ='<b>Эстафетная содержанка</b> - это девушка нестойких моральных принципов, которая последовательно и регулярно меняет содержателей на все более богатых и влиятельных. Конечная цель такой особы, дойдя до некоторого топ-уровня, попытаться увести содержателя из семьи и женить на себе. Говорят, такому их учат на специальных дорогих тренингах.';
        $list[]     ='<b>Питательный хуй</b> - это перспективный, состоятельный клиент эскортницы, к которому она хотела бы попасть на содержание. Ежемесячная абонентка от одного мужчины - более безопасный и стабильный способ существования. Однако, даже находясь на содержании, большинство эскортниц продолжает тайно ездить на темы к старым клиентам.';
        $list[]     ='<b>Пробег</b> - это оценочное количество половых партнеров эскортницы (меряется в сотнях членов), который сильно влияет на стоимость шкуры на рынке. Сами девушки пытаются всеми правдами и неправдами уменьшать это количество, но, как правило, все в тусовке знают примерный пробег каждой эскортницы.';
        $list[]     ='<b>Нолик</b> - девственница на языке эскорт-менеджеров. Девушка с нулевым пробегом в хуях. Считается эксклюзивом и стоит заметно дороже подраздолбанных. Но, зачастую, это не так. Как правило за "ноликов" ушлые скауты выдают молодых провинциальных baby-face студенток. Известны случаи двойной, тройной продажи ноликов. Некоторые 16-17-ти летние писюхи умудряются продать свою девственность по 6 раз.';

        self::$term='<p>'.implode('</p><p>',$list).'</p>';

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=array(
            'action'        =>'root_term',
            'data'          =>[
                'face_list'                 =>self::$face_list,
                'face_image_random_list'    =>self::$face_image_random_list,
                'term'                      =>self::$term
            ]
        );

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('root_term_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('root'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    page_object.action.root.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_face_image_random_list();
        self::set_term_content();

        return self::init_page();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}