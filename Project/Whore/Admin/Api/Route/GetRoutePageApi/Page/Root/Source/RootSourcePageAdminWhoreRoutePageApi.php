<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Root\Source;

use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Face\GetFaceImageRandomListAction;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;

class RootSourcePageAdminWhoreRoutePageApi{

    /** @var string */
    private static $source                       ='';

    /** @var array */
    private static $face_list                   =[];

    /** @var array */
    private static $face_image_random_list      =[];

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_random_list(){

        $r=GetFaceImageRandomListAction::init();

        if(count($r)==0)
            return false;

        self::$face_image_random_list   =$r['face_image_list'];
        self::$face_list                =$r['face_list'];

        return true;

    }

    /**
     * @return bool
     */
    private static function set_source_content(){

        $list       =[];
        $list[]     ='<h1>Источники анкет эскортниц, проституток и содержанок</h1>';
        $list[]     ='<b>Model Zone</b> - сайт, рассылка, канал и бот в мессенджере Telegram, которые агрегируют анкеты девушек, предоставляющих эскорт-услуги на территории стран СНГ. Иногда на одну и туже девушку может выдаваться несколько анкет из этого источника. Это означает, что девушка на время выходила из бизнеса (например, имела постоянные отношения), а потом вернулась в бизнес, создав новую анкету.';
        $list[]     ='<b>Рынок Шкур (РШ)</b> - канал в мессенджере Telegram, разоблачающий Instagram-звезд, различных блогерок, медийных персон, участниц реалити-шоу, спортсменок, певичек и успешных моделей, которые не брезгуют эскортом. Анкеты большинства из них вы и так можете найти в нашей базе, но бывают и эксклюзивы.';
        $list[]     ='<b>Атолин.ру</b> – сайт позиционирует себя как ресурс для конфиденциальных знакомств, помогающий состоятельным женатым мужчинам найти себе содержанку для долговременных возмездных отношений или девушку на одну ночь. По сути же - это просто каталог проституток-индивидуалок с рубрикацией по городам.';
        $list[]     ='<b>Содержанка.рф</b> - сайт с анкетами женщин, которые жаждут стать твоими любовницами, если ты успешный мужчина. Зачастую прямо в анкетах соискательниц на почетное звание содержанки стоит ценник, который начинается от 100К/мес. Короче, этот ресурс - еще одна добровольная перепись проституток России.';
        $list[]     ='<b>Dosug.ru (dosug.cz, dosug.nu)</b> - один из старейших и самых известных сайтов, рекламирующих интимные услуги в России. В основном содержит анкеты дорогих московских проституток, т.к. разместить информацию на нем стоит не дешего. В настоящий момент сайт заблокирован по решению Роскомнадзора, но это не сильно затрудняет доступ к нему.';
        $list[]     ='<b>Whores777.com</b> - самый большой и самый полный каталог проституток Москвы всех ценовых категорий, содержащий фотографии, параметры и контактные данные девушек легкого поведения, а так же полное описание предоставляемых ими услуг.';
        $list[]     ='<b>EscortNews.NL</b> - крупный агрегатор предложений от эскортниц и проституток в Голландии и Бельгии. Исторически так сложилось, что он содержит огромное количество предложений от наших соотечественниц, как от агентств, так и от инди.';
        $list[]     ='<b>Lovesss.ru</b> - как и другие сайты для начинающих проституток, позиционирует себя как "место, где встречаются красота и успех" и предназначен для поиска содержанок. По факту же - это очередная добровольная перепись эскортниц из России и стран СНГ.';
        $list[]     ='<b>Prostitutki SPb</b>, <b>Prostitutki Red</b>, <b>ProstitutkiPiteraGood.net</b>, <b>Gorchiza.com</b> - избыточный перечень сайтов-каталогов проституток Санкт-Петербурга, на которых представлены как индивидуалки, так и салоны-бордели';
        $list[]     ='<b>Lovelama.ru</b> - это очередное место встречи красивых девушек и состоятельных мужчин, ищущих содержанок/эскортниц. Лама - это, по сути, перуанская овца. Вот эти овцы и клюют на словосочетание "богатый мужчина", которое встречается в описании сайта 144 раза. Создают анкеты, заливают фото и встают на скользкую дорожку торговли телом.';
        $list[]     ='<b>Zolushka-project.com</b> - сайт знакомит "современных золушек" с мужчинами, желающими "отдохнуть от супружеской жизни", очевидно играя на комплексах симпатичных неудачниц, несумевших реализоваться в жизни любым другим способом, кроме торговли своим телом и временем.';
        $list[]     ='<b>Soderganki-online.ru</b> - как и десяток других сайтов выше, знакомит красивых бездельниц с неверными мужьями для промискуитета на возмездной основе, обещая первым сумки, цацки, перья и машины. Из особенностей - сайт пытается выстроить аналогии между посетительницами сайта и героинями одноименного российского сериала, обещая им образ жизни "как там" после продажи мохнатки.';

        $list[]     ='<b>Luxmodel.info</b> - сайт позиционирует себя как топовое эскорт-агентство Санкт-Петербурга, основной целевой аудиторией которого являются скучающие иностранцы, прибывшие в город на отдых или по делам (у сайта даже нет русскоязычного интерфейса). Но в целом по подаче, по почасовой оплате и доступным тарифам, перечню включенных в описании каждой девушки интимных услуг - это, скорее, обычная проститушная.';
        $list[]     ='<b>IntimCity.nl</b> - огромный онлайн каталог проституток Москвы с потертым дизайном из нулевых. Тут есть все - от пристарелых путан, которые пришли в профессию еще на волне "Интердевочки", до трансвеститов, которые сверху, по пояс, выглядят как модели "Victorias Secret", а снизу, по пояс - как Рокко Сиффреди. Наверняка многих из них вы встречаете на улице, а некоторые даже ваши соседи.';
        $list[]     ='<b>Favoritka.org</b> -  еще один сайт для поиска любовницы (содержанки) или спонсора в Москве и любом другом городе России, Украины или Беларуси. Заявляется, что все анкеты сайта проходят ручную модерацию, что гарантирует соответствие фотографий и указанных в анкете данных реальности. Причем это касается как анкет содержанок, так и анкет спонсоров. Спасибо, это то, что надо!';
        $list[]     ='<b>Paramours.ru (любовницы.ру)</b> - место, где богатые спонсоры ищут любовниц. "Этот ресурс создан удобным и понятным, чтобы каждый мужчина смог найти себе девушку содержанку или любовницу, а девушки, в свою очередь, смогли найти себе спонсора - богатого любовника. Любовницы.ру - это сайт знакомств для женатых мужчин (папиков) и красивых девушек-содержанок."';
        $list[]     ='<b>Prostitutki Kieva</b>, <b>ProstitutkiKieva.Info</b>, <b>Fei Kiev</b> - избыточный перечень сайтов-каталогов проституток Киева и других городов Украины, на страницах которых представлены как индивидуалки, так и салоны-бордели.';

        self::$source='<p>'.implode('</p><p>',$list).'</p>';

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=array(
            'action'        =>'root_source',
            'data'          =>[
                'face_list'                 =>self::$face_list,
                'face_image_random_list'    =>self::$face_image_random_list,
                'source'                    =>self::$source
            ]
        );

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('root_source_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('root'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    page_object.action.root.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_face_image_random_list();
        self::set_source_content();

        return self::init_page();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}