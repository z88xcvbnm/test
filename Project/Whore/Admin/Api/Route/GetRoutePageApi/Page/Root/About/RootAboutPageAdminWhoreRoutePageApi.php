<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Root\About;

use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Face\GetFaceImageRandomListAction;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;

class RootAboutPageAdminWhoreRoutePageApi{

    /** @var string */
    private static $about                       ='';

    /** @var array */
    private static $face_list                   =[];

    /** @var array */
    private static $face_image_random_list      =[];

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_random_list(){

        $r=GetFaceImageRandomListAction::init();

        if(count($r)==0)
            return false;

        self::$face_image_random_list   =$r['face_image_list'];
        self::$face_list                =$r['face_list'];

        return true;

    }

    /**
     * @return bool
     */
    private static function set_about_content(){

        $list       =[];

        $list[]='<h1 class="page_title">Для чего создан шлюхам.нет (шкурам.нет)</h1>';
        $list[]='<p>По нашей статистике в России, Украине, Беларуси и прочих государствах бывшего СССР примерно <b>250 тысяч</b> особей женского пола занимаются проституцией, эскортом и прочей торговлей собственным телом.</p>';
        $list[]='<p>Вам кажется, что это мало?</p>';
        $list[]='<p>Но на самом деле каждая вторая девушка, которую вы видите в клубе или ресторане вечером, вовлечена в занятие эскортом или проституцией.</p>';
        $list[]='<p style="margin-bottom: 0">Давайте посчитаем:</p>';
        $list[]='<ul>';
        $list[]='<li>женщин в возрасте от 18 до 35 лет в этих странах живет порядка 40 миллионов</li>';
        $list[]='<li>из них в крупных городах проживает примерно 25 процентов – круг сузился до 10 миллионов, далее</li>';
        $list[]='<li>половина из них условно страшненькие, уже - 5 миллионов</li>';
        $list[]='<li>10% из них регулярно ходят в рестораны и клубы, получается, что круг сужается до 500 тысяч женщин, примерно половина из них ходит в клубы и рестораны с любимыми или друзьями (просто присмотритесь).</li>';
        $list[]='</ul>';
        $list[]='<p>И что у нас остается? Именно! Примерно половина девушек, которых вы видите в этих местах занимаются эскортом или проституцией. Так может и вам настало время проверить свою избранницу?</p>';
        $list[]='<p>Скрытая проституция в виде всевозможных эскорт услуг стала настолько массовым явлением, что перестала существовать отдельно от повседневной жизни обычных людей. Именно поэтому мы и сделали для вас этот сервис.</p>';

        self::$about=implode('',$list);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=array(
            'action'        =>'root_about',
            'data'          =>[
                'face_list'                 =>self::$face_list,
                'face_image_random_list'    =>self::$face_image_random_list,
                'about'                     =>self::$about
            ]
        );

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('root_about_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('root'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    page_object.action.root.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_face_image_random_list();
        self::set_about_content();

        return self::init_page();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}