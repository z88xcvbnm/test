<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Invite;

use Core\Action\Logout\UserLogout\UserLogoutSystemAction;
use Core\Module\Error\ErrorCashContent;
use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Core\Module\User\UserHash;
use Core\Module\User\UserHashTypeConfig;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Action\Face\GetFaceImageRandomListAction;
use Project\Whore\Admin\Action\Face\GetFaceStatsForRootAction;
use Project\Whore\Admin\Api\Route\GetRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Admin\AdminRootPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;

class InvitePageAdminWhoreRoutePageApi{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_hash_type_ID;

    /** @var string */
    private static $email;

    /** @var string */
    private static $login;

    /** @var string */
    private static $name;

    /** @var string */
    private static $surname;

    /** @var string */
    private static $hash;

    /** @var int */
    private static $face_len                    =0;

    /** @var int */
    private static $face_image_indexed_len      =0;

    /** @var array */
    private static $face_list                   =[];

    /** @var array */
    private static $face_image_random_list      =[];

    /** @var array */
    private static $city_list                   =[];

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_hash(){

        $data=UserHash::get_user_hash_data_from_hash(self::$hash,self::$user_hash_type_ID);

        if(empty($data)){

            ErrorCashContent::add_error('hash','Hash is not valid');

            return false;

        }

        self::$user_ID=$data['user_ID'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_random_list(){

        $r=GetFaceImageRandomListAction::init();

        if(count($r)==0)
            return false;

        self::$face_image_random_list   =$r['face_image_list'];
        self::$face_list                =$r['face_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_root_stats(){

        $r=GetFaceStatsForRootAction::init();

        self::$face_len                     =$r['face_len'];
        self::$city_list                    =$r['city_list'];
        self::$face_image_indexed_len       =$r['face_image_indexed_len'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_hash_type_ID(){

        self::$user_hash_type_ID=UserHashTypeConfig::get_user_hash_type_ID('registration_invite');

        if(empty(self::$user_hash_type_ID)){

            ErrorCashContent::add_error('hash','Hash is not valid');

            return false;

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_login(){

        self::$login=UserLogin::get_user_login(self::$user_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_email(){

        self::$email=UserEmail::get_user_email_last(self::$user_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_name(){

        $data=UserData::get_user_data(self::$user_ID);

        if(empty($data)){

            ErrorCashContent::add_error('user_data','User data is empty');

            return false;

        }

        self::$name     =$data['name'];
        self::$surname  =$data['surname'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=array(
            'action'                    =>'invite',
            'user_ID'                   =>self::$user_ID,
            'login'                     =>self::$login,
            'email'                     =>self::$email,
            'name'                      =>self::$name,
            'surname'                   =>self::$surname,
            'hash'                      =>self::$hash,
            'data'                      =>[
                'face_list'                 =>self::$face_list,
                'face_image_random_list'    =>self::$face_image_random_list,
                'city_list'                 =>self::$city_list,
                'face_len'                  =>self::$face_len,
                'face_image_indexed_len'    =>self::$face_image_indexed_len
            ]
        );

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('invite_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('invite'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    trace(data);
                    page_object.action.invite.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @param string|NULL $hash
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_hash(string $hash=NULL){

        if(empty($hash))
            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        self::$hash=$hash;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(User::is_login())
            UserLogoutSystemAction::init();

        self::set_hash(GetRoutePageApi::$list[1]);
        self::set_user_hash_type_ID();

        if(self::isset_user_hash()){

            self::set_root_stats();
            self::set_face_image_random_list();

            self::set_user_name();
            self::set_user_email();
            self::set_user_login();

            return self::init_page();

        }

        return AdminRootPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}