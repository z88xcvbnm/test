<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\RecoveryPassword;

use Core\Module\Error\ErrorCashContent;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\UserHash;
use Core\Module\User\UserHashTypeConfig;
use Project\Whore\Admin\Action\Face\GetFaceImageRandomListAction;
use Project\Whore\Admin\Action\Face\GetFaceStatsForRootAction;
use Project\Whore\Admin\Api\Route\GetRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;

class RecoveryPasswordPageAdminWhoreRoutePageApi{

    /** @var int */
    private static $face_len                    =0;

    /** @var int */
    private static $face_image_indexed_len      =0;

    /** @var array */
    private static $face_list                   =[];

    /** @var array */
    private static $face_image_random_list      =[];

    /** @var array */
    private static $city_list                   =[];

    /** @var int */
    private static $user_hash_type_ID;

    /** @var string */
    private static $hash;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_hash(){

        if(!UserHash::isset_user_hash(NULL,self::$user_hash_type_ID,NULL,self::$hash)){

            ErrorCashContent::add_error('user_hash','User hash is not exists');

            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_hash_type(){

        self::$user_hash_type_ID=UserHashTypeConfig::get_user_hash_type_ID('recovery_password');

        if(empty(self::$user_hash_type_ID)){

            ErrorCashContent::add_error('user_hash_type','User hash type is not exists');

            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_random_list(){

        $r=GetFaceImageRandomListAction::init();

        if(count($r)==0)
            return false;

        self::$face_image_random_list   =$r['face_image_list'];
        self::$face_list                =$r['face_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_root_stats(){

        $r=GetFaceStatsForRootAction::init();

        self::$face_len                     =$r['face_len'];
        self::$face_image_indexed_len       =$r['face_image_indexed_len'];
        self::$city_list                    =$r['city_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=array(
            'action'        =>'forgot',
            'data'          =>[
                'face_list'                 =>self::$face_list,
                'face_image_random_list'    =>self::$face_image_random_list,
                'city_list'                 =>self::$city_list,
                'face_len'                  =>self::$face_len,
                'face_image_indexed_len'    =>self::$face_image_indexed_len
            ]
        );

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('recovery_password_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('auth'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    page_object.action.auth.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_user_hash_type())
            if(self::isset_user_hash()){

                self::set_root_stats();
                self::set_face_image_random_list();

                return self::init_page();

            }

        return AuthPageRedirectAdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        if(empty(GetRoutePageApi::$list[1])){

            ErrorCashContent::add_error('hash','Hash is empty');

            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        }

        self::$hash=GetRoutePageApi::$list[1];

        return self::set();

    }

}