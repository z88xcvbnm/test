<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Profile\Login\Settings;

use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Project\Whore\Admin\Action\Face\GetFaceImageRandomListAction;
use Project\Whore\Admin\Action\User\Data\UserPageDataAction;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;

class ProfileLoginSettingsPageAdminWhoreRoutePageApi{

    /** @var string */
    private static $name;

    /** @var string */
    private static $surname;

    /** @var string */
    private static $email;

    /** @var array */
    private static $face_list                   =[];

    /** @var array */
    private static $face_image_random_list      =[];

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_random_list(){

        $r=GetFaceImageRandomListAction::init();

        if(count($r)==0)
            return false;

        self::$face_image_random_list   =$r['face_image_list'];
        self::$face_list                =$r['face_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_name(){

        $data           =UserData::get_user_data(User::$user_ID);

        self::$name     =empty($data['name'])?'':$data['name'];
        self::$surname  =empty($data['surname'])?'':$data['surname'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_email(){

        self::$email=UserEmail::get_user_email_last(User::$user_ID);

        if(empty(self::$email))
            self::$email='';

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=[
            'user'      =>UserPageDataAction::get_data(),
            'action'    =>'profile_settings',
            'settings'  =>[
                'name'      =>self::$name,
                'surname'   =>self::$surname,
                'email'     =>self::$email
            ],
            'data'      =>[
                'face_list'                 =>self::$face_list,
                'face_image_random_list'    =>self::$face_image_random_list
            ]
        ];

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('settings_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('profile'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    trace(data);
                    page_object.action.profile.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(!User::is_login())
            return AuthPageRedirectAdminWhoreRoutePageApi::init();

        self::set_face_image_random_list();
        self::set_user_name();
        self::set_user_email();


        return self::init_page();

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}