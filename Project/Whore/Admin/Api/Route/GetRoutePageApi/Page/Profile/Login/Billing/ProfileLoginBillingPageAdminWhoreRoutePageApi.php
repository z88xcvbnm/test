<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Profile\Login\Billing;

use Core\Action\Crypto\CryptoCompareCurrencyAction;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\User\UserBalanceTransaction;
use Core\Module\Wallet\WalletName;
use Project\Whore\Admin\Action\Face\GetFaceImageRandomListAction;
use Project\Whore\Admin\Action\User\Data\UserPageDataAction;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;
use Project\Whore\All\Module\Wallet\WalletConfig;

class ProfileLoginBillingPageAdminWhoreRoutePageApi{

    /** @var array */
    private static $face_list                   =[];

    /** @var array */
    private static $face_image_random_list      =[];

    /** @var array */
    private static $wallet_name_list            =[];

    /** @var array */
    private static $billing_list                =[];

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_wallet_list(){

        self::$wallet_name_list=WalletName::get_wallet_name_list(false);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_billing_list(){

        $r=UserBalanceTransaction::get_user_balance_transaction_list(User::$user_ID);

        if(count($r)==0)
            return true;

        foreach($r as $row)
            switch($row['action']){

                case'add_hand_money':
                case'auto_payment':
                case'remove_hand_money':
                case'confirm_email_money':
                case'promo_code':
                case'promo_login_use':
                case'promo_login_invite':
                case'find_face':{

                    $action_type    =NULL;
                    $source_action  =NULL;

                    switch($row['action']){

                        case'promo_login_use':{

                            $action_type='promo_login_use';

                            break;

                        }

                        case'promo_login_invite':{

                            $action_type='promo_login_invite';

                            break;

                        }

                        case'promo_code':{

                            $action_type='promo_code';

                            break;

                        }

                        case'auto_payment':
                        case'add_hand_money':
                        case'confirm_email_money':{

                            $action_type='add';

                            break;

                        }

                        case'remove_hand_money':
                        case'find_face':{

                            $action_type='remove';

                            break;

                        }

                    }

                    switch($row['action']){

                        case'add_hand_money':{

                            $source_action='hand';

                            break;

                        }

                        case'auto_payment':
                        case'confirm_email_money':{

                            $source_action=empty(self::$wallet_name_list[$row['wallet_name_ID']])?NULL:self::$wallet_name_list[$row['wallet_name_ID']];

                            if(!empty($source_action))
                                switch(mb_strtolower($source_action,'utf-8')){

                                    case'pp':{

                                        $source_action='PayPal';

                                        break;

                                    }

                                    case'qiwi':{

                                        $source_action='QIWI';

                                        break;

                                    }

                                    case'btc':{

                                        $source_action='Bitcoin';

                                        break;

                                    }

                                    case'eth':{

                                        $source_action='Ethereum';

                                        break;

                                    }

                                }

                            break;

                        }

                    }

                    self::$billing_list[]=[
                        'ID'                        =>$row['ID'],
                        'wallet_name'               =>$source_action,
                        'action'                    =>$action_type,
                        'transaction_sum'           =>$row['transaction_sum'],
                        'transaction_wallet_sum'    =>$row['transaction_wallet_sum'],
                        'balance'                   =>is_null($row['balance'])?NULL:$row['balance'],
                        'user_wallet_address'       =>$row['user_wallet_address'],
                        'target_wallet_address'     =>$row['target_wallet_address'],
                        'promo_code'                =>$row['promo_code'],
                        'date_create'               =>$row['date_create']
                    ];

                    break;

                }

            }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_random_list(){

        $r=GetFaceImageRandomListAction::init();

        if(count($r)==0)
            return false;

        self::$face_image_random_list   =$r['face_image_list'];
        self::$face_list                =$r['face_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=array(
            'user'          =>UserPageDataAction::get_data(),
            'action'        =>'profile_billing',
            'data'          =>[
                'billing_list'              =>self::$billing_list,
                'wallet_list'               =>self::$wallet_name_list,
                'face_list'                 =>self::$face_list,
                'face_image_random_list'    =>self::$face_image_random_list
            ]
        );

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('profile_billing_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('profile'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    page_object.action.profile.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_face_image_random_list();
        self::set_wallet_list();
        self::set_billing_list();

        return self::init_page();

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}