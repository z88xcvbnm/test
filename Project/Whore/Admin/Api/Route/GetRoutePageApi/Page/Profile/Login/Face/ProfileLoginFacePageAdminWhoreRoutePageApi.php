<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Profile\Login\Face;

use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Face\GetFaceImageRandomListAction;
use Project\Whore\Admin\Action\Face\GetFaceStatsForRootAction;
use Project\Whore\Admin\Action\User\Data\UserPageDataAction;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;
use Project\Whore\All\Module\Price\PriceConfig;

class ProfileLoginFacePageAdminWhoreRoutePageApi{

    /** @var int */
    private static $face_len                    =0;

    /** @var int */
    private static $face_image_indexed_len      =0;

    /** @var array */
    private static $face_list                   =[];

    /** @var array */
    private static $face_image_random_list      =[];

    /** @var array */
    private static $city_list                   =[];

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_root_stats(){

        $r=GetFaceStatsForRootAction::init();

        self::$face_len                     =$r['face_len'];
        self::$face_image_indexed_len       =$r['face_image_indexed_len'];
        self::$city_list                    =$r['city_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_random_list(){

        $r=GetFaceImageRandomListAction::init();

        if(count($r)==0)
            return false;

        self::$face_image_random_list   =$r['face_image_list'];
        self::$face_list                =$r['face_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=array(
            'user'          =>UserPageDataAction::get_data(),
            'action'        =>'profile_face',
            'data'          =>[
                'cost'                      =>[
                    'black_friday_date_limit'   =>PriceConfig::get_black_friday_date_limit(),
                    'face_1'                    =>PriceConfig::get_price(1),
                    'face_2'                    =>PriceConfig::get_price(2),
                    'face_3'                    =>PriceConfig::get_price(3),
                ],
                'face_len'                  =>self::$face_len,
                'face_image_indexed_len'    =>self::$face_image_indexed_len,
                'city_list'                 =>self::$city_list,
                'face_list'                 =>self::$face_list,
                'face_image_random_list'    =>self::$face_image_random_list
            ]
        );

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('profile_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('profile'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    page_object.action.profile.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_root_stats();
        self::set_face_image_random_list();

        return self::init_page();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}