<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Profile\Login\Payment;

use Core\Action\Crypto\CryptoCompareCurrencyAction;
use Core\Module\Date\Date;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Face\GetFaceImageRandomListAction;
use Project\Whore\Admin\Action\User\Data\UserPageDataAction;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;
use Project\Whore\All\Module\Wallet\WalletConfig;

class ProfileLoginPaymentPageAdminWhoreRoutePageApi{

    /** @var array */
    private static $face_list                   =[];

    /** @var array */
    private static $face_image_random_list      =[];

    /** @var array */
    private static $currency_list               =[];

    /** @var array */
    private static $wallet_list                 =[];

    /** @var string */
    private static $currency_from               ='RUB';

    /** @var array */
    private static $currency_to_list            =['BTC','ETH'];

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_currency_list(){

        $r=CryptoCompareCurrencyAction::init(self::$currency_from,self::$currency_to_list);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Crypto currency have error'
            ];

            throw new PhpException($error);

        }

        self::$currency_list=$r['currency_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_wallet_list(){

        self::$wallet_list=WalletConfig::get_wallet_list();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_image_random_list(){

        $r=GetFaceImageRandomListAction::init();

        if(count($r)==0)
            return false;

        self::$face_image_random_list   =$r['face_image_list'];
        self::$face_list                =$r['face_list'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function init_page(){

        $data=array(
            'user'          =>UserPageDataAction::get_data(),
            'action'        =>'profile_payment',
            'data'          =>[
                'timestamp'                 =>Date::get_timestamp(),
                'currency_list'             =>self::$currency_list,
                'wallet_list'               =>self::$wallet_list,
                'face_list'                 =>self::$face_list,
                'face_image_random_list'    =>self::$face_image_random_list
            ]
        );

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('profile_payment_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('profile'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    page_object.action.profile.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_wallet_list();
        self::set_currency_list();
        self::set_face_image_random_list();

        return self::init_page();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}