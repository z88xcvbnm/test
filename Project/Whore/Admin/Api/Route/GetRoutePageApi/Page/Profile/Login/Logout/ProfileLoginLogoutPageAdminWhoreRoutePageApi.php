<?php

namespace Project\Whore\Admin\Api\Route\GetRoutePageApi\Page\Profile\Login\Logout;

use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Session\Session;
use Core\Module\Token\Token;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Action\User\Admin\LogoutUserAdminAction;
use Project\Whore\Admin\Config\JsConfigAdminWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;

class ProfileLoginLogoutPageAdminWhoreRoutePageApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function reset_data(){

        User::reset_data();
        UserAccess::reset_data();
        UserLogin::reset_data();
        Token::reset_data();
        Session::reset_data();

        return true;

    }

    /**
     * @return bool
     */
    private static function logout(){

        return LogoutUserAdminAction::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_token(){

        return Token::add_token_default();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function add_session(){

        return Session::add_session_default();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function redirect(){

        $data=array(
            'action'=>'profile_logout'
        );

        $action_data=array(
            'title'                 =>ContentAdminWhore::get_page_title('logout_page_title'),
            'script_list'           =>JsConfigAdminWhore::get_script_path('profile'),
            'init_list'             =>array(
                '
                    var data='.Json::encode($data).';
                    page_object.action.profile.init(data);
                '
            )
        );

        return ResponseSuccess::init($action_data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::logout();
        self::reset_data();

        self::add_token();
        self::add_session();

        return self::redirect();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}