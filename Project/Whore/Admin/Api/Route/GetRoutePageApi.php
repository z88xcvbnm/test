<?php

namespace Project\Whore\Admin\Api\Route;

use Core\Module\Response\ResponseSuccess;
use Core\Module\Response\ResponseUnprocessableEntity;
use Core\Module\Url\Url;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Prepare\PrepareLinkLen0AdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Prepare\PrepareLinkLen1AdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Prepare\PrepareLinkLen2AdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Prepare\PrepareLinkLen3AdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Prepare\PrepareLinkLen4AdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Prepare\PrepareLinkLen5AdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Prepare\PrepareLinkLen6AdminWhoreRoutePageApi;
use Project\Whore\Admin\Api\Route\GetRoutePageApi\Redirect\Auth\AuthPageRedirectAdminWhoreRoutePageApi;

class GetRoutePageApi{

    /** @var array */
    public  static $list=[];

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_url_list_0(){

        return PrepareLinkLen0AdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_url_list_1(){

        return PrepareLinkLen1AdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_url_list_2(){

        return PrepareLinkLen2AdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_url_list_3(){

        return PrepareLinkLen3AdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_url_list_4(){

        return PrepareLinkLen4AdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_url_list_5(){

        return PrepareLinkLen5AdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_url_list_6(){

        return PrepareLinkLen6AdminWhoreRoutePageApi::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_redirect(){

        switch(count(self::$list)){

            case 0:
                return self::prepare_url_list_0();

            case 1:
                return self::prepare_url_list_1();

            case 2:
                return self::prepare_url_list_2();

            case 3:
                return self::prepare_url_list_3();

            case 4:
                return self::prepare_url_list_4();

            case 5:
                return self::prepare_url_list_5();

            case 6:
                return self::prepare_url_list_6();

            default:
                return AuthPageRedirectAdminWhoreRoutePageApi::init();

        }

    }

    /**
     * @param string|NULL $title
     * @param string|NULL $link
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function redirect(string $title=NULL,string $link=NULL){

        $data=array(
            'title'     =>$title,
            'redirect'  =>$link
        );

        return ResponseSuccess::init($data);

    }

    /**
     * Prepare link list
     */
    private static function set_link_list(){

        if(!isset($_POST['link'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Link is not exists'
            );

            ResponseUnprocessableEntity::init($error);

        }

        self::$list=Url::get_url_list($_POST['link']);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_link_list();

        return self::set_redirect();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}