<?php

namespace Project\Whore\Admin\Api\PayPal;

use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;
use Core\Module\PayPal\PayPalPaymentCheck;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\Admin\Action\PayPal\PayPalPaymentCheckAction;

class PayPalPaymentCheckApi{

    /** @var string */
    private static $order_ID;

    /** @var int */
    private static $paypal_payment_ID;

    /** @var float */
    private static $amount;

    /** @var float */
    private static $balance;

    /** @var bool */
    private static $is_sandbox;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_payment(){

        $r=PayPalPaymentCheckAction::init(self::$order_ID,self::$amount,self::$is_sandbox);

        self::$balance=$r['balance'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'       =>true,
            'data'          =>[
                'balance'=>self::$balance
            ]
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::check_payment();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

//        $list=Json::decode(file_get_contents('php://input'));

        $error_info_list=[];

        if(empty($_POST['order_ID']))
            $error_info_list[]='Order ID is empty';

        if(empty($_POST['amount']))
            $error_info_list[]='Amount is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

//        \Config::$is_debug=true;
//
        if(!empty($_POST['user_ID']))
            User::$user_ID=$_POST['user_ID'];

        self::$order_ID     =$_POST['order_ID'];
        self::$amount       =(float)$_POST['amount'];
        self::$is_sandbox   =empty($_POST['is_sandbox'])?false:(bool)$_POST['is_sandbox'];

        return self::set();

    }

}