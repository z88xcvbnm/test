<?php

namespace Project\Whore\Admin\Module\Dir;

class DirConfigAdminProject{

    /** @var string */
    public  static $dir_js                      ='Project/Whore/Admin/Template/Js';

    /** @var string */
    public  static $dir_js_hash_show            ='show/js';

    /** @var string */
    public  static $dir_css                     ='Project/Whore/Admin/Template/Css';

    /** @var string */
    public  static $dir_css_hash_show           ='show/css';

    /** @var string */
    public  static $dir_image                   ='Project/Whore/Admin/Template/Images';

    /** @var string */
    public  static $dir_image_hash_show         ='show/image/content';

}