<?php

namespace Project\Whore\Admin\Shell\Source;

use Core\Module\Dir\Dir;
use Core\Module\Exception\PhpException;
use Core\Module\Response\ResponseSuccess;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\All\Action\FacePlusPlus\AddFaceTokenToFacesetFacePlusPlusAction;
use Project\Whore\All\Action\FacePlusPlus\GetFacesCoordsFacePlusPlusAction;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;

class AddFaceTokenListToFacePlusPlusShell{

    /** @var int */
    private static $source_ID;

    /** @var array */
    private static $face_ID_list        =[];

    /** @var string */
    private static $file_log_path;

    /** @var int */
    private static $face_len            =100;

    /** @var int  */
    private static $memory_usage        =0;

    /** @var int */
    private static $file_log_size_max   =10485760;

    /** @var string */
    private static $cookie_path         ='Temp/Parsing/FacePlusPlus/reverse_ip.txt';

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$source_ID        =NULL;
        self::$face_ID_list     =[];
        self::$file_log_path    =NULL;
        self::$cookie_path      =NULL;

        return true;

    }

    /**
     * @param $data
     * @return bool|int
     */
    private static function add_to_log($data){

        $memory         =round((memory_get_usage()/(1024*1024)),3);
        $memory_peak    =round((memory_get_peak_usage()/(1024*1024)),3);

        if($memory>self::$memory_usage){

            $old_memory             =self::$memory_usage;
            self::$memory_usage     =$memory;

            self::add_to_log('############## -> Memory usage update from '.$old_memory.' to '.self::$memory_usage.'/'.$memory_peak.'mb'."\n");

        }

        echo print_r($data,true);

        if(file_exists(self::$file_log_path))
            if(filesize(self::$file_log_path)>self::$file_log_size_max)
                self::set_log_path();

        if(!empty($_POST['need_log']))
            return file_put_contents(self::$file_log_path,$data,FILE_APPEND);

        return false;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_file_log_path(){

        self::$file_log_path=DIR_ROOT.'/'.DirConfigProject::$dir_parsing_log;

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/FacePlusPlus';

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/shell_face_plus_plus_'.time().'.log';

        return true;

    }

    /**
     * @return bool
     */
    private static function set_cookie_path(){

        self::$cookie_path=DIR_ROOT.'/'.self::$cookie_path;

        return true;

    }

    /**
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_ID_list(){

        $index=0;

        do{

            $face_ID_list=FaceData::get_face_ID_list_with_zero_image_facekit_len_with_source_ID(self::$source_ID,0,self::$face_len);

            self::prepare_face_image_list($face_ID_list);

            $index+=self::$face_len;

            if(count($face_ID_list)<self::$face_len){

                self::add_to_log('DONE');

                break;

            }

        }while(true);

    }

    /**
     * @param array $face_ID_list
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_image_list(array $face_ID_list=[]){

        if(count($face_ID_list)==0)
            return true;

        self::add_to_log("Start prepare face ID list\n");

        foreach($face_ID_list as $face_ID){

            self::add_to_log("-> Start face ID: ".$face_ID."\n");
            self::add_to_log("--> Start get image list\n");

            $image_list=FaceImage::get_face_plus_plus_data_without_faceset_ID($face_ID);

            self::add_to_log("--> Have image len: ".count($image_list)." - done\n");
            self::add_to_log("--> ".print_r($image_list,true)."\n");

            if(count($image_list)>0){

                self::add_to_log("--> Start send image to Face Plus Plus\n");

                $image_coords_list=[];

                foreach($image_list as $index=>$row){

                    self::add_to_log("---> Get image path list\n");

                    $image_path_list=ImagePathAction::get_image_path_data($row['image_ID'],true,true,true);

                    self::add_to_log("----> ".print_r($image_path_list,true)."\n");
                    self::add_to_log("----> Done\n");

                    if(!empty($image_path_list)){

                        $image_path=$image_path_list['image_dir'].'/'.$image_path_list['image_item_ID_list']['large']['ID'];

                        self::add_to_log("----> image_path: ".$image_path."\n");

                        if(file_exists($image_path)){

                            self::add_to_log("----> Get faces coords\n");

                            $get_coords_data=GetFacesCoordsFacePlusPlusAction::init($row['image_ID'],$image_path,false,self::$file_log_path);

                            self::add_to_log("-----> done\n");
//                            self::add_to_log(print_r($get_coords_data,true)."\n");

                            if(!empty($get_coords_data)){

                                $image_list[$index]['face_len']     =$get_coords_data['face_len'];
                                $image_list[$index]['coords']       =$get_coords_data['face_list'];

                                $face_token=NULL;

                                if(count($get_coords_data['face_list'])==1)
                                    if(!empty($get_coords_data['face_list'][0]))
                                        if(!empty($get_coords_data['face_list'][0]['face_token'])){

                                            $face_token                         =$get_coords_data['face_list'][0]['face_token'];
                                            $image_list[$index]['face_token']   =$face_token;

                                        }

                                self::add_to_log("-----> Start update face image\n");

                                if(!FaceImage::update_face_image_face_token($face_ID,$row['image_ID'],$get_coords_data['face_list'],$face_token)){

                                    $error=[
                                        'title'     =>PhpException::$title,
                                        'info'      =>'Face image was not update'
                                    ];

                                    throw new PhpException($error);

                                }

                                $image_coords_list[]=$image_list[$index];

                                self::add_to_log("------> done\n");

                            }

                        }

                    }

                }

                if(count($image_coords_list)>0){

                    self::add_to_log("-----> Start face token to Face++ \n");

                    $add_face_token_data=AddFaceTokenToFacesetFacePlusPlusAction::init($face_ID,$image_coords_list,false,self::$file_log_path,true);

                    self::add_to_log(print_r($add_face_token_data,true)."\n");

                    if(empty($add_face_token_data)){

                        $error=[
                            'title'     =>PhpException::$title,
                            'info'      =>'Face token was not add'
                        ];

                        throw new PhpException($error);

                    }

                    self::add_to_log("-----> Done \n");

                }

                self::add_to_log("--> Done\n");

            }

            FaceData::update_face_data_face_plus_plus_image_len($face_ID);

//            $n++;
//
//            if($n>10)
//                return true;

        }

        self::add_to_log("Finish prepare face ID list\n");

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_file_log_path();
        self::set_cookie_path();

        self::prepare_face_ID_list();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::reset_data();

        self::$source_ID=empty($_POST['source_ID'])?NULL:(int)$_POST['source_ID'];

        return self::set();

    }

}