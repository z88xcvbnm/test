<?php

namespace Project\Whore\Admin\Shell\Source;

use Core\Module\Telegram\TelegramAuth;

class TestTelegramShell{

    // sudo -u www-data /usr/bin/php /var/www/public/index.php "action=test_telegram" "page=1" "need_log=1" "need_tor=0" "is_test=0" "is_test=0" "need_tor=0" "host_index=0"

    /**
     * @return bool
     */
    public  static function init(){

        \Config::$is_debug=true;

        TelegramAuth::init();

        return true;

    }

}