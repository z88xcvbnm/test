<?php

namespace Project\Whore\Admin\Shell\Source;

use Core\Module\Exception\ParametersException;
use Project\Whore\All\Module\Face\FaceData;

class UpdateFaceDataInfoShell{

    // sudo -u www-data /usr/bin/php /var/www/public/index.php "action=update_face_data_info"

    /** @var int */
    private static $source_ID;

    /** @var array */
    private static $face_ID_list=[];

    /** @var int */
    private static $face_len=0;

    /**
     * @var array
     */
    private static $key_list=[
        'Рост',
        'Вес',
        'Бюст',
        'Параметры',
        'Тату',
        'О себе',
        'Мин. гонорар',
        'Instagram',
        'VK'
    ];

    /**
     * @return bool
     */
    private static function set_return(){

        file_put_contents('Temp/Log/update_face_data_info.log',print_r(self::$face_ID_list,true),FILE_APPEND);

        $data=[
            'success'=>true,
            'data'      =>[
                'prepared_count'=>self::$face_len
            ]
        ];

        print_r($data);

        echo"\n\n";

        return true;

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_info(){

        do{

            $r=FaceData::get_face_data_info_list_with_not_check_info(0,100,NULL,self::$source_ID);

            if(count($r)==0)
                return true;

            foreach($r as $face_ID=>$info){

//                sleep(1);

                $list       =mb_split("\n",$info);
                $r_list     =[];

                if(count($list)>0){

                    $temp_list  =[];
                    $key_last   =NULL;

                    foreach($list as $row)
                        if(!empty($row)){

                            $row_list=mb_split(':',$row);

                            if(count($row_list)==1){

                                if(empty($key_last))
                                    self::$face_ID_list[]=$face_ID;


                            }
                            else{

                                $key_last=trim($row_list[0]);

                                switch(trim($key_last)){

                                    case'WhatsApp':
                                    case'Viber':
                                    case'Telegram':
                                    case'FaceTime':
                                    case'Телефон':{

                                        preg_match_all('/<a.*?>\s*(.*?)\s*<\/a>/is',$row,$r_row);

                                        if(count($r_row[1])>0){

                                            $phone_list=[];

                                            foreach($r_row[1] as $phone)
                                                if(array_search(trim($phone),$phone_list)===false)
                                                    $phone_list[]=$phone;

                                            if(count($phone_list)>0)
                                                $row=$key_last.': '.implode(', ',$phone_list);

                                        }

                                        break;

                                    }

                                    case'О себе':{

                                        $about=mb_strtolower(trim($row_list[1]),'utf-8');

                                        if($about=='пользователь предпочёл не указывать информацию о себе')
                                            $row='О себе: Ищу спонсора';

                                        break;

                                    }

                                    case'Бюст':{

                                        $boobs  =mb_strtolower(trim($row_list[1]),'utf-8');
                                        $r_row  =NULL;

                                        switch($boobs){

                                            case'aa':
                                            case'аа':{

                                                $r_row=1;

                                                break;

                                            }

                                            case'a':
                                            case'а':{

                                                $r_row=1;

                                                break;

                                            }

                                            case'b':
                                            case'б':{

                                                $r_row=2;

                                                break;

                                            }

                                            case'c':
                                            case'с':{

                                                $r_row=3;

                                                break;

                                            }

                                            case'd':
                                            case'д':{

                                                $r_row=4;

                                                break;

                                            }

                                            case'dd':
                                            case'дд':{

                                                $r_row=5;

                                                break;

                                            }

                                            case'e':
                                            case'е':{

                                                $r_row=6;

                                                break;

                                            }

                                            case'f':
                                            case'ф':{

                                                $r_row=7;

                                                break;

                                            }

                                            case'ff':
                                            case'фф':{

                                                $r_row=8;

                                                break;

                                            }

                                            case'g':
                                            case'г':{

                                                $r_row=9;

                                                break;

                                            }

                                            case'gg':
                                            case'гг':{

                                                $r_row=10;

                                                break;

                                            }

                                        }

                                        if(!empty($r_row))
                                            $row='Бюст: '.$r_row;

                                        break;

                                    }

                                }

                                $temp_list[$key_last]=$row;

                            }

                        }

                    if(count($temp_list)>0){

                        $r_list=[];

                        foreach(self::$key_list as $index=>$key){

                            if(isset($temp_list[$key])){

                                $r_list[]=$temp_list[$key];

                                unset($temp_list[$key]);

                            }

                        }

                        foreach($temp_list as $key=>$row)
                            $r_list[]=$row;

                    }

                }

                if(count($r_list)>0)
                    $info=implode("\n",$r_list);

                FaceData::update_face_data_info_dev($face_ID,$info);

                self::$face_len++;

                echo 'prepared count: '.self::$face_len.' -> '.$face_ID."\n";

//                exit;

            }

        }while(true);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::prepare_face_info();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::$source_ID=empty($_POST['source_ID'])?NULL:(int)$_POST['source_ID'];

        return self::set();

    }

}