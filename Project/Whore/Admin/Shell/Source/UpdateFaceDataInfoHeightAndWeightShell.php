<?php

namespace Project\Whore\Admin\Shell\Source;

use Core\Module\Exception\ParametersException;
use Project\Whore\All\Module\Face\FaceData;

class UpdateFaceDataInfoHeightAndWeightShell{

    // sudo -u www-data /usr/bin/php /var/www/public/index.php "action=update_face_data_info_height_and_weight"

    /** @var int */
    private static $source_ID=3;

    /** @var array */
    private static $face_ID_list=[];

    /** @var int */
    private static $face_len=0;

    /**
     * @return bool
     */
    private static function set_return(){

        file_put_contents('Temp/update_face_data_info.log',print_r(self::$face_ID_list,true),FILE_APPEND);

        $data=[
            'success'=>true,
            'data'      =>[
                'prepared_count'=>self::$face_len
            ]
        ];

        print_r($data);

        echo"\n\n";

        return true;

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_info(){

        do{

            $r=FaceData::get_face_data_info_list_with_not_check_info(0,100,NULL,self::$source_ID);

            if(count($r)==0)
                return true;

            foreach($r as $face_ID=>$info){

                $list=mb_split("\n",$info);

                if(count($list)>0){

                    $key_last=NULL;

                    foreach($list as $index=>$row)
                        if(!empty($row)){

                            $row_list=mb_split(':',$row);

                                $key_last=trim($row_list[0]);

                                switch(trim($key_last)){

                                    case'Рост':
                                    case'Вес':{

                                        $list[$index]=$key_last.': '.(int)$row_list[1];

                                        break;

                                    }

                                }

                            }

                }

                $info=implode("\n",$list);

                FaceData::update_face_data_info_dev($face_ID,$info);

                self::$face_len++;

                echo 'prepared count: '.self::$face_len.' -> '.$face_ID."\n";

//                exit;

            }

        }while(true);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::prepare_face_info();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}