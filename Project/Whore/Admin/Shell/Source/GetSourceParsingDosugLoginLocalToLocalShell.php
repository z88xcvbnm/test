<?php

namespace Project\Whore\Admin\Shell\Source;

use Core\Module\Curl\CurlGet;
use Core\Module\Curl\CurlPost;
use Core\Module\Curl\CurlPostParsing;
use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Dir\Dir;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\ShluhamNetException;
use Core\Module\File\File;
use Core\Module\File\FileParametersCash;
use Core\Module\Geo\City;
use Core\Module\Geo\CityLocalization;
use Core\Module\Geo\CountryLocalization;
use Core\Module\Image\Image;
use Core\Module\Image\ImageUploadedPrepare;
use Core\Module\Json\Json;
use Core\Module\Lang\LangConfig;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;
use Project\Whore\All\Module\Source\Source;

class GetSourceParsingDosugLoginLocalToLocalShell{

    // sudo -u www-data /usr/bin/php /var/www/public/index.php "action=get_source_parsing_dosug_login" "page=1" "need_log=1" "need_tor=0" "is_test=0" "is_test=0" "need_tor=0" "host_index=0"
    // sudo -u www-data /usr/bin/php /var/www/public/index.php "action=send_face_token_to_face_plus_plus" "source_ID=25" "need_log=1" "face_ID=1"

    /** @var int */
    private static $source_ID                               =25;

    /** @var int */
    private static $page                                    =1;

    /** @var array */
    private static $header_list=[
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
//        'Accept-Encoding: gzip, deflate, br',
        'Accept-Language: en,ru;q=0.9',
//        'Connection: keep-alive',
        'Host: www.portaldosug.cz',
//        'Upgrade-Insecure-Requests: 1',
        'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36',
    ];

    /** @var string */
    private static $fp_hash                                 ='7b45ae15eef893be882fc55bc27ac67b3f9da2ded67b2ddd033003e7ee623a6c';

    /** @var array */
    private static $header                                  =[
        'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36',
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'Accept-Encoding: gzip, deflate, br',
        'Accept-Language: en,ru;q=0.9',
        'Cache-Control: max-age=0',
        'Connection: keep-alive',
        'Content-Type: application/x-www-form-urlencoded',
        'Host: www.portaldosug.cz',
        'Origin: https://www.portaldosug.cz',
        'Referer: https://www.portaldosug.cz/ru/p/login',
        'Upgrade-Insecure-Requests: 1',
    ];

    /** @var int */
    private static $host_index                              =0;

    /** @var string */
    private static $host                                    ='https://www.portaldosug.cz';

    /** @var string */
    private static $url_login                               ='ru/p/login';

    /** @var string */
    private static $csrf_token;

    /** @var string */
//    private static $url_rating                              ='ru/p/ratings/top/total/all/page';
//    private static $url_rating                              ='ru/p/ratings/top/14/all/page';
//    private static $url_rating                              ='ru/p/ratings/top/10/all/page'; // просмотры профилей за все время
    private static $url_rating                              ='ru/p/ratings/top/10/month/page'; // просмотры профилей за все месяц // working now
//    private static $url_rating                              ='ru/p/ratings/top/10/week/page'; // просмотры профилей за все месяц
//    private static $url_rating                              ='ru/p/ratings/top/11/all/page'; // Сумма полученных подарков за все месяц
//    private static $url_rating                              ='ru/p/ratings/top/12/all/page'; // Колечество проданных просмотров анкет за все месяц

    /** @var string */
    private static $url_profile                             ='ru/p/profile';

    /** @var string */
    private static $dir_path                                ='Temp/DosugRating';

    /** @var string */
    private static $cookie_path                             ='Temp/Parsing/DosugRating/reverse_ip.txt';

    /** @var string */
    private static $url_api                                 ='https://shluham.net/api/json';

    /** @var string */
    private static $api_add_face_data_action                ='add_face_data';

    /** @var string */
    private static $api_add_face_image_action               ='add_face_image';

    /** @var string */
    private static $api_get_face_ID_action                  ='get_face_ID';

    /** @var string */
    private static $api_update_face_to_public_action        ='update_face_to_public';

    /** @var string */
    private static $key_hash                                ='domino777';

    /** @var string */
    public  static $file_log_path                           ='';

    /** @var int  */
    private static $memory_usage                            =0;

    /** @var int */
    private static $image_min_len                           =2;

    /** @var int */
    private static $age_min                                 =0;

    /** @var int */
    private static $age_max                                 =50;

    /** @var int */
    private static $profile_index                           =0;

    /** @var int */
    private static $file_log_size_max                       =10485760;

//    /** @var array */
//    private static $sleep_profile_len_list                  =[1,2];
//
//    /** @var array */
//    private static $sleep_after_profile_list                =[1,2];
//
//    /** @var array */
//    private static $sleep_random_list                       =[1,2];

    /** @var array */
    private static $sleep_profile_len_list                  =[10,20];

    /** @var array */
    private static $sleep_after_profile_list                =[10,20];

    /** @var array */
    private static $sleep_random_list                       =[10,20];

    /** @var int */
    private static $face_data_type_ID;

    /** @var bool */
    private static $need_sleep                              =true;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_data_type(){

        self::$face_data_type_ID=Source::get_source_rang(self::$source_ID);

        return true;

    }

    /**
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function sleep_random(){

        $rand=rand(self::$sleep_random_list[0],self::$sleep_random_list[1]);

        self::add_to_log("SLEEP: ".$rand." ".Date::get_date_time_full()."\n");

        if(!self::$need_sleep)
            return true;

        return sleep($rand);
        
    }

    /**
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function sleep_after_profile_uploaded(){

        $rand=rand(self::$sleep_after_profile_list[0],self::$sleep_after_profile_list[1]);

        self::add_to_log("SLEEP after parse profile: ".$rand." ".Date::get_date_time_full()."\n");

        if(!self::$need_sleep)
            return true;

        return sleep($rand);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_log_path(){

        self::$dir_path=DIR_ROOT.'/'.self::$dir_path;

        if(!file_exists(self::$dir_path))
            Dir::create_dir(self::$dir_path);

        self::$file_log_path=DIR_ROOT.'/'.DirConfigProject::$dir_parsing_log;

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/DosugRating';

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/dosug_rating_'.time().'.log';

//        self::add_to_log('Created log path');

        return true;

    }

    /**
     * @param $data
     * @return bool|int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_to_log($data){

        $memory         =round((memory_get_usage()/(1024*1024)),3);
        $memory_peak    =round((memory_get_peak_usage()/(1024*1024)),3);

        if($memory>self::$memory_usage){

            $old_memory             =self::$memory_usage;
            self::$memory_usage     =$memory;

            self::add_to_log('############## -> Memory usage update from '.$old_memory.' to '.self::$memory_usage.'/'.$memory_peak.'mb'."\n");

        }

        echo print_r($data,true);

        if(file_exists(self::$file_log_path))
            if(filesize(self::$file_log_path)>self::$file_log_size_max)
                self::set_log_path();

        if(!empty($_POST['need_log']))
            return file_put_contents(self::$file_log_path,$data,FILE_APPEND);

        return false;
        
    }

    /**
     * @return bool
     */
    private static function set_cookie_path(){

        self::$cookie_path=DIR_ROOT.'/'.self::$cookie_path;

        return true;

    }

    /**
     * @param string|NULL $source_account_link
     * @return |null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_get_face_ID(string $source_account_link=NULL){

        if(empty($source_account_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Source account link is empty'
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_get_face_ID_action;
        $data       =[
            'source_account_link'   =>$source_account_link,
            'key_hash'              =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face ID was not give',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){
            
            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if($r['status']!=200){

            self::add_to_log("--> restart send get face ID: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_get_face_ID($source_account_link);

        }

        if(isset($r['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if(empty($r['data']))
            return NULL;

        if(!Json::is_json($r['data']))
            return NULL;

        $data=Json::decode($r['data']);

        if(isset($data['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        return $data['data']['face_ID'];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $source_ID
     * @param int|NULL $image_ID
     * @param int|NULL $city_ID
     * @param string|NULL $source_account_link
     * @param string|NULL $name
     * @param int|NULL $age
     * @param string|NULL $city
     * @param string|NULL $info
     * @return bool|null
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_add_face_data(int $face_ID=NULL,int $source_ID=NULL,int $image_ID=NULL,int $city_ID=NULL,string $source_account_link=NULL,string $name=NULL,int $age=NULL,string $city=NULL,string $info=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(empty($source_ID))
            $error_info_list[]='Source ID is empty';

        if(empty($source_account_link))
            $error_info_list[]='Source account link is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_add_face_data_action;
        $data       =[
            'face_ID'                   =>$face_ID,
            'source_ID'                 =>$source_ID,
            'image_ID'                  =>$image_ID,
            'city_ID'                   =>$city_ID,
            'source_account_link'       =>$source_account_link,
            'name'                      =>$name,
            'age'                       =>$age,
            'city'                      =>$city,
            'info'                      =>$info,
            'key_hash'                  =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face data was not add',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if($r['status']!=200){

            self::add_to_log("--> restart send add face data: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_add_face_data($face_ID,$source_ID,$image_ID,$city_ID,$source_account_link,$name,$age,$city,$info);

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $facekit_image_ID
     * @param int|NULL $file_size
     * @param string|NULL $file_content_type
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param string|NULL $file_path
     * @param array|NULL $face_coords
     * @return |null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_add_face_image(int $face_ID=NULL,int $facekit_image_ID=NULL,int $file_size=NULL,string $file_content_type=NULL,string $file_mime_type=NULL,string $file_extension=NULL,string $file_path=NULL,array $face_coords=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_add_face_image_action;
        $data       =[
            'face_ID'               =>$face_ID,
            'facekit_image_ID'      =>$facekit_image_ID,
            'file_size'             =>$file_size,
            'file_content_type'     =>$file_content_type,
            'file_mime_type'        =>$file_mime_type,
            'file_extension'        =>$file_extension,
            'image_base64'          =>base64_encode(file_get_contents($file_path)),
            'face_len'              =>count($face_coords),
            'face_coords'           =>empty($face_coords)?'[]':Json::encode($face_coords),
            'key_hash'              =>self::$key_hash,
            'time'                  =>time().'_'.rand(0,time())
        ];

        $r=CurlPost::init($url,[],$data);

        self::add_to_log("--> ERROR: ".print_r($r,true)."\n");

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face image was not add',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if($r['status']==500||$r['status']==500){

            self::add_to_log("--> request: ".Date::get_date_time_full()."\n");
            self::add_to_log(print_r($r,true)."\n");
            self::add_to_log(print_r($data,true)."\n");
            self::add_to_log("--> restart send add face image: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_add_face_image($face_ID,$facekit_image_ID,$file_size,$file_content_type,$file_mime_type,$file_extension,$file_path,$face_coords);

        }
        else if($r['status']!=200)
            return NULL;
        else{

            if(empty($r['data']))
                return NULL;

            if(!Json::is_json($r['data']))
                return NULL;

            $data=Json::decode($r['data']);

            if(isset($data['error'])){

                self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

                return NULL;

            }

            return $data['data']['image_ID'];

        }

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return bool|null
     * @throws ParametersException
     * @throws PhpException
     * @throws ShluhamNetException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_update_face_to_public(int $face_ID=NULL,int $image_ID=NULL){

        $error_info_list=[];

        if(empty($face_ID))
            $error_info_list[]='Face ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $url        =self::$url_api.'/'.self::$api_update_face_to_public_action;
        $data       =[
            'face_ID'               =>$face_ID,
            'image_ID'              =>$image_ID,
            'key_hash'              =>self::$key_hash
        ];

        $r=CurlPost::init($url,[],$data);

//        echo"\n\n";
//        print_r($r);
//        echo"\n\n";

        if(empty($r)){

            $error=[
                'title'     =>ShluhamNetException::$title,
                'info'      =>'Face was not update to public',
                'data'      =>$r
            ];

            throw new ShluhamNetException($error);

        }

        if(isset($r['error'])){

            self::add_to_log("--> ERROR: ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if($r['status']!=200){

            self::add_to_log("--> restart send update face to public: ".Date::get_date_time_full()."\n");

            sleep(FacePlusPlusConfig::get_random_sleep_timeout());

            return self::send_update_face_to_public($face_ID,$image_ID);

        }

        return true;

    }

    /**
     * @param string|NULL $source_account_link
     * @return int|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_face_ID_from_source_account_link(string $source_account_link=NULL){

        $source_account_link=preg_replace('/(https\:\/\/.*?)\./is','',$source_account_link);

        $q=[
            'select'=>[
                'face_id'
            ],
            'table'=>FaceData::$table_name,
            'where'=>[
                'source_account_link'   =>[
                    'method'    =>'like',
                    'value'     =>$source_account_link
                ],
                'type'                  =>[0,1,2]
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['face_id'];

//        if(empty($source_account_link)){
//
//            $error=[
//                'title'     =>ParametersException::$title,
//                'info'      =>'Source account link is empty'
//            ];
//
//            throw new ParametersException($error);
//
//        }
//
//        return FaceData::get_face_ID_from_source_account_link($source_account_link);

    }

    /**
     * @param string|NULL $file_extension
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_file(string $file_extension=NULL){

        return File::add_file_without_params($file_extension,true,false,false);

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_hash
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_file(int $file_ID=NULL,string $file_hash=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_content_type=NULL,string $file_extension=NULL){

        return File::update_file($file_ID,$file_hash,1,1,$file_size,$file_size,$file_mime_type,$file_content_type,$file_extension,true,false,true);

    }

    /**
     * @param string|NULL $file_path
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_file(string $file_path=NULL){

        if(empty($file_path))
            return NULL;

        self::add_to_log("start get file from url: ".$file_path." ".Date::get_date_time_full()."\n");

//        $image=@file_get_contents($file_path);

//        $r=CurlPost::init($file_path,[],UseragentStack::get_random_useragent());
//        $r=CurlPost::init($file_path,[],NULL,true);

        $image=file_get_contents($file_path);

//        if($r['status']!=200)
//            return NULL;
//
//        $image=$r['data'];

        self::add_to_log("file downloaded: ".$file_path." ".Date::get_date_time_full()."\n");

        if($image===false)
            return NULL;

        self::add_to_log("try get file extension: ".$file_path." ".Date::get_date_time_full()."\n");

        $file_name          =basename($file_path);
        $query              =parse_url($file_path, PHP_URL_QUERY);
        $file_name          =str_replace('?'.$query,'',$file_name);
        $file_extension     =File::get_file_extension_from_file_name($file_name);

        if(empty($file_extension))
            $file_extension='jpg';

        self::add_to_log("got file extension: ".$file_path." ".Date::get_date_time_full()."\n");

        $file_ID=self::add_file($file_extension);

        self::add_to_log("create file row in DB: ".$file_path." ".Date::get_date_time_full()."\n");

        if(empty($file_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new PhpException($error);

        }

        $file_dest_path=File::get_file_path_from_file_ID($file_ID,true);

        File::get_file_dir($file_ID,NULL,Date::get_date_time_full());
        FileParametersCash::add_file_parameter_in_cash($file_ID,'file_extension',$file_extension);

        self::add_to_log("try put file on the drive: ".$file_path." ".Date::get_date_time_full()."\n");

//        $query              =parse_url($file_dest_path, PHP_URL_QUERY);
//        $file_dest_path     =str_replace('?'.$query,'',$file_dest_path);

        if(file_put_contents($file_dest_path,$image)===false){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not save'
            ];

            throw new PhpException($error);

        }

        self::add_to_log("check min file size: ".$file_path." ".Date::get_date_time_full()."\n");
        self::add_to_log("file size: ".filesize($file_dest_path)." ".Date::get_date_time_full()."\n");

        if(filesize($file_dest_path)<=16000){

            self::add_to_log("BAD FILE: ".$file_path." ".Date::get_date_time_full()."\n");

            return NULL;

        }

//        if(!OsServer::$is_windows){
//
//            self::add_to_log("try create temp image file: ".$file_path." ".Date::get_date_time_full()."\n");
//
//            $temp_image_path='Temp/ModelZoneParsing/img_'.time();
//
//            try{
//
//                self::add_to_log("try open Imagick: ".$file_path." ".Date::get_date_time_full()."\n");
//
//                $img=new \Imagick($file_dest_path);
//                self::add_to_log("read image with Imagick: ".$file_path." ".Date::get_date_time_full()."\n");
//                $img->readImage($file_dest_path);
//                self::add_to_log("try write image with Imagick: ".$file_path." ".Date::get_date_time_full()."\n");
//                $img->writeImage($temp_image_path);
//                self::add_to_log("temp image file created: ".$file_path." ".Date::get_date_time_full()."\n");
//
//            }
//            catch (\ImagickException $e){
//
//                self::add_to_log("create temp file ERROR: ".print_r($e)." ".Date::get_date_time_full()."\n");
//
//                return NULL;
//
//            }
//
//            unset($img);
//
//            unlink($file_dest_path);
//
//            copy($temp_image_path,$file_dest_path);
//
//            unlink($temp_image_path);
//
//        }

        self::add_to_log("try get image object from file path: ".$file_path." ".Date::get_date_time_full()."\n");

        $image_object=Image::get_image_object_from_file_path($file_dest_path);

        if(empty($image_object)){

            self::add_to_log("FILE ERROR: ".$file_path." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        $size       =getimagesize($file_dest_path,$im_info);
        $sum        =$size[0]*$size[1];

        if($sum!=0){

            $file_size  =filesize($file_dest_path);
            $cof        =$file_size/$sum;

        }
        else
            $cof=0;

        self::add_to_log("-> file cof: ".$cof." ".Date::get_date_time_full()."\n");

        if($cof<.01){

            self::add_to_log("BAD FILE SUM: ".$file_path." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        switch(exif_imagetype($file_dest_path)){

            case 1:
            case 2:
            case 3:
            case 7:
            case 8:
                break;

            default:
                return NULL;

        }

        $file_mime_type         =File::get_file_mime_type_from_path($file_dest_path);
        $file_content_type      =File::get_file_content_type_from_path($file_dest_path);
        $file_size              =filesize($file_dest_path);
        $data_list              =[
            User::$user_ID,
            $file_size,
            $file_mime_type,
            $file_content_type,
            time(),
            rand(0,time())
        ];
        $file_hash              =Hash::get_sha1_encode(implode(':',$data_list));

        if($file_size==0){

            File::remove_file_ID($file_ID);

            self::add_to_log("--> file is not exists: ".$file_size." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        if(!self::update_file($file_ID,$file_hash,$file_size,$file_mime_type,$file_content_type,$file_extension)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not update'
            ];

            throw new PhpException($error);

        }

        return[
            'file_ID'               =>$file_ID,
            'file_content_type'     =>$file_content_type,
            'file_extension'        =>$file_extension
        ];

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_image(int $file_ID=NULL,string $file_content_type='image',string $file_extension=NULL){

        return ImageUploadedPrepare::init($file_ID,$file_content_type,$file_extension);

    }

    /**
     * @param array $image_list
     * @return array
     */
    private static function search_face_in_image_list(array $image_list=[]){

        return $image_list;

    }

    /**
     * @param string|NULL $source_account_link
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_face_link(string $source_account_link=NULL){

        $face_ID=self::get_face_ID_from_source_account_link($source_account_link);

        if(!empty($face_ID))
            self::add_to_log("Isset face_ID: ".$face_ID."\n");

        return !empty($face_ID);

    }

    /**
     * @param string|NULL $source_account_link
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face(string $source_account_link=NULL){

        $face_ID=self::get_face_ID_from_source_account_link($source_account_link);

        if(empty($face_ID))
            $face_ID=Face::add_face();
        
        return[
            'face_ID'=>$face_ID
        ];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @param string|NULL $name
     * @param string|NULL $source_account_link
     * @param int $image_len
     * @param int|NULL $age
     * @param string|NULL $city
     * @param string|NULL $info
     * @param int|NULL $face_data_type_ID
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_face_data(int $face_ID=NULL,int $image_ID=NULL,string $name=NULL,string $source_account_link=NULL,int $image_len=0,int $age=NULL,string $city=NULL,string $info=NULL,int $face_data_type_ID=NULL){

        $face_data_ID=FaceData::get_face_data_ID($face_ID);

        $city_ID=NULL;

        if(!empty($city)){

            $city_data=City::get_city_IDs_from_city($city);

            if(!empty($city_data)){

                $city_ID    =$city_data['city_ID'];
                $city       =$city_data['result'];
            }

        }

        if(empty($face_data_ID))
            $face_data_ID=FaceData::add_face_data($face_ID,$city_ID,$image_ID,self::$source_ID,$source_account_link,$name,$age,$city,$info,$image_len,0,NULL,2,NULL,$face_data_type_ID);

        return[
            'face_data_ID'=>$face_data_ID
        ];

    }

    /**
     * @param int|NULL $face_ID
     * @param int|NULL $image_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_data_image(int $face_ID=NULL,int $image_ID=NULL){

        return FaceData::update_face_data_image_ID($face_ID,$image_ID);

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face(int $face_ID=NULL){

        return Face::update_face($face_ID);

    }

    /**
     * @param int|NULL $face_data_ID
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_face_data(int $face_data_ID=NULL){

        return FaceData::update_face_data_to_public($face_data_ID);

    }

    /**
     * @param int|NULL $face_ID
     * @param array $image_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function upload_image_to_facekit_db(int $face_ID=NULL,array $image_list=[]){

        if(count($image_list)==0)
            return[];

        $image_ID       =NULL;
        $api_image_ID   =NULL;

        foreach($image_list as $image_index=>$image_data){

            self::add_to_log("-> start prepare image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

            $is_added=false;

            $image_list[$image_index]['face_image_ID']      =NULL;
            $facekit_image_ID                               =NULL;

            if(!$is_added){

                self::add_to_log("--> start add to local face image image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

                $image_list[$image_index]['face_image_ID']=FaceImage::add_face_image($face_ID,$image_data['file_ID'],$image_data['image_ID'],NULL,NULL,[]);

                self::add_to_log("--> added to local face image: ".$image_list[$image_index]['face_image_ID']." image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

            }

            self::add_to_log("-> finish upload image_ID: ".$image_list[$image_index]['image_ID'].": ".Date::get_date_time_full()."\n");

        }

        return[
            'image_ID'          =>empty($image_ID)?$image_list[0]['image_ID']:$image_ID,
            'image_list'        =>$image_list
        ];

    }

    /**
     * @param string|NULL $profile_item_ID
     * @param string|NULL $name
     * @param int|NULL $age
     * @param string|NULL $city
     * @param int|NULL $height
     * @param int|NULL $boobs
     * @param int|NULL $price
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile_link(string $profile_item_ID=NULL){

        $profile_link_default       =self::$host.'/'.self::$url_profile.'/'.$profile_item_ID;
        $profile_link               =self::$host.'/'.self::$url_profile.'/'.$profile_item_ID.'/details';

        if(self::isset_face_link($profile_link_default)){

            self::add_to_log("WARNING: Already exists".$profile_link." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        self::add_to_log("Set profile link: ".$profile_link." ".Date::get_date_time_full()."\n");

        $r=CurlPost::init($profile_link,[],[],false,self::$cookie_path,false,NULL,NULL,NULL,self::$header_list);

        if($r['status']==504){

            self::add_to_log("Cloudflare restart profile link: ".$profile_link." ".Date::get_date_time_full()."\n");

            self::sleep_random();

            self::reset_connect();

            return self::prepare_profile_link($profile_item_ID);

        }

        self::add_to_log("Got profile link status: ".$r['status']." ".Date::get_date_time_full()."\n");

        if($r['status']!=200){

            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            self::add_to_log("ERROR: ".$r['status']." ".Date::get_date_time_full()."\n");

            self::sleep_random();

            self::reset_connect();

            return self::prepare_profile_link($profile_item_ID);

        }

        $r=$r['data'];

        if($r!==false){

            $body=preg_match_all('/<body.*?>(.*?)<\/body>/is',$r,$body_list);

            if($body===false){

                self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                self::add_to_log("ERROR body is empty ".Date::get_date_time_full()."\n");

            }

            if($body!==false)
                if(count($body_list[1])>0){

                    $data=[
                        'id'            =>$profile_item_ID,
                        'link'          =>$profile_link_default,
                        'name'          =>NULL,
                        'sex'           =>NULL,
                        'age'           =>NULL,
                        'city'          =>NULL,
                        'info'          =>[],
                        'image_list'    =>[]
                    ];

                    preg_match_all('/<tr>\s*<td\s*class=\"dataLabel\"\s*>Ник\:<\/td>\s*<td\s*class=\"dataText\"\s*>(.*?)<\/td>\s*<\/tr>/is',$body_list[1][0],$list);

                    if(count($list[1])>0)
                        $data['name']=trim($list[1][0]);

                    preg_match_all('/<tr>\s*<td\s*class=\"dataLabel\"\s*>Возраст\:<\/td>\s*<td\s*class=\"dataText\"\s*>(\d+)<\/td>\s*<\/tr>/is',$body_list[1][0],$list);

                    if(count($list[1])>0)
                        $data['age']=trim($list[1][0]);

                    preg_match_all('/<tr>\s*<td\s*class=\"dataLabel\"\s*>Пол\:<\/td>\s*<td\s*class=\"dataText\"\s*>(.*?)<\/td>\s*<\/tr>/is',$body_list[1][0],$list);

                    if(count($list[1])>0){

                        $data['sex']=trim($list[1][0]);

                        if($data['sex']!='женский')
                            return NULL;

                    }

                    $city       =NULL;
                    $country    =NULL;

                    preg_match_all('/<tr>\s*<td\s*class=\"dataLabel\"\s*>Город\:<\/td>\s*<td\s*class=\"dataText\"\s*>(.*?)<\/td>\s*<\/tr>/is',$body_list[1][0],$list);

                    if(count($list[1])>0)
                        $city=trim($list[1][0]);

                    if(empty($city)){

                        preg_match_all('/<tr>\s*<td\s*class=\"dataLabel\"\s*>Часовой пояс\:<\/td>\s*<td\s*class=\"dataText\"\s*>(.*?)\s*\(.*?\)<\/td>\s*<\/tr>/is',$body_list[1][0],$list);

                        if(count($list[1])>0)
                            $city=trim($list[1][0]);

                    }

                    if(!empty($city)){

                        $country        =NULL;
                        $country_ID     =NULL;
                        $city_ID        =CityLocalization::get_city_ID_from_name(mb_strtolower($city,'utf-8'));

                        if(!empty($city_ID)){

                            $country_ID             =City::get_country_ID($city_ID);

                            if(!empty($country_ID))
                                $country=CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);

                        }

                        $data['country_ID']     =$country_ID;
                        $data['city_ID']        =$city_ID;

                        if(!empty($country))
                            $data['city']=$country.', '.$city;
                        else
                            $data['city']=$city;

                    }

                    preg_match_all('/<div\s*class=\"girlNoteTextWhite\"\s*>(.*?)<\/div>/is',$body_list[1][0],$list);

                    if(count($list[1])>0)
                        $data['info'][]='О себе: '.html_entity_decode(str_replace(array('<p>','</p>','<br />'),array('','',"\n"),trim(trim($list[1][0]))));

                    $data['image_list']=self::prepare_profile_gallery_list($profile_item_ID);

                    self::add_to_log(print_r($data,true));

                    return $data;

                }

        }

        return NULL;

    }

    /**
     * @param string|NULL $profile_item_ID
     * @param string|NULL $name
     * @param int|NULL $age
     * @param string|NULL $city
     * @param int|NULL $height
     * @param int|NULL $boobs
     * @param int|NULL $price
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile_gallery_list(string $profile_item_ID=NULL){

        $profile_link=self::$host.'/'.self::$url_profile.'/'.$profile_item_ID.'/galleries';

        if(self::isset_face_link($profile_link)){

            self::add_to_log("WARNING: Already exists".$profile_link." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        self::add_to_log("Set profile link: ".$profile_link." ".Date::get_date_time_full()."\n");

        $r=CurlPost::init($profile_link,[],[],false,self::$cookie_path,false,NULL,NULL,NULL,self::$header_list);

        if($r['status']==504){

            self::add_to_log("Cloudflare restart profile link: ".$profile_link." ".Date::get_date_time_full()."\n");

            self::sleep_random();

            self::reset_connect();

            return self::prepare_profile_gallery_list($profile_item_ID);

        }

        self::add_to_log("Got profile link status: ".$r['status']." ".Date::get_date_time_full()."\n");

        if($r['status']!=200){

            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            self::add_to_log("ERROR: ".$r['status']." ".Date::get_date_time_full()."\n");

            self::sleep_random();

            self::reset_connect();

            return self::prepare_profile_gallery_list($profile_item_ID);

        }

        $r=$r['data'];

        if($r!==false){

            $body=preg_match_all('/<body.*?>(.*?)<\/body>/is',$r,$body_list);

            if($body===false){

                self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                self::add_to_log("ERROR body is empty ".Date::get_date_time_full()."\n");

            }

            if($body!==false)
                if(count($body_list[1])>0){

                    $image_list=[];

                    $temp_list=self::prepare_profile_image_list_from_dashboard($profile_item_ID);

                    if(!empty($temp_list))
                        $image_list=array_merge($image_list,$temp_list);

                    preg_match_all('/<div\s*class=\"galleryHolder\"\s*>.*?<a\s*href=\"(.*?)\"\s*title=\"Смотреть\s*галерею\"\s*>.*?<span\s*class=\"galleryCounter\"\s*>\s*Фото\:\s*(.*?)\s*<\/span>.*?<span\s*class=\"galleryPriceText\"\s*>(.*?)<\/span>/is',$body_list[1][0],$list);

                    foreach($list[1] as $index=>$link)
                        if($list[3][$index]=='Видимая для всех')
                            if((int)$list[2][$index]>0){

                                $link_list      =mb_split('\/',$link);
                                $gallery_ID     =end($link_list);

                                $temp_list      =self::get_profile_galley_image_list($gallery_ID);

                                if(!empty($temp_list))
                                    $image_list=array_merge($image_list,$temp_list);

                            }

                    return $image_list;

                }

        }

        return[];

    }

    /**
     * @param string|NULL $profile_item_ID
     * @param string|NULL $name
     * @param int|NULL $age
     * @param string|NULL $city
     * @param int|NULL $height
     * @param int|NULL $boobs
     * @param int|NULL $price
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_profile_image_list_from_dashboard(string $profile_item_ID=NULL){

        $profile_link=self::$host.'/'.self::$url_profile.'/'.$profile_item_ID;

//        $profile_link='https://www.portaldosug.cz/ru/p/ajax/profile/46756060/getwall/page/2';

        if(self::isset_face_link($profile_link)){

            self::add_to_log("WARNING: Already exists".$profile_link." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        self::add_to_log("Set profile link: ".$profile_link." ".Date::get_date_time_full()."\n");

        $r=CurlPost::init($profile_link,[],[],false,self::$cookie_path,false,NULL,NULL,NULL,self::$header_list);

        if($r['status']==504){

            self::add_to_log("Cloudflare restart profile link: ".$profile_link." ".Date::get_date_time_full()."\n");

            self::sleep_random();

            self::reset_connect();

            return self::prepare_profile_image_list_from_dashboard($profile_item_ID);

        }

        self::add_to_log("Got profile link status: ".$r['status']." ".Date::get_date_time_full()."\n");

        if($r['status']!=200){

            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            self::add_to_log("ERROR: ".$r['status']." ".Date::get_date_time_full()."\n");

            self::sleep_random();

            self::reset_connect();

            return self::prepare_profile_image_list_from_dashboard($profile_item_ID);

        }

        $r=$r['data'];

        if($r!==false){

            $body=preg_match_all('/<body.*?>(.*?)<\/body>/is',$r,$body_list);

            if($body===false){

                self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                self::add_to_log("ERROR body is empty ".Date::get_date_time_full()."\n");

            }

            if($body!==false)
                if(count($body_list[1])>0){

                    $image_list         =[];
                    $temp_image_list    =[];

                    $preg_list=[
                        '/',
                        '<div\s*class=\"wallHeadTitle\"\s*>\s*',
                        '<p>\s*<a.*?>.*?<\/a>\s*(?:добавляет.*?|\s*приняла\s*участие\s*в\s*конкурсе|\s*)\s*<\/p>\s*',
                        '<\/div>\s*',
                        '<p\s*class="wallHeadDate">(.*?)<div\s*class=\"(?:generalDataContainer|photoCaptionHolder)\"\s*',
                        '/is',
                    ];

                    preg_match_all(implode('',$preg_list),$body_list[1][0],$list);

                    self::add_to_log(print_r($list,true));

                    if(count($list[1])!=0)
                        $temp_image_list=array_merge($temp_image_list,$list[1]);

                    if(count($temp_image_list)==0)
                        return[];

                    //<img latesrc="https://www.portaldosug.cz/image/167878732134958033519281696705969021939" class="shadowBox lateload"

                    $preg_list=[
                        '/',
                        '<img.*?(?:late|\s*)src=\"(https\:\/\/www\.portaldosug\.cz\/image\/.*?)\".*?>',
                        '/is',
                    ];

                    foreach($temp_image_list as $row){

                        preg_match_all(implode('',$preg_list),$row,$temp_list);

                        foreach($temp_list[1] as $index=>$link)
                            if(array_search($link,$image_list)===false)
                                $image_list[]=$link;

                    }

                    return $image_list;

                }

        }

        return[];

    }

    /**
     * @param string|NULL $gallery_ID
     * @return array
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_profile_galley_image_list(string $gallery_ID=NULL){

        // https://www.portaldosug.cz/ru/p/ajax/gallery/item/270671676

        $profile_link=self::$host.'/ru/p/gallery/'.$gallery_ID;

        $r=CurlPost::init($profile_link,[],[],false,self::$cookie_path,false,NULL,NULL,NULL,self::$header_list);

        $r=$r['data'];

        preg_match_all('/<a.*?onclick=\"openGalleryWindow\((\d+)\,\s*1\)\;\"\s*class=\"galleryPreviewLink\"\s*title=\"Смотреть\"\s*>\s*<img.*?>\s*<\/a>/is',$r,$list);

        if(count($list[1])==0)
            return[];

        $profile_link=self::$host.'/ru/p/gallery/item/'.$list[1][0];

        self::add_to_log("Set profile link: ".$profile_link." ".Date::get_date_time_full()."\n");

//        $header_list=[
//            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
//            'Accept-Encoding: gzip, deflate, br',
//            'Accept-Language: en,ru;q=0.9',
//            'Cache-Control: max-age=0',
//            'Connection: keep-alive',
//            'Host: www.portaldosug.cz',
//            'Upgrade-Insecure-Requests: 1',
//            'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36',
//        ];

        $r=CurlPost::init($profile_link,[],[],false,self::$cookie_path,false,NULL,NULL,NULL,self::$header_list);
//        $r=CurlGet::init($profile_link,[],[],false,self::$cookie_path,false,NULL,NULL,NULL);

        if($r['status']==504){

            self::add_to_log("Cloudflare restart profile link: ".$profile_link." ".Date::get_date_time_full()."\n");

            self::sleep_random();

            self::reset_connect();

            return self::get_profile_galley_image_list($profile_link);

        }

        self::add_to_log("Got profile link status: ".$r['status']." ".Date::get_date_time_full()."\n");

        if($r['status']!=200){

            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            self::add_to_log("ERROR: ".$r['status']." ".Date::get_date_time_full()."\n");

            self::sleep_random();

            self::reset_connect();

            return self::get_profile_galley_image_list($profile_link);

        }

        $r=$r['data'];

        if($r!==false){

            $image_list=[];

            // var initArGalleryItems = jQuery.parseJSON('{"result":"success","arGalleryItems":[{"previewIdImage":"181924726304040641841954901929444956834","idGalleryItem":270671676,"isImage":true,"image":{"imageThumb":"https://www.portaldosug.cz/image/181924726304040641841954901929444956834","imageBig":"https://www.portaldosug.cz/image/181924726304039432916135287300270250658"},"isOwner":false,"isModerated":1,"isVideo":false,"idGallery":36088186},{"previewIdImage":"181933488605416134994748630580723357605","idGalleryItem":270671196,"isImage":true,"image":{"imageThumb":"https://www.portaldosug.cz/image/181933488605416134994748630580723357605","imageBig":"https://www.portaldosug.cz/image/181933488605414926068929015951548651429"},"isOwner":false,"isModerated":1,"isVideo":false,"idGallery":36088186},{"previewIdImage":"181933489430023247347636990814141997852","idGalleryItem":270671216,"isImage":true,"image":{"imageThumb":"https://www.portaldosug.cz/image/181933489430023247347636990814141997852","imageBig":"https://www.portaldosug.cz/image/181933489430022038421817376184967291676"},"isOwner":false,"isModerated":1,"isVideo":false,"idGallery":36088186},{"previewIdImage":"181933489513546634053644277258606226660","idGalleryItem":270671226,"isImage":true,"image":{"imageThumb":"https://www.portaldosug.cz/image/181933489513546634053644277258606226660","imageBig":"https://www.portaldosug.cz/image/181933489513545425127824662629431520484"},"isOwner":false,"isModerated":1,"isVideo":false,"idGallery":36088186},{"previewIdImage":"181933139178293846282360064764700971608","idGalleryItem":270671336,"isImage":true,"image":{"imageThumb":"https://www.portaldosug.cz/image/181933139178293846282360064764700971608","imageBig":"https://www.portaldosug.cz/image/181933139178292637356540450135526265432"},"isOwner":false,"isModerated":1,"isVideo":false,"idGallery":36088186}],"rightRowsCount":0,"leftRowsCount":0,"idGalleryItemIndex":0}');

            self::add_to_log($r);

            preg_match_all('/var\s*initArGalleryItems\s*=\s*jQuery\.parseJSON\((.*?)\)\s*\;/is',$r,$list);

            if(count($list[1])>0){

                $r=mb_substr($list[1][0],2);
                $r=mb_substr($r,0,-2);
                $r=stripslashes($r);

                $r=Json::decode($r);

                self::add_to_log(print_r($r,true));

                if(count($r['arGalleryItems'])>0)
                    foreach($r['arGalleryItems'] as $row)
                        $image_list[]=$row['image']['imageBig'];

            }

            return $image_list;

        }

        return[];

    }

    /**
     * @param array $image_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_image_list(array $image_list=[]){

        if(count($image_list)==0)
            return[];

        $list=[];

        foreach($image_list as $link){

            $file_data=self::prepare_file($link);

            if(!empty($file_data)){

                $file_ID                            =$file_data['file_ID'];
                $file_extension                     =$file_data['file_extension'];
                $image_data                         =self::prepare_image($file_ID,'image',$file_extension);
                $image_path                         =$image_data['image_dir'].'/'.$image_data['image_item_ID_list']['large'];
                $image_data['file_path']            =$image_path;
                $image_data['file_size']            =filesize($image_path);
                $image_data['file_content_type']    ='image';
                $image_data['file_extension']       =$file_extension;
                $image_data['file_mime_type']       =File::get_file_mime_type_from_path($image_path);
                $image_data['image_resolution']     =Image::get_image_pixel_size_from_file_path($image_path);

                $key            =$image_data['image_resolution']['width'].':'.$image_data['image_resolution']['height'].':'.$image_data['file_size'].':'.$image_data['file_mime_type'];
                $list[$key]     =$image_data;

            }

        }

        return array_values($list);

    }

    /**
     * @param array|NULL $profile_data
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_profile(array $profile_data=NULL){

        if(empty($profile_data))
            return NULL;

        if(count($profile_data['image_list'])==0)
            return NULL;

        $profile_data['image_list']=self::get_image_list($profile_data['image_list']);

        if(count($profile_data['image_list'])==0){

            self::add_to_log("Image list is empty ".Date::get_date_time_full()."\n");

            return NULL;

        }

        self::add_to_log("Start create face data ".Date::get_date_time_full()."\n");

        $image_list         =self::search_face_in_image_list($profile_data['image_list']);
        $face_data          =self::prepare_face($profile_data['link']);
        $face_ID            =$face_data['face_ID'];
        $face_data          =self::prepare_face_data($face_ID,NULL,$profile_data['name'],$profile_data['link'],count($profile_data['image_list']),$profile_data['age'],$profile_data['city'],implode("\n",$profile_data['info']),self::$face_data_type_ID);
        $face_data_ID       =$face_data['face_data_ID'];
        $image_data         =self::upload_image_to_facekit_db($face_ID,$image_list);

        self::update_face_data_image($face_ID,$image_data['image_ID']);
        self::update_face($face_ID);
        self::update_face_data($face_data_ID);

        self::add_to_log("FINISH create face data ".Date::get_date_time_full()."\n");

        return $profile_data;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function reset_connect(){

        return true;

        self::add_to_log("Reset connect to server ".Date::get_date_time_full()."\n");
        self::add_to_log("Clean cookie ".Date::get_date_time_full()."\n");

        file_put_contents(self::$cookie_path,'');

        self::add_to_log("get default url ".Date::get_date_time_full()."\n");

        $r=CurlPost::init('http://whores777.com/',[],[],false,self::$cookie_path,false,NULL,NULL,NULL,self::$header_list);

        self::add_to_log("content: ".print_r($r,true)." ".Date::get_date_time_full()."\n");
        self::add_to_log("get redirect url ".Date::get_date_time_full()."\n");

        $r=CurlPost::init('http://b.whores777.com',[],[],false,self::$cookie_path,false,NULL,NULL,NULL,self::$header_list);

        self::add_to_log("continue ".Date::get_date_time_full()."\n");
        self::add_to_log("content: ".print_r($r,true)." ".Date::get_date_time_full()."\n");

        return true;


    }

    /**
     * @param string|NULL $body
     * @return bool
     */
    private static function set_csrf_token(string $body=NULL){

        preg_match_all('/<meta\s*name=\"\_csrf\"\s*content=\"(.*?)\"\s*\/\s*>/is',$body,$param_list);

        if(count($param_list[1])>0)
            self::$csrf_token=$param_list[1][0];

        return true;

    }

    /**
     * @param string|NULL $search_link
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_search_link(string $search_link=NULL){

        self::add_to_log("set search firm list: ".$search_link." ".Date::get_date_time_full()."\n");

        self::reset_connect();

        $url_referer=self::$host.'/'.self::$url_rating;

        $cookie_list=[
            'dsession='.self::$csrf_token
        ];

        $r=CurlPostParsing::init($search_link,[],[],false,self::$cookie_path,false,NULL,NULL,NULL,self::$header_list,$cookie_list);

        echo 'Status: '.$r['status']."\n";

//        exit;

        self::add_to_log(print_r($r,true));

//        echo 'search_link: '.$search_link."\n";exit;
//        echo 'Isset redirect: '.($r['isset_redirect']?'true':'false')."\n";
//        echo 'URL redirect: '.$r['url_redirect']."\n";
//        exit;

        self::add_to_log("got DOM from link: ".$search_link." ".Date::get_date_time_full()."\n");

        if($r['status']==504){

            self::add_to_log("Cloudflare restart: ".$search_link." ".Date::get_date_time_full()."\n");

            self::sleep_random();

            self::reset_connect();

            return self::get_search_link($search_link);

        }

        if($r['status']!=200){

            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            self::add_to_log("ERROR ".$search_link." ".Date::get_date_time_full()."\n");
            self::add_to_log("Header error: ".$r['status']." ".$search_link." ".Date::get_date_time_full()."\n");
            self::add_to_log("Content: ".$r['data']." ".$search_link." ".Date::get_date_time_full()."\n");

//            self::sleep_random();

            sleep(11);

            self::reset_connect();

            return self::get_search_link($search_link);

        }

        $r=$r['data'];

        $r=file_get_contents($search_link);

        self::add_to_log($r);

        if($r!==false){

            self::set_csrf_token($r);

            $body=preg_match_all('/<div\s*class=\"generalDataContainer\".*?>(.*?)$/is',$r,$body_list);

            if($body===false){

                echo"\n\n";
                echo'BODY IS EMPTY';
                echo"\n\n";
                exit;

                self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                self::add_to_log("ERROR: have not body ".$search_link." ".Date::get_date_time_full()."\n");

                self::sleep_random();

                return self::get_search_link($search_link);

            }

            if($body!==false)
                if(count($body_list[0])>0){
                    
                    $preg_list          ='/<div\s*class=\"photoVideoHolder\".*?>\s*<a\s*href=\"(.*?)\".*?>\s*<span\s*class=\"friendTextStatusInNick\"\s*>(.*?)<\/span>/is';
                    $profile_list_r     =preg_match_all($preg_list,$body_list[1][0],$profile_item_list);

                    if($profile_list_r===false){

                        self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        self::add_to_log("ERROR: have not DOM element for ".$search_link." ".Date::get_date_time_full()."\n");

                        $error=[
                            'title'     =>ParametersValidationException::$title,
                            'info'      =>'have not DOM element with ID W0'
                        ];

                        throw new ParametersValidationException($error);

                    }

                    if($profile_list_r!==false){

                        if(count($profile_item_list[0])==0){

                            self::add_to_log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                            self::add_to_log("ERROR: Profile list is empty ".$search_link." ".Date::get_date_time_full()."\n");

                            self::sleep_random();

                            self::login();

                            return self::get_search_link($search_link);

                        }

                        self::add_to_log("Have profile list count: ".count($profile_item_list[1])." ".Date::get_date_time_full()."\n");

                        print_r($profile_item_list[1]);
                        exit;

                        foreach($profile_item_list[1] as $profile_index=>$link){

//                            $link='https://www.portaldosug.cz/p/profile/46756060';
//                            $link='https://www.portaldosug.cz/ru/p/profile/2584714';
//                            $link='https://www.portaldosug.cz/ru/p/profile/2615324';
//                            $link='https://www.portaldosug.cz/ru/p/profile/202977470';

                            if(self::isset_face_link($link)){

                                self::add_to_log("WARNING: Already exists".$link." ".Date::get_date_time_full()."\n");

                            }
                            else{

                                self::prepare_profile_item_ID($link);

                                self::add_to_log('Done: '.$link."\n\n\n");

                                self::sleep_random();

                            }

                        }

                    }

                }

        }

        return true;

    }

    /**
     * @param string|NULL $link
     * @param int|NULL $profile_index
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_profile_item_ID(string $link=NULL,int $profile_index=NULL){

        $link                       =str_replace('&amp;','&',$link);
        $link_list                  =mb_split('\/',$link);
        $profile_item_ID            =end($link_list);

        self::add_to_log("Start prepare profile item with index: ".$profile_index." ".Date::get_date_time_full()."\n");

        $profile_link=self::$host.'/'.self::$url_profile.'/'.$profile_item_ID;

        if(self::isset_face_link($profile_link)){

            self::add_to_log("WARNING: Already exists".$profile_link." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        $profile_data=self::prepare_profile_link($profile_item_ID);

        if(empty($profile_data)){

            self::add_to_log("Empty profile data with index: ".$profile_index." ".Date::get_date_time_full()."\n");

            return NULL;

        }

        self::add_to_log("Start prepare profile data with index: ".$profile_index." ".Date::get_date_time_full()."\n");

        $r=self::prepare_profile($profile_data);

        self::add_to_log("Finish prepare profile data: ".$profile_index." ".Date::get_date_time_full()."\n");

        self::$profile_index++;

        return $r;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function get_profile_links(){

        libxml_use_internal_errors(true);

//        do{
//
//            self::$profile_index=0;
//
//            $search_link=self::$host.'/'.self::$url_rating.'/'.self::$page;
//
//            self::get_search_link($search_link);
//
//            self::sleep_random();
//
//            self::$page++;
//
//        }while(true);

        $n=0;

        foreach(DosugLoginProfileConfig::$profile_list as $profile_ID){

//            $profile_ID=152116105;

            $link=self::$host.'/ru/p/profile/'.$profile_ID;

            self::add_to_log("Get link: ".$link." ".Date::get_date_time_full()."\n");

            if(self::isset_face_link($link)){

                self::add_to_log("WARNING: Already exists".$link." ".Date::get_date_time_full()."\n");

            }
            else{

                $n++;

                self::login();

                $r=self::prepare_profile_item_ID($link);

                self::add_to_log('Done: '.$link."\n\n\n");

//                if(!empty($r))
                self::sleep_random();

            }

        }

        return true;

    }

    /**
     * @return mixed
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function login(){

        self::add_to_log("Start LOGIN\n");

        if(file_exists(self::$cookie_path))
            unlink(self::$cookie_path);

        $url=self::$host.'/'.self::$url_login;

//        $r=CurlGet::init($url,[],self::$header,false,[],[],self::$cookie_path);
        $r=CurlPostParsing::init($url,[],[],false,self::$cookie_path,false,NULL,NULL,NULL,self::$header_list);

        self::set_csrf_token($r['data']);

        $url=self::$host.'/'.self::$url_login;

        $post_list=[
            'login'         =>'z88xcvbnm2',
            'fP'            =>self::$fp_hash,
            'password'      =>'123456',
            '_rememberMe'   =>'on',
            'rememberMe'    =>'on',
            'back'          =>'',
            'modal'         =>''
        ];

        $r=CurlGet::init($url,[],self::$header,false,$post_list,[],self::$cookie_path);

        self::set_csrf_token($r['data']);

        echo $r['data'];

        $url=self::$host.'/ru/p';

//        $r=CurlGet::init($url,[],self::$header,false,$post_list,[],self::$cookie_path);

        $r=CurlPost::init($url,[],[],false,self::$cookie_path,false,NULL,NULL,$url,self::$header_list);

        self::set_csrf_token($r['data']);

        self::add_to_log("FINISH LOGIN\n\n");

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        self::set_log_path();
        self::set_cookie_path();
        self::set_face_data_type();
//        self::login();
        self::get_profile_links();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        \Config::$is_debug=true;

        if(empty($_POST['page'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Page is empty'
            ];

            throw new ParametersException($error);

        }

        self::$page         =(int)$_POST['page'];
        self::$host_index   =empty($_POST['host_index'])?NULL:(int)$_POST['host_index'];

        return self::set();

    }

}

//login: z88xcvbnm
//fP: 7b45ae15eef893be882fc55bc27ac67b3f9da2ded67b2ddd033003e7ee623a6c
//password: domino000
//_rememberMe: on
//rememberMe: on
//back:
//modal:


//Cookie: entryPoint=ssl:www.portaldosug.cz;
// BSID=server.dosug2.nlz1;
// dosugDomain=urapib.newdosug.eu;
// JSESSIONID=aaamNeHa-a9Dn_v1E7UWw;
// _ga=GA1.2.1810996744.1564155443;
// reme=ejg4eGN2Ym5tOjE1NjY3NDc0NDg5NzE6MDFhOWZhNjg1MTRkMTBkMjNhZjhiZDE5OTQ3ZDBkYjE;
// view_mode=NORMAL;
// dsession=9f59b535-7131-4091-a465-fa276e572da7;
// lang=ru;
// allol=mjaLk2CCaqivJ%2Brfh7bJK12DGR6QZAnABsYYsSKkbxM%3D;
// _gid=GA1.2.1446279540.1564586988;
// _gat=1