<?php

namespace Project\Whore\Admin\Shell\Source;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Geo\City;
use Core\Module\Geo\CityLocalization;
use Core\Module\Geo\CountryLocalization;
use Core\Module\Lang\LangConfig;
use Project\Whore\All\Module\Face\FaceCity;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Info\Info;

class UpdateFaceCityStatsShell{

    // sudo -u www-data /usr/bin/php /var/www/public/index.php "action=update_face_city_stats"

    /** @var array */
    private static $city_list               =[];

    /** @var int */
    private static $timeout                 =43200;

    /** @var int */
    private static $face_city_add_len       =0;

    /** @var int */
    private static $face_city_update_len    =0;

    /** @var int */
    private static $face_len                =0;

    /** @var int */
    private static $image_len               =0;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$city_list                =[];
        self::$face_city_update_len     =0;
        self::$face_city_add_len        =0;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_len(){

        $r=FaceData::get_face_stats_len();

        if(empty($r))
            return false;

        self::$face_len     =$r['face_len'];
        self::$image_len    =$r['image_len'];

        Info::update_info_data('info','face_len',self::$face_len);
        Info::update_info_data('info','image_len',self::$image_len);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_city_list(){

        self::$city_list=FaceData::get_face_city_stats_list();

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_city_list(){

        foreach(self::$city_list as $city_ID=>$row){

            if(!empty($city_ID)){

                if(!FaceCity::isset_face_city($city_ID)){

                    $country_ID         =City::get_country_ID($city_ID);

                    if(!empty($country_ID)){

                        $country_name       =CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);
                        $city_name          =CityLocalization::get_city_name($city_ID,LangConfig::$lang_ID_default);

                        FaceCity::add_face_city($country_ID,$city_ID,$country_name,$city_name,$row['face_len'],$row['image_len']);

                        self::$face_city_add_len++;

                        echo"Added city_ID: ".$city_ID." name: ".$city_name."\n";

                    }

                }
                else{

                    $city_name=CityLocalization::get_city_name($city_ID,LangConfig::$lang_ID_default);

                    FaceCity::update_face_len($city_ID,$row['face_len'],$row['image_len']);

                    self::$face_city_update_len++;

                    echo"Updated city_ID: ".$city_ID." name: ".$city_name."\n";

                }

            }

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_return(){

        $data=[
            'success'=>true,
            'data'=>[
                'added_city'    =>self::$face_city_add_len,
                'updated_city'  =>self::$face_city_update_len
            ]
        ];

        print_r($data);

        echo"\n\n";

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        do{

            self::reset_data();
            self::set_face_len();
            self::set_city_list();
            self::prepare_city_list();

            self::set_return();

            sleep(self::$timeout);

        }while(true);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}