<?php

namespace Project\Whore\Admin\Shell\Source;

use Core\Module\Curl\CurlPost;
use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Dir\Dir;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\ShluhamNetException;
use Core\Module\File\File;
use Core\Module\File\FileParametersCash;
use Core\Module\Image\Image;
use Core\Module\Image\ImageItem;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\User;
use Core\Module\Video\Video;
use Project\Whore\All\Module\Dir\DirConfigProject;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Face\FaceTrig;
use Project\Whore\All\Module\Face\FaceTrigItem;
use Project\Whore\All\Module\Face\FaceVideo;
use Project\Whore\All\Module\FacePlusPlus\FacePlusPlusConfig;
use Project\Whore\All\Module\Source\Source;

class TrigDuplicateFaceLocalToLocalShell{

    // sudo -u www-data /usr/bin/php /var/www/public/index.php "action=trig_duplicate_face" "page=1" "need_log=1" "need_tor=0" "is_test=0"

    /** @var int */
    private static $source_ID                               =1;

    /** @var int */
    private static $page                                    =1;

    /** @var array */
    private static $profile_list                            =[];

    /** @var string */
    private static $dir_path                                ='Temp/TrigDuplicateFace';

    /** @var string */
    private static $cookie_path                             ='Temp/Parsing/ModelZoneVideo/reverse_ip.txt';

    /** @var string */
    public  static $file_log_path                           ='';

    /** @var int  */
    private static $memory_usage                            =0;

    /** @var int */
    private static $file_log_size_max                       =10485760;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_log_path(){

        self::$dir_path=DIR_ROOT.'/'.self::$dir_path;

        if(!file_exists(self::$dir_path))
            Dir::create_dir(self::$dir_path);

        self::$file_log_path=DIR_ROOT.'/'.DirConfigProject::$dir_parsing_log;

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/ModelZoneVideo';

        if(!file_exists(self::$file_log_path))
            Dir::create_dir(self::$file_log_path);

        self::$file_log_path.='/model_zone_video_'.time().'.log';

//        self::add_to_log('Created log path');

        return true;

    }

    /**
     * @param $data
     * @return bool|int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_to_log($data){

        $memory         =round((memory_get_usage()/(1024*1024)),3);
        $memory_peak    =round((memory_get_peak_usage()/(1024*1024)),3);

        if($memory>self::$memory_usage){

            $old_memory             =self::$memory_usage;
            self::$memory_usage     =$memory;

            self::add_to_log('############## -> Memory usage update from '.$old_memory.' to '.self::$memory_usage.'/'.$memory_peak.'mb'."\n");

        }

        echo print_r($data,true);

        if(file_exists(self::$file_log_path))
            if(filesize(self::$file_log_path)>self::$file_log_size_max)
                self::set_log_path();

        if(!empty($_POST['need_log']))
            return file_put_contents(self::$file_log_path,$data,FILE_APPEND);

        return false;

    }

    /**
     * @return bool
     */
    private static function set_cookie_path(){

        self::$cookie_path=DIR_ROOT.'/'.self::$cookie_path;

        return true;

    }

    /**
     * @param int|NULL $offset
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_profile_list(int $offset=NULL){

        $q=[
            'select'=>[
                'face_id'
            ],
            'table'=>FaceData::$table_name,
            'where'=>[
                [
                    'column'    =>'is_check_duplicate_face',
                    'value'     =>0
                ],
                [
                    'column'    =>'type',
                    'value'     =>0
                ]
            ],
            'order'=>[
                [
                    'column'    =>'face_id',
                    'direction' =>'asc'
                ]
            ],
            'limit'=>[$offset,100]
        ];

        if(!empty($_POST['source_ID']))
            $q['where'][]=[
                'column'    =>'source_id',
                'value'     =>(int)$_POST['source_ID']
            ];

        if(!empty($_POST['face_ID_min']))
            $q['where'][]=[
                'column'    =>'face_id',
                'method'    =>'<=',
                'value'     =>(int)$_POST['face_ID_min']
            ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'face_ID'=>$row['face_id']
            ];

        return $list;

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_image(int $face_ID=NULL){

        $image_ID_list=FaceImage::get_image_ID_list($face_ID);

        if(count($image_ID_list)==0)
            return false;

        $image_item_list=ImageItem::get_image_item_hash_list($image_ID_list,'small');

        if(count($image_item_list)==0)
            return false;

        $image_hash_list=[];

        foreach($image_item_list as $row)
            $image_hash_list[]=$row['hash'];

        if(count($image_hash_list)==0)
            return false;

        $duplicate_image_ID_list=ImageItem::get_duplicate_image_ID_list($image_hash_list);

        if(count($duplicate_image_ID_list)==0)
            return false;

        $face_ID_list=FaceImage::get_face_ID_list_from_image_ID_list($duplicate_image_ID_list);

        if(count($face_ID_list)==0)
            return false;

        $face_data_list=FaceData::get_face_data_list_from_face_ID_list($face_ID_list);

        if(count($face_data_list)==0)
            return false;

        foreach($face_data_list as $face_ID=>$row){

            $face_trig_ID=FaceTrigItem::get_face_trig_ID($face_ID);

            if(!empty($face_trig_ID))
                break;

        }

        if(empty($face_trig_ID))
            $face_trig_ID=FaceTrig::add_face_trig(0,0,0);

        foreach($face_data_list as $face_ID=>$row){

            if(!FaceTrigItem::isset_face_trig($face_ID)){

                FaceTrigItem::add_face_trig_item($face_trig_ID,$face_ID,$row['image_len'],$row['video_len']);

                FaceTrig::update_face_trig_image_len($face_trig_ID);

            }

        }

        return true;

    }

    /**
     * @param int|NULL $face_ID
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_duplicate(int $face_ID=NULL){

        $image_ID_list=FaceImage::get_image_ID_list($face_ID);

        if(count($image_ID_list)==0)
            return NULL;

        $image_item_list=ImageItem::get_image_item_hash_list($image_ID_list,'small');

        if(count($image_item_list)==0)
            return NULL;

        $list=[];

        $image_len=0;

        foreach($image_item_list as $row){

            if(!isset($list[$row['hash']]))
                $list[$row['hash']]=[];

            $list[$row['hash']][]=$row['image_ID'];

            if(count($list[$row['hash']])>1)
                $image_len++;

        }

        if($image_len>0)
            return $image_len;

        return NULL;

//        if(count($list)==0)
//            return true;
//
//        $remove_image_ID_list=[];
//
//        foreach($list as $hash=>$row)
//            if(count($row)>1){
//
//                $temp_list=array_slice($row,1);
//
//                $remove_image_ID_list=array_merge($remove_image_ID_list,$temp_list);
//
//            }
//
//        if(count($remove_image_ID_list)==0)
//            return true;
//
//        FaceImage::remove_face_image_from_image_ID_list($face_ID,$remove_image_ID_list);
//        Image::remove_image_ID_list($remove_image_ID_list);
//
//        $image_ID_list=FaceImage::get_image_ID_list($face_ID);
//
//        if(count($image_ID_list)==0)
//            return true;
//
//        $image_ID=$image_ID_list[0];
//
//        FaceData::update_face_data_image_ID($face_ID,$image_ID);
//
//        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_profile_links(){

        libxml_use_internal_errors(true);

        $offset=0;
        $image_all_len=0;

        do{

            self::$profile_list=self::get_profile_list($offset);

            if(count(self::$profile_list)==0){

                echo"\n\n";
                echo 'Duplicate count: '.$image_all_len."\n\n";

                self::add_to_log("\n\n\nDONE");

                return true;

            }

            foreach(self::$profile_list as $row){

                self::add_to_log("Start prepare face_ID: ".$row['face_ID']."\n");

                self::prepare_image($row['face_ID']);

                FaceData::update_face_data_check_duplicate_face($row['face_ID']);

                self::add_to_log("Finish prepare face_ID: ".$row['face_ID']."\n\n");

            }

            $offset+=100;

        }while(true);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        self::set_log_path();
        self::get_profile_links();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        if(empty($_POST['page'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Page is empty'
            ];

            throw new ParametersException($error);

        }

        self::$page=(int)$_POST['page'];

        return self::set();

    }

}