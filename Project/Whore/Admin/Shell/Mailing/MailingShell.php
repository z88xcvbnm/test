<?php

namespace Project\Whore\Admin\Shell\Mailing;

use Core\Module\Email\EmailSend;
use Core\Module\Email\EmailValidation;
use Core\Module\Exception\ParametersException;
use Core\Module\Response\ResponseSuccess;
use Core\Module\User\UserEmail;
use Core\Module\User\UserLogin;
use Project\Whore\All\Module\Mailing\Mailing;
use Project\Whore\All\Module\Mailing\MailingItem;

class MailingShell{

    // sudo -u www-data /usr/bin/php /var/www/public/index.php "action=mailing" "mailing_ID=1"

    /** @var int */
    private static $mailing_ID;

    /** @var string */
    private static $title;

    /** @var string */
    private static $message;

    /** @var int */
    private static $start           =0;

    /** @var int */
    private static $len             =100;

    /** @var int */
    private static $timeout_count   =100;

    /** @var int */
    private static $timeout_send    =3600;

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_sent_to_user(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is not exists'
            ];

            throw new ParametersException($error);

        }

        return MailingItem::isset_user_ID($user_ID,self::$mailing_ID);

    }

    /**
     * @param int $start
     * @param int $len
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_email_list(int $start=0,int $len=100){

        return UserEmail::get_user_email_list_for_mailing($start,$len);

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_to_mailing_item(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is not exists'
            ];

            throw new ParametersException($error);

        }

        MailingItem::add_mailing_item($user_ID,self::$mailing_ID);

        return true;

    }

    /**
     * @param string|NULL $email
     * @param string|NULL $login
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send(string $email=NULL,string $login=NULL){

        echo 'Email: "'.$email.'"'."\n";

        if(empty($email))
            return false;

        if(empty($login))
            return false;

        if(!EmailValidation::is_valid_email($email))
            return false;

        $title      =self::$title;
        $message    =str_replace('%login%',$login,self::$message);
        $message    =str_replace("\n","<br />\n",$message);

        return EmailSend::init([$email],$title,$message,\Config::$email_no_replay);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_message(){

        $r=Mailing::get_data(self::$mailing_ID);

        if(empty($r)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Mailing ID is not exists'
            ];

            throw new ParametersException($error);

        }

        self::$title        =$r['title'];
        self::$message      =$r['message'];

        return true;

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare(){

        Mailing::update_user_all_len(self::$mailing_ID);

        $n=0;

        do{

            echo"Start from ".self::$start."\n";

            $email_list     =self::get_email_list(self::$start,self::$len);
            $add_count      =0;

            if(count($email_list)==0){

                $user_len_data=Mailing::get_user_len(self::$mailing_ID);

                echo"\n";
                echo"\n";
                echo"DONE\n";
                echo"Added ".$user_len_data['user_send_len']."/".$user_len_data['user_all_len']."\n";

                return true;

            }

            foreach($email_list as $row){

                $user_ID=$row['user_ID'];
//                $user_ID=1;

                if(self::is_sent_to_user($user_ID))
                    echo"Already sent to user ID: ".$user_ID."\n";
                else{

                    $email          =$row['email'];
//                    $email          ='fenicks88@gmail.com';
                    $login          =UserLogin::get_user_login($user_ID);

                    self::send($email,$login);
                    self::add_to_mailing_item($user_ID);

                    Mailing::user_send_len_plus(self::$mailing_ID);

                    $user_len_data=Mailing::get_user_len(self::$mailing_ID);

                    echo"Sent to user ID: ".$user_ID."; ".$user_len_data['user_send_len']."/".$user_len_data['user_all_len']."\n";

                    $add_count      ++;
                    $n              ++;

                    if($n>=self::$timeout_count){

                        echo"\n";
                        echo"Sleep timeout: ".self::$timeout_send."\n";
                        echo"\n";

                        $n=0;

                        sleep(self::$timeout_send);

                    }

                }

            }

            self::$start+=self::$len;

        }while(true);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        return ResponseSuccess::init($data);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_message();
        self::prepare();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        \Config::$is_debug=true;

        if(empty($_POST['mailing_ID'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Mailing ID is empty'
            ];

            throw new ParametersException($error);

        }

        self::$mailing_ID=$_POST['mailing_ID'];

        return self::set();

    }

}