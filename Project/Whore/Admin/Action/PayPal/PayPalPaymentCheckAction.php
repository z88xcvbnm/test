<?php

namespace Project\Whore\Admin\Action\PayPal;

use Core\Module\Exception\ParametersException;
use Core\Module\PayPal\PayPalPaymentCheck;
use Project\Whore\Admin\Action\Payment\AddAutoPaymentAction;

class PayPalPaymentCheckAction{

    /** @var int */
    private static $order_ID;

    /** @var int */
    private static $paypal_payment_ID;

    /** @var string */
    private static $user_wallet_address;

    /** @var string */
    private static $target_wallet_address;

    /** @var float */
    private static $amount;

    /** @var float */
    private static $balance;

    /** @var bool */
    private static $is_sandbox=false;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function paypal_payment_check(){

        $r=PayPalPaymentCheck::init(self::$order_ID,self::$amount,self::$is_sandbox);

        self::$paypal_payment_ID        =$r['paypal_payment_ID'];
        self::$user_wallet_address      =$r['user_wallet_address'];
        self::$target_wallet_address    =$r['target_wallet_address'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_payment(){

        $r=AddAutoPaymentAction::init('pp',self::$user_wallet_address,self::$target_wallet_address,self::$amount,self::$paypal_payment_ID);

        self::$balance=$r['balance'];

        return true;

    }


    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'balance'=>self::$balance
        ];

        return $data;

    }

    /**
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::paypal_payment_check();
        self::prepare_payment();

        return self::set_return();

    }

    /**
     * @param string|NULL $order_ID
     * @param float|NULL $amount
     * @param bool $is_sandbox
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $order_ID=NULL,float $amount=NULL,bool $is_sandbox=false){

        $error_info_list=[];

        if(empty($order_ID))
            $error_info_list[]='Order ID is empty';

        if(empty($amount))
            $error_info_list[]='Amount is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$order_ID     =$order_ID;
        self::$amount       =$amount;
        self::$is_sandbox   =empty($is_sandbox)?false:$is_sandbox;

        return self::set();

    }

}