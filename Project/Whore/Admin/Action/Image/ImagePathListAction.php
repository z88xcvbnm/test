<?php

namespace Project\Whore\Admin\Action\Image;

use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Exception\PhpException;
use Core\Module\Image\Image;
use Core\Module\Image\ImageItem;

class ImagePathListAction{

    /** @var array */
    private static $image_ID_list=[];

    /** @var array */
    private static $image_list=[];

    /** @var bool */
    private static $is_assoc;

    /** @var bool */
    private static $is_file_extension;

    /** @var bool */
    private static $is_server_link;

    /** @var array */
    private static $resolution_list=[];

    /** @var array */
    private static $resolution_all_list=[
        'preview',
        'small',
        'middle',
        'large'
    ];

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$image_list               =[];
        self::$resolution_list          =[];
        self::$is_assoc                 =NULL;
        self::$is_file_extension        =NULL;
        self::$is_server_link           =NULL;

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_list(){

        if(self::$is_server_link){

            $image_dir_list         =Image::get_image_dir_list(self::$image_ID_list);
            $image_item_ID_list     =ImageItem::get_image_item_ID_list_with_size_from_image_ID_list(self::$image_ID_list,true,self::$is_file_extension);

            foreach($image_dir_list as $image_ID=>$image_dir)
                if(isset($image_item_ID_list[$image_ID]))
                    self::$image_list[$image_ID]=array(
                        'image_ID'              =>$image_ID,
                        'image_dir'             =>$image_dir,
                        'image_item_ID_list'    =>$image_item_ID_list[$image_ID]
                    );

            if(!self::$is_assoc)
                self::$image_list=array_values(self::$image_list);

            return true;

        }

        $image_item_ID_list     =[];
        $image_dir_list         =Image::get_image_dir_list(self::$image_ID_list);
//        $image_item_ID_list     =ImageItem::get_image_item_ID_list_with_size_from_image_ID_list(self::$image_ID_list,true,self::$is_file_extension);

        $r=ImageItem::get_image_item_hash_link_resolution_list(self::$image_ID_list,self::$resolution_list);

        foreach($r as $image_ID=>$image_row)
            foreach($image_row as $resolution=>$image_resolution_row){

                $file_path=Dir::get_dir_from_date(DirConfig::$dir_image,$image_resolution_row['date_create']).'/'.$image_resolution_row['image_ID'].'/'.$image_resolution_row['ID'].'.'.$image_resolution_row['file_extension'];

//                if(file_exists($file_path)){

                if(!isset($image_item_ID_list[$image_ID]))
                    $image_item_ID_list[$image_ID]=[];

                $image_item_ID_list[$image_ID][$resolution]=[
                    'ID'        =>$image_resolution_row['hash_link'].'.'.$image_resolution_row['file_extension'],
                    'width'     =>$image_resolution_row['width'],
                    'height'    =>$image_resolution_row['height'],
                    'file_size' =>$image_resolution_row['file_size']
                ];

//                }

        }

        $error_info_list=[];

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new PhpException($error);

        }

        foreach($image_dir_list as $image_ID=>$image_dir)
            if(isset($image_item_ID_list[$image_ID]))
                self::$image_list[$image_ID]=array(
                    'image_ID'              =>$image_ID,
                    'image_dir'             =>DirConfig::$dir_image_hash_show,
                    'image_item_ID_list'    =>$image_item_ID_list[$image_ID]
                );

//        foreach($image_dir_list as $image_ID=>$image_dir)
//            if(isset($image_item_ID_list[$image_ID]))
//                self::$image_list[$image_ID]=array(
//                    'image_ID'              =>$image_ID,
//                    'image_dir'             =>$image_dir,
//                    'image_item_ID_list'    =>$image_item_ID_list[$image_ID]
//                );

        if(!self::$is_assoc)
            self::$image_list=array_values(self::$image_list);

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        return self::$image_list;

    }

    /**
     * @return array
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_image_list();

        return self::set_return();

    }

    /**
     * @param array $image_ID_list
     * @param bool $is_assoc
     * @param bool $is_file_extension
     * @param bool $is_server_link
     * @param array $resolution_list
     * @return array
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_path_list(array $image_ID_list=[],bool $is_assoc=false,bool $is_file_extension=false,bool $is_server_link=false,array $resolution_list=[]){

        if(count($image_ID_list)==0)
            return[];

        self::reset_data();

        self::$image_ID_list        =$image_ID_list;
        self::$resolution_list      =count($resolution_list)==0?self::$resolution_all_list:$resolution_list;
        self::$is_assoc             =$is_assoc;
        self::$is_file_extension    =$is_file_extension;
        self::$is_server_link       =$is_server_link;

        return self::set();

    }

}