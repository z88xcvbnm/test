<?php

namespace Project\Whore\Admin\Action\Image;

use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Exception\ParametersException;
use Core\Module\Image\Image;
use Core\Module\Image\ImageItem;
use Core\Module\Os\OsDetermine;
use Core\Module\OsServer\OsServer;
use Core\Module\Php\PhpValidation;

class ImagePathAction{

    /** @var int */
    private static $image_ID;

    /** @var array */
    private static $image_data;

    /** @var bool */
    private static $is_need_resolution_type;

    /** @var bool */
    private static $is_file_extension;

    /** @var bool */
    private static $is_server_link;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_data(){

        if(self::$is_server_link){

            $image_dir              =Image::get_image_dir_from_image_ID(self::$image_ID);
            $image_item_ID_list     =ImageItem::get_image_item_ID_list_width_size(self::$image_ID,self::$is_need_resolution_type,self::$is_file_extension);

            self::$image_data=array(
                'image_ID'              =>self::$image_ID,
                'image_dir'             =>$image_dir,
                'image_item_ID_list'    =>$image_item_ID_list
            );

            return true;

        }

        $image_item_ID_list     =[];
        $r                      =ImageItem::get_image_item_hash_link_resolution_list([self::$image_ID]);

        foreach($r as $image_ID=>$image_row)
            foreach($image_row as $resolution=>$image_resolution_row){

                $file_path  =Dir::get_dir_from_date(DirConfig::$dir_image,$image_resolution_row['date_create']).'/'.$image_resolution_row['image_ID'].'/'.$image_resolution_row['ID'].'.'.$image_resolution_row['file_extension'];
                $file_path  =Dir::get_global_dir($file_path);

                if(file_exists($file_path)){

                    if(!isset($image_item_ID_list[$image_ID]))
                        $image_item_ID_list[$image_ID]=[];

                    $image_item_ID_list[$image_ID][$resolution]=[
                        'ID'            =>$image_resolution_row['hash_link'].'.'.$image_resolution_row['file_extension'],
                        'width'         =>$image_resolution_row['width'],
                        'height'        =>$image_resolution_row['height'],
                        'file_size'     =>$image_resolution_row['file_size']
                    ];

                }

            }

        if(!empty($image_item_ID_list[self::$image_ID]))
            self::$image_data=array(
                'image_ID'              =>self::$image_ID,
                'image_dir'             =>DirConfig::$dir_image_hash_show,
                'image_item_ID_list'    =>$image_item_ID_list[self::$image_ID]
            );

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        return self::$image_data;

    }

    /**
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_image_data();

        return self::set_return();

    }

    /**
     * @param int|NULL $image_ID
     * @param bool $is_need_resolution_type
     * @param bool $is_file_extension
     * @param bool $is_server_link
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_path_data(int $image_ID=NULL,bool $is_need_resolution_type=true,bool $is_file_extension=true,bool $is_server_link=false){

        if(empty($image_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        self::$image_ID                     =$image_ID;
        self::$is_need_resolution_type      =$is_need_resolution_type;
        self::$is_file_extension            =$is_file_extension;
        self::$is_server_link               =$is_server_link;

        return self::set();

    }

}