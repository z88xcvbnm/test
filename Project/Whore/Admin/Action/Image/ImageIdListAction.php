<?php

namespace Project\Whore\Admin\Action\Image;

use Core\Module\Exception\ParametersException;
use Core\Module\Image\Image;
use Core\Module\Json\Json;

class ImageIdListAction{

    /** @var string */
    private static $data;

    /** @var array */
    private static $list=[];

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     */
    private static function prepare(){

        $r=Json::decode(self::$data);

        if(!is_array($r)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image list is not JSON'
            );

            throw new ParametersException($error);

        }

        self::$list=Image::get_image_ID_from_image_list($r);

    }

    /**
     * @return array
     */
    private static function set_return(){

        return self::$list;

    }

    /**
     * @return array
     * @throws ParametersException
     */
    private static function set(){

        self::prepare();

        return self::set_return();

    }

    /**
     * @param string|NULL $data
     * @return array
     */
    public  static function get_image_ID_list(string $data=NULL){

        if(empty($data))
            return[];

        self::$data=$data;

        return self::set();

    }

}