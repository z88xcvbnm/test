<?php

namespace Project\Whore\Admin\Action\Face;

use Core\Module\Geo\CityLocalization;
use Core\Module\Lang\LangConfig;
use Project\Whore\All\Module\Face\Face;
use Project\Whore\All\Module\Face\FaceCity;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;
use Project\Whore\All\Module\Info\Info;

class GetFaceStatsForRootAction{

    /** @var int */
    private static $face_len                    =0;

    /** @var int */
    private static $face_image_indexed_len      =0;

    /** @var array */
    private static $city_list                   =[];

    /** @var int */
    private static $city_len                    =3;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_city_list(){

        self::$city_list=FaceCity::get_face_city_stats_list(3);

//        self::$city_list=[
//            [
//                'name'  =>'Москва',
//                'len'   =>147078
//            ],
//            [
//                'name'  =>'Санкт-Петербург',
//                'len'   =>39412
//            ],
//            [
//                'name'  =>'Краснодар',
//                'len'   =>5889
//            ],
////            [
////                'name'  =>'Киев',
////                'len'   =>4240
////            ]
//        ];

        return true;

    }

    /**
     * @return bool
     */
    private static function set_face_len(){

        $r=Info::get_info_list('info');

        if(empty($r))
            return false;

        self::$face_len                 =$r['info']['face_len'];
        self::$face_image_indexed_len   =$r['info']['image_len'];

//        self::$face_len=Face::get_face_count();
//        self::$face_len=306458;

        return true;

    }

    /**
     * @return bool
     */
    private static function set_face_image_indexed_len(){

//        self::$face_image_indexed_len=FaceImage::get_global_face_plus_plus_image_len();
//        self::$face_image_indexed_len=1207143;

        return true;

    }

    /**
     * @return array
     */
    private static function init_page(){

        $data=[
            'face_len'                  =>self::$face_len,
            'face_image_indexed_len'    =>self::$face_image_indexed_len,
            'city_list'                 =>self::$city_list
        ];

        return $data;

    }

    /**
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_face_len();
        self::set_face_image_indexed_len();
        self::set_city_list();

        return self::init_page();

    }

    /**
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}