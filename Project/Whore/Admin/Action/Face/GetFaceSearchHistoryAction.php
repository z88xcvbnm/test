<?php

namespace Project\Whore\Admin\Action\Face;

use Core\Module\Sort\Sort;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\Admin\Action\Image\ImagePathListAction;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceSearch;
use Project\Whore\All\Module\Face\FaceSearchItem;
use Project\Whore\All\Module\Face\FaceSearchMulti;
use Project\Whore\All\Module\Face\FaceSearchMultiItem;

class GetFaceSearchHistoryAction{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $face_search_history_ID;

    /** @var array */
    private static $face_ID_list                    =[];

    /** @var array */
    private static $face_search_history_ID_list     =[];

    /** @var array */
    private static $face_search_history_list        =[];

    /** @var int */
    private static $start                           =0;

    /** @var int */
    private static $len                             =10;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_search_list(){

        $r=FaceSearchMulti::get_face_search_multi_data_history_list(self::$face_search_history_ID,self::$start,self::$len,self::$user_ID);

        if(count($r)==0)
            return true;

        foreach($r as $row){

            self::$face_search_history_ID_list[]=$row['ID'];

            if(!empty($row['source_image_ID']))
                $row['image']=ImagePathAction::get_image_path_data($row['source_image_ID'],true,true);

            self::$face_search_history_list[]=$row;

            if(!empty($row['face_ID']))
                if(array_search($row['face_ID'],self::$face_ID_list)===false)
                    self::$face_ID_list[]=$row['face_ID'];

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_list(){

        if(count(self::$face_ID_list)==0)
            return true;

        $r=FaceData::get_face_data_list_from_face_ID_list(self::$face_ID_list);

        if(count($r)==0)
            return true;

        $list=[];

        foreach(self::$face_search_history_list as $search_index=>$search_row)
            if(isset($r[$search_row['face_ID']])){

                $face_row=$r[$search_row['face_ID']];

                self::$face_search_history_list[$search_index]['name']                      =$face_row['name'];
                self::$face_search_history_list[$search_index]['age']                       =$face_row['age'];
                self::$face_search_history_list[$search_index]['city']                      =$face_row['city'];
                self::$face_search_history_list[$search_index]['info']                      =$face_row['info'];
                self::$face_search_history_list[$search_index]['source_account_link']       =$face_row['source_account_link'];
                self::$face_search_history_list[$search_index]['face_list']                 =[];

                $list[]=self::$face_search_history_list[$search_index];


            }
            else{

                self::$face_search_history_list[$search_index]['name']                      =NULL;
                self::$face_search_history_list[$search_index]['age']                       =NULL;
                self::$face_search_history_list[$search_index]['city']                      =NULL;
                self::$face_search_history_list[$search_index]['info']                      =NULL;
                self::$face_search_history_list[$search_index]['source_account_link']       =NULL;
                self::$face_search_history_list[$search_index]['face_list']                 =[];

                $list[]=self::$face_search_history_list[$search_index];

            }

        self::$face_search_history_list=$list;

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_search_history_item_list(){

        $face_search_history_item_list=FaceSearchMultiItem::get_face_search_item_list(self::$face_search_history_ID_list);

        if(count($face_search_history_item_list)==0)
            return true;

        foreach($face_search_history_item_list as $face_search_history_ID=>$face_search_history_item_row){

            $index=array_search($face_search_history_ID,array_column(self::$face_search_history_list,'ID'));

            if($index!==false){

                $face_search_history_item_row['face_list']              =Sort::sort_with_key('percent',$face_search_history_item_row['face_list'],'desc');
                self::$face_search_history_list[$index]['face_list']    =$face_search_history_item_row['face_list'];

                self::$face_search_history_list[$index]['face_list']    =array_splice(self::$face_search_history_list[$index]['face_list'],0,5);

            }

        }

        $face_ID_list=[];

        foreach(self::$face_search_history_list as $face_search_history_ID=>$face_search_history_item_row){

            foreach($face_search_history_item_row['face_list'] as $face_index=>$face_row){

                $face_ID_list[]=$face_row['face_ID'];

                if(!empty($face_row['image_ID']))
                    self::$face_search_history_list[$face_search_history_ID]['face_list'][$face_index]['image']=ImagePathAction::get_image_path_data($face_row['image_ID']);

            }

        }

        if(count($face_ID_list)==0)
            return true;

        $r=FaceData::get_face_data_list_from_face_ID_list($face_ID_list);

        if(count($r)==0)
            return true;

        foreach(self::$face_search_history_list as $face_search_history_ID=>$face_search_history_row)
            foreach($face_search_history_row['face_list'] as $face_index=>$face_row)
                if(isset($r[$face_row['face_ID']])){

                    $face_row['name']                   =$r[$face_row['face_ID']]['name'];
                    $face_row['age']                    =$r[$face_row['face_ID']]['age'];
                    $face_row['city']                   =$r[$face_row['face_ID']]['city'];
                    $face_row['info']                   =$r[$face_row['face_ID']]['info'];
                    $face_row['source_account_link']    =$r[$face_row['face_ID']]['source_account_link'];

                    self::$face_search_history_list[$face_search_history_ID]['face_list'][$face_index]=$face_row;

                }

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'face_search_history_list'=>self::$face_search_history_list
        ];

        return $data;

    }

    /**
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_face_search_list();
        self::set_face_list();
        self::set_face_search_history_item_list();

        return self::set_return();

    }

    /**
     * @param int|NULL $face_search_history_ID
     * @param int $start
     * @param int $len
     * @param int|NULL $user_ID
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $face_search_history_ID=NULL,int $start=0,int $len=10,int $user_ID=NULL){

        self::$face_search_history_ID       =$face_search_history_ID;
        self::$start                        =$start;
        self::$len                          =$len;
        self::$user_ID                      =empty($user_ID)?NULL:$user_ID;

        return self::set();

    }

}