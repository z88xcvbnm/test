<?php

namespace Project\Whore\Admin\Action\Face;

use Core\Module\Device\DeviceType;
use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Encrypt\Hash;
use Core\Module\Image\ImageItem;
use Project\Whore\Admin\Action\Image\ImagePathListAction;
use Project\Whore\All\Module\Face\FaceData;
use Project\Whore\All\Module\Face\FaceImage;

class GetFaceImageRandomListAction{

    /** @var array */
    private static $image_ID_list               =[];

    /** @var array */
    private static $face_image_list             =[];

    /** @var array */
    private static $face_list                   =[];

    /** @var array */
    private static $face_ID_list                =[];

    /** @var int */
    private static $limit                       =30;

    /** @var bool */
    private static $is_need_face_preview_list   =false;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$image_ID_list                    =[];
        self::$face_image_list                  =[];
        self::$limit                            =30;
        self::$is_need_face_preview_list        =false;

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_ID_list(){

        $min_image_ID           =FaceImage::get_image_ID_min();
        $max_image_ID           =FaceImage::get_image_ID_max();

        do{

            $image_ID           =rand($min_image_ID,$max_image_ID);
            $image_data         =FaceData::get_image_ID_list_from_min_image_ID_new($image_ID,self::$limit);
            $image_ID_list      =array_keys($image_data);

            foreach($image_data as $image_ID=>$face_ID){

                self::$face_ID_list[$image_ID]=$face_ID;

            }

            $r=ImagePathListAction::get_image_path_list($image_ID_list,true,true,false,['preview']);

            foreach($r as $row){

                self::$face_image_list[$row['image_ID']]=[
                    'face_ID'       =>Hash::get_sha1_encode(self::$face_ID_list[$row['image_ID']]),
                    'link'          =>$row['image_dir'].'/'.$row['image_item_ID_list']['preview']['ID']
                ];

            }

        }while(count(self::$face_image_list)<self::$limit);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_face_list(){

        if(DeviceType::$is_mobile)
            return true;

        $list=[];

        foreach(self::$face_ID_list as $face_ID)
            if(array_search($face_ID,$list)===false)
                $list[]=$face_ID;

        do{

            $face_ID_list=array_splice($list,0,self::$limit);

            $r=FaceData::get_face_data_list_for_root($face_ID_list);

            self::$face_list=array_merge(self::$face_list,$r);

        }while(count($list)!=0);

        return true;

    }

    /**
     * @return bool
     */
    private static function set_face_image_list(){

        shuffle(self::$face_image_list);

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'face_list'         =>self::$face_list,
            'face_image_list'   =>array_slice(array_values(self::$face_image_list),0,self::$limit)
        ];

        return $data;

    }

    /**
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_image_ID_list();
        self::set_face_image_list();
        self::set_face_list();

        return self::set_return();

    }

    /**
     * @param int $limit
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $limit=60){

        self::reset_data();

        self::$is_need_face_preview_list        =!empty($_POST['is_need_face_preview_list']);
        self::$limit                            =$limit;

        if(DeviceType::$is_mobile)
            self::$limit=16;

        if(!self::$is_need_face_preview_list)
            return[];

        return self::set();

    }

}