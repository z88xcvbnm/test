<?php

namespace Project\Whore\Admin\Action\Payment;

use Core\Module\Email\EmailSend;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Url\Url;
use Core\Module\User\UserBalance;
use Core\Module\User\UserBalanceTransaction;
use Core\Module\User\UserEmail;
use Core\Module\User\UserWalletDonate;

class ConfirmPaymentAction{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_balance_ID;

    /** @var int */
    private static $user_balance_transaction_ID;

    /** @var int */
    private static $wallet_name_ID;

    /** @var int */
    private static $user_wallet_ID;

    /** @var int */
    private static $target_wallet_ID;

    /** @var int */
    private static $user_wallet_donate_ID;

    /** @var string */
    private static $user_wallet_address;

    /** @var string */
    private static $target_wallet_address;

    /** @var string */
    private static $user_email;

    /** @var float */
    private static $transaction_sum;

    /** @var float */
    private static $transaction_wallet_sum;

    /** @var float */
    private static $balance;

    /** @var string */
    private static $hash;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_transaction_data(){

        $r=UserWalletDonate::get_data_from_hash(self::$hash,false);

        if(empty($r)){

            echo'Неверный ключ транзакции';

            exit;

        }

        self::$user_wallet_donate_ID            =$r['ID'];
        self::$user_ID                          =$r['user_ID'];
        self::$wallet_name_ID                   =$r['wallet_name_ID'];
        self::$user_wallet_ID                   =$r['user_wallet_ID'];
        self::$user_wallet_address              =$r['user_wallet_address'];
        self::$target_wallet_address            =$r['target_wallet_address'];
        self::$target_wallet_ID                 =$r['target_wallet_ID'];
        self::$transaction_sum                  =$r['transaction_sum_rub'];
        self::$transaction_wallet_sum           =$r['transaction_sum'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_balance_ID(){

        self::$user_balance_ID=UserBalance::get_user_balance_ID(self::$user_ID);

        if(empty(self::$user_balance_ID))
            self::$user_balance_ID=UserBalance::add_user_balance(self::$user_ID);

        if(empty(self::$user_balance_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User balance was not create'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_balance(){

        self::$balance=UserBalance::get_user_balance(self::$user_ID);

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_money_to_user_balance(){

        if(!UserBalance::add_money_user_balance(self::$user_ID,self::$transaction_sum)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User balance was not create'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_balance_transaction(){

        self::$user_balance_transaction_ID=UserBalanceTransaction::add_user_balance_transaction(self::$user_ID,self::$wallet_name_ID,self::$user_wallet_ID,self::$target_wallet_ID,self::$user_wallet_donate_ID,'confirm_email_money',self::$transaction_sum,NULL,NULL,self::$user_wallet_address,self::$target_wallet_address,self::$transaction_wallet_sum);

        if(empty(self::$user_wallet_donate_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User balance was not create'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_email(){

        self::$user_email=UserEmail::get_user_email_last(self::$user_ID);

        if(empty(self::$user_email)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User email is empty'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_notification(){

        $inner  =[];

        $inner[]='Добрый день!';
        $inner[]='';
        $inner[]='Ваш баланс пополнен на сумму '.self::$transaction_sum.' ₽';
        $inner[]='';
        $inner[]='С уважением,';
        $inner[]='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

        return EmailSend::init(array(self::$user_email),'Пополнение счета '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_balance_in_user_balance_transaction(){

        if(!UserBalanceTransaction::update_balance_in_user_balance_transaction(self::$user_balance_transaction_ID,self::$balance)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Balance was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_wallet_donate_to_confirmed(){

        if(!UserWalletDonate::update_user_wallet_donate_to_confirmed(self::$user_wallet_donate_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User wallet donate was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     *
     */
    private static function set_return(){

        echo'Сумма <b>'.self::$transaction_sum.' ₽</b> была успешно зачислена на счет пользователя <b>'.self::$user_email.'</b>';

        exit;

    }

    /**
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_transaction_data();

        self::set_user_balance_ID();
        self::update_user_wallet_donate_to_confirmed();
        self::add_user_balance_transaction();
        self::add_money_to_user_balance();
        self::set_user_balance();
        self::update_balance_in_user_balance_transaction();

        self::set_user_email();

        self::send_notification();

        return self::set_return();

    }

    /**
     * @param string|NULL $hash
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $hash=NULL){

        if(empty($hash)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is empty'
            ];

            throw new ParametersException($error);

        }

        self::$hash=$hash;

        return self::set();

    }

}