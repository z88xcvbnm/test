<?php

namespace Project\Whore\Admin\Action\Payment;

use Core\Action\Crypto\CryptoCompareCurrencyAction;
use Core\Module\Email\EmailSend;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\OsServer\OsServer;
use Core\Module\Response\ResponseSuccess;
use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserBalance;
use Core\Module\User\UserBalanceTransaction;
use Core\Module\User\UserEmail;
use Core\Module\User\UserWallet;
use Core\Module\User\UserWalletDonate;
use Core\Module\Wallet\WalletAddress;
use Core\Module\Wallet\WalletName;
use Core\Module\Wallet\WalletValidation;

class AddAutoPaymentAction{

    /** @var int */
    private static $kiwi_payment_ID;

    /** @var int */
    private static $paypal_payment_ID;

    /** @var int */
    private static $wallet_name_ID;

    /** @var int */
    private static $user_wallet_ID;

    /** @var int */
    private static $target_wallet_ID;

    /** @var int */
    private static $user_wallet_donate_ID;

    /** @var int */
    private static $user_balance_ID;

    /** @var int */
    private static $user_balance_transaction_ID;

    /** @var float */
    private static $balance;

    /** @var string */
    private static $user_email;

    /** @var string */
    private static $wallet_name;

    /** @var string */
    private static $user_wallet_address;

    /** @var string */
    private static $target_wallet_address;

    /** @var float */
    private static $transaction_sum;

    /** @var float */
    private static $transaction_wallet_sum;

    /** @var float */
    private static $transaction_sum_rub;

    /** @var array */
    private static $transaction_sum_rub_list=[
        0.01,
        1,
        100,
        500,
        1000,
        3000,
        5000,
        10000
    ];

    /** @var float */
    private static $transaction_currency;

    /** @var string */
    private static $hash;

    /** @var string */
    private static $link;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_transaction_sum_rub(){

        if(array_search(self::$transaction_sum_rub,self::$transaction_sum_rub_list)===false){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Transaction sum is not valid'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_user_wallet_address(){

        if(!WalletValidation::is_valid_wallet_address(self::$wallet_name,self::$user_wallet_address)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>(self::$wallet_name=='sberbank'?'Name':'Wallet').' address is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_wallet_name_ID(){

        self::$wallet_name_ID=WalletName::get_wallet_name_ID_from_key(self::$wallet_name);

        if(empty(self::$wallet_name_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Wallet name is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_wallet_ID(){

        self::$user_wallet_ID=UserWallet::get_user_wallet_ID(User::$user_ID,self::$user_wallet_address);

        if(empty(self::$user_wallet_ID))
            self::$user_wallet_ID=UserWallet::add_user_wallet(User::$user_ID,self::$wallet_name_ID,self::$user_wallet_address,0,0,false);

        if(empty(self::$user_wallet_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User wallet was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_target_wallet_ID(){

        self::$target_wallet_ID=WalletAddress::get_wallet_address_ID_from_address(self::$target_wallet_address);

        if(empty(self::$target_wallet_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User wallet was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_transaction_currency(){

        if(
              self::$wallet_name=='sberbank'
            ||self::$wallet_name=='qiwi'
            ||self::$wallet_name=='pp'
            ||self::$wallet_name=='cc'
        ){

            self::$transaction_currency     =1;
            self::$transaction_sum          =self::$transaction_sum_rub;

            return true;

        }

        $r=CryptoCompareCurrencyAction::init('RUB',[self::$wallet_name]);

        if(empty($r)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Problem with get currency'
            ];

            throw new PhpException($error);

        }

        foreach($r['currency_list'] as $value){

            self::$transaction_currency=$value;

            break;

        }

        self::$transaction_sum=round(self::$transaction_sum_rub*self::$transaction_currency,5);

        return true;

    }

    /**
     * @return bool
     */
    private static function set_hash(){

        $list=[
            User::$user_ID,
            self::$wallet_name_ID,
            self::$user_wallet_ID,
            self::$target_wallet_ID,
            self::$user_wallet_address,
            self::$target_wallet_address,
            self::$transaction_sum,
            self::$transaction_currency,
            self::$transaction_sum_rub,
            time(),
            rand(0,time())
        ];

        self::$hash=Hash::get_sha1_encode(implode(':',$list));

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_email(){

        self::$user_email=UserEmail::get_user_email_last(User::$user_ID);

        if(empty(self::$user_email)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User email is empty'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_wallet_donate_ID(){

        self::$user_wallet_donate_ID=UserWalletDonate::add_user_wallet_donate(
            User::$user_ID,
            self::$wallet_name_ID,
            self::$user_wallet_ID,
            self::$target_wallet_ID,
            self::$user_wallet_address,
            self::$target_wallet_address,
            self::$transaction_sum,
            self::$transaction_currency,
            self::$transaction_sum_rub,
            self::$hash,
            NULL,
            false,
            self::$paypal_payment_ID,
            self::$kiwi_payment_ID
        );

        if(empty(self::$user_wallet_donate_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User wallet donate ID was not add'
            ];

            throw new PhpException($error);

        }

        UserWalletDonate::update_user_wallet_donate_to_confirmed(self::$user_wallet_donate_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_balance_ID(){

        self::$user_balance_ID=UserBalance::get_user_balance_ID(User::$user_ID);

        if(empty(self::$user_balance_ID))
            self::$user_balance_ID=UserBalance::add_user_balance(User::$user_ID);

        if(empty(self::$user_balance_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User balance was not create'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_balance(){

        self::$balance=UserBalance::get_user_balance(User::$user_ID);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_bonus(){

        self::add_money_to_user_balance();
        self::set_user_balance();

        self::$user_balance_transaction_ID=UserBalanceTransaction::add_user_balance_transaction(User::$user_ID,NULL,self::$user_wallet_ID,NULL,self::$user_wallet_donate_ID,'auto_bonus',self::$transaction_sum_rub,'PayPal auto bonus',NULL,NULL,NULL,NULL);

        self::update_balance_in_user_balance_transaction();

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_money_to_user_balance(){

        if(!UserBalance::add_money_user_balance(User::$user_ID,self::$transaction_sum)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User balance was not create'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_balance_transaction(){

        self::$user_balance_transaction_ID=UserBalanceTransaction::add_user_balance_transaction(User::$user_ID,self::$wallet_name_ID,self::$user_wallet_ID,self::$target_wallet_ID,self::$user_wallet_donate_ID,'auto_payment',self::$transaction_sum,NULL,NULL,self::$user_wallet_address,self::$target_wallet_address,self::$transaction_wallet_sum);

        if(empty(self::$user_wallet_donate_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User balance was not create'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_notification_to_user(){

        $inner  =[];

        $inner[]='Добрый день!';
        $inner[]='';
        $inner[]='Ваш баланс пополнен на сумму '.self::$transaction_sum.' ₽';
        $inner[]='';
        $inner[]='С уважением,';
        $inner[]='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

        return EmailSend::init(array(self::$user_email),'Пополнение счета '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_notification_to_admin(){

        $inner          =[];
        $wallet_name    =self::$wallet_name;
        $currency       =self::$wallet_name;

        switch(self::$wallet_name){

            case'pp':{

                $wallet_name    ='PayPal';
                $currency       ='₽';

                break;

            }

            case'cc':{

                $wallet_name    ='Credit Card';
                $currency       ='₽';

                break;

            }

            case'sberbank':{

                $wallet_name    ='Sberbank';
                $currency       ='₽';

                break;

            }

            case'kiwi':{

                $wallet_name    ='Kiwi';
                $currency       ='₽';

                break;

            }

        }

        $inner[]='Пользователь: '.self::$user_email;
        $inner[]='Способ пополнения: '.mb_strtoupper($wallet_name);
        $inner[]='Сумма пополнения: '.self::$transaction_sum.' '.mb_strtoupper($currency);
//        $inner[]='Кошелек пользователя: '.self::$user_wallet_address;
        $inner[]='Кошелек пользователя: '.(self::$wallet_name=='sberbank'?'...':'').self::$user_wallet_address;
        $inner[]='Кошелек пополнения: '.self::$target_wallet_address;
        $inner[]='Сумма к зачислению: '.self::$transaction_sum_rub.' ₽';

        return EmailSend::init(array(\Config::$email_payment),'Пополнение счета '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_balance_in_user_balance_transaction(){

        if(!UserBalanceTransaction::update_balance_in_user_balance_transaction(self::$user_balance_transaction_ID,self::$balance)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Balance was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'balance'=>self::$balance
        ];

        return $data;

    }

    /**
     * @return array|bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::is_valid_transaction_sum_rub()){

            self::set_wallet_name_ID();

            if(self::is_valid_user_wallet_address()){

                self::set_user_wallet_ID();
                self::set_target_wallet_ID();
                self::set_transaction_currency();
                self::set_user_email();
                self::set_hash();
                self::set_user_wallet_donate_ID();

                self::set_user_balance_ID();
                self::add_user_balance_transaction();
                self::add_money_to_user_balance();
                self::set_user_balance();
                self::update_balance_in_user_balance_transaction();

//                self::add_user_bonus();

                self::send_notification_to_admin();
                self::send_notification_to_user();

                return self::set_return();

            }

        }

        return false;

    }

    /**
     * @param string|NULL $wallet_name
     * @param string|NULL $user_wallet_address
     * @param string|NULL $target_wallet_address
     * @param float|NULL $transaction_sum_rub
     * @param int|NULL $paypal_payment_ID
     * @param int|NULL $kiwi_payment_ID
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $wallet_name=NULL,string $user_wallet_address=NULL,string $target_wallet_address=NULL,float $transaction_sum_rub=NULL,int $paypal_payment_ID=NULL,int $kiwi_payment_ID=NULL){

        $error_info_list=[];

        if(empty($wallet_name))
            $error_info_list[]='Wallet name is empty';

        if(empty($user_wallet_address))
            $error_info_list[]='User wallet address is empty';

        if(empty($target_wallet_address))
            $error_info_list[]='Target wallet address is empty';

        if(empty($transaction_sum_rub))
            $error_info_list[]='Transaction sum RUB is empty';

        if(
              empty($paypal_payment_ID)
            &&empty($kiwi_payment_ID)
        )
            $error_info_list[]='Payment ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$wallet_name                  =mb_strtolower($wallet_name,'utf-8');
        self::$user_wallet_address          =$user_wallet_address;
        self::$target_wallet_address        =$target_wallet_address;
        self::$transaction_sum_rub          =$transaction_sum_rub;
        self::$paypal_payment_ID            =$paypal_payment_ID;
        self::$kiwi_payment_ID              =$kiwi_payment_ID;

        return self::set();

    }

}