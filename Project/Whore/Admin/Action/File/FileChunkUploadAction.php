<?php

namespace Project\Whore\Admin\Action\File;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\File\FileUpload;

class FileChunkUploadAction{

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $file_chunk_index;

    /** @var int */
    private static $file_chunk_count;

    /** @var int */
    private static $file_chunk_size;

    /** @var int */
    private static $file_size;

    /** @var string */
    private static $file_extension;

    /** @var string */
    private static $file_content_type;

    /** @var string */
    private static $file_mime_type;

    /** @var string */
    private static $file_chunk;

    /** @var array */
    private static $data;

    /**
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set_file_upload(){

        self::$data=FileUpload::init(self::$file_ID,self::$file_mime_type,self::$file_content_type,self::$file_extension,self::$file_chunk_index,self::$file_chunk_count,self::$file_chunk_size,self::$file_size,self::$file_chunk);

        if(empty(self::$data)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'File was not uploaded'
            );

            throw new PhpException($error);

        }

        return self::$data;

    }

    /**
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        return self::set_file_upload();

    }

    /**
     * @param int|NULL $file_ID
     * @param int|NULL $file_chunk_index
     * @param int|NULL $file_chunk_count
     * @param int|NULL $file_chunk_size
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @param string|NULL $file_chunk
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(int $file_ID=NULL,int $file_chunk_index=NULL,int $file_chunk_count=NULL,int $file_chunk_size=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_content_type=NULL,string $file_extension=NULL,string $file_chunk=NULL){

        $error_info_list=[];

        if(is_null($file_chunk_index))
            $error_info_list['file_chunk_index']='File chunk index is NULL';

        if(empty($file_chunk_count))
            $error_info_list['file_chunk_count']='File chunk count is empty';

        if(empty($file_chunk_size))
            $error_info_list['file_chunk_size']='File chunk size is empty';

        if(empty($file_size))
            $error_info_list['file_size']='File size is empty';

        if(empty($file_mime_type))
            $error_info_list['file_mime_type']='File mime type is empty';

        if(empty($file_content_type))
            $error_info_list['file_content_type']='File content type is empty';

        if(empty($file_extension))
            $error_info_list['file_extension']='File extension is empty';

        if(empty($file_chunk))
            $error_info_list['file_chunk']='File chunk is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$file_ID                  =empty($file_ID)?NULL:$file_ID;
        self::$file_chunk_index         =$file_chunk_index;
        self::$file_chunk_count         =$file_chunk_count;
        self::$file_chunk_size          =$file_chunk_size;
        self::$file_size                =$file_size;
        self::$file_mime_type           =$file_mime_type;
        self::$file_content_type        =$file_content_type;
        self::$file_extension           =$file_extension;
        self::$file_chunk               =$file_chunk;

        return self::set();

    }

}