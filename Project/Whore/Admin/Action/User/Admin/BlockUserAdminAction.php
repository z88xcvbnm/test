<?php

namespace Project\Whore\Admin\Action\User\Admin;

use Core\Module\Exception\ParametersException;
use Core\Module\User\User;
use Core\Module\User\UserBlock;

class BlockUserAdminAction{
    
    /** @var int */
    private static $user_ID;
    
    /** @var bool */
    private static $is_block;

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function update_user_block(){

        self::$is_block=!UserBlock::isset_user_block(self::$user_ID);

        if(self::$is_block){

            $block_info='Admin blocked your profile';

            UserBlock::add_user_block(User::$user_ID,self::$user_ID,$block_info,$block_info);

        }
        else
            UserBlock::remove_user_block(self::$user_ID);

    }

    /**
     * Prepare
     * @return array
     */
    private static function set(){

        self::update_user_block();

        return array(
            'ID'        =>self::$user_ID,
            'block'     =>self::$is_block
        );

    }

    /**
     * Init
     * @return array
     * @throws ParametersException
     */
    public  static function init(){

        if(!isset($_POST['user_ID'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        self::$user_ID=$_POST['user_ID'];

        return self::set();

    }
    
}