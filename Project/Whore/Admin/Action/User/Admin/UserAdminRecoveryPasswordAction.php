<?php

namespace Project\Whore\Admin\Action\User\Admin;

use Core\Module\Email\EmailSend;
use Core\Module\Email\EmailValidation;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\OsServer\OsServer;
use Core\Module\Url\Url;
use Core\Module\User\UserBlock;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Core\Module\User\UserHash;
use Core\Module\User\UserHashLinkConfig;
use Core\Module\User\UserHashTypeConfig;
use Core\Module\User\UserRemove;

class UserAdminRecoveryPasswordAction{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_hash_ID;

    /** @var int */
    private static $user_hash_type_ID;

    /** @var string */
    private static $email;

    /** @var string */
    private static $hash;

    /** @var string */
    private static $name;

    /** @var string */
    private static $surname;

    /** @var string */
    private static $link;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_email(){

        if(!EmailValidation::is_valid_email(self::$email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>array(
                    'email'=>'Email is not valid'
                )
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_email(){

        self::$user_ID=UserEmail::get_user_ID(self::$email);

        if(empty(self::$user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>array(
                    'email'     =>'Email is not valid'
                )
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_hash_type(){

        self::$user_hash_type_ID=UserHashTypeConfig::get_user_hash_type_ID('recovery_password');

        if(empty(self::$user_hash_type_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>array(
                    'hash_type'     =>'Hash type for recovery password is not exists'
                )
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_block(){

        if(UserBlock::isset_user_block(self::$user_ID)){

            $error=array(
                'title'     =>'Access denied',
                'info'      =>array(
                    'user_block'        =>'User was blocked'
                )
            );

            throw new AccessDeniedException($error);

        }

        return false;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_remove(){

        if(UserRemove::isset_user_remove(self::$user_ID)){

            $error=array(
                'title'     =>'Access denied',
                'info'      =>array(
                    'user_remove'=>'User was removed'
                )
            );

            throw new AccessDeniedException($error);

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_data(){

        $data=UserData::get_user_data(self::$user_ID);

        if(empty($data))
            return true;

        self::$name     =$data['name'];
        self::$surname  =$data['surname'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_hash(){

        $list=array(
            self::$user_ID,
            self::$email,
            self::$name,
            self::$surname,
            rand(0,time())
        );

        self::$hash             =Hash::get_sha1_encode(implode(':',$list));
        self::$user_hash_ID     =UserHash::add_user_hash(self::$user_ID,self::$user_hash_type_ID,NULL,self::$hash);

        if(empty(self::$user_hash_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>array(
                    'user_hash'=>'User hash was not added'
                )
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_hash(){

        if(!UserHash::remove_user_hash(self::$user_ID,self::$user_hash_type_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>array(
                    'user_hash'=>'User hash was not removed'
                )
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_mail(){

        $list   =[];
        $link   =UserHashLinkConfig::get_link_full('recovery_password').'/'.self::$hash;

        self::$link=$link;

        $hi_line='Здравствуйте';

        if(!empty(self::$surname)||!empty(self::$name)){

            $hi_line.=',';

            if(!empty(self::$surname))
                $hi_line.=' '.self::$surname;

            if(!empty(self::$name))
                $hi_line.=' '.self::$name;

        }

        $list[]=$hi_line;

        $list[]='';
        $list[]='Для восстановления пароля, пожалуйста, перейдите по ссылке: <a href="'.$link.'" target="_blank">'.$link.'</a>';
        $list[]='';
        $list[]='С уважением,';
        $list[]='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

        if(!EmailSend::init(array(self::$email),'Восстановление пароля',implode("<br />\n",$list),\Config::$email_no_replay)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>array(
                    'email_send'=>'Email was not send'
                )
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'success'=>true
        ];

        if(OsServer::$is_windows)
            $data['link']=self::$link;

        return $data;

    }

    /**
     * @return array|bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::is_valid_email())
            if(self::isset_user_email())
                if(!self::isset_user_block())
                    if(!self::isset_user_remove())
                        if(self::isset_user_hash_type()){

                            self::set_user_data();
                            self::remove_user_hash();
                            self::set_user_hash();
                            self::send_mail();

                            return self::set_return();

                        }

        return false;

    }

    /**
     * @param string|NULL $email
     * @return array|bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $email=NULL){

        if(empty($email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>array(
                    'email'     =>'Email is empty'
                )
            );

            throw new ParametersException($error);

        }

        self::$email=strtolower($email);

        return self::set();

    }

}