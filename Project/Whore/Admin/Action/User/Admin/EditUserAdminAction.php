<?php

namespace Project\Whore\Admin\Action\User\Admin;

use Core\Module\Email\EmailValidation;
use Core\Module\Error\ErrorCashContent;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Core\Module\User\UserLogin;
use Core\Module\User\UserLoginValidation;

class EditUserAdminAction{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_access_ID;

    /** @var int */
    private static $user_data_ID;

    /** @var int */
    private static $user_email_ID;

    /** @var int */
    private static $user_login_ID;

    /** @var int */
    private static $firm_ID;

    /** @var string */
    private static $email;

    /** @var string */
    private static $login;

    /** @var string */
    private static $surname;

    /** @var string */
    private static $name;

    /** @var bool */
    private static $is_admin;

    /** @var bool */
    private static $is_manager;

    /** @var bool */
    private static $is_bookkeep;

    /** @var bool */
    private static $is_museum;

    /** @var array */
    private static $place_ID_list;

    /**
     * @return bool
     */
    private static function is_valid_email(){

        return EmailValidation::is_valid_email(self::$email);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_email(){

        return UserEmail::isset_user_email_in_active_list(self::$email,self::$user_ID);

    }

    /**
     * @return bool|int
     */
    private static function is_valid_login(){

        return UserLoginValidation::is_valid_user_login(self::$login);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_login(){

        return UserLogin::isset_user_login_in_active(self::$login,self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_email(){

        $error_info_list=[];

        if(!self::is_valid_email())
            $error_info_list[]='Email is not valid';

        if(self::isset_email())
            $error_info_list[]='Email already exists';

        if(count($error_info_list)>0){

            ErrorCashContent::add_error_list('email',$error_info_list);

            return false;

        }
        
        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_login(){

        $error_info_list=[];

        if(!self::is_valid_login())
            $error_info_list[]='Login is not valid';

        if(self::isset_login())
            $error_info_list[]='Login already exists';

        if(count($error_info_list)==0)
            return true;

        ErrorCashContent::add_error_list('login',$error_info_list);

        return false;

    }

    /**
     * @return bool
     */
    private static function check_name(){

        return true;

    }

    /**
     * @return bool
     */
    private static function check_surname(){

        return true;

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user(){

        User::update_user_date_update(self::$user_ID);

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_login(){

        $user_login_data=UserLogin::get_user_login_info_from_user_ID(self::$user_ID);

        if(empty($user_login_data)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User login data is empty'
            );

            throw new ParametersException($error);

        }

        if(isset($user_login_data['ID']))
            UserLogin::remove_user_login_ID($user_login_data['ID']);

        self::$user_login_ID=UserLogin::add_user_login(self::$user_ID,self::$login,$user_login_data['password']);

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_access(){

        if(!UserAccess::remove_user_access(self::$user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access was not removed'
            );

            throw new ParametersException($error);

        }

        $list=[];

        if(self::$is_admin)
            $list[]='admin';

        if(self::$is_manager)
            $list[]='manager';

        if(self::$is_bookkeep)
            $list[]='bookkeep';

        if(self::$is_museum)
            $list[]='financial';

        self::$user_access_ID=UserAccess::add_user_access(self::$user_ID,$list);

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_data(){

        if(!UserData::remove_user_data(self::$user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User data was not removed'
            );

            throw new ParametersException($error);

        }

        self::$user_data_ID=UserData::add_user_data(self::$user_ID,self::$name,self::$surname);

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_email(){

        if(!UserEmail::remove_user_email(self::$user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User email was not removed'
            );

            throw new ParametersException($error);

        }

        self::$user_email_ID=UserEmail::add_user_email(self::$user_ID,self::$email);

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=array(
            'ID'            =>(int)self::$user_ID,
            'login'         =>stripslashes(self::$login),
            'image_ID'      =>0,
            'firm_ID'       =>(int)self::$firm_ID,
            'email'         =>stripslashes(self::$email),
            'name'          =>stripslashes(self::$name),
            'surname'       =>stripslashes(self::$surname),
            'admin'         =>self::$is_admin,
            'manager'       =>self::$is_manager,
            'bookkeep'      =>self::$is_bookkeep,
            'museum'        =>self::$is_museum,
            'block'         =>false,
            'place_list'    =>self::$place_ID_list,
            'date_create'   =>(int)time(),
            'date_online'   =>0
        );

        return $data;

    }

    /**
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::check_email())
            if(self::check_login())
                if(self::check_name()&&self::check_surname())
                    if(!self::isset_login())
                        if(!self::isset_email()){

                            self::update_user();
                            self::update_user_login();
                            self::update_user_email();
                            self::update_user_access();
                            self::update_user_data();

                            return self::set_return();

                        }

        return false;

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $email
     * @param string|NULL $login
     * @param string|NULL $surname
     * @param string|NULL $name
     * @param bool|NULL $is_admin
     * @param bool|NULL $is_manager
     * @param bool|NULL $is_bookkeep
     * @param bool|NULL $is_museum
     * @param int|NULL $firm_ID
     * @param array $place_ID_list
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $user_ID=NULL,string $email=NULL,string $login=NULL,string $surname=NULL,string $name=NULL,bool $is_admin=NULL,bool $is_manager=NULL,bool $is_bookkeep=NULL,bool $is_museum=NULL,int $firm_ID=NULL,array $place_ID_list=[]){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list['user']='User ID is empty';

        if(empty($email))
            $error_info_list['email']='Email is empty';

        if(empty($login))
            $error_info_list['login']='Login is empty';

        if(empty($surname))
            $error_info_list['surname']='Surname is empty';

        if(empty($name))
            $error_info_list['name']='Name is empty';

        if(count($error_info_list)>0){

            ErrorCashContent::add_error_key_list($error_info_list);

            return false;

        }

        self::$user_ID          =$user_ID;
        self::$email            =$email;
        self::$login            =$login;
        self::$surname          =$surname;
        self::$name             =$name;
        self::$is_admin         =$is_admin;
        self::$is_manager       =$is_manager;
        self::$is_bookkeep      =$is_bookkeep;
        self::$is_museum        =$is_museum;
        self::$firm_ID          =$firm_ID;
        self::$place_ID_list    =$place_ID_list;

        return self::set();

    }

}