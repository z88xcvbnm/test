<?php

namespace Project\Whore\Admin\Action\User\Admin;

use Core\Action\Logout\UserLogout\UserLogoutSystemAction;

class LogoutUserAdminAction{

    /**
     * Prepare
     * @return bool
     */
    private static function set(){

        return UserLogoutSystemAction::init();

    }

    /**
     * Init logout
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}