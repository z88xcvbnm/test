<?php

namespace Project\Whore\Admin\Action\User\Admin;

use Core\Module\Exception\ParametersException;
use Core\Module\User\UserAccess;

class SetToAdminUserAdminAction{

    /** @var int */
    private static $user_ID;

    /** @var bool */
    private static $is_admin;

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function update_user_access(){

        if(UserAccess::isset_user_access(self::$user_ID,'admin')){

            $user_access_type_list  =array('manager');
            self::$is_admin         =false;

        }
        else{

            $user_access_type_list  =array('admin');
            self::$is_admin         =true;

        }

        UserAccess::remove_user_access(self::$user_ID);
        UserAccess::add_user_access(self::$user_ID,$user_access_type_list);

    }

    /**
     * Prepare
     * @return array
     */
    private static function set(){

        self::update_user_access();

        return array(
            'ID'        =>self::$user_ID,
            'admin'     =>self::$is_admin,
            'manager'   =>!self::$is_admin
        );

    }

    /**
     * Init
     * @return array
     * @throws ParametersException
     */
    public  static function init(){

        if(!isset($_POST['user_ID'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        self::$user_ID=$_POST['user_ID'];

        return self::set();

    }

}