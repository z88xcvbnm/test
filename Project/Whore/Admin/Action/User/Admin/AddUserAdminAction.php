<?php

namespace Project\Whore\Admin\Action\User\Admin;

use Core\Module\Email\EmailSend;
use Core\Module\Email\EmailValidation;
use Core\Module\Encrypt\Hash;
use Core\Module\Error\ErrorCashContent;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Core\Module\User\UserHash;
use Core\Module\User\UserHashLinkConfig;
use Core\Module\User\UserHashType;
use Core\Module\User\UserHashTypeConfig;
use Core\Module\User\UserLogin;
use Core\Module\User\UserLoginValidation;
use Project\Whore\Admin\Config\NotificationConfigAdminWhore;

class AddUserAdminAction{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_access_ID;

    /** @var int */
    private static $user_data_ID;

    /** @var int */
    private static $user_email_ID;

    /** @var int */
    private static $user_login_ID;

    /** @var int */
    private static $user_hash_ID;

    /** @var int */
    private static $user_hash_type_ID;

    /** @var string */
    private static $email;

    /** @var string */
    private static $login;

    /** @var string */
    private static $surname;

    /** @var string */
    private static $name;

    /** @var bool */
    private static $is_admin;

    /** @var bool */
    private static $is_manager;

    /** @var string */
    private static $hash;

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_email(){

        if(!EmailValidation::is_valid_email(self::$email)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'Email is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_email(){

        if(!UserEmail::isset_user_email_in_active_list(self::$email)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'User email already exists'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_login(){

        if(!UserLoginValidation::is_valid_user_login(self::$login)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'User login is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_login(){

        if(!UserLogin::isset_user_login_in_active(self::$login)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Login already exists'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_email(){

        $error_info_list=[];

        if(!self::is_valid_email())
            $error_info_list[]='Email is not valid';

        if(self::isset_email())
            $error_info_list[]='Email already exists';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_login(){

        $error_info_list=[];

        if(!self::is_valid_login())
            $error_info_list[]='Login is not valid';

        if(self::isset_login())
            $error_info_list[]='Login already exists';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersValidationException($error);

        }

        return false;

    }

    /**
     * @return bool
     */
    private static function check_name(){

        return true;

    }

    /**
     * @return bool
     */
    private static function check_surname(){

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user(){

        self::$user_ID=User::add_user();

        if(empty(self::$user_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_login(){

        self::$user_login_ID=UserLogin::add_user_login(self::$user_ID,self::$login);

        if(empty(self::$user_login_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User login was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_access(){

        $list=[];

        if(self::$is_admin)
            $list[]='admin';

        if(self::$is_manager)
            $list[]='manager';

        self::$user_access_ID=UserAccess::add_user_access(self::$user_ID,$list);

        if(empty(self::$user_access_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User access was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_data(){

        self::$user_data_ID=UserData::add_user_data(self::$user_ID,self::$name,self::$surname);

        if(empty(self::$user_data_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User data was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_email(){

        self::$user_email_ID=UserEmail::add_user_email(self::$user_ID,self::$email);

        if(empty(self::$user_email_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User email was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_hash(){

        $list=array(
            self::$user_ID,
            self::$email,
            self::$login,
            self::$name,
            self::$surname
        );

        self::$hash                 =Hash::get_sha1_encode(implode(',',$list));
        self::$user_hash_type_ID    =UserHashTypeConfig::get_user_hash_type_ID('registration_invite');

        if(empty(self::$user_hash_type_ID)){

            $error=array(
                'title'     =>PhpException::$title,
                'info'      =>'User hash type is not exists'
            );

            throw new PhpException($error);

        }

        self::$user_hash_ID=UserHash::add_user_hash(self::$user_ID,self::$user_hash_type_ID,NULL,self::$hash);

        if(empty(self::$user_hash_ID)){

            $error=array(
                'title'     =>PhpException::$title,
                'info'      =>'User hash was not added'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_invite(){

        $inner  =[];
        $link   =UserHashLinkConfig::get_link_full('invite').'/'.self::$hash;

        if(UserAccess::$is_admin||UserAccess::$is_root){

            $inner[]='Здравствуйте, '.self::$name;
            $inner[]='';
            $inner[]='Для подтверждения регистрации, пожалуйста, перейдите по ссылке: <a href="'.$link.'">'.$link.'</a>';
            $inner[]='';
            $inner[]='С уважением,';
            $inner[]='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

        }

        return EmailSend::init(array(self::$email),'Регистрация '.NotificationConfigAdminWhore::$url_link_for_email_notification,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=array(
            'ID'            =>(int)self::$user_ID,
            'login'         =>stripslashes(self::$login),
            'image_ID'      =>0,
            'email'         =>stripslashes(self::$email),
            'name'          =>stripslashes(self::$name),
            'surname'       =>stripslashes(self::$surname),
            'admin'         =>self::$is_admin,
            'manager'       =>self::$is_manager,
            'block'         =>false,
            'date_create'   =>(int)time(),
            'date_online'   =>0
        );

        return $data;

    }

    /**
     * @return array|bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::check_email())
            if(self::check_login())
                if(self::check_name()&&self::check_surname())
                    if(!self::isset_login())
                        if(!self::isset_email()){

                            self::add_user();
                            self::add_user_login();
                            self::add_user_email();
                            self::add_user_access();
                            self::add_user_data();
                            self::add_user_hash();
                            self::send_invite();

                            return self::set_return();

                        }

        return false;

    }

    /**
     * @param string|NULL $email
     * @param string|NULL $login
     * @param string|NULL $surname
     * @param string|NULL $name
     * @param bool|NULL $is_admin
     * @param bool|NULL $is_manager
     * @return array|bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $email=NULL,string $login=NULL,string $surname=NULL,string $name=NULL,bool $is_admin=NULL,bool $is_manager=NULL){

        $error_info_list=[];

        if(empty($email))
            $error_info_list['email']='Email is empty';

        if(empty($login))
            $error_info_list['login']='Login is empty';

        if(empty($surname))
            $error_info_list['surname']='Surname is empty';

        if(empty($name))
            $error_info_list['name']='Name is empty';

        if(count($error_info_list)>0){

            ErrorCashContent::add_error_key_list($error_info_list);

            return false;

        }

        self::$email            =$email;
        self::$login            =$login;
        self::$surname          =$surname;
        self::$name             =$name;
        self::$is_admin         =$is_admin;
        self::$is_manager       =$is_manager;

        return self::set();

    }

}