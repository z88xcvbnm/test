<?php

namespace Project\Whore\Admin\Action\User\Admin;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Image\Image;
use Core\Module\User\User;
use Core\Module\User\UserAvatar;

class UserSaveAvatarAction{

    /** @var int */
    private static $user_avatar_ID;

    /** @var int */
    private static $image_ID;

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function isset_image(){

        if(!Image::isset_image_ID(self::$image_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is not exists'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function remove_user_avatar(){

        if(!UserAvatar::remove_user_avatar(User::$user_ID)){

            $error=array(
                'title'     =>'System error',
                'info'      =>'User avatar was not removed'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function add_user_avatar(){

        self::$user_avatar_ID=UserAvatar::add_user_avatar(User::$user_ID,self::$image_ID);

        if(empty(self::$user_avatar_ID)){

            $error=array(
                'title'     =>'System error',
                'info'      =>'User avatar was not added'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_return(){

        return !empty(self::$user_avatar_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    private static function set(){

        if(self::isset_image())
            if(self::remove_user_avatar())
                if(self::add_user_avatar())
                    return self::set_return();

        return false;

    }

    /**
     * @param int|NULL $image_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function init(int $image_ID=NULL){

        if(empty($image_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        self::$image_ID=$image_ID;

        return self::set();

    }

}