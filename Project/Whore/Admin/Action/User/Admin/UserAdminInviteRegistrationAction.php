<?php

namespace Project\Whore\Admin\Action\User\Admin;

use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Session\Session;
use Core\Module\Token\Token;
use Core\Module\User\User;
use Core\Module\User\UserAuth;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Core\Module\User\UserEmailCheck;
use Core\Module\User\UserHash;
use Core\Module\User\UserHashTypeConfig;
use Core\Module\User\UserLogin;
use Core\Module\User\UserLoginValidation;

class UserAdminInviteRegistrationAction{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_email_ID;

    /** @var int */
    private static $user_email_check_ID;

    /** @var int */
    private static $user_hash_ID;

    /** @var int */
    private static $user_hash_type_ID;

    /** @var string */
    private static $hash;

    /** @var string */
    private static $login;

    /** @var string */
    private static $email;

    /** @var string */
    private static $name;

    /** @var string */
    private static $surname;

    /** @var string */
    private static $password;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_login(){

        if(!UserLoginValidation::is_valid_user_login(self::$login)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Login is not valid'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_password(){

        if(!UserLoginValidation::is_valid_user_password(self::$password)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Password is not valid'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_hash(){

        $data=UserHash::get_user_hash_data_from_hash(self::$hash,self::$user_hash_type_ID);

        if(empty($data)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>array(
                    'hash'=>'Hash is not valid'
                )
            );

            throw new ParametersException($error);

        }

        self::$user_hash_ID     =$data['ID'];
        self::$user_ID          =$data['user_ID'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_hash_type(){

        self::$user_hash_type_ID=UserHashTypeConfig::get_user_hash_type_ID('registration_invite');

        if(empty(self::$user_hash_type_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Hash type is not exists'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_login(){

        return UserLogin::remove_user_login_from_user_ID(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_data(){

        return UserData::remove_user_data(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_hash_use(){

        return UserHash::update_user_hash_use_from_user_hash_ID(self::$user_hash_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_email(){

        return UserEmail::remove_user_email(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_login(){

        return (bool)UserLogin::add_user_login(self::$user_ID,self::$login,self::$password);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_email(){

        self::$user_email_ID=UserEmail::add_user_email(self::$user_ID,self::$email);

        return !empty(self::$user_email_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_email_check(){

        self::$user_email_check_ID=UserEmailCheck::add_user_email_check(self::$user_ID,self::$user_email_ID,NULL,self::$hash);

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_data(){

        return (bool)UserData::add_user_data(self::$user_ID,self::$name,self::$surname);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_auth(){

        return (bool)UserAuth::add_user_auth(self::$user_ID,Token::$token_ID,Session::$session_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_date_email_check(){

        if(!User::update_user_date_email_check(self::$user_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User data email confirmed was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_token(){

        return (bool)Token::update_token_user_ID(Token::$token_ID,self::$user_ID);

    }

    /**
     * @return bool
     */
    private static function set_return(){

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::is_valid_login())
            if(self::is_valid_password()){

                self::set_user_hash_type();

                if(self::isset_user_hash()){

                    self::remove_user_login();
                    self::remove_user_email();
                    self::remove_user_data();

                    self::add_user_login();
                    self::add_user_email();
                    self::add_user_data();
                    self::add_user_email_check();

                    self::update_user_date_email_check();
                    self::update_user_hash_use();

                    self::update_token();
                    self::set_user_auth();

                    return self::set_return();

                }

            }

        return false;

    }

    /**
     * @param string|NULL $hash
     * @param string|NULL $login
     * @param string|NULL $email
     * @param string|NULL $name
     * @param string|NULL $surname
     * @param string|NULL $password
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $hash=NULL,string $login=NULL,string $email=NULL,string $name=NULL,string $surname=NULL,string $password=NULL){

        $error_info_list=[];

        if(empty($hash))
            $error_info_list['hash']='Hash is empty';

        if(empty($login))
            $error_info_list['login']='Login is empty';

        if(empty($email))
            $error_info_list['email']='Email is empty';

        if(empty($name))
            $error_info_list['name']='Name is empty';

        if(empty($surname))
            $error_info_list['surname']='Surname is empty';

        if(empty($password))
            $error_info_list['password']='Password is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$hash         =$hash;
        self::$login        =strtolower($login);
        self::$email        =strtolower($email);
        self::$name         =$name;
        self::$surname      =$surname;
        self::$password     =Hash::get_sha1_encode($password);

        return self::set();

    }

}
