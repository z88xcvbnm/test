<?php

namespace Project\Whore\Admin\Action\User\Profile;

use Core\Module\Exception\PhpException;
use Core\Module\User\UserBalance;
use Core\Module\User\UserBalanceTransaction;
use Core\Module\User\UserEmailCheck;
use Project\Whore\Admin\Action\User\Data\UserListAction;
use Project\Whore\All\Module\Face\FaceSearch;

class GetProfileListAction{

    /** @var int */
    private static $user_ID;

    /** @var string */
    private static $search;

    /** @var array */
    private static $user_access_type_name_list=[
        'profile',
        'profile_wallet',
    ];

    /** @var array */
    private static $user_list           =[];

    /** @var array */
    private static $user_ID_list        =[];

    /** @var int */
    private static $start               =0;

    /** @var int */
    private static $len                 =20;

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_profile_list(){

        self::$user_list            =UserListAction::init(self::$user_access_type_name_list,[],self::$start,self::$len,self::$user_ID,self::$search);
        self::$user_ID_list         =array_keys(self::$user_list);

        foreach(self::$user_list as $user_ID=>$row){

            $row['balance']                 =NULL;
            $row['is_email_confirmed']      =false;

            self::$user_list[$user_ID]      =$row;

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_balance(){

        if(count(self::$user_ID_list)==0)
            return false;

        $r=UserBalance::get_user_balance_list(self::$user_ID_list);

//        print_r($r);

        if(count($r)==0)
            return false;

        $user_balance_list=UserBalanceTransaction::get_user_balance_isset_transaction(self::$user_ID_list);

        foreach($r as $user_ID=>$balance)
            if(isset(self::$user_list[$user_ID])){

                if(empty($balance)){

                    if(isset($user_balance_list[$user_ID]))
                        self::$user_list[$user_ID]['balance']=$balance;
                    else
                        self::$user_list[$user_ID]['balance']=NULL;

                }
                else
                    self::$user_list[$user_ID]['balance']=$balance;

            }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_find_face(){

        switch(self::$search){

            case'[face_search]':
            case'[only_paid]':
            case'[only_paid_date_online]':{

                if(count(self::$user_ID_list)==0)
                    return false;

                foreach(self::$user_ID_list as $user_ID){

                    $percent=FaceSearch::get_face_search_data_last($user_ID);

                    if(empty($percent))
                        self::$user_list[$user_ID]['percent']=NULL;
                    else
                        self::$user_list[$user_ID]['percent']=$percent;

                }

                break;

            }

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_email_confirmed(){

        if(count(self::$user_ID_list)==0)
            return false;

        $r=UserEmailCheck::get_user_email_confirmed_list(self::$user_ID_list);

        if(count($r)==0)
            return false;

        foreach($r as $user_ID=>$is_checked)
            if(isset(self::$user_list[$user_ID]))
                self::$user_list[$user_ID]['is_email_confirmed']=$is_checked;

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'user_list'=>self::$user_list
        ];

        return $data;

    }

    /**
     * @return array
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_profile_list();
        self::set_user_balance();
        self::set_user_email_confirmed();
        self::set_user_find_face();

        return self::set_return();

    }

    /**
     * @param int $start
     * @param int $len
     * @param int|NULL $user_ID
     * @param string|NULL $search
     * @return array
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $start=0,int $len=20,int $user_ID=NULL,string $search=NULL){

        self::$user_ID      =empty($user_ID)?NULL:$user_ID;
        self::$search       =empty($search)?NULL:$search;
        self::$start        =$start;
        self::$len          =$len;

        return self::set();

    }

}