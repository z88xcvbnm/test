<?php

namespace Project\Whore\Admin\Action\User\Profile;

use Core\Module\Exception\ParametersException;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserAddress;
use Core\Module\User\UserBalance;
use Core\Module\User\UserBalanceTransaction;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Core\Module\User\UserHash;
use Core\Module\User\UserLogin;
use Core\Module\User\UserPhone;
use Core\Module\User\UserRemove;
use Core\Module\User\UserWallet;
use Core\Module\User\UserWalletDonate;

class RemoveProfileAdminAction{

    /** @var int */
    private static $user_ID;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user(){

        return User::remove_user_ID(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_access(){

        return UserAccess::remove_user_access(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_balance(){

        return UserBalance::remove_user_balance(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_balance_transaction(){

        return UserBalanceTransaction::remove_user_balance_transaction(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_wallet(){

        return UserWallet::remove_user_wallet_all(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_waller_donate(){

        return UserWalletDonate::remove_user_wallet_all(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function remove_user_address(){

        return UserAddress::remove_user_address(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_data(){

        return UserData::remove_user_data(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_email(){

        return UserEmail::remove_user_email(self::$user_ID);

    }

    /**
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_login(){

        return UserLogin::remove_user_login_from_user_ID(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_hash(){

        return UserHash::remove_user_hash(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function remove_user_phone(){

        return UserPhone::remove_user_phone(self::$user_ID);

    }

    /**
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_remove(){

        return UserRemove::add_user_remove(User::$user_ID,self::$user_ID,'Admin removed your profile');

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::remove_user();
        self::remove_user_access();
        self::remove_user_address();
        self::remove_user_data();
        self::remove_user_email();
        self::remove_user_login();
        self::remove_user_hash();
        self::remove_user_phone();
        self::remove_user_balance();
        self::remove_user_balance_transaction();
        self::remove_user_wallet();
        self::remove_user_waller_donate();

        self::add_user_remove();

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $user_ID=NULL){

        if(!isset($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        self::$user_ID=$user_ID;

        return self::set();

    }

}