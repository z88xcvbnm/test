<?php

namespace Project\Whore\Admin\Action\User\Data;

use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;
use Core\Module\User\UserLogin;
use Core\Module\User\UserLoginValidation;

class UserSaveLoginAction{

    /** @var int */
    private static $user_login_ID;

    /** @var string */
    private static $login;

    /** @var string */
    private static $password;

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_login(){

        if(UserLogin::isset_user_login_in_active(self::$login,User::$user_ID)){

            $error=[
                'title'     =>AccessDeniedException::$title,
                'info'      =>'Login already exists'
            ];

            throw new AccessDeniedException($error);

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_login(){

        if(!UserLoginValidation::check_user_login_length(self::$login)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Login is not valid'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_user_login_data(){

        $data=UserLogin::get_user_login_data(User::$user_ID);

        if(empty($data)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User data is empty'
            );

            throw new PhpException($error);

        }

        self::$password=$data['password'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_login(){

        if(!UserLogin::remove_user_login_from_user_ID(User::$user_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User login was not removed'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_login(){

        self::$user_login_ID=UserLogin::add_user_login(User::$user_ID,self::$login,self::$password);

        if(empty(self::$user_login_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User login was not updated'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_return(){

        return true;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::is_valid_login())
            if(!self::isset_user_login())
                if(self::get_user_login_data())
                    if(self::remove_user_login())
                        if(self::add_user_login())
                            return self::set_return();

        return false;

    }

    /**
     * @param string|NULL $login
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $login=NULL){

        if(empty($login)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User login is empty'
            );

            throw new ParametersException($error);

        }

        self::$login=strtolower($login);

        return self::set();

    }

}