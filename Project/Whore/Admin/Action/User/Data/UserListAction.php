<?php

namespace Project\Whore\Admin\Action\User\Data;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\User\User;
use Core\Module\User\UserAccessTypeConfig;
use Core\Module\User\UserBalanceTransaction;
use Core\Module\User\UserBlock;
use Core\Module\User\UserWalletDonate;
use Project\Whore\Admin\Action\Image\ImagePathListAction;

class UserListAction{

    /** @var int */
    private static $user_ID;

    /** @var string */
    private static $search;

    /** @var array */
    private static $user_ID_list                    =[];

    /** @var array */
    private static $list                            =[];

    /** @var array */
    private static $user_access_type_ID_list        =[1,2];

    /** @var array */
    private static $user_access_type_name_list      =[];

    /** @var int */
    private static $start;

    /** @var int */
    private static $len;

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_access_type_list(){

        self::$user_access_type_ID_list=UserAccessTypeConfig::get_user_access_type_ID_list(self::$user_access_type_name_list);

        if(count(self::$user_access_type_ID_list)==0){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'User access name list is empty'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_ID_list_with_login(){

        $q=array(
            'select'=>array(
                'user_id'
            ),
            'table'=>'_user_login',
            'where'=>array(
                'type'=>0
            )
        );

        if(!empty(self::$search)){

            $search=mb_strtolower(self::$search,'utf-8');

            $q['where']['login']=[
                'function'=>"like '%{$search}%'"
            ];

        }

        $r=Db::select($q);

        if(count($r)==0)
            return true;

        foreach($r as $row)
            if(array_search($row['user_id'],self::$user_ID_list)===false)
                self::$user_ID_list[]=$row['user_id'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_ID_list_with_email(){

        $q=array(
            'select'=>array(
                'user_id'
            ),
            'table'=>'_user_email',
            'where'=>array(
                'type'=>0
            )
        );

        if(!empty(self::$search)){

            $search=mb_strtolower(self::$search,'utf-8');

            $q['where']['email']=[
                'function'=>"like '%{$search}%'"
            ];

        }

        $r=Db::select($q);

        if(count($r)==0)
            return true;

        foreach($r as $row)
            if(array_search($row['user_id'],self::$user_ID_list)===false)
                self::$user_ID_list[]=$row['user_id'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_ID_list_with_only_paid(){

//        $q=array(
//            'select'=>array(
//                'user_id'
//            ),
//            'table'=>UserWalletDonate::$table_name,
//            'where'=>array(
//                'is_confirm'    =>1,
//                'type'          =>0
//            ),
//            'order'=>[
//                [
//                    'column'        =>'date_update',
//                    'direction'     =>'desc'
//                ]
//            ]
//        );

        $q=array(
            'select'=>array(
                'user_id'
            ),
            'table'=>UserBalanceTransaction::$table_name,
            'where'=>array(
                'action'        =>'find_face',
                'type'          =>0
            ),
            'order'=>[
                [
                    'column'        =>'date_update',
                    'direction'     =>'desc'
                ]
            ]
        );

        $r=Db::select($q);

        if(count($r)==0)
            return true;

        foreach($r as $row)
            if(array_search($row['user_id'],self::$user_ID_list)===false)
                self::$user_ID_list[]=$row['user_id'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_ID_list_with_order_by_date_online(){

        $q=array(
            'select'=>array(
                [
                    'table'=>User::$table_name,
                    'column'=>'id'
                ]
            ),
            'table'=>UserWalletDonate::$table_name,
            'join'=>[
                [
                    'table'=>User::$table_name,
                    'where'=>[
                        [
                            'table'=>UserWalletDonate::$table_name,
                            'table_join'=>User::$table_name,
                            'column'=>'user_id',
                            'column_join'=>'id'
                        ],
                        [
                            'table_join'=>User::$table_name,
                            'column_join'=>'type',
                            'value'=>0
                        ]
                    ]
                ]
            ],
            'where'=>array(
                [
                    'table'=>UserWalletDonate::$table_name,
                    'column'=>'is_confirm',
                    'value'=>1
                ],
                [
                    'table'=>UserWalletDonate::$table_name,
                    'column'=>'type',
                    'value'=>0
                ]
            ),
            'order'=>[
                [
                    'table'         =>User::$table_name,
                    'column'        =>'date_online',
                    'direction'     =>'desc'
                ]
            ]
        );

        $r=Db::select($q);

        if(count($r)==0)
            return true;

        foreach($r as $row)
            if(array_search($row['id'],self::$user_ID_list)===false)
                self::$user_ID_list[]=$row['id'];

        return true;

    }

    /**
     * @return bool
     */
    private static function prepare_user_ID_list(){

        if(empty(self::$user_ID))
            return true;

        $list=[];

        foreach(self::$user_ID_list as $user_ID)
            if($user_ID<self::$user_ID)
                $list[]=$user_ID;

        self::$user_ID_list=$list;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_search(){

        if(empty(self::$search))
            return true;

        switch(self::$search){

            case'[face_search]':
            case'[only_paid]':{

                self::set_user_ID_list_with_only_paid();

                break;

            }

            case'[only_paid_date_online]':{

                self::set_user_ID_list_with_order_by_date_online();

                break;

            }

            default:{

                self::set_user_ID_list_with_email();
                self::set_user_ID_list_with_login();

                break;

            }

        }

        self::prepare_user_ID_list();

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_access_list(){

        $q=array(
            'select'=>array(
                'user_id',
                'user_access_type_id'
            ),
            'table'=>'_user_access',
            'where'=>array(
                'user_access_type_id'   =>self::$user_access_type_ID_list,
                'type'                  =>0
            ),
            'order'=>array(
                array(
                    'column'        =>'user_id',
                    'direction'     =>'desc'
                )
            )
        );

        if(count(self::$user_ID_list)>0)
            $q['where']['user_id']=self::$user_ID_list;
        else if(!empty(self::$user_ID))
            $q['where']['user_id']=[
                'method'        =>'<',
                'value'         =>self::$user_ID
            ];

        if(
              !is_null(self::$start)
            ||!is_null(self::$len)
        ){

            if(
                  !is_null(self::$start)
                &&!is_null(self::$len)
            )
                $q['limit']=[
                    self::$start,
                    self::$len
                ];
            else if(!is_null(self::$len))
                $q['limit']=self::$len;

        }

        if(!empty(self::$user_ID_list))
            $q['where']['user_id']=self::$user_ID_list;

        $r=Db::select($q);

        foreach($r as $row){

            if(!isset(self::$list[$row['user_id']]))
                self::$list[$row['user_id']]=array(
                    'ID'                        =>$row['user_id'],
                    'image'                     =>[],
                    'login'                     =>NULL,
                    'surname'                   =>NULL,
                    'name'                      =>NULL,
                    'email'                     =>NULL,
                    'is_root'                   =>false,
                    'is_block'                  =>false,
                    'date_create'               =>NULL,
                    'date_online'               =>NULL
                );

            switch($row['user_access_type_id']){

                case 1:
                case 9:
                case 11:{

                    self::$list[$row['user_id']]['is_root']=true;

                    break;

                }

            }

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_avatar_list(){

        if(count(self::$list)==0)
            return true;
        
        $user_ID_list=array_keys(self::$list);
        
        foreach($user_ID_list as $user_ID){

            $q=array(
                'select'=>array(
                    'image_id',
                    'user_id'
                ),
                'table'=>'_user_avatar',
                'where'=>array(
                    'user_id'   =>$user_ID,
                    'type'      =>0
                ),
                'order'=>array(
                    array(
                        'column'        =>'id',
                        'direction'     =>'desc'
                    )
                )
            );
    
            $r=Db::select($q);

            if(count($r)==0)
                return false;

            $image_ID_list=[];

            foreach($r as $row)
                if(!empty($row['image_id']))
                    $image_ID_list[$row['image_id']]=$row['user_id'];

            if(count($image_ID_list)==0)
                return false;

            $image_list=ImagePathListAction::get_image_path_list(array_keys($image_ID_list),true,true);

            if(count($image_list)==0)
                return false;

            foreach($image_list as $image_ID=>$image_row){

                $user_ID=$image_ID_list[$image_ID];

                if(isset(self::$list[$user_ID]))
                    self::$list[$user_ID]['image']=$image_row;

            }

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_login(){

        if(count(self::$list)==0)
            return true;

        $q=array(
            'select'=>array(
                'user_id',
                'login'
            ),
            'table'=>'_user_login',
            'where'=>array(
                'user_id'   =>array_keys(self::$list),
                'type'      =>0
            )
        );

        $r=Db::select($q);

        foreach($r as $row)
            if(isset(self::$list[$row['user_id']]))
                self::$list[$row['user_id']]['login']=$row['login'];
        
        return true;
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_email(){

        if(count(self::$list)==0)
            return true;

        $q=array(
            'select'=>array(
                'user_id',
                'email'
            ),
            'table'=>'_user_email',
            'where'=>array(
                'user_id'   =>array_keys(self::$list),
                'type'      =>0
            )
        );

        $r=Db::select($q);

        foreach($r as $row)
            if(isset(self::$list[$row['user_id']]))
                self::$list[$row['user_id']]['email']=$row['email'];
        
        return true;
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_data(){

        if(count(self::$list)==0)
            return true;

        $q=array(
            'select'=>array(
                'user_id',
                'name',
                'surname'
            ),
            'table'=>'_user_data',
            'where'=>array(
                'user_id'   =>array_keys(self::$list),
                'type'      =>0
            )
        );

        $r=Db::select($q);

        foreach($r as $row)
            if(isset(self::$list[$row['user_id']])){

                self::$list[$row['user_id']]['name']        =$row['name'];
                self::$list[$row['user_id']]['surname']     =$row['surname'];

            }
        
        return true;
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_date(){

        if(count(self::$list)==0)
            return true;

        $q=array(
            'select'=>array(
                'id',
                'date_create',
                'date_online'
            ),
            'table'=>'_user',
            'where'=>array(
                'id'        =>array_keys(self::$list),
                'type'      =>0
            )
        );

        $r=Db::select($q);

        foreach($r as $row)
            if(isset(self::$list[$row['id']])){

                self::$list[$row['id']]['date_create']  =empty($row['date_create'])?NULL:strtotime($row['date_create']);
                self::$list[$row['id']]['date_online']  =empty($row['date_online'])?NULL:strtotime($row['date_online']);

            }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_block(){

        if(count(self::$list)==0)
            return true;

        $list=UserBlock::get_user_block_list(array_keys(self::$list));

        if(count($list)==0)
            return false;

        foreach($list as $user_ID)
            self::$list[$user_ID]['is_block']=true;

        return true;

    }

    /**
     * @return bool
     */
    private static function set_user_to_list_up(){

        if(isset(self::$list[User::$user_ID])){

            $data=[
                User::$user_ID=>self::$list[User::$user_ID]
            ];

            unset(self::$list[User::$user_ID]);

            self::$list=$data+self::$list;

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function remove_emapy(){

        $list=[];

        foreach(self::$list as $user_ID=>$row)
            if(!empty($row['email'])||!empty($row['login']))
                $list[$user_ID]=$row;

        self::$list=$list;

        return true;

    }

    /**
     * @return array
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set(){

        self::prepare_search();
        self::set_user_access_type_list();
        self::set_user_access_list();
        self::set_avatar_list();
        self::set_user_login();
        self::set_user_email();
        self::set_user_data();
        self::set_user_date();
        self::set_user_block();
        self::set_user_to_list_up();
        self::remove_emapy();

        return self::$list;

    }

    /**
     * @param array $user_access_name_list
     * @param array|NULL $user_ID_list
     * @param int|NULL $start
     * @param int|NULL $len
     * @param int|NULL $user_ID
     * @param string|NULL $search
     * @return array
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(array $user_access_name_list=[],array $user_ID_list=NULL,int $start=NULL,int $len=NULL,int $user_ID=NULL,string $search=NULL){

        if(count($user_access_name_list)==0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User access name list is empty'
            ];

            throw new ParametersException($error);

        }

        self::$user_ID                          =empty($user_ID)?NULL:$user_ID;
        self::$search                           =empty($search)?NULL:mb_strtolower($search,'utf-8');
        self::$user_access_type_name_list       =$user_access_name_list;
        self::$user_ID_list                     =empty($user_ID_list)?[]:$user_ID_list;
        self::$start                            =is_null($start)?NULL:$start;
        self::$len                              =is_null($len)?NULL:$len;

        return self::set();

    }

}