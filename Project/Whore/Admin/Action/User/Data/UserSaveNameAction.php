<?php

namespace Project\Whore\Admin\Action\User\Data;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;
use Core\Module\User\UserData;

class UserSaveNameAction{

    /** @var int */
    private static $user_data_ID;

    /** @var string */
    private static $name;

    /** @var string */
    private static $surname;

    /** @var int */
    private static $sex;

    /**
     * @return bool
     */
    private static function is_valid_name(){

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_data(){

        $data=UserData::get_user_data(User::$user_ID);

        if(empty($data))
            return true;

        self::$sex=$data['sex'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_data(){

        if(!UserData::remove_user_data(User::$user_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User data was not removed'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_data(){

        self::$user_data_ID=UserData::add_user_data(User::$user_ID,self::$name,self::$surname,self::$sex);

        if(empty(self::$user_data_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User data was not added'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_return(){

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::is_valid_name())
            if(self::set_user_data())
                if(self::remove_user_data())
                    if(self::add_user_data())
                        return self::set_return();

        return false;

    }

    /**
     * @param string|NULL $name
     * @param string|NULL $surname
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $name=NULL,string $surname=NULL){

        $error_info_list=[];

        if(empty($name))
            $error_info_list['name']='Name is empty';

        if(empty($surname))
            $error_info_list['suranme']='Suranme is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$name     =$name;
        self::$surname  =$surname;

        return self::set();

    }

}