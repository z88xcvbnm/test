<?php

namespace Project\Whore\Admin\Action\User\Data;

use Core\Module\Email\EmailSend;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Session\Session;
use Core\Module\Token\Token;
use Core\Module\Url\Url;
use Core\Module\User\UserAuth;
use Core\Module\User\UserBlock;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Core\Module\User\UserHash;
use Core\Module\User\UserHashType;
use Core\Module\User\UserHashTypeConfig;
use Core\Module\User\UserLogin;
use Core\Module\User\UserLoginValidation;
use Core\Module\User\UserPasswordHistory;
use Core\Module\User\UserRemove;

class UserSaveRecoveryPasswordAction{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_hash_ID;

    /** @var int */
    private static $user_hash_type_ID;

    /** @var int */
    private static $user_login_ID;

    /** @var string */
    private static $login;

    /** @var string */
    private static $email;

    /** @var string */
    private static $name;

    /** @var string */
    private static $surname;

    /** @var string */
    private static $hash;

    /** @var string */
    private static $password;

    /** @var string */
    private static $password_old;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_password(){

        if(!UserLoginValidation::is_valid_user_password(self::$password)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>array(
                    'password'=>'Password is not valid'
                )
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_hash_type(){

        self::$user_hash_type_ID=UserHashTypeConfig::get_user_hash_type_ID('recovery_password');

        if(empty(self::$user_hash_type_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>array(
                    'hash_type'     =>'Hash type for recovery password is not exists'
                )
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_hash(){

        $data=UserHash::get_user_hash_data_from_hash(self::$hash,self::$user_hash_type_ID);

        if(empty($data)){

            $error=array(
                'title'     =>'Access denied',
                'info'      =>array(
                    'hash'=>'Hash is not exists'
                )
            );

            throw new AccessDeniedException($error);

        }

        self::$user_ID          =$data['user_ID'];
        self::$user_hash_ID     =$data['ID'];

        return true;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_block(){

        if(UserBlock::isset_user_block(self::$user_ID)){

            $error=array(
                'title'     =>'Access denied',
                'info'      =>array(
                    'user_block'        =>'User was blocked'
                )
            );

            throw new AccessDeniedException($error);

        }

        return false;

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_remove(){

        if(UserRemove::isset_user_remove(self::$user_ID)){

            $error=array(
                'title'     =>'Access denied',
                'info'      =>array(
                    'user_remove'=>'User was removed'
                )
            );

            throw new AccessDeniedException($error);

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_data(){

        $data=UserData::get_user_data(self::$user_ID);

        if(empty($data))
            return true;

        self::$name     =$data['name'];
        self::$surname  =$data['surname'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_login(){

        $data=UserLogin::get_user_login_data(self::$user_ID);

        if(empty($data)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>array(
                    'user_login_data'=>'User login data is empty'
                )
            );

            throw new PhpException($error);

        }

        self::$user_login_ID    =$data['ID'];
        self::$login            =$data['login'];
        self::$password_old     =$data['password'];

        return true;

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_email(){

        self::$email=UserEmail::get_user_email_last(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_login_password_history(){

        $user_login_history_ID=UserPasswordHistory::add_user_password_history(self::$user_ID,self::$password_old);

        if(empty($user_login_history_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>array(
                    'user_login_history'=>'User login history was not added'
                )
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_login(){

        if(!UserLogin::remove_user_login_ID(self::$user_login_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>array(
                    'user_login'=>'User login was not removed'
                )
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_login(){

        self::$user_login_ID=UserLogin::add_user_login(self::$user_ID,self::$login,self::$password);

        if(empty(self::$user_login_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>array(
                    'user_login'=>'User login was not added'
                )
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_user_hash(){

        if(!UserHash::update_user_hash_use_from_user_hash_ID(self::$user_hash_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>array(
                    'user_login'=>'User hash was not updated'
                )
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_email(){

        if(empty(self::$email))
            return false;

        $list=[];

        $hi_line='Здравствуйте';

        if(!empty(self::$surname)||!empty(self::$name)){

            $hi_line.=',';

            if(!empty(self::$surname))
                $hi_line.=' '.self::$surname;

            if(!empty(self::$name))
                $hi_line.=' '.self::$name;

        }

        $list[]=$hi_line;

        $list[]='';
        $list[]='Ваш пароль обновлен.';
        $list[]='';
        $list[]='С уважением,';
        $list[]='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

        if(!EmailSend::init(array(self::$email),'Восстановление пароля',implode("<br />\n",$list),\Config::$email_no_replay)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>array(
                    'email_send'=>'Email was not send'
                )
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_auth(){

        return (bool)UserAuth::add_user_auth(self::$user_ID,Token::$token_ID,Session::$session_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_token(){

        return (bool)Token::update_token_user_ID(Token::$token_ID,self::$user_ID);

    }

    /**
     * @return array
     */
    private static function set_return(){

        return array(
            'login'     =>self::$login,
            'name'      =>self::$name,
            'surname'   =>self::$surname
        );

    }

    /**
     * @return array|null
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::is_valid_password())
            if(self::isset_user_hash_type())
                if(self::isset_user_hash())
                    if(!self::isset_user_block())
                        if(!self::isset_user_remove()){

                            self::set_user_data();
                            self::set_user_login();
                            self::set_user_email();

                            self::remove_user_login();

                            self::add_user_login_password_history();
                            self::add_user_login();

                            self::update_user_hash();

//                            self::update_token();
//                            self::set_user_auth();

                            self::send_email();

                            return self::set_return();

                        }

        return NULL;

    }

    /**
     * @param string|NULL $hash
     * @param string|NULL $password
     * @return array|null
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $hash=NULL,string $password=NULL){

        $error_info_list=[];

        if(empty($hash))
            $error_info_list['hash']='Hash is empty';

        if(empty($password))
            $error_info_list['password']='Password is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$hash         =$hash;
        self::$password     =Hash::get_sha1_encode($password);

        return self::set();

    }

}