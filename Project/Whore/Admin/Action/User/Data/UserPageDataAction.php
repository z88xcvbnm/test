<?php

namespace Project\Whore\Admin\Action\User\Data;

use Core\Module\Image\Image;
use Core\Module\Image\ImageItem;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserAvatar;
use Core\Module\User\UserBalance;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Action\Image\ImagePathAction;
use Project\Whore\All\Module\Price\PriceConfig;

class UserPageDataAction{

    /** @var int */
    private static $image_ID;

    /** @var string */
    private static $image_dir;

    /** @var array */
    private static $image_item_ID_list;

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_image_data(){

        if(empty(User::$user_ID))
            return true;

        self::$image_ID=UserAvatar::get_user_avatar_image_ID(User::$user_ID);

        if(empty(self::$image_ID))
            return false;

        $r=ImagePathAction::get_image_path_data(self::$image_ID);

        self::$image_dir            =$r['image_dir'];
        self::$image_item_ID_list   =$r['image_item_ID_list'];

//        self::$image_dir            =Image::get_image_dir_from_image_ID(self::$image_ID);
//        self::$image_item_ID_list   =ImageItem::get_image_item_ID_list(self::$image_ID,true);
//
//        print_r(self::$image_item_ID_list);

        return true;

    }

    /**
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_data(){

        self::set_image_data();

        $data=array(
            'login'                         =>stripslashes(UserLogin::get_user_login_default()),
            'is_root'                       =>UserAccess::$is_root,
            'is_admin'                      =>UserAccess::$is_admin,
            'is_profile'                    =>UserAccess::$is_profile,
            'is_profile_wallet'             =>UserAccess::$is_profile_wallet,
            'image_ID'                      =>(int)self::$image_ID,
            'image_dir'                     =>self::$image_dir,
            'image_item_ID_list'            =>self::$image_item_ID_list
        );

        if(
              UserAccess::$is_profile
            ||UserAccess::$is_profile_wallet
        ){

            $data['balance']        =UserBalance::get_user_balance(User::$user_ID);
            $data['price']          =PriceConfig::$search_price;

        }

        return $data;

    }

}