<?php

namespace Project\Whore\Admin\Action\User\Data;

use Core\Module\Email\EmailValidation;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;
use Core\Module\User\UserEmail;

class UserSaveEmailAction{

    /** @var int */
    private static $user_email_ID;

    /** @var string */
    private static $email;

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function is_valid_email(){

        if(!EmailValidation::is_valid_email(self::$email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Email is not valid'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function remove_user_email(){

        if(!UserEmail::remove_user_email(User::$user_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User email was not removed'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function isset_email_conflict(){

        if(UserEmail::isset_user_email_in_active_list(self::$email,User::$user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Email already use other user'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function add_user_email(){

        self::$user_email_ID=UserEmail::add_user_email(User::$user_ID,self::$email);

        if(empty(self::$user_email_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User email was not added'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_return(){

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    private static function set(){

        if(self::is_valid_email())
            if(self::isset_email_conflict())
                if(self::remove_user_email())
                    if(self::add_user_email())
                        return self::set_return();

        return false;

    }

    /**
     * @param string|NULL $email
     * @return bool
     * @throws ParametersException
     */
    public  static function init(string $email=NULL){

        if(empty($email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Email is empty'
            );

            throw new ParametersException($error);

        }

        self::$email=$email;

        return self::set();

    }

}