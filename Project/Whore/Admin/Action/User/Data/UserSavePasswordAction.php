<?php

namespace Project\Whore\Admin\Action\User\Data;

use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;
use Core\Module\User\UserLogin;
use Core\Module\User\UserLoginValidation;
use Core\Module\User\UserPasswordHistory;

class UserSavePasswordAction{

    /** @var int */
    private static $user_login_ID;

    /** @var string */
    private static $login;

    /** @var string */
    private static $password_old;

    /** @var string */
    private static $password_new;

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function is_valid_password(){

        $error_info_list=[];

        if(!UserLoginValidation::check_user_password_length(self::$password_old))
            $error_info_list['password_old']='Old password is not valid';

        if(!UserLoginValidation::check_user_password_length(self::$password_new))
            $error_info_list['password_new']='New password is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function is_valid_password_old(){

        if(!UserLogin::check_user_password(User::$user_ID,self::$password_old)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Old password is not valid'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    private static function get_user_login_data(){

        $data=UserLogin::get_user_login_data(User::$user_ID);

        if(empty($data)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User data is empty'
            );

            throw new PhpException($error);

        }

        self::$login            =$data['login'];
        self::$password_old     =$data['password'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    private static function remove_user_login(){

        if(!UserLogin::remove_user_login_from_user_ID(User::$user_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User login was not removed'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function add_user_password_to_history(){

        if(!UserPasswordHistory::add_user_password_history(User::$user_ID,self::$password_old)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User password history was not added'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    private static function add_user_login(){

        self::$user_login_ID=UserLogin::add_user_login(User::$user_ID,self::$login,self::$password_new);

        if(empty(self::$user_login_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User password was not updated'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_return(){

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    private static function set(){

        if(self::is_valid_password())
            if(self::is_valid_password_old())
                if(self::get_user_login_data())
                    if(self::add_user_password_to_history())
                        if(self::remove_user_login())
                            if(self::add_user_login())
                                return self::set_return();

        return false;

    }

    /**
     * @param string|NULL $password_old
     * @param string|NULL $password_new
     * @return bool
     * @throws ParametersException
     */
    public  static function init(string $password_old=NULL,string $password_new=NULL){

        $error_info_list=[];

        if(empty($password_old))
            $error_info_list['password_old']='Old password is empty';

        if(empty($password_new))
            $error_info_list['password_new']='New password is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$password_old     =Hash::get_sha1_encode($password_old);
        self::$password_new     =Hash::get_sha1_encode($password_new);

        return self::set();

    }

}