<?php

namespace Project\Whore\Admin\Action\User\Data;

use Core\Module\Exception\PhpException;
use Core\Module\User\UserAccess;
use Core\Module\User\UserAccessType;

class UserLenAction{

    /**
     * @return int
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_company_profile_len(){

        $user_access_type_ID=UserAccessType::get_user_access_type_ID('company_profile');

        if(empty($user_access_type_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User access type ID is empty'
            ];

            throw new PhpException($error);

        }

        return UserAccess::get_user_len_from_access_type_ID($user_access_type_ID);

    }

}