<?php

namespace Project\Whore\Admin\Action\Video;

use Core\Module\Video\Video;

class VideoPathListAction{

    /** @var array */
    private static $video_ID_list=[];

    /** @var array */
    private static $video_list=[];

    /** @var bool */
    private static $is_assoc;

    /** @var bool */
    private static $is_file_extension;

    /** @var bool */
    private static $is_server_link;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$video_list               =[];
        self::$is_assoc                 =NULL;
        self::$is_file_extension        =NULL;
        self::$is_server_link           =NULL;

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_video_list(){

        $video_dir_list=Video::get_video_path_list(self::$video_ID_list,self::$is_server_link);

        foreach($video_dir_list as $video_ID=>$video_path)
            self::$video_list[$video_ID]=array(
                'video_ID'              =>$video_ID,
                'video_path'            =>$video_path
            );

        if(!self::$is_assoc)
            self::$video_list=array_values(self::$video_list);

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        return self::$video_list;

    }

    /**
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_video_list();

        return self::set_return();

    }

    /**
     * @param array $video_ID_list
     * @param bool $is_assoc
     * @param bool $is_file_extension
     * @param bool $is_server_link
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_path_list(array $video_ID_list=[],bool $is_assoc=false,bool $is_file_extension=false,bool $is_server_link=false){

        if(count($video_ID_list)==0)
            return[];

        self::reset_data();

        self::$video_ID_list        =$video_ID_list;
        self::$is_assoc             =$is_assoc;
        self::$is_file_extension    =$is_file_extension;
        self::$is_server_link       =$is_server_link;

        return self::set();

    }

}