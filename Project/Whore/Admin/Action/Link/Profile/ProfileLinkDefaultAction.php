<?php

namespace Project\Whore\Admin\Action\Link\Profile;

use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserLogin;
use Project\Whore\Admin\Config\PageConfigAdminWhore;
use Project\Whore\Admin\Config\PageConfigProfileWhore;
use Project\Whore\Admin\Content\ContentAdminWhore;

class ProfileLinkDefaultAction{

    /**
     * @return string
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_profile_link_default(){

        if(User::is_login()){

            $user_login=UserLogin::get_user_login_default();

            if(empty($user_login))
                return '/'.PageConfigAdminWhore::$root_unreg_page_default;
            else{

                if(
                      UserAccess::$is_profile
                    ||UserAccess::$is_profile_wallet
                ){

                    $profile_link   ='/profile/'.$user_login;
                    $link_default   =$profile_link.'/'.PageConfigProfileWhore::$root_profile_face_page_default;

                }

                if(empty($link_default))
                    return '/'.PageConfigAdminWhore::$root_unreg_page_default;

                return $link_default;

            }

        }
        else
            return '/'.PageConfigAdminWhore::$root_unreg_page_default;

    }

    /**
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_profile_page_title_and_link_default(){

        if(User::is_login()){

            $user_login=UserLogin::get_user_login_default();

            if(!empty($user_login)){

                $profile_link='/profile/'.$user_login;

                if(UserAccess::$is_profile||UserAccess::$is_root)
                    return array(
                        ContentAdminWhore::get_content('dashboard_page_title'),
                        '/'.$profile_link.'/'.PageConfigAdminWhore::$root_profile_page_default
                    );

            }

        }

        return array(
            ContentAdminWhore::get_content('auth_page_title'),
            '/'.PageConfigAdminWhore::$root_unreg_page_default
        );

    }

}