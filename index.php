<?php

if(!empty($_REQUEST['print_server'])){

    print_r($_SERVER);

    exit;

}

define('DIR_ROOT',__DIR__);

set_include_path(DIR_ROOT);

require_once(__DIR__."/Core/Module/Autoloader/Autoloader.php");

\Core\Module\Worktime\Worktime::start();

\Core\Module\Php\Php::init();

\Core\Module\Exception\CustomizeException::init();
//\Core\Module\Error\CustomizeError::init();

\Core\Module\Install\Install::init();

\Core\Module\Db\Db::init();
\Core\Module\Route\Route::init();

//\Migrate\Whore\MysqlConnect::init();
//\Migrate\Whore\Migrate::init();

\Core\Module\Worktime\Worktime::finish();

//if(Config::$is_debug)
//    print_r(\Core\Module\Worktime\Worktime::get_full_info());