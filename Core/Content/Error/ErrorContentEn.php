<?php

namespace Core\Content\Error;

class ErrorContentEn{

    /** @var array */
    private static $content_list=array(
        'bad_request'                   =>'Bad request',
        'unauthorized'                  =>'Unauthorized',
        'access_denied'                 =>'Access denied',
        'user_blocked'                  =>'User blocked',
        'user_removed'                  =>'User removed',
        'forbidden'                     =>'Forbidden',
        'not_found'                     =>'Not found',
        'method_not_allowed'            =>'Method not allowed',
        'unprocessable_entity'          =>'Unprocessable entity',
        'server_error'                  =>'Server error',
        'bad_getway'                    =>'Bad getway',
        'service_unavailable'           =>'Service unavailable',
        'unkonown_error'                =>'Unknown error',
        'parameters_are_not_valid'      =>'Parameters are not valid',
        'parameters_are_not_exists'     =>'Parameters are not exists',
        'action_is_not_exists'          =>'Action is not exists'
    );

    /** @var string */
    private static $content_default='Unknown error';

    /**
     * @param string|NULL $key
     * @return string
     */
    public  static function init(string $key=NULL){

        if(empty($key))
            return self::$content_default;

        if(empty(self::$content_list[$key]))
            return self::$content_default;

        return self::$content_list[$key];

    }

}