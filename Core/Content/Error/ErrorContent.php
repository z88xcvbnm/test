<?php

namespace Core\Content\Error;

use Core\Module\Dir\DirConfig;
use Core\Module\Exception\PathException;
use Core\Module\Exception\SystemException;
use Core\Module\Lang\Lang;
use Core\Module\Lang\LangConfig;

class ErrorContent{

    /** @var string */
    private static $lang_key_default    ='en';

    /** @var array */
    private static $lang_list           =array(
        'ru',
        'en'
    );

    /**
     * @var array
     */
    private static $code_list=array(
        'bad_request'                   =>400,
        'unauthorized'                  =>401,
        'access_denied'                 =>403,
        'user_blocked'                  =>403,
        'user_removed'                  =>403,
        'forbidden'                     =>403,
        'not_found'                     =>404,
        'method_not_allowed'            =>405,
        'unprocessable_entity'          =>422,
        'server_error'                  =>500,
        'bad_nettopway'                 =>502,
        'service_unavailable'           =>503,
        'unkonown_error'                =>520,
    );

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function bad_request(array $data=NULL){

        return self::get_error('bad_request',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function unauthorized(array $data=NULL){

        return self::get_error('unauthorized',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function access_denied(array $data=NULL){

        return self::get_error('access_denied',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function user_blocked(array $data=NULL){

        return self::get_error('user_blocked',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function user_removed(array $data=NULL){

        return self::get_error('user_removed',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function forbidden(array $data=NULL){

        return self::get_error('forbidden',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function not_found(array $data=NULL){

        return self::get_error('not_found',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function method_not_allowed(array $data=NULL){

        return self::get_error('method_not_allowed',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function unprocessable_entity(array $data=NULL){

        return self::get_error('unprocessable_entity',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function server_error(array $data=NULL){

        return self::get_error('server_error',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function bad_nettopway(array $data=NULL){

        return self::get_error('bad_nettopway',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function service_unavailable(array $data=NULL){

        return self::get_error('service_unavailable',$data);

    }

    /**
     * @param array|NULL $data
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function unkonown_error(array $data=NULL){

        return self::get_error('unkonown_error',$data);

    }

    /**
     * @param string|NULL $key
     * @param array|NULL $info
     * @return array
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function get_error(string $key=NULL,array $info=NULL){
        
        $data=array(
            'code'      =>self::get_code($key),
            'title'     =>self::get_content($key)
        );

        if(!empty($info)){

            if(isset($info['title']))
                unset($info['title']);

            $data['data']=$info;

        }

        return $data;
        
    }

    /**
     * @param string|NULL $key
     * @return int
     */
    public  static function get_code(string $key=NULL){
        
        if(isset(self::$code_list[$key]))
            return self::$code_list[$key];
        
        return 0;
        
    }

    /**
     * @param string|NULL $key
     * @return mixed
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function get_content(string $key=NULL){

        if(empty(Lang::$lang_key)){

            Lang::set_lang_ID_default(LangConfig::$lang_ID_default);
            Lang::set_lang_key_default(LangConfig::$lang_key_default);

        }

        if(array_search(Lang::$lang_key,self::$lang_list)!==false){

            $file_path=DIR_ROOT.'/Core/Content/Error/ErrorContent'.ucfirst(Lang::$lang_key).'.php';

            if(file_exists($file_path))
                $class_name='\Core\Content\Error\ErrorContent'.ucfirst(Lang::$lang_key);

        }

        if(empty($class_name)){

//            $file_path      =DirConfig::$dir_root.'/Core/Content/Error/ErrorContent'.ucfirst(self::$lang_key_default).'.php';
            $file_path      =DIR_ROOT.'/Core/Content/Error/ErrorContent'.ucfirst(self::$lang_key_default).'.php';
            $class_name     ='\Core\Content\Error\ErrorContent'.ucfirst(self::$lang_key_default);

            if(!file_exists($file_path)){

                $error=array(
                    'title'     =>'System problem',
                    'info'      =>'Error file is not exists'
                );

                throw new PathException($error);

            }

        }

        return $class_name::init($key);

    }

}