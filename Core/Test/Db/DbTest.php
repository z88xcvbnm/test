<?php

namespace Core\Test\Db;

use Core\Module\Db\Db;

class DbTest{

    private static function select_city(){

        $data=array(
            'table'=>array('name'=>'_city'),
            'select'=>array(
                array(
                    'column'=>'name_ru'
                ),
                array(
                    'column'=>'name_en'
                ),
                array(
                    'column'=>'type'
                )
            ),
            'where'=>array(
                array(
                    'column'=>'type',
                    'value'=>0
                )
            )
        );

        print_r(Db::select($data));

    }
    private static function update_date_create_of_city(){

        $data=array(
            'table'=>array('name'=>'_city'),
            'set'=>array(
                array(
                    'column'=>'date_remove',
                    'value'=>'NULL'
                )
            ),
            'where'=>array(
                array(
                    'column'=>'type',
                    'value'=>0
                )
            )
        );

        print_r(json_encode(Db::update($data)));

    }
    private static function select_city_join_country(){

        $data=array(
            'table'=>array('name'=>'_city'),
            'join'=>array(
                array(
                    'table'=>'_country',
                    'where'=>array(
                        array(
                            'table'=>'_city',
                            'column'=>'country_id',
                            'method'=>'=',
                            'table_join'=>'_country',
                            'column_join'=>'id'
                        )
                    )
                )
            ),
            'select'=>array(
                array(
                    'table'     =>'_city',
                    'column'    =>'name_ru'
                ),
                array(
                    'table'     =>'_city',
                    'column'    =>'name_en'
                ),
                array(
                    'table'     =>'_country',
                    'column'    =>'name_ru',
                    'rename'    =>'country_name_ru'
                ),
                array(
                    'table'     =>'_country',
                    'column'    =>'name_en',
                    'rename'    =>'country_name_en'
                ),
            ),
            'where'=>array(
                array(
                    'table'     =>'_city',
                    'column'    =>'type',
                    'value'     =>0
                ),
                array(
                    'table'     =>'_country',
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>10
        );

        print_r(json_encode(Db::select($data)));

    }
    public static function insert_session(){

        $data=array(
            'table'     =>'_session',
            'values'    =>array(
                array(
                    'user_id'   =>111,
                    'ip_id'     =>7
                ),
                array(
                    'user_id'   =>111231,
                    'ip_id'     =>712321
                ),
                array(
                    'user_id'   =>11231,
                    'ip_id'     =>7
                ),
                array(
                    'user_id'   =>111,
                    'ip_id'     =>712312
                ),
                array(
                    'user_id'   =>111231,
                    'ip_id'     =>71231
                )
            )
        );

        print_r(Db::insert($data));

    }

    public static function init(){

//        self::select_city();
//        self::select_city_join_country();
//        self::update_date_create_of_city();
//        self::insert_session();

    }

}