<?php

namespace Core\Test\LangRefactor;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Lang\Lang;

class LangRefactor{

    private static $lang_key_list=array(
        'ru'=>0,
        'en'=>0,
        'de'=>0,
        'fr'=>0,
        'es'=>0,
        'ja'=>0,
        'pt'=>0,
        'zh'=>0
    );
    private static $select_len=500;

    private static function set_localization(string $table_name=NULL,array $lang_key_list=[]){

        if(empty($table_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Table name is empty'
            );

            throw new ParametersException($error);

        }

        if(count($lang_key_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key list is empty'
            );

            throw new ParametersException($error);

        }

        $select_list=array(
            array(
                'column'=>'id'
            )
        );

        foreach($lang_key_list as $key)
            $select_list[]=array(
                'column'=>'name_'.$key
            );

        if(count($select_list)>0){

            $offset=0;

            do{

                $q=array(
                    'select'=>$select_list,
                    'table'=>'_'.$table_name,
                    'limit'=>array($offset,self::$select_len)
                );

                $r=Db::select($q);

                if(count($r)==0)
                    break;

                $value_list=[];

                foreach($r as $row){

                    $ID=$row['id'];

                    foreach($lang_key_list as $lang_key)
                        if(!empty($row['name_'.$lang_key])){

                            if($table_name=='lang')
                                $value_list[]=array(
                                    $table_name.'_id'=>$ID,
                                    'content_lang_id'=>$lang_key=='default'?0:self::$lang_key_list[$lang_key],
                                    'name'=>$row['name_'.$lang_key]
                                );
                            else
                                $value_list[]=array(
                                    $table_name.'_id'=>$ID,
                                    'lang_id'=>self::$lang_key_list[$lang_key],
                                    'name'=>$row['name_'.$lang_key]
                                );

                        }

                }

                if(count($value_list)>0){

                    $q=array(
                        'table'=>'_'.$table_name.'_localization',
                        'values'=>$value_list
                    );

                    $r=Db::insert($q);

                    if(count($r)==0){

                        $error=array(
                            'title'     =>'Parameters problem',
                            'info'      =>'Continent localization was not added'
                        );

                        throw new ParametersException($error);

                    }

                }

                $offset+=self::$select_len;

            }while(count($r)>0);

        }

    }

    private static function set_continent_list(){

//        self::set_localization('continent',array_keys(self::$lang_key_list));
        self::set_localization('country',array_keys(self::$lang_key_list));
        self::set_localization('region',array_keys(self::$lang_key_list));
        self::set_localization('city',array_keys(self::$lang_key_list));
        self::set_localization('lang',array('ru','en','default'));

    }
    private static function set_lang_ID_list(){

        self::$lang_key_list=Lang::get_lang_ID_list(array_keys(self::$lang_key_list),true);

        self::set_continent_list();

    }

    private static function set(){

        self::set_lang_ID_list();

    }

    public  static function init(){

        self::set();

    }

}