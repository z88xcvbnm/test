<?php

namespace Core\Action\Error;

use Core\Module\Email\EmailSend;
use Core\Module\Exception\ParametersException;

class SendErrorSystemAction{

    /** @var array */
    private static $data;

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function send_error(){

        $list=[];

        $list[]='Date: '.date("Y-m-d H:i:s");
        $list[]='';
        $list[]=print_r(self::$data,true);

        return EmailSend::init(\Config::$email_log,'Mobile Error','<pre>'.implode("<br />\n",$list).'</pre>',\Config::$email_no_replay);

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=array(
            'success'=>true
        );

        return $data;

    }

    /**
     * @return array
     */
    private static function set(){

        self::send_error();

        return self::set_return();

    }

    /**
     * @param array $data
     * @return array
     * @throws ParametersException
     */
    public  static function init(array $data=[]){

        if(count($data)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Data is empty'
            );

            throw new ParametersException($error);

        }

        self::$data=$data;

        return self::set();

    }

}