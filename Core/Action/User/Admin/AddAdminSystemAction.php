<?php

namespace Core\Action\User\Admin;

use Core\Module\Email\EmailSend;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\OsServer\OsServer;
use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserAccessTypeConfig;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Core\Module\User\UserHash;
use Core\Module\User\UserHashLinkConfig;
use Core\Module\User\UserHashTypeConfig;
use Core\Module\User\UserLogin;

class AddAdminSystemAction{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_login_ID;

    /** @var int */
    private static $user_access_ID;

    /** @var int */
    private static $user_email_ID;

    /** @var int */
    private static $user_data_ID;

    /** @var int */
    private static $user_hash_ID;

    /** @var int */
    private static $user_hash_type_ID;

    /** @var int */
    private static $user_access_type_ID;

    /** @var string */
    private static $user_login;

    /** @var string */
    private static $user_name;

    /** @var string */
    private static $user_surname;

    /** @var string */
    private static $user_email;

    /** @var string */
    private static $user_hash_invite;

    /** @var string */
    private static $user_access_type_name;

    /** @var string */
    private static $user_hash_type_name;

    /** @var bool */
    private static $need_send_invite;

    /** @var string */
    private static $link;

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_access(){

        if(
              !UserAccess::$is_root
            &&!UserAccess::$is_admin
        ){

            $error=[
                'title'     =>AccessDeniedException::$title,
                'info'      =>'Access denied'
            ];

            throw new AccessDeniedException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_user_hash_type(){

        self::$user_hash_type_ID=UserHashTypeConfig::get_user_hash_type_ID(self::$user_hash_type_name);

        if(empty(self::$user_hash_type_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'User hash type name is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_user_access_type(){

        self::$user_access_type_ID=UserAccessTypeConfig::get_user_access_type_ID(self::$user_access_type_name);

        if(empty(self::$user_access_type_ID)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'User access type name is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_login(){

        if(UserLogin::isset_user_login_in_active(self::$user_login,User::$user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User login already exists'
            ];

            throw new ParametersValidationException($error);

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_email(){

        if(UserEmail::isset_user_email_in_active_list(self::$user_email,User::$user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User email already exists'
            ];

            throw new ParametersValidationException($error);

        }

        return false;

    }

    /**
     * @return bool
     */
    private static function set_user_hash_invite(){

        $user_hash_invite_list=[
            User::$user_ID,
            self::$user_login,
            self::$user_email,
            self::$user_name,
            self::$user_surname,
            time(),
            rand(0,time())
        ];

        self::$user_hash_invite=Hash::get_sha1_encode(implode(':',$user_hash_invite_list));

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user(){

        self::$user_ID=User::add_user();

        if(empty(self::$user_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_email(){

        self::$user_email_ID=UserEmail::add_user_email(self::$user_ID,self::$user_email);

        if(empty(self::$user_email_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User email was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_data(){

        self::$user_data_ID=UserData::add_user_data(self::$user_ID,self::$user_name,self::$user_surname,NULL);

        if(empty(self::$user_data_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User data was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_login(){

        self::$user_login_ID=UserLogin::add_user_login(self::$user_ID,self::$user_login,NULL);

        if(empty(self::$user_login_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User login was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_access(){

        self::$user_access_ID=UserAccess::add_user_access(self::$user_ID,[self::$user_access_type_name]);

        if(empty(self::$user_access_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User access was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_hash_invite(){

        $user_hash_type_ID      =UserHashTypeConfig::get_user_hash_type_ID('registration_invite');
        self::$user_hash_ID     =UserHash::add_user_hash(self::$user_ID,$user_hash_type_ID,NULL,self::$user_hash_invite);

        if(empty(self::$user_access_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User hash invite was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_invite(){

        if(!self::$need_send_invite)
            return false;

        $inner  =[];
        $link   =UserHashLinkConfig::get_link_full('registration_invite').'/'.self::$user_hash_invite;

        if(OsServer::$is_windows)
            self::$link=$link;

        $inner[]='Здравствуйте!';
        $inner[]='';
        $inner[]='Для подтверждения регистрации, пожалуйста, перейдите по ссылке: <a href="'.$link.'">'.$link.'</a>';
        $inner[]='';
        $inner[]='С уважением,';
        $inner[]='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

        return EmailSend::init(array(self::$user_email),'Регистрация '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'user_ID'               =>self::$user_ID,
            'user_login_ID'         =>self::$user_login_ID,
            'user_email_ID'         =>self::$user_email_ID,
            'user_access_ID'        =>self::$user_access_ID,
            'user_data_ID'          =>self::$user_data_ID,
            'user_hash_ID'          =>self::$user_hash_ID,
            'user_hash_invite'      =>self::$user_hash_invite
        ];

        if(!empty(self::$link))
            $data['link']=self::$link;

        return $data;

    }

    /**
     * @return array|null
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::is_valid_access())
            if(self::is_valid_user_access_type())
                if(self::is_valid_user_hash_type())
                    if(!self::isset_login())
                        if(!self::isset_email()){

                            self::add_user();
                            self::add_user_access();
                            self::add_user_login();
                            self::add_user_email();
                            self::add_user_data();
                            self::set_user_hash_invite();
                            self::add_user_hash_invite();
                            self::send_invite();

                            return self::set_return();

                        }

        return NULL;

    }

    /**
     * @param string|NULL $user_login
     * @param string|NULL $user_email
     * @param string|NULL $user_name
     * @param string|NULL $user_surname
     * @param string|NULL $user_hash_type_name
     * @param string|NULL $user_access_type_name
     * @param bool $need_send_invite
     * @return array|null
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(
        string $user_login              =NULL,
        string $user_email              =NULL,
        string $user_name               =NULL,
        string $user_surname            =NULL,
        string $user_hash_type_name     =NULL,
        string $user_access_type_name   =NULL,
        bool $need_send_invite          =true
    ){

        $error_info_list=[];

        if(empty($user_login))
            $error_info_list[]='User login is empty';

        if(empty($user_email))
            $error_info_list[]='User email is empty';

        if(empty($user_name))
            $error_info_list[]='User name is empty';

        if(empty($user_surname))
            $error_info_list[]='User surname is empty';

        if(empty($user_access_type_name))
            $error_info_list[]='User access type name is empty';

        if(empty($user_hash_type_name))
            $error_info_list[]='User hash type name is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$user_login               =mb_strtolower($user_login);
        self::$user_email               =mb_strtolower($user_email);
        self::$user_name                =$user_name;
        self::$user_surname             =$user_surname;
        self::$user_access_type_name    =$user_access_type_name;
        self::$user_hash_type_name      =$user_hash_type_name;
        self::$need_send_invite         =$need_send_invite;

        return self::set();

    }

}