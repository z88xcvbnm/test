<?php

namespace Core\Action\User\Admin;

use Core\Module\Exception\ParametersException;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserAddress;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Core\Module\User\UserHash;
use Core\Module\User\UserLogin;
use Core\Module\User\UserPhone;
use Core\Module\User\UserRemove;

class RemoveUserAdminSystemAction{

    /** @var int */
    private static $user_ID;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user(){

        return User::remove_user_ID(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_access(){

        return UserAccess::remove_user_access(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function remove_user_address(){

        return UserAddress::remove_user_address(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_data(){

        return UserData::remove_user_data(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_email(){

        return UserEmail::remove_user_email(self::$user_ID);

    }

    /**
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_user_login(){

        return UserLogin::remove_user_login_from_user_ID(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function remove_user_hash(){

        return UserHash::remove_user_hash(self::$user_ID);

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function remove_user_phone(){

        return UserPhone::remove_user_phone(self::$user_ID);

    }

    /**
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    private static function add_user_remove(){

        return UserRemove::add_user_remove(User::$user_ID,self::$user_ID,'Admin removed your profile');

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::remove_user();
        self::remove_user_access();
        self::remove_user_address();
        self::remove_user_data();
        self::remove_user_email();
        self::remove_user_login();
        self::remove_user_hash();
        self::remove_user_phone();

        self::add_user_remove();

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        self::$user_ID=$user_ID;

        return self::set();

    }

}