<?php

namespace Core\Action\User\Profile;

use Core\Module\Email\EmailSend;
use Core\Module\Email\EmailValidation;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\OsServer\OsServer;
use Core\Module\Url\Url;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserAccessTypeValidation;
use Core\Module\User\UserBalance;
use Core\Module\User\UserData;
use Core\Module\User\UserEmail;
use Core\Module\User\UserHash;
use Core\Module\User\UserHashLinkConfig;
use Core\Module\User\UserHashTypeConfig;
use Core\Module\User\UserLogin;

class AddProfileSystemAction{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $user_login_ID;

    /** @var int */
    private static $user_data_ID;

    /** @var int */
    private static $user_email_ID;

    /** @var int */
    private static $user_balance_ID;

    /** @var int */
    private static $user_hash_ID;

    /** @var int */
    private static $user_hash_type_ID;

    /** @var string */
    private static $user_invite_hash;

    /** @var string */
    private static $user_hash_type_name;

    /** @var string */
    private static $user_email;

    /** @var string */
    private static $user_login;

    /** @var string */
    private static $user_name;

    /** @var string */
    private static $user_surname;

    /** @var string */
    private static $link;

    /** @var array */
    private static $user_access_type_list=[];

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function is_valid_user_email(){

        if(!EmailValidation::is_valid_email(self::$user_email)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'User email is not valid'
            ];

            throw new ParametersValidationException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_user_email(){

        if(UserEmail::isset_user_email_in_all_list(self::$user_email)){

            $error=[
                'title'     =>ParametersValidationException::$title,
                'info'      =>'User email already exists'
            ];

            throw new ParametersValidationException($error);

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_login(){

        if(empty(self::$user_login))
            return false;

        if(UserLogin::isset_user_login_in_active(self::$user_login)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User login already exists'
            ];

            throw new ParametersValidationException($error);

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_hash_type_ID(){

        self::$user_hash_type_ID=UserHashTypeConfig::get_user_hash_type_ID(self::$user_hash_type_name);

        if(empty(self::$user_hash_type_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User hash type ID is empty'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user(){

        self::$user_ID=User::add_user();

        if(empty(self::$user_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_email(){

        self::$user_email_ID=UserEmail::add_user_email(self::$user_ID,self::$user_email);

        if(empty(self::$user_email)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User email was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_data(){

        self::$user_data_ID=UserData::add_user_data(self::$user_ID,self::$user_name,self::$user_surname,NULL);

        if(empty(self::$user_data_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User data was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_login(){

        self::$user_login_ID=UserLogin::add_user_login(self::$user_ID,self::$user_login,NULL);

        if(empty(self::$user_login_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User login was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_balance(){

        if(UserBalance::isset_user_balance(self::$user_ID))
            return true;

        self::$user_balance_ID=UserBalance::add_user_balance(self::$user_ID);

        if(empty(self::$user_balance_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User balance was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_access_list(){

        if(count(self::$user_access_type_list)==0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User access type list is empty'
            ];

            throw new ParametersException($error);

        }

        $user_access_type_name_list=[];

        foreach(self::$user_access_type_list as $user_access_type_name){

            if(!UserAccessTypeValidation::isset_user_access_name($user_access_type_name)){

                $error=[
                    'title'     =>ParametersValidationException::$title,
                    'info'      =>'User access type is not valid'
                ];

                throw new ParametersValidationException($error);

            }

            $user_access_type_name_list[]=$user_access_type_name;

        }

        if(count($user_access_type_name_list)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Value list is empty'
            ];

            throw new PhpException($error);

        }

        if(!UserAccess::add_user_access(self::$user_ID,$user_access_type_name_list)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User access list was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_user_invite_hash(){

        $list=[
            User::$user_ID,
            self::$user_ID,
            self::$user_email,
            self::$user_email_ID,
            time(),
            rand(0,time())
        ];

        self::$user_invite_hash=Hash::get_sha1_encode(implode(':',$list));

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_user_hash_for_invite(){

        self::$user_hash_ID=UserHash::add_user_hash(self::$user_ID,self::$user_hash_type_ID,NULL,self::$user_invite_hash);

        if(empty(self::$user_hash_type_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'User hash type was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_invite(){

        $inner  =[];
        $link   =UserHashLinkConfig::get_link_full('registration_invite').'/'.self::$user_invite_hash;

        self::$link=$link;

        $inner[]='Здравствуйте!';
        $inner[]='';
        $inner[]='Для подтверждения регистрации, пожалуйста, перейдите по ссылке: <a href="'.$link.'">'.$link.'</a>';
        $inner[]='';
        $inner[]='С уважением,';
        $inner[]='Команда <a href="'.\Config::$http_type.'://'.Url::$host.'/">Shluham.net</a>';

        return EmailSend::init(array(self::$user_email),'Регистрация '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'user_ID'           =>self::$user_ID,
            'user_email'        =>self::$user_email,
            'user_login'        =>self::$user_login,
            'user_surname'      =>self::$user_surname,
            'user_name'         =>self::$user_name
        ];

        if(OsServer::$is_windows)
            $data['link']=self::$link;

        return $data;

    }

    /**
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::is_valid_user_email())
            if(!self::isset_user_email())
                if(!self::isset_login()){

                    self::add_user();
                    self::add_email();
                    self::add_user_data();
                    self::add_user_login();
                    self::add_user_balance();
                    self::add_user_access_list();

                    self::set_user_hash_type_ID();
                    self::set_user_invite_hash();

                    self::add_user_hash_for_invite();

                    self::send_invite();

                    return self::set_return();

                }

        return NULL;

    }

    /**
     * @param string|NULL $user_email
     * @param string|NULL $login
     * @param string|NULL $surname
     * @param string|NULL $name
     * @param array $user_access_type_name_list
     * @param string|NULL $user_hash_type_name
     * @return array|null
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $user_email=NULL,string $login=NULL,string $surname=NULL,string $name=NULL,array $user_access_type_name_list=[],string $user_hash_type_name=NULL){

        $error_info_list=[];

        if(empty($user_email))
            $error_info_list[]='User email is empty';

        if(empty($user_hash_type_name))
            $error_info_list[]='User hash type name is empty';

        if(count($user_access_type_name_list)==0)
            $error_info_list[]='User access type name list is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$user_login               =empty($login)?NULL:$login;
        self::$user_email               =$user_email;
        self::$user_surname             =empty($surname)?NULL:$surname;
        self::$user_name                =empty($name)?NULL:$name;
        self::$user_access_type_list    =$user_access_type_name_list;
        self::$user_hash_type_name      =$user_hash_type_name;

        return self::set();

    }


}