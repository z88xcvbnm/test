<?php

namespace Core\Action\Token;

use Core\Module\Response\ResponseSuccess;
use Core\Module\Token\Token;

class GetTokenSystemAction{

    /**
     * @return string
     */
    public  static function init(){

        return Token::$token_hash;

    }

}