<?php

namespace Core\Action\Token;

use Core\Module\App\App;
use Core\Module\App\AppVersion;
use Core\Module\Browser\Browser;
use Core\Module\Browser\BrowserVersion;
use Core\Module\Device\DeviceFirm;
use Core\Module\Device\DeviceModel;
use Core\Module\Device\DeviceToken;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Os\Os;
use Core\Module\Os\OsVersion;
use Core\Module\Token\Token;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserDevice;
use Project\Whore\All\Module\Orange\Orange;
use Project\Whore\All\Module\Orange\OrangeConfig;

class GetDeviceTokenSystemAction{
    
    /** @var int */
    private static $device_firm_ID;
    
    /** @var int */
    private static $device_model_ID;
    
    /** @var int */
    private static $device_token_ID;
    
    /** @var int */
    private static $os_ID;
    
    /** @var int */
    private static $os_version_ID;

    /** @var int */
    private static $user_device_ID;

    /** @var string */
    private static $device_firm_name;

    /** @var string */
    private static $device_model_name;

    /** @var string */
    private static $device_token;

    /** @var string */
    private static $device_push_token;

    /** @var string */
    private static $os_name;

    /** @var string */
    private static $os_version;

    /** @var string */
    private static $orange_hash;

    /** @var bool */
    private static $is_google=false;

    /** @var bool */
    private static $is_apple=false;

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function is_valid_os_name(){

        switch(strtolower(self::$os_name)){

            case'android_test':
            case'android':{

                self::$is_google    =true;
                self::$is_apple     =false;

                return true;

            }

            case'ios_test':
            case'ios':{

                self::$is_google    =false;
                self::$is_apple     =true;

                return true;

            }

            default:{

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>array(
                        'os_name'=>'Os name is not valid'
                    )
                );

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function isset_device_token(){

        $data=DeviceToken::get_device_token_data_from_hash(self::$device_token);

        if(empty($data))
            return false;

        $need_update=false;

        if($data['device_firm_ID']!=self::$device_firm_ID)
            $need_update=true;

        if($data['device_model_ID']!=self::$device_model_ID)
            $need_update=true;

        if($data['os_ID']!=self::$os_ID)
            $need_update=true;

        if($data['os_version_ID']!=self::$os_version_ID)
            $need_update=true;

        self::$device_token_ID=$data['ID'];

        self::set_user_ID_from_user_device();

        if($need_update)
            self::update_device_token();

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function set_orange_ID(){

        if(empty(self::$orange_hash))
            return false;

        Orange::$orange_ID=Orange::get_orange_ID(NULL,NULL,self::$orange_hash);

        if(empty(Orange::$orange_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Orange is not exists'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function remove_device_token(){

        if(!DeviceToken::remove_device_token_ID(self::$device_token_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Device token was not removed'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function remove_user_device(){

        if(!UserDevice::remove_user_device_ID(self::$user_device_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User device was not removed'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    private static function update_device_token(){

        self::remove_device_token();
        self::add_device_token();
        self::set_user_device_ID();

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function set_device_firm_ID(){
        
        self::$device_firm_ID=DeviceFirm::get_device_firm_ID(self::$device_firm_name);
        
        if(!empty(self::$device_firm_ID))
            return true;
        
        self::$device_firm_ID=DeviceFirm::add_device_firm_name(self::$device_firm_name);
        
        if(empty(self::$device_firm_ID)){
            
            $error=array(
                'title'     =>'System problem',
                'info'      =>'Device firm name was not added'
            );

            throw new ParametersException($error);
            
        }
        
        return true;
        
    }

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function set_device_model_ID(){
        
        self::$device_model_ID=DeviceModel::get_device_model_ID(self::$device_firm_ID,self::$device_model_name);
        
        if(!empty(self::$device_model_ID))
            return true;
        
        self::$device_model_ID=DeviceModel::add_device_model(self::$device_firm_ID,self::$device_model_name);
        
        if(empty(self::$device_model_ID)){
            
            $error=array(
                'title'     =>'System problem',
                'info'      =>'Device model name was not added'
            );

            throw new ParametersException($error);
            
        }
        
        return true;
        
    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    private static function set_user_ID_from_user_device(){

        User::$user_ID=UserDevice::get_user_ID_from_device_token_ID(self::$device_token_ID);

        if(empty(User::$user_ID))
            self::set_user();

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function set_os_name(){

        self::$os_ID=Os::get_os_ID(self::$os_name);

        if(!empty(self::$os_ID))
            return true;

        self::$os_ID=Os::add_os(self::$os_name);

        if(empty(self::$os_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Os name was not added'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function set_os_version(){

        self::$os_version_ID=OsVersion::get_os_version_ID(self::$os_ID,self::$os_version);

        if(!empty(self::$os_version_ID))
            return true;

        self::$os_version_ID=OsVersion::add_os_version(self::$os_ID,self::$os_version);

        if(empty(self::$os_version_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Os version was not added'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function add_device_token(){

        self::$device_token_ID=DeviceToken::add_device_token(self::$device_firm_ID,self::$device_model_ID,self::$os_ID,self::$os_version_ID,self::$device_token,self::$device_push_token,self::$is_google,self::$is_apple);

        if(empty(self::$device_token_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Device token was not added'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function set_user_device_ID(){

        if(empty(User::$user_ID))
            return self::add_user_device();

        $data=UserDevice::get_user_device_data(User::$user_ID,self::$device_token_ID);

        if(empty($data))
            return self::add_user_device();

        self::$user_device_ID=$data['ID'];

        $need_update=false;

        if(self::$os_ID!=$data['os_ID'])
            $need_update=true;

        if(self::$os_version_ID!=$data['os_version_ID'])
            $need_update=true;

        if(self::$device_firm_ID!=$data['device_firm_ID'])
            $need_update=true;

        if(self::$device_model_ID!=$data['device_model_ID'])
            $need_update=true;

        if($need_update){

            self::remove_user_device();
            self::add_user_device();

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function add_user_device(){

        self::$user_device_ID=UserDevice::add_user_device(User::$user_ID,self::$device_firm_ID,self::$device_model_ID,self::$device_token_ID,self::$os_ID,self::$os_version_ID,self::$is_google,self::$is_apple);

        if(empty(self::$user_device_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Device token was not added'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function add_user_access(){

        $list=array(
            'app'
        );

        UserAccess::$user_access_ID=UserAccess::add_user_access(User::$user_ID,$list);

        if(empty(UserAccess::$user_access_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User access was not added'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function add_user(){

        User::$user_ID=User::add_user();

        if(empty(User::$user_ID)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'User was not added'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     */
    private static function update_user_date_online(){

        return User::update_user_date_online_default();

    }

    /**
     * @return bool
     */
    private static function reset_token(){

        Os::set_os_ID_default(self::$os_ID);
        OsVersion::set_os_version_ID_default(self::$os_version_ID);
        DeviceFirm::set_device_firm_ID_default(self::$device_firm_ID);
        DeviceModel::set_device_model_ID_default(self::$device_model_ID);
        DeviceToken::set_device_token_ID_default(self::$device_token_ID);
        UserDevice::set_user_device_ID_default(self::$user_device_ID);
        App::set_app_ID_default(NULL);
        AppVersion::set_app_version_ID_default(NULL);

        Browser::set_browser_ID_default(NULL);
        BrowserVersion::set_browser_version_ID_default(NULL);

        return Token::reset_token_default();

    }

    /**
     * @throws ParametersException
     */
    private static function set_user(){

        self::add_user();
        self::add_user_access();
        self::add_user_device();

        self::reset_token();

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=array(
            'user_ID'   =>User::$user_ID,
            'token'     =>Token::$token_hash
        );

        if(!empty(Orange::$orange_ID))
            $data['config']=array(
                'timeout_scan'      =>OrangeConfig::$timeout_scan,
                'timeout_send'      =>OrangeConfig::$timeout_send
            );

        return $data;

    }

    /**
     * @return array|null
     * @throws ParametersException
     * @throws PhpException
     */
    private static function set(){

        if(!self::is_valid_os_name())
            return NULL;

        self::set_device_firm_ID();
        self::set_device_model_ID();
        self::set_os_name();
        self::set_os_version();
        self::set_orange_ID();

        if(self::isset_device_token()){

            self::set_user_device_ID();
            self::update_user_date_online();

            self::reset_token();

            return self::set_return();

        }

        self::set_user();
        self::add_device_token();
        self::add_user_device();

        self::reset_token();

        return self::set_return();

    }

    /**
     * @param string|NULL $device_firm_name
     * @param string|NULL $device_model_name
     * @param string|NULL $os_name
     * @param string|NULL $os_version
     * @param string|NULL $device_token
     * @param string|NULL $device_push_token
     * @param string|NULL $orange_hash
     * @return array|null
     * @throws ParametersException
     */
    public  static function init(string $device_firm_name=NULL,string $device_model_name=NULL,string $os_name=NULL,string $os_version=NULL,string $device_token=NULL,string $device_push_token=NULL,string $orange_hash=NULL){

        $error_info_list=[];

        if(empty($device_firm_name))
            $error_info_list['device_firm_name']='Device firm name is empty';

        if(empty($device_model_name))
            $error_info_list['device_model_name']='Device model name is empty';

        if(empty($os_name))
            $error_info_list['os_name']='Os name is empty';

        if(empty($os_version))
            $error_info_list['os_version']='Os version is empty';

        if(empty($device_token))
            $error_info_list['device_token']='Device tokne is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$device_firm_name     =$device_firm_name;
        self::$device_model_name    =$device_model_name;
        self::$os_name              =$os_name;
        self::$os_version           =$os_version;
        self::$device_token         =$device_token;
        self::$device_push_token    =$device_push_token;
        self::$orange_hash          =empty($orange_hash)?NULL:$orange_hash;

        return self::set();

    }

}