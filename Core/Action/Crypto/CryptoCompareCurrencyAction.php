<?php

namespace Core\Action\Crypto;

use Core\Module\Curl\CurlGet;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;

class CryptoCompareCurrencyAction{

    /** @var string */
    private static $link                    ='https://min-api.cryptocompare.com/data/price';

    /** @var string */
    private static $currency_from;

    /** @var array */
    private static $currency_to_list        =[];

    /** @var array */
    private static $currency_list           =[];

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$currency_from        =NULL;
        self::$currency_to_list     =[];
        self::$currency_list        =[];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_currecny_list(){

        if(count(self::$currency_to_list)==0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Currency to list is empty'
            ];

            throw new ParametersException($error);

        }

        $list=[];

        foreach(self::$currency_to_list as $value)
            $list[]=mb_strtoupper($value,'utf-8');

        self::$currency_to_list=$list;

        $get_list=[
            'fsym'      =>mb_strtoupper(self::$currency_from,'utf-8'),
            'tsyms'     =>implode(',',self::$currency_to_list)
        ];

        $r=file_get_contents(self::$link.'?'.http_build_query($get_list));

        $r=Json::decode($r);

        if(isset($r['Response']))
            switch($r['Response']){

                case'Error':{

                    $error=[
                        'title'     =>ParametersValidationException::$title,
                        'info'      =>$r['Message']
                    ];

                    throw new ParametersValidationException($error);

                }

            }

        $list=[];

        foreach($r as $key=>$value)
            $list[mb_strtolower($key,'utf-8')]=$value;

        self::$currency_list=$list;

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'currency_list'=>self::$currency_list
        ];

        return $data;

    }

    /**
     * @return array
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_currecny_list();

        return self::set_return();

    }

    /**
     * @param string|NULL $currency_from
     * @param array $currency_to_list
     * @return array
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $currency_from=NULL,array $currency_to_list=[]){

        $error_info_list=[];

        if(count($currency_to_list)==0)
            $error_info_list[]='Currency list is empty';

        if(empty($currency_from))
            $error_info_list[]='Currency from is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$currency_from        =$currency_from;
        self::$currency_to_list     =$currency_to_list;

        return self::set();

    }

}
