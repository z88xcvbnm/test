<?php

namespace Core\Action\History;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Email\EmailSend;
use Core\Module\Exception\PhpException;
use Core\Module\History\History;

class RemoveHistorySystemAction{

    /** @var int */
    private static $timestamp_to;

    /** @var int */
    private static $timestamp_delta=43200;

    /**
     * @return bool
     */
    private static function set_timestamp(){

        self::$timestamp_to=Date::get_timestamp()-self::$timestamp_delta;

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function remove_history(){

        if(!History::drop_history(NULL,self::$timestamp_to)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'History was not removed'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function vacuum_table(){

        if(Db::is_postgresql())
            return Db::vacuum_full('_history');

        return false;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=array(
            'success'=>true
        );

        EmailSend::init(\Config::$email_log,'Remove History','DONE');

        return $data;

    }

    /**
     * @return array
     * @throws PhpException
     */
    private static function set(){

        self::set_timestamp();
        self::remove_history();
        self::vacuum_table();

        return self::set_return();

    }

    /**
     * @return array
     */
    public  static function init(){

        return self::set();

    }

}