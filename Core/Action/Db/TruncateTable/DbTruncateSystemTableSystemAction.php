<?php

namespace Core\Action\Db\TruncateTable;

use Core\Module\Date\Date;
use Core\Module\Db\Db;

class DbTruncateSystemTableSystemAction{

    /** @var array */
    private static $list=array(
        '_app',
        '_app_store',
        '_app_store_purchase',
        '_app_version',
        '_audio',
        '_audio_item',
        '_avatar',
        '_avatar_item',
        '_browser',
        '_browser_version',
        '_device_firm',
        '_device_model',
        '_device_token',
        '_file',
        '_file_item',
        '_history',
        '_image',
        '_image_item',
        '_ip',
        '_ip_ban',
        '_lang_keyboard',
        '_os',
        '_os_version',
        '_session',
        '_token',
        '_user',
        '_user_auth',
        '_user_access',
        '_user_access_type',
        '_user_address',
        '_user_block',
        '_user_data',
        '_user_device',
        '_user_email',
        '_user_email_check',
        '_user_hash',
        '_user_login',
        '_user_logout',
        '_user_password_history',
        '_user_phone',
        '_user_phone_check',
        '_user_remove',
        '_useragent',
        '_useragent_trigger'
    );

    /** @var array */
    private static $stats_list=[];

    /** @var int */
    private static $worktime=0;

    /**
     * Prepare
     */
    private static function set(){

        foreach(self::$list as $table_name){

            $start=Date::get_timestamp_ms();

            Db::truncate_table($table_name);

            $finish=Date::get_timestamp_ms();

            self::$stats_list[$table_name]=$finish-$start;

            self::$worktime+=self::$stats_list[$table_name];

        }

        $data=array(
            'prepare_time'  =>self::$worktime,
            'list'          =>self::$stats_list
        );

        return $data;

    }

    /**
     * Init truncate system table
     * @return array
     */
    public  static function init(){

        return self::set();

    }

}