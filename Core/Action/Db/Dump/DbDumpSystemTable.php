<?php

namespace Core\Action\Db;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Db\DbConfig;
use Core\Module\Db\DbPgDumpConfig;
use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;

class DbSumpSystemTable{

    /** @var string */
    private static $file_name;

    /** @var string */
    private static $file_path;

    /** @var array */
    private static $table_list=array(
        '_app',
        '_app_version',
        '_audio',
        '_audio_item',
        '_avatar',
        '_avatar_item',
        '_browser',
        '_browser_version',
        '_city',
        '_city_localization',
        '_continent',
        '_continent_localization',
        '_country',
        '_country_localization',
        '_currency',
        '_currency_localization',
        '_device_firm',
        '_device_model',
        '_device_token',
        '_file',
        '_file_item',
        '_geo',
        '_geo_ip',
        '_history',
        '_image',
        '_image_item',
        '_ip',
        '_ip_ban',
        '_lang_keyboard',
        '_os',
        '_os_version',
        '_region',
        '_region_localization',
        '_session',
        '_timezone',
        '_token',
        '_user',
        '_user_access',
        '_user_access_type',
        '_user_address',
        '_user_block',
        '_user_remove',
        '_user_data',
        '_user_device_token',
        '_user_email',
        '_user_hash',
        '_user_login',
        '_user_phone',
        '_useragent',
        '_useragent_trigger'
    );


    /**
     * Create hash of filename
     */
    private static function set_file_puth(){

        self::$file_name        =Hash::get_sha1_encode(Date::get_timestamp());
        self::$file_path        =DirConfig::$dir_db_dump.'/'.self::$file_name.'.pgdump';

    }

    /**
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function set_dir(){

        Dir::create_dir(DirConfig::$dir_db_dump);

    }

    /**
     * @return string
     */
    private static function make_dump_query(){

        $pg_dump_path=DbPgDumpConfig::get_pg_dump_path();

        $q      =$pg_dump_path.' -F custom -Z '.DbPgDumpConfig::$compress_level.' -d '.Db::$db_name_default.' -U '.Db::$db_login_default.' -W';
        $q      .=' -t '.implode(' -t ',self::$table_list);
        $q      .=' >> '.self::$file_path;

        return $q;

    }

    /**
     * @return string
     */
    private static function set(){

        self::set_dir();
        self::set_file_puth();

        return self::make_dump_query();

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    public  static function init(){

        if(DbConfig::$type=='postgresql'){

            echo self::set();

            return true;

        }

        $error=array(
            'title'     =>'Parameters problem',
            'info'      =>'Database type is not PostgreSQL'
        );

        throw new ParametersException($error);

    }

}