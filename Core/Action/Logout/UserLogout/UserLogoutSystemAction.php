<?php

namespace Core\Action\Logout\UserLogout;

use Core\Module\Session\Session;
use Core\Module\Token\Token;
use Core\Module\User\User;
use Core\Module\User\UserLogout;

class UserLogoutSystemAction{

    /** @var int */
    private static $user_ID;

    /** @var int */
    private static $token_ID;

    /** @var  */
    private static $session_ID;

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function remove_token(){

        if(!empty(Token::$token_hash))
            Token::remove_token_from_token_hash(Token::$token_hash);

    }

    /**
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function remove_session(){

        if(!empty(Session::$session_ID)){

            if(empty(User::$user_ID)&&empty(Token::$token_ID))
                Session::remove_session_ID_default();
            else
                Session::remove_session(User::$user_ID,Token::$token_ID);

        }

    }

    /**
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function add_user_logout(){

        UserLogout::add_user_logout(self::$user_ID,self::$token_ID,self::$session_ID);

    }

    /**
     * Set user ID
     */
    private static function set_user_ID(){

        self::$user_ID=User::$user_ID;

    }

    /**
     * Set token ID
     */
    private static function set_token_ID(){

        self::$token_ID=Token::$token_ID;

    }

    /**
     * Set session ID
     */
    private static function set_session_ID(){

        self::$session_ID=Session::$session_ID;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_token_ID();
        self::set_user_ID();
        self::set_session_ID();

        self::remove_session();
        self::remove_token();

        self::add_user_logout();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}