<?php

namespace Core\Action\Feedback;

use Core\Module\Email\EmailSend;
use Core\Module\Email\EmailValidation;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Feedback\Feedback;
use Core\Module\User\User;
use Core\Module\User\UserLogin;

class FeedbackSystemAction{

    /** @var int */
    private static $feedback_ID;

    /** @var string */
    private static $email;

    /** @var string */
    private static $content;

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_content(){

        self::$feedback_ID=Feedback::add_feedback(self::$email,self::$content);

        if(empty(self::$feedback_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'FeedbackApi was not add'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send(){

        $inner=[];

        $inner[]='FeedbackApi ID: '.self::$feedback_ID;
        $inner[]='User ID: '.User::$user_ID;
        $inner[]='Login: '.UserLogin::get_user_login(User::$user_ID);
        $inner[]='Email: '.self::$email;
        $inner[]='';
        $inner[]='Content: '.nl2br(self::$content);

//        return EmailSend::init(array(\Config::$email_default),'Обратная связь '.\Config::$project_name,implode("<br />\n\r",$inner));
        return EmailSend::init(array(\Config::$email_feedback),'Обратная связь '.\Config::$project_name,implode("<br />\n\r",$inner),\Config::$email_no_replay);

    }

    /**
     * @return bool
     */
    private static function set_return(){

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::add_content()){

            self::send();

            return self::set_return();

        }

        return false;

    }

    /**
     * @param string|NULL $email
     * @param string|NULL $content
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $email=NULL,string $content=NULL){

        $error_info_list=[];

        if(empty($email))
            $error_info_list['email']='Email is empty';
        else if(!EmailValidation::is_valid_email($email))
            $error_info_list['email']='Email is not valid';

        if(empty($content))
            $error_info_list['content']='Content is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$email                =mb_strtolower($email,'utf-8');
        self::$content              =$content;

        return self::set();

    }

}