<?php

namespace Core\Module\User;

class UserLoginValidation{

    /** @var int */
    public  static $login_max_len       =64;

    /** @var int */
    public  static $login_min_len       =3;

    /** @var int */
    public  static $password_max_len    =64;

    /** @var int */
    public  static $password_min_len    =6;

    /**
     * @param string|NULL $login
     * @return bool
     */
    public  static function check_user_login_length(string $login=NULL){

        if(empty($login))
            return false;

        $login_len=mb_strlen($login,'utf-8');

        return
              $login_len<=self::$login_max_len
            &&$login_len>=self::$login_min_len;

    }

    /**
     * @param string|NULL $login
     * @return bool|int
     */
    public  static function is_valid_user_login(string $login=NULL){

        if(empty($login))
            return false;

        if(!self::check_user_login_length($login))
            return false;

        return preg_match('/^[a-z\d_]{'.self::$login_min_len.','.self::$login_max_len.'}$/i',strtolower($login));

    }

    /**
     * @param string|NULL $password
     * @return bool
     */
    public  static function check_user_password_length(string $password=NULL){

        if(empty($password))
            return false;

        $password_len=mb_strlen($password,'utf-8');

        return
              $password_len<=self::$password_max_len
            &&$password_len>=self::$password_min_len;

    }

    /**
     * @param string|NULL $password
     * @return bool|int
     */
    public  static function is_valid_user_password(string $password=NULL){

        if(empty($password))
            return false;

        if(!self::check_user_password_length($password))
            return false;

//        return preg_match('/^[a-z0-9]{'.self::$password_min_len.','.self::$password_max_len.'}$/i',strtolower($password));

        return true;

    }

}