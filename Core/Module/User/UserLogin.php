<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserLogin{

    /** @var int */
    public  static $user_login_ID;

    /** @var string */
    public  static $login;

    /** @var string */
    public  static $password;

    /** @var string */
    public  static $table_name='_user_login';

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$user_login_ID        =NULL;
        self::$login                =NULL;
        self::$password             =NULL;

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $password
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_verify_password(int $user_ID=NULL,string $password=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($password))
            $error_info_list[]='Password is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'   =>$user_ID,
            'password'  =>$password
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param string|NULL $login
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_login_in_active(string $login=NULL,int $user_ID=NULL){

        if(empty($login)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Login is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_user_login',
            'where'=>array(
                array(
                    'column'    =>'login',
                    'value'     =>strtolower($login)
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        if(!empty($user_ID))
            $q['where'][]=array(
                'column'    =>'user_id',
                'method'    =>'!=',
                'value'     =>$user_ID
            );

        $r=Db::select($q);

        return count($r)>0;

    }

    /**
     * @param string|NULL $login
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_login_in_all(string $login=NULL){

        if(empty($login)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Login is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_user_login',
            'where'=>array(
                'login'     =>strtolower($login),
                'type'      =>[0,1]
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return count($r)>0;

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $login
     * @param string|NULL $password
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_login(int $user_ID=NULL,string $login=NULL,string $password=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list['user_ID']='User ID is empty';

        if(empty($login))
            $error_info_list['login_ID']='Login is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_login',
            'values'    =>array(
                'user_id'       =>$user_ID,
                'login'         =>strtolower($login),
                'date_create'   =>'NOW()'
            )
        );

        if(!empty($password))
            $q['values']['password']=$password;

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User login was not added'
            );

            throw new ParametersException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $login
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function replace_user_login_for_user_ID(int $user_ID=NULL,string $login=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($login))
            $error_info_list[]='Login is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::remove_user_login_from_user_ID($user_ID);

        return self::add_user_login($user_ID,$login);

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $password
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function check_user_password(int $user_ID=NULL,string $password=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list['user_ID']='User ID is empty';

        if(empty($password))
            $error_info_list['password']='Password is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'   =>$user_ID,
            'password'  =>$password
        );

        return Db::isset_row('_user_login',0,$where_list);

    }

    /**
     * @param string|NULL $login
     * @param string|NULL $password
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_ID(string $login=NULL,string $password=NULL){

        $error_info_list=[];

        if(empty($password))
            $error_info_list[]='Password is empty';

        if(empty($login))
            $error_info_list[]='Login is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'user_id'
            ),
            'table'=>'_user_login',
            'where'=>array(
                'login'     =>$login,
                'password'  =>$password,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['user_id'];

    }

    /**
     * @param string|NULL $login
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_ID_from_login(string $login=NULL){

        $error_info_list=[];

        if(empty($login))
            $error_info_list[]='Login is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'user_id'
            ),
            'table'=>'_user_login',
            'where'=>array(
                'login'     =>$login,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['user_id'];

    }

    /**
     * @param string|NULL $login
     * @param string|NULL $password
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_login_info(string $login=NULL,string $password=NULL){

        $error_info_list=[];

        if(empty($password))
            $error_info_list[]='Password is empty';

        if(empty($login))
            $error_info_list[]='Login is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'user_id',
                'type'
            ),
            'table'=>'_user_login',
            'where'=>array(
                'login'     =>$login,
                'password'  =>$password,
                'type'      =>0
            ),
            'limit'=>1
        );

//        print_r($q);

        $r=Db::select($q);

//        echo Db::$query_last;
//
//        print_r($r);

        if(count($r)==0)
            return NULL;

        return array(
            'user_ID'   =>$r[0]['user_id'],
            'type'      =>$r[0]['type']
        );

    }

    /**
     * @param int|NULL $user_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_login_info_from_user_ID(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'login',
                'password'
            ),
            'table'=>'_user_login',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'        =>$r[0]['id'],
            'login'     =>$r[0]['login'],
            'password'  =>$r[0]['password']
        );

    }

    /**
     * @param int|NULL $user_ID
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_login(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'login'
            ),
            'table'=>'_user_login',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['login'];

    }

    /**
     * @param int|NULL $user_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_login_data(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'login',
                'password'
            ),
            'table'=>'_user_login',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'        =>$r[0]['id'],
            'login'     =>$r[0]['login'],
            'password'  =>$r[0]['password']
        );

    }

    /**
     * @return string|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_login_default(){

        if(!empty(self::$login))
            return self::$login;

        if(empty(User::$user_ID)){
            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default user ID is empty'
            );

            throw new ParametersException($error);

        }

        self::$login=self::get_user_login(User::$user_ID);

        return self::$login;

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $login
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_login_from_user_ID_and_login(int $user_ID=NULL,string $login=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($login))
            $error_info_list[]='Login is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_login',
            'set'       =>array(
                'date_remove'   =>'NOW()',
                'type'          =>1
            ),
            'where'     =>array(
                'user_id'       =>$user_ID,
                'login'         =>$login,
                'type'          =>0
            )
        );

        return Db::update($q);

    }

    /**
     * @param int|NULL $user_ID
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_login_from_user_ID(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_login',
            'set'       =>array(
                'date_remove'   =>'NOW()',
                'type'          =>1
            ),
            'where'     =>array(
                'user_id'       =>$user_ID,
                'type'          =>0
            )
        );

        return Db::update($q);

    }

    /**
     * @param int|NULL $user_login_ID
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_login_ID(int $user_login_ID=NULL){

        if(empty($user_login_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User login ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_login',
            'set'       =>array(
                'date_remove'   =>'NOW()',
                'type'          =>1
            ),
            'where'     =>array(
                'id'        =>$user_login_ID,
                'type'      =>0
            )
        );

        return Db::update($q);

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_login(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'=>$user_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User login was not remove'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

}