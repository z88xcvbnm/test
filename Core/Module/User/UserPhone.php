<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Phone\PhoneValidation;

class UserPhone{

    /** @var int */
    public  static $user_phone_ID;

    /** @var int */
    public  static $county_code;

    /** @var int */
    public  static $zone_code;

    /** @var string */
    public  static $user_phone_number;

    /** @var string */
    public  static $user_phone_number_full;

    /** @var array */
    public  static $user_phone_list;

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$user_phone_ID            =NULL;
        self::$county_code              =NULL;
        self::$zone_code                =NULL;
        self::$user_phone_number        =NULL;
        self::$user_phone_number_full   =NULL;

    }

    /**
     * @param int|NULL $user_phone_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_user_phone_ID(int $user_phone_ID=NULL){

        if(empty($user_phone_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User phone ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($user_phone_ID,'_user_phone',0);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $country_code
     * @param int|NULL $zone_code
     * @param string|NULL $number
     * @param string|NULL $number_full
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_user_phone(int $user_ID=NULL,int $country_code=NULL,int $zone_code=NULL,string $number=NULL,string $number_full=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($number_full))
            $error_info_list[]='Phone number full is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'       =>$user_ID,
            'number_full'   =>$number_full
        );

        if(!empty($country_code))
            $where_list['country_code']=$country_code;

        if(!empty($zone_code))
            $where_list['zone_code']=$zone_code;

        if(!empty($number))
            $where_list['number']=$number_full;

        return Db::isset_row('_user_phone',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $country_code
     * @param int|NULL $zone_code
     * @param string|NULL $number
     * @param string|NULL $number_full
     * @return int|null
     * @throws ParametersException
     */
    public  static function get_user_phone_ID(int $user_ID=NULL,int $country_code=NULL,int $zone_code=NULL,string $number=NULL,string $number_full=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($number_full))
            $error_info_list[]='Phone number full is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'       =>$user_ID,
            'number_full'   =>$number_full
        );

        if(!empty($country_code))
            $where_list['country_code']=$country_code;

        if(!empty($zone_code))
            $where_list['zone_code']=$zone_code;

        if(!empty($number))
            $where_list['number']=$number;

        return Db::get_row_ID('_user_phone',0,$where_list);

    }

    /**
     * @param int|NULL $user_phone_ID
     * @return array|null
     * @throws ParametersException
     */
    public  static function get_user_phone_data(int $user_phone_ID=NULL){

        if(empty($user_phone_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User phone ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'user_id',
                'country_code',
                'zone_code',
                'number',
                'number_full'
            ),
            'table'=>'_user_phone',
            'where'=>array(
                'id'    =>$user_phone_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'user_ID'       =>$r[0]['user_id'],
            'country_code'  =>$r[0]['country_code'],
            'zone_code'     =>$r[0]['zone_code'],
            'number'        =>$r[0]['number'],
            'number_full'   =>$r[0]['number_full']
        );

    }

    /**
     * @param int|NULL $user_ID
     * @return array
     * @throws ParametersException
     */
    public  static function get_user_phone_list(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'user_id',
                'country_code',
                'zone_code',
                'number',
                'number_full'
            ),
            'table'=>'_user_phone',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];
            
        $list=[];
        
        foreach($r as $row)
            $list[]=array(
                'user_ID'       =>$row['user_id'],
                'country_code'  =>$row['country_code'],
                'zone_code'     =>$row['zone_code'],
                'number'        =>$row['number'],
                'number_full'   =>$row['number_full']
            );

        return $list;

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $country_code
     * @param int|NULL $zone_code
     * @param string|NULL $number
     * @param string|NULL $number_full
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function add_user_phone(int $user_ID=NULL,int $country_code=NULL,int $zone_code=NULL,string $number=NULL,string $number_full=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($number_full))
            $error_info_list[]='Phone number full is empty';
        else if(!PhoneValidation::is_valid_phone($number_full))
            $error_info_list[]='Phone number full is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $value_list=array(
            'user_id'       =>$user_ID,
            'number_full'   =>$number_full,
            'date_create'   =>'NOW()'
        );

        if(!empty($country_code))
            $value_list['country_code']=$country_code;

        if(!empty($zone_code))
            $value_list['zone_code']=$zone_code;

        if(!empty($number))
            $value_list['number']=$number;

        $q=array(
            'table'     =>'_user_phone',
            'values'    =>$value_list
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User phone was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_phone_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_user_phone_ID(int $user_phone_ID=NULL){

        if(empty($user_phone_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User phone ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($user_phone_ID,'_user_phone',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User phone was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $country_code
     * @param int|NULL $zone_code
     * @param string|NULL $number
     * @param string|NULL $number_full
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_user_phone(int $user_ID=NULL,int $country_code=NULL,int $zone_code=NULL,string $number=NULL,string $number_full=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'=>$user_ID
        );

        if(!empty($number_full))
            $where_list['number_full']=$number_full;

        if(!empty($country_code))
            $where_list['country_code']=$country_code;

        if(!empty($zone_code))
            $where_list['zone_code']=$zone_code;

        if(!empty($number))
            $where_list['number']=$number;

        if(!empty($number_full))
            $where_list['number_full']=$number_full;

        if(!Db::delete_from_where_list('_user_phone',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User phone was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_phone_ID
     */
    public  static function set_user_phone_ID_default(int $user_phone_ID=NULL){

        self::$user_phone_ID=empty($user_phone_ID)?NULL:$user_phone_ID;

    }

    /**
     * @param int|NULL $country_code
     */
    public  static function set_country_code_default(int $country_code=NULL){
        
        self::$county_code=empty($country_code)?NULL:$country_code;
        
    }

    /**
     * @param int|NULL $zone_code
     */
    public  static function set_zone_code_default(int $zone_code=NULL){
        
        self::$zone_code=empty($zone_code)?NULL:$zone_code;
        
    }

    /**
     * @param string|NULL $user_phone_number
     */
    public  static function set_user_phone_number_default(string $user_phone_number=NULL){

        self::$user_phone_number=empty($user_phone_number)?NULL:$user_phone_number;

    }

    /**
     * @param string|NULL $user_phone_number_full
     */
    public  static function set_user_phone_number_full_default(string $user_phone_number_full=NULL){

        self::$user_phone_number_full=empty($user_phone_number_full)?NULL:$user_phone_number_full;

    }

}