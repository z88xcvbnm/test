<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserAccessType{

    /** @var string */
    public  static $table_name='_user_access_type';

    /**
     * @param int|NULL $user_access_type_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_access_type_ID(int $user_access_type_ID=NULL){

        if(empty($user_access_type_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access type ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($user_access_type_ID,'_user_access_type',0);

    }

    /**
     * @param string|NULL $user_access_type_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_access_name(string $user_access_type_name=NULL){

        if(empty($user_access_type_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name'=>$user_access_type_name
        );

        return Db::isset_row('_user_access_type',0,$where_list);

    }

    /**
     * @param string|NULL $user_access_type_name
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_access_type_ID(string $user_access_type_name=NULL){

        if(empty($user_access_type_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access type name is empty'
            );

            throw new ParametersException($error);

        }

        if(!isset(UserAccessTypeConfig::$user_access_type_name_list[$user_access_type_name])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access type name is not exist'
            );

            throw new ParametersException($error);

        }

        return UserAccessTypeConfig::$user_access_type_name_list[$user_access_type_name];

    }

    /**
     * @param int|NULL $user_access_type_ID
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_access_type_name(int $user_access_type_ID=NULL){

        if(empty($user_access_type_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!UserAccessTypeValidation::isset_user_access_type_ID($user_access_type_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access ID is not exist'
            );

            throw new ParametersException($error);

        }

        return UserAccessTypeConfig::get_user_access_type_name($user_access_type_ID);

    }

    /**
     * @param array $user_access_type_name_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_access_type_ID_list(array $user_access_type_name_list=[]){

        if(count($user_access_type_name_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access type name list is empty'
            );

            throw new ParametersException($error);

        }

        $user_access_type_ID_list=[];

        foreach($user_access_type_name_list as $user_access_type_name)
            if(!empty($user_access_type_name))
                if(isset(UserAccessTypeConfig::$user_access_type_name_list[$user_access_type_name]))
                    $user_access_type_ID_list[]=self::get_user_access_type_ID($user_access_type_name);

        return $user_access_type_ID_list;

    }

    /**
     * @param array $user_access_type_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_access_type_name_list(array $user_access_type_ID_list=[]){

        if(count($user_access_type_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access type ID list is empty'
            );

            throw new ParametersException($error);

        }

        $user_access_type_name_list=[];

        foreach($user_access_type_ID_list as $user_access_type_ID)
            if(!empty($user_access_type_ID))
                if(UserAccessTypeValidation::isset_user_access_type_ID($user_access_type_ID))
                    $user_access_type_name_list[]=self::get_user_access_type_name($user_access_type_ID);

        return $user_access_type_name_list;

    }

    /**
     * @param string|NULL $user_access_type_name
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_access_type(string $user_access_type_name=NULL){

        if(empty($user_access_type_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access type name is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_access_type',
            'values'    =>array(
                'name'          =>$user_access_type_name,
                'date_create'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User access type was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_access_type_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_access_type_ID(int $user_access_type_ID=NULL){

        if(empty($user_access_type_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access type ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($user_access_type_ID,'_user_access_type',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User access type was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $user_access_type_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_access_type(string $user_access_type_name=NULL){

        if(empty($user_access_type_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access type name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name'=>$user_access_type_name
        );

        if(!Db::delete_from_where_list('_user_access_type',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User access type was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

}