<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Session\Session;
use Core\Module\Token\Token;

class UserAuth{

    /** @var int */
    public  static $user_auth_ID;

    /**
     * @param int|NULL $user_auth_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_auth_ID(int $user_auth_ID=NULL){

        if(empty($user_auth_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User auth ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($user_auth_ID,'_user_auth',0);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $token_ID
     * @param int|NULL $session_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_auth(int $user_ID=NULL,int $token_ID=NULL,int $session_ID=NULL){

        if(empty($user_ID)&&empty($token_ID)&&empty($session_ID)){

            $error=array(
                'title'     =>'Paramteres problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($token_ID))
            $where_list['token_id']=$token_ID;

        if(!empty($session_ID))
            $where_list['session_id']=$session_ID;

        if(count($where_list)==0){

            $error=array(
                'title'     =>'Paramteres problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row('_user_auth',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $token_ID
     * @param int|NULL $session_ID
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_auth(int $user_ID=NULL,int $token_ID=NULL,int $session_ID=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($token_ID))
            $error_info_list[]='Token ID is empty';

        if(empty($session_ID))
            $error_info_list[]='Session ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_auth',
            'values'    =>array(
                'user_id'       =>$user_ID,
                'token_id'      =>$token_ID,
                'session_id'    =>$session_ID,
                'date_create'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User auth was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @return int|mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_auth_default(){

        $error_info_list=[];

        if(empty(User::$user_ID))
            $error_info_list[]='User ID is empty';

        if(empty(Token::$token_ID))
            $error_info_list[]='Token ID is empty';

        if(empty(Session::$session_ID))
            $error_info_list[]='Session ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$user_auth_ID=self::add_user_auth(User::$user_ID,Token::$token_ID,Session::$session_ID);

        if(empty(self::$user_auth_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User auth was not added'
            );

            throw new DbQueryException($error);

        }

        return self::$user_auth_ID;

    }

    /**
     * @param int|NULL $user_auth_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_auth_ID(int $user_auth_ID=NULL){

        if(empty($user_auth_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User auth ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($user_auth_ID,'_user_auth',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User auth was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $token_ID
     * @param int|NULL $session_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_auth(int $user_ID=NULL,int $token_ID=NULL,int $session_ID=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($token_ID))
            $error_info_list[]='Token ID is empty';

        if(empty($session_ID))
            $error_info_list[]='Session ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($token_ID))
            $where_list['token_id']=$token_ID;

        if(!empty($session_ID))
            $where_list['session_id']=$session_ID;

        if(count($where_list)==0){

            $error=array(
                'title'     =>'Paramteres problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_where_list('_user_auth',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User auth was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

}