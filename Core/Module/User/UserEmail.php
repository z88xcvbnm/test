<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Email\EmailValidation;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserEmail{

    /** @var string */
    public  static $table_name='_user_email';

    /** @var int */
    public  static $user_email_ID;

    /** @var string */
    public  static $user_email;

    /** @var array */
    public  static $user_email_list;

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$user_email_ID    =NULL;
        self::$user_email       =NULL;
        self::$user_email_list  =NULL;

    }

    /**
     * @param int|NULL $user_email_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_email_ID(int $user_email_ID=NULL){

        if(empty($user_email_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User email ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($user_email_ID,'_user_email',0);

    }

    /**
     * @param string|NULL $email
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_email_in_active_list(string $email=NULL,int $user_ID=NULL){

        if(empty($email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Email is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_user_email',
            'where'=>array(
                array(
                    'column'    =>'email',
                    'value'     =>$email
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        if(!empty($user_ID))
            $q['where'][]=array(
                'column'    =>'user_id',
                'method'    =>'!=',
                'value'     =>$user_ID
            );

        $r=Db::select($q);

        return count($r)>0;

    }

    /**
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_len(){

        return Db::get_row_len(self::$table_name,0);

    }

    /**
     * @param string|NULL $email
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_email_in_all_list(string $email=NULL,int $user_ID=NULL){

        if(empty($email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Email is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_user_email',
            'where'=>array(
                'email'     =>$email,
                'type'      =>[0,1]
            ),
            'limit'=>1
        );

        if(!empty($user_ID))
            $q['where'][]=array(
                'column'    =>'user_id',
                'method'    =>'!=',
                'value'     =>$user_ID
            );

        $r=Db::select($q);

        return count($r)>0;

    }

    /**
     * @param int|NULL $user_ID
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_email_ID(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id'
            ],
            'where'     =>[
                'user_id'       =>$user_ID,
                'type'          =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param int $start
     * @param int $len
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_email_list_for_mailing(int $start=0,int $len=100){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'user_id',
                'email'
            ],
            'where'=>[
                'type'=>0
            ],
            'limit'=>[$start,$len]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'user_ID'       =>$row['user_id'],
                'email'         =>$row['email']
            ];

        return $list;

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $user_email
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_email(int $user_ID=NULL,string $user_email=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($user_email))
            $error_info_list[]='User email is empty';
        else if(!EmailValidation::is_valid_email($user_email))
            $error_info_list[]='User email is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_email',
            'values'    =>array(
                'user_id'       =>$user_ID,
                'email'         =>strtolower($user_email),
                'date_create'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User email was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_email_list(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'email'
            ),
            'table'=>'_user_email',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['email'];

        return $list;

    }

    /**
     * @param int|NULL $user_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_email_last(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'email'
            ),
            'table'=>'_user_email',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            ),
            'order'=>array(
                array(
                    'column'    =>'date_create',
                    'direct'    =>'desc'
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return [];

        return $r[0]['email'];

    }

    /**
     * @param string|NULL $email
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_ID(string $email=NULL){

        if(empty($email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Email is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'user_id'
            ),
            'table'=>'_user_email',
            'where'=>array(
                'email'     =>$email,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['user_id'];

    }

    /**
     * @param string|NULL $email
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_email_info(string $email=NULL){

        if(empty($email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Email is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'user_id',
                'type'
            ),
            'table'=>'_user_email',
            'where'=>array(
                'email'     =>$email,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'user_ID'   =>$r[0]['user_id'],
            'type'      =>$r[0]['type']
        );

    }

    /**
     * @param int|NULL $user_email_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_email_ID(int $user_email_ID=NULL){

        if(empty($user_email_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User email ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($user_email_ID,'_user_email',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User email was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $user_email
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_email(int $user_ID=NULL,string $user_email=NULL){

        if(empty($user_email)&&empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($user_email))
            $where_list['email']=$user_email;

        if(!Db::delete_from_where_list('_user_email',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User email was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_email_to_unshow(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'date_update'   =>'NOW()',
                'date_remove'   =>'NOW()',
                'type'          =>2
            ],
            'where'=>[
                'user_id'       =>$user_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User email was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_email_ID
     */
    public  static function set_user_email_ID_default(int $user_email_ID=NULL){

        self::$user_email_ID=empty($user_email_ID)?NULL:$user_email_ID;

    }

    /**
     * @param string|NULL $user_email
     */
    public  static function set_user_email_default(string $user_email=NULL){

        self::$user_email=empty($user_email)?NULL:$user_email;

    }

    /**
     * @param array|NULL $user_email_list
     */
    public  static function set_user_email_list(array $user_email_list=NULL){

        self::$user_email_list=empty($user_email_list)?NULL:$user_email_list;

    }

}
