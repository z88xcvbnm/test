<?php

namespace Core\Module\User;

use Core\Module\Exception\ParametersException;

class UserAccessTypeValidation{

    /**
     * @param string|NULL $name
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_access_name(string $name=NULL){

        if(empty($name)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Name is empty'
            ];

            throw new ParametersException($error);

        }

        return isset(UserAccessTypeConfig::$user_access_type_name_list[$name]);

    }

    /**
     * @param int|NULL $index
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_access_type_ID(int $index=NULL){

        if(is_null($index)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Index is not set'
            ];

            throw new ParametersException($error);

        }

        $name=array_search($index,UserAccessTypeConfig::$user_access_type_name_list);

        return $name!==false;

    }

}