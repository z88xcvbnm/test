<?php

namespace Core\Module\User;

class UserBalanceTransactionConfig{

    /** @var array */
    public  static $action_name_list=[
        'add',
        'remove',
        'find_face'
    ];

    /**
     * @param string|NULL $action
     * @return bool
     */
    public  static function isset_action_name(string $action=NULL){

        if(empty($action))
            return false;

        return array_search($action,self::$action_name_list)!==false;

    }

}