<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserPasswordHistory{

    /** @var array */
    public  static $user_password_list;

    /**
     * @param int|NULL $user_password_history_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_user_password_history_ID(int $user_password_history_ID=NULL){

        if(empty($user_password_history_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User password history ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($user_password_history_ID,'_user_password_history',0);

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $password
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_user_pasword_history(int $user_ID=NULL,string $password=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($password))
            $error_info_list[]='Password is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'       =>$user_ID,
            'password'      =>$password
        );

        return Db::isset_row('_user_password_history',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $password
     * @return int|null
     * @throws ParametersException
     */
    public  static function get_user_password_history_ID(int $user_ID=NULL,string $password=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($password))
            $error_info_list[]='Password is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'       =>$user_ID,
            'password'      =>$password
        );

        return Db::get_row_ID('_user_password_history',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @return array
     * @throws ParametersException
     */
    public  static function get_user_password_history_list(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'password',
                'date_create'
            ),
            'table'=>'_user_password_history',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            ),
            'order'=>array(
                array(
                    'column'    =>'date_create',
                    'direct'    =>'desc'
                )
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=array(
                'ID'            =>$row['id'],
                'password'      =>$row['password'],
                'date_create'   =>$row['date_create']
            );

        return $list;

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $password
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function add_user_password_history(int $user_ID=NULL,string $password=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($password))
            $error_info_list[]='Password is empty';
        else if(!UserLoginValidation::is_valid_user_password($password))
            $error_info_list[]='Password is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_password_history',
            'values'    =>array(
                'user_id'       =>$user_ID,
                'password'      =>$password,
                'date_create'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User password history was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_password_history_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_user_password_history_ID(int $user_password_history_ID=NULL){

        if(empty($user_password_history_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User password history ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($user_password_history_ID,'_user_password_history',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User password history was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**$r
     * @param int|NULL $user_ID
     * @param string|NULL $password
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_user_password_history(int $user_ID=NULL,string $password=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'=>$user_ID
        );

        if(!empty($password))
            $where_list['password']=$password;

        if(!Db::delete_from_where_list('_user_password_history',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User password history was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param array|NULL $user_password_list
     */
    public  static function set_user_password_list(array $user_password_list=NULL){

        self::$user_password_list=empty($user_password_list)?NULL:$user_password_list;

    }

}