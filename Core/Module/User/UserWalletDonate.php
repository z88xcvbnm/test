<?php

namespace Core\Module\User;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserWalletDonate{

    /** @var string */
    public  static $table_name='_user_wallet_donate';

    /**
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_checked_sum(){

        $q=[
            'select'=>[
                [
                    'function'=>'SUM(transaction_sum_rub) as profit'
                ]
            ],
            'table'=>self::$table_name,
            'where'=>[
                'is_confirm'    =>1,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['profit'];

    }

    /**
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_checked_sum_last_month(){

        $timestamp_to           =Date::get_timestamp()+3*3600;
        $timestamp_from         =Date::get_date($timestamp_to,"Y-m-01 00:00:00");

        $q=[
            'select'=>[
                [
                    'function'=>'SUM(transaction_sum_rub) as profit'
                ]
            ],
            'table'=>self::$table_name,
            'where'=>[
                'date_confirm'  =>[
                    'method'        =>'>=',
                    'value'         =>$timestamp_from
                ],
                'is_confirm'    =>1,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return 0;

        return empty($r[0]['profit'])?0:$r[0]['profit'];

    }

    /**
     * @param int $day_len
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_reg_count_paid(int $day_len=10){

        $date_time=Date::get_date_for_db(Date::get_timestamp()-$day_len*24*3600).' 00:00:00';

        $q=[
            'select'=>[
                [
                    'function'=>'COUNT(id) as profit_len'
                ],
                [
                    'function'=>'SUM(transaction_sum_rub) as profit'
                ],
                [
                    'function'=>"DATE(date_update AT TIME ZONE 'UTC+3') as date_paid"
                ]
            ],
            'table'=>self::$table_name,
            'where'=>[
                'date_create'       =>[
                    'method'    =>'>=',
                    'value'     =>$date_time
                ],
                'is_confirm'    =>1,
                'type'          =>0
            ],
            'group'=>[
                [
                    'function'=>"DATE(date_update AT TIME ZONE 'UTC+3')"
//                    'function'=>"DATE(date_update AT TIME ZONE 'UTC-3')"
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        $list=[];

        foreach($r as $row)
            $list[$row['date_paid']]=[
                'profit'        =>(int)$row['profit'],
                'len'           =>(int)$row['profit_len']
            ];

        return $list;

    }

    /**
     * @param string|NULL $date
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_reg_count_paid_to_date(string $date=NULL){

        $q=[
            'select'=>[
                [
                    'function'=>'SUM(transaction_sum_rub) as profit'
                ]
            ],
            'table'=>self::$table_name,
            'where'=>[
                [
                    'function'=>"DATE(date_update AT TIME ZONE 'UTC+3')='".$date."'"
                ],
                [
                    'column'    =>'is_confirm',
                    'value'     =>1
                ],
                [
                    'column'    =>'type',
                    'value'     =>0
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return (int)$r[0]['profit'];

    }

    /**
     * @param string|NULL $hash
     * @param bool|NULL $is_confirm
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_data_from_hash(string $hash=NULL,bool $is_confirm=NULL){

        if(empty($hash)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id',
                'user_id',
                'wallet_name_id',
                'user_wallet_id',
                'target_wallet_id',
                'user_wallet_address',
                'target_wallet_address',
                'transaction_sum',
                'transaction_currency',
                'transaction_sum_rub'
            ],
            'where'=>[
                'hash'=>$hash
            ],
            'limit'=>1
        ];

        if(!is_null($is_confirm))
            $q['where']['is_confirm']=(int)$is_confirm;

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'ID'                                =>$r[0]['id'],
            'user_ID'                           =>$r[0]['user_id'],
            'wallet_name_ID'                    =>$r[0]['wallet_name_id'],
            'user_wallet_ID'                    =>$r[0]['user_wallet_id'],
            'target_wallet_ID'                  =>$r[0]['target_wallet_id'],
            'user_wallet_address'               =>$r[0]['user_wallet_address'],
            'target_wallet_address'             =>$r[0]['target_wallet_address'],
            'transaction_sum'                   =>$r[0]['transaction_sum'],
            'transaction_currency'              =>$r[0]['transaction_currency'],
            'transaction_sum_rub'               =>$r[0]['transaction_sum_rub']
        ];

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $wallet_name_ID
     * @param int|NULL $user_wallet_ID
     * @param int|NULL $target_wallet_ID
     * @param string|NULL $user_wallet_address
     * @param string|NULL $target_wallet_address
     * @param float $transaction_sum
     * @param float $transaction_currency
     * @param float $transaction_sum_rub
     * @param string|NULL $hash
     * @param string|NULL $info
     * @param bool $is_confirm
     * @param int|NULL $paypal_payment_ID
     * @param int|NULL $kiwi_payment_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException     */
    public  static function add_user_wallet_donate(
        int $user_ID                    =NULL,
        int $wallet_name_ID             =NULL,
        int $user_wallet_ID             =NULL,
        int $target_wallet_ID           =NULL,
        string $user_wallet_address     =NULL,
        string $target_wallet_address   =NULL,
        float $transaction_sum          =0,
        float $transaction_currency     =0,
        float $transaction_sum_rub      =0,
        string $hash                    =NULL,
        string $info                    =NULL,
        bool $is_confirm                =false,
        int $paypal_payment_ID          =NULL,
        int $kiwi_payment_ID            =NULL
    ){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($wallet_name_ID))
            $error_info_list[]='Wallet name ID is empty';

        if(empty($user_wallet_ID))
            $error_info_list[]='User wallet ID is empty';

        if(empty($target_wallet_ID))
            $error_info_list[]='Target wallet ID is empty';

        if(empty($user_wallet_address))
            $error_info_list[]='User wallet address is empty';

        if(empty($target_wallet_address))
            $error_info_list[]='Target wallet address is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'                   =>$user_ID,
                'wallet_name_id'            =>$wallet_name_ID,
                'user_wallet_id'            =>$user_wallet_ID,
                'target_wallet_id'          =>$target_wallet_ID,
                'paypal_payment_id'         =>empty($paypal_payment_ID)?NULL:$paypal_payment_ID,
                'kiwi_payment_id'           =>empty($kiwi_payment_ID)?NULL:$kiwi_payment_ID,
                'user_wallet_address'       =>$user_wallet_address,
                'target_wallet_address'     =>$target_wallet_address,
                'transaction_sum'           =>$transaction_sum,
                'transaction_currency'      =>$transaction_currency,
                'transaction_sum_rub'       =>$transaction_sum_rub,
                'hash'                      =>empty($hash)?NULL:$hash,
                'info'                      =>empty($info)?NULL:$info,
                'is_confirm'                =>(int)$is_confirm,
                'date_create'               =>'NOW()',
                'date_update'               =>'NOW()',
                'type'                      =>0
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User wallet donate was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_wallet_donate_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_wallet_donate_to_confirmed(int $user_wallet_donate_ID=NULL){

        if(empty($user_wallet_donate_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User wallet donate ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'is_confirm'        =>1,
                'date_update'       =>'NOW()',
                'date_confirm'      =>'NOW()'
            ],
            'where'     =>[
                'id'            =>$user_wallet_donate_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User wallet donate was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_wallet_all(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'=>$user_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User wallet was not remove'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_wallet_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_wallet(int $user_wallet_ID=NULL){

        if(empty($user_wallet_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User wallet ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'id'=>$user_wallet_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User wallet was not remove'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }


}