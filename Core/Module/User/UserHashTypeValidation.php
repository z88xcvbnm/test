<?php

namespace Core\Module\User;

use Core\Module\Exception\ParametersException;

class UserHashTypeValidation{

    /**
     * @param int|NULL $user_hash_type_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_hash_type_ID(int $user_hash_type_ID=NULL){

        if(empty($user_hash_type_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User hash type ID is empty'
            ];

            throw new ParametersException($error);

        }

        return array_search($user_hash_type_ID,UserHashTypeConfig::$user_hash_type_ID_list)!==false;

    }

    /**
     * @param string|NULL $user_hash_type_name
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_hash_type_name(string $user_hash_type_name=NULL){

        if(empty($user_hash_type_name)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User hash type name is empty'
            ];

            throw new ParametersException($error);

        }

        return isset(UserHashTypeConfig::$user_hash_type_ID_list[$user_hash_type_name]);

    }

}