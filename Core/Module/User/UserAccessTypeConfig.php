<?php

namespace Core\Module\User;

use Core\Module\Exception\ParametersException;

class UserAccessTypeConfig{

    /** @var array */
    public  static $user_access_type_name_list=array(
        'root'                          =>1,
        'admin'                         =>2,
        'bot'                           =>3,
        'app'                           =>4,
        'web'                           =>5,
        'debug'                         =>6,
        'guest'                         =>7,
        'test'                          =>8,
        'profile'                       =>9,
        'profile_wallet'                =>10
    );

    /** @var array */
    public  static $user_access_list=array(
        'root'                          =>false,
        'admin'                         =>false,
        'bot'                           =>false,
        'app'                           =>false,
        'web'                           =>false,
        'debug'                         =>false,
        'guest'                         =>false,
        'test'                          =>false,
        'profile'                       =>false,
        'profile_wallet'                =>false
    );

    /** @var array */
    public  static $user_access_list_default=array(
        'root'                          =>false,
        'admin'                         =>false,
        'bot'                           =>false,
        'app'                           =>false,
        'web'                           =>false,
        'debug'                         =>false,
        'guest'                         =>false,
        'test'                          =>false,
        'profile'                       =>false,
        'profile_wallet'                =>false
    );

    /**
     * @param int|NULL $index
     * @return string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_access_type_name(int $index=NULL){

        if(is_null($index)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Index is not set'
            ];

            throw new ParametersException($error);

        }

        $name=array_search($index,self::$user_access_type_name_list);

        if($name===false){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Index is not exists'
            ];

            throw new ParametersException($error);

        }

        return $name;

    }

    /**
     * @param string|NULL $name
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_access_type_ID(string $name=NULL){

        if(empty($name)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Name is empty'
            ];

            throw new ParametersException($error);

        }

        if(!UserAccessTypeValidation::isset_user_access_name($name)){

            $error=[
                'title'     =>ParametersException::$title,
                'into'      =>'Name is not exists'
            ];

            throw new ParametersException($error);

        }

        return self::$user_access_type_name_list[$name];

    }

    /**
     * @param array $name_list
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_access_type_ID_list(array $name_list=[]){

        if(count($name_list)==0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Name list is empty'
            ];

            throw new ParametersException($error);

        }

        $list=[];

        foreach($name_list as $name){

            if(!UserAccessTypeValidation::isset_user_access_name($name)){

                $error=[
                    'title'     =>ParametersException::$title,
                    'into'      =>'Name is not exists'
                ];

                throw new ParametersException($error);

            }

            $list[]=self::$user_access_type_name_list[$name];

        }

        return $list;

    }

    /**
     * @param array $access_list
     * @return array
     */
    public  static function get_user_access_type_list(array $access_list=[]){

        return array_merge(self::$user_access_list,$access_list);

    }

}