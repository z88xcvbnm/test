<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserBalance{

    /** @var string */
    public  static $table_name='_user_balance';

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_balance(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'=>$user_ID
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_balance(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'balance'
            ],
            'where'     =>[
                'user_id'       =>$user_ID,
                'type'          =>0
            ],
            'limit'=>1
        ];

//        print_r($q);

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['balance'];

    }

    /**
     * @param int|NULL $user_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_balance_ID(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id'
            ],
            'where'     =>[
                'user_id'       =>$user_ID,
                'type'          =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param array $user_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_balance_list(array $user_ID_list=[]){

        if(count($user_ID_list)==0)
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'user_id',
                'balance'
            ],
            'where'     =>[
                'user_id'       =>$user_ID_list,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['user_id']]=$row['balance'];

        return $list;

    }

    /**
     * @param int|NULL $user_ID
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_balance(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'           =>$user_ID,
                'balance'           =>0,
                'date_create'       =>'NOW()',
                'date_update'       =>'NOW()',
                'type'              =>0
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User balance was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_ID
     * @param float $balance
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_balance(int $user_ID=NULL,float $balance=0){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'balance'       =>$balance,
                'date_update'   =>'NOW()'
            ],
            'where'     =>[
                'user_id'       =>$user_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User balance was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param float $balance
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_money_user_balance(int $user_ID=NULL,float $balance=0){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'balance'       =>[
                    'function'=>'balance + '.$balance
                ],
                'date_update'   =>'NOW()'
            ],
            'where'     =>[
                'user_id'       =>$user_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User balance was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param float $balance
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_money_user_balance(int $user_ID=NULL,float $balance=0){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'balance'       =>[
                    'function'=>'balance - '.$balance
                ],
                'date_update'   =>'NOW()'
            ],
            'where'     =>[
                'user_id'       =>$user_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User balance was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_balance(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'=>$user_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User balance was not remove'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

}