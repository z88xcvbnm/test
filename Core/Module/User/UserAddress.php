<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserAddress{

    /** @var int */
    public  static $user_address_ID;

    /** @var int */
    public  static $continent_ID;

    /** @var int */
    public  static $country_ID;

    /** @var int */
    public  static $city_ID;

    /** @var string */
    public  static $street_name;

    /** @var string */
    public  static $home;

    /** @var string */
    public  static $building;

    /** @var string */
    public  static $apartment;

    /** @var string */
    public  static $postcode;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$user_address_ID          =NULL;
        self::$continent_ID             =NULL;
        self::$country_ID               =NULL;
        self::$city_ID                  =NULL;
        self::$street_name              =NULL;
        self::$home                     =NULL;
        self::$building                 =NULL;
        self::$apartment                =NULL;
        self::$postcode                 =NULL;

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $continent_ID
     * @param int|NULL $country_ID
     * @param int|NULL $city_ID
     * @param string|NULL $street_name
     * @param string|NULL $home
     * @param string|NULL $building
     * @param string|NULL $apartment
     * @param string|NULL $postcode
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function add_user_address(int $user_ID=NULL,int $continent_ID=NULL,int $country_ID=NULL,int $city_ID=NULL,string $street_name=NULL,string $home=NULL,string $building=NULL,string $apartment=NULL,string $postcode=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($continent_ID))
            $error_info_list[]='Continent ID is empty';

        if(empty($country_ID))
            $error_info_list[]='Country ID is empty';

        if(empty($city_ID))
            $error_info_list[]='City ID is empty';

        if(empty($street_name))
            $error_info_list[]='Street name is empty';

        if(empty($home))
            $error_info_list[]='Home is empty';

        if(empty($building))
            $error_info_list[]='Building is empty';

        if(empty($apartment))
            $error_info_list[]='Apartment is empty';

        if(empty($postcode))
            $error_info_list[]='Postcode is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_address',
            'values'    =>array(
                'user_id'               =>$user_ID,
                'continent_id'          =>$continent_ID,
                'country_id'            =>$country_ID,
                'city_id'               =>$city_ID,
                'street_name'           =>$street_name,
                'home'                  =>$home,
                'building'              =>$building,
                'apartment'             =>$apartment,
                'postcode'              =>$postcode,
                'date_create'           =>'NOW()',
                'date_update'           =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User address was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function remove_user_address(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'=>$user_ID
        );

        if(!Db::delete_from_where_list('_user_address',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User address was not removed'
            );

            throw new ParametersException($error);

        }

        return true;

    }

}