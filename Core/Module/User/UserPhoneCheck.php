<?php

namespace Core\Module\User;

use Core\Module\Code\Code;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserPhoneCheck{

    /** @var int */
    public  static $user_phone_check_ID;

    /** @var int */
    public  static $user_phone_ID;

    /** @var string */
    public  static $hash;

    /** @var string */
    public  static $code;

    /** @var bool */
    public  static $is_check;

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$user_phone_check_ID      =NULL;
        self::$user_phone_ID            =NULL;
        self::$hash                     =NULL;
        self::$code                     =NULL;
        self::$is_check                 =NULL;

    }

    /**
     * @param int|NULL $user_phone_check_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_user_phone_check_ID(int $user_phone_check_ID=NULL){

        if(empty($user_phone_check_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User phone check ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($user_phone_check_ID,'_user_phone_check',0);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $user_phone_ID
     * @param string|NULL $code
     * @param string|NULL $hash
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_user_phone_check(int $user_ID=NULL,int $user_phone_ID=NULL,string $code=NULL,string $hash=NULL){

        if(empty($user_ID)&&empty($user_phone_ID)&&empty($code)&&empty($hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($user_phone_ID))
            $where_list['user_phone_id']=$user_phone_ID;

        if(!empty($code))
            $where_list['code']=$code;
        
        if(!empty($hash))
            $where_list['hash']=$hash;
        
        if(count($where_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);
            
        }
        
        return Db::isset_row('_user_phone_check',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $user_phone_ID
     * @param string|NULL $code
     * @param string|NULL $hash
     * @return bool|null
     * @throws ParametersException
     */
    public  static function is_user_phone_check(int $user_ID=NULL,int $user_phone_ID=NULL,string $code=NULL,string $hash=NULL){

        if(empty($user_ID)&&empty($user_phone_ID)&&empty($code)&&empty($hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($user_phone_ID))
            $where_list['user_phone_id']=$user_phone_ID;

        if(!empty($code))
            $where_list['code']=$code;
        
        if(!empty($hash))
            $where_list['hash']=$hash;
        
        if(count($where_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);
            
        }
        
        $where_list['type']=0;
        
        $q=array(
            'select'=>array(
                'check'
            ),
            'table'=>'_user_phone_check',
            'where'=>$where_list,
            'limit'=>1
        );
        
        $r=Db::select($q);
        
        if(count($r)==0)
            return NULL;
        
        return (bool)$r[0]['check'];

    }

    /**
     * @param int|NULL $user_phone_check_ID
     * @return bool|null
     * @throws ParametersException
     */
    public  static function is_user_phone_check_ID(int $user_phone_check_ID=NULL){

        if(empty($user_phone_check_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User phone check ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'check'
            ),
            'table'=>'_user_phone_check',
            'where'=>array(
                'id'    =>$user_phone_check_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return (bool)$r[0]['check'];

    }

    /**
     * @return array
     */
    public  static function get_user_phone_code_and_hash(){

        $code   =Code::get_code_number(6);
        $hash   =Code::get_code_hash($code);

        return array(
            'code'      =>$code,
            'hash'      =>$hash
        );

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $user_phone_ID
     * @param string|NULL $code
     * @param string|NULL $hash
     * @return int|null
     * @throws ParametersException
     */
    public  static function get_user_phone_check_ID(int $user_ID=NULL,int $user_phone_ID=NULL,string $code=NULL,string $hash=NULL){

        if(empty($user_ID)&&empty($user_phone_ID)&&empty($code)&&empty($hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($user_phone_ID))
            $where_list['user_phone_id']=$user_phone_ID;

        if(!empty($code))
            $where_list['code']=$code;
        
        if(!empty($hash))
            $where_list['hash']=$hash;
        
        if(count($where_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);
            
        }
        
        return Db::get_row_ID('_user_phone_check',0,$where_list);
        
    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $user_phone_ID
     * @param string|NULL $code
     * @param string|NULL $hash
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function add_user_phone_check(int $user_ID=NULL,int $user_phone_ID=NULL,string $code=NULL,string $hash=NULL){

        if(empty($user_ID)&&empty($user_phone_ID)&&empty($code)&&empty($hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);

        }

        $value_list=[];

        if(!empty($user_ID))
            $value_list['user_id']=$user_ID;

        if(!empty($user_phone_ID))
            $value_list['user_phone_id']=$user_phone_ID;

        if(!empty($code))
            $value_list['code']=$code;
        
        if(!empty($hash))
            $value_list['hash']=$hash;
        
        if(count($value_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);
            
        }

        $value_list['date_create']='NOW()';

        $q=array(
            'table'     =>'_user_phone_check',
            'values'    =>$value_list
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User phone check was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];
        
    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $user_phone_ID
     * @param string|NULL $code
     * @param string|NULL $hash
     * @param bool|true $is_check
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function update_user_phone_check_status(int $user_ID=NULL,int $user_phone_ID=NULL,string $code=NULL,string $hash=NULL,bool $is_check=true){

        if(empty($user_ID)&&empty($user_phone_ID)&&empty($code)&&empty($hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);

        }

        if(is_null($is_check)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Check status is NULL'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($user_phone_ID))
            $where_list['user_phone_id']=$user_phone_ID;

        if(!empty($code))
            $where_list['code']=$code;

        if(!empty($hash))
            $where_list['hash']=$hash;

        if(count($where_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_phone_check',
            'set'       =>array(
                'check'         =>$is_check,
                'date_update'   =>'NOW()'
            ),
            'where'     =>$where_list
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User phone check status was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_phone_check_ID
     * @param bool|true $is_check
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function update_user_phone_check_status_from_user_phone_check_ID(int $user_phone_check_ID=NULL,bool $is_check=true){

        $error_info_list=[];

        if(empty($user_phone_check_ID))
            $error_info_list[]='User phone check ID is empty';

        if(is_null($is_check))
            $error_info_list[]='Check status is NULL';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table' =>'_user_phone_check',
            'set'   =>array(
                'check'         =>$is_check,
                'date_update'   =>'NOW()'
            ),
            'where' =>array(
                'id'    =>$user_phone_check_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User phone check status was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $user_phone_ID
     * @param string|NULL $code
     * @param string|NULL $hash
     * @param bool|true $is_check
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_user_phone_check(int $user_ID=NULL,int $user_phone_ID=NULL,string $code=NULL,string $hash=NULL,bool $is_check=true){

        if(empty($user_ID)&&empty($user_phone_ID)&&empty($code)&&empty($hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);

        }

        if(is_null($is_check)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Check status is NULL'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($user_phone_ID))
            $where_list['user_phone_id']=$user_phone_ID;

        if(!empty($code))
            $where_list['code']=$code;

        if(!empty($hash))
            $where_list['hash']=$hash;

        if(count($where_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App parameters are empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_where_list('_user_phone_check',0,$where_list)){

            $error=array(
                'title'     =>'DB qeury problem',
                'info'      =>'User phone check was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_phone_check_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_user_phone_check_ID(int $user_phone_check_ID=NULL){

        if(empty($user_phone_check_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User phone check ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($user_phone_check_ID,'_user_phone_check',0)){

            $error=array(
                'title'     =>'DB qeury problem',
                'info'      =>'User phone check was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_phone_check_ID
     */
    public  static function set_user_phone_check_ID_default(int $user_phone_check_ID=NULL){

        self::$user_phone_check_ID=empty($user_phone_check_ID)?NULL:$user_phone_check_ID;

    }

    /**
     * @param int|NULL $user_phone_ID
     */
    public  static function set_user_phone_ID_default(int $user_phone_ID=NULL){

        self::$user_phone_ID=empty($user_phone_ID)?NULL:$user_phone_ID;

    }

    /**
     * @param string|NULL $code
     */
    public  static function set_code_default(string $code=NULL){

        self::$code=empty($code)?NULL:$code;

    }

    /**
     * @param string|NULL $hash
     */
    public  static function set_hash_default(string $hash=NULL){

        self::$hash=empty($hash)?NULL:$hash;

    }

    /**
     * @param bool|NULL $is_check
     */
    public  static function set_is_check_default(bool $is_check=NULL){

        self::$is_check=empty($is_check)?NULL:$is_check;

    }
    
}