<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use function Sodium\add;

class UserWallet{

    /** @var string */
    public  static $table_name='_user_wallet';

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $address
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_wallet_ID(int $user_ID=NULL,string $address=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($address))
            $error_info_list[]='Address is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'       =>$user_ID,
            'address'       =>$address
        ];

        return Db::get_row_ID(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $wallet_name_ID
     * @param string|NULL $address
     * @param float $transaction_sum
     * @param float $transaction_sum_rub
     * @param bool $is_confirm
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_wallet(int $user_ID=NULL,int $wallet_name_ID=NULL,string $address=NULL,float $transaction_sum=0,float $transaction_sum_rub=0,bool $is_confirm=false){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($wallet_name_ID))
            $error_info_list[]='Wallet name ID is empty';

        if(empty($address))
            $error_info_list[]='Wallet number is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'                   =>$user_ID,
                'wallet_name_id'            =>$wallet_name_ID,
                'address'             =>$address,
                'transaction_sum'           =>$transaction_sum,
                'transaction_sum_rub'       =>$transaction_sum_rub,
                'is_confirm'                =>(int)$is_confirm,
                'date_create'               =>'NOW()',
                'date_update'               =>'NOW()',
                'type'                      =>0
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User wallet was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_wallet_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_wallet_to_confirmed(int $user_wallet_ID=NULL){

        if(empty($user_wallet_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User wallet ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'set'       =>[
                'is_confirm'        =>1,
                'date_update'       =>'NOW()'
            ],
            'where'     =>[
                'id'            =>$user_wallet_ID,
                'type'          =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User wallet was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_wallet_all(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'=>$user_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User wallet was not remove'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_wallet_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_wallet(int $user_wallet_ID=NULL){

        if(empty($user_wallet_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User wallet ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'id'=>$user_wallet_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User wallet was not remove'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }


}