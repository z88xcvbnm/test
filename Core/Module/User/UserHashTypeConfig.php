<?php

namespace Core\Module\User;

use Core\Module\Exception\ParametersException;

class UserHashTypeConfig{

    /** @var array */
    public  static $user_hash_type_ID_list=array(
        'registration_invite'       =>1,
        'recovery_password'         =>2,
        'confirm_email'             =>3
    );

    /** @var array */
    public  static $user_hash_type_list_default=array(
        'registration_invite'       =>false,
        'recovery_password'         =>false,
        'confirm_email'             =>false
    );

    /**
     * @param int|NULL $index
     * @return string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_hash_type_name(int $index=NULL){

        if(is_null($index)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Index is not set'
            ];

            throw new ParametersException($error);

        }

        $name=array_search($index,self::$user_hash_type_ID_list);

        if($name===false){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Index is not exists'
            ];

            throw new ParametersException($error);

        }

        return $name;

    }

    /**
     * @param string|NULL $name
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_hash_type_ID(string $name=NULL){

        if(empty($name)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Name is empty'
            ];

            throw new ParametersException($error);

        }

        if(!UserHashTypeValidation::isset_user_hash_type_name($name)){

            $error=[
                'title'     =>ParametersException::$title,
                'into'      =>'Name is not exists'
            ];

            throw new ParametersException($error);

        }

        return self::$user_hash_type_ID_list[$name];

    }

    /**
     * @param array $access_list
     * @return array
     */
    public  static function get_user_hash_type_list(array $access_list=[]){

        return array_merge(self::$user_hash_type_list_default,$access_list);

    }

}