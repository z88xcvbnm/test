<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserData{

    /**
     * @param int|NULL $user_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_data(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'name',
                'surname',
                'sex'
            ),
            'table'=>'_user_data',
            'where'=>array(
                'user_id'       =>$user_ID,
                'type'          =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'        =>$r[0]['id'],
            'name'      =>$r[0]['name'],
            'surname'   =>$r[0]['surname'],
            'sex'       =>$r[0]['sex']
        );

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $name
     * @param string|NULL $surname
     * @param int|NULL $sex
     * @param string|NULL $fathername
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_data(int $user_ID=NULL,string $name=NULL,string $surname=NULL,int $sex=NULL,string $fathername=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_data',
            'values'    =>array(
                'user_id'       =>$user_ID,
                'name'          =>empty($name)?NULL:$name,
                'surname'       =>empty($surname)?NULL:$surname,
                'fathername'    =>empty($fathername)?NULL:$fathername
            )
        );

        if(!is_null($sex))
            $q['values']['sex']=$sex;

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User data was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_data(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'=>$user_ID
        );

        if(!Db::delete_from_where_list('_user_data',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User data was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

}