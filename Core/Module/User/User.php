<?php

namespace Core\Module\User;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class User{

    /** @var int */
    public  static $user_ID;

    /** @var int */
    public  static $date_create;

    /** @var int */
    public  static $date_online;

    /** @var int */
    public  static $date_block;

    /** @var int */
    public  static $date_unblock;

    /** @var int */
    public  static $date_remove;

    /** @var int */
    public  static $date_recovery;

    /** @var array */
    public  static $date_list=array(
        'date_create'       =>NULL,
        'date_online'       =>NULL,
        'date_block'        =>NULL,
        'date_unblock'      =>NULL,
        'date_remove'       =>NULL,
        'date_recovery'     =>NULL
    );

    /** @var string */
    public  static $table_name='_user';

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$user_ID              =NULL;
        self::$date_create          =NULL;
        self::$date_online          =NULL;
        self::$date_block           =NULL;
        self::$date_unblock         =NULL;
        self::$date_remove          =NULL;
        self::$date_recovery        =NULL;

        return true;

    }

    /**
     * @param string|NULL $user_access_type_name
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_email_checked_all_len(string $user_access_type_name=NULL){

        $user_access_type_ID=UserAccessTypeConfig::get_user_access_type_ID($user_access_type_name);

        $q=[
            'select'=>array(
                array(
                    'function'  =>'COUNT(*)',
                    'rename'    =>'len'
                )
            ),
            'table'=>self::$table_name,
            'join'=>[
                [
                    'table'=>UserAccess::$table_name,
                    'where'=>[
                        [
                            'table'         =>self::$table_name,
                            'table_join'    =>UserAccess::$table_name,
                            'column'        =>'id',
                            'column_join'   =>'user_id'
                        ],
                        [
                            'table'         =>UserAccess::$table_name,
                            'column'        =>'user_access_type_id',
                            'value'         =>$user_access_type_ID
                        ],
                        [
                            'table'         =>UserAccess::$table_name,
                            'column'        =>'type',
                            'value'         =>0
                        ]
                    ]
                ]
            ],
            'where'=>[
                [
                    'table'         =>self::$table_name,
                    'column'        =>'type',
                    'value'         =>0
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['len'];

    }

    /**
     * @param string|NULL $user_access_type_name
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_email_checked_all_30_days_len(string $user_access_type_name=NULL){

        $timestamp_to           =Date::get_timestamp();
        $timestamp_from         =$timestamp_to-30*24*3600;
        $user_access_type_ID    =UserAccessTypeConfig::get_user_access_type_ID($user_access_type_name);

        $q=[
            'select'=>array(
                array(
                    'function'  =>'COUNT(*)',
                    'rename'    =>'len'
                )
            ),
            'table'=>self::$table_name,
            'join'=>[
                [
                    'table'=>UserAccess::$table_name,
                    'where'=>[
                        [
                            'table'         =>self::$table_name,
                            'table_join'    =>UserAccess::$table_name,
                            'column'        =>'id',
                            'column_join'   =>'user_id'
                        ],
                        [
                            'table'         =>UserAccess::$table_name,
                            'column'        =>'user_access_type_id',
                            'value'         =>$user_access_type_ID
                        ],
                        [
                            'table'         =>UserAccess::$table_name,
                            'column'        =>'type',
                            'value'         =>0
                        ]
                    ]
                ]
            ],
            'where'=>[
                [
                    'table'         =>self::$table_name,
                    'column'        =>'date_create',
                    'method'        =>'>=',
                    'value'         =>Date::get_date_time_full($timestamp_from)
                ],
                [
                    'table'         =>self::$table_name,
                    'column'        =>'date_create',
                    'method'        =>'<=',
                    'value'         =>Date::get_date_time_full($timestamp_to)
                ],
                [
                    'table'         =>self::$table_name,
                    'column'        =>'type',
                    'value'         =>0
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['len'];

    }

    /**
     * @param int|NULL $user_ID
     * @return false|int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_date_online(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'date_online'
            ],
            'where'     =>[
                'id'    =>$user_ID,
                'type'  =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return empty($r[0]['date_online'])?NULL:strtotime($r[0]['date_online']);

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_login(int $user_ID=NULL){

        if(empty($user_ID))
            return !empty(self::$user_ID);

        $error=array(
            'title'     =>'Parameters problem',
            'info'      =>'Code for indety auth of user is not exists'
        );

        throw new ParametersException($error);

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_ID(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($user_ID,'_user',0);

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_ID_default(){

        if(empty(self::$user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default user ID is empty'
            );

            throw new ParametersException($error);

        }

        return self::isset_user_ID(self::$user_ID);

    }

    /**
     * @return bool
     */
    public  static function isset_user_default(){

        return !empty(self::$user_ID);

    }

    /**
     * @param int|NULL $user_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_data(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User_ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'date_create',
                'date_online',
                'date_block',
                'date_unblock',
                'date_remove',
                'date_recovery'
            ),
            'table'=>'_user',
            'where'=>array(
                'id'    =>$user_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return self::$date_list;

        return array(
            'date_create'       =>$r[0]['date_create'],
            'date_online'       =>$r[0]['date_online'],
            'date_block'        =>$r[0]['date_block'],
            'date_unblock'      =>$r[0]['date_unblock'],
            'date_remove'       =>$r[0]['date_remove'],
            'date_recovery'     =>$r[0]['date_recovery']
        );

    }

    /**
     * @param int $day_len
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_reg_count(int $day_len=10){

        $date_time=Date::get_date_for_db(Date::get_timestamp()-$day_len*24*3600).' 00:00:00';

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                [
                    'function'=>'COUNT(*) as len'
                ],
                [
                    'function'=>"DATE(date_create AT TIME ZONE 'UTC+3') as d"
                ]
            ],
            'where'=>[
                'date_create'       =>[
                    'method'    =>'>=',
                    'value'     =>$date_time
                ],
                'type'              =>0
            ],
            'group'=>[
                [
                    'function'=>"DATE(date_create AT TIME ZONE 'UTC+3')"
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['d']]=(int)$row['len'];

        return $list;

    }

    /**
     * @param int $day_len
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_reg_count_confirmed_email(int $day_len=10){

        $date_time=Date::get_date_for_db(Date::get_timestamp()-$day_len*24*3600).' 00:00:00';

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                [
                    'function'=>'COUNT(*) as len'
                ],
                [
                    'function'=>"DATE(date_create AT TIME ZONE 'UTC+3') as d"
                ]
            ],
            'where'=>[
                'date_create'       =>[
                    'method'        =>'>=',
                    'value'     =>$date_time
                ],
                'date_email_check'  =>[
                    'method'        =>'!=',
                    'value'         =>NULL
                ],
                'type'              =>0
            ],
            'group'=>[
                [
                    'function'=>"DATE(date_create AT TIME ZONE 'UTC+3')"
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['d']]=(int)$row['len'];

        return $list;

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_data_default(){

        if(empty(self::$user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default user ID is empty'
            );

            throw new ParametersException($error);

        }

        $user_data=self::get_user_data(self::$user_ID);

        if(count($user_data)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User din not get data',
                'data'      =>array(
                    'user_ID'       =>self::$user_ID
                )
            );

            throw new DbQueryException($error);

        }
        
        return $user_data;
        
    }

    /**
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user(){

        $q=array(
            'table'     =>'_user',
            'values'    =>array(
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>0
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_default(){

        $user_ID=self::add_user();

        if(empty($user_ID)){

            $error=array(
                'title'     =>'User DB problem',
                'info'      =>'New user was not make'
            );

            throw new DbQueryException($error);

        }

        self::$user_ID=$user_ID;

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_date_online(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User_ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_user',
            'set'=>array(
                'date_online'=>'NOW()'
            ),
            'where'=>array(
                array(
                    'column'    =>'id',
                    'value'     =>$user_ID
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User date online was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_date_update(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User_ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_user',
            'set'=>array(
                'date_update'=>'NOW()'
            ),
            'where'=>array(
                'id'    =>$user_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User date online was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_date_block(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User_ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_user',
            'set'=>array(
                'date_update'   =>'NOW()',
                'date_block'    =>'NOW()',
                'date_unblock'  =>NULL,
            ),
            'where'=>array(
                'id'    =>$user_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User date online was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_date_email_check(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User_ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_user',
            'set'=>array(
                'date_update'           =>'NOW()',
                'date_email_check'      =>'NOW()'
            ),
            'where'=>array(
                'id'    =>$user_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User date email check was not update'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_date_unblock(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User_ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_user',
            'set'=>array(
                'date_update'   =>'NOW()',
                'date_block'    =>NULL,
                'date_unblock'  =>'NOW()',
            ),
            'where'=>array(
                'id'    =>$user_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User date online was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_date_online_default(){

        if(empty(self::$user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default user ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!self::update_user_date_online(self::$user_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Default user dare online was not updated'
            );

            throw new DbQueryException($error);

        }

        self::$date_online=Date::get_timestamp();

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function block_user_ID(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User_ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_user',
            'set'=>array(
                'date_block'    =>'NOW()',
                'type'          =>1
            ),
            'where'=>array(
                'id'    =>$user_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User was not blocked'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function unblock_user_ID(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User_ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_user',
            'set'=>array(
                'date_unblock'  =>'NOW()',
                'type'          =>0
            ),
            'where'=>array(
                'id'    =>$user_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User was not unblocked'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function recovery_user_ID(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_user',
            'set'=>array(
                'date_recovery'     =>'NOW()',
                'type'              =>0
            ),
            'where'=>array(
                'id'        =>$user_ID,
                'type'      =>1
            ),
            'limit'=>1
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User was not recovered'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_user_data_default(){

        if(empty(self::$user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default user ID is empty'
            );

            throw new ParametersException($error);

        }

        $user_data=self::get_user_data_default();

        self::$date_create      =$user_data['date_create'];
        self::$date_online      =$user_data['date_online'];
        self::$date_block       =$user_data['date_block'];
        self::$date_unblock     =$user_data['date_unblock'];
        self::$date_remove      =$user_data['date_remove'];
        self::$date_recovery    =$user_data['date_recovery'];

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     */
    public  static function set_user_ID_default(int $user_ID=NULL){

        self::$user_ID=empty($user_ID)?NULL:$user_ID;

        return true;

    }

    /**
     * @param int|NULL $user_date_create
     * @return bool
     */
    public  static function set_user_date_create_default(int $user_date_create=NULL){
        
        self::$date_create=empty($user_date_create)?NULL:$user_date_create;

        return true;
        
    }

    /**
     * @param int|NULL $user_date_online
     * @return bool
     */
    public  static function set_user_date_online_default(int $user_date_online=NULL){
        
        self::$date_online=empty($user_date_online)?NULL:$user_date_online;

        return true;
        
    }

    /**
     * @param int|NULL $user_date_block
     * @return bool
     */
    public  static function set_user_date_block_default(int $user_date_block=NULL){
        
        self::$date_block=empty($user_date_block)?NULL:$user_date_block;

        return true;
        
    }

    /**
     * @param int|NULL $user_date_remove
     * @return bool
     */
    public  static function set_user_date_remove_default(int $user_date_remove=NULL){
        
        self::$date_remove=empty($user_date_remove)?NULL:$user_date_remove;

        return true;
        
    }

    /**
     * @param int|NULL $user_date_unblock
     * @return bool
     */
    public  static function set_user_date_unblock_default(int $user_date_unblock=NULL){
        
        self::$date_unblock=empty($user_date_unblock)?NULL:$user_date_unblock;

        return true;
        
    }

    /**
     * @param int|NULL $user_date_recovery
     * @return bool
     */
    public  static function set_user_date_recovery_default(int $user_date_recovery=NULL){
        
        self::$date_recovery=empty($user_date_recovery)?NULL:$user_date_recovery;

        return true;
        
    }

    /**
     * @param int|NULL $user_ID
     * @param bool $need_remove_child
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_ID(int $user_ID=NULL,bool $need_remove_child=true){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($user_ID,self::$table_name,0)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User was not remove'
            ];

            throw new DbQueryException($error);

        }

        if($need_remove_child){

            if(!UserAccess::remove_user_access($user_ID)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'User access was not remove'
                ];

                throw new DbQueryException($error);

            }

            if(!UserAddress::remove_user_address($user_ID)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'User address was not remove'
                ];

                throw new DbQueryException($error);

            }

            if(!UserAvatar::remove_user_avatar($user_ID)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'User avatar was not remove'
                ];

                throw new DbQueryException($error);

            }

            if(!UserBlock::remove_user_block($user_ID)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'User block was not remove'
                ];

                throw new DbQueryException($error);

            }

            if(!UserData::remove_user_data($user_ID)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'User data was not remove'
                ];

                throw new DbQueryException($error);

            }

            if(!UserDevice::remove_user_device($user_ID)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'User devices were not remove'
                ];

                throw new DbQueryException($error);

            }

            if(!UserEmail::remove_user_email($user_ID)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'User email was not remove'
                ];

                throw new DbQueryException($error);

            }

            if(!UserHash::remove_user_hash($user_ID)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'User hash was not remove'
                ];

                throw new DbQueryException($error);

            }

            if(!UserLogin::remove_user_login($user_ID)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'User login was not remove'
                ];

                throw new DbQueryException($error);

            }

            if(!UserPhone::remove_user_phone($user_ID)){

                $error=[
                    'title'     =>DbQueryException::$title,
                    'info'      =>'User login was not remove'
                ];

                throw new DbQueryException($error);

            }

        }

        return true;

    }

}