<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserBalanceTransaction{

    /** @var string */
    public  static $table_name='_user_balance_transaction';

    /**
     * @param array $user_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_balance_isset_transaction(array $user_ID_list=[]){

        if(count($user_ID_list)==0)
            return[];

        $q=[
            'select'=>[
                'user_id'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'user_id'       =>$user_ID_list,
                'action'        =>[
                    'auto_payment',
                    'promo_code',
                    'promo_login_invite',
                    'promo_login_use',
                    'add_hand_money',
                    'confirm_email_money',
                ],
                'type'          =>0
            ],
            'group'=>[
                [
                    'column'=>'user_id'
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['user_id']]=$row['user_id'];

        return $list;

    }

    /**
     * @param string|NULL $hash
     * @param bool $is_confirm
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_balance_transaction_ID_from_hash(string $hash=NULL,bool $is_confirm=false){

        if(empty($hash)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'hash'              =>$hash,
            'is_confirm'        =>(int)$is_confirm
        ];

        return Db::get_row_ID(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_balance_transaction_list(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id',
                'wallet_name_id',
                'user_wallet_address',
                'target_wallet_address',
                'action',
                'transaction_sum',
                'transaction_wallet_sum',
                'balance',
                'promo_code',
                'date_create'
            ],
            'where'=>[
                'user_id'   =>$user_ID,
                'type'      =>0
            ],
            'order'=>[
                [
                    'column'        =>'id',
                    'direction'     =>'desc'
                ]
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'ID'                        =>$row['id'],
                'wallet_name_ID'            =>$row['wallet_name_id'],
                'action'                    =>$row['action'],
                'transaction_sum'           =>$row['transaction_sum'],
                'transaction_wallet_sum'    =>$row['transaction_wallet_sum'],
                'balance'                   =>$row['balance'],
                'user_wallet_address'       =>$row['user_wallet_address'],
                'target_wallet_address'     =>$row['target_wallet_address'],
                'promo_code'                =>$row['promo_code'],
                'date_create'               =>strtotime($row['date_create'])
            ];

        return $list;

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $wallet_name_ID
     * @param int|NULL $user_wallet_ID
     * @param int|NULL $target_wallet_ID
     * @param int|NULL $user_wallet_donate_ID
     * @param string|NULL $action
     * @param float|NULL $sum
     * @param string|NULL $info
     * @param float|NULL $balance
     * @param string|NULL $user_wallet_address
     * @param string|NULL $target_wallet_address
     * @param float|NULL $transaction_wallet_sum
     * @param string|NULL $promo_code
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_balance_transaction(int $user_ID=NULL,int $wallet_name_ID=NULL,int $user_wallet_ID=NULL,int $target_wallet_ID=NULL,int $user_wallet_donate_ID=NULL,string $action=NULL,float $sum=NULL,string $info=NULL,float $balance=NULL,string $user_wallet_address=NULL,string $target_wallet_address=NULL,float $transaction_wallet_sum=NULL,string $promo_code=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($action))
            $error_info_list[]='Action is empty';

        if(empty($sum))
            $error_info_list[]='Sum is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'                   =>$user_ID,
                'wallet_name_id'            =>$wallet_name_ID,
                'user_wallet_id'            =>$user_wallet_ID,
                'target_wallet_id'          =>$target_wallet_ID,
                'user_wallet_donate_id'     =>$user_wallet_donate_ID,
                'action'                    =>$action,
                'transaction_sum'           =>$sum,
                'transaction_wallet_sum'    =>$transaction_wallet_sum,
                'balance'                   =>empty($balance)?NULL:$balance,
                'user_wallet_address'       =>empty($user_wallet_address)?NULL:$user_wallet_address,
                'target_wallet_address'     =>empty($target_wallet_address)?NULL:$target_wallet_address,
                'info'                      =>empty($info)?NULL:$info,
                'promo_code'                =>empty($promo_code)?NULL:$promo_code,
                'date_create'               =>'NOW()',
                'date_update'               =>'NOW()',
                'type'                      =>0
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User balance transaction was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_balance_transaction_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_balance_transaction_to_confirmed(int $user_balance_transaction_ID=NULL){

        if(empty($user_balance_transaction_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User balance transaction ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'is_confirm'        =>1,
                'date_update'       =>'NOW()'
            ],
            'where'=>[
                'id'        =>$user_balance_transaction_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User balance transaction was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_balance_transaction_ID
     * @param float $balance
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_balance_in_user_balance_transaction(int $user_balance_transaction_ID=NULL,float $balance=0){

        if(empty($user_balance_transaction_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User balance transaction ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'balance'           =>$balance,
                'date_update'       =>'NOW()'
            ],
            'where'=>[
                'id'        =>$user_balance_transaction_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User balance transaction was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_balance_transaction(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'=>$user_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User balance was not remove'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

}