<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserAccess{
    
    /** @var int */
    public  static $user_access_ID;

    /** @var array */
    public  static $user_access_ID_list             =[];

    /** @var bool */
    public  static $is_root                         =false;

    /** @var bool */
    public  static $is_admin                        =false;

    /** @var bool */
    public  static $is_profile                      =false;

    /** @var bool */
    public  static $is_profile_wallet               =false;

    /** @var bool */
    public  static $is_bot                          =false;

    /** @var bool */
    public  static $is_app                          =false;

    /** @var bool */
    public  static $is_web                          =false;

    /** @var bool */
    public  static $is_debug                        =false;

    /** @var bool */
    public  static $is_guest                        =false;

    /** @var string */
    public  static $table_name                      ='_user_access';

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function reset_data(){

        self::$user_access_ID           =NULL;
        self::$user_access_ID_list      =[];

        self::set_user_access_type_name_list_default([]);

        return true;

    }

    /**
     * @return bool
     */
    public  static function reset_user_access_list(){

        self::$is_root                          =false;
        self::$is_admin                         =false;
        self::$is_profile                       =false;
        self::$is_profile_wallet                =false;
        self::$is_bot                           =false;
        self::$is_app                           =false;
        self::$is_web                           =false;
        self::$is_guest                         =false;

        return true;

    }

    /**
     * @param int|NULL $user_access_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_access_ID(int $user_access_ID=NULL){

        if(empty($user_access_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($user_access_ID,'_user_access',0);

    }

    /**
     * @param int|NULL $user_ID
     * @param string|NULL $user_access_type_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_access(int $user_ID=NULL,string $user_access_type_name=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($user_access_type_name))
            $error_info_list[]='User access type name is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $user_access_type_ID=UserAccessType::get_user_access_type_ID($user_access_type_name);

        if(empty($user_access_type_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access type name is not valid'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'               =>$user_ID,
            'user_access_type_id'   =>$user_access_type_ID
        );

        return Db::isset_row('_user_access',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_access_ID(int $user_ID=NULL){
        
        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);
            
        }
        
        $where_list=array(
            'user_id'=>$user_ID
        );
        
        return Db::get_row_ID('_user_access',0,$where_list);
        
    }

    /**
     * @param array $access_list
     * @return array
     */
    public  static function get_list_access(array $access_list=[]){

        return UserAccessTypeConfig::get_user_access_type_list($access_list);

    }

    /**
     * @param array $user_access_type_name_list
     * @return array
     */
    public  static function get_prepare_user_access_type_name_list(array $user_access_type_name_list=[]){

        if(count($user_access_type_name_list)==0)
            return[];

        $list=[];

        foreach($user_access_type_name_list as $user_access_type_name)
            if(array_search($user_access_type_name,UserAccessTypeConfig::$user_access_type_name_list)!==false)
                $list[]=$user_access_type_name;

        return $list;

    }

    /**
     * @param int|NULL $user_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_access_type_name_list(int $user_ID=NULL){
        
        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);
            
        }
        
        $q=array(
            'select'=>array(
                'id',
                'user_access_type_id'
            ),
            'table' =>'_user_access',
            'where' =>array(
                'user_id'       =>$user_ID,
                'type'          =>0
            )
        );

        $r=Db::select($q);
        
        if(count($r)==0)
            return[];

        $user_access_type_ID_list=[];

        foreach($r as $row)
            if(!empty($row['user_access_type_id']))
                $user_access_type_ID_list[]=$row['user_access_type_id'];

        return UserAccessType::get_user_access_type_name_list($user_access_type_ID_list);
        
    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_access_list_default(){

        if(empty(User::$user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $user_access_type_name_list=self::get_user_access_type_name_list(User::$user_ID);

        if(count($user_access_type_name_list)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User access list is empty'
            );

            throw new ParametersException($error);

        }

        return self::set_user_access_type_name_list_default($user_access_type_name_list);

    }

    /**
     * @param int|NULL $user_access_type_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_len_from_access_type_ID(int $user_access_type_ID=NULL){

        if(empty($user_access_type_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'User access type ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_access_type_id'=>$user_access_type_ID
        ];

        return Db::get_row_len(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param array $user_access_type_name_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_access(int $user_ID=NULL,array $user_access_type_name_list=[]){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(count($user_access_type_name_list)==0)
            $error_info_list[]='User access type name list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $user_access_type_ID_list=UserAccessType::get_user_access_type_ID_list($user_access_type_name_list);

        if(count($user_access_type_ID_list)==0){

            $error=array(
                'title'     =>'Db query problem',
                'info'      =>'Access list is empty'
            );

            throw new ParametersException($error);

        }

        $value_list=[];

        foreach($user_access_type_ID_list as $user_access_type_ID)
            if(!empty($user_access_type_ID))
                $value_list[]=array(
                    'user_id'               =>$user_ID,
                    'user_access_type_id'   =>$user_access_type_ID,
                    'date_create'           =>'NOW()',
                    'date_update'           =>'NOW()',
                    'type'                  =>0
                );

        if(count($value_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Access list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_access',
            'values'    =>$value_list
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User access was not added'
            );

            throw new DbQueryException($error);

        }

        if(count($r)==0)
            return[];

        return $r;

    }

    /**
     * @param int|NULL $user_access_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_access_ID(int $user_access_ID=NULL){

        if(empty($user_access_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_user_access',
            'set'=>array(
                'date_remove'   =>'NOW()',
                'type'          =>1
            ),
            'where'=>array(
                'id'    =>$user_access_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User access was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_access(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_user_access',
            'set'=>array(
                'date_remove'   =>'NOW()',
                'type'          =>1
            ),
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'Db query problem',
                'info'      =>'User access was not remove',
                'data'      =>array(
                    'user_ID'   =>$user_ID,
                    'query'     =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param array $user_access_type_name_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_access_type(int $user_ID=NULL,array $user_access_type_name_list=[]){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(count($user_access_type_name_list)==0)
            $error_info_list[]='User access type name list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $user_access_type_ID_list=UserAccessType::get_user_access_type_ID_list($user_access_type_name_list);

        if(count($user_access_type_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access type ID list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_user_access',
            'set'=>array(
                'date_remove'   =>'NOW()',
                'type'          =>1
            ),
            'where'=>array(
                'user_id'               =>$user_ID,
                'user_access_type_id'   =>$user_access_type_ID_list,
                'type'                  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query probelm',
                'info'      =>'User access was not remove'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param array $user_access_ID_list
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_access_ID_list(array $user_access_ID_list=[]){

        if(count($user_access_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access ID list is empty'
            );

            throw new ParametersException($error);

        }

        $ID_list=[];

        foreach($user_access_ID_list as $user_access_ID)
            if(!empty($user_access_ID))
                $ID_list[]=$user_access_ID;

        if(count($ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User access ID list is empty'
            );

            throw new ParametersException($error);

        }

        return Db::delete_from_ID_list($ID_list,'_user_access',0);

    }

    /**
     * @param int|NULL $user_access_ID
     */
    public  static function set_user_access_ID_default(int $user_access_ID=NULL){
        
        self::$user_access_ID=empty($user_access_ID)?NULL:$user_access_ID;
        
    }

    /**
     * @param array $user_access_type_name_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_user_access_type_name_list_default(array $user_access_type_name_list=[]){

        self::reset_user_access_list();

        foreach($user_access_type_name_list as $user_access_type_name)
            switch($user_access_type_name){

                case'root':{

                    self::$is_root=true;

                    break;

                }

                case'admin':{

                    self::$is_admin=true;

                    break;

                }

                case'profile':{

                    self::$is_profile=true;

                    break;

                }

                case'profile_wallet':{

                    self::$is_profile_wallet=true;

                    break;

                }

                case'bot':{

                    self::$is_bot=true;

                    break;

                }

                case'app':{

                    self::$is_app=true;

                    break;

                }

                case'web':{

                    self::$is_web=true;

                    break;

                }

                case'debug':{

                    self::$is_debug=true;

                    break;

                }

                case'guest':{

                    self::$is_guest=true;

                    break;

                }

                default:{

                    $error=array(
                        'title'             =>'Parameters problem',
                        'info'              =>'User access parameter from list is not exists',
                        'data'              =>array(
                            'list'      =>$user_access_type_name_list,
                            'key'       =>$user_access_type_name
                        )
                    );

                    throw new ParametersException($error);

                }

            }

        return true;

    }

}