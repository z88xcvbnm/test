<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserDevice{

    /** @var int */
    public  static $user_device_ID;

    /** @var array */
    public  static $user_device_list;

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$user_device_ID       =NULL;
        self::$user_device_list     =[];

    }

    /**
     * @param int|NULL $user_device_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_user_device_ID(int $user_device_ID=NULL){

        if(empty($user_device_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User device ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($user_device_ID,'_user_device',0);

    }

    /**
     * @param int|NULL $device_token_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\PhpException
     */
    public  static function isset_user_device_from_device_token_ID(int $device_token_ID=NULL){

        if(empty($device_token_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device token ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'device_token_id'   =>$device_token_ID
        );

        return Db::isset_row('_user_device',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @return array
     * @throws ParametersException
     */
    public  static function get_user_device_list(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'device_firm_id',
                'device_model_id',
                'device_token_id',
                'os_id',
                'os_version_id'
            ),
            'table'=>'_user_device',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=array(
                'device_firm_ID'        =>$row['device_firm_id'],
                'device_model_ID'       =>$row['device_model_id'],
                'device_token_ID'       =>$row['device_token_id'],
                'os_ID'                 =>$row['os_id'],
                'os_version_ID'         =>$row['os_version_id']
            );

        return $list;

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $device_token_ID
     * @return array|null
     * @throws ParametersException
     */
    public  static function get_user_device_data(int $user_ID=NULL,int $device_token_ID=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list['user_ID']='User ID is empty';

        if(empty($device_token_ID))
            $error_info_list['device_token_ID']='Device token ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'device_firm_id',
                'device_model_id',
                'device_token_id',
                'os_id',
                'os_version_id'
            ),
            'table'=>'_user_device',
            'where'=>array(
                'user_id'           =>$user_ID,
                'device_token_id'   =>$device_token_ID,
                'type'              =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'                    =>$r[0]['id'],
            'device_firm_ID'        =>$r[0]['device_firm_id'],
            'device_model_ID'       =>$r[0]['device_model_id'],
            'device_token_ID'       =>$r[0]['device_token_id'],
            'os_ID'                 =>$r[0]['os_id'],
            'os_version_ID'         =>$r[0]['os_version_id']
        );

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $device_firm_ID
     * @param int|NULL $device_model_ID
     * @param int|NULL $device_token_ID
     * @param int|NULL $os_ID
     * @param int|NULL $os_version_ID
     * @return int|null
     * @throws ParametersException
     */
    public  static function get_user_device_ID(int $user_ID=NULL,int $device_firm_ID=NULL,int $device_model_ID=NULL,int $device_token_ID=NULL,int $os_ID=NULL,int $os_version_ID=NULL){

        if(
              empty($user_ID)
            &&empty($device_firm_ID)
            &&empty($device_model_ID)
            &&empty($device_token_ID)
            &&empty($os_ID)
            &&empty($os_version_ID)
        ){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($device_firm_ID))
            $where_list['device_firm_id']=$device_firm_ID;

        if(!empty($device_model_ID))
            $where_list['device_model_id']=$device_model_ID;

        if(!empty($device_token_ID))
            $where_list['device_token_id']=$device_token_ID;

        if(!empty($os_ID))
            $where_list['os_id']=$os_ID;

        if(!empty($os_version_ID))
            $where_list['os_version_id']=$os_version_ID;

        if(count($where_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        return Db::get_row_ID('_user_device',0,$where_list);

    }

    /**
     * @param int|NULL $device_token_ID
     * @return null|int
     * @throws ParametersException
     */
    public  static function get_user_ID_from_device_token_ID(int $device_token_ID=NULL){

        if(empty($device_token_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device token ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'user_id'
            ),
            'table'=>'_user_device',
            'where'=>array(
                'device_token_id'   =>$device_token_ID,
                'type'              =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['user_id'];

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $device_firm_ID
     * @param int|NULL $device_model_ID
     * @param int|NULL $device_token_ID
     * @param int|NULL $os_ID
     * @param int|NULL $os_version_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function add_user_device(int $user_ID=NULL,int $device_firm_ID=NULL,int $device_model_ID=NULL,int $device_token_ID=NULL,int $os_ID=NULL,int $os_version_ID=NULL,bool $is_google=false,bool $is_apple=false){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($device_firm_ID))
            $error_info_list[]='Device firm ID is empty';

        if(empty($device_model_ID))
            $error_info_list[]='Device model ID is empty';

        if(empty($os_ID))
            $error_info_list[]='OS ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_device',
            'values'    =>array(
                'user_id'           =>$user_ID,
                'device_firm_id'    =>$device_firm_ID,
                'device_model_id'   =>$device_model_ID,
                'device_token_id'   =>$device_token_ID,
                'os_id'             =>$os_ID,
                'os_version_id'     =>$os_version_ID,
                'google'            =>(int)$is_google,
                'apple'             =>(int)$is_apple,
                'date_create'       =>'NOW()',
                'date_update'       =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User device was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_device_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_user_device_ID(int $user_device_ID=NULL){

        if(empty($user_device_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User device ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($user_device_ID,'_user_device',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User device was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $device_firm_ID
     * @param int|NULL $device_model_ID
     * @param int|NULL $device_token_ID
     * @param int|NULL $os_ID
     * @param int|NULL $os_version_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_device(int $user_ID=NULL,int $device_firm_ID=NULL,int $device_model_ID=NULL,int $device_token_ID=NULL,int $os_ID=NULL,int $os_version_ID=NULL){

        if(
              empty($user_ID)
            &&empty($device_firm_ID)
            &&empty($device_model_ID)
            &&empty($device_token_ID)
            &&empty($os_ID)
            &&empty($os_version_ID)
        ){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($device_firm_ID))
            $where_list['device_firm_id']=$device_firm_ID;

        if(!empty($device_model_ID))
            $where_list['device_model_id']=$device_model_ID;

        if(!empty($device_token_ID))
            $where_list['device_token_id']=$device_token_ID;

        if(!empty($os_ID))
            $where_list['os_id']=$os_ID;

        if(!empty($os_version_ID))
            $where_list['os_version_id']=$os_version_ID;

        if(count($where_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_where_list('_user_device',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User device was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_device_ID
     */
    public  static function set_user_device_ID_default(int $user_device_ID=NULL){

        self::$user_device_ID=empty($user_device_ID)?NULL:$user_device_ID;

    }

    /**
     * @param array|NULL $user_device_list
     */
    public  static function set_user_device_list_default(array $user_device_list=NULL){

        self::$user_device_list=empty($user_device_list)?NULL:$user_device_list;

    }

}