<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserAvatar{

    /**
     * @param int|NULL $user_ID
     * @return array|null
     * @throws ParametersException
     */
    public  static function get_user_avatar_data(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'image_id'
            ),
            'table'=>'_user_avatar',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'        =>$r[0]['id'],
            'image_ID'  =>$r[0]['image_id']
        );

    }

    /**
     * @param int|NULL $user_ID
     * @return int|null
     * @throws ParametersException
     */
    public  static function get_user_avatar_image_ID(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'image_id'
            ),
            'table'=>'_user_avatar',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['image_id'];

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $image_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function add_user_avatar(int $user_ID=NULL,int $image_ID=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list['user_ID']='User ID is empty';

        if(empty($image_ID))
            $error_info_list['image_ID']='Image ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_avatar',
            'values'    =>array(
                'user_id'       =>$user_ID,
                'image_id'      =>$image_ID,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'User avatar was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_avatar_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\PhpException
     */
    public  static function remove_user_avatar_ID(int $user_avatar_ID=NULL){

        if(empty($user_avatar_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User avatar ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($user_avatar_ID,'_user_avatar',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User avatar was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $image_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\PhpException
     */
    public  static function remove_user_avatar(int $user_ID=NULL,int $image_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'=>$user_ID
        );

        if(!empty($image_ID))
            $where_list['image_id']=$image_ID;

        if(!Db::delete_from_where_list('_user_avatar',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User avatar was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

}