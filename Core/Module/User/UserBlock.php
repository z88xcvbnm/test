<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserBlock{

    /** @var int */
    public  static $user_block_ID;

    /** @var int */
    public  static $action_user_ID;

    /** @var string */
    public  static $user_block_admin_info;

    /** @var string */
    public  static $user_block_user_info;

    /** @var int */
    public  static $user_date_block;

    /** @var array */
    public  static $column_name_list=array(
        'id',
        'action_user_id',
        'admin_info',
        'user_info',
        'date_create',
        'date_update',
        'date_remove',
        'date_recovery'
    );

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$user_block_ID            =NULL;
        self::$action_user_ID           =NULL;
        self::$user_block_admin_info    =NULL;
        self::$user_block_user_info     =NULL;
        self::$user_date_block          =NULL;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_block(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'=>$user_ID
        );

        return Db::isset_row('_user_block',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param array $column_name_list
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_block_full_from_column_name(int $user_ID=NULL,array $column_name_list=[]){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(count($column_name_list)==0)
            $error_info_list[]='Column name list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $column_name_checked_list=[];

        foreach($column_name_list as $column_name)
            if(!empty($column_name))
                if(array_search($column_name,self::$column_name_list)!==false)
                    $column_name_checked_list[]=$column_name;

        if(count($column_name_checked_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Column name list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>$column_name_checked_list,
            'table'=>'_user_block',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0];

    }

    /**
     * @param int|NULL $user_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_block_full_info(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $user_block_info=self::get_user_block_full_from_column_name($user_ID,self::$column_name_list);

        if(empty($user_block_info))
            return NULL;

        $data=array(
            'ID'                =>$user_block_info[0]['id'],
            'action_user_ID'    =>$user_block_info[0]['action_user_id'],
            'date_create'       =>$user_block_info[0]['date_create']
        );

        if(!empty($user_block_info[0]['admin_info']))
            $data['admin_info']=$user_block_info[0]['admin_info'];

        if(!empty($user_block_info[0]['user_info']))
            $data['user_info']=$user_block_info[0]['user_info'];

        return $data;

    }

    /**
     * @param int|NULL $user_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_block_admin_info(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $column_name_list=array(
            'id',
            'admin_info'
        );

        $user_block_info=self::get_user_block_full_from_column_name($user_ID,$column_name_list);

        if(empty($user_block_info))
            return NULL;

        $data=array(
            'ID'=>$user_block_info[0]['id']
        );

        if(!empty($user_block_info[0]['admin_info']))
            $data['admin_info']=$user_block_info[0]['admin_info'];

        return $data;

    }

    /**
     * @param int|NULL $user_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_block_user_info(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $column_name_list=array(
            'id',
            'user_info'
        );

        $user_block_info=self::get_user_block_full_from_column_name($user_ID,$column_name_list);

        if(empty($user_block_info))
            return NULL;

        $data=array(
            'ID'=>$user_block_info[0]['id']
        );

        if(!empty($user_block_info[0]['user_info']))
            $data['user_info']=$user_block_info[0]['user_info'];

        return $data;

    }

    /**
     * @param array $user_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_block_list(array $user_ID_list=[]){

        if(count($user_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID list is empty'
            );

            throw new ParametersException($error);

        }

        $ID_list=[];

        foreach($user_ID_list as $user_ID)
            if(!empty($user_ID))
                $ID_list[]=$user_ID;

        if(count($ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'user_id'
            ),
            'table'=>'_user_block',
            'where'=>array(
                'user_id'   =>$ID_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['user_id'];

        return $list;

    }

    /**
     * @param int|NULL $action_user_ID
     * @param int|NULL $user_ID
     * @param string|NULL $admin_info
     * @param string|NULL $user_info
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_block(int $action_user_ID=NULL,int $user_ID=NULL,string $admin_info=NULL,string $user_info=NULL){

        $error_info_list=[];

        if(empty($action_user_ID))
            $error_info_list[]='Action user ID is empty';

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $value_list=array(
            'action_user_id'    =>$action_user_ID,
            'user_id'           =>$user_ID,
            'date_create'       =>'NOW()'
        );

        if(!empty($admin_info))
            $value_list['admin_info']=$admin_info;

        if(!empty($user_info))
            $value_list['user_info']=$user_info;

        $q=array(
            'table'     =>'_user_block',
            'values'    =>$value_list
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query pronlem',
                'info'      =>$error_info_list
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_block_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_block_ID(int $user_block_ID=NULL){

        if(empty($user_block_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User block ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($user_block_ID,'_user_block',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User block was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_block(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'=>$user_ID
        );

        if(!Db::delete_from_where_list('_user_block',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User block was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_block_ID
     */
    public  static function set_user_block_ID_default(int $user_block_ID=NULL){
        
        self::$user_block_ID=empty($user_block_ID)?NULL:$user_block_ID;
        
    }

    /**
     * @param int|NULL $action_user_ID
     */
    public  static function set_action_user_ID_default(int $action_user_ID=NULL){
        
        self::$action_user_ID=empty($action_user_ID)?NULL:$action_user_ID;
        
    }

    /**
     * @param string|NULL $user_block_admin_info
     */
    public  static function set_user_block_admin_info(string $user_block_admin_info=NULL){
        
        self::$user_block_admin_info=empty($user_block_admin_info)?NULL:$user_block_admin_info;
        
    }

    /**
     * @param string|NULL $user_block_user_info
     */
    public  static function set_user_block_user_info(string $user_block_user_info=NULL){
        
        self::$user_block_user_info=empty($user_block_user_info)?NULL:$user_block_user_info;
        
    }

}