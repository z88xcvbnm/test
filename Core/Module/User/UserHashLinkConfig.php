<?php

namespace Core\Module\User;

use Core\Module\Exception\ParametersException;
use Core\Module\Url\Url;

class UserHashLinkConfig{

    /** @var string */
    public  static $link_registration_invite        ='registration_invite';

    /** @var string */
    public  static $link_recovery_password          ='recovery_password';

    /** @var string */
    public  static $link_confirm_email              ='confirm_email';

    /**
     * @param string|NULL $key
     * @return string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_link(string $key=NULL){

        if(empty($key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Key is empty'
            );

            throw new ParametersException($error);

        }

        switch($key){

            case'registration_invite':
                return self::$link_registration_invite;

            case'recovery_password':
                return self::$link_recovery_password;

            case'confirm_email':
                return self::$link_confirm_email;

            default:{

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>'Key is not valid'
                );

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @param string|NULL $key
     * @return string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_link_full(string $key=NULL){

        $link=self::get_link($key);

        return \Config::$http_type.'://'.Url::$host.'/'.$link;

    }

}