<?php

namespace Core\Module\User;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class UserHash{

    /** @var int */
    public  static $user_hash_ID;

    /** @var int */
    public  static $user_hash_type_ID;

    /** @var string */
    public  static $code;

    /** @var string */
    public  static $hash;

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$user_hash_ID             =NULL;
        self::$user_hash_type_ID        =NULL;
        self::$code                     =NULL;
        self::$hash                     =NULL;

    }

    /**
     * @param int|NULL $user_hash_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_hash_ID(int $user_hash_ID=NULL){

        if(empty($user_hash_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User hash ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($user_hash_ID,'_user_hash',0);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $user_hash_type_ID
     * @param string|NULL $code
     * @param string|NULL $hash
     * @param bool|NULL $is_hash_use
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_user_hash(int $user_ID=NULL,int $user_hash_type_ID=NULL,string $code=NULL,string $hash=NULL,bool $is_hash_use=NULL){

        if(empty($user_ID)&&empty($user_hash_type_ID)&&empty($code)&&empty($hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($user_hash_type_ID))
            $where_list['user_hash_type_id']=$user_hash_type_ID;

        if(!empty($code))
            $where_list['code']=$code;

        if(!empty($hash))
            $where_list['hash']=$hash;

        if(!is_null($is_hash_use))
            $where_list['date_use']=$is_hash_use?'IS NOT NULL':NULL;

        return Db::isset_row('_user_hash',0,$where_list);

    }

    /**
     * @param int|NULL $user_hash_ID
     * @param int|NULL $user_hash_type_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_hash_data(int $user_hash_ID=NULL,int $user_hash_type_ID=NULL){

        if(empty($user_hash_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User hash ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'user_id',
                'user_hash_type_id',
                'code',
                'hash',
                'date_create',
                'date_use'
            ),
            'table'=>'_user_hash',
            'where'=>array(
                'id'        =>$user_hash_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        if(!is_null($user_hash_type_ID))
            $q['where']['user_hash_type_id']=$user_hash_type_ID;

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'                    =>$r[0]['id'],
            'user_ID'               =>$r[0]['user_id'],
            'user_hash_type_ID'     =>$r[0]['user_hash_type_id'],
            'code'                  =>$r[0]['code'],
            'hash'                  =>$r[0]['hash'],
            'date_create'           =>$r[0]['date_create'],
            'date_use'              =>$r[0]['date_use']
        );

    }

    /**
     * @param string|NULL $hash
     * @param int|NULL $user_hash_type_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_ID(string $hash=NULL,int $user_hash_type_ID=NULL){

        if(empty($hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>array(
                    'hash'=>'Hash is empty'
                )
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'user_id'
            ),
            'table'=>'_user_hash',
            'where'=>array(
                'hash'      =>$hash,
                'type'      =>0
            ),
            'limit'=>1
        );

        if(!is_null($user_hash_type_ID))
            $q['where']['user_hash_type_id']=$user_hash_type_ID;

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['user_id'];

    }

    /**
     * @param string|NULL $hash
     * @param int|NULL $user_hash_type_ID
     * @param bool|NULL $is_hash_use
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_hash_data_from_hash(string $hash=NULL,int $user_hash_type_ID=NULL,bool $is_hash_use=NULL){

        if(empty($hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Hash is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'user_id',
                'user_hash_type_id',
                'code',
                'login',
                'hash',
                'date_create',
                'date_use'
            ),
            'table'=>'_user_hash',
            'where'=>array(
                'hash'      =>$hash,
                'type'      =>0
            ),
            'limit'=>1
        );

        if(!is_null($user_hash_type_ID))
            $q['where']['user_hash_type_id']=$user_hash_type_ID;

        if(!is_null($is_hash_use))
            $q['where']['date_use']=$is_hash_use?'IS NOT NULL':NULL;

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'                    =>$r[0]['id'],
            'user_ID'               =>$r[0]['user_id'],
            'user_hash_type_ID'     =>$r[0]['user_hash_type_id'],
            'code'                  =>$r[0]['code'],
            'login'                 =>$r[0]['login'],
            'hash'                  =>$r[0]['hash'],
            'date_create'           =>$r[0]['date_create'],
            'date_use'              =>$r[0]['date_use']
        );

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $user_hash_type_ID
     * @param string|NULL $code
     * @param string|NULL $hash
     * @param bool|NULL $is_hash_use
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_user_hash_ID(int $user_ID=NULL,int $user_hash_type_ID=NULL,string $code=NULL,string $hash=NULL,bool $is_hash_use=NULL){

        if(empty($user_ID)&&empty($user_hash_type_ID)&&empty($code)&&empty($hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($user_hash_type_ID))
            $where_list['user_hash_type_id']=$user_hash_type_ID;

        if(!empty($code))
            $where_list['code']=$code;

        if(!empty($hash))
            $where_list['hash']=$hash;

        if(!is_null($is_hash_use))
            $where_list['date_use']=$is_hash_use?'IS NOT NULL':NULL;

        return Db::get_row_ID('_user_hash',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $user_hash_type_ID
     * @param string|NULL $code
     * @param string|NULL $hash
     * @param string|NULL $login
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_user_hash(int $user_ID=NULL,int $user_hash_type_ID=NULL,string $code=NULL,string $hash=NULL,string $login=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($user_hash_type_ID))
            $error_info_list[]='User hash type ID is empty';

        if(empty($hash))
            $error_info_list[]='Hash is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_hash',
            'values'    =>[
                'user_id'               =>$user_ID,
                'user_hash_type_id'     =>$user_hash_type_ID,
                'code'                  =>empty($code)?NULL:$code,
                'login'                 =>empty($login)?NULL:$login,
                'hash'                  =>$hash,
                'date_create'           =>'NOW()',
                'date_update'           =>'NOW()',
                'type'                  =>0
            ]
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'User hash was not added'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $user_hash_type_ID
     * @param string|NULL $code
     * @param string|NULL $hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_hash_date_use(int $user_ID=NULL,int $user_hash_type_ID=NULL,string $code=NULL,string $hash=NULL){

        if(empty($user_ID)&&empty($user_hash_type_ID)&&empty($code)&&empty($hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($user_hash_type_ID))
            $where_list['user_hash_type_id']=$user_hash_type_ID;

        if(!empty($code))
            $where_list['code']=$code;

        if(!empty($hash))
            $where_list['hash']=$hash;

        $q=array(
            'table'     =>'_user_hash',
            'set'       =>array(
                'date_use'      =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>'2'
            ),
            'where'     =>$where_list
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User hash date use was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_hash_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_hash_date_use_from_user_hash_ID(int $user_hash_ID=NULL){

        if(empty($user_hash_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User hash ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_hash',
            'set'       =>array(
                'date_use'      =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>'2'
            ),
            'where'     =>[
                'id'            =>$user_hash_ID,
                'type'          =>0
            ]
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User hash date use was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_hash_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_user_hash_use_from_user_hash_ID(int $user_hash_ID=NULL){

        if(empty($user_hash_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User hash ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_user_hash',
            'set'       =>array(
                'date_use'      =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>'2'
            ),
            'where'     =>array(
                'id'        =>$user_hash_ID,
                'type'      =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User hash date use was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_hash_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_hash_ID(int $user_hash_ID=NULL){

        if(empty($user_hash_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User hash ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($user_hash_ID,'_user_hash',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User hash was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $user_hash_type_ID
     * @param string|NULL $code
     * @param string|NULL $hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_user_hash(int $user_ID=NULL,int $user_hash_type_ID=NULL,string $code=NULL,string $hash=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'=>$user_ID
        );

        if(!empty($user_hash_type_ID))
            $where_list['user_hash_type_id']=$user_hash_type_ID;

        if(!empty($code))
            $where_list['code']=$code;

        if(!empty($hash))
            $where_list['hash']=$hash;

        if(!Db::delete_from_where_list('_user_hash',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'User hash was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_hash_ID
     */
    public  static function set_user_hash_ID_default(int $user_hash_ID=NULL){

        self::$user_hash_ID=empty($user_hash_ID)?NULL:$user_hash_ID;

    }

    /**
     * @param int|NULL $user_hash_type_ID
     */
    public  static function set_user_hash_type_ID_default(int $user_hash_type_ID=NULL){

        self::$user_hash_type_ID=empty($user_hash_type_ID)?NULL:$user_hash_type_ID;

    }

    /**
     * @param string|NULL $code
     */
    public  static function set_code_default(string $code=NULL){

        self::$code=empty($code)?NULL:$code;

    }

    /**
     * @param string|NULL $hash
     */
    public  static function set_hash_default(string $hash=NULL){

        self::$hash=empty($hash)?NULL:$hash;

    }

}