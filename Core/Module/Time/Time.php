<?php

namespace Core\Module\Time;

class Time{

    /**
     * @param string|NULL $file_path
     * @return int
     */
    public  static function get_milliseconds_from_time_string(string $time_string=NULL){

        $list           =mb_split(':',$time_string);
        $milliseconds   =0;

        if(count($list)==3)
            $milliseconds=ceil((ceil($list[0])*3600+ceil($list[1])*60+$list[2])*1000);
        else if(count($list)==2)
            $milliseconds=ceil((ceil($list[1])*60+$list[2])*1000);
        else if(count($list)==1)
            $milliseconds=ceil($list[2]*1000);

        return $milliseconds;

    }

    /**
     * @param string|NULL $file_path
     * @return float|int
     */
    public  static function get_seconds_from_time_string(string $time_string=NULL){

        $list       =mb_split(':',$time_string);
        $seconds    =0;

        if(count($list)==3)
            $seconds=ceil($list[0])*3600+ceil($list[1])*60+ceil($list[2]);
        else if(count($list)==2)
            $seconds=ceil($list[1])*60+ceil($list[2]);
        else if(count($list)==1)
            $seconds=ceil($list[2]);

        return $seconds;

    }

}