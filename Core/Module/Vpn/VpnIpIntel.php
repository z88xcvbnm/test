<?php

namespace Core\Module\Vpn;

use Core\Module\Curl\CurlGet;
use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Ip\Ip;
use Core\Module\Json\Json;
use Core\Module\Log\Log;

class VpnIpIntel{

    /** @var string */
    private static $url_check='http://check.getipintel.net/check.php';

    /** @var array */
    private static $data;

    /**
     * @return bool|null
     */
    private static function set_success(){

        switch((int)self::$data['result']){

            case 0:{

                Ip::$is_vpn=false;

                return false;

            }

            case 1:{

                Ip::$is_vpn=true;

                return true;

            }

        }

        return NULL;

    }

    /**
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_error(){

        switch((int)self::$data['result']){

            case -1:{

                $error_info='Invalid no input';

                break;

            }

            case -2:{

                $error_info='Invalid IP address';

                break;

            }

            case -3:{

                $error_info='Unroutable address / private address';

                break;

            }

            case -4:{

                $error_info='Unable to reach database, most likely the database is being updated. Keep an eye on twitter for more information';

                break;

            }

            case -5:{

                $error_info='Your connecting IP has been banned from the system or you do not have permission to access a particular service. Did you exceed your query limits? Did you use an invalid email address? If you want more information, please use the contact links below';

                break;

            }

            case -6:{

                $error_info='You did not provide any contact information with your query or the contact information is invalid';

                break;

            }

            default:{

                $error_info='You exceed the number of allowed queries';

                break;

            }

        }

        Log::init([$error_info],true,true);

        return NULL;

    }

    /**
     * @return bool|null
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_vpn(){

        $data=array(
            'ip'        =>Ip::$ip_address,
            'contact'   =>\Config::$email_log,
            'flags'     =>'m',
            'format'    =>'json'
        );

        $r=CurlGet::init(self::$url_check,$data);

        if(empty($r))
            return NULL;

        if(!is_array($r))
            self::$data=Json::decode($r['data']);
        else
            self::$data=$r;

        $error_info_list=[];

        if(empty(self::$data['status']))
            $error_info_list[]='Status is not exists';

        if(isset(self::$data['result']))
            $error_info_list[]='Result is not exists';

        if(count($error_info_list)>0)
            return NULL;

        switch(self::$data['status']){

            case'success':
                return self::set_success();

            case'error':
                return self::set_error();

            default:
                return NULL;

        }

    }

    /**
     * @return bool|null
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        return self::check_vpn();

    }

    /**
     * @return bool|null
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}