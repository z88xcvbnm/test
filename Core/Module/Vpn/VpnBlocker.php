<?php

namespace Core\Module\Vpn;

use Core\Module\Curl\CurlGet;
use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Ip\Ip;
use Core\Module\Json\Json;
use Core\Module\Log\Log;

class VpnBlocker{

    /** @var string */
    private static $url_check='http://api.vpnblocker.net/v2/json';

    /** @var array */
    private static $data;

    /**
     * @return bool
     */
    private static function set_success(){

        Ip::$is_vpn=(bool)self::$data['host-ip'];

        return Ip::$is_vpn;

    }

    /**
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_error(){

        Log::init(['Unknown error'],true,true);

        return NULL;

    }

    /**
     * @return bool|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_vpn(){

        $url    =self::$url_check.'/'.Ip::$ip_address;
        $r      =CurlGet::init($url);

        if(empty($r))
            return NULL;

        self::$data=Json::decode($r['data']);

        $error_info_list=[];

        if(empty(self::$data['status']))
            $error_info_list[]='Status is not exists';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        switch(self::$data['status']){

            case'success':
                return self::set_success();

            case'error':
                return self::set_error();

            default:
                return NULL;

        }

    }

    /**
     * @return bool|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        return self::check_vpn();

    }

    /**
     * @return bool|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}