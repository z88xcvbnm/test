<?php

namespace Core\Module\App;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class AppVersion{

    /** @var int */
    public  static $app_version_ID;

    /** @var string */
    public  static $app_version;

    /** @var string */
    public  static $app_build;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$app_version_ID       =NULL;
        self::$app_version          =NULL;
        self::$app_build            =NULL;

        return true;

    }

    /**
     * @param int|NULL $app_ID
     * @param string|NULL $app_version
     * @param string|NULL $app_build
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_app_version(int $app_ID=NULL,string $app_version=NULL,string $app_build=NULL){

        $error_info_list=[];

        if(empty($app_ID))
            $error_info_list[]='App ID is empty';

        if(empty($app_version))
            $error_info_list[]='App version is empty';

        if(empty($app_build))
            $error_info_list[]='App build is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'app_ID'        =>$app_ID,
                    'app_version'   =>$app_version,
                    'app_build'     =>$app_build
                )
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'app_id'        =>$app_ID,
            'version'       =>$app_version,
            'build'         =>$app_build
        );

        return Db::isset_row('_app_version',0,$where_list);

    }

    /**
     * @param int|NULL $app_version_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_app_version_ID(int $app_version_ID=NULL){

        if(empty($app_version_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App version ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($app_version_ID,'_app_version',0);

    }

    /**
     * @param int|NULL $app_version_ID
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_app_version_ID(int $app_version_ID=NULL){

        if(empty($app_version_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App version ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::delete_from_ID($app_version_ID,'_app_version',0);

    }

    /**
     * @param int|NULL $app_ID
     * @param string|NULL $app_version
     * @param string|NULL $app_build
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_app_version(int $app_ID=NULL,string $app_version=NULL,string $app_build=NULL){

        if(empty($app_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App ID is empty',
                'data'      =>array(
                    'app_ID'        =>$app_ID,
                    'app_version'   =>$app_version,
                    'app_build'     =>$app_build
                )
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!is_null($app_version))
            $where_list['version']=strtolower($app_version);

        if(!is_null($app_build))
            $where_list['build']=strtolower($app_build);

        if(count($where_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are is empty',
                'data'      =>array(
                    'app_ID'        =>$app_ID,
                    'app_version'   =>$app_version,
                    'app_build'     =>$app_build
                )
            );

            throw new ParametersException($error);

        }

        return Db::delete_from_where_list('_app_version',0,$where_list);

    }

    /**
     * @param int|NULL $app_ID
     * @param string|NULL $app_version
     * @param string|NULL $app_build
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_app_version(int $app_ID=NULL,string $app_version=NULL,string $app_build=NULL){

        $error_info_list=[];

        if(empty($app_ID))
            $error_info_list[]='App ID is empty';

        if(empty($app_version))
            $error_info_list[]='App version is empty';

        if(empty($app_build))
            $error_info_list[]='App build is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'app_ID'        =>$app_ID,
                    'app_version'   =>$app_version,
                    'app_build'     =>$app_build
                )
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_app_version',
            'values'    =>array(
                'app_id'        =>$app_ID,
                'version'       =>$app_version,
                'build'         =>$app_build,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'App version was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $app_ID
     * @param string|NULL $app_version
     * @param string|NULL $app_build
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_version_ID(int $app_ID=NULL,string $app_version=NULL,string $app_build=NULL){

        if(empty($app_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App ID is empty',
                'data'      =>array(
                    'app_ID'        =>$app_ID,
                    'app_version'   =>$app_version,
                    'app_build'     =>$app_build
                )
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!is_null($app_version))
            $where_list['version']=strtolower($app_version);

        if(!is_null($app_build))
            $where_list['build']=strtolower($app_build);

        if(count($where_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are is empty',
                'data'      =>array(
                    'app_ID'        =>$app_ID,
                    'app_version'   =>$app_version,
                    'app_build'     =>$app_build
                )
            );

            throw new ParametersException($error);

        }

        return Db::get_row_ID('_app_version',0,$where_list);

    }

    /**
     * @param int|NULL $app_version_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_version_info(int $app_version_ID=NULL){

        if(empty($app_version_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App version ID is empty'
            );

            throw new ParametersException($error);

        }

        $select_list=array(
            'app_id',
            'version',
            'build'
        );

        $r=Db::get_data_from_ID($app_version_ID,'_app_version',$select_list,0);

        if(count($r)==0)
            return NULL;

        return array(
            'app_ID'        =>$r['app_id'],
            'app_version'   =>$r['version'],
            'app_build'     =>$r['build']
        );

    }

    /**
     * @param int|NULL $app_version_ID
     * @return bool
     */
    public  static function set_app_version_ID_default(int $app_version_ID=NULL){

        self::$app_version_ID=$app_version_ID;

        return true;

    }

    /**
     * @param string|NULL $app_version
     * @return bool
     */
    public  static function set_app_version_default(string $app_version=NULL){

        self::$app_version=strtolower($app_version);

        return true;

    }

    /**
     * @param string|NULL $app_build
     * @return bool
     */
    public  static function set_app_build_default(string $app_build=NULL){

        self::$app_build=strtolower($app_build);

        return true;

    }

}