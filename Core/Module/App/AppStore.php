<?php

namespace Core\Module\App;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class AppStore{

    /** @var int */
    public  static $app_store_ID;

    /** @var string */
    public  static $store_name;
    
    /** @var string */
    public  static $app_name;

    /** @var string */
    public  static $key;

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$app_store_ID     =NULL;
        self::$store_name       =NULL;
        self::$app_name         =NULL;
        self::$key              =NULL;

    }

    /**
     * @param int|NULL $app_store_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_app_store_ID(int $app_store_ID=NULL){

        if(empty($app_store_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App store ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($app_store_ID,'_app_store',0);

    }

    /**
     * @param int|NULL $app_ID
     * @param string|NULL $store_name
     * @param string|NULL $app_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_app_store(int $app_ID=NULL,string $store_name=NULL,string $app_name=NULL){

        if(empty($app_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'app_id'=>$app_ID
        );

        if(!empty($store_name))
            $where_list['store_name']=$store_name;
        
        if(!empty($app_name))
            $where_list['app_name']=$app_name;

        return Db::isset_row('_app_store',0,$where_list);

    }

    /**
     * @param int|NULL $app_ID
     * @param string|NULL $store_name
     * @param string|NULL $app_name
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_store_ID(int $app_ID=NULL,string $store_name=NULL,string $app_name=NULL){

        if(empty($app_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'app_id'=>$app_ID
        );

        if(!empty($store_name))
            $where_list['store_name']=$store_name;

        if(!empty($app_name))
            $where_list['app_name']=$app_name;

        return Db::get_row_ID('_app_store',0,$where_list);

    }

    /**
     * @param int|NULL $app_store_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_store_data_from_app_store_ID(int $app_store_ID=NULL){

        if(empty($app_store_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App store ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'app_id',
                'name',
                'key'
            ),
            'table'=>'_app_store',
            'where'=>array(
                'id'    =>$app_store_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        return array(
            'ID'            =>$app_store_ID,
            'app_ID'        =>$r[0]['app_id'],
            'name'          =>$r[0]['name'],
            'key'           =>$r[0]['key']
        );

    }

    /**
     * @param int|NULL $app_ID
     * @param string|NULL $app_store_name
     * @param string|NULL $app_name
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_store_data(int $app_ID=NULL,string $app_store_name=NULL,string $app_name=NULL){

        if(empty($app_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'app_id'=>$app_ID
        );

        if(!empty($app_store_name))
            $where_list['store_name']=$app_store_name;

        if(!empty($app_app_name))
            $where_list['app_name']=$app_name;

        $q=array(
            'select'=>array(
                'id',
                'app_id',
                'store_name',
                'app_name',
                'key'
            ),
            'table'=>'_app_store',
            'where'=>array(
                'app_id'    =>$app_ID,
                'name'      =>$app_store_name,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'            =>$r[0]['id'],
            'app_ID'        =>$r[0]['app_id'],
            'store_name'    =>$r[0]['store_name'],
            'app_name'      =>$r[0]['app_name'],
            'key'           =>$r[0]['key']
        );

    }

    /**
     * @param int|NULL $app_ID
     * @param string|NULL $store_name
     * @param string|NULL $app_name
     * @param string|NULL $key
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_app_store(int $app_ID=NULL,string $store_name=NULL,string $app_name=NULL,string $key=NULL){

        $error_info_list=[];

        if(empty($app_ID))
            $error_info_list[]='App ID is empty';

        if(empty($store_name))
            $error_info_list[]='App store name is empty';

        if(empty($app_name))
            $error_info_list[]='App name is empty';

        if(empty($key))
            $error_info_list[]='Key is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_app_store',
            'values'    =>array(
                'app_id'        =>$app_ID,
                'store_name'    =>$store_name,
                'app_name'      =>$app_name,
                'key'           =>$key,
                'date_create'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'App store was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $app_store_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_app_store_ID(int $app_store_ID=NULL){

        if(empty($app_store_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App store ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($app_store_ID,'_app_store',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'App store was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $app_ID
     * @param string|NULL $store_name
     * @param string|NULL $app_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_app_store(int $app_ID=NULL,string $store_name=NULL,string $app_name=NULL){

        if(empty($app_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'app_id'=>$app_ID
        );

        if(!empty($store_name))
            $where_list['store_name']=$store_name;

        if(!empty($app_name))
            $where_list['app_name']=$app_name;

        if(!Db::delete_from_where_list('_app_store',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'App store was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $app_store_ID
     * @return bool
     */
    public  static function set_app_store_ID_default(int $app_store_ID=NULL){

        self::$app_store_ID=empty($app_store_ID)?NULL:$app_store_ID;

        return true;

    }

    /**
     * @param string|NULL $store_name
     * @return bool
     */
    public  static function set_store_name_default(string $store_name=NULL){

        self::$store_name=empty($store_name)?NULL:$store_name;

        return true;

    }

    /**
     * @param string|NULL $app_name
     * @return bool
     */
    public  static function set_app_name_default(string $app_name=NULL){

        self::$app_name=empty($app_name)?NULL:$app_name;

        return true;

    }

    /**
     * @param string|NULL $key
     * @return bool
     */
    public  static function set_key_default(string $key=NULL){

        self::$key=empty($key)?NULL:$key;

        return true;

    }

}