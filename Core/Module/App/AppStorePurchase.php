<?php

namespace Core\Module\App;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class AppStorePurchase{

    /** @var int */
    public  static $app_store_purchase_ID;

    /** @var string */
    public  static $app_store_purchase_name;

    /** @var array */
    public  static $app_store_purchase_list;

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$app_store_purchase_ID        =NULL;
        self::$app_store_purchase_name      =NULL;
        self::$app_store_purchase_list      =NULL;

    }

    /**
     * @param int|NULL $app_store_purchase_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_app_store_purchase_ID(int $app_store_purchase_ID=NULL){

        if(empty($app_store_purchase_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App store purchase ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($app_store_purchase_ID,'_app_store_purchase');

    }

    /**
     * @param string|NULL $app_store_purchase_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_app_store_purchase_name(string $app_store_purchase_name=NULL){

        if(empty($app_store_purchase_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App store purchase name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name'=>$app_store_purchase_name
        );

        return Db::isset_row('_app_store_purchase',0,$where_list);

    }

    /**
     * @param int|NULL $app_store_purchase_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_store_purchase_name(int $app_store_purchase_ID=NULL){

        if(empty($app_store_purchase_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App store purchase ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_app_store_purchase',
            'where'=>array(
                'id'    =>$app_store_purchase_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param int|NULL $app_store_purchase_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_store_purchase_data(int $app_store_purchase_ID=NULL){

        if(empty($app_store_purchase_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App store purchase ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'app_id',
                'app_store_id',
                'name'
            ),
            'table'=>'_app_store_purchase',
            'where'=>array(
                'id'    =>$app_store_purchase_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'app_ID'        =>$r[0]['app_id'],
            'app_store_ID'  =>$r[0]['app_store_id'],
            'name'          =>$r[0]['name']
        );

    }

    /**
     * @param int|NULL $app_ID
     * @param int|NULL $app_store_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_store_purchase_list(int $app_ID=NULL,int $app_store_ID=NULL){

        $error_info_list=[];

        if(empty($app_ID))
            $error_info_list[]='App ID is empty';

        if(empty($app_store_ID))
            $error_info_list[]='App store ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'app_id',
                'app_store_id',
                'name'
            ),
            'table'=>'_app_store_purchase',
            'where'=>array(
                'app_id'        =>$app_ID,
                'app_store_id'  =>$app_store_ID,
                'type'          =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'            =>$r[0]['id'],
            'app_ID'        =>$r[0]['app_id'],
            'app_store_ID'  =>$r[0]['app_store_id'],
            'name'          =>$r[0]['name']
        );

    }

    /**
     * @param int|NULL $app_ID
     * @param int|NULL $app_store_ID
     * @param string|NULL $app_store_purchase_name
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_store_purchase_ID(int $app_ID=NULL,int $app_store_ID=NULL,string $app_store_purchase_name=NULL){

        if(empty($app_store_purchase_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App store purchase name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name'=>$app_store_purchase_name
        );

        if(!empty($app_ID))
            $where_list['app_id']=$app_ID;

        if(!empty($app_store_ID))
            $where_list['app_store_id']=$app_store_ID;

        return Db::get_row_ID('_app_store_purchase',0,$where_list);

    }

    /**
     * @param int|NULL $app_ID
     * @param int|NULL $app_store_ID
     * @param string|NULL $purchase
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_app_store_purchase(int $app_ID=NULL,int $app_store_ID=NULL,string $purchase=NULL){

        $error_info_list=[];

        if(empty($app_ID))
            $error_info_list[]='App ID is empty';

        if(empty($app_store_ID))
            $error_info_list[]='App store ID is empty';

        if(empty($purchase))
            $error_info_list[]='Purchase is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_app_store_purchase',
            'values'    =>array(
                'app_id'        =>$app_ID,
                'app_store_id'  =>$app_store_ID,
                'name'          =>$purchase,
                'date_create'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'App store purchase was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $app_store_purchase_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_app_store_purchase_ID(int $app_store_purchase_ID=NULL){

        if(empty($app_store_purchase_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App store purchase ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($app_store_purchase_ID,'_app_store_purchase',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'App store purchase was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $app_ID
     * @param int|NULL $app_store_ID
     * @param string|NULL $app_store_purchase_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remover_app_store_purchase(int $app_ID=NULL,int $app_store_ID=NULL,string $app_store_purchase_name=NULL){

        if(empty($app_store_purchase_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App store purchase name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name'=>$app_store_purchase_name
        );

        if(!empty($app_ID))
            $where_list['app_id']=$app_ID;

        if(!empty($app_store_ID))
            $where_list['app_store_id']=$app_store_ID;

        if(!Db::delete_from_where_list('_app_store_purchase',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'App store purchase was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $app_store_purchase_ID
     * @return bool
     */
    public  static function set_app_store_purchase_ID(int $app_store_purchase_ID=NULL){
        
        self::$app_store_purchase_ID=empty($app_store_purchase_ID)?NULL:$app_store_purchase_ID;

        return true;
        
    }

    /**
     * @param string|NULL $app_store_purchase_name
     * @return bool
     */
    public  static function set_app_store_purchase_name(string $app_store_purchase_name=NULL){
        
        self::$app_store_purchase_name=empty($app_store_purchase_name)?NULL:$app_store_purchase_name;

        return true;
        
    }

    /**
     * @param array|NULL $app_store_purchase_list
     * @return bool
     */
    public  static function set_app_store_purchase_list(array $app_store_purchase_list=NULL){
        
        self::$app_store_purchase_list=empty($app_store_purchase_list)?NULL:$app_store_purchase_list;

        return true;
        
    }

}