<?php

namespace Core\Module\App;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class App{

    /** @var int */
    public  static $app_ID;

    /** @var string */
    public  static $app_name;

    /** @var string */
    public  static $app_info;

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$app_ID       =NULL;
        self::$app_name     =NULL;
        self::$app_info     =NULL;

    }

    /**
     * @param string|NULL $app_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_app(string $app_name=NULL){

        if(empty($app_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name_lower'=>strtolower($app_name)
        );

        return Db::isset_row('_app',0,$where_list);

    }

    /**
     * @param int|NULL $app_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_app_ID(int $app_ID=NULL){

        if(empty($app_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($app_ID,'_app',0);

    }

    /**
     * @param string|NULL $app_name
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_ID(string $app_name=NULL){

        if(empty($app_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name_lower'=>strtolower($app_name)
        );

        return Db::get_row_ID('_app',0,$where_list);

    }

    /**
     * @param int|NULL $app_ID
     * @param array $column_list
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_data_from_app_ID(int $app_ID=NULL,array $column_list=[]){

        if(empty($app_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App ID is empty'
            );

            throw new ParametersException($error);

        }

        if(count($column_list)==0)
            $column_list=array(
                'id',
                'name',
                'info'
            );

        $data=Db::get_data_from_ID($app_ID,'_app',$column_list,0);

        if(empty($data))
            return NULL;

        return $data;

    }

    /**
     * @param string|NULL $app_name
     * @param array $column_list
     * @return array|bool|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_app_data_from_app_name(string $app_name=NULL,array $column_list=[]){

        if(empty($app_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App name is empty'
            );

            throw new ParametersException($error);

        }

        if(count($column_list)==0)
            $column_list=array(
                'id',
                'name',
                'info'
            );

        $q=array(
            'select'        =>$column_list,
            'table'         =>'_app',
            'where_list'    =>array(
                'name_lower'    =>strtolower($app_name),
                'type'          =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r;

    }

    /**
     * @param string|NULL $app_name
     * @param string|NULL $app_info
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_app(string $app_name=NULL,string $app_info=NULL){

        if(empty($app_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App name is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_app',
            'values'    =>array(
                'name_lower'    =>strtolower($app_name),
                'info'          =>empty($app_info)?NULL:$app_info,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'App was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param string|NULL $app_name
     * @param bool $need_remove_app_version
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_app(string $app_name=NULL,bool $need_remove_app_version=true){

        if(empty($app_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App name is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_app_version){

            $app_ID=self::get_app_ID($app_name);

            if(!empty($app_ID))
                if(!AppVersion::remove_app_version($app_ID)){

                    $error=array(
                        'title'     =>'DB query problem',
                        'info'      =>'App version was not removed'
                    );

                    throw new DbQueryException($error);

                }

        }

        $where_list=array(
            'name_lower'=>strtolower($app_name)
        );

        return Db::delete_from_where_list('_app',0,$where_list);

    }

    /**
     * @param int|NULL $app_ID
     * @param bool $need_remove_app_version
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_app_ID(int $app_ID=NULL,bool $need_remove_app_version=true){

        if(empty($app_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App ID is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_app_version)
            if(!AppVersion::remove_app_version($app_ID)){

                $error=array(
                    'title'     =>'DB query problem',
                    'info'      =>'App version was not removed'
                );

                throw new DbQueryException($error);

            }

        return Db::delete_from_ID($app_ID,'_app',0);

    }


    /**
     * @param int|NULL $app_ID
     */
    public  static function set_app_ID_default(int $app_ID=NULL){

        self::$app_ID=empty($app_ID)?NULL:$app_ID;

    }

    /**
     * @param string|NULL $app_name
     */
    public  static function set_app_name_default(string $app_name=NULL){

        self::$app_name=empty($app_name)?NULL:$app_name;

    }

    /**
     * @param string|NULL $app_info
     */
    public  static function set_app_info_default(string $app_info=NULL){

        self::$app_info=empty($app_info)?NULL:$app_info;

    }

}