<?php

namespace Core\Module\Error;

use Core\Module\Log\Log;

class UnknownException extends \ErrorException{

    /**
     * UnknownException constructor.
     * @param $message
     * @param $code
     * @param $severity
     * @param $filename
     * @param $lineno
     * @param $previous
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public function __construct($message,$code,$severity,$filename,$lineno,$previous){

        $error=array(
            'type'          =>'E_UNKNOWN',
            'title'         =>array(
                'ru'    =>'Неизвестная ошибка',
                'en'    =>'Unknown error'
            ),
            'info'          =>array(
                'ru'    =>'Неизвестная ошибка',
                'en'    =>'Unknown error'
            ),
            'code'          =>$code,
            'line'          =>$lineno,
            'file'          =>$filename,
            'severity'      =>$severity,
            'message'       =>$message,
            'previous'      =>$previous
        );

        Log::init($error);

        exit;

    }

}