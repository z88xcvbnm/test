<?php

namespace Core\Module\Error;

use Core\Module\Log\Log;

class UserErrorException extends \ErrorException{

    /**
     * UserErrorException constructor.
     * @param $message
     * @param $code
     * @param $severity
     * @param $filename
     * @param $lineno
     * @param $previous
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public function __construct($message,$code,$severity,$filename,$lineno,$previous){

        $error=array(
            'type'          =>'E_USER_ERROR',
            'title'         =>array(
                'ru'    =>'Сообщения об ошибках сгенерированные пользователем',
                'en'    =>'User-generated error message'
            ),
            'info'          =>array(
                'ru'    =>'Сообщения об ошибках сгенерированные пользователем. Такие ошибки схожи с E_ERROR, за исключением того, что они генерируются в коде скрипта средствами функции PHP trigger_error().',
                'en'    =>'User-generated error message. This is like an E_ERROR, except it is generated in PHP code by using the PHP function trigger_error().'
            ),
            'code'          =>$code,
            'line'          =>$lineno,
            'file'          =>$filename,
            'severity'      =>$severity,
            'message'       =>$message,
            'previous'      =>$previous
        );

        Log::init($error);

        exit;

    }

}