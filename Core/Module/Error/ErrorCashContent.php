<?php

namespace Core\Module\Error;

use Core\Module\Data\Data;

class ErrorCashContent{

    /** @var array */
    private static $error_list=[];

    /**
     * @param string|NULL $key
     * @param string|NULL $content
     * @return bool
     */
    public  static function add_error(string $key=NULL,string $content=NULL){

        if(empty($key)||empty($content))
            return false;

        if(isset(self::$error_list[$key])){

            if(Data::is_array(self::$error_list[$key]))
                self::$error_list[$key][]=$content;
            else
                self::$error_list[$key]=array(
                    self::$error_list[$key],
                    $content
                );

            return true;

        }

        self::$error_list[$key]=$content;

        return true;

    }

    /**
     * @param string|NULL $key
     * @param array $content_list
     * @return bool
     */
    public  static function add_error_list(string $key=NULL,array $content_list=[]){

        if(empty($key)||empty($content_list))
            return false;

        if(isset(self::$error_list[$key]))
            self::$error_list[$key]=array_merge(self::$error_list[$key],array_values($content_list));
        else
            self::$error_list[$key]=$content_list;

        return true;

    }

    /**
     * @param array $content_key_list
     * @return bool
     */
    public  static function add_error_key_list(array $content_key_list=[]){

        foreach($content_key_list as $key=>$row)
            if(Data::is_array($row))
                self::add_error_list($key,$row);
            else
                self::add_error($key,$row);

        return true;

    }

    /**
     * @return array
     */
    public  static function get_error_list(){

        $list=[];

        foreach(self::$error_list as $key=>$row)
            if(Data::is_array($row)){

                if(count($row)==1)
                    $list[$key]=$row[0];
                else
                    $list[$key]=$row;

            }
            else
                $list[$key]=$row;

        self::$error_list=$list;

        return self::$error_list;

    }

}