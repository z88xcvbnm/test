<?php

namespace Core\Module\Error;

use Core\Module\Log\Log;

class WarningException extends \ErrorException{

    /**
     * WarningException constructor.
     * @param $message
     * @param $code
     * @param $severity
     * @param $filename
     * @param $lineno
     * @param $previous
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public function __construct($message,$code,$severity,$filename,$lineno,$previous){

        $error=array(
            'type'          =>'E_WARNING',
            'title'         =>array(
                'ru'    =>'Предупреждения времени выполнения',
                'en'    =>'Run-time warnings'
            ),
            'info'          =>array(
                'ru'    =>'Предупреждения времени выполнения (не фатальные ошибки). Выполнение скрипта в таком случае не прекращается.',
                'en'    =>'Run-time warnings (non-fatal errors). Execution of the script is not halted.'
            ),
            'code'          =>$code,
            'line'          =>$lineno,
            'file'          =>$filename,
            'severity'      =>$severity,
            'message'       =>$message,
            'previous'      =>$previous
        );

        Log::init($error);

        exit;

    }

}