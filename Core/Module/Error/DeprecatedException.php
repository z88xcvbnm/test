<?php

namespace Core\Module\Error;

use Core\Module\Log\Log;

class DeprecatedException extends \ErrorException{

    /**
     * DeprecatedException constructor.
     * @param $message
     * @param $code
     * @param $severity
     * @param $filename
     * @param $lineno
     * @param $previous
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public function __construct($message,$code,$severity,$filename,$lineno,$previous){

        $error=array(
            'type'          =>'E_DEPRECATED',
            'title'         =>array(
                'ru'    =>'Уведомления времени выполнения об использовании устаревших конструкций',
                'en'    =>'Run-time notices'
            ),
            'info'          =>array(
                'ru'    =>'Уведомления времени выполнения об использовании устаревших конструкций. Включаются для того, чтобы получать предупреждения о коде, который не будет работать в следующих версиях PHP.',
                'en'    =>'Run-time notices. Enable this to receive warnings about code that will not work in future versions.'
            ),
            'code'          =>$code,
            'line'          =>$lineno,
            'file'          =>$filename,
            'severity'      =>$severity,
            'message'       =>$message,
            'previous'      =>$previous
        );

        Log::init($error);

        exit;

    }

}