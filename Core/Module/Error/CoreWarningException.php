<?php

namespace Core\Module\Error;

use Core\Module\Log\Log;

class CoreWarningException extends \ErrorException{

    /**
     * CoreWarningException constructor.
     * @param $message
     * @param $code
     * @param $severity
     * @param $filename
     * @param $lineno
     * @param $previous
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public function __construct($message,$code,$severity,$filename,$lineno,$previous){

        $error=array(
            'type'          =>'E_CORE_WARNING',
            'title'         =>array(
                'ru'    =>'Предупреждения (не фатальные ошибки), которые происходят во время начального запуска',
                'en'    =>'Warnings (non-fatal errors) that occur during PHP\'s initial startup'
            ),
            'info'          =>array(
                'ru'    =>'Предупреждения (не фатальные ошибки), которые происходят во время начального запуска РНР. Такие предупреждения схожи с E_WARNING, за исключением того, что они генерируются ядром PHP.',
                'en'    =>'Warnings (non-fatal errors) that occur during PHP\'s initial startup. This is like an E_WARNING, except it is generated by the core of PHP.'
            ),
            'code'          =>$code,
            'line'          =>$lineno,
            'file'          =>$filename,
            'severity'      =>$severity,
            'message'       =>$message,
            'previous'      =>$previous
        );

        Log::init($error);

        exit;

    }

}