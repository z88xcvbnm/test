<?php

namespace Core\Module\Error;

use Core\Module\Log\Log;

class NoticeException extends \ErrorException{

    /**
     * NoticeException constructor.
     * @param $message
     * @param $code
     * @param $severity
     * @param $filename
     * @param $lineno
     * @param $previous
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public function __construct($message,$code,$severity,$filename,$lineno,$previous){

        $error=array(
            'type'          =>'E_NOTICE',
            'title'         =>array(
                'ru'    =>'Уведомления времени выполнения',
                'en'    =>'Run-time notices'
            ),
            'info'          =>array(
                'ru'    =>'Уведомления времени выполнения. Указывают на то, что во время выполнения скрипта произошло что-то, что может указывать на ошибку, хотя это может происходить и при обычном выполнении программы.',
                'en'    =>'Run-time notices. Indicate that the script encountered something that could indicate an error, but could also happen in the normal course of running a script.'
            ),
            'code'          =>$code,
            'line'          =>$lineno,
            'file'          =>$filename,
            'severity'      =>$severity,
            'message'       =>$message,
            'previous'      =>$previous
        );

        Log::init($error);

        exit;

    }

}