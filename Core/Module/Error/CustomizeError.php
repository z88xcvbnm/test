<?php

namespace Core\Module\Error;

use Core\Module\Response\ResponseServerError;
use Core\Module\Worktime\Worktime;

class CustomizeError{

    /** @var bool */
    public static $is_error=false;

    /**
     * @return bool
     */
    public static function init(){

        Worktime::set_timestamp_point('Customize Error Start');

        set_error_handler(array('Core\Module\Error\CustomizeError','error_handler'));
        register_shutdown_function(array('Core\Module\Error\CustomizeError', 'error_fatal_handler'));

        ob_start();

        Worktime::set_timestamp_point('Customize Error Finish');

        return true;

    }

    /**
     * @return bool
     * @throws CompileErrorException
     * @throws CoreErrorException
     * @throws CoreWarningException
     * @throws DeprecatedException
     * @throws FatalErrorException
     * @throws NoticeException
     * @throws ParseException
     * @throws RecoverableErrorException
     * @throws StrictException
     * @throws UnknownException
     * @throws UserDeprecatedException
     * @throws UserErrorException
     * @throws UserNoticeException
     * @throws UserWarningException
     * @throws WarningException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function error_fatal_handler(){

        if(error_reporting()===0)
            return false;

        $error=error_get_last();

        if(isset($error)){
            switch($error['type']){

                case E_ERROR:
                    throw new FatalErrorException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_WARNING:
                    throw new WarningException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_PARSE:
                    throw new ParseException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_NOTICE:
                    throw new NoticeException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_CORE_ERROR:
                    throw new CoreErrorException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_CORE_WARNING:
                    throw new CoreWarningException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_COMPILE_ERROR:
                    throw new CompileErrorException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_COMPILE_WARNING:
                    throw new CoreWarningException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_USER_ERROR:
                    throw new UserErrorException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_USER_WARNING:
                    throw new UserWarningException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_USER_NOTICE:
                    throw new UserNoticeException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_STRICT:
                    throw new StrictException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_RECOVERABLE_ERROR:
                    throw new RecoverableErrorException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_DEPRECATED:
                    throw new DeprecatedException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

                case E_USER_DEPRECATED:
                    throw new UserDeprecatedException($error['message'],$error['type'],NULL,$error['file'],$error['line'],NULL);

            }

            ob_end_flush();

        }
        else
            ob_end_flush();

        return true;

    }

    /**
     * @param $err_severity
     * @param $err_msg
     * @param $err_file
     * @param $err_line
     * @param array $err_context
     * @return bool
     * @throws CompileErrorException
     * @throws CoreErrorException
     * @throws CoreWarningException
     * @throws DeprecatedException
     * @throws FatalErrorException
     * @throws NoticeException
     * @throws ParseException
     * @throws RecoverableErrorException
     * @throws StrictException
     * @throws UnknownException
     * @throws UserDeprecatedException
     * @throws UserErrorException
     * @throws UserNoticeException
     * @throws UserWarningException
     * @throws WarningException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public static function error_handler($err_severity,$err_msg,$err_file,$err_line,array $err_context){

        if(error_reporting()===0)
            return false;

        self::$is_error=true;

        ResponseServerError::init();

        switch($err_severity){

            case E_ERROR:
                throw new FatalErrorException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_WARNING:
                throw new WarningException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_PARSE:
                throw new ParseException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_NOTICE:
                throw new NoticeException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_CORE_ERROR:
                throw new CoreErrorException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_CORE_WARNING:
                throw new CoreWarningException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_COMPILE_ERROR:
                throw new CompileErrorException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_COMPILE_WARNING:
                throw new CoreWarningException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_USER_ERROR:
                throw new UserErrorException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_USER_WARNING:
                throw new UserWarningException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_USER_NOTICE:
                throw new UserNoticeException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_STRICT:
                throw new StrictException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_RECOVERABLE_ERROR:
                throw new RecoverableErrorException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_DEPRECATED:
                throw new DeprecatedException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            case E_USER_DEPRECATED:
                throw new UserDeprecatedException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

            default:
                throw new UnknownException($err_msg,0,$err_severity,$err_file,$err_line,$err_context);

        }

    }

}

