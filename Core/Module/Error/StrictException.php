<?php

namespace Core\Module\Error;

use Core\Module\Log\Log;

class StrictException extends \ErrorException{

    /**
     * StrictException constructor.
     * @param $message
     * @param $code
     * @param $severity
     * @param $filename
     * @param $lineno
     * @param $previous
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public function __construct($message,$code,$severity,$filename,$lineno,$previous){

        $error=array(
            'type'          =>'E_STRICT',
            'title'         =>array(
                'ru'    =>'Включаются для того, чтобы PHP предлагал изменения в коде, которые обеспечат лучшее взаимодействие и совместимость кода.',
                'en'    =>'Enable to have PHP suggest changes to your code which will ensure the best interoperability and forward compatibility of your code.	'
            ),
            'info'          =>array(
                'ru'    =>'Включаются для того, чтобы PHP предлагал изменения в коде, которые обеспечат лучшее взаимодействие и совместимость кода.',
                'en'    =>'Enable to have PHP suggest changes to your code which will ensure the best interoperability and forward compatibility of your code.	'
            ),
            'code'          =>$code,
            'line'          =>$lineno,
            'file'          =>$filename,
            'severity'      =>$severity,
            'message'       =>$message,
            'previous'      =>$previous
        );

        Log::init($error);

        exit;

    }

}