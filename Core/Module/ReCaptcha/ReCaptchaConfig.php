<?php

namespace Core\Module\ReCaptcha;

class ReCaptchaConfig{

    /** @var string */
    public  static $url             ='https://www.google.com/recaptcha/api/siteverify';

    /** @var string */
    public  static $key_public      ='6Lez3J4UAAAAAKzX2vcL3Gd_inzV55NQdtaT9d29';

    /** @var string */
    public  static $key_private     ='6Lez3J4UAAAAAEE9ZIqEb4wV7pEmSzS5vpzAk1ax';

}