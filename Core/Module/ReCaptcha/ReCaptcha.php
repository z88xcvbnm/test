<?php

namespace Core\Module\ReCaptcha;

use Core\Module\Curl\CurlPost;
use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;

class ReCaptcha{

    /**
     * @param string|NULL $token
     * @param string|NULL $action
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_valid_re_captcha_token(string $token=NULL,string $action=NULL){

        if(empty($token)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Token is empty'
            ];

            throw new ParametersException($error);

        }

        $data=[
            'secret'        =>ReCaptchaConfig::$key_private,
            'response'      =>$token
        ];

        $r=CurlPost::init(ReCaptchaConfig::$url,[],$data);

        if($r['status']!=200){

            $error=[
                'title'     =>AccessDeniedException::$title,
                'info'      =>'ReCaptcha is not valid'
            ];

            throw new AccessDeniedException($error);

        }

        $data=Json::decode($r['data']);

        if(!$data['success']){

            $error=[
                'title'     =>AccessDeniedException::$title,
                'info'      =>'ReCaptcha is not valid'
            ];

            throw new AccessDeniedException($error);

        }

        if($data['action']!=$action){

            $error=[
                'title'     =>AccessDeniedException::$title,
                'info'      =>'ReCaptcha action is not valid'
            ];

            throw new AccessDeniedException($error);

        }

        return true;

    }

}