<?php

namespace Core\Module\Php;

class PhpValidation{

    /**
     * @return bool
     */
    public  static function is_console_running(){

        return
              php_sapi_name()=='cli'
            ||php_sapi_name()=='phpdbg';

    }

}