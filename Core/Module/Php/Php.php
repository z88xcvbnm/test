<?php

namespace Core\Module\Php;

class Php{

    /**
     * Set default timezone
     */
    private static function set_timezone(){

        date_default_timezone_set(PhpConfig::$timezone);

    }

    /**
     * Set error report
     */
    private static function set_error_reporting(){

         ini_set('error_reporting',PhpConfig::$error_reporting);

    }

    /**
     * Set all error in log file
     */
    private static function set_error_log(){

        ini_set('log_errors',PhpConfig::$error_log);

        ini_set('error_log',DIR_ROOT.'/'.PhpConfig::$error_log_path.'/'.date("Ymd").'.log');

    }

    /**
     * Set display error
     */
    private static function set_display_error(){

        ini_set('display_errors',PhpConfig::$display_error);

    }

    /**
     * Set max time
     */
    private static function set_max_execution_time_limit(){

        ini_set('max_execution_time',PhpConfig::$max_executsion_time_limit);

    }

    /**
     * Set memory size
     */
    private static function set_memory_size(){

        ini_set('memory_limit',PhpConfig::$memory_limit);

    }

    /**
     * Set ignore user abourt
     */
    private static function set_ignore_user_abort(){

        ignore_user_abort(PhpConfig::$ignore_user_abort);

    }

    /**
     * set ob_implicit_flush
     */
    private static function set_ob_implicit_flush(){

        if(PhpConfig::$ob_implicit_flush)
            ob_implicit_flush();

    }

    /**
     * Set parameters
     */
    private static function set(){

        self::set_error_reporting();
        self::set_display_error();
        self::set_error_log();
        self::set_memory_size();
        self::set_max_execution_time_limit();
        self::set_ignore_user_abort();
        self::set_ob_implicit_flush();
        self::set_timezone();

    }

    /**
     * Initialization php settings
     */
    public static function init(){

        self::set();

    }

}