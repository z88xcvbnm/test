<?php

namespace Core\Module\Php;

class PhpConfig{

    /** @var int */
    public  static $memory_limit                    =-1;

    /** @var int */
    public  static $max_executsion_time_limit       =0;

    /** @var int */
    public  static $time_limit                      =-1;

    /** @var int */
    public  static $error_reporting                 =E_ALL;

    /** @var int */
    public  static $error_log                       =1;

    /** @var string */
    public  static $error_log_path                  ='Core/Log';

    /** @var int */
    public  static $display_error                   =1;

    /** @var string */
    public  static $timezone                        ='UTC';

    /** @var bool */
    public  static $ignore_user_abort               =true;

    /** @var bool */
    public  static $ob_implicit_flush               =true;

}