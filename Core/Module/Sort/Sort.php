<?php

namespace Core\Module\Sort;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\ParametersValidationException;

class Sort{

    /** @var string */
    private static $key;

    /** @var bool */
    private static $direction;

    /**
     * @param $a
     * @param $b
     * @return mixed
     * @throws ParametersException
     * @throws ParametersValidationException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function sort_by_order($a,$b){

        switch(self::$direction){

            case'asc':
                return $a[self::$key]-$b[self::$key];

            case'desc':
                return $b[self::$key]-$a[self::$key];

            default:{

                $error=[
                    'title'     =>ParametersValidationException::$title,
                    'info'      =>'Direction is not exists'
                ];

                throw new ParametersValidationException($error);

            }

        }

    }

    /**
     * @param string|NULL $key
     * @param array $list
     * @param string $direction
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function sort_with_key(string $key=NULL,array $list=[],string $direction='asc'){

        if(count($list)==0)
            return[];

        if(empty($key)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Key is empty'
            ];

            throw new ParametersException($error);

        }

        self::$key          =$key;
        self::$direction    =$direction;

        usort($list,['\Core\Module\Sort\Sort','sort_by_order']);

        return $list;

    }

}