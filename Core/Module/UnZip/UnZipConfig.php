<?php

namespace Core\Module\UnZip;

use Core\Module\Exception\PhpException;
use Core\Module\OsServer\OsServer;
use Core\Module\OsServer\OsServerCommandValidation;

class UnZipConfig{

    /** @var string */
    public  static $unzip_command_path;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$unzip_command_path=NULL;

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     */
    public  static function set_unzip_command_path(){

        if(OsServer::$is_linux)
            self::$unzip_command_path='unzip';
        else if(OsServer::$is_windows)
            self::$unzip_command_path='C:\unzip\unzip.exe';
        else if(OsServer::$is_freebsd)
            self::$unzip_command_path='unzip';
        else{

            $error=array(
                'title'     =>'Server problem',
                'info'      =>'UnZip command path is not valid'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return string
     * @throws PhpException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function get_unzip_command_path(){

        if(!empty(self::$unzip_command_path))
            return self::$unzip_command_path;

        self::set_unzip_command_path();

        if(!OsServerCommandValidation::isset_shell_command(self::$unzip_command_path)){

            $error=array(
                'title'     =>'Php problem',
                'info'      =>'Command "'.self::$unzip_command_path.'" is not exists'
            );

            throw new PhpException($error);

        }

        return self::$unzip_command_path;

    }

}