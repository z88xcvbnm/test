<?php

namespace Core\Module\UnZip;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Path\Path;
use Core\Module\SevenZa\SevenZaConfig;
use Core\Module\SevenZa\SevenZaLog;

class UnZipShell{

    /**
     * @param string|NULL $zip_path
     * @param string|NULL $unzip_path
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    public  static function unzip(string $zip_path=NULL,string $unzip_path=NULL){

        $error_info_list=[];

        if(empty($zip_path))
            $error_info_list['zip_path']='Zip path is empty';
        else if(!file_exists($zip_path))
            $error_info_list['zip_path']='Zip path is not exists';

        if(empty($unzip_path))
            $error_info_list['unzip_path']='Unzip path is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $unzip_command_path     =UnZipConfig::get_unzip_command_path();
        $unzip_log_path         =UnZipLog::get_unzip_log_path();

        if(empty($zip_command_path)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Zip command path is empty'
            );

            throw new PhpException($error);

        }

        $q=$unzip_command_path.' '.Path::$dir_root.'/'.$zip_path.' -d '.Path::$dir_root.'/'.$unzip_path.' > '.Path::$dir_root.'/'.$unzip_log_path;

        exec($q);

        return true;

    }

}