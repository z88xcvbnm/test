<?php

namespace Core\Module\Translate\Google;

class GoogleTranslate{

    /** @var string */
    private static $url_of_google_translate     ='https://translate.google.com/translate_a/single?client=t&sl=auto&tl=ru&hl=%lang%&dt=at&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&ie=UTF-8&oe=UTF-8&otf=1&ssel=0&tsel=0&kc=11&tk=24899.402896';

    /** @var array */
    private static $lang_available_list         =array(
        'en'        =>'English'
    );

    /**
     * @param string $lang
     * @return boolean
     */
    private static function isset_lang_in_available_list(string $lang){

        if(empty($lang))
            return false;

        return !empty(self::$lang_available_list[$lang]);

    }

    /**
     * @param string $content
     * @param string $lang
     * @return string|boolean
     */
    public  static function get(string $content,string $lang){

        if(empty($lang)||empty($content))
            return false;

        if(!self::isset_lang_in_available_list($lang))
            return false;

        $link=str_replace('%lang%',$lang,self::$url_of_google_translate).'&q='.urlencode($content);

        $content=file_get_contents($link);

        if(empty($content))
            return false;

        $temp=preg_replace('/\"|\[|\]/is','',$content);

        $r=mb_split(',',$temp);

        return $r[0];

    }

}