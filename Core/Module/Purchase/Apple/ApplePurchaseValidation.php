<?php

namespace Core\Module\Purchase\Apple;

use Core\Module\Encrypt\Hash;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\ResponseException;
use Core\Module\Json\Json;

class ApplePurchaseValidation{

    /**
     * @param string|NULL $signature
     * @param bool|true $is_sandbox
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\ParametersValidationException
     */
    public  static function get_apple_response_from_apple_server(string $signature=NULL,bool $is_sandbox=true){

        $error_info_list=[];

        if(empty($signature))
            $error_info_list[]='Signature is empty';

        if(is_null($is_sandbox))
            $error_info_list[]='Sandbox is NULL';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Signature is empty'
            );

            throw new ParametersException($error);

        }

        $url=ApplePurchaseConfig::get_url($is_sandbox);

        $data=json_encode(array(
            'receipt-data'      =>$signature
        ));

        $curl=curl_init();

        if(!$curl){

            $error=array(
                'title'     =>'PHP problem',
                'info'      =>'Curl was not started'
            );

            throw new PhpException($error);

        }

        curl_setopt($curl,CURLOPT_URL,$url);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_POST,true);
        curl_setopt($curl,CURLOPT_POSTFIELDS,$data);

        $r=Json::decode(curl_exec($curl));

        curl_close($curl);

        return $r;

    }

    public  static function get_prepared_response_from_apple_server(string $app_name=NULL,string $purchase_name=NULL,array $data=NULL,bool $is_sendbox=true,bool $is_resend=false){

        if(empty($r['status'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Response from Apple server have not status parameter'
            );

            throw new ResponseException($error);

        }

        if($r['status']==0&&!empty($r['receipt'])){

            if(count($r['receipt']['in_app'])>0)
                foreach($r['receipt']['in_app'] as $row)
                    if($row['product_id']==$purchase_name){

                        $data=array(
                            'app_name'          =>stripslashes($r['receipt']['bundle_id']),
                            'purchase_name'     =>stripslashes($row['product_id']),
                            'order_ID'          =>(int)$row['original_transaction_id'],
                            'timestamp'         =>(int)$row['purchase_date_ms']
                        );

                        $data['hash']=Hash::get_sha1_encode($data['app_name'].':'.$data['product_ID'].':'.$data['order_ID'].':'.$data['timestamp'].':'.time().':'.rand(0,time()));

                        return $data;

                    }

            return false;

        }
        else if($r['status']==21007&&!$is_resend){

            $sandbox    =1;
            $is_resend  =true;

            $data=self::get_apple_response_from_apple_server($data,$is_sendbox);

            return $data;

        }
        else if($r['status']==21008&&!$is_resend){

            $sandbox    =0;
            $is_resend  =true;

//            return self::get_prepared_response_from_apple_server()

        }

    }

}