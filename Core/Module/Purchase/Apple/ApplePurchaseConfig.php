<?php

namespace Core\Module\Purchase\Apple;

use Core\Module\Exception\ParametersException;

class ApplePurchaseConfig{

    /**
     * @var array
     */
    public  static $url_list=array(
        'production'    =>'https://buy.itunes.apple.com/verifyReceipt',
        'sandbox'       =>'https://sandbox.itunes.apple.com/verifyReceipt'
    );

    /**
     * @param bool|true $is_sandbox
     * @return string
     * @throws ParametersException
     */
    public  static function get_url(bool $is_sandbox=true){

        if(is_null($is_sandbox)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Sandbox is NULL'
            );

            throw new ParametersException($error);

        }

        return $is_sandbox
            ?self::$url_list['sandbox']
            :self::$url_list['production'];

    }

}