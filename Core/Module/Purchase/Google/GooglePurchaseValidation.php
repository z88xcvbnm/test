<?php

namespace Core\Module\Purchase\Google;

use Core\Module\Exception\ParametersException;

class GooglePurchaseValidation{

    /**
     * @param string|NULL $public_key
     * @param string|NULL $signature
     * @param string|NULL $purchase
     * @return bool
     * @throws ParametersException
     */
    public  static function is_valid_google_purchase(string $public_key=NULL,string $signature=NULL,string $purchase=NULL){

        $error_info_list=[];

        if(empty($public_key))
            $error_info_list[]='Public key is empty';

        if(empty($signature))
            $error_info_list[]='Signature is empty';

        if(empty($purchase))
            $error_info_list[]='Purchase is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $key        ="-----BEGIN PUBLIC KEY-----\n".chunk_split($public_key,64,"\n").'-----END PUBLIC KEY-----';
        $key        =openssl_get_publickey($key);
        $binary     =base64_decode($signature);
        $r          =openssl_verify($purchase,$binary,$key,OPENSSL_ALGO_SHA1);

        return($r===1);

    }

}