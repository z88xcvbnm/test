<?php

namespace Core\Module\Os;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class Os{

    /** @var int */
    public  static $os_ID;

    /** @var string */
    public  static $os_name;

    /** @var string */
    public  static $os_type;

    /** @var bool */
    public  static $is_google=false;

    /** @var bool */
    public  static $is_apple=false;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$os_ID        =NULL;
        self::$os_name      =NULL;

        return true;

    }

    /**
     * @param string|NULL $os_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_os_name(string $os_name=NULL){

        if(empty($os_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'OS name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name_lower'=>strtolower($os_name)
        );

        return Db::isset_row('_os',0,$where_list);

    }

    /**
     * @param int|NULL $os_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_os_ID(int $os_ID=NULL){

        if(empty($os_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'OS ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($os_ID,'_os',0);

    }

    /**
     * @param string|NULL $os_name
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_os_ID(string $os_name=NULL){

        if(empty($os_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'OS name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name_lower'=>strtolower($os_name)
        );

        return Db::get_row_ID('_os',0,$where_list);

    }

    /**
     * @param int|NULL $os_ID
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_os_name(int $os_ID=NULL){

        if(empty($os_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'OS ID is empty'
            );

            throw new ParametersException($error);

        }

        $column_list=array(
            'name'
        );

        $data=Db::get_data_from_ID($os_ID,'_os',$column_list,0);

        if(empty($data['name']))
            return NULL;

        return $data['name'];

    }

    /**
     * @return string|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_os_name_default(){

        if(empty(self::$os_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Os ID is empty'
            );

            throw new ParametersException($error);

        }

        self::$os_name=self::get_os_name(self::$os_ID);

        return self::$os_name;

    }

    /**
     * @param string|NULL $os_name
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_os(string $os_name=NULL){

        if(empty($os_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'OS name is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_os',
            'values'    =>array(
                'name'              =>$os_name,
                'name_lower'        =>strtolower($os_name),
                'date_create'       =>'NOW()',
                'date_update'       =>'NOW()'
            )
        );

        $conflict_list=[
            'column_list'=>[
                'name_lower'
            ],
            'return_list'=>[
                'id'
            ]
        ];

        $r=Db::insert($q,true,[],NULL,$conflict_list);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'OS was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param string|NULL $os_name
     * @param bool $need_remove_os_version
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_os(string $os_name=NULL,bool $need_remove_os_version=true){

        if(empty($os_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'OS name is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_os_version){

            $os_ID=self::get_os_ID($os_name);

            if(!empty($os_ID))
                if(!OsVersion::remove_os_version($os_ID)){

                    $error=array(
                        'title'     =>'DB query problem',
                        'info'      =>'OS was not removed'
                    );

                    throw new DbQueryException($error);

                }

        }

        $where_list=array(
            'name_lower'=>strtolower($os_name)
        );

        return Db::delete_from_where_list('_os',0,$where_list);

    }

    /**
     * @param int|NULL $os_ID
     * @param bool $need_remove_os_version
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_os_ID(int $os_ID=NULL,bool $need_remove_os_version=true){

        if(empty($os_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'OS ID is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_os_version)
            if(!OsVersion::remove_os_version($os_ID)){

                $error=array(
                    'title'     =>'DB query problem',
                    'info'      =>'OS was not removed'
                );

                throw new DbQueryException($error);

            }

        return Db::delete_from_ID($os_ID,'_os',0);

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_device_type_default(){

        if(empty(self::$os_name))
            self::get_os_name_default();

        switch(strtolower(self::$os_name)){

            case'android_test':
            case'android':{

                self::$is_google    =true;
                self::$is_apple     =false;
                self::$os_type      ='google';

                return true;

            }

            case'ios_test':
            case'ios':{

                self::$is_google    =false;
                self::$is_apple     =true;
                self::$os_type      ='apple';

                return true;

            }

            default:{

                self::$is_google    =NULL;
                self::$is_apple     =NULL;
                self::$os_type      =NULL;

                return false;

            }

        }

    }

    /**
     * @param int|NULL $os_ID
     */
    public  static function set_os_ID_default(int $os_ID=NULL){

        self::$os_ID=empty($os_ID)?NULL:$os_ID;

    }

    /**
     * @param string|NULL $os_name
     */
    public  static function set_os_name_default(string $os_name=NULL){

        self::$os_name=empty($os_name)?NULL:$os_name;

    }

}