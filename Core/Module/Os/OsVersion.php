<?php

namespace Core\Module\Os;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class OsVersion{

    /** @var int */
    public  static $os_version_ID;

    /** @var string */
    public  static $os_version;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$os_version_ID    =NULL;
        self::$os_version       =NULL;

        return true;

    }

    /**
     * @param int|NULL $os_ID
     * @param string|NULL $os_version
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_os_version(int $os_ID=NULL,string $os_version=NULL){

        $error_info_list=[];

        if(empty($os_ID))
            $error_info_list[]='OS ID is empty';

        if(empty($os_version))
            $error_info_list[]='OS version is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'os_ID'         =>$os_ID,
                    'os_version'    =>$os_version
                )
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'os_id'     =>$os_ID,
            'version'   =>strtolower($os_version)
        );

        return Db::isset_row('_os_version',0,$where_list);

    }

    /**
     * @param int|NULL $os_version_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_os_version_ID(int $os_version_ID=NULL){

        if(empty($os_version_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'OS version ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($os_version_ID,'_os_version',0);

    }

    /**
     * @param int|NULL $os_ID
     * @param string|NULL $os_version
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_os_version_ID(int $os_ID=NULL,string $os_version=NULL){

        $error_info_list=[];

        if(empty($os_ID))
            $error_info_list[]='OS ID is empty';

        if(empty($os_version))
            $error_info_list[]='OS version is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'os_ID'         =>$os_ID,
                    'os_version'    =>$os_version
                )
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'os_id'     =>$os_ID,
            'version'   =>strtolower($os_version)
        );

        return Db::get_row_ID('_os_version',0,$where_list);

    }

    /**
     * @param int|NULL $os_ID
     * @param string|NULL $os_version
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_os_version(int $os_ID=NULL,string $os_version=NULL){

        $error_info_list=[];

        if(empty($os_ID))
            $error_info_list[]='OS ID is empty';

        if(empty($os_version))
            $error_info_list[]='OS version is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'os_ID'         =>$os_ID,
                    'os_version'    =>$os_version
                )
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_os_version',
            'values'    =>array(
                'os_id'         =>$os_ID,
                'version'       =>strtolower($os_version),
                'date_create'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'OS version was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $os_ID
     * @param string $os_version
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_os_version(int $os_ID=NULL,string $os_version){

        $error_info_list=[];

        if(empty($os_ID))
            $error_info_list[]='OS ID is empty';

        if(empty($os_version))
            $error_info_list[]='OS version is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'os_ID'         =>$os_ID,
                    'os_version'    =>$os_version
                )
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'os_id'     =>$os_ID,
            'version'   =>strtolower($os_version)
        );

        return Db::delete_from_where_list('_os_version',0,$where_list);

    }

    /**
     * @param int|NULL $os_version_ID
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_os_version_ID(int $os_version_ID=NULL){

        if(empty($os_version_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'OS version ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::delete_from_ID($os_version_ID,'_os_version',0);

    }

    /**
     * @param int|NULL $os_version_ID
     */
    public  static function set_os_version_ID_default(int $os_version_ID=NULL){

        self::$os_version_ID=empty($os_version_ID)?NULL:$os_version_ID;

    }

    /**
     * @param string|NULL $os_version
     */
    public  static function set_os_version_default(string $os_version=NULL){

        self::$os_version=empty($os_version)?NULL:strtolower($os_version);

    }

}