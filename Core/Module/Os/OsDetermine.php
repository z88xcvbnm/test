<?php

namespace Core\Module\Os;

use Core\Module\Device\DeviceFirm;
use Core\Module\Device\DeviceType;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Useragent\Useragent;
use Core\Module\Worktime\Worktime;

class OsDetermine{

    /**
     * @param string|NULL $useragent
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_determine_data(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'windows')!==false)
            return array(
                'os_name'           =>'Windows',
                'device_type'       =>'PC'
            );
        else if(stripos($useragent,'iPad')!==false)
            return array(
                'os_name'           =>'iOS',
                'is_tablet'         =>true,
                'is_mobile'         =>true,
                'device_firm_name'  =>'Apple',
                'device_type'       =>'iPad'
            );
        else if(stripos($useragent,'iPod')!==false)
            return array(
                'os_name'           =>'iOS',
                'is_mobile'         =>true,
                'device_firm_name'  =>'Apple',
                'device_type'       =>'iPod'
            );
        else if(stripos($useragent,'iPhone')!==false)
            return array(
                'os_name'           =>'iOS',
                'is_mobile'         =>true,
                'device_firm_name'  =>'Apple',
                'device_type'       =>'iPhone'
            );
        else if(stripos($useragent,'mac')!==false)
            return array(
                'os_name'           =>'iOS',
                'device_firm_name'  =>'Apple',
                'device_type'       =>'PC'
            );
        else if(stripos($useragent,'android')!==false)
            return array(
                'os_name'           =>'Android',
                'is_mobile'         =>true
            );
        else if(stripos($useragent,'linux')!==false)
            return array(
                'os_name'           =>'Linux'
            );
        else if(stripos($useragent,'Nokia')!==false)
            return array(
                'os_name'           =>'Symbian',
                'is_mobile'         =>true,
                'device_firm_name'  =>'Nokia'
            );
        else if(stripos($useragent,'BlackBerry')!==false)
            return array(
                'os_name'           =>'BlackBerry',
                'is_mobile'         =>true,
                'device_firm_name'  =>'BlackBerry'
            );
        else if(stripos($useragent,'FreeBSD')!==false)
            return array(
                'os_name'=>'FreeBSD'
            );
        else if(stripos($useragent,'OpenBSD')!==false)
            return array(
                'os_name'=>'OpenBSD'
            );
        else if(stripos($useragent,'NetBSD')!==false)
            return array(
                'os_name'=>'NetBSD'
            );
        else if(stripos($useragent,'OpenSolaris')!==false)
            return array(
                'os_name'=>'OpenSolaris'
            );
        else if(stripos($useragent,'SunOS')!==false)
            return array(
                'os_name'=>'SunOS'
            );
        else if(stripos($useragent,'OS\/2')!==false)
            return array(
                'os_name'           =>'OS/2',
                'device_firm_name'  =>'IBM'
            );
        else if(stripos($useragent,'BeOS')!==false)
            return array(
                'os_name'=>'BeOS'
            );
        else if(stripos($useragent,'win')!==false)
            return array(
                'os_name'           =>'Windows',
                'device_type'       =>'PC'
            );
        else if(stripos($useragent,'Playstation')!==false)
            return array(
                'os_name'           =>'Playstation',
                'device_firm_name'  =>'Sony'
            );

        return[];

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_os_ID_to_default(){

        if(empty(Os::$os_name))
            return false;

        $os_ID=Os::get_os_ID(Os::$os_name);

        if(empty($os_ID))
            $os_ID=Os::add_os(Os::$os_name);

        if(empty($os_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'OS name was not added'
            );

            throw new DbQueryException($error);

        }

        Os::set_os_ID_default($os_ID);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_os_version_ID_to_default(){

        if(empty(OsVersion::$os_version))
            return false;

        $os_version_ID=OsVersion::get_os_version_ID(Os::$os_ID,OsVersion::$os_version);

        if(empty($os_version_ID))
            $os_version_ID=OsVersion::add_os_version(Os::$os_ID,OsVersion::$os_version);

        if(empty($os_version_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'OS version was not added'
            );

            throw new DbQueryException($error);

        }

        OsVersion::set_os_version_ID_default($os_version_ID);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function determine_default(){

        if(empty(Useragent::$useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Client useragent is empty'
            );

            throw new ParametersException($error);

        }

        $data=self::get_determine_data(Useragent::$useragent);

        if(count($data)==0)
            return false;

        if(isset($data['os_name']))
            Os::set_os_name_default($data['os_name']);

        if(isset($data['is_mobile']))
            DeviceType::$is_mobile=$data['is_mobile'];

        if(isset($data['is_tablet']))
            DeviceType::$is_tablet=$data['is_tablet'];

        if(isset($data['device_firm_name']))
            DeviceFirm::set_device_firm_name_default($data['device_firm_name']);

        if(isset($data['device_type']))
            DeviceType::set_device_type($data['device_type']);

        return true;

    }

    /**
     * @return bool
     */
    public  static function determine_from_post(){

        if(!empty($_POST['os_name']))
            Os::set_os_name_default($_POST['os_name']);

        if(!empty($_POST['os_version']))
            OsVersion::set_os_version_default($_POST['os_version']);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::determine_default();
        self::determine_from_post();

        self::set_client_os_ID_to_default();
        self::set_client_os_version_ID_to_default();

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Worktime::set_timestamp_point('OS Determine Start');

        self::set();

        Worktime::set_timestamp_point('OS Determine Finish');

        return true;

    }

}