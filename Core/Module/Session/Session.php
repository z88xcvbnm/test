<?php

namespace Core\Module\Session;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Token\Token;
use Core\Module\User\User;

class Session{

    /** @var int */
    public  static $session_ID;

    /** @var string */
    public  static $session_date_live;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$session_ID           =NULL;
        self::$session_date_live    =NULL;

        return true;

    }

    /**
     * @param int|NULL $session_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_session_ID(int $session_ID=NULL){

        if(empty($session_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Session ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id'
                )
            ),
            'table'=>'_session',
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$session_ID
                ),
                array(
                    'column'=>'date_live',
                    'method'=>'>',
                    'value'=>Date::get_date_time_full()
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return !empty($r);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $token_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_session(int $user_ID=NULL,int $token_ID=NULL){

        if(empty($token_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Token ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            array(
                'column'    =>'token_id',
                'value'     =>$token_ID
            ),
            array(
                'column'    =>'date_live',
                'method'=>'>',
                'value'=>Date::get_date_time_full()
            )
        );

        if(!empty($user_ID))
            $where_list[]=array(
                'column'    =>'user_id',
                'value'     =>$user_ID
            );

        return Db::isset_row('_session',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $token_ID
     * @return int|null
     * @throws ParametersException
     */
    public  static function get_session_ID(int $user_ID=NULL,int $token_ID=NULL){

        if(empty($token_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Token ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            array(
                'column'    =>'token_id',
                'value'     =>$token_ID
            ),
            array(
                'column'    =>'date_live',
                'method'    =>'>',
                'value'     =>Date::get_date_time_full()
            ),
            array(
                'column'    =>'type',
                'value'     =>0
            )
        );

        if(!empty($user_ID))
            $where_list[]=array(
                'column'    =>'user_id',
                'value'     =>$user_ID
            );

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id'
                )
            ),
            'table'=>'_session',
            'where'=>$where_list,
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @return string
     */
    public  static function get_session_date_live(){

        return Date::get_date_time_full(Date::get_timestamp()+SessionConfig::$session_date_delta);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $token_ID
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function add_session(int $user_ID=NULL,int $token_ID=NULL){

        $error_info_list=[];

        if(empty($token_ID))
            $error_info_list[]='Token ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'user_ID'   =>$user_ID,
                    'token_ID'  =>$token_ID
                )
            );

            throw new ParametersException($error);

        }

        $session_date_live      =self::get_session_date_live();
        $value_list     =array(
            'token_id'      =>$token_ID,
            'date_create'   =>'NOW()',
            'date_live'     =>$session_date_live
        );

        if(!empty(User::$user_ID))
            $value_list['user_id']=User::$user_ID;

        $q=array(
            'table'=>'_session',
            'values'=>array($value_list)
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'New session was not added',
                'data'  =>array(
                    'user_ID'   =>$user_ID,
                    'token_ID'  =>$token_ID,
                    'query'     =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_session_default(){

        self::$session_date_live    =self::get_session_date_live();
        self::$session_ID           =self::add_session(User::$user_ID,Token::$token_ID);

        if(empty(self::$session_ID)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Session was not removed',
                'data'  =>array(
                    'token_ID'      =>Token::$token_ID,
                    'user_ID'       =>User::$user_ID,
                    'date_live'     =>self::$session_date_live
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $token_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_session(int $user_ID=NULL,int $token_ID=NULL){

        if(empty($token_ID)&&empty($user_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Token ID and user ID are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($token_ID))
            $where_list['token_id']=$token_ID;

        if(!Db::delete_from_where_list('_session',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Session was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $session_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_session_ID(int $session_ID=NULL){

        if(empty($session_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Session ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($session_ID,'_session',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Session was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_session_ID_default(){

        if(empty(self::$session_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default session ID is empty'
            );

            throw new ParametersException($error);

        }

        return self::remove_session_ID(self::$session_ID);

    }

    /**
     * @param int|NULL $session_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function update_session_date_live(int $session_ID=NULL,string $session_date_live=NULL){

        if(empty($session_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Session ID is empty'
            );

            throw new ParametersException($error);

        }

        if(empty($session_date_live))
            $session_date_live=self::get_session_date_live();

        $q=array(
            'table'=>'_session',
            'set'=>array(
                array(
                    'column'=>'date_update',
                    'value'=>'NOW()'
                ),
                array(
                    'column'=>'date_live',
                    'value'=>$session_date_live
                )
            ),
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$session_ID
                )
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Session was not updated date live',
                'data'  =>array(
                    'session_ID'    =>$session_ID,
                    'query'         =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function update_session_date_live_default(){

        if(empty(self::$session_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Default session ID is empty'
            );

            throw new ParametersException($error);

        }

        self::$session_date_live=self::get_session_date_live();

        if(!self::update_session_date_live(self::$session_ID,self::$session_date_live)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Default session date live was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $session_ID
     */
    public  static function set_session_ID_default(int $session_ID=NULL){

        self::$session_ID=empty($session_ID)?NULL:$session_ID;

    }

    /**
     * @param int|NULL $session_date_live
     */
    public  static function set_session_date_live(int $session_date_live=NULL){

        self::$session_date_live=empty($session_date_live)?NULL:$session_date_live;

    }

}