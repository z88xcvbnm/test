<?php

namespace Core\Module\Session;

class SessionConfig{

    /** @var int */
    public  static $session_date_delta=300;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$session_date_delta=300;

        return true;

    }

    /**
     * @param int $seconds
     * @return bool
     */
    public  static function set_session_date_delta(int $seconds=300){

        self::$session_date_delta=$seconds;

        return true;

    }

}