<?php

namespace Core\Module\Apple;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Json\Json;

class AppStore{

    /**
     * @param bool|NULL $is_sandbox
     * @return string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_url(bool $is_sandbox=NULL){

        if(is_null($is_sandbox)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Sandbox is NULL'
            );

            throw new ParametersException($error);

        }

        return $is_sandbox?AppStoreConfig::$url_sandbox:AppStoreConfig::$url_public;

    }

    /**
     * @param string|NULL $name
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_app_name(string $name=NULL){

        if(empty($name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App name is empty'
            );

            throw new ParametersException($error);

        }

        return array_search($name,AppStoreConfig::$app_name_list)!==false;

    }

    /**
     * @param string|NULL $purchase
     * @param bool|NULL $is_sandbox
     * @param bool $is_resend
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_response(string $purchase=NULL,bool $is_sandbox=NULL,bool $is_resend=false){

        $error_info_list=[];

        if(empty($purchase))
            $error_info_list[]='Purchase is empty';

        if(is_null($is_sandbox))
            $error_info_list[]='Sandbox is NULL';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $url=self::get_url($is_sandbox);

        $data=json_encode(array(
            'receipt-data'=>$purchase
        ));

        $curl=curl_init();

        if(!$curl){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Curl was not started'
            );

            throw new PhpException($error);

        }

        curl_setopt($curl,CURLOPT_URL,$url);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_POST,true);

        curl_setopt($curl,CURLOPT_POSTFIELDS,$data);

        $r=curl_exec($curl);

        $r=Json::decode($r);

        curl_close($curl);

        if($r['status']==0&&!empty($r['receipt'])){

            $list=[];

            if(count($r['receipt']['in_app'])>0)
                foreach($r['receipt']['in_app'] as $row)
                    $list[$row['product_id']]=[
                        'app_name'      =>stripslashes($r['receipt']['bundle_id']),
                        'product_ID'    =>stripslashes($row['product_id']),
                        'order_ID'      =>(int)$row['original_transaction_id'],
                        'timestamp'     =>(int)$row['purchase_date_ms']
                    ];

            return $list;

        }
        else if($r['status']==21007&&!$is_resend)
            return self::get_response($purchase,true,true);
        else if($r['status']==21008&&!$is_resend)
            return self::get_response($purchase,false,true);
        else{

            switch($r['status']){

                case 21000:{

                    $error_info='The App Store could not read the JSON object you provided';

                    break;

                }

                case 21002:{

                    $error_info='The data in the receipt-data property was malformed or missing';

                    break;

                }

                case 21003:{

                    $error_info='The receipt could not be authenticated';

                    break;

                }

                case 21004:{

                    $error_info='The shared secret you provided does not match the shared secret on file for your account';

                    break;

                }

                case 21005:{

                    $error_info='The receipt server is not currently available';

                    break;

                }

                case 21006:{

                    $error_info='This receipt is valid but the subscription has expired. When this status code is returned to your server, the receipt data is also decoded and returned as part of the response. Only returned for iOS 6 style transaction receipts for auto-renewable subscriptions';

                    break;

                }

                case 21010:{

                    $error_info='This receipt could not be authorized. Treat this the same as if a purchase was never made';

                    break;

                }

                default:{

                    $error_info='Internal data access error';

                    break;

                }

            }

            $error=array(
                'title'     =>'System problem',
                'info'      =>$error_info
            );

            throw new PhpException($error);

        }

    }

}