<?php

namespace Core\Module\Apple;

class AppStoreConfig{

    /** @var string */
    public  static $url_public='https://buy.itunes.apple.com/verifyReceipt';

    /** @var string */
    public  static $url_sandbox='https://sandbox.itunes.apple.com/verifyReceipt';

    /** @var array */
    public static $app_name_list=array();

}