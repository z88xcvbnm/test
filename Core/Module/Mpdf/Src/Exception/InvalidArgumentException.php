<?php

namespace Core\Module\Mpdf\Src\Exception;

use Core\Module\Mpdf\Src\MpdfException;

class InvalidArgumentException extends MpdfException
{

}
