<?php

namespace Core\Module\Mpdf\Src\Fonts;

use Core\Module\Mpdf\Src\MpdfException;

class FontFileFinder
{

	private $directories;

	public function __construct($directories)
	{
		$this->setDirectories($directories);
	}

	public function setDirectories($directories)
	{
		if (!is_array($directories)) {
			$directories = [$directories];
		}

		$this->directories = $directories;
	}

	public function findFontFile($name)
	{
		foreach ($this->directories as $directory) {
			$filename = $directory . '/' . $name;

//			echo $directory."\n";
//			echo $name."\n";
//			echo $filename."\n\n";

			if (file_exists($filename)) {
				return $filename;
			}
		}

		throw new MpdfException(sprintf('Cannot find TTF TrueType font file "%s" in configured font directories.', $name));
	}
}
