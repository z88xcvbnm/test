<?php

namespace Core\Module\Mpdf\Src\Output;

class Destination
{

	const FILE = 'F';

	const DOWNLOAD = 'D';

	const STRING_RETURN = 'S';

	const INLINE = 'I';
}
