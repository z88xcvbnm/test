<?php

namespace Core\Module\Mpdf\Src\Tag;

use Core\Module\Mpdf\Src\Strict;

use Core\Module\Mpdf\Src\Cache;
use Core\Module\Mpdf\Src\Color\ColorConverter;
use Core\Module\Mpdf\Src\CssManager;
use Core\Module\Mpdf\Src\Form;
use Core\Module\Mpdf\Src\Image\ImageProcessor;
use Core\Module\Mpdf\Src\Language\LanguageToFontInterface;
use Core\Module\Mpdf\Src\Mpdf;
use Core\Module\Mpdf\Src\Otl;
use Core\Module\Mpdf\Src\SizeConverter;
use Core\Module\Mpdf\Src\TableOfContents;

abstract class Tag
{

	use Strict;

	/**
	 * @var \Core\Module\Mpdf\Src
	 */
	protected $mpdf;

	/**
	 * @var \Core\Module\Mpdf\Src\Cache
	 */
	protected $cache;

	/**
	 * @var \Core\Module\Mpdf\Src\CssManager
	 */
	protected $cssManager;

	/**
	 * @var \Core\Module\Mpdf\Src\Form
	 */
	protected $form;

	/**
	 * @var \Core\Module\Mpdf\Src\Otl
	 */
	protected $otl;

	/**
	 * @var \Core\Module\Mpdf\Src\TableOfContents
	 */
	protected $tableOfContents;

	/**
	 * @var \Core\Module\Mpdf\Src\SizeConverter
	 */
	protected $sizeConverter;

	/**
	 * @var \Core\Module\Mpdf\Src\Color\ColorConverter
	 */
	protected $colorConverter;

	/**
	 * @var \Core\Module\Mpdf\Src\Image\ImageProcessor
	 */
	protected $imageProcessor;

	/**
	 * @var \Core\Module\Mpdf\Src\Language\LanguageToFontInterface
	 */
	protected $languageToFont;

	const ALIGN = [
		'left' => 'L',
		'center' => 'C',
		'right' => 'R',
		'top' => 'T',
		'text-top' => 'TT',
		'middle' => 'M',
		'baseline' => 'BS',
		'bottom' => 'B',
		'text-bottom' => 'TB',
		'justify' => 'J'
	];

	public function __construct(
		Mpdf $mpdf,
		Cache $cache,
		CssManager $cssManager,
		Form $form,
		Otl $otl,
		TableOfContents $tableOfContents,
		SizeConverter $sizeConverter,
		ColorConverter $colorConverter,
		ImageProcessor $imageProcessor,
		LanguageToFontInterface $languageToFont
	) {

		$this->mpdf = $mpdf;
		$this->cache = $cache;
		$this->cssManager = $cssManager;
		$this->form = $form;
		$this->otl = $otl;
		$this->tableOfContents = $tableOfContents;
		$this->sizeConverter = $sizeConverter;
		$this->colorConverter = $colorConverter;
		$this->imageProcessor = $imageProcessor;
		$this->languageToFont = $languageToFont;
	}

	public function getTagName()
	{
		$tag = get_class($this);
		return strtoupper(str_replace('Core\Module\Mpdf\Src\Tag\\', '', $tag));
	}

	abstract public function open($attr, &$ahtml, &$ihtml);

	abstract public function close(&$ahtml, &$ihtml);

}
