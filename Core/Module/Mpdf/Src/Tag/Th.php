<?php

namespace Core\Module\Mpdf\Src\Tag;

class Th extends Td
{

	public function close(&$ahtml, &$ihtml)
	{
		$this->mpdf->SetStyle('B', false);
		parent::close($ahtml, $ihtml);
	}
}
