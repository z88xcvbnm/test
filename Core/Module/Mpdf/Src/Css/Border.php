<?php

namespace Core\Module\Mpdf\Src\Css;

class Border
{

	const ALL = 15;
	const TOP = 8;
	const RIGHT = 4;
	const BOTTOM = 2;
	const LEFT = 1;
}
