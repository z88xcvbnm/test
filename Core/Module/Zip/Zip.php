<?php

namespace Core\Module\Zip;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;

class Zip{

    /**
     * @param string|NULL $zip_path
     * @return \ZipArchive
     * @throws ParametersException
     * @throws PhpException
     */
    public  static function open_zip(string $zip_path=NULL){

        if(empty($zip_path)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        $zip_object         =new \ZipArchive();
        $isset_file_zip     =file_exists($zip_path);

        $zip_object->open($zip_path,\ZipArchive::CREATE);

        if(!$isset_file_zip)
            if(!$zip_object->addEmptyDir('/')){

                $error=array(
                    'title'     =>'System problem',
                    'info'      =>'File was not added empty dir to ZIP'
                );

                throw new PhpException($error);

            }

        return $zip_object;

    }

    /**
     * @param null $zip_object
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    public  static function close_zip($zip_object=NULL){

        if(empty($zip_object)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Zip object is empty'
            );

            throw new ParametersException($error);

        }

        if(!$zip_object->close()){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'ZIP file was not closed'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $zip_path
     * @param string|NULL $file_source_path
     * @param string|NULL $file_result_path
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    public  static function add_file(string $zip_path=NULL,string $file_source_path=NULL,string $file_result_path=NULL){

        $error_info_list=[];

        if(empty($zip_path))
            $error_info_list['zip_path']='Zip path is empty';

        if(empty($file_source_path))
            $error_info_list['file_source_path']='File source path is empty';

        if(empty($file_result_path))
            $error_info_list['file_result_path']='File result path is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $zip_object=self::open_zip($zip_path);

        if(!$zip_object->addFile($file_source_path,$file_result_path)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'File was not added to ZIP'
            );

            throw new PhpException($error);

        }

        return self::close_zip($zip_object);

    }

    /**
     * @param string|NULL $zip_path
     * @param string|NULL $file_source_path
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    public  static function add_empty_dir_from_path(string $zip_path=NULL,string $file_source_path=NULL){

        $error_info_list=[];

        if(empty($zip_path))
            $error_info_list['zip_path']='Zip path is empty';

        if(empty($file_source_path))
            $error_info_list['file_source_path']='File source path is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $path_info      =pathinfo($file_source_path);
        $zip_object     =self::open_zip($zip_path);

        if(!$zip_object->addEmptyDir($path_info['basename'])){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'File was not added empty dir to ZIP'
            );

            throw new PhpException($error);

        }

        return self::close_zip($zip_object);

    }

    /**
     * @param string|NULL $zip_path
     * @param string|NULL $file_result_path
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    public  static function add_empty_dir(string $zip_path=NULL,string $file_result_path=NULL){

        $error_info_list=[];

        if(empty($zip_path))
            $error_info_list['zip_path']='Zip path is empty';

        if(empty($file_result_path))
            $error_info_list['file_result_path']='File result path is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $zip_object=self::open_zip($zip_path);

        if(!$zip_object->addEmptyDir($file_result_path)){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'File was not added empty dir to ZIP',
                'data'      =>array(
                    'dir'=>$file_result_path
                )
            );

            throw new PhpException($error);

        }

        return self::close_zip($zip_object);

    }

    /**
     * @param string|NULL $zip_path
     * @param string|NULL $file_source_path
     * @param int|NULL $path_len
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    public  static function add_folder(string $zip_path=NULL,string $dir_source_path=NULL,string $dir_result='',int $path_len=NULL){

        $error_info_list=[];

        if(empty($zip_path))
            $error_info_list['zip_path']='Zip path is empty';

        if(empty($dir_source_path))
            $error_info_list['dir_source_path']='Dir source path is empty';

        if(empty($path_len))
            $error_info_list['path_len']='Path len is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        if(!self::add_empty_dir($zip_path,(empty($dir_result)?'':($dir_result)).mb_substr($dir_source_path,$path_len))){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Empty dir was not added to ZIP'
            );

            throw new PhpException($error);

        }

        foreach(scandir($dir_source_path) as $file_name)
            if($file_name!='.'&&$file_name!='..'){

                $file_source_path       =$dir_source_path.'/'.$file_name;
                $file_result_path       =(empty($dir_result)?'':($dir_result)).mb_substr($file_source_path,$path_len);

                if(is_file($file_source_path)){

                    if(!self::add_file($zip_path,$file_source_path,$file_result_path)){

                        $error=array(
                            'title'     =>'System problem',
                            'info'      =>'File was not added to ZIP'
                        );

                        throw new PhpException($error);

                    }

                }
                else if(is_dir($file_source_path)){

                    if(!self::add_folder($zip_path,$file_source_path,$dir_result,$path_len)){

                        $error=array(
                            'title'     =>'System problem',
                            'info'      =>'DIR was not added to ZIP'
                        );

                        throw new PhpException($error);

                    }

                }

            }

        return true;

    }

}