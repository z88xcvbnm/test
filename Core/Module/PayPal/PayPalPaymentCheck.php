<?php

namespace Core\Module\PayPal;

use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;
use Core\Module\Response\ResponseSuccess;
use function Sodium\add;

class PayPalPaymentCheck{

    /** @var int */
    private static $order_ID;

    /** @var int */
    private static $paypal_payment_ID;

    /** @var float */
    private static $amount;

    /** @var bool */
    private static $is_sandbox=false;

    /** @var string */
    private static $scope;

    /** @var string */
    private static $access_token;

    /** @var string */
    private static $token_type;

    /** @var string */
    private static $app_ID;

    /** @var int */
    private static $expires_in;

    /** @var string */
    private static $nonce;

    /** @var string */
    private static $user_wallet_address;

    /** @var string */
    private static $target_wallet_address;

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_order_ID(){

        if(PayPalPayment::isset_order_ID(self::$order_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Order ID already exists'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function paypal_auth(){

        $ch         =curl_init();
        $url        =self::$is_sandbox?PayPalConfig::$url_oauth:PayPalLiveConfig::$url_oauth;
        $clientId   =self::$is_sandbox?PayPalConfig::$client_ID:PayPalLiveConfig::$client_ID;
        $secret     =self::$is_sandbox?PayPalConfig::$secret:PayPalLiveConfig::$secret;

        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLVERSION , 6); //NEW ADDITION
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

        $result=curl_exec($ch);

        curl_close($ch);

        if(empty($result)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Auth status not 200'
            ];

            throw new ParametersException($error);

        }

        $r=Json::decode($result);

        if(!isset($r['scope'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Error scope'
            ];

            throw new ParametersException($error);

        }

        self::$scope                =$r['scope'];
        self::$access_token         =$r['access_token'];
        self::$token_type           =$r['token_type'];
        self::$app_ID               =$r['app_id'];
        self::$expires_in           =$r['expires_in'];
        self::$nonce                =$r['nonce'];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_order(){

        $url        =self::$is_sandbox?(PayPalConfig::$url_order_verify.self::$order_ID):(PayPalLiveConfig::$url_order_verify.self::$order_ID);
        $ch         =curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json",
          "Authorization: Bearer ".self::$access_token,
        ));

        $result=curl_exec($ch);

        curl_close($ch);

        if(empty($result)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Verify status not 200'
            ];

            throw new ParametersException($error);

        }

        $r=Json::decode(trim($result));

        if(!isset($r['purchase_units'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Problem with check payment'
            ];

            throw new ParametersException($error);

        }

        if($r['status']!='COMPLETED'){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Transaction was not finish'
            ];

            throw new ParametersException($error);

        }

        $order_ID           =$r['id'];
        $status             =empty($r['status'])?NULL:$r['status'];
        $intent             =empty($r['intent'])?NULL:$r['intent'];
        $reference_ID       =NULL;
        $currency           =NULL;
        $amount             =NULL;
        $create_time        =NULL;
        $response           =Json::encode($r);
        $payer_ID           =NULL;
        $name               =NULL;
        $surname            =NULL;
        $full_name          =NULL;
        $address            =NULL;
        $country_code       =NULL;
        $zip_code           =NULL;
        $email              =NULL;
        $phone              =NULL;
        $wallet_email       =NULL;


        if(isset($r['purchase_units']))
            if(count($r['purchase_units'])>0){

                $reference_ID=empty($r['purchase_units'][0]['reference_id'])?NULL:$r['purchase_units'][0]['reference_id'];

                if(isset($r['purchase_units'][0]['amount'])){

                    $currency           =empty($r['purchase_units'][0]['amount']['currency_code'])?NULL:$r['purchase_units'][0]['amount']['currency_code'];
                    $amount             =empty($r['purchase_units'][0]['amount']['value'])?NULL:$r['purchase_units'][0]['amount']['value'];

                }

                if(isset($r['purchase_units'][0]['payee']))
                    $wallet_email=empty($r['purchase_units'][0]['payee'])?NULL:(empty($r['purchase_units'][0]['payee']['email_address'])?NULL:mb_strtolower($r['purchase_units'][0]['payee']['email_address'],'utf-8'));

                if(isset($r['purchase_units'][0]['payments']))
                    if(isset($r['purchase_units'][0]['payments']['captures']))
                        if(count($r['purchase_units'][0]['payments']['captures'])>0)
                            $create_time=$r['purchase_units'][0]['payments']['captures'][0]['create_time'];

                if(isset($r['purchase_units'][0]['shipping'])){

                    $full_name      =empty($r['purchase_units'][0]['shipping']['name'])?NULL:Json::encode($r['purchase_units'][0]['shipping']['name']);
                    $address_data   =empty($r['purchase_units'][0]['shipping']['address'])?NULL:$r['purchase_units'][0]['shipping']['address'];

                    if(!empty($address_data)){

                        $zip_code           =empty($address_data['postal_code'])?NULL:$address_data['postal_code'];
                        $country_code       =empty($address_data['country_code'])?NULL:mb_strtolower($address_data['country_code'],'utf-8');

                        $address_list=[];

                        if(!empty($address_data['admin_area_2']))
                            $address_list[]=$address_data['admin_area_2'];

                        if(!empty($address_data['admin_area_1']))
                            $address_list[]=$address_data['admin_area_1'];

                        if(!empty($address_data['address_line_1']))
                            $address_list[]=$address_data['address_line_1'];

                        if(!empty($address_data['address_line_2']))
                            $address_list[]=$address_data['address_line_2'];

                        $address=implode(', ',$address_list);

                    }

                    if(!empty($r['payer'])){

                        $name           =empty($r['payer']['name']['given_name'])?NULL:$r['payer']['name']['given_name'];
                        $surname        =empty($r['payer']['name']['surname'])?NULL:$r['payer']['name']['surname'];
                        $email          =empty($r['payer']['email_address'])?NULL:mb_strtolower($r['payer']['email_address'],'utf-8');
                        $country_code   =empty($r['payer']['address'])?NULL:$r['payer']['address']['country_code'];
                        $payer_ID       =empty($r['payer']['payer_id'])?NULL:$r['payer']['payer_id'];
                        $phone          =empty($r['payer']['phone'])?NULL:(empty($r['payer']['phone']['phone_number'])?NULL:Json::encode($r['payer']['phone']['phone_number']));

                        self::$user_wallet_address=$email;

                    }


                }

            }

        if(empty($wallet_email)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'PayPal email is empty'
            ];

            throw new ParametersException($error);

        }

        $paypal_ID=PayPal::get_paypal_ID($wallet_email);

        self::$target_wallet_address=$wallet_email;

        self::$paypal_payment_ID=PayPalPayment::add_payment(
            $order_ID,
            $reference_ID,
            $status,
            $intent,
            $currency,
            $amount,
            true,
            false,
            $create_time,
            $response,
            $paypal_ID,
            $name,
            $surname,
            $full_name,
            $email,
            $payer_ID,
            $country_code,
            $address,
            $zip_code,
            $phone
        );

        return true;

    }

    /**
     * @return array
     */
    private static function set_return(){

        $data=[
            'paypal_payment_ID'     =>self::$paypal_payment_ID,
            'user_wallet_address'   =>self::$user_wallet_address,
            'target_wallet_address' =>self::$target_wallet_address
        ];

        return $data;

    }

    /**
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_order_ID())
            if(self::paypal_auth())
                if(self::check_order())
                    return self::set_return();

            return false;

    }

    /**
     * @param string|NULL $order_ID
     * @param float|NULL $amount
     * @param bool $is_sandbox
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $order_ID=NULL,float $amount=NULL,bool $is_sandbox=false){

        $error_info_list=[];

        if(empty($order_ID))
            $error_info_list[]='Order ID is empty';

        if(empty($amount))
            $error_info_list[]='Amount is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        self::$order_ID     =$order_ID;
        self::$amount       =(float)$amount;
        self::$is_sandbox   =$is_sandbox;

        return self::set();

    }

}