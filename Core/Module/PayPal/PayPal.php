<?php

namespace Core\Module\PayPal;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;

class PayPal{

    /** @var string */
    public  static $table_name='_paypal';

    /**
     * @param string|NULL $email
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_paypal_ID(string $email=NULL){

        if(empty($email)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Order ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'email'=>$email
        ];

        return Db::get_row_ID(self::$table_name,0,$where_list);

    }

}