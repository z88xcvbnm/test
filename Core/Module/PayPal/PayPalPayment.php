<?php

namespace Core\Module\PayPal;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;

class PayPalPayment{

    /** @var string */
    public  static $table_name='_paypal_payment';

    /**
     * @param string|NULL $order_ID
     * @param int|NULL $user_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_order_ID(string $order_ID=NULL,int $user_ID=NULL){

        if(empty($order_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Order ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'order_id'=>$order_ID
        ];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param string|NULL $order_ID
     * @param string|NULL $reference_ID
     * @param string|NULL $status
     * @param string|NULL $intent
     * @param string|NULL $currency
     * @param float|NULL $amount
     * @param bool $is_check
     * @param bool $is_error
     * @param string|NULL $date_paypal_create
     * @param string|NULL $response
     * @param int|NULL $paypal_ID
     * @param string|NULL $name
     * @param string|NULL $surname
     * @param string|NULL $full_name
     * @param string|NULL $email
     * @param string|NULL $payer_ID
     * @param string|NULL $country_code
     * @param string|NULL $address
     * @param string|NULL $zip_code
     * @param string|NULL $phone
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_payment(
        string $order_ID                    =NULL,
        string $reference_ID                =NULL,
        string $status                      =NULL,
        string $intent                      =NULL,
        string $currency                    =NULL,
        float $amount                       =NULL,
        bool $is_check                      =false,
        bool $is_error                      =false,
        string $date_paypal_create          =NULL,
        string $response                    =NULL,
        int $paypal_ID                      =NULL,
        string $name                        =NULL,
        string $surname                     =NULL,
        string $full_name                   =NULL,
        string $email                       =NULL,
        string $payer_ID                    =NULL,
        string $country_code                =NULL,
        string $address                     =NULL,
        string $zip_code                    =NULL,
        string $phone                       =NULL
    ){

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'               =>User::$user_ID,
                'order_id'              =>$order_ID,
                'reference_id'          =>$reference_ID,
                'status'                =>$status,
                'intent'                =>$intent,
                'currency'              =>$currency,
                'amount'                =>$amount,
                'response'              =>$response,
                'is_check'              =>(int)$is_check,
                'is_error'              =>(int)$is_error,
                'date_paypal_create'    =>$date_paypal_create,
                'paypal_id'             =>empty($paypal_ID)?NULL:$paypal_ID,
                'name'                  =>empty($name)?NULL:$name,
                'surname'               =>empty($surname)?NULL:$surname,
                'email'                 =>empty($email)?NULL:$email,
                'payer_id'              =>empty($payer_ID)?NULL:$payer_ID,
                'country_code'          =>empty($country_code)?NULL:$country_code,
                'full_name'             =>empty($full_name)?NULL:$full_name,
                'address'               =>empty($address)?NULL:$address,
                'zip_code'              =>empty($zip_code)?NULL:$zip_code,
                'phone'                 =>empty($phone)?NULL:$phone,
                'date_create'           =>'NOW()',
                'date_update'           =>'NOW()',
                'type'                  =>0
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Payment was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

}