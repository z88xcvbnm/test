<?php

namespace Core\Module\File;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Encrypt\HashFile;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\ShellExecException;
use Core\Module\Os\Os;
use Core\Module\OsServer\OsServer;
use Core\Module\User\User;

class FileCat{

    /** @var int */
    private static $file_ID;

    /** @var string */
    private static $date_create;

    /** @var int */
    private static $chunk_len;

    /** @var string */
    private static $file_content_type;

    /** @var string */
    private static $file_extension;

    /** @var int */
    private static $file_size;

    /** @var string */
    private static $dir;

    /** @var string */
    private static $file_path;

    /** @var string */
    private static $file_mime_type;

    /** @var string */
    private static $dir_chunk;

    /** @var string */
    private static $file_hash;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$file_ID                  =NULL;
        self::$date_create              =NULL;
        self::$chunk_len                =NULL;
        self::$file_content_type        =NULL;
        self::$file_extension           =NULL;
        self::$file_size                =NULL;
        self::$dir                      =NULL;
        self::$file_path                =NULL;
        self::$file_mime_type           =NULL;
        self::$dir_chunk                =NULL;
        self::$file_hash                =NULL;

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_file_ID(){

        if(empty(self::$file_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        $select_list=array(
            'user_id',
            'chunk_len',
            'file_size',
            'file_content_type',
            'file_extension',
            'uploaded',
            'prepared',
            'date_create'
        );

        $file_data=Db::get_data_from_ID(self::$file_ID,'_file',$select_list,0);

        $error_info_list=[];

        if(empty($file_data))
            $error_info_list[]='File ID is not exists';

        if($file_data['user_id']!=User::$user_ID)
            $error_info_list[]='You don\'t have access for this file';

        if(empty($file_data['uploaded']))
            $error_info_list[]='File don\'t have full upload';

        if(!empty($file_data['prepared']))
            $error_info_list[]='File already prepared';

        if(empty($file_data['chunk_len']))
            $error_info_list[]='File chunk length is zero';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'file_ID'       =>self::$file_ID,
                    'user_ID'       =>User::$user_ID,
                    'file_data'     =>$file_data
                )
            );

            throw new ParametersException($error);

        }

        self::$chunk_len            =$file_data['chunk_len'];
        self::$file_size            =$file_data['file_size'];
        self::$file_extension       =mb_strtolower($file_data['file_extension'],'utf-8');
        self::$file_content_type    =$file_data['file_content_type'];
        self::$date_create          =$file_data['date_create'];

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_file_preparing(){

        $q=array(
            'table'=>'_file',
            'set'=>array(
                'preparing'     =>1,
                'date_create'   =>'NOW()'
            ),
            'where'=>array(
                'id'        =>self::$file_ID,
                'type'      =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'=>'DB query problem',
                'info'=>'File prepared was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_file_prepared(){

        $q=array(
            'table'=>'_file',
            'set'=>array(
                array(
                    'column'=>'hash',
                    'value'=>self::$file_hash
                ),
                array(
                    'column'=>'file_mime_type',
                    'value'=>self::$file_mime_type
                ),
                array(
                    'column'=>'preparing',
                    'value'=>0
                ),
                array(
                    'column'=>'prepared',
                    'value'=>1
                ),
                array(
                    'column'=>'date_create',
                    'value'=>'NOW()'
                )
            ),
            'where'=>array(
                'id'    =>self::$file_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'=>'DB query problem',
                'info'=>'File prepared was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * Set file hash
     */
    private static function set_file_hash(){

        self::$file_hash=HashFile::get_sha1_encode(self::$file_path);

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function cat_file_chunk(){

        $file_list  =[];
        $dir_root   =Dir::get_root_dir();

        for($index=1;$index<=self::$chunk_len;$index++){

            $file_path=$dir_root.'/'.self::$dir_chunk.'/'.$index.'.tmp';

            if(!file_exists($file_path)){

                $error=array(
                    'title'     =>'System problem',
                    'info'      =>'File chunk is not exists'
                );

                throw new PhpException($error);

            }

            $file_list[]=$file_path;

        }

        $error_info_list=[];

        if(count($file_list)==0)
            $error_info_list[]='File chunk list is empty';

        if(count($file_list)!=self::$chunk_len)
            $error_info_list[]='File chunk length is not correct';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Path problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'file_ID'=>self::$file_ID,
                    'user_ID'=>User::$user_ID,
                    'dir'=>self::$dir,
                    'dir_chunk'=>self::$dir_chunk
                )
            );

            throw new PathException($error);

        }

        if(OsServer::$is_windows){

            $file_list  =str_replace('/','\\',$file_list);
            $file_path  =str_replace('/','\\',$dir_root.'/'.self::$file_path);

            $q='copy /b "'.implode('"+"',$file_list).'" "'.$file_path.'"';

        }
        else
            $q='cat '.implode(' ',$file_list).' > '.$dir_root.'/'.self::$file_path;

        shell_exec($q);

        if(!file_exists(self::$file_path)){

            $error=array(
                'title'=>'Shell exec problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'file_ID'=>self::$file_ID,
                    'user_ID'=>User::$user_ID,
                    'dir'=>self::$dir,
                    'dir_chunk'=>self::$dir_chunk,
                    'file_path'=>self::$file_path,
                    'query'=>$q
                )
            );

            throw new ShellExecException($error);

        }

        return true;

    }

    /**
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function valid_file_size(){

        $file_size=filesize(self::$file_path);

        if(self::$file_size!=$file_size){

            $error=array(
                'title'=>'Path problem',
                'info'=>'Result file size on drive is not correct',
                'data'=>array(
                    'file_ID'=>self::$file_ID,
                    'user_ID'=>User::$user_ID,
                    'dir'=>self::$dir,
                    'dir_chunk'=>self::$dir_chunk,
                    'file_path'=>self::$file_path,
                    'file_size_result'=>$file_size,
                    'file_size'=>self::$file_size
                )
            );

            throw new PathException($error);

        }

    }

    /**
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_file_mime_type(){

        self::$file_mime_type=FileType::get_file_mime_type(self::$file_path);

        if(!isset(FileConfig::$file_mime_type_list[self::$file_mime_type])){

            $error=array(
                'title'=>'File problem',
                'info'=>'File mime type have not access',
                'data'=>array(
                    'file_ID'           =>self::$file_ID,
                    'date_create'       =>self::$date_create,
                    'user_ID'           =>User::$user_ID,
                    'dir'               =>self::$dir,
                    'file_path'         =>self::$file_path,
                    'file_mime_type'    =>self::$file_mime_type
                )
            );

            throw new PathException($error);

        }

        if(array_search(self::$file_extension,FileConfig::$file_mime_type_list[self::$file_mime_type])===false){

            $error=array(
                'title'=>'File problem',
                'info'=>'File type not exists in file config',
                'data'=>array(
                    'file_ID'           =>self::$file_ID,
                    'date_create'       =>self::$date_create,
                    'user_ID'           =>User::$user_ID,
                    'dir'               =>self::$dir,
                    'file_path'         =>self::$file_path,
                    'file_mime_type'    =>self::$file_mime_type,
                    'file_extension'    =>self::$file_extension
                )
            );

            throw new PathException($error);

        }

    }

    /**
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_dir(){

        $date_path      =Date::get_date_path(self::$date_create,DirConfig::$dir_upload);
        self::$dir      =$date_path.'/'.self::$file_ID;

        if(!file_exists(self::$dir)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Dir is not exists',
                'data'=>array(
                    'file_ID'       =>self::$file_ID,
                    'date_create'   =>self::$date_create,
                    'user_ID'       =>User::$user_ID,
                    'dir'           =>self::$dir
                )
            );

            throw new PathException($error);

        }

    }

    /**
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_dir_chunk(){

        self::$dir_chunk=self::$dir.'/chunk';

        if(!file_exists(self::$dir_chunk)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Dir for chunk is not exists',
                'data'=>array(
                    'file_ID'       =>self::$file_ID,
                    'date_create'   =>self::$date_create,
                    'user_ID'       =>User::$user_ID,
                    'dir'           =>self::$dir,
                    'dir_chunk'     =>self::$dir_chunk
                )
            );

            throw new PathException($error);

        }

        $file_len=scandir(self::$dir_chunk);

        if(count($file_len)==2){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Dir for chunk is empty',
                'data'=>array(
                    'file_ID'       =>self::$file_ID,
                    'date_create'   =>self::$date_create,
                    'user_ID'       =>User::$user_ID,
                    'dir'           =>self::$dir,
                    'dir_chunk'     =>self::$dir_chunk
                )
            );

            throw new PathException($error);

        }

    }

    /**
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_file_path(){

        self::$file_path=self::$dir.'/'.self::$file_ID.'.'.self::$file_extension;

        if(file_exists(self::$file_path)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'File already exists on the disk',
                'data'=>array(
                    'file_ID'       =>self::$file_ID,
                    'date_create'   =>self::$date_create,
                    'user_ID'       =>User::$user_ID,
                    'dir'           =>self::$dir,
                    'file_path'     =>self::$file_path
                )
            );

            throw new PathException($error);

        }

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_file_ID()){

            self::update_file_preparing();

            self::set_dir();
            self::set_dir_chunk();
            self::set_file_path();

            self::cat_file_chunk();

            self::valid_file_size();

            self::set_file_mime_type();
            self::set_file_hash();

            self::update_file_prepared();

            return true;

        }

        return false;

    }

    /**
     * @param int|NULL $file_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $file_ID=NULL){

        self::$file_ID=(int)$file_ID;

        if(self::set())
            return array(
                'file_ID'       =>self::$file_ID,
                'file_size'     =>self::$file_size
            );

        return NULL;

    }

}