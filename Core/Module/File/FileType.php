<?php

namespace Core\Module\File;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Exception\PhpException;

class FileType{

    /**
     * @param string|NULL $file_path
     * @return bool|mixed|string
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_extension(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title'=>'Parameter problem',
                'info'=>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FileTypeCash::isset_file_parameter_in_cash($file_path,'file_extension'))
            return FileTypeCash::get_file_parameter_in_cash($file_path,'file_extension');

        if(!file_exists($file_path)){

            $error=array(
                'title'=>'File path problem',
                'info'=>'File is not exists',
                'data'=>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $file_extension=substr(strrchr($file_path,'.'),1);

        FileTypeCash::add_file_parameter_in_cash($file_path,'file_extension',$file_extension);

        return $file_extension;

    }

    /**
     * @param string|NULL $file_path
     * @return string[]|null
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_mime_type(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title'=>'Parameter problem',
                'info'=>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FileTypeCash::isset_file_parameter_in_cash($file_path,'file_mime_type'))
            return FileTypeCash::get_file_parameter_in_cash($file_path,'file_mime_type');

        if(!file_exists($file_path)){

            $error=array(
                'title'=>'File path problem',
                'info'=>'File is not exists',
                'data'=>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $result                 =new \finfo();
        $file_mime_type         =$result->file($file_path,FILEINFO_MIME);
        $file_mime_type_list    =mb_split(';',$file_mime_type);

//        print_r($file_mime_type_list);

        if(count($file_mime_type_list)==0){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Mime type is not valid'
            ];

            throw new PhpException($error);

        }

        FileTypeCash::add_file_parameter_in_cash($file_path,'file_mime_type',$file_mime_type_list[0]);

        return $file_mime_type_list[0];

    }

    /**
     * @param string|NULL $file_path
     * @return bool|string|null
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_content_type(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title'=>'Parameter problem',
                'info'=>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FileTypeCash::isset_file_parameter_in_cash($file_path,'file_content_type'))
            return FileTypeCash::get_file_parameter_in_cash($file_path,'file_content_type');

        if(!file_exists($file_path)){

            $error=array(
                'title'=>'File path problem',
                'info'=>'File is not exists',
                'data'=>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $file_mime_type=self::get_file_mime_type($file_path);

        if(empty($file_mime_type)){

            $error=array(
                'title'=>'File problem',
                'info'=>'File mime type is empty',
                'data'=>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $file_content_type=substr($file_mime_type,0,strpos($file_mime_type,'/'));

        FileTypeCash::add_file_parameter_in_cash($file_path,'file_content_type',$file_content_type);

        return $file_content_type;

    }

}