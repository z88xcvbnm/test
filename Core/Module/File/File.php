<?php

namespace Core\Module\File;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Exception\DbParametersException;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\FileException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Php\PhpValidation;
use Core\Module\User\User;

class File{

    /** @var string */
    public  static $table_name='_file';

    /**
     * @param int|NULL $file_ID
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_file_ID(int $file_ID=NULL){

        if(empty($file_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($file_ID,'_file');

    }

    /**
     * @param int|NULL $file_ID
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_file(int $file_ID=NULL){

        if(empty($file_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($file_ID,'_file');

    }

    /**
     * @param int|NULL $file_ID
     * @param int|NULL $chunk_index
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_file_chunk_index(int $file_ID=NULL,int $chunk_index=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(is_null($chunk_index))
            $error_info_list[]='Chunk index is null';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'file_ID'       =>$file_ID,
                    'chunk_index'   =>$chunk_index
                )
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            array(
                'column'=>'file_ID',
                'value'=>$file_ID
            ),
            array(
                'column'=>'index',
                'value'=>$chunk_index
            )
        );

        return Db::isset_row('_file_item',0,$where_list);

    }

    /**
     * @param int|NULL $file_chunk_ID
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_file_chunk_ID(int $file_chunk_ID=NULL){

        if(empty($file_chunk_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File chunk ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($file_chunk_ID,'_file_item',0);

    }

    /**
     * @param string|NULL $file_path
     * @return bool
     */
    public  static function isset_file_link(string $file_path=NULL){

        $r=@get_headers($file_path);

        if(!isset($r[0]))
            return false;

        foreach($r as $row){

            switch($row){

                case'HTTP/1.0 404 Not Found':
                case'HTTP/1.0 302 Found':
                    return false;

                case'Content-Type: text/html':
                    return false;

            }

        }

        return true;

    }

    /**
     * @param string|NULL $hash
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_file_hash(string $hash=NULL){

        if(empty($hash)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'File hash is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_file',
            'where'=>array(
                array(
                    'column'    =>'file_id',
                    'value'     =>NULL
                ),
                array(
                    'column'    =>'hash',
                    'value'     =>$hash
                ),
                array(
                    'column'    =>'prepared',
                    'value'     =>1
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return count($r)>0;

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_extension
     * @param string|NULL $date_create
     * @return string|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_dir(int $file_ID=NULL,string $file_extension=NULL,string $date_create=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='file ID is empty';

//        if(empty($file_extension))
//            $error_info_list[]='File type is empty';

        if(empty($date_create))
            $error_info_list[]='Date create is empty';

        if(FileParametersCash::isset_file_parameter_in_cash($file_ID,'file_dir'))
            return FileParametersCash::get_file_parameter_in_cash($file_ID,'file_dir');

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'file_ID'           =>$file_ID,
                    'file_extension'    =>$file_extension,
                    'date_create'       =>$date_create
                )
            );

            throw new ParametersException($error);

        }

        $date_path      =Date::get_date_path($date_create,DirConfig::$dir_upload);
        $file_path      =$date_path.'/'.$file_ID;

        if(PhpValidation::is_console_running())
            $file_path=DIR_ROOT.'/'.$file_path;

        if(!file_exists($file_path))
            Dir::create_dir($file_path);

        FileParametersCash::add_file_parameter_in_cash($file_ID,$file_path);

        return $file_path;

    }

    /**
     * @param array $file_ID_list
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_dir_list(array $file_ID_list=[]){

        if(count($file_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File ID list is empty'
            );

            throw new ParametersException($error);

        }
        
        $q=array(
            'select'=>array(
                'id',
                'file_extension',
                'date_create'
            ),
            'table'=>'_file',
            'where'=>array(
                'id'        =>$file_ID_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);
        
        if(count($r)==0)
            return[];
        
        $list=[];
        
        foreach($r as $row){

            $file_file_path=self::get_file_dir($row['id'],$row['file_extension'],$row['date_create']);

            if(!file_exists($file_file_path)){

                $error=array(
                    'title' =>'File path problem',
                    'info'  =>'File dir is not exists'
                );

                throw new PathException($error);

            }

            $list[$row['id']]=$file_file_path;

            FileParametersCash::add_file_parameter_in_cash($row['id'],'file_dir',$file_file_path);
            
        }

        return $list;

    }

    /**
     * @param string|NULL $hash
     * @return |null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_ID_from_hash(string $hash=NULL){

        if(empty($hash)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'File hash is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_file',
            'where'=>array(
                array(
                    'column'    =>'file_id',
                    'value'     =>NULL
                ),
                array(
                    'column'    =>'hash',
                    'value'     =>$hash
                ),
                array(
                    'column'    =>'prepared',
                    'value'     =>1
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return empty($r[0]['id'])?NULL:$r[0]['id'];

    }

    /**
     * @param int|NULL $file_ID
     * @param bool $is_uploaded
     * @param bool $is_prepared
     * @return mixed|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_data(int $file_ID=NULL,bool $is_uploaded=true,bool $is_prepared=true){

        if(empty($file_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        $column_list=array(
            'user_id',
            'hash',
            'chunk_uploaded',
            'chunk_len',
            'chunk_size',
            'file_size',
            'file_mime_type',
            'file_content_type',
            'file_extension',
            'uploaded',
            'date_create'
        );

        $where_list=array(
            'uploaded'  =>(int)$is_uploaded,
            'prepared'  =>(int)$is_prepared
        );

        if(!empty($file_ID))
            $where_list['id']=$file_ID;

        $data=Db::get_data_from_ID($file_ID,'_file',$column_list,0,$where_list);

        if(empty($data))
            return NULL;

        return $data;

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_extension
     * @param string|NULL $date_create
     * @return string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_path(int $file_ID=NULL,string $file_extension=NULL,string $date_create=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($file_extension))
            $error_info_list[]='File type is empty';

        if(empty($date_create))
            $error_info_list[]='Date create is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'file_ID'       =>$file_ID,
                    'file_extension'     =>$file_extension,
                    'date_create'   =>$date_create
                )
            );

            throw new ParametersException($error);

        }

        $date_path=Date::get_date_path($date_create,DirConfig::$dir_upload);

        return $date_path.'/'.$file_ID.'.'.$file_extension;

    }

    /**
     * @param int|NULL $file_ID
     * @return |null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_duration(int $file_ID=NULL){

        if(empty($file_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        $r=Db::get_data_from_ID($file_ID,'_file',array('duration'),0);

        if(count($r)==0)
            return NULL;

        $duration=empty($r[0]['duration'])?NULL:$r[0]['duration'];

        FileParametersCash::add_file_parameter_in_cash($file_ID,'duration',$duration);

        return $duration;

    }

    /**
     * @param int|NULL $file_ID
     * @return |null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_size(int $file_ID=NULL){

        if(empty($file_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        $r=Db::get_data_from_ID($file_ID,'_file',array('file_size'),0);

        if(count($r)==0)
            return NULL;

        $file_size=empty($r[0]['file_size'])?NULL:$r[0]['file_size'];

        FileParametersCash::add_file_parameter_in_cash($file_ID,'file_size',$file_size);

        return $file_size;

    }

    /**
     * @param string|NULL $file_path
     * @return string[]|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_mime_type_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'File path is empty'
            ];

            throw new ParametersException($error);

        }

        if(!file_exists($file_path)){

            $error=[
                'title'     =>FileException::$title,
                'info'      =>'File is not exists'
            ];

            throw new FileException($error);

        }


        return FileType::get_file_mime_type($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_content_type_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'File path is empty'
            ];

            throw new ParametersException($error);

        }

        if(!file_exists($file_path)){

            $error=[
                'title'     =>FileException::$title,
                'info'      =>'File is not exists'
            ];

            throw new FileException($error);

        }

        $file_mime_type=self::get_file_mime_type_from_path($file_path);

        if(empty($file_mime_type)){

            $error=[
                'title'     =>FileException::$title,
                'info'      =>'File mime type is empty'
            ];

            throw new FileException($error);

        }

        $list=mb_split('/',$file_mime_type);

        if(count($list)<2){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'File mime type is not valid'
            ];

            throw new ParametersException($error);

        }

        return $list[0];

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_extension_from_file_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'File name is empty'
            ];

            throw new ParametersException($error);

        }

        if(!file_exists($file_path)){

            $error=[
                'title'     =>FileException::$title,
                'info'      =>'File is not exists'
            ];

            throw new FileException($error);

        }

        return pathinfo($file_path,PATHINFO_EXTENSION);

    }

    /**
     * @param string|NULL $file_name
     * @return mixed|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_extension_from_file_name(string $file_name=NULL){

        if(empty($file_name)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'File name is empty'
            ];

            throw new ParametersException($error);

        }

        $list=mb_split('\.',$file_name);

        if(count($list)<2)
            return NULL;

        return end($list);

    }

    /**
     * @param int|NULL $file_ID
     * @return |null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_date_create(int $file_ID=NULL){

        if(empty($file_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        if(FileParametersCash::isset_file_parameter_in_cash($file_ID,'date_create'))
            return FileParametersCash::get_file_parameter_in_cash($file_ID,'date_create');

        $r=Db::get_data_from_ID($file_ID,'_file',array('date_create'),0);

        if(count($r)==0)
            return NULL;

        $date_create=empty($r[0]['date_create'])?NULL:$r[0]['date_create'];

        FileParametersCash::add_file_parameter_in_cash($file_ID,'date_create',$date_create);

        return $date_create;

    }

    /**
     * @param int|NULL $file_ID
     * @param bool $need_path
     * @return string|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_path_from_file_ID(int $file_ID=NULL,bool $need_path=false){

        if(empty($file_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        if(FileParametersCash::isset_file_parameter_in_cash($file_ID,'file_path'))
            return FileParametersCash::get_file_parameter_in_cash($file_ID,'file_path');

        $column_list        =[];
        $file_extension     =NULL;
        $date_create        =NULL;

        if(FileParametersCash::isset_file_parameter_in_cash($file_ID,'file_extension'))
            $file_extension=FileParametersCash::get_file_parameter_in_cash($file_ID,'file_extension');
        else
            $column_list[]='file_extension';

        if(FileParametersCash::isset_file_parameter_in_cash($file_ID,'date_create'))
            $date_create=FileParametersCash::get_file_parameter_in_cash($file_ID,'date_create');
        else
            $column_list[]='date_create';

        if(count($column_list)>0){

            $r=Db::get_data_from_ID($file_ID,'_file',$column_list,0);

            if(!empty($r['file_extension'])){

                $file_extension=$r['file_extension'];

                FileParametersCash::add_file_parameter_in_cash($file_ID,'file_extension',$file_extension);

            }

            if(!empty($r['date_create'])){

                $date_create=$r['date_create'];

                FileParametersCash::add_file_parameter_in_cash($file_ID,'date_create',$date_create);

            }

        }

        if(
              empty($date_create)
            &&empty($file_extension)
        ){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'System cannot get file date create',
                'data'  =>array(
                    'file_ID'           =>$file_ID,
                    'file_extension'    =>$file_extension,
                    'date_create'       =>$date_create
                )
            );

            throw new DbParametersException($error);

        }

        $file_dir   =self::get_file_dir($file_ID,$file_extension,$date_create);
        $file_path  =$file_dir.'/'.$file_ID;

//        if(PhpValidation::is_console_running())
//            $file_path=DIR_ROOT.'/'.$file_path;

        if(!empty($file_extension))
            $file_path.='.'.$file_extension;

        if(!$need_path)
            if(!file_exists($file_path)){

                $error=array(
                    'title' =>'File path problem',
                    'info'  =>'File file is not exists',
                    'data'  =>array(
                        'file_ID'       =>$file_ID,
                        'file_path'     =>$file_path
                    )
                );

                throw new PathException($error);

            }

        FileParametersCash::add_file_parameter_in_cash($file_ID,'file_path',$file_path);

        return $file_path;

    }

    /**
     * @param array $file_ID_list
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_path_from_file_ID_list(array $file_ID_list=[]){

        if(count($file_ID_list)==0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'File ID list is empty'
            );

            throw new ParametersException($error);

        }

        $file_extension              =NULL;
        $file_path              =NULL;
        $date_create            =NULL;
        $file_list             =[];
        $select_file_ID_list   =[];

        foreach($file_ID_list as $file_ID)
            if(!isset($file_list[$file_ID])){

                if(FileParametersCash::isset_file_parameter_in_cash($file_ID,'file_path'))
                    $file_list[$file_ID]=FileParametersCash::get_file_parameter_in_cash($file_ID,'file_path');
                else{

                    if(FileParametersCash::isset_file_parameter_in_cash($file_ID,'file_extension'))
                        $file_extension=FileParametersCash::get_file_parameter_in_cash($file_ID,'file_extension');

                    if(FileParametersCash::isset_file_parameter_in_cash($file_ID,'date_create'))
                        $date_create=FileParametersCash::get_file_parameter_in_cash($file_ID,'date_create');

                    if(
                          empty($file_extension)
                        ||empty($date_create)
                    )
                        $select_file_ID_list[]=$file_ID;
                    else
                        $file_list[$file_ID]=self::get_file_path($file_ID,$file_extension,$date_create);

                }

            }

        if(count($select_file_ID_list)>0){

            $select_list=array(
                'id',
                'file_extension',
                'date_create'
            );

            $r=Db::get_data_from_ID_list($select_file_ID_list,'_file',$select_list,0);

            foreach($r as $row){

                $file_ID                    =$row['id'];
                $file_extension             =$row['file_extension'];
                $date_create                =$row['date_create'];
                $file_path                  =self::get_file_path($file_ID,$file_extension,$date_create);

                if(PhpValidation::is_console_running())
                    $file_path=DIR_ROOT.'/'.$file_path;

                $file_list[$file_ID]=$file_path;

                FileParametersCash::add_file_parameter_in_cash($file_ID,'file_extension',$file_extension);
                FileParametersCash::add_file_parameter_in_cash($file_ID,'date_create',$date_create);
                FileParametersCash::add_file_parameter_in_cash($file_ID,'file_path',$file_path);

            }

        }

        return $file_list;

    }

    /**
     * @param string|NULL $file_hash
     * @param int|NULL $chunk_uploaded
     * @param int|NULL $chunk_len
     * @param int|NULL $chunk_size
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @param bool $uploaded
     * @param bool $preparing
     * @param bool $prepared
     * @param string|NULL $source_link
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_file(string $file_hash=NULL,int $chunk_uploaded=NULL,int $chunk_len=NULL,int $chunk_size=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_content_type=NULL,string $file_extension=NULL,bool $uploaded=false,bool $preparing=false,bool $prepared=false,string $source_link=NULL){

        $error_info_list=[];

        if(empty($file_hash))
            $error_info_list[]='File hash is empty';

        if(is_null($chunk_uploaded))
            $error_info_list[]='Chunk uploaded is not set';

        if(is_null($chunk_size))
            $error_info_list[]='Chunk size is not set';

        if(empty($chunk_len))
            $error_info_list[]='Chunk length is empty';

        if(empty($file_size))
            $error_info_list[]='File size is empty';

        if(empty($file_mime_type))
            $error_info_list[]='File mime type is empty';

        if(empty($file_content_type))
            $error_info_list[]='File content type is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>'_file',
            'values'=>[
                'user_id'               =>User::$user_ID,
                'hash'                  =>$file_hash,
                'chunk_uploaded'        =>$chunk_uploaded,
                'chunk_len'             =>$chunk_len,
                'chunk_size'            =>$chunk_size,
                'file_size'             =>$file_size,
                'file_mime_type'        =>$file_mime_type,
                'file_content_type'     =>$file_content_type,
                'file_extension'        =>$file_extension,
                'uploaded'              =>(int)$uploaded,
                'preparing'             =>(int)$preparing,
                'prepared'              =>(int)$prepared,
                'source_link'           =>empty($source_link)?NULL:$source_link,
                'date_create'           =>'NOW()',
                'date_update'           =>'NOW()',
                'type'                  =>0
            ]
        ];

        $r=Db::insert($q,true,['date_create']);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'File was not add'
            ];

            throw new DbQueryException($error);

        }

        FileParametersCash::add_file_parameter_in_cash($r[0]['id'],'date_create',$r[0]['date_create']);

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_hash
     * @param int|NULL $chunk_uploaded
     * @param int|NULL $chunk_len
     * @param int|NULL $chunk_size
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @param bool $uploaded
     * @param bool $preparing
     * @param bool $prepared
     * @param string|NULL $source_link
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_file(int $file_ID=NULL,string $file_hash=NULL,int $chunk_uploaded=NULL,int $chunk_len=NULL,int $chunk_size=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_content_type=NULL,string $file_extension=NULL,bool $uploaded=false,bool $preparing=false,bool $prepared=false,string $source_link=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($file_hash))
            $error_info_list[]='File hash is empty';

        if(is_null($chunk_uploaded))
            $error_info_list[]='Chunk uploaded is not set';

        if(is_null($chunk_size))
            $error_info_list[]='Chunk size is not set';

        if(empty($chunk_len))
            $error_info_list[]='Chunk length is empty';

        if(empty($file_size))
            $error_info_list[]='File size is empty';

        if(empty($file_mime_type))
            $error_info_list[]='File mime type is empty';

        if(empty($file_content_type))
            $error_info_list[]='File content type is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>'_file',
            'set'=>[
                'hash'                  =>$file_hash,
                'chunk_uploaded'        =>$chunk_uploaded,
                'chunk_len'             =>$chunk_len,
                'chunk_size'            =>$chunk_size,
                'file_size'             =>$file_size,
                'file_mime_type'        =>$file_mime_type,
                'file_content_type'     =>$file_content_type,
                'file_extension'        =>$file_extension,
                'uploaded'              =>(int)$uploaded,
                'preparing'             =>(int)$preparing,
                'prepared'              =>(int)$prepared,
                'date_update'           =>'NOW()',
                'type'                  =>0
            ],
            'where'=>[
                'id'        =>$file_ID,
                'type'      =>0
            ]
        ];

        if(!empty($source_link))
            $q['set']['source_link']=$source_link;

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'File was not add'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_extension
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_file_extension(int $file_ID=NULL,string $file_extension=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($file_extension))
            $error_info_list[]='File extension is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>'_file',
            'set'=>[
                'file_extension'        =>$file_extension,
                'date_update'           =>'NOW()',
                'type'                  =>0
            ],
            'where'=>[
                'id'        =>$file_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'File was not add'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $file_extension
     * @param bool $uploaded
     * @param bool $preparing
     * @param bool $prepared
     * @param string|NULL $source_link
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_file_without_params(string $file_extension=NULL,bool $uploaded=false,bool $preparing=false,bool $prepared=false,string $source_link=NULL){

        $q=[
            'table'=>'_file',
            'values'=>[
                'user_id'               =>User::$user_ID,
                'file_extension'        =>$file_extension,
                'uploaded'              =>(int)$uploaded,
                'preparing'             =>(int)$preparing,
                'prepared'              =>(int)$prepared,
                'date_create'           =>'NOW()',
                'date_update'           =>'NOW()',
                'type'                  =>0
            ]
        ];

        if(!empty($source_link))
            $q['values']['source_link']=$source_link;

        $r=Db::insert($q,true,['date_create']);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'File was not add'
            ];

            throw new DbQueryException($error);

        }

        FileParametersCash::add_file_parameter_in_cash($r[0]['id'],'date_create',$r[0]['date_create']);

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $file_ID
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_file_ID(int $file_ID=NULL){

        if(empty($file_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'File ID is empty'
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'id'=>$file_ID
        ];

        if(!Db::delete_from_where_list(self::$table_name,0,$where_list)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'File was not remove'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

}