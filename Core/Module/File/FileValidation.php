<?php

namespace Core\Module\File;

use Core\Module\Exception\ParametersException;

class FileValidation{

    /**
     * @param string|NULL $file_path_1
     * @param string|NULL $file_path_2
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_equal_files(string $file_path_1=NULL,string $file_path_2=NULL){
        
        $error_info_list=[];
        
        if(empty($file_path_1))
            $error_info_list[]='File path 1 is empty';
        else if(!file_exists($file_path_1))
            $error_info_list[]='File path 1 is not exists';
        
        if(empty($file_path_2))
            $error_info_list[]='File path 2 is empty';
        else if(!file_exists($file_path_2))
            $error_info_list[]='File path 2 is not exists';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $file_hash_1        =md5_file($file_path_1);
        $file_hash_2        =md5_file($file_path_2);

        return $file_hash_1==$file_hash_2;
        
    }

    /**
     * @param string|NULL $file_path_1
     * @param string|NULL $file_path_2
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_equal_file_sizes(string $file_path_1=NULL,string $file_path_2=NULL){

        $error_info_list=[];

        if(empty($file_path_1))
            $error_info_list[]='File path 1 is empty';
        else if(!file_exists($file_path_1))
            $error_info_list[]='File path 1 is not exists';

        if(empty($file_path_2))
            $error_info_list[]='File path 2 is empty';
        else if(!file_exists($file_path_2))
            $error_info_list[]='File path 2 is not exists';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $file_size_1        =filesize($file_path_1);
        $file_size_2        =filesize($file_path_2);

        return $file_size_1==$file_size_2;

    }

}