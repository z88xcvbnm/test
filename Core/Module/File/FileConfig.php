<?php

namespace Core\Module\File;

class FileConfig{

    /** @var int */
    public  static $file_size_max           =134217728; // 128 MB

    /** @var int */
    public  static $file_chunk_size_max     =1048576; // 1 MB

    /** @var int */
    public  static $file_chunk_len_max      =512;

    /** @var array */
    public  static $file_content_type_list  =array(
        'image'=>array(
            'image/jpeg',
            'image/png',
            'image/gif'
        ),
        'video'=>array(
            'video/mp4'
        ),
        'slide'=>array(
            'image/jpeg',
            'image/png',
            'image/gif'
        ),
        'logo'=>array(
            'image/jpeg',
            'image/png',
            'image/gif'
        ),
        'image_ico'=>array(
            'image/jpeg',
            'image/png',
            'image/gif'
        ),
        'audio'=>array(
            'audio/mp3'
        )
    );

    /** @var array */
    public  static $file_extension_list          =array(
        'jpeg'  =>'image',
        'jpg'   =>'image',
        'png'   =>'image',
        'mp3'   =>'audio'
    );

    /** @var array */
    public  static $file_mime_type_list=array(
        'image/jpeg'    =>array(
            'jpg','jpeg'
        ),
        'image/png'     =>array(
            'png'
        ),
        'image/gif'     =>array(
            'gif'
        ),
        'audio/mp3'     =>array(
            'mp3'
        ),
        'audio/mpeg'     =>array(
            'mp3',
            'mpg',
            'mpeg'
        ),
        'video/mp4'     =>array(
            'mp4'
        ),
    );

}