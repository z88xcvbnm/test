<?php

namespace Core\Module\File;

use Core\Module\Exception\ParametersException;

class FileParametersCash{

    /** @var array */
    public  static $file_list=[];

    /**
     * @param int|NULL $file_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_file_ID_in_cash(int $file_ID=NULL){

        if(empty($file_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        return isset(self::$file_list[$file_ID]);

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $param
     * @return bool
     */
    public  static function isset_file_parameter_in_cash(int $file_ID=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(empty(self::$file_list[$file_ID]))
            return false;

        return !empty(self::$file_list[$file_ID][$param]);

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $param
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_parameter_in_cash(int $file_ID=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'file_ID'      =>$file_ID,
                    'param'         =>$param
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$file_list[$file_ID]))
            return NULL;

        return empty(self::$file_list[$file_ID][$param])?NULL:self::$file_list[$file_ID][$param];

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $param
     * @param null $value
     * @param bool $is_rewrite
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_file_parameter_in_cash(int $file_ID=NULL,string $param=NULL,$value=NULL,bool $is_rewrite=true){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($param))
            $error_info_list[]='Parameters is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'file_ID'      =>$file_ID,
                    'param'         =>$param,
                    'value'         =>$value,
                    'is_rewrite'    =>$is_rewrite
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$file_list[$file_ID]))
            self::$file_list[$file_ID]=[];

        if($is_rewrite)
            self::$file_list[$file_ID][$param]=$value;

        return true;

    }

    /**
     * @param int|NULL $file_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_file_from_cash(int $file_ID=NULL){

        if(empty($file_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        if(self::isset_file_ID_in_cash($file_ID))
            unset($file_ID);

        return true;

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $param
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_file_parameter_from_cash(int $file_ID=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list,
                'data'  =>array(
                    'file_ID'      =>$file_ID,
                    'param'         =>$param,
                )
            );

            throw new ParametersException($error);

        }

        if(self::isset_file_ID_in_cash($file_ID))
            if(self::isset_file_parameter_in_cash($file_ID,$param))
                unset(self::$file_list[$file_ID]);

        return true;

    }

    /**
     * @param int|NULL $file_ID
     * @param array $param_list
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_file_parameters_list_form_cash(int $file_ID=NULL,array $param_list=[]){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(count($param_list)==0)
            $error_info_list[]='Parameters list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list,
                'data'  =>array(
                    'file_ID'       =>$file_ID,
                    'param_list'    =>$param_list,
                )
            );

            throw new ParametersException($error);

        }

        if(self::isset_file_ID_in_cash($file_ID))
            foreach($param_list as $param)
                if(self::remove_file_parameter_from_cash($file_ID,$param))
                    return true;

        return true;

    }

}