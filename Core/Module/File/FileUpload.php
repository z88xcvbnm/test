<?php

namespace Core\Module\File;

use Core\Module\Audio\AudioUploadedPrepare;
use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\FileException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Exception\SystemException;
use Core\Module\Image\ImageUploadedPrepare;
use Core\Module\User\User;
use Core\Module\Video\VideoUploadedPrepare;

class FileUpload{

    /** @var int */
    public  static $file_ID;

    /** @var int */
    public  static $file_chunk_ID;

    /** @var string */
    public  static $hash;

    /** @var string */
    public  static $chunk_base64;

    /** @var int */
    public  static $chunk_index;

    /** @var int */
    public  static $chunk_uploaded;

    /** @var int */
    public  static $chunk_len;

    /** @var int */
    public  static $file_chunk_size;

    /** @var int */
    public  static $file_size;

    /** @var string */
    public  static $file_mime_type;

    /** @var string */
    public  static $file_content_type;

    /** @var string */
    public  static $date_create;

    /** @var string */
    public  static $dir_file;

    /** @var string */
    public  static $dir_file_chunk;

    /** @var string */
    public  static $file_path;

    /** @var string */
    public  static $file_chunk_path;

    /** @var string */
    public  static $file_extension;

    /** @var bool */
    private static $is_complete;

    /** @var array */
    private static $data;

    /**
     * @return bool
     */
    private static function reset_data(){
        
        self::$file_ID              =NULL;
        self::$file_chunk_ID        =NULL;
        self::$hash                 =NULL;
        self::$chunk_base64         =NULL;
        self::$chunk_index          =NULL;
        self::$chunk_uploaded       =NULL;
        self::$chunk_len            =NULL;
        self::$file_chunk_size      =NULL;
        self::$file_size            =NULL;
        self::$file_mime_type       =NULL;
        self::$file_content_type    =NULL;
        self::$date_create          =NULL;
        self::$file_path            =NULL;

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function add_file(){

        $q=array(
            'table'     =>'_file',
            'values'    =>array(
                'user_id'               =>User::$user_ID,
                'chunk_len'             =>self::$chunk_len,
                'file_content_type'     =>self::$file_content_type,
                'file_mime_type'        =>self::$file_mime_type,
                'file_extension'        =>self::$file_extension,
                'file_size'             =>self::$file_size,
                'date_create'           =>'NOW()'
            )
        );

        $r=Db::insert($q,true,array('date_create'));
        
        if(empty($r)){
            
            $error=array(
                'title' =>'DB query problem',
                'info'  =>'File was not added'
            );

            throw new DbQueryException($error);
            
        }
        
        self::$date_create      =$r[0]['date_create'];
        self::$file_ID          =$r[0]['id'];
        self::$chunk_uploaded   =0;

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function add_file_chunk(){
        
        if(empty(self::$file_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_file_item',
            'values'    =>array(
                'user_id'       =>User::$user_ID,
                'file_id'       =>self::$file_ID,
                'index'         =>self::$chunk_index,
                'size'          =>self::$file_chunk_size,
                'date_create'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(empty($r)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'File chunk was not added',
                'data'  =>array(
                    'user_ID'       =>User::$user_ID,
                    'file_ID'       =>self::$file_ID,
                    'index'         =>self::$chunk_index,
                    'size'          =>self::$file_chunk_size,
                    'chunk_len'     =>self::$chunk_len,
                    'file_size'     =>self::$file_size,
                    'query'         =>$q
                )
            );

            throw new DbQueryException($error);

        }

        self::$file_chunk_ID=$r[0]['id'];

        return true;
        
    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function valid_file_mime_type(){

        if(!isset(FileConfig::$file_mime_type_list[self::$file_mime_type])){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File type don\'t isset in file content type list'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function valid_file_content_type(){

        if(!isset(FileConfig::$file_content_type_list[self::$file_content_type])){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File content type don\'t access in file config'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function valid_file_size(){

        if(self::$file_size>FileConfig::$file_size_max){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File size is big'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function valid_chunk_index(){

        if(self::$chunk_index>self::$chunk_len){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Chunk index is not valid'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function valid_chunk_len(){

        if(self::$chunk_len>FileConfig::$file_chunk_len_max){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Chunk length is big'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function valid_chunk_size(){

        if(self::$file_chunk_size>FileConfig::$file_chunk_size_max){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Chunk size is big'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function is_last_chunk(){

        return self::$chunk_uploaded+1==self::$chunk_len;

    }

    /**
     * @return bool|string
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function get_chunk_base64_decode(){

        try{

            return base64_decode(self::$chunk_base64);

        }
        catch(\Exception $error){

            throw new SystemException($error);

        }

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    public  static function update_file_chunk_uploaded(){

        $set_list=array(
            'chunk_uploaded'    =>array(
                'function'=>'"chunk_uploaded"+1'
            ),
            'uploaded'          =>(int)(self::$chunk_uploaded==self::$chunk_len),
            'date_update'       =>'NOW()'
        );

        if(!Db::update_data_from_ID(self::$file_ID,'_file',$set_list)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'File upload length was not added'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function set_file_chunk(){

        self::add_file_chunk();

        try{

//            echo self::$file_chunk_path."\n";

            $file_size=file_put_contents(self::$file_chunk_path.'.tmp',self::$chunk_base64);

            if(
                  $file_size>self::$file_chunk_size+50
                ||$file_size<self::$file_chunk_size-50
            ){

                $error=array(
                    'title' =>'File problem',
                    'info'  =>'File chunk size on server have not equal parameter',
                    'data'  =>[
                        'file_size'     =>$file_size,
                        'param_size'    =>self::$file_chunk_size
                    ]
                );

                throw new FileException($error);

            }

            return true;

        }
        catch(\Exception $error){

            throw new SystemException($error);

        }

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function set_file_chunk_uploaded(){

        self::$chunk_uploaded++;

        return self::update_file_chunk_uploaded();

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\ShellExecException
     */
    private static function set_prepare_concat_chunk_list(){

        self::$is_complete=false;

        if(self::$chunk_uploaded==self::$chunk_len){

            FileCat::init(self::$file_ID);

            self::$is_complete=true;

            return true;

        }

        return false;

    }

    /**
     * @throws DbQueryException
     * @throws FileException
     * @throws ParametersException
     * @throws PhpException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \ImagickException
     */
    private static function set_switch_prepare_content(){

        if(self::$is_complete)
            switch(self::$file_content_type){

                case'slide':
                case'image':
                case'logo':
                case'image_ico':{

                    self::$data=ImageUploadedPrepare::init(self::$file_ID,self::$file_content_type,self::$file_extension,NULL,false);

                    if(empty(self::$data)){

                        $error=array(
                            'title'     =>'System problem',
                            'info'      =>'Image was not uploaded'
                        );

                        throw new PhpException($error);

                    }

                    break;

                }

                case'video':{

                    self::$data=VideoUploadedPrepare::init(self::$file_ID,self::$file_content_type,self::$file_extension);

                    if(empty(self::$data)){

                        $error=array(
                            'title'     =>'System problem',
                            'info'      =>'Video was not uploaded'
                        );

                        throw new PhpException($error);

                    }

                    break;

                }

                case'audio':{

                    self::$data=AudioUploadedPrepare::init(self::$file_ID,self::$file_content_type,self::$file_extension);

                    if(empty(self::$data)){

                        $error=array(
                            'title'     =>'System problem',
                            'info'      =>'Audio was not uploaded'
                        );

                        throw new PhpException($error);

                    }

                    break;

                }

                default:{

                    $error=array(
                        'title'     =>'Parameters problem',
                        'info'      =>'File conttent type is not valid'
                    );

                    throw new ParametersException($error);

                }

            }

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function set_dir_file(){

        self::$dir_file=Date::get_date_path(self::$date_create,DirConfig::$dir_upload);

        Dir::create_dir(self::$dir_file);

        self::$dir_file.='/'.self::$file_ID;

        return Dir::create_dir(self::$dir_file);

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function set_dir_file_chunk(){

        self::$dir_file_chunk=self::$dir_file.'/chunk';

        return Dir::create_dir(self::$dir_file_chunk);

    }

    /**
     * @return bool
     */
    private static function set_file_path(){

        self::$file_path=self::$dir_file.'/'.self::$file_ID;

        return true;

    }

    /**
     * @return bool
     */
    private static function set_file_chunk_path(){

        self::$file_chunk_path=self::$dir_file_chunk.'/'.self::$chunk_index;

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function set_chunk_base64(){

        self::$chunk_base64=self::get_chunk_base64_decode();

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function set_file_data(){

        $data=File::get_file_data(self::$file_ID,false,false);

        if(empty($data)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'File data is empty'
            );

            throw new DbQueryException($error);

        }

        if(!empty($data['uploaded'])){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'File already uploaded',
                'data'  =>array(
                    'user_ID'       =>User::$user_ID,
                    'file_ID'       =>self::$file_ID,
                    'index'         =>self::$chunk_index,
                    'size'          =>self::$file_chunk_size,
                    'chunk_len'     =>self::$chunk_len,
                    'file_size'     =>self::$file_size
                )
            );

            throw new DbQueryException($error);

        }

        $error_info_list=[];

        if($data['user_id']!=User::$user_ID)
            $error_info_list[]='File don\'t access for you user ID';

        if($data['chunk_len']!=self::$chunk_len)
            $error_info_list[]='File chunk length is not correct';

        if(!isset(FileConfig::$file_mime_type_list[$data['file_mime_type']]))
            $error_info_list[]='File content type don\'t access in file config';

        if($data['file_mime_type']!=self::$file_mime_type)
            $error_info_list[]='File content type is not correct';

        if($data['file_content_type']!=self::$file_content_type)
            $error_info_list[]='File content type is not correct';

        if($data['file_size']!=self::$file_size)
            $error_info_list[]='File size is not correct';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$hash                 =$data['hash'];
        self::$file_mime_type       =$data['file_mime_type'];
        self::$file_content_type    =$data['file_content_type'];
        self::$chunk_uploaded       =$data['chunk_uploaded'];
        self::$date_create          =$data['date_create'];

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function set_file_ID(){

        if(empty(self::$file_ID))
            return self::add_file();

        return self::set_file_data();

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function set_dir(){

        self::set_dir_file();
        self::set_dir_file_chunk();
        self::set_file_chunk_path();

        if(self::is_last_chunk()){

            self::set_dir_file();
            self::set_file_path();

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws FileException
     * @throws ParametersException
     * @throws PhpException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \ImagickException
     */
    private static function set(){

        if(self::valid_file_mime_type())
            if(self::valid_file_content_type())
                if(self::valid_file_size())
                    if(self::valid_file_size())
                        if(self::valid_chunk_size())
                            if(self::valid_chunk_len())
                                if(self::valid_chunk_index())
                                    if(self::set_file_ID()){

                                        self::set_chunk_base64();

                                        self::set_dir();
                                        self::set_file_chunk();
                                        self::set_file_chunk_uploaded();

                                        self::set_prepare_concat_chunk_list();
                                        self::set_switch_prepare_content();

                                        return true;

                                    }

        return false;

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @param int|NULL $chunk_index
     * @param int|NULL $chunk_len
     * @param int|NULL $file_chunk_size
     * @param int|NULL $file_size
     * @param string|NULL $chunk_base64
     * @return array|null
     * @throws DbQueryException
     * @throws FileException
     * @throws ParametersException
     * @throws PhpException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \ImagickException
     */
    public  static function init(int $file_ID=NULL,string $file_mime_type=NULL,string $file_content_type=NULL,string $file_extension=NULL,int $chunk_index=NULL,int $chunk_len=NULL,int $file_chunk_size=NULL,int $file_size=NULL,string $chunk_base64=NULL){

        $error_info_list=[];

        if(is_null($chunk_index))
            $error_info_list[]='Chunk index is NULL';

        if(empty($file_mime_type))
            $error_info_list[]='File content type is empty';

        if(empty($file_content_type))
            $error_info_list[]='File type is empty';

        if(empty($chunk_len))
            $error_info_list[]='Chunk length is empty';

        if(empty($file_chunk_size))
            $error_info_list[]='Chunk size is empty';

        if(empty($file_size))
            $error_info_list[]='File size is empty';

        if(empty($file_extension))
            $error_info_list[]='File extension is empty';

        if(empty($chunk_base64))
            $error_info_list[]='Chunk part is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'file_ID'           =>$file_ID,
                    'chunk_index'       =>$chunk_index,
                    'chunk_len'         =>$chunk_len,
                    'chunk_size'        =>$file_chunk_size,
                    'file_size'         =>$file_size
                )
            );

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$file_ID                  =$file_ID;
        self::$file_mime_type           =$file_mime_type;
        self::$file_extension           =$file_extension;
        self::$file_content_type        =$file_content_type;
        self::$chunk_index              =$chunk_index;
        self::$chunk_len                =$chunk_len;
        self::$file_chunk_size          =$file_chunk_size;
        self::$file_size                =$file_size;
        self::$chunk_base64             =$chunk_base64;

        if(self::set()){

            $data=array(
                'file_ID'           =>self::$file_ID,
                'chunk_index'       =>self::$chunk_index,
                'chunk_uploaded'    =>self::$chunk_uploaded,
                'chunk_len'         =>self::$chunk_len,
                'complete'          =>self::$is_complete,
                'date_create'       =>self::$date_create
            );

            if(self::$is_complete){

                if(isset(self::$data['image_ID'])){

//                    $image_data=ImagePathAction::get_image_path_data(self::$data['image_ID']);
//
//                    $data['image_ID']               =self::$data['image_ID'];
//                    $data['image_item_ID_list']     =$image_data['image_item_ID_list'];
//                    $data['image_dir']              =self::$data['image_dir'];

                    $data['image_ID']               =self::$data['image_ID'];
                    $data['image_item_ID_list']     =self::$data['image_item_ID_list'];
                    $data['image_dir']              =self::$data['image_dir'];


                }
                else if(isset(self::$data['audio_ID'])){

                    $data['audio_ID']               =self::$data['audio_ID'];
                    $data['audio_item_ID_list']     =self::$data['audio_item_ID_list'];
                    $data['audio_dir']              =self::$data['audio_dir'];

                }
                else if(isset(self::$data['video_ID'])){

                    $data['video_ID']               =self::$data['video_ID'];
                    $data['video_path']             =self::$data['video_hash_path'];

                }

            }

            return $data;


        }

        return NULL;

    }

}