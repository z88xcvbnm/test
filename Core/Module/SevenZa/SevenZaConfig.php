<?php

namespace Core\Module\SevenZa;

use Core\Module\Exception\PhpException;
use Core\Module\OsServer\OsServer;
use Core\Module\OsServer\OsServerCommandValidation;

class SevenZaConfig{

    /** @var string */
    public  static $sevenza_command_path;

    /**
     * @return bool
     * @throws PhpException
     */
    public  static function set_sevenza_command_path(){

        if(OsServer::$is_linux)
            self::$sevenza_command_path='7za';
        else if(OsServer::$is_windows)
            self::$sevenza_command_path='C:\zip\7za.exe';
        else if(OsServer::$is_freebsd)
            self::$sevenza_command_path='7za';
        else{

            $error=array(
                'title'     =>'Server problem',
                'info'      =>'7za command path is not valid'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return string
     * @throws PhpException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function get_sevenza_command_path(){

        if(!empty(self::$sevenza_command_path))
            return self::$sevenza_command_path;

        self::set_unzip_command_path();

        if(!OsServerCommandValidation::isset_shell_command(self::$sevenza_command_path)){

            $error=array(
                'title'     =>'Php problem',
                'info'      =>'Command "'.self::$sevenza_command_path.'" is not exists'
            );

            throw new PhpException($error);

        }

        return self::$sevenza_command_path;

    }

}