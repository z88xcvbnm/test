<?php

namespace Core\Module\SevenZa;

use Core\Module\Date\Date;
use Core\Module\Dir\Dir;
use Core\Module\User\User;

class SevenZaLog{

    /** @var string */
    public  static $dir_log='Temp/7Za';

    /**
     * @return string
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_sevenza_log_path(){

        if(!file_exists(self::$dir_log))
            Dir::create_dir(self::$dir_log);

        $dir_date               =Date::get_date_path(self::$dir_log);
        $unzip_log_filename     =User::$user_ID.'_'.Date::get_timestamp().'.log';

        return $dir_date.'/'.$unzip_log_filename;

    }

}