<?php

namespace Core\Module\SevenZa;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Path\Path;

class SevenZaShell{

    /**
     * @param string|NULL $dir_for_packing
     * @param string|NULL $zip_path
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     */
    public  static function pack_dir_to_zip(string $dir_for_packing=NULL,string $zip_path=NULL){

        $error_info_list=[];

        if(empty($dir_for_packing))
            $error_info_list['dir']='Dir is empty';
        else if(!file_exists($zip_path))
            $error_info_list['dir']='Dir is not exists';

        if(empty($zip_path))
            $error_info_list['zip_path']='Zip path is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $sevenza_command_path   =SevenZaConfig::get_sevenza_command_path();
        $sevenza_log_path       =SevenZaLog::get_sevenza_log_path();

        if(empty($zip_command_path)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Zip command path is empty'
            );

            throw new PhpException($error);

        }
        
        $q=$sevenza_command_path.' a '.Path::$dir_root.'/'.$zip_path.' '.Path::$dir_root.'/'.$dir_for_packing.'/* > '.Path::$dir_root.'/'.$sevenza_log_path;

        exec($q);

        return true;

    }

}