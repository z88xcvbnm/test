<?php

namespace Core\Module\Ip;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Vpn\VpnDetermine;

class Ip{

    /** @var int */
    public  static $ip_ID;

    /** @var string */
    public  static $ip_address;

    /** @var int */
    public  static $ip_address_long;

    /** @var bool */
    public  static $is_vpn=false;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$ip_ID                =NULL;
        self::$ip_address           =NULL;
        self::$ip_address_long      =NULL;

        return true;

    }

    /**
     * @param int|NULL $ip_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_ip_ID(int $ip_ID=NULL){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($ip_ID,'_ip',0);

    }

    /**
     * @param string|NULL $ip_address
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_ip_address(string $ip_address=NULL){

        if(empty($ip_address)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'ip_address'=>$ip_address
        );

        return Db::isset_row('_ip',0,$where_list);

    }

    /**
     * @param int|NULL $ip_address_long
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_ip_address_long(int $ip_address_long=NULL){

        if(empty($ip_address_long)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address long is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'ip_long'=>$ip_address_long
        );

        return Db::isset_row('_ip',0,$where_list);

    }

    /**
     * @param string|NULL $ip_address
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_ip_ID_from_ip_address(string $ip_address=NULL){

        if(empty($ip_address)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'ip_address'=>$ip_address
        );

        return Db::get_row_ID('_ip',0,$where_list);

    }

    /**
     * @param string|NULL $ip_address
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_ip_data_from_ip_address(string $ip_address=NULL){

        if(empty($ip_address)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'vpn'
            ),
            'table'=>'_ip',
            'where'=>array(
                'ip_address'    =>$ip_address,
                'type'          =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'        =>$r[0]['id'],
            'is_vpn'    =>(bool)$r[0]['vpn']
        );

    }

    /**
     * @return int|mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_ip_ID_form_ip_address_default(){

        if(!empty(self::$ip_ID))
            return self::$ip_ID;

        if(empty(self::$ip_address)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default IP address is empty'
            );

            throw new ParametersException($error);

        }

        $data=self::get_ip_data_from_ip_address(self::$ip_address);

        if(empty($data)){

            self::$ip_ID=self::add_ip(self::$ip_address);

            VpnDetermine::init();

            if(self::$is_vpn)
                self::update_ip_vpn_status(self::$ip_ID,self::$is_vpn);

        }
        else{

            self::$ip_ID        =$data['ID'];
            self::$is_vpn       =$data['is_vpn'];

        }

        if(empty(self::$ip_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'IP address was not added'
            );

            throw new DbQueryException($error);

        }

        return self::$ip_ID;

    }

    /**
     * @param int|NULL $ip_ID
     * @param bool $is_vpn
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_ip_vpn_status(int $ip_ID=NULL,bool $is_vpn=false){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_ip',
            'set'=>array(
                'vpn'           =>(int)$is_vpn,
                'date_update'   =>'NOW()'
            ),
            'where'=>array(
                'id'    =>$ip_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'IP VPN status was updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $ip_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_ip_request_len(int $ip_ID=NULL){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_ip',
            'set'=>array(
                'request_len'   =>[
                    'function'=>'request_len+1'
                ],
                'date_update'   =>'NOW()'
            ),
            'where'=>array(
                'id'    =>$ip_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'IP VPN status was updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $ip_address
     * @param bool $is_vpn
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_ip(string $ip_address=NULL,bool $is_vpn=false){

        if(empty($ip_address)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address is empty'
            );

            throw new ParametersException($error);

        }

        $ip_address_long=IpConvert::get_ip_long($ip_address);

        $q=array(
            'table'     =>'_ip',
            'values'    =>array(
                'ip_address'    =>$ip_address,
                'ip_long'       =>empty($ip_address_long)?NULL:$ip_address_long,
                'vpn'           =>(int)$is_vpn,
                'date_create'   =>'NOW()'
            )
        );

        $conflict_list=[
            'column_list'=>[
                'ip_address'
            ],
            'update_list'=>[
                'set'=>[
                    'date_update'   =>'NOW()',
                    'type'          =>0
                ]
            ],
            'return_list'=>[
                'id'
            ]
        ];

        $r=Db::insert($q,true,[],NULL,$conflict_list);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'IP address was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $ip_address_long
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_ip_ID_form_ip_address_long(int $ip_address_long=NULL){

        if(empty($ip_address_long)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address long is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'ip_long'=>$ip_address_long
        );

        return Db::get_row_ID('_ip',0,$where_list);

    }

    /**
     * @param int|NULL $ip_ID
     * @return string|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_ip_address(int $ip_ID=NULL){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'ip_address'
            ),
            'table'=>'_ip',
            'where'=>array(
                'id'    =>$ip_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['ip_address'];

    }

    /**
     * @param int|NULL $ip_ID
     * @return string|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_ip_address_long(int $ip_ID=NULL){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'ip_long'
            ),
            'table'=>'_ip',
            'where'=>array(
                'id'    =>$ip_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['ip_long'];

    }

    /**
     * @param int|NULL $ip_ID
     * @param bool $need_remove_ip_ban
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_ip_ID(int $ip_ID=NULL,bool $need_remove_ip_ban=true){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_ip_ban)
            if(!IpBan::remove_ip_ban($ip_ID)){

                $error=array(
                    'title'     =>'DB query problem',
                    'info'      =>'IP ban was not removed'
                );

                throw new DbQueryException($error);

            }

        if(!Db::delete_from_ID($ip_ID,'_ip',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'IP was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $ip_address
     * @param bool $need_remove_ip_ban
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_ip_address(string $ip_address=NULL,bool $need_remove_ip_ban=true){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_ip_ban){

            $ip_ID=self::get_ip_ID_from_ip_address($ip_address);

            if(!empty($ip_ID))
                if(!IpBan::remove_ip_ban($ip_ID)){

                    $error=array(
                        'title'     =>'DB query problem',
                        'info'      =>'IP ban was not removed'
                    );

                    throw new DbQueryException($error);

                }

        }

        $where_list=array(
            'ip_address'=>$ip_address
        );

        if(!Db::delete_from_where_list('_ip',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'IP was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $ip_address_long
     * @param bool $need_remove_ip_ban
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_ip_address_long(int $ip_address_long=NULL,bool $need_remove_ip_ban=true){

        if(empty($ip_address_long)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address long is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_ip_ban){

            $ip_ID=self::get_ip_ID_form_ip_address_long($ip_address_long);

            if(!empty($ip_ID))
                if(!IpBan::remove_ip_ban($ip_ID)){

                    $error=array(
                        'title'     =>'DB query problem',
                        'info'      =>'IP ban was not removed'
                    );

                    throw new DbQueryException($error);

                }

        }

        $where_list=array(
            'ip_long'=>$ip_address_long
        );

        if(!Db::delete_from_where_list('_ip',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'IP address long was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $ip_ID
     */
    public  static function set_ip_ID_default(int $ip_ID=NULL){
        
        self::$ip_ID=empty($ip_ID)?NULL:$ip_ID;
        
    }

    /**
     * @param string|NULL $ip_address
     */
    public  static function set_ip_address_default(string $ip_address=NULL){
        
        self::$ip_address=empty($ip_address)?NULL:$ip_address;
        
    }

    /**
     * @param int|NULL $ip_address_long
     */
    public  static function set_ip_address_long_default(int $ip_address_long=NULL){
        
        self::$ip_address_long=empty($ip_address_long)?NULL:$ip_address_long;
        
    }

}