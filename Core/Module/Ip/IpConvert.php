<?php

namespace Core\Module\Ip;

use Core\Module\Exception\ParametersException;

class IpConvert{

    /**
     * @param string|NULL $cidr
     * @return array
     * @throws ParametersException
     */
    public  static function get_cidr_to_ip_range(string $cidr=NULL){

        if(empty($cidr)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'CIDR is empty'
            );

            throw new ParametersException($error);

        }

        $start  =strtok($cidr,"/");
        $n      =3-substr_count($cidr,".");

        if($n>0)
            for($i=$n;$i>0;$i--)
                $start.= ".0";

        $bits1      =str_pad(decbin(ip2long($start)),32,"0",STR_PAD_LEFT);
        $cidr       =(1<<(32-substr(strstr($cidr,"/"),1)))-1;
        $bits2      =str_pad(decbin($cidr),32,"0",STR_PAD_LEFT);
        $final      ="";

        for($i=0;$i<32;$i++){

            if($bits1[$i]==$bits2[$i])
                $final.=$bits1[$i];

            if($bits1[$i]==1&&$bits2[$i]==0)
                $final.=$bits1[$i];

            if($bits1[$i]==0&&$bits2[$i]==1)
                $final.=$bits2[$i];

        }

        return array($start,long2ip(bindec($final)));

    }

    /**
     * @param string|NULL $cidr
     * @return null|string
     * @throws ParametersException
     */
    public  static function get_netmask_from_cidr(string $cidr=NULL){

        if(empty($cidr)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'CIDR is empty'
            );

            throw new ParametersException($error);

        }

        $bin='';

        for($i=1;$i<33;$i++)
            $bin.=$cidr>=$i?'1':'0';

        $netmask=long2ip(bindec($bin));

        if($netmask=='0.0.0.0')
            return NULL;

        return $netmask;

    }

    /**
     * @param string|NULL $ip
     * @param string|NULL $cidr
     * @return string
     * @throws ParametersException
     */
    public  static function get_network_from_cidr(string $ip=NULL,string $cidr=NULL){

        if(empty($ip)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP is empty'
            );

            throw new ParametersException($error);

        }

        if(empty($cidr)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'CIDR is empty'
            );

            throw new ParametersException($error);

        }

        return long2ip((ip2long($ip))&((-1<<(32-(int)$cidr))));

    }

    /**
     * @param string|NULL $netmask
     * @return int
     * @throws ParametersException
     */
    public  static function get_cidr_from_netmask(string $netmask=NULL){

        if(empty($netmask)){

            $error=array(
                'title'     =>'NETMASK problems',
                'info'      =>'NETMASK is empty'
            );

            throw new ParametersException($error);

        }

        $bits       =0;
        $netmask    =explode(".", $netmask);

        foreach($netmask as $octect)
            $bits+=strlen(str_replace('0','',decbin($octect)));

        return $bits;

    }

    /**
     * @param string|NULL $ip_address
     * @return int
     */
    public  static function get_ip_long(string $ip_address=NULL){

        return ip2long($ip_address);

    }

}