<?php

namespace Core\Module\Ip;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;

class IpBan{

    /** @var int */
    public static $ip_ban_ID;

    /** @var string */
    public static $ip_ban_info;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$ip_ban_ID        =NULL;
        self::$ip_ban_info      =NULL;

        return true;

    }

    /**
     * @param int|NULL $ip_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_ip_ban(int $ip_ID=NULL){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'ip_id'=>$ip_ID
        );

        return Db::isset_row('_ip_ban',0,$where_list);

    }

    /**
     * @param int|NULL $ip_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_ip_ban_ID(int $ip_ID=NULL){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'ip_id'=>$ip_ID
        );

        return Db::get_row_ID('_ip_ban',0,$where_list);

    }

    /**
     * @param int|NULL $ip_ban_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_ip_ban_info_from_ip_ban_ID(int $ip_ban_ID=NULL){

        if(empty($ip_ban_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ban ID is empty'
            );

            throw new ParametersException($error);

        }

        $select_list=array(
            'ip_id',
            'user_id',
            'info'
        );

        $r=Db::get_data_from_ID($ip_ban_ID,'_ip_ban',$select_list,0);

        if(empty($r))
            return NULL;

        return array(
            'ip_ID'     =>$r['ip_id'],
            'user_ID'   =>$r['user_id'],
            'info'      =>$r['info']
        );

    }

    /**
     * @param int|NULL $ip_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_ip_ban_info_from_ip_ID(int $ip_ID=NULL){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'user_id',
                'info'
            ),
            'table'=>'_ip_ban',
            'where'=>array(
                'ip_id'     =>$ip_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'        =>$r[0]['id'],
            'user_ID'   =>$r[0]['user_id'],
            'info'      =>$r[0]['info']
        );

    }

    /**
     * @param int|NULL $ip_ban_ID
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_ip_ban_ID(int $ip_ban_ID=NULL){

        if(empty($ip_ban_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ban ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::delete_from_ID($ip_ban_ID,'_ip_ban',0);

    }

    /**
     * @param int|NULL $ip_ID
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_ip_ban(int $ip_ID=NULL){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'ip_id'=>$ip_ID
        );

        return Db::delete_from_where_list('_ip_ban',0,$where_list);

    }

    /**
     * @param string|NULL $ip_address
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_ip_ban_from_ip_address(string $ip_address=NULL){

        if(empty($ip_address)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address is empty'
            );

            throw new ParametersException($error);

        }

        $ip_ID=Ip::get_ip_ID_from_ip_address($ip_address);

        if(empty($ip_ID))
            return false;

        $where_list=array(
            'ip_id'=>$ip_ID
        );

        return Db::delete_from_where_list('_ip_ban',0,$where_list);

    }

    /**
     * @param int|NULL $ip_ID
     * @param string|NULL $info
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_ip_ban(int $ip_ID=NULL,string $info=NULL){

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_ip_ban',
            'values'    =>array(
                'user_id'       =>User::$user_ID,
                'ip_id'         =>$ip_ID,
                'info'          =>$info,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'IP ban was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param string|NULL $ip_address
     * @param string|NULL $info
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_ip_ban_from_ip_address(string $ip_address=NULL,string $info=NULL){

        if(empty($ip_address)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address is empty'
            );

            throw new ParametersException($error);

        }

        $ip_ID=Ip::get_ip_ID_from_ip_address($ip_address);

        if(empty($ip_ID))
            $ip_ID=Ip::add_ip_address($ip_address);

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'IP was not added'
            );

            throw new DbQueryException($error);

        }

        return self::add_ip_ban($ip_ID,$info);

    }


}