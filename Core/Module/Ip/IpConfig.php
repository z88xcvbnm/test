<?php

namespace Core\Module\Ip;

use Core\Module\Exception\ParametersException;

class IpConfig{

    /** @var array */
    public  static $ip_list=array(
        '37.9.129.6'
    );

    /** @var bool */
    public  static $access_for_vpn=true;

    /**
     * @param int $index
     * @return string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_ip_localhost(int $index=0){

        if(count(self::$ip_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP list is empty'
            );

            throw new ParametersException($error);

        }

        if(!isset(self::$ip_list[$index]))
            $index=0;

        return self::$ip_list[$index];

    }

}