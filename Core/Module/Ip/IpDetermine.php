<?php

namespace Core\Module\Ip;

use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ForbiddenException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Vpn\VpnDetermine;
use Core\Module\Worktime\Worktime;
use MongoDB\Driver\Exception\ServerException;

class IpDetermine{

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_vpn_determine(){

        return VpnDetermine::init();

    }

    /**
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_ip_address_to_default(){

        if(empty($_SERVER['REMOTE_ADDR'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Client IP address is empty'
            );

            throw new ParametersException($error);

        }

        $ip_address=$_SERVER['REMOTE_ADDR'];

        if($_SERVER['REMOTE_ADDR']=='127.0.0.1')
            $ip_address=IpConfig::get_ip_localhost();

        if(!empty($_POST['ip_address']))
            $ip_address=$_POST['ip_address'];

        Ip::set_ip_address_default($ip_address);

    }

    /**
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_ip_address_long_to_default(){

        if(empty($_SERVER['REMOTE_ADDR'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Client IP address is empty'
            );

            throw new ParametersException($error);

        }

        $ip_address_long=IpConvert::get_ip_long($_SERVER['REMOTE_ADDR']);

        Ip::set_ip_address_long_default($ip_address_long);

    }

    /**
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_ip_ID_to_default(){

        $ip_ID=Ip::get_ip_ID_form_ip_address_default();

        if(empty($ip_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'IP address was not checked or added'
            );

            throw new DbQueryException($error);

        }

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_client_ip_ID_default_in_ip_ban(){

        if(empty(Ip::$ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default IP ID is empty'
            );

            throw new ParametersException($error);

        }

        if(IpBan::isset_ip_ban(Ip::$ip_ID)){

            $error=[
                'title'     =>'500 Internal Server Error',
                'info'      =>'Please come back later'
            ];

            throw new ServerException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ForbiddenException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_access_vpn(){

        if(!IpConfig::$access_for_vpn)
            if(Ip::$is_vpn){

                $error=array(
                    'title'     =>'Access denied',
                    'info'      =>'Please switch off VPN'
                );

                throw new ForbiddenException($error);

            }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_ip_request_len(){

        if(!Ip::update_ip_request_len(Ip::$ip_ID)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'IP was not update'
            ];

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @throws DbQueryException
     * @throws ForbiddenException
     * @throws ParametersException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_client_ip_address_to_default();

        self::set_client_ip_address_long_to_default();
        self::set_client_ip_ID_to_default();
        self::check_client_ip_ID_default_in_ip_ban();
        self::check_access_vpn();
        self::update_ip_request_len();

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ForbiddenException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Worktime::set_timestamp_point('IP Determine Start');

        self::set();

        Worktime::set_timestamp_point('IP Determine Finish');

        return true;

    }

}