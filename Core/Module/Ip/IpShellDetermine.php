<?php

namespace Core\Module\Ip;

use Core\Module\Url\Url;

class IpShellDetermine{

    /**
     * @return bool
     */
    private static function set_ip(){

        $ip_address=gethostbyname(Url::$host);

        Ip::set_ip_address_default($ip_address);

        return true;

    }

    /**
     * @return bool
     */
    private static function set(){

        self::set_ip();

        return true;

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}