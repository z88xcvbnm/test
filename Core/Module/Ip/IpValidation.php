<?php

namespace Core\Module\Ip;

use Core\Module\Exception\ParametersException;

class IpValidation{

    /**
     * @param string|NULL $ip
     * @return bool
     * @throws ParametersException
     */
    public  static function valid_ipv6(string $ip=NULL){

        if(empty($ip)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP is empty'
            );

            throw new ParametersException($error);

        }

        return (bool)filter_var($ip,FILTER_VALIDATE_IP,FILTER_FLAG_IPV6);

    }

    /**
     * @param string|NULL $ip
     * @return bool
     * @throws ParametersException
     */
    public  static function valid_ipv4(string $ip=NULL){

        if(empty($ip)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP is empty'
            );

            throw new ParametersException($error);

        }

        return (bool)filter_var($ip,FILTER_VALIDATE_IP,FILTER_FLAG_IPV4);

    }

}