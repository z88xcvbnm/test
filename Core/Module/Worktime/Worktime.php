<?php

namespace Core\Module\Worktime;

use Core\Module\Date\Date;

class Worktime{

    /** @var double */
    public  static $timestamp_start;

    /** @var double */
    public  static $timestamp_finish;

    /** @var array */
    public  static $timestamp_list=[];

    /** @var double */
    public  static $delta;

    /** @var bool */
    private static $is_finish=false;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$timestamp_start          =NULL;
        self::$timestamp_finish         =NULL;
        self::$timestamp_list           =[];
        self::$delta                    =NULL;
        self::$is_finish                =false;

        return true;

    }

    /**
     * @return array
     */
    public  static function get_full_info(){

        $data=array(
            'delta'=>self::$delta
        );

        if(WorktimeConfig::$need_worktime_list)
            $data['list']=self::$timestamp_list;

        return $data;

    }
    /**
     * @return double
     */
    public  static function get_delta(){

        if(self::$is_finish)
            return self::$delta;
        else{

            $timestamp_now=Date::get_timestamp_ms();

            return $timestamp_now-self::$timestamp_start;

        }

    }

    /**
     * @return bool
     */
    public  static function start(){

        self::$is_finish            =false;
        self::$timestamp_start      =Date::get_timestamp_ms();

        self::set_timestamp_point('Start timer');

        return true;

    }
    public  static function finish(){

        self::$is_finish            =true;
        self::$timestamp_finish     =Date::get_timestamp_ms();

        self::set_timestamp_point('Finish timer');
        self::set_delta();

    }

    /**
     * @param null $info
     * @return bool
     */
    public  static function set_timestamp_point($info=NULL){

        $timestamp_last     =count(self::$timestamp_list)==0?0:self::$timestamp_list[count(self::$timestamp_list)-1]['timestamp'];
        $timestamp_now      =Date::get_timestamp_ms();

        $temp=array(
            'delta'         =>0,
            'timestamp'     =>$timestamp_now
        );

        if(!empty($timestamp_last)){

            $temp['delta']          =$timestamp_now-$timestamp_last;
            $temp['delta_round']    =round($timestamp_now-$timestamp_last,5)*1000;

        }

        if(!empty($info))
            $temp['info']=$info;

        self::$timestamp_list[]=$temp;

        return true;

    }

    /**
     * @return bool
     */
    private static function set_delta(){

        if(!empty(self::$timestamp_finish)&&!empty(self::$timestamp_start))
            self::$delta=round(self::$timestamp_finish-self::$timestamp_start,WorktimeConfig::$round_length);

        return true;

    }

    /**
     * @return bool
     */
    private static function set(){

        return self::start();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}