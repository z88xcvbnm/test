<?php

namespace Core\Module\Worktime;

class WorktimeConfig{

    /** @var int */
    public  static $round_length            =3;

    /** @var bool */
    public  static $need_worktime_list      =false;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$round_length             =5;
        self::$need_worktime_list       =false;

        return true;

    }

}