<?php

namespace Core\Module\Data;

class Data{

    /**
     * @param null $data
     * @return bool
     */
    public  static function is_string($data=NULL){

        if(empty($data))
            return false;

        return is_string($data);

    }

    /**
     * @param null $data
     * @return bool
     */
    public  static function is_integer($data=NULL){

        if(is_null($data))
            return false;

        return is_integer($data);

    }

    /**
     * @param null $data
     * @return bool
     */
    public  static function is_float($data=NULL){

        if(is_null($data))
            return false;

        return is_float($data);

    }

    /**
     * @param string|NULL $word
     * @param string|NULL $string
     * @return bool
     */
    public  static function isset_word_in_string(string $word=NULL,string $string=NULL){

        if(empty($word)||empty($string))
            return false;

        return strpos($string,$word)!==false;

    }

    /**
     * @param null $data
     * @return bool
     */
    public  static function is_array($data=NULL){

        if(is_null($data))
            return false;

        return is_array($data);

    }

    /**
     * @param null $data
     * @return bool
     */
    public  static function is_associative_array($data=NULL){

        if(is_null($data))
            return false;

        if(!is_array($data))
            return false;

        return array_keys($data)!==range(0,count($data)-1);

    }

}