<?php

namespace Core\Module\History;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Error\CustomizeError;
use Core\Module\Exception\CustomizeException;
use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;
use Core\Module\Log\Log;
use Core\Module\Php\PhpValidation;
use Core\Module\Response\Response;
use Core\Module\Session\Session;
use Core\Module\Token\Token;
use Core\Module\Exception\DbQueryException;
use Core\Module\User\User;
use Core\Module\Worktime\Worktime;

class History{

    /** @var int */
    private static $history_ID;

    /** @var string */
    private static $header;

    /** @var string */
    private static $cookie_list;

    /** @var string */
    private static $get_list;

    /** @var string */
    private static $post_list;

    /** @var string */
    private static $response_list;

    /** @var array */
    private static $error;

    /** @var bool */
    private static $is_error            =false;

    /** @var bool */
    private static $is_success          =false;

    /** @var bool */
    private static $is_shell            =false;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$history_ID               =NULL;
        self::$header                   =NULL;
        self::$cookie_list              =NULL;
        self::$get_list                 =NULL;
        self::$post_list                =NULL;
        self::$response_list            =NULL;
        self::$error                    =NULL;
        self::$is_error                 =false;
        self::$is_success               =false;
        self::$is_shell                 =false;

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_history(){

        $q=array(
            'table'     =>'_history',
            'values'    =>array(
                array(
                    'user_id'       =>User::$user_ID,
                    'token_id'      =>Token::$token_ID,
                    'session_id'    =>Session::$session_ID,
                    'is_shell'      =>(int)self::$is_shell,
                    'get'           =>self::$get_list,
                    'post'          =>self::$post_list,
                    'header'        =>self::$header,
                    'date_create'   =>'NOW()'
                )
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'User history DB Problem',
                'info'      =>'User history was not added'
            );

            throw new DbQueryException($error);

        }

        self::$history_ID=(int)$r[0]['id'];

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_header(){

        self::$header=Json::encode($_SERVER);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_cookie_list(){

        self::$cookie_list=Json::encode($_COOKIE);

        return true;

    }

    /**
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_get_list(){

        self::$get_list=Json::encode($_GET);

    }

    /**
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_post_list(){

        self::$post_list=Json::encode($_POST);

    }

    /**
     * @return bool
     */
    private static function set_response_list(){

        self::$response_list=NULL;

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_error(){

        self::$is_error=CustomizeError::$is_error||CustomizeException::$is_exception||Log::$is_error;

        if(self::$is_error)
            self::$error=Log::$file_path;

        self::$response_list=Json::encode(empty(Log::$content)?[]:Log::$content);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_success(){

        self::$is_success       =1;
        self::$response_list    =Json::encode(empty(Response::$data)?[]:Response::$data);

        return true;

    }

    /**
     * @return bool
     */
    private static function set_shell(){

        self::$is_shell=PhpValidation::is_console_running();

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_header();
        self::set_post_list();
        self::set_get_list();
        self::set_cookie_list();
        self::set_response_list();
        self::set_shell();

        return self::add_history();

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token(){

        if(empty(self::$history_ID))
            return false;

        if(empty(Token::$token_ID))
            return false;

        $q=array(
            'table'=>'_history',
            'set'=>array(
                'user_id'       =>User::$user_ID,
                'token_id'      =>Token::$token_ID,
                'session_id'    =>Session::$session_ID,
                'date_update'   =>'NOW()'
            ),
            'where'=>array(
                'id'    =>self::$history_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'History was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_error(){

        if(empty(self::$history_ID))
            return false;

        self::set_error();

        $q=array(
            'table'=>'_history',
            'set'=>array(
                'user_id'       =>User::$user_ID,
                'token_id'      =>Token::$token_ID,
                'session_id'    =>Session::$session_ID,
                'is_error'      =>(int)self::$is_error,
                'response'      =>self::$response_list,
                'error'         =>self::$error,
                'worktime'      =>Worktime::get_delta(),
                'date_update'   =>'NOW()'
            ),
            'where'=>array(
                'id'    =>self::$history_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'History was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_success(){

        if(empty(self::$history_ID))
            return false;

        self::set_success();

        $q=array(
            'table'=>'_history',
            'set'=>array(
                'user_id'       =>User::$user_ID,
                'token_id'      =>Token::$token_ID,
                'session_id'    =>Session::$session_ID,
                'is_success'    =>(int)self::$is_success,
                'response'      =>self::$response_list,
                'worktime'      =>Worktime::get_delta(),
                'date_update'   =>'NOW()'
            ),
            'where'=>array(
                'id'    =>self::$history_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'History was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $timestamp_from
     * @param int|NULL $timestamp_to
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function drop_history(int $timestamp_from=NULL,int $timestamp_to=NULL){
        $where_list=[];

        if(!empty($timestamp_from))
            $where_list[]=array(
                'column'    =>'date_create',
                'method'    =>'>=',
                'value'     =>Db::is_postgresql()?Date::get_date_time_full($timestamp_from):$timestamp_from
            );

        if(!empty($timestamp_to))
            $where_list[]=array(
                'column'    =>'date_create',
                'method'    =>'<=',
                'value'     =>Db::is_postgresql()?Date::get_date_time_full($timestamp_to):$timestamp_to
            );

        if(count($where_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $data=array(
            'table'     =>'_history',
            'where'     =>$where_list
        );

        if(!Db::delete($data)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'History was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        self::reset_data();

        return self::set();

    }

}