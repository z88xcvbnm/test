<?php

namespace Core\Module\Currency;

class Currency{

    /** @var string */
    private static $sberbank_link               ='http://www.cbr.ru/scripts/XML_daily.asp?date_req=';

    /** @var array */
    public  static $currency_list               =[];

    /** @var string */
    public  static $currency_key_default        ='RUB';

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$currency_list            =[];
        self::$currency_key_default     ='RUS';

        return true;

    }

    /**
     * @param string|NULL $date
     * @return false|string
     */
    private static function get_date(string $date=NULL){

        return empty($date)?date("Y-m-d"):date("Y-m-d",strtotime($date));

    }

    /**
     * @param string|NULL $date
     * @param string|NULL $currency_key
     * @return int
     */
    public  static function get_currency_rate(string $date=NULL,string $currency_key=NULL){

        if(empty($currency_key))
            return 0;

        $currency_key=strtoupper($currency_key);

        if($currency_key==self::$currency_key_default)
            return 1;

        $date           =self::get_date($date);
        $currency_list  =self::get_currency_list($date);

        return isset($currency_list[$currency_key])?$currency_list[$currency_key]:0;

    }

    /**
     * @param string|NULL $date
     * @return array
     */
    public  static function get_currency_list(string $date=NULL){

        $date=self::get_date($date);

        if(isset(self::$currency_list[$date]))
            return self::$currency_list[$date];

        self::set_currency_list($date);

        return isset(self::$currency_list[$date])?self::$currency_list[$date]:[];

    }

    /**
     * @param string|NULL $date
     * @return bool
     */
    private static function set_currency_list(string $date=NULL){

        $date=self::get_date($date);

        if(!isset(self::$currency_list[$date])){

            $r=file_get_contents(self::$sberbank_link.date("d/m/Y",strtotime($date)));

            if(empty($r))
                return false;

            $pattern="#<Valute ID=\"([^\"]+)[^>]+>[^>]+>([^<]+)[^>]+>[^>]+>[^>]+>[^>]+>[^>]+>[^>]+>([^<]+)[^>]+>[^>]+>([^<]+)#i";

            preg_match_all($pattern,$r,$out,PREG_SET_ORDER);

            if(count($out[1])==0)
                return false;

            $temp_list=[];

            foreach($out as $cur){

                preg_match_all('/<CharCode>(.*)<\/CharCode>/i',$cur[0],$current_list);
                preg_match_all('/<Nominal>(.*)<\/Nominal>/i',$cur[0],$nominal_list);

                if(count($current_list[1])>0&&count($nominal_list)>0){

                    $currency_key       =$current_list[1][0];
                    $currency_nominal   =$nominal_list[1][0];
                    $currency_rate      =(double)(str_replace(",",".",$cur[4])/$currency_nominal);

                    $temp_list[$currency_key]=$currency_rate;

                }

            }

            if(count($temp_list)==0)
                return false;

            self::$currency_list[$date]=$temp_list;

            return true;

        }

        return false;

    }


    /**
     * @param string|NULL $date
     * @return bool
     */
    public  static function init(string $date=NULL){

        self::set_currency_list($date);

        return true;

    }

}