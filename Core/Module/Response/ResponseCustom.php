<?php

namespace Core\Module\Response;

use Core\Module\Header\HeaderCode;
use Core\Module\Worktime\Worktime;

class ResponseCustom extends Response{

    /**
     * @return bool
     */
    private static function set_data(){

        self::$data['worktime']=Worktime::get_delta();

        static::$data=self::$data;

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_header_status(){

        return HeaderCode::init(200);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_header_status();

        self::set_data();

        return self::give_response();

    }

    /**
     * @param array|NULL $data
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(array $data=NULL){

        if(!empty($data))
            self::$data=$data;

        return self::set();

    }

}