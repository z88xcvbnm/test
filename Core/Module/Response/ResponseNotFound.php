<?php

namespace Core\Module\Response;

use Core\Content\Error\ErrorContent;
use Core\Module\Header\HeaderCode;
use Core\Module\Worktime\Worktime;

class ResponseNotFound extends Response{

    /**
     * @return bool
     */
    private static function set_data(){

        static::$data=array(
            'error'     =>ErrorContent::get_error('not_found',self::$data),
            'worktime'  =>Worktime::get_delta()
        );

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function set_header_status(){

        return HeaderCode::init(404);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function set(){

        self::set_header_status();
        self::set_data();

        return self::give_response();

    }

    /**
     * @param array|NULL $data
     * @return bool
     */
    public  static function init(array $data=NULL){

        if(!empty($data))
            self::$data=$data;

        return self::set();

    }

}