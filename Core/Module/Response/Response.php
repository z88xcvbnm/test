<?php

namespace Core\Module\Response;

use Core\Module\Exception\ParametersException;
use Core\Module\Header\HeaderCode;
use Core\Module\Header\HeaderContentType;
use Core\Module\Json\Json;
use Core\Module\Worktime\Worktime;

class Response{

    /** @var string */
    public  static $type='page';

    /** @var array */
    public  static $data=[];

    /** @var bool */
    public  static $is_already_send             =false;

    /** @var bool */
    public  static $is_already_send_parent      =false;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$type                 ='page';
        self::$data                 =[];
        self::$is_already_send      =false;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function give_response(){

        if(self::$is_already_send_parent)
            return false;

        self::$is_already_send_parent=true;

        if(empty(self::$type)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Response type is empty'
            );

            HeaderCode::init(502);

            throw new ParametersException($error);

        }

        Worktime::finish();

        switch(self::$type){

            case'page':
            case'json':{

                HeaderContentType::init('application/json');

                if(isset($_SERVER['HTTP_HOST'])&&\Config::$is_debug)
                    self::$data['host']=$_SERVER['HTTP_HOST'];

                self::$data['worktime']=Worktime::get_delta();

                echo Json::encode(self::$data);

                break;

            }

//            case'page':{
//
//                self::$data['worktime']=Worktime::get_delta();
//
//                echo '<pre>'.print_r(self::$data,true).'</pre>';
//
//                break;
//
//            }

            case'shell':{

                self::$data['worktime']=Worktime::get_delta();

                print_r(self::$data);

                break;

            }

        }

        return true;

    }

}