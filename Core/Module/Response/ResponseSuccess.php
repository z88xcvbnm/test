<?php

namespace Core\Module\Response;

use Core\Module\Header\HeaderCode;
use Core\Module\History\History;
use Core\Module\Php\PhpValidation;
use Core\Module\Token\Token;

class ResponseSuccess extends Response{

    /**
     * @return bool
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function set_header_status(){

        if(!PhpValidation::is_console_running())
            return HeaderCode::init(200);

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_header_status();

        self::give_response();

        return History::update_success();

    }

    /**
     * @param array $data
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(array $data=[]){

        self::$data             =$data;
        self::$data['token']    =Token::$token_hash;

        return self::set();

    }

}