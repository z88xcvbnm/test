<?php

namespace Core\Module\Audio;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;

class AudioItem{

    /**
     * @param int|NULL $audio_ID
     * @param bool $is_file_extension
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_item_ID_list(int $audio_ID=NULL,bool $is_file_extension=false){

        if(empty($audio_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_audio_item',
            'where'=>array(
                'audio_id'  =>$audio_ID,
                'type'      =>0
            )
        );

        if($is_file_extension)
            $q['select'][]='file_extension';

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['id'].($is_file_extension?('.'.$row['file_extension']):'');

        return $list;

    }

    /**
     * @param array $audio_ID_list
     * @param bool $is_file_extension
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_item_ID_list_from_audio_ID_list(array $audio_ID_list=[],bool $is_file_extension=false){

        if(count($audio_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'audio_id',
            ),
            'table'=>'_audio_item',
            'where'=>array(
                'audio_id'  =>$audio_ID_list,
                'type'      =>0
            )
        );

        if($is_file_extension)
            $q['select'][]='file_extension';

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row){

            if(!isset($list[$row['audio_id']]))
                $list[$row['audio_id']]=[];

            $list[$row['audio_id']][]=$row['id'].($is_file_extension?('.'.$row['file_extension']):'');

        }

        return $list;

    }

    /**
     * @param array $audio_ID_list
     * @param bool $is_assoc
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_item_list(array $audio_ID_list=[],bool $is_assoc=false){

        if(count($audio_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        $ID_list=[];

        foreach($audio_ID_list as $audio_ID)
            $ID_list[]=$audio_ID;

        if(count($ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'audio_id'
            ),
            'table'=>'_audio_item',
            'where'=>array(
                'audio_id'  =>$ID_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        if($is_assoc)
            foreach($r as $row){

                if(!isset($list[$row['audio_id']]))
                    $list[$row['audio_id']]=[];

                $list[$row['audio_id']][]=$row['id'];

            }
        else
            foreach($r as $row)
                $list[]=$row['id'];

        return $list;

    }

    /**
     * @param int|NULL $audio_item_ID
     * @param string|NULL $hash
     * @param int|NULL $duration
     * @param int|NULL $bitrate
     * @param int|NULL $hz
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_audio_item_ID_prepared(int $audio_item_ID=NULL,string $hash=NULL,int $duration=NULL,int $bitrate=NULL,int $hz=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL){

        $error_info_list=[];

        if(empty($audio_item_ID))
            $error_info_list['audio_item_ID']='Audio item ID is empty';

        if(empty($hash))
            $error_info_list['hash']='Hash is empty';

        if(empty($duration))
            $error_info_list['duration']='Duration is empty';

        if(empty($bitrate))
            $error_info_list['bitrate']='Bitrate is empty';

        if(empty($hz))
            $error_info_list['hz']='HZ is empty';

        if(empty($file_size))
            $error_info_list['file_size']='HZ is empty';

        if(empty($file_mime_type))
            $error_info_list['file_mime_type']='File mime type is empty';

        if(empty($file_extension))
            $error_info_list['file_extension']='File extension is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_audio_item',
            'set'=>array(
                'hash'                  =>$hash,
                'duration'              =>$duration,
                'bitrate'               =>$bitrate,
                'hertz'                 =>$hz,
                'file_size'             =>$file_size,
                'file_mime_type'        =>$file_mime_type,
                'file_extension'        =>$file_extension,
                'preparing'             =>0,
                'prepared'              =>1,
                'public'                =>1,
                'hide'                  =>0,
                'date_update'           =>'NOW()',
                'type'                  =>0
            ),
            'where'=>array(
                'id'    =>$audio_item_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID is empty'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $file_ID
     * @param int|NULL $audio_ID
     * @param string|NULL $hash
     * @param int|NULL $duration
     * @param int|NULL $bitrate
     * @param int|NULL $hz
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param bool $preparing
     * @param bool $prepared
     * @param bool $public
     * @param bool $hide
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_audio_item(int $file_ID=NULL,int $audio_ID=NULL,string $hash=NULL,int $duration=NULL,int $bitrate=NULL,int $hz=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL,bool $preparing=false,bool $prepared=false,bool $public=false,bool $hide=false){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($audio_ID))
            $error_info_list[]='Audio ID is empty';

        if(empty($bitrate))
            $error_info_list[]='Bitrate is empty';

        if(empty($hz))
            $error_info_list[]='HZ is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_audio_item',
            'values'    =>array(
                'user_id'       =>User::$user_ID,
                'file_id'       =>$file_ID,
                'audio_id'      =>$audio_ID,
                'hash'          =>$hash,
                'duration'      =>$duration,
                'bitrate'       =>$bitrate,
                'hertz'         =>$hz,
                'file_size'     =>$file_size,
                'file_mime_type'=>$file_mime_type,
                'file_extension'=>$file_extension,
                'preparing'     =>(int)$preparing,
                'prepared'      =>(int)$prepared,
                'public'        =>(int)$public,
                'hide'          =>(int)$hide,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>0
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Audio item was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $file_ID
     * @param int|NULL $audio_ID
     * @param int|NULL $bitrate
     * @param int|NULL $hz
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_audio_item_preparing(int $file_ID=NULL,int $audio_ID=NULL,int $bitrate=NULL,int $hz=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($audio_ID))
            $error_info_list[]='Audio ID is empty';

        if(empty($bitrate))
            $error_info_list[]='Bitrate is empty';

        if(empty($hz))
            $error_info_list[]='HZ is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        return self::add_audio_item($file_ID,$audio_ID,NULL,NULL,$bitrate,$hz,NULL,NULL,NULL,true,false,false,true);

    }

    /**
     * @param int|NULL $audio_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_audio_item(int $audio_ID=NULL){

        if(empty($audio_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'audio_id'=>$audio_ID
        );

        if(!Db::delete_from_where_list('_audio_item',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Audio item was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param array $audio_item_ID_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_audio_item_ID_list(array $audio_item_ID_list=[]){

        if(count($audio_item_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio item ID list is empty'
            );

            throw new ParametersException($error);

        }

        $ID_list=[];

        foreach($audio_item_ID_list as $audio_item_ID)
            if(!empty($audio_item_ID))
                $ID_list[]=$audio_item_ID;

        if(count($ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID list is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID_list($ID_list,'_audio_item',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Audio item was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param array $audio_ID_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_audio_item_list(array $audio_ID_list=[]){

        if(count($audio_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID list is empty'
            );

            throw new ParametersException($error);

        }

        $ID_list=[];

        foreach($audio_ID_list as $audio_ID)
            if(!empty($audio_ID))
                $ID_list[]=$audio_ID;

        if(count($ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID list is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'audio_id'=>$ID_list
        );

        if(!Db::delete_from_where_list('_audio_item',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Audio item was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $audio_item_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_audio_item_ID(int $audio_item_ID=NULL){

        if(empty($audio_item_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio item ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($audio_item_ID,'_audio_item',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Audio item was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

}