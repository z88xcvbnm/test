<?php

namespace Core\Module\Audio;

use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Encrypt\HashFile;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Exception\PhpException;
use Core\Module\File\File;
use Core\Module\File\FileType;

class AudioUploadedPrepare{

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $audio_ID;

    /** @var string */
    private static $audio_date_create;

    /** @var array */
    private static $audio_item_ID_list=[];

    /** @var string */
    private static $file_content_type;

    /** @var string */
    private static $file_extension;

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_convert_type(){

        if(!isset(AudioConfig::$audio_convert_type_list[self::$file_content_type])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'File type is not exists'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return string
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_file_source_path(){

        return File::get_file_path_from_file_ID(self::$file_ID);

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_audio(){
        
        $audio_data=Audio::add_audio_preparing(self::$file_ID,self::$file_extension,true);

        if(empty($audio_data)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Image was not added'
            );

            throw new DbQueryException($error);

        }

        self::$audio_ID             =$audio_data['ID'];
        self::$audio_date_create    =$audio_data['date_create'];

        $file_result_path           =self::get_file_source_path();
        $dir_date_audio_result      =Dir::get_dir_from_date(DirConfig::$dir_audio,self::$audio_date_create);
        $dir_audio_result           =$dir_date_audio_result.'/'.self::$audio_ID;
        $audio_source_path          =$dir_audio_result.'/source.'.self::$file_extension;

        Dir::create_dir($dir_audio_result);

        if(!copy($file_result_path,$audio_source_path)){

            $error=array(
                'title' =>'File problem',
                'info'  =>'File was not copied'
            );

            throw new PathException($error);

        }

        foreach(AudioConfig::$audio_bitrate_list as $bitrate)
            foreach(AudioConfig::$audio_hz_list as $hz){

                    $audio_item_ID                      =AudioItem::add_audio_item_preparing(self::$file_ID,self::$audio_ID,$bitrate,$hz);
                    $file_audio_result_path             =$dir_audio_result.'/'.$audio_item_ID.'.mp3';

                    self::$audio_item_ID_list[]         =$audio_item_ID;

                    if(!AudioConvert::convert($file_result_path,$file_audio_result_path,'libmp3lame',$bitrate,$hz)){
                        
                        $error=array(
                            'title'     =>'System problem',
                            'info'      =>'Audio was not converted'
                        );

                        throw new PhpException($error);
                        
                    }

                    $audio_hash         =HashFile::get_sha1_encode($file_audio_result_path);
                    $audio_mime_type    =FileType::get_file_mime_type($file_audio_result_path);
                    $audio_extension    =AudioConfig::$audio_extension_default;
                    $audio_file_size    =filesize($file_audio_result_path);
                    $audio_duration     =Audio::get_audio_duration_in_seconds_from_path($file_audio_result_path);

                    AudioItem::update_audio_item_ID_prepared($audio_item_ID,$audio_hash,$audio_duration,$bitrate,$hz,$audio_file_size,$audio_mime_type,$audio_extension);

                    if($bitrate==AudioConfig::$audio_bitrate_default&&$hz==AudioConfig::$audio_hz_default)
                        Audio::update_audio_preparing(self::$audio_ID,$audio_hash,$audio_duration,$bitrate,$hz,$audio_file_size,$audio_mime_type,$audio_extension);

                }

        Audio::update_audio_ID_prepared(self::$audio_ID);

        return array(
            'audio_ID'              =>self::$audio_ID,
            'audio_item_ID_list'    =>self::$audio_item_ID_list,
            'audio_dir'             =>$dir_audio_result
        );

    }

    /**
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_convert_type())
            return self::prepare_audio();

        return NULL;

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $file_ID=NULL,string $file_content_type=NULL,string $file_extension=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list['file_ID']='File ID is empty';

        if(empty($file_content_type))
            $error_info_list['file_content_type']='File content type is empty';

        if(empty($file_extension))
            $error_info_list['file_extension']='File extension is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$file_ID              =$file_ID;
        self::$file_content_type    =$file_content_type;
        self::$file_extension       =$file_extension;

        return self::set();

    }

}