<?php

namespace Core\Module\Audio;

use Core\Module\Exception\ParametersException;
use Core\Module\Ffmpeg\Ffmpeg;

class AudioConvert{

    /**
     * @param string|NULL $file_path_from
     * @param string|NULL $file_path_to
     * @param string $codec
     * @param int $bitrate
     * @param int $hz
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function convert(string $file_path_from=NULL,string $file_path_to=NULL,string $codec='libmp3lame',int $bitrate=128000,int $hz=44100){

        $error_info_list=[];

        if(empty($file_path_from))
            $error_info_list[]='File path from is empty';

        if(empty($file_path_to))
            $error_info_list[]='File path to is empty';

        if(empty($codec))
            $error_info_list[]='Audio codec is empty';

        if(empty($bitrate))
            $error_info_list[]='Audio bitrate is empty';

        if(empty($hz))
            $error_info_list[]='Audio Hz is empty';

        if(!AudioValidation::valid_audio_codec($codec))
            $error_info_list[]='Audio codec is not valid';

        if(!AudioValidation::valid_audio_bitrate($bitrate))
            $error_info_list[]='Audio bitrate is not valid';

        if(!AudioValidation::valid_audio_hz($hz))
            $error_info_list[]='Audio Hz is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'file_path_from'        =>$file_path_from,
                    'file_path_to'          =>$file_path_to,
                    'codec'                 =>$codec,
                    'bitrate'               =>$bitrate,
                    'hz'                    =>$hz
                )
            );

            throw new ParametersException($error);

        }

        if(!file_exists($file_path_from))
            $error_info_list[]='Audio path from is not exists';

        if(file_exists($file_path_to))
            $error_info_list[]='Audio path to already exists';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Path problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'file_path_from'        =>$file_path_from,
                    'file_path_to'          =>$file_path_to
                )
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::convert_audio($file_path_from,$file_path_to,$codec,$bitrate,$hz);

    }

}