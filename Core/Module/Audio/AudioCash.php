<?php

namespace Core\Module\Audio;

use Core\Module\Exception\ParametersException;

class AudioCash{

    /** @var array */
    public  static $audio_list=[];

    /**
     * @param int|NULL $audio_ID
     * @param string|NULL $param
     * @return bool
     */
    public  static function isset_audio_parameter_in_cash(int $audio_ID=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($audio_ID))
            $error_info_list[]='Audio ID is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(empty(self::$audio_list[$audio_ID]))
            return false;

        return !empty(self::$audio_list[$audio_ID][$param]);

    }

    /**
     * @param int|NULL $audio_ID
     * @param string|NULL $param
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_parameter_in_cash(int $audio_ID=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($audio_ID))
            $error_info_list[]='Audio ID is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'audio_ID'      =>$audio_ID,
                    'param'         =>$param
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$audio_list[$audio_ID]))
            return NULL;

        return empty(self::$audio_list[$audio_ID][$param])?NULL:self::$audio_list[$audio_ID][$param];

    }

    /**
     * @param int|NULL $audio_ID
     * @param string|NULL $param
     * @param null $value
     * @param bool $is_rewrite
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_audio_parameter_in_cash(int $audio_ID=NULL,string $param=NULL,$value=NULL,bool $is_rewrite=true){

        $error_info_list=[];

        if(empty($audio_ID))
            $error_info_list[]='Audio ID is empty';

        if(empty($param))
            $error_info_list[]='Parameters is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'audio_ID'      =>$audio_ID,
                    'param'         =>$param,
                    'value'         =>$value,
                    'is_rewrite'    =>$is_rewrite
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$audio_list[$audio_ID]))
            self::$audio_list[$audio_ID]=[];

        if($is_rewrite)
            self::$audio_list[$audio_ID][$param]=$value;

        return true;

    }

}