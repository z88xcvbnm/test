<?php

namespace Core\Module\Audio;

class AudioConfig{

    /** @var int */
    public  static $audio_hz_default        =44100;

    /** @var int */
    public  static $audio_bitrate_default   =128000;

    /** @var array */
    public  static $audio_bitrate_list=array(
        96000,
        128000
    );

    /** @var array */
    public  static $audio_codec_list=array(
        'libmp3lame',
        'aac'
    );

    /** @var array */
    public  static $audio_hz_list=array(
        44100
    );

    /** @var string */
    public  static $audio_extension_default='mp3';

    /** @var array */
    public  static $audio_convert_type_list=array(
        'audio'=>array(
            'bitrate'=>array(
                96000,
                128000
            ),
            'hz'=>array(
                44100
            )
        )
    );

}