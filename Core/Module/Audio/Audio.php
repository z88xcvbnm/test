<?php

namespace Core\Module\Audio;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Dir\DirConfig;
use Core\Module\Exception\DbParametersException;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Ffmpeg\Ffmpeg;
use Core\Module\User\User;

class Audio{

    /**
     * @param int|NULL $audio_ID
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_audio_ID(int $audio_ID=NULL){

        if(empty($audio_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($audio_ID,'_audio');

    }

    /**
     * @param array $audio_ID_list
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_audio_ID_list(array $audio_ID_list=[]){

        if(count($audio_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Audio ID list is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID_list($audio_ID_list,'_audio');

    }

    /**
     * @param string|NULL $hash
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_audio_hash(string $hash=NULL){

        if(empty($hash)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Audio hash is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_audio',
            'where'=>array(
                array(
                    'column'    =>'audio_id',
                    'value'     =>NULL
                ),
                array(
                    'column'    =>'hash',
                    'value'     =>$hash
                ),
                array(
                    'column'    =>'prepared',
                    'value'     =>1
                ),
                array(
                    'column'    =>'hide',
                    'value'     =>0
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return count($r)>0;

    }

    /**
     * @param array $audio_ID_list
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_ID_list(array $audio_ID_list=[]){

        if(count($audio_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Audio ID list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'    =>array(
                'id'
            ),
            'table'     =>'_audio',
            'where'     =>array(
                'id'        =>$audio_ID_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['id'];

        return $list;

    }

    /**
     * @param string|NULL $hash
     * @return null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_ID_from_hash(string $hash=NULL){

        if(empty($hash)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Audio hash is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_audio',
            'where'=>array(
                array(
                    'column'    =>'audio_id',
                    'value'     =>NULL
                ),
                array(
                    'column'    =>'hash',
                    'value'     =>$hash
                ),
                array(
                    'column'    =>'prepared',
                    'value'     =>1
                ),
                array(
                    'column'    =>'hide',
                    'value'     =>0
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return empty($r[0]['id'])?NULL:$r[0]['id'];

    }

    /**
     * @param int|NULL $file_ID
     * @return null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_ID_from_file_ID(int $file_ID=NULL){

        if(empty($file_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_audio',
            'where'=>array(
                array(
                    'column'    =>'audio_id',
                    'value'     =>NULL
                ),
                array(
                    'column'    =>'file_id',
                    'value'     =>$file_ID
                ),
                array(
                    'column'    =>'prepared',
                    'value'     =>1
                ),
                array(
                    'column'    =>'hide',
                    'value'     =>0
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return empty($r[0]['id'])?NULL:$r[0]['id'];

    }

    /**
     * @param int|NULL $audio_ID
     * @return null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_duration(int $audio_ID=NULL){

        if(empty($audio_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        $r=Db::get_data_from_ID($audio_ID,'_audio',array('duration'),0);

        if(count($r)==0)
            return NULL;

        $duration=empty($r[0]['duration'])?NULL:$r[0]['duration'];

        AudioCash::add_audio_parameter_in_cash($audio_ID,'duration',$duration);

        return $duration;

    }

    /**
     * @param int|NULL $audio_ID
     * @return null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_size(int $audio_ID=NULL){

        if(empty($audio_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        $r=Db::get_data_from_ID($audio_ID,'_audio',array('file_size'),0);

        if(count($r)==0)
            return NULL;

        $file_size=empty($r[0]['file_size'])?NULL:$r[0]['file_size'];

        AudioCash::add_audio_parameter_in_cash($audio_ID,'file_size',$file_size);

        return $file_size;

    }

    /**
     * @param int|NULL $audio_ID
     * @return null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_date_create(int $audio_ID=NULL){

        if(empty($audio_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        $r=Db::get_data_from_ID($audio_ID,'_audio',array('date_create'),0);

        if(count($r)==0)
            return NULL;

        $date_create=empty($r[0]['date_create'])?NULL:$r[0]['date_create'];

        AudioCash::add_audio_parameter_in_cash($audio_ID,'date_create',$date_create);

        return $date_create;

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_info_list_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_info_list($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_file_extension_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_file_extension($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return float|int|mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_bitrate_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_audio_bitrate($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_codec_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_audio_codec($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return float|int|mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_hz_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_audio_hz($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_duration_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_duration($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return float|int|mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_duration_in_seconds_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_duration_in_seconds($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return int|mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_duration_in_milliseconds_from_file_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_duration_in_milliseconds($file_path);

    }

    /**
     * @param int|NULL $audio_ID
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_item_info_from_audio_ID(int $audio_ID=NULL){

        if(empty($audio_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'file_id',
                'hash',
                'duration',
                'bitrate',
                'hertz',
                'file_size',
                'file_mime_type',
                'file_extension',
                'preparing',
                'prepared',
                'public',
                'hide'
            ),
            'table'=>'_audio_item',
            'where'=>array(
                'audio_id'      =>$audio_ID,
                'type'          =>0
            )
        );

        return Db::select($q);

    }

    /**
     * @param int|NULL $audio_ID
     * @return null|string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_file_path_from_audio_ID(int $audio_ID=NULL){

        if(empty($audio_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        if(AudioCash::isset_audio_parameter_in_cash($audio_ID,'file_path'))
            return AudioCash::get_audio_parameter_in_cash($audio_ID,'file_path');

        $column_list    =[];
        $file_extension      =NULL;
        $date_create    =NULL;

        if(AudioCash::isset_audio_parameter_in_cash($audio_ID,'file_extension'))
            $file_extension=AudioCash::get_audio_parameter_in_cash($audio_ID,'file_extension');
        else
            $column_list[]='file_extension';

        if(AudioCash::isset_audio_parameter_in_cash($audio_ID,'date_create'))
            $date_create=AudioCash::get_audio_parameter_in_cash($audio_ID,'date_create');
        else
            $column_list[]='date_create';

        if(count($column_list)>0){

            $r=Db::get_data_from_ID($audio_ID,'_audio',$column_list,0);

            if(!empty($r['file_extension'])){

                $file_extension=$r['file_extension'];

                AudioCash::add_audio_parameter_in_cash($audio_ID,'file_extension',$file_extension);

            }

            if(!empty($r['date_create'])){

                $date_create=$r['date_create'];

                AudioCash::add_audio_parameter_in_cash($audio_ID,'date_create',$date_create);

            }

        }

        if(
              empty($date_create)
            ||empty($file_extension)
        ){

            $error=array(
                'title'=>'DB query problem',
                'info'=>'System cannot get audio date create',
                'data'=>array(
                    'audio_ID'      =>$audio_ID,
                    'file_extension'     =>$file_extension,
                    'date_create'   =>$date_create
                )
            );

            throw new DbParametersException($error);

        }

        $audio_file_path=self::get_audio_file_path($audio_ID,$file_extension,$date_create);

        if(!file_exists($audio_file_path)){

            $error=array(
                'title'=>'File path problem',
                'info'=>'Audio file is not exists',
                'data'=>array(
                    'audio_ID'      =>$audio_ID,
                    'file_path'     =>$audio_file_path
                )
            );

            throw new PathException($error);

        }

        AudioCash::add_audio_parameter_in_cash($audio_ID,'file_path',$audio_file_path);

        return $audio_file_path;

    }

    /**
     * @param int|NULL $audio_ID
     * @return null|string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_dir_from_audio_ID(int $audio_ID=NULL){

        if(empty($audio_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        if(AudioCash::isset_audio_parameter_in_cash($audio_ID,'audio_dir'))
            return AudioCash::get_audio_parameter_in_cash($audio_ID,'audio_dir');

        $column_list    =[];
        $file_extension      =NULL;
        $date_create    =NULL;

        if(AudioCash::isset_audio_parameter_in_cash($audio_ID,'file_extension'))
            $file_extension=AudioCash::get_audio_parameter_in_cash($audio_ID,'file_extension');
        else
            $column_list[]='file_extension';

        if(AudioCash::isset_audio_parameter_in_cash($audio_ID,'date_create'))
            $date_create=AudioCash::get_audio_parameter_in_cash($audio_ID,'date_create');
        else
            $column_list[]='date_create';

        if(count($column_list)>0){

            $r=Db::get_data_from_ID($audio_ID,'_audio',$column_list,0);

            if(!empty($r['file_extension'])){

                $file_extension=$r['file_extension'];

                AudioCash::add_audio_parameter_in_cash($audio_ID,'file_extension',$file_extension);

            }

            if(!empty($r['date_create'])){

                $date_create=$r['date_create'];

                AudioCash::add_audio_parameter_in_cash($audio_ID,'date_create',$date_create);

            }

        }

        if(
              empty($date_create)
            ||empty($file_extension)
        )
            return NULL;
        else{

            $audio_file_path=self::get_audio_dir($audio_ID,$file_extension,$date_create);

            if(!file_exists($audio_file_path)){

                $error=array(
                    'title' =>'File path problem',
                    'info'  =>'Audio file is not exists'
                );

                throw new PathException($error);

            }

            AudioCash::add_audio_parameter_in_cash($audio_ID,'audio_dir',$audio_file_path);

            return $audio_file_path;

        }

    }

    /**
     * @param int|NULL $audio_ID
     * @param string|NULL $file_extension
     * @param string|NULL $date_create
     * @return string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_file_path(int $audio_ID=NULL,string $file_extension=NULL,string $date_create=NULL){

        $error_info_list=[];

        if(empty($audio_ID))
            $error_info_list[]='Audio ID is empty';

        if(empty($file_extension))
            $error_info_list[]='File type is empty';

        if(empty($date_create))
            $error_info_list[]='Date create is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

//        $date_path      =Date::get_date_path($date_create);
//        $file_path      =DirConfig::$dir_audio.'/'.$date_path.'/'.$audio_ID.'.'.$file_extension;

        $date_path      =Date::get_date_path($date_create,DirConfig::$dir_audio);
        $file_path      =$date_path.'/'.$audio_ID.'.'.$file_extension;

        AudioCash::add_audio_parameter_in_cash($audio_ID,'audio_file_path',$file_path);

        return $file_path;

    }

    /**
     * @param int|NULL $audio_ID
     * @param string|NULL $file_extension
     * @param string|NULL $date_create
     * @return string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_dir(int $audio_ID=NULL,string $file_extension=NULL,string $date_create=NULL){

        $error_info_list=[];

        if(empty($audio_ID))
            $error_info_list[]='Audio ID is empty';

        if(empty($file_extension))
            $error_info_list[]='File type is empty';

        if(empty($date_create))
            $error_info_list[]='Date create is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $date_path      =Date::get_date_path($date_create,DirConfig::$dir_audio);
        $file_path      =$date_path.'/'.$audio_ID;

        AudioCash::add_audio_parameter_in_cash($audio_ID,'audio_dir',$file_path);

        return $file_path;

    }

    /**
     * @param array $audio_ID_list
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_dir_list(array $audio_ID_list=[]){

        if(count($audio_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Audio ID list is empty'
            );

            throw new ParametersException($error);

        }
        
        $q=array(
            'select'=>array(
                'id',
                'file_extension',
                'date_create'
            ),
            'table'=>'_audio',
            'where'=>array(
                'id'        =>$audio_ID_list,
                'type'      =>0
            )
        );
        
        $r=Db::select($q);
        
        if(count($r)==0)
            return[];
        
        $list=[];
        
        foreach($r as $row){

            $audio_file_path=self::get_audio_dir($row['id'],$row['file_extension'],$row['date_create']);

            if(!file_exists($audio_file_path)){

                $error=array(
                    'title' =>'File path problem',
                    'info'  =>'Audio dir is not exists'
                );

                throw new PathException($error);

            }

            $list[$row['id']]=$audio_file_path;

            AudioCash::add_audio_parameter_in_cash($row['id'],'audio_dir',$audio_file_path);
            
        }

        return $list;

    }

    /**
     * @param array $audio_ID_list
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_file_path_from_audio_ID_list(array $audio_ID_list=[]){

        if(count($audio_ID_list)==0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Audio ID list is empty'
            );

            throw new ParametersException($error);

        }

        $file_extension              =NULL;
        $file_path              =NULL;
        $date_create            =NULL;
        $audio_list             =[];
        $select_audio_ID_list   =[];

        foreach($audio_ID_list as $audio_ID)
            if(!isset($audio_list[$audio_ID])){

                if(AudioCash::isset_audio_parameter_in_cash($audio_ID,'file_path'))
                    $audio_list[$audio_ID]=AudioCash::get_audio_parameter_in_cash($audio_ID,'file_path');
                else{

                    if(AudioCash::isset_audio_parameter_in_cash($audio_ID,'file_extension'))
                        $file_extension=AudioCash::get_audio_parameter_in_cash($audio_ID,'file_extension');

                    if(AudioCash::isset_audio_parameter_in_cash($audio_ID,'date_create'))
                        $date_create=AudioCash::get_audio_parameter_in_cash($audio_ID,'date_create');

                    if(
                          empty($file_extension)
                        ||empty($date_create)
                    )
                        $select_audio_ID_list[]=$audio_ID;
                    else
                        $audio_list[$audio_ID]=self::get_audio_file_path($audio_ID,$file_extension,$date_create);

                }

            }

        if(count($select_audio_ID_list)>0){

            $select_list=array(
                'id',
                'file_extension',
                'date_create'
            );

            $r=Db::get_data_from_ID_list($select_audio_ID_list,'_audio',$select_list,0);

            foreach($r as $row){

                $audio_ID               =$row['id'];
                $file_extension              =$row['file_extension'];
                $date_create            =$row['date_create'];
                $file_path              =self::get_audio_file_path($audio_ID,$file_extension,$date_create);
                $audio_list[$audio_ID]  =$file_path;

                AudioCash::add_audio_parameter_in_cash($audio_ID,'file_extension',$file_extension);
                AudioCash::add_audio_parameter_in_cash($audio_ID,'date_create',$date_create);
                AudioCash::add_audio_parameter_in_cash($audio_ID,'file_path',$file_path);

            }

        }

        return $audio_list;

    }

    /**
     * @param array $audio_ID_list
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_audio_date_update(array $audio_ID_list=[]){

        if(count($audio_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_audio',
            'set'       =>array(
                'date_update'=>'NOW()'
            ),
            'where'     =>array(
                'id'        =>$audio_ID_list,
                'type'      =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Audio list date update was not update',
                'data'  =>array(
                    'audio_ID_list'     =>$audio_ID_list,
                    'query'             =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $audio_ID
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_audio_ID_date_update(int $audio_ID=NULL){

        if(empty($audio_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_audio',
            'set'       =>array(
                'date_update'=>'NOW()'
            ),
            'where'     =>array(
                'id'        =>$audio_ID,
                'type'      =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Audio date update was not update',
                'data'  =>array(
                    'audio_ID'      =>$audio_ID,
                    'query'         =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $audio_ID
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_audio_ID_prepared(int $audio_ID=NULL){

        if(empty($audio_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_audio',
            'set'=>array(
                'preparing'             =>0,
                'prepared'              =>1,
                'public'                =>1,
                'hide'                  =>0,
                'date_update'           =>'NOW()',
                'type'                  =>0
            ),
            'where'=>array(
                'id'    =>$audio_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio was not updated'
            );

            throw new DbQueryException($error);


        }

        return true;

    }

    /**
     * @param int|NULL $audio_ID
     * @param string|NULL $hash
     * @param int|NULL $duration
     * @param int|NULL $bitrate
     * @param int|NULL $hz
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_audio_prepared(int $audio_ID=NULL,string $hash=NULL,int $duration=NULL,int $bitrate=NULL,int $hz=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL){

        $error_info_list=[];

        if(empty($audio_ID))
            $error_info_list['audio_ID']='Audio ID is empty';

        if(empty($hash))
            $error_info_list['hash']='Hash is empty';

        if(empty($duration))
            $error_info_list['duration']='Duration is empty';

        if(empty($hz))
            $error_info_list['hz']='HZ is empty';

        if(empty($file_size))
            $error_info_list['file_size']='File size is empty';

        if(empty($file_mime_type))
            $error_info_list['file_mime_type']='File mime type is empty';

        if(empty($file_extension))
            $error_info_list['file_extension']='File extension is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_audio',
            'set'=>array(
                'hash'                  =>$hash,
                'duration'              =>$duration,
                'bitrate'               =>$bitrate,
                'hertz'                 =>$hz,
                'file_size'             =>$file_size,
                'file_mime_type'        =>$file_mime_type,
                'file_extension'        =>$file_extension,
                'preparing'             =>0,
                'prepared'              =>1,
                'public'                =>1,
                'hide'                  =>0,
                'date_update'           =>'NOW()',
                'type'                  =>0
            ),
            'where'=>array(
                'id'    =>$audio_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID si empty'
            );

            throw new DbQueryException($error);


        }

        return true;

    }

    /**
     * @param int|NULL $audio_ID
     * @param string|NULL $hash
     * @param int|NULL $duration
     * @param int|NULL $bitrate
     * @param int|NULL $hz
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_audio_preparing(int $audio_ID=NULL,string $hash=NULL,int $duration=NULL,int $bitrate=NULL,int $hz=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL){

        $error_info_list=[];

        if(empty($audio_ID))
            $error_info_list['audio_ID']='Audio ID is empty';

        if(empty($hash))
            $error_info_list['hash']='Hash is empty';

        if(empty($duration))
            $error_info_list['duration']='Duration is empty';

        if(empty($hz))
            $error_info_list['hz']='HZ is empty';

        if(empty($file_size))
            $error_info_list['file_size']='File size is empty';

        if(empty($file_mime_type))
            $error_info_list['file_mime_type']='File mime type is empty';

        if(empty($file_extension))
            $error_info_list['file_extension']='File extension is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_audio',
            'set'=>array(
                'hash'                  =>$hash,
                'duration'              =>$duration,
                'bitrate'               =>$bitrate,
                'hertz'                 =>$hz,
                'file_size'             =>$file_size,
                'file_mime_type'        =>$file_mime_type,
                'file_extension'        =>$file_extension,
                'preparing'             =>1,
                'prepared'              =>0,
                'public'                =>0,
                'hide'                  =>0,
                'date_update'           =>'NOW()',
                'type'                  =>0
            ),
            'where'=>array(
                'id'    =>$audio_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio ID si empty'
            );

            throw new DbQueryException($error);


        }

        return true;

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $hash
     * @param int|NULL $duration
     * @param int|NULL $bitrate
     * @param int|NULL $hz
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param bool $preparing
     * @param bool $prepared
     * @param bool $public
     * @param bool $hide
     * @param bool $need_return_date_create
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_audio(int $file_ID=NULL,string $hash=NULL,int $duration=NULL,int $bitrate=NULL,int $hz=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL,bool $preparing=false,bool $prepared=false,bool $public=false,bool $hide=false,bool $need_return_date_create=false){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_audio',
            'values'=>array(
                'user_id'           =>User::$user_ID,
                'file_id'           =>$file_ID,
                'hash'              =>$hash,
                'duration'          =>$duration,
                'bitrate'           =>$bitrate,
                'hertz'             =>$hz,
                'file_size'         =>$file_size,
                'file_mime_type'    =>$file_mime_type,
                'file_extension'    =>$file_extension,
                'preparing'         =>(int)$preparing,
                'prepared'          =>(int)$prepared,
                'public'            =>(int)$public,
                'hide'              =>(int)$hide,
                'date_create'       =>'NOW()',
                'date_update'       =>'NOW()',
                'type'              =>0
            )
        );

        $return_list=[];

        if($need_return_date_create)
            $return_list[]='date_create';
        
        $r=Db::insert($q,true,$return_list);

        if(count($r)==0){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Audio was not added'
            );

            throw new DbQueryException($error);

        }

        if(count($return_list)==0)
            return $r[0]['id'];
        else
            return array(
                'ID'            =>$r[0]['id'],
                'date_create'   =>$r[0]['date_create']
            );

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_extension
     * @param bool $need_return_date_create
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_audio_preparing(int $file_ID=NULL,string $file_extension=NULL,bool $need_return_date_create=false){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        return self::add_audio($file_ID,NULL,NULL,NULL,NULL,NULL,NULL,$file_extension,true,false,false,true,$need_return_date_create);

    }

    /**
     * @param int|NULL $audio_ID
     * @param bool $is_need_remove_audio_item
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_audio_ID(int $audio_ID=NULL,bool $is_need_remove_audio_item=true){

        if(empty($audio_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Audio ID is empty'
            );

            throw new ParametersException($error);

        }

        if($is_need_remove_audio_item)
            AudioItem::remove_audio_item($audio_ID);

        if(!Db::delete_from_ID($audio_ID,'_audio',0)){

            $error=array(
                'title' =>'Db query problem',
                'info'  =>'Audio ID was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param array $audio_ID_list
     * @param bool $is_need_remove_audio_item
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_audio_ID_list(array $audio_ID_list=[],bool $is_need_remove_audio_item=true){

        if(count($audio_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Audio ID list is empty'
            );

            throw new ParametersException($error);

        }

        $ID_list=[];

        foreach($audio_ID_list as $audio_ID)
            if(!empty($audio_ID))
                $ID_list[]=$audio_ID;

        if(count($ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Audio ID list is empty'
            );

            throw new ParametersException($error);

        }

        if($is_need_remove_audio_item)
            AudioItem::remove_audio_item_list($ID_list);

        if(!Db::delete_from_ID_list($ID_list,'_audio',0)){

            $error=array(
                'title' =>'Db query problem',
                'info'  =>'Audio ID was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

}