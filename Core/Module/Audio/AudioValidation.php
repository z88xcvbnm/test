<?php

namespace Core\Module\Audio;

use Core\Module\Exception\ParametersException;
use Core\Module\Ffmpeg\Ffmpeg;

class AudioValidation{

    /**
     * @param string|NULL $codec
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function valid_audio_codec(string $codec=NULL){

        if(empty($codec)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio codec is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::valid_audio_codec($codec);

    }

    /**
     * @param string|NULL $bitrate
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function valid_audio_bitrate(string $bitrate=NULL){

        if(empty($bitrate)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Bitrate is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::valid_audio_bitrate($bitrate);

    }

    /**
     * @param string|NULL $hz
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function valid_audio_hz(string $hz=NULL){

        if(empty($hz)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio HZ is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::valid_audio_hz($hz);

    }

}