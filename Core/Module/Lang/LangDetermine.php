<?php

namespace Core\Module\Lang;

use Core\Module\Data\Data;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;
use Core\Module\Worktime\Worktime;

class LangDetermine{

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_lang_list_to_default(){

        if(!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])){

            $list=strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']);

            if(!preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/',$list,$list)){

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>'Client lang list is not valid'
                );

                throw new ParametersException($error);

            }

            if(count($list[1])==0)
                return false;

            $lang_list_temp     =array_combine($list[1],$list[2]);
            $lang_list          =[];

            foreach($lang_list_temp as $key=>$percent){

                $key_list=mb_split('-',$key);

                $lang_list[$key_list[0]]=empty($percent)?1:$percent;

            }

            arsort($lang_list,SORT_NUMERIC);

            $lang_key_list          =array_keys($lang_list);
            $lang_id_list           =Lang::get_lang_ID_associative_list_from_lang_key_list($lang_key_list);
            $lang_keyboard_list     =[];

            foreach($lang_list as $key=>$percent)
                if(isset($lang_id_list[$key])){

                    if(empty(Lang::$lang_ID)){

                        Lang::set_lang_ID_default((int)$lang_id_list[$key]);
                        Lang::set_lang_key_default($key);

                    }

                    $lang_keyboard_list[$key]=array(
                        'key'           =>$key,
                        'percent'       =>$percent,
                        'lang_id'       =>$lang_id_list[$key]
                    );

                }

        }
        else{

            Lang::set_lang_ID_default(LangConfig::$lang_ID_spare);
            Lang::set_lang_key_default(LangConfig::$lang_key_spare);

            $lang_keyboard_list=array(
                'en'=>array(
                    'key'       =>'en',
                    'percent'   =>1,
                    'lang_id'   =>42
                )
            );

        }

        $lang_keyboard_hash=LangKeyboard::get_lang_kayboard_hash_from_lang_keyboard_list($lang_keyboard_list);

        LangKeyboard::set_lang_keyboard_list_default($lang_keyboard_list);
        LangKeyboard::set_lang_keyboard_hash_default($lang_keyboard_hash);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_lang_keyboard_list_to_default(){

        if(empty(LangKeyboard::$lang_keyboard_hash))
            return false;

        $lang_keyboard_ID=LangKeyboard::get_lang_keyboard_ID_from_lang_keyboard_hash(LangKeyboard::$lang_keyboard_hash);

        if(empty($lang_keyboard_ID))
            $lang_keyboard_ID=LangKeyboard::add_lang_kayboard_list(LangKeyboard::$lang_keyboard_list);

        if(empty($lang_keyboard_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Lang keyboard list was not added'
            );

            throw new DbQueryException($error);

        }

        LangKeyboard::set_lang_keyboard_ID_default($lang_keyboard_ID);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_lang_list_from_post_to_default(){

        if(empty($_POST['lang']))
            return false;

        if(Json::is_json($_POST['lang'])){

            $lang_key_list  =Json::decode($_POST['lang']);
            $error_info     =NULL;

            if(empty($lang_key_list))
                $error_info='Lang list is empty';

            if(Data::is_array($lang_key_list))
                if(count($lang_key_list)==0)
                    $error_info='Lang list is empty';

            if(!empty($error_info)){

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>'Lang list from json is empty'
                );

                throw new ParametersException($error);

            }

            $percent=round(100/count($lang_key_list),0);


        }
        else{

            if(!LangValidation::is_valid_lang_key($_POST['lang'])){

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>'Lang is not valid'
                );

                throw new ParametersException($error);

            }

            $lang_key_list          =array($_POST['lang']);
            $percent                =100;

        }

        $lang_ID_list=Lang::get_lang_ID_associative_list_from_lang_key_list($lang_key_list);

        if(count($lang_ID_list)==0)
            return false;

        $lang_keyboard_list     =[];
        $is_default             =false;

        foreach($lang_ID_list as $key=>$lang_ID){

            $lang_keyboard_list[$key]=array(
                'key'           =>$key,
                'percent'       =>$percent,
                'lang_id'       =>$lang_ID
            );

            if(!$is_default){

                $is_default=true;

                Lang::set_lang_ID_default($lang_ID);
                Lang::set_lang_key_default($key);

            }

        }

        $lang_keyboard_hash=LangKeyboard::get_lang_kayboard_hash_from_lang_keyboard_list($lang_keyboard_list);

        LangKeyboard::set_lang_keyboard_list_default($lang_keyboard_list);
        LangKeyboard::set_lang_keyboard_hash_default($lang_keyboard_hash);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_client_lang_list_from_post_to_default();
        self::set_client_lang_list_to_default();
        self::set_client_lang_keyboard_list_to_default();

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Worktime::set_timestamp_point('Lang Determine Start');

        self::set();

        Worktime::set_timestamp_point('Lang Determine Finish');

        return true;

    }

}