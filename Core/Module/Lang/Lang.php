<?php

namespace Core\Module\Lang;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class Lang{

    /** @var int */
    public  static $lang_ID;

    /** @var string */
    public  static $lang_key;

    /** @var string */
    public  static $lang_key_full;

    /** @var string */
    public  static $lang_key_alt;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$lang_ID          =NULL;
        self::$lang_key         =NULL;
        self::$lang_key_full    =NULL;
        self::$lang_key_alt     =NULL;

        return true;

    }

    /**
     * @param int|NULL $lang_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_lang_ID(int $lang_ID=NULL){

        if(empty($lang_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($lang_ID,'_lang',0);

    }

    /**
     * @param string|NULL $lang_key
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_lang_key(string $lang_key=NULL){

        if(empty($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is empty'
            );

            throw new ParametersException($error);

        }

        if(!LangValidation::is_valid_lang_key($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is not valid'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'key'=>$lang_key
        );

        return Db::isset_row('_lang',0,$where_list);

    }

    /**
     * @param string|NULL $lang_key
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_ID_from_lang_key(string $lang_key=NULL){

        if(empty($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is empty'
            );

            throw new ParametersException($error);

        }

        if(!LangValidation::is_valid_lang_key($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is not valid'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'key'=>$lang_key
        );

        return Db::get_row_ID('_lang',0,$where_list);

    }

    /**
     * @param array $lang_key_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_ID_associative_list_from_lang_key_list(array $lang_key_list=[]){

        if(count($lang_key_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key list is empty'
            );

            throw new ParametersException($error);

        }

        $value_list=[];

        foreach($lang_key_list as $lang_key)
            if(LangValidation::is_valid_lang_key($lang_key))
                $value_list[]=strtolower($lang_key);
            else{

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>'Lang key is not valid',
                    'data'      =>array(
                        'lang_key_list'=>$lang_key_list
                    )
                );

                throw new ParametersException($error);

            }

        $q=array(
            'select'=>array(
                'id',
                'key'
            ),
            'table'=>'_lang',
            'where'=>array(
                'key'   =>$lang_key_list,
                'type'  =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['key']]=$row['id'];

        return $list;

    }

    /**
     * @param int|NULL $lang_ID
     * @return string|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_key_from_lang_ID(int $lang_ID=NULL){

        if(empty($lang_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'key'
            ),
            'table'=>'_lang',
            'where'=>array(
                'id'    =>$lang_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $lang_ID
     * @return string|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_name_default_from_lang_ID(int $lang_ID=NULL){

        if(empty($lang_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name_default'
            ),
            'table'=>'_lang',
            'where'=>array(
                'id'    =>$lang_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name_default'];

    }

    /**
     * @param string|NULL $lang_key
     * @param string|NULL $lang_key_full
     * @param string|NULL $lang_key_alt
     * @param string|NULL $lang_name_default
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_lang(string $lang_key=NULL,string $lang_key_full=NULL,string $lang_key_alt=NULL,string $lang_name_default=NULL){
        
        $error_info_list=[];
        
        if(empty($lang_key))
            $error_info_list[]='Lang key is empty';
        else if(!LangValidation::is_valid_lang_key($lang_key))
            $error_info_list[]='Lang key is not valid';
        
        if(!empty($lang_key_full))
            if(!LangValidation::is_valid_lang_key_full($lang_key_full))
                $error_info_list[]='Lang key full is not valid';
        
        if(!empty($lang_key_alt))
            if(!LangValidation::is_valid_lang_key_alt($lang_key_alt))
                $error_info_list[]='Lang key alt is not valid';

        if(empty($lang_name_default))
            $error_info_list[]='Lang name default is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'lang_key'              =>$lang_key,
                    'lang_key_full'         =>$lang_key_full,
                    'lang_key_alt'          =>$lang_key_alt,
                    'lang_name_default'     =>$lang_name_default
                )
            );

            throw new ParametersException($error);

        }

        $value_list=array(
            'key'           =>strtolower($lang_key),
            'name_default'  =>$lang_name_default
        );

        if(!empty($lang_key_full))
            $value_list['key_full']=strtolower($lang_key_full);

        if(!empty($lang_key_alt))
            $value_list['key_alt']=strtolower($lang_key_alt);

        $q=array(
            'table' =>'_lang',
            'valus' =>$value_list
        );

        $conflict_list=[
            'column_name'=>[
                'key'
            ],
            'return_list'=>[
                'id'
            ]
        ];

        $r=Db::insert($q,true,[],NULL,$conflict_list);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Lang was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];
        
    }

    /**
     * @param int|NULL $lang_ID
     * @param bool $need_remove_lang_localization
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_lang_ID(int $lang_ID=NULL,bool $need_remove_lang_localization=true){

        if(empty($lang_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang ID is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_lang_localization)
            if(!LangLocalization::remove_lang_localization($lang_ID)){

                $error=array(
                    'title'     =>'DB query problem',
                    'info'      =>'Lang localization was not remvoed'
                );

                throw new DbQueryException($error);

            }

        if(!Db::delete_from_ID($lang_ID,'_lang',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Lang was not remvoed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $lang_key
     * @param bool $need_remove_lang_localization
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_lang_key(string $lang_key=NULL,bool $need_remove_lang_localization=true){

        if(empty($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is empty'
            );

            throw new ParametersException($error);

        }

        if(!LangValidation::is_valid_lang_key($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is not valid'
            );

            throw new ParametersException($error);

        }

        if($need_remove_lang_localization){

            $lang_ID=self::get_lang_ID_from_lang_key($lang_key);

            if(!empty($lang_ID))
                if(!LangLocalization::remove_lang_localization($lang_ID)){

                    $error=array(
                        'title'     =>'DB query problem',
                        'info'      =>'Lang localization was not remvoed'
                    );

                    throw new DbQueryException($error);

                }

        }

        $where_list=array(
            'key'=>strtolower($lang_key)
        );

        if(!Db::delete_from_where_list('_lang',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Lang was not remvoed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $lang_ID
     */
    public  static function set_lang_ID_default(int $lang_ID=NULL){

        self::$lang_ID=empty($lang_ID)?NULL:$lang_ID;

    }

    /**
     * @param string|NULL $lang_key
     */
    public  static function set_lang_key_default(string $lang_key=NULL){

        self::$lang_key=empty($lang_key)?NULL:strtolower($lang_key);

    }

    /**
     * @param string|NULL $lang_key_alt
     */
    public  static function set_lang_key_alt_default(string $lang_key_alt=NULL){

        self::$lang_key_alt=empty($lang_key_alt)?NULL:strtolower($lang_key_alt);

    }

    /**
     * @param string|NULL $lang_key_full
     */
    public  static function set_lang_key_full_default(string $lang_key_full=NULL){

        self::$lang_key_full=empty($lang_key_full)?NULL:strtolower($lang_key_full);

    }

}