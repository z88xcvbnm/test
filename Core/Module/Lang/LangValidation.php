<?php

namespace Core\Module\Lang;

class LangValidation{

    /** @var int */
    public  static $lang_key_len        =2;

    /** @var int */
    public  static $lang_key_full_len   =3;

    /** @var int */
    public  static $lang_key_alt_len    =3;

    /**
     * @param string|NULL $lang_key
     * @return bool
     */
    public  static function is_valid_lang_key(string $lang_key=NULL){

        if(empty($lang_key))
            return false;

        $lang_key_len=mb_strlen($lang_key,'UTF-8');

        if(
              $lang_key=='q'
            ||$lang_key=='und'
        )
            return true;

        return $lang_key_len==self::$lang_key_len;

    }

    /**
     * @param string|NULL $lang_key_full
     * @return bool
     */
    public  static function is_valid_lang_key_full(string $lang_key_full=NULL){

        if(empty($lang_key_full))
            return false;

        $lang_key_full_len=mb_strlen($lang_key_full,'UTF-8');

        return $lang_key_full_len==self::$lang_key_full_len;

    }

    /**
     * @param string|NULL $lang_key_alt
     * @return bool
     */
    public  static function is_valid_lang_key_alt(string $lang_key_alt=NULL){

        if(empty($lang_key_alt))
            return false;

        $lang_key_alt_len=mb_strlen($lang_key_alt,'UTF-8');

        return $lang_key_alt_len==self::$lang_key_alt_len;

    }

}