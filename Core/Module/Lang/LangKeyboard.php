<?php

namespace Core\Module\Lang;

use Core\Module\Db\Db;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;

class LangKeyboard{

    /** @var int */
    public  static $lang_keyboard_ID;

    /** @var string */
    public  static $lang_keyboard_hash;

    /** @var array */
    public  static $lang_keyboard_list;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$lang_keyboard_ID         =NULL;
        self::$lang_keyboard_hash       =NULL;
        self::$lang_keyboard_list       =NULL;

        return true;

    }

    /**
     * @param int|NULL $lang_keyboard_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_lang_keyboard_ID(int $lang_keyboard_ID=NULL){

        if(empty($lang_keyboard_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($lang_keyboard_ID,'_lang_keyboard',0);

    }

    /**
     * @param string|NULL $lang_keyboard_hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_lang_keyboard_from_lang_keyboard_hash(string $lang_keyboard_hash=NULL){

        if(empty($lang_keyboard_hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard hash is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'hash'=>$lang_keyboard_hash
        );

        return Db::isset_row('_lang_keyboard',0,$where_list);

    }

    /**
     * @param string|NULL $lang_keyboard_json
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_lang_keyboard_from_lang_keyboard_json(string $lang_keyboard_json=NULL){

        if(empty($lang_keyboard_json)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard json is empty'
            );

            throw new ParametersException($error);

        }

        $lang_keyboard_hash=self::get_lang_kayboard_hash_from_lang_keyboard_json($lang_keyboard_json);

        if(empty($lang_keyboard_hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard hash is empty'
            );

            throw new ParametersException($error);

        }

        return self::isset_lang_keyboard_from_lang_keyboard_hash($lang_keyboard_hash);

    }

    /**
     * @param string|NULL $lang_keyboard_json
     * @return string
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_kayboard_hash_from_lang_keyboard_json(string $lang_keyboard_json=NULL){

        if(empty($lang_keyboard_json)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard json is empty'
            );

            throw new ParametersException($error);

        }

        return Hash::get_sha1_encode($lang_keyboard_json);

    }

    /**
     * @param array $lang_keyboard_list
     * @return string
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_kayboard_hash_from_lang_keyboard_list(array $lang_keyboard_list=[]){

        if(count($lang_keyboard_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard list is empty'
            );

            throw new ParametersException($error);

        }

        $lang_keyboard_json=Json::encode($lang_keyboard_list);

        return Hash::get_sha1_encode($lang_keyboard_json);

    }

    /**
     * @param string|NULL $lang_keyboard_hash
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_keyboard_ID_from_lang_keyboard_hash(string $lang_keyboard_hash=NULL){

        if(empty($lang_keyboard_hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard hash is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'hash'=>$lang_keyboard_hash
        );

        return Db::get_row_ID('_lang_keyboard',0,$where_list);

    }

    /**
     * @param string|NULL $lang_keyboard_json
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_keyboard_ID_from_lang_keyboard_json(string $lang_keyboard_json=NULL){

        if(empty($lang_keyboard_json)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard json is empty'
            );

            throw new ParametersException($error);

        }

        $lang_keyboard_hash=self::get_lang_kayboard_hash_from_lang_keyboard_json($lang_keyboard_json);

        if(empty($lang_keyboard_hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard hash is empty'
            );

            throw new ParametersException($error);

        }

        return self::get_lang_keyboard_ID_from_lang_keyboard_hash($lang_keyboard_hash);

    }

    /**
     * @param int|NULL $lang_keyboard_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_keyboard(int $lang_keyboard_ID=NULL){

        if(empty($lang_keyboard_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'keyboard'
            ),
            'table'=>'_lang_keyboard',
            'where'=>array(
                'id'        =>$lang_keyboard_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['keyboard'];

    }

    /**
     * @param int|NULL $lang_keyboard_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_keyboard_list(int $lang_keyboard_ID=NULL){

        if(empty($lang_keyboard_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard ID is empty'
            );

            throw new ParametersException($error);

        }

        $lang_keyboard_json=self::get_lang_keyboard($lang_keyboard_ID);

        return Json::decode($lang_keyboard_json);

    }

    /**
     * @param array $lang_keyboard_list
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_lang_kayboard_list(array $lang_keyboard_list=[]){

        if(count($lang_keyboard_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard list is empty'
            );

            throw new ParametersException($error);

        }

        $lang_kayboard_json     =Json::encode($lang_keyboard_list);
        $lang_kayboard_hash     =self::get_lang_kayboard_hash_from_lang_keyboard_list($lang_keyboard_list);

        $q=array(
            'table'     =>'_lang_keyboard',
            'values'    =>array(
                'hash'          =>$lang_kayboard_hash,
                'keyboard'      =>$lang_kayboard_json,
                'date_create'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Lang keyboard was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param array|NULL $lang_keyboard_list
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_lang_kayboard_json(array $lang_keyboard_list=NULL){

        if(empty($lang_kayboard_json)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard json is empty'
            );

            throw new ParametersException($error);

        }

        $lang_kayboard_hash=self::get_lang_kayboard_hash_from_lang_keyboard_list($lang_keyboard_list);

        $q=array(
            'table'     =>'_lang_keyboard',
            'values'    =>array(
                'hash'          =>$lang_kayboard_hash,
                'keyboard'      =>$lang_kayboard_json,
                'date_create'   =>'NOW()'
            )
        );

        $conflict_list=[
            'column_list'=>[
                'hash'
            ],
            'update_list'=>[
                'set'=>[
                    'date_update'   =>'NOW()',
                    'type'          =>0
                ]
            ],
            'return_list'=>[
                'id'
            ]
        ];

        $r=Db::insert($q,true,[],NULL,$conflict_list);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Lang keyboard was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $lang_keyboard_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_lang_keyboard_ID(int $lang_keyboard_ID=NULL){

        if(empty($lang_keyboard_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($lang_keyboard_ID,'_lang_keyboard',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Lang keyboard was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $lang_keyboard_json
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_lang_keyboard(string $lang_keyboard_json=NULL){

        if(empty($lang_keyboard_json)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang keyboard json is empty'
            );

            throw new ParametersException($error);

        }

        $lang_keyboard_hash=self::get_lang_kayboard_hash_from_lang_keyboard_json($lang_keyboard_json);

        $where_list=array(
            'hash'=>$lang_keyboard_hash
        );

        return Db::delete_from_where_list('_lang_keyboard',0,$where_list);

    }

    /**
     * @param int|NULL $lang_keyboard_ID
     */
    public  static function set_lang_keyboard_ID_default(int $lang_keyboard_ID=NULL){

        self::$lang_keyboard_ID=empty($lang_keyboard_ID)?NULL:$lang_keyboard_ID;

    }

    /**
     * @param array|NULL $lang_keyboard_list
     */
    public  static function set_lang_keyboard_list_default(array $lang_keyboard_list=NULL){

        self::$lang_keyboard_list=empty($lang_keyboard_list)?NULL:$lang_keyboard_list;

    }

    /**
     * @param string|NULL $lang_keyboard_hash
     */
    public  static function set_lang_keyboard_hash_default(string $lang_keyboard_hash=NULL){

        self::$lang_keyboard_hash=empty($lang_keyboard_hash)?NULL:$lang_keyboard_hash;

    }

    /**
     * @param string|NULL $lang_keyboard_json
     * @throws \Core\Module\Exception\ParametersValidationException
     */
    public  static function set_lang_keyboard_json_default(string $lang_keyboard_json=NULL){

        self::$lang_keyboard_list=empty($lang_keyboard_json)?NULL:Json::decode($lang_keyboard_json);

    }

}