<?php

namespace Core\Module\Lang;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class LangLocalization{

    /** @var int */
    public  static $lang_localization_ID;

    /** @var int */
    public  static $lang_ID;

    /** @var int */
    public  static $content_lang_ID;

    /** @var string */
    public  static $lang_name;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$lang_localization_ID     =NULL;
        self::$lang_ID                  =NULL;
        self::$content_lang_ID          =NULL;
        self::$lang_name                =NULL;

        return true;

    }

    /**
     * @param int|NULL $lang_localization_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_lang_localization_ID(int $lang_localization_ID=NULL){

        if(empty($lang_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang localization ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($lang_localization_ID,'_lang_localization',0);

    }

    /**
     * @param int|NULL $lang_ID
     * @param int|NULL $content_lang_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_lang_localization(int $lang_ID=NULL,int $content_lang_ID=NULL){

        if(empty($lang_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'lang_id'=>$lang_ID
        );

        if(!empty($content_lang_ID))
            $where_list['content_lang_id']=$content_lang_ID;

        return Db::isset_row('_lang_localization',0,$where_list);

    }

    /**
     * @param int|NULL $lang_ID
     * @param int|NULL $content_lang_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_localization_ID(int $lang_ID=NULL,int $content_lang_ID=NULL){

        if(empty($lang_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'lang_id'=>$lang_ID
        );

        if(!empty($content_lang_ID))
            $where_list['content_lang_id']=$content_lang_ID;

        return Db::get_row_ID('_lang_localization',0,$where_list);

    }

    /**
     * @param int|NULL $lang_ID
     * @param int|NULL $content_lang_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_name(int $lang_ID=NULL,int $content_lang_ID=NULL){

        $error_info_list=[];

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(empty($content_lang_ID))
            $error_info_list[]='Content lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_lang_localization',
            'where'=>array(
                'lang_id'           =>$lang_ID,
                'content_lang_id'   =>$content_lang_ID,
                'type'              =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param int|NULL $lang_localization_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_lang_name_from_lang_localization_ID(int $lang_localization_ID=NULL){

        if(empty($lang_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang localization ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_lang_localization',
            'where'=>array(
                'id'    =>$lang_localization_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param int|NULL $lang_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_lang_localization(int $lang_ID=NULL){

        if(empty($lang_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'lang_id'=>$lang_ID
        );

        if(!Db::delete_from_where_list('_lang_localization',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Lang localization was not remvoed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $lang_localization_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_lang_localization_ID(int $lang_localization_ID=NULL){

        if(empty($lang_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang localization ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($lang_localization_ID,'_lang_localization',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Lang localization was not remvoed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $lang_localization_ID
     */
    public  static function set_lang_localization_ID_default(int $lang_localization_ID=NULL){

        self::$lang_localization_ID=empty($lang_localization_ID)?NULL:$lang_localization_ID;

    }

    /**
     * @param int|NULL $lang_ID
     */
    public  static function set_lang_ID_default(int $lang_ID=NULL){

        self::$lang_ID=empty($lang_ID)?NULL:$lang_ID;

    }

    /**
     * @param int|NULL $content_lang_ID
     */
    public  static function set_content_lang_ID_default(int $content_lang_ID=NULL){

        self::$content_lang_ID=empty($content_lang_ID)?NULL:$content_lang_ID;

    }

    /**
     * @param string|NULL $lang_name
     */
    public  static function set_lang_name_default(string $lang_name=NULL){

        self::$lang_name=empty($lang_name)?NULL:$lang_name;

    }

}