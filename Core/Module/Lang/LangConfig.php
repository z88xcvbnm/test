<?php

namespace Core\Module\Lang;

class LangConfig{

    /** @var int */
    public  static $lang_ID_default     =135;

    /** @var int */
    public  static $lang_ID_spare       =42;

    /** $var string */
    public  static $lang_key_default    ='ru';

    /** $var string */
    public  static $lang_key_spare      ='en';

    /**
     * @return int
     */
    public  static function get_lang_ID_default(){

        return self::$lang_ID_default;

    }

    /**
     * @return string
     */
    public  static function get_lang_key_default(){

        return self::$lang_key_default;

    }

    /**
     * @return int
     */
    public  static function get_lang_key_spare(){

        return self::$lang_ID_spare;

    }

    /**
     * @return string
     */
    public  static function get_lang_hey_spare(){

        return self::$lang_key_spare;

    }

}
