<?php

namespace Core\Module\Log;

use Core\Module\Date\Date;
use Core\Module\Dir\Dir;
use Core\Module\Email\EmailSend;
use Core\Module\History\History;

class Log{

    /** @var bool */
    public  static $is_error;

    /** @var string */
    public  static $file_path;

    /** @var string */
    public  static $file_name;

    /** @var bool */
    private static $need_send_to_mail;

    /** @var string */
    private static $title_default='Unknown title';

    /** @var array */
    public  static $content;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$is_error             =true;
        self::$file_path            =NULL;
        self::$file_name            =NULL;
        self::$need_send_to_mail    =true;

        return true;

    }

    /**
     * @param array $content
     * @param array $error
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send_content(array $content,array $error){

        $title=\Config::$project_name.' | Unknown title';

        if(isset($error['title'])){

            if(is_array($error['title']))
                $title=array_shift($error['title']);
            else if(is_string($error['title']))
                $title=$error['title'];
            else
                $title=self::$title_default;

        }

        return EmailSend::init(\Config::$email_log,$title,'<pre>'.implode("<br />".PHP_EOL,$content).'</pre>',\Config::$email_no_replay);

    }

    /**
     * @param array $error
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function write_content(array $error){

        $content[]='ERROR:';
        $content[]='<pre>'.print_r($error,true).'</pre>';

        $content[]='';
        $content[]='SERVER:';
        $content[]='<pre>'.print_r($_SERVER,true).'</pre>';

        $content[]='';
        $content[]='GET:';
        $content[]='<pre>'.print_r($_GET,true).'</pre>';

        $content[]='';
        $content[]='POST:';
        $content[]='<pre>'.print_r($_POST,true).'</pre>';

        $content[]='';
        $content[]='FILES:';
        $content[]='<pre>'.print_r($_FILES,true).'</pre>';

//        if(self::$need_send_to_mail)
//            self::send_content($content,$error);

        if(\Config::$is_debug)
            echo '<a href="'.self::$file_path.'" target="_blank">Link</a>'."\n";

        self::$file_path=Dir::get_global_dir(self::$file_path);

        return (bool)file_put_contents(self::$file_path,implode('<br />',$content),FILE_APPEND);

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function create_dir(){

        $year       =Date::get_year();
        $month      =Date::get_month();
        $day        =Date::get_day();

        Dir::create_dir(LogConfig::$dir_log);
        Dir::create_dir(LogConfig::$dir_log.'/'.$year);
        Dir::create_dir(LogConfig::$dir_log.'/'.$year.'/'.$month);
        Dir::create_dir(LogConfig::$dir_log.'/'.$year.'/'.$month.'/'.$day);

        self::$file_path=LogConfig::$dir_log.'/'.$year.'/'.$month.'/'.$day.'/'.self::$file_name;

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_file_name(){

        if(empty(self::$file_name))
            self::$file_name=Date::get_date_from_mask("Y_m_d_h_i_s_ms").'__'.time().rand(0,time()).'.html';

        return true;

    }

    /**
     * @param array $content
     * @param bool $is_error
     * @param bool $need_send_to_mail
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(array $content=[],bool $is_error=true,bool $need_send_to_mail=true){

        if(isset($_SERVER['HTTP_HOST']))
            $content['host']=$_SERVER['HTTP_HOST'];

        if(\Config::$is_debug)
            print_r($content);

        self::reset_data();

        self::$is_error             =$is_error;
        self::$need_send_to_mail    =$need_send_to_mail;
        self::$content              =$content;

        self::set_file_name();
        self::create_dir();

        History::update_error();

        return (bool)self::write_content($content);

    }

}