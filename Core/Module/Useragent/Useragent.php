<?php

namespace Core\Module\Useragent;

use Core\Module\Db\Db;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Ip\Ip;

class Useragent{

    /** @var integer */
    public static $useragent_ID;

    /** @var string */
    public static $useragent;

    /** @var string */
    public static $useragent_hash;

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$useragent_ID         =NULL;
        self::$useragent            =NULL;
        self::$useragent_hash       =NULL;

    }

    /**
     * @param int|NULL $useragent_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_useragent_ID(int $useragent_ID=NULL){

        if(empty($useragent_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($useragent_ID,'_useragent',0);

    }

    /**
     * @param string|NULL $useragent
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_useragent(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'useragent'=>$useragent
        );

        return Db::isset_row('_useragent',0,$where_list);

    }

    /**
     * @param string|NULL $useragent_hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_useragent_hash(string $useragent_hash=NULL){

        if(empty($useragent_hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent hash is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'hash'=>$useragent_hash
        );

        return Db::isset_row('_useragent',0,$where_list);

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_useragent_default(){

        if(empty(self::$useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        self::$useragent_ID=self::get_useragent_ID_from_useragent(self::$useragent);

        return !empty(self::$useragent_ID);

    }

    /**
     * @param string|NULL $useragent
     * @return string
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_useragent_hash(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        return Hash::get_sha1_encode($useragent);

    }

    /**
     * @param int|NULL $useragent_ID
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_useragent_from_useragent_ID(int $useragent_ID=NULL){

        if(empty($useragent_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                array(
                    'column'=>'useragent'
                )
            ),
            'table'=>'_useragent',
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$useragent_ID
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return count($r)==0?NULL:$r[0]['useragent'];

    }

    /**
     * @param string|NULL $useragent_hash
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_useragent_ID_from_useragent_hash(string $useragent_hash=NULL){

        if(empty($useragent_hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent hash is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id'
                )
            ),
            'table'=>'_useragent',
            'where'=>array(
                array(
                    'column'=>'hash',
                    'value'=>$useragent_hash
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return count($r)==0?NULL:$r[0]['id'];

    }

    /**
     * @param string|NULL $useragent
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_useragent_ID_from_useragent(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        $useragent_hash=self::get_useragent_hash($useragent);

        return self::get_useragent_ID_from_useragent_hash($useragent_hash);

    }

    /**
     * @return int|mixed|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_useragent_ID_default(){

        if(!empty(self::$useragent_ID))
            return self::$useragent_ID;

        $error_info_list=[];

        if(empty(self::$useragent))
            $error_info_list[]='Useragent default is empty';
        else if(empty(self::$useragent_hash))
            self::$useragent_hash=self::get_useragent_hash_default();

        if(empty(self::$useragent_hash))
            $error_info_list[]='Useragent hash default is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$useragent_ID=self::get_useragent_ID_from_useragent_hash(self::$useragent_hash);

        if(empty(self::$useragent_ID))
            self::$useragent_ID=self::add_useragent(self::$useragent);

        if(empty(self::$useragent_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Useragent was not added'
            );

            throw new DbQueryException($error);

        }

        return self::$useragent_ID;

    }

    /**
     * @return null|string
     */
    public  static function get_useragent_default(){

        return empty(self::$useragent)?NULL:self::$useragent;

    }

    /**
     * @return null|string
     */
    public  static function get_useragent_hash_default(){

        return empty(self::$useragent_hash)?NULL:self::$useragent_hash;

    }

    /**
     * @param string|NULL $useragent
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_useragent(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        $useragent_hash=self::get_useragent_hash($useragent);

        $q=array(
            'table'=>'_useragent',
            'values'=>array(
                array(
                    'hash'          =>$useragent_hash,
                    'useragent'     =>$useragent,
                    'date_create'   =>'NOW()'
                )
            )
        );

        $conflict_list=[
            'column_list'=>[
                'hash'
            ],
            'update_list'=>[
                'set'=>[
                    'date_update'   =>'NOW()',
                    'type'          =>0
                ]
            ],
            'return_list'=>[
                'id'
            ]
        ];

        $r=Db::insert($q,true,[],NULL,$conflict_list);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'New useragent was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @return int|mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_useragent_default(){

        if(empty(self::$useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default useragent is empty'
            );

            throw new ParametersException($error);

        }

        $useragent_ID=self::add_useragent(self::$useragent);

        if(empty($useragent_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'New useragent was not added'
            );

            throw new DbQueryException($error);

        }

        self::$useragent_ID=$useragent_ID;

        return self::$useragent_ID;

    }

    /**
     * @param int|NULL $useragent_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_useragent_ID(int $useragent_ID=NULL){

        if(empty($useragent_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($useragent_ID,'_useragent',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Useragent was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $useragent
     * @param string|NULL $useragent_hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_useragnet(string $useragent=NULL,string $useragent_hash=NULL){

        if(empty($useragent)&&empty($useragent_hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent and useragent hash are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($useragent))
            $where_list['useragent']=$useragent;

        if(!empty($useragent_hash))
            $where_list['useragent_hash']=$useragent_hash;

        if(!Db::delete_from_where_list('_useragent',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Useragent was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $useragent_ID
     */
    public  static function set_useragent_ID_default(int $useragent_ID=NULL){

        self::$useragent_ID=empty($useragent_ID)?NULL:$useragent_ID;

    }

    /**
     * @param string|NULL $useragent
     */
    public  static function set_useragent_default(string $useragent=NULL){

        self::$useragent=empty($useragent)?NULL:$useragent;

    }

    /**
     * @param string|NULL $useragent_hash
     */
    public  static function set_useragent_hash_default(string $useragent_hash=NULL){

        self::$useragent_hash=empty($useragent_hash)?NULL:$useragent_hash;

    }

}