<?php

namespace Core\Module\Useragent;

use Core\Module\Exception\ParametersException;
use Core\Module\Worktime\Worktime;

class UseragentDetermine{

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_useragent_to_default(){

        if(empty($_SERVER['HTTP_USER_AGENT'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'The client did not pass the useragent'
            );

            throw new ParametersException($error);

        }

        Useragent::set_useragent_default($_SERVER['HTTP_USER_AGENT']);

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_useragent_hash_to_default(){

        if(empty($_SERVER['HTTP_USER_AGENT'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'The client did not pass the useragent'
            );

            throw new ParametersException($error);

        }

        $useragent_hash=Useragent::get_useragent_hash($_SERVER['HTTP_USER_AGENT']);

        Useragent::set_useragent_hash_default($useragent_hash);

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_useragent_ID_to_default(){

        Useragent::$useragent_ID=Useragent::get_useragent_ID_default();

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_client_useragent_to_default();
        self::set_client_useragent_hash_to_default();
        self::set_client_useragent_ID_to_default();

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Worktime::set_timestamp_point('Useragent Determine Start');

        self::set();

        Worktime::set_timestamp_point('Useragent Determine Finish');

    }

}