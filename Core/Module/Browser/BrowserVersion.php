<?php

namespace Core\Module\Browser;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class BrowserVersion{

    /** @var int */
    public  static $browser_version_ID;

    /** @var string */
    public  static $browser_version;

    /**
     * Reset default parameters
     * @return bool
     */
    public  static function reset_data(){

        self::$browser_version_ID   =NULL;
        self::$browser_version      =NULL;

        return true;

    }

    /**
     * @param int|NULL $browser_ID
     * @param string|NULL $browser_version
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_browser_version(int $browser_ID=NULL,string $browser_version=NULL){

        $error_info_list=[];

        if(empty($browser_ID))
            $error_info_list[]='Browser ID is empty';

        if(empty($browser_version))
            $error_info_list[]='Browser version is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'browser_ID'            =>$browser_ID,
                    'browser_version'       =>$browser_version
                )
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'browser_id'        =>$browser_ID,
            'version'           =>strtolower($browser_version)
        );

        return Db::isset_row('_browser_version',0,$where_list);

    }

    /**
     * @param int|NULL $browser_version_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_browser_version_ID(int $browser_version_ID=NULL){

        if(empty($browser_version_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser version ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($browser_version_ID,'_browser_version',0);

    }

    /**
     * @param int|NULL $browser_ID
     * @param string|NULL $browser_version
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_browser_version_ID(int $browser_ID=NULL,string $browser_version=NULL){

        $error_info_list=[];

        if(empty($browser_ID))
            $error_info_list[]='Browser ID is empty';

        if(empty($browser_version))
            $error_info_list[]='Browser version is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'browser_ID'            =>$browser_ID,
                    'browser_version'       =>$browser_version
                )
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'browser_id'        =>$browser_ID,
            'version'           =>strtolower($browser_version)
        );

        return Db::get_row_ID('_browser_version',0,$where_list);

    }

    /**
     * @param string $version
     * @return string
     */
    public  static function get_prepare_browser_version(string $version){

        return preg_replace('/[^0-9,.,a-z,A-Z-]/','',$version);

    }

    /**
     * @param int|NULL $browser_ID
     * @param string|NULL $browser_version
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_browser_version(int $browser_ID=NULL,string $browser_version=NULL){

        $error_info_list=[];

        if(empty($browser_ID))
            $error_info_list[]='Browser ID is empty';

        if(empty($browser_version))
            $error_info_list[]='Browser version is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'browser_ID'            =>$browser_ID,
                    'browser_version'       =>$browser_version
                )
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'browser_id'        =>$browser_ID,
            'version'           =>strtolower($browser_version)
        );

        if(!Db::delete_from_where_list('_browser_version',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Browser version was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $browser_version_ID
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_browser_version_ID(int $browser_version_ID=NULL){

        if(empty($browser_version_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser version ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::delete_from_ID($browser_version_ID,'_browser_version',0);

    }

    /**
     * @param int|NULL $browser_ID
     * @param string|NULL $browser_version
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_browser_version(int $browser_ID=NULL,string $browser_version=NULL){

        $error_info_list=[];

        if(empty($browser_ID))
            $error_info_list[]='Browser ID is empty';

        if(empty($browser_version))
            $error_info_list[]='Browser version is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'browser_ID'            =>$browser_ID,
                    'browser_version'       =>$browser_version
                )
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_browser_version',
            'values'    =>array(
                'browser_id'    =>$browser_ID,
                'version'       =>strtolower($browser_version),
                'date_create'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Browser version was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $browser_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_browser_version_list(int $browser_ID=NULL){

        if(empty($browser_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'version'
            ),
            'table'=>'_browser_version',
            'where'=>array(
                'browser_id'    =>$browser_ID,
                'type'          =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=array(
                'ID'        =>$row['id'],
                'version'   =>$row['version']
            );

        return $list;

    }

    /**
     * @param int|NULL $browser_version_ID
     * @return bool
     */
    public  static function set_browser_version_ID_default(int $browser_version_ID=NULL){

        self::$browser_version_ID=$browser_version_ID;

        return true;

    }

    /**
     * @param string|NULL $browser_version
     * @return bool
     */
    public  static function set_browser_version_default(string $browser_version=NULL){

        self::$browser_version=strtolower($browser_version);

        return true;

    }

}