<?php

namespace Core\Module\Browser;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class Browser{

    /** @var integer */
    public  static $browser_ID;

    /** @var string */
    public  static $browser_name;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$browser_ID       =NULL;
        self::$browser_name     =NULL;

        return true;

    }

    /**
     * @param string|NULL $browser_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_browser(string $browser_name=NULL){

        if(empty($browser_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name_lower'=>strtolower($browser_name)
        );

        return Db::isset_row('_browser',0,$where_list);

    }

    /**
     * @param int|NULL $browser_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_browser_ID(int $browser_ID=NULL){

        if(empty($browser_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($browser_ID,'_browser',0);

    }

    /**
     * @param string|NULL $browser_name
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_browser_ID(string $browser_name=NULL){

        if(empty($browser_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name_lower'=>strtolower($browser_name)
        );

        return Db::get_row_ID('_browser',0,$where_list);

    }

    /**
     * @param string|NULL $browser_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_browser_name(string $browser_ID=NULL){

        if(empty($browser_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_browser',
            'where'=>array(
                'id'        =>$browser_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param string|NULL $browser_name
     * @param bool $need_remove_browser_version
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_browser(string $browser_name=NULL,bool $need_remove_browser_version=true){

        if(empty($browser_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser name is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_browser_version){

            $browser_ID=self::get_browser_ID($browser_name);

            if(!empty($browser_ID))
                if(!BrowserVersion::remove_browser_version($browser_ID)){

                    $error=array(
                        'title'     =>'DB query problem',
                        'info'      =>'Browser version was not removed'
                    );

                    throw new DbQueryException($error);

                }

        }

        $where_list=array(
            'name_lower'=>strtolower($browser_name)
        );

        return Db::delete_from_where_list('_browser',0,$where_list);

    }

    /**
     * @param int|NULL $browser_ID
     * @param bool $need_remove_browser_version
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_browser_ID(int $browser_ID=NULL,bool $need_remove_browser_version=true){

        if(empty($browser_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser ID is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_browser_version)
            if(!BrowserVersion::remove_browser_version($browser_ID)){

                $error=array(
                    'title'     =>'DB query problem',
                    'info'      =>'Browser version was not removed'
                );

                throw new DbQueryException($error);

            }

        return Db::delete_from_ID($browser_ID,'_browser',0);

    }

    /**
     * @param string|NULL $browser_name
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_browser(string $browser_name=NULL){

        if(empty($browser_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser name is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_browser',
            'values'    =>array(
                'name'          =>$browser_name,
                'name_lower'    =>strtolower($browser_name),
                'date_create'   =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Browser version was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param array $browser_name_list
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_browser_list(array $browser_name_list=[]){

        if(count($browser_name_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser name list is empty'
            );

            throw new ParametersException($error);

        }

        $values_list=[];

        foreach($browser_name_list as $browser_name)
            if(!empty($browser_name))
                $values_list[]=array(
                    'name_lower'    =>strtolower($browser_name),
                    'name'          =>$browser_name,
                    'date_create'   =>'NOW()',
                    'date_update'   =>'NOW()'
                );

        if(count($values_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser name list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_browser',
            'values'    =>$values_list
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Browser name list was not added'
            );

            throw new DbQueryException($error);

        }

        return $r;

    }

    /**
     * @param int|NULL $browser_ID
     * @return bool
     */
    public  static function set_browser_ID_default(int $browser_ID=NULL){

        self::$browser_ID=$browser_ID;

        return true;

    }

    /**
     * @param string|NULL $browser_name
     * @return bool
     */
    public  static function set_browser_name_default(string $browser_name=NULL){

        self::$browser_name=empty($browser_name)?NULL:$browser_name;

        return true;

    }

}