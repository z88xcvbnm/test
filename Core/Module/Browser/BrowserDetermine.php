<?php

namespace Core\Module\Browser;

use Core\Module\Bot\Bot;
use Core\Module\Device\DeviceType;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\UserAccess;
use Core\Module\Useragent\Useragent;
use Core\Module\Worktime\Worktime;

class BrowserDetermine{

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_webtv(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'webtv')===false)
            return NULL;

        $r=explode('/',stristr($useragent,'webtv'));

        if(isset($r[1])){

            $version=explode(' ',$r[1]);

            return array(
                'browser_name'      =>'WebTV',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
            );

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_edge(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'Edge/')===false)
            return NULL;

        $r=explode('/',stristr($useragent,'Edge'));

        if(isset($r[1])){

            $version=explode(' ',$r[1]);

            $list=array(
                'browser_name'      =>'Edge',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
            );

            if(
                  stripos($useragent,'Windows Phone')!==false
                ||stripos($useragent,'Android')!==false
            )
                $list['is_mobile']=true;

            return $list;

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_ie(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'Trident/7.0; rv:11.0')!==false)
            return array(
                'browser_name'      =>'Internet Explorer',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version('11.0')
            );
        else if(stripos($useragent,'microsoft internet explorer')!==false){

            $list=array(
                'browser_name'      =>'Internet Explorer',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version('1.0')
            );

            $version=stristr($useragent,'/');

            if(preg_match('/308|425|426|474|0b1/i',$version))
               $list['browser_version']=BrowserVersion::get_prepare_browser_version('1.5');

            return $list;

        }
        else if(
              stripos($useragent,'msie')!==false
            &&stripos($useragent,'opera')===false
        ){

            if(stripos($useragent,'msnb')!==false){

                $r=explode(' ',stristr(str_replace(';','; ',$useragent),'MSN'));

                if(isset($r[1]))
                    return array(
                        'browser_name'      =>'MSN Browser',
                        'browser_version'   =>BrowserVersion::get_prepare_browser_version(str_replace(array('(',')',';'),'',$r[1]))
                    );

            }

            $r=explode(' ',stristr(str_replace(';','; ',$useragent),'msie'));

            if(isset($r[1])){

                $list=array(
                    'browser_name'      =>'Internet Explorer',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version(str_replace(array('(',')',';'),'',$r[1]))
                );

                if(stripos($useragent,'IEMobile')!==false)
                    return array(
                        'browser_name'      =>'Pocket Internet Explorer',
                        'is_mobile'         =>true
                    );

                return $list;

            }

        }
		else if(stripos($useragent,'trident')!==false){

			$r=explode('rv:',$useragent);

            if(isset($r[1]))
                return array(
                    'browser_name'      =>'Internet Explorer',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version(preg_replace('/[^0-9.]+/','',$r[1]))
                );

		}
        else if(
              stripos($useragent,'mspie')!==false
            ||stripos($useragent,'pocket')!==false
        ){

            $r=explode(' ',stristr($useragent,'mspie'));

            if(isset($r[1])){

                $list=array(
                    'os_name'           =>'Windows CE',
                    'browser_name'      =>'Pocket Internet Explorer',
                    'is_mobile'         =>true
                );

                if(stripos($useragent,'mspie')!==false)
                    $list['browser_version']=BrowserVersion::get_prepare_browser_version($r[1]);
                else{

                    $r=explode('/',$useragent);

                    if(isset($r[1]))
                        $list['browser_version']=BrowserVersion::get_prepare_browser_version($r[1]);

                }

                return $list;

            }

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_opera(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'opera mini')!==false){

            $r      =stristr($useragent,'opera mini');
            $list   =[];

            if(preg_match('/\//',$r)){

                $r=explode('/',$r);

                if(isset($r[1])){

                    $version=explode(' ',$r[1]);

                    $list['browser_version']=BrowserVersion::get_prepare_browser_version($version[0]);

                }

            }
            else{

                $version=explode(' ',stristr($r,'opera mini'));

                if(isset($version[1]))
                    $list['browser_version']=BrowserVersion::get_prepare_browser_version($version[1]);

            }

            $list['browser_name']       ='Opera Mini';
            $list['is_mobile']          =true;

            return $list;

        }
        else if(stripos($useragent,'opera')!==false){

            $r      =stristr($useragent,'opera');
            $list   =[];

            if(preg_match('/Version\/(1*.*)$/',$r,$matches))
                $list['browser_version']=BrowserVersion::get_prepare_browser_version($matches[1]);
            else if(preg_match('/\//',$r)){

                $r=explode('/',str_replace("("," ",$r));

                if(isset($r[1])){

                    $version=explode(' ',$r[1]);

                    $list['browser_version']=BrowserVersion::get_prepare_browser_version($version[0]);

                }

            }
            else{

                $version=explode(' ',stristr($r,'opera'));

                if(isset($version[1]))
                    $list['browser_version']=BrowserVersion::get_prepare_browser_version($version[1]);

            }

            if(stripos($useragent,'Opera Mobi')!==false)
                $list['is_mobile']=true;

            $list['browser_name']='Opera';

            return $list;

        }
        else if(stripos($useragent,'OPR')!==false){

            $r      =stristr($useragent,'OPR');
            $list   =[];

            if(preg_match('/\//',$r)){

                $r=explode('/',str_replace("("," ",$r));

                if(isset($r[1])){

                    $version=explode(' ',$r[1]);

                    $list['browser_version']=BrowserVersion::get_prepare_browser_version($version[0]);

                }

            }

            if(stripos($useragent,'Mobile')!==false)
                $list['is_mobile']=true;

            $list['browser_name']='Opera';

            return $list;

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_galeon(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'galeon')!==false){

            $r          =explode(' ',stristr($useragent,'galeon'));
            $version    =explode('/',$r[0]);

            if(isset($version[1]))
                return array(
                    'browser_name'      =>'Galeon',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[1])
                );

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_netscape(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(
            stripos($useragent,'Firefox')!==false
            &&preg_match('/Navigator\/([^ ]*)/i',$useragent,$r)
        )
            return array(
                'browser_name'      =>'Netscape Navigator',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($r[1])
            );
        else if(
            stripos($useragent,'Firefox')===false
            &&preg_match('/Netscape6?\/([^ ]*)/i',$useragent,$r)
        )
            return array(
                'browser_name'      =>'Netscape Navigator',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($r[1])
            );

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_vivaldi(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'Vivaldi')!==false){

            $r=explode('/',stristr($useragent,'Vivaldi'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                return array(
                    'browser_name'      =>'Vivalidi',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
                );

            }

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_firefox(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'safari')===false){

            if(preg_match('/Firefox[\/ \(]([^ ;\)]+)/i',$useragent,$r)){

                $list=array(
                    'browser_name'      =>'Firefox',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version($r[1])
                );

                if(stripos($useragent,'Android')!==false){

                    if(stripos($useragent,'Mobile')!==false)
                        $list['is_mobile']=true;
                    else{

                        $list['is_mobile']  =true;
                        $list['is_tablet']  =true;

                    }

                }

                return $list;

            }
            else if(preg_match("/Firefox$/i",$useragent,$r))
                return array(
                    'browser_name'      =>'Firefox'
                );

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_chrome(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'Chrome')!==false){

            $r=explode('/',stristr($useragent,'Chrome'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                $list=array(
                    'browser_name'      =>'Chrome',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
                );

                if(stripos($useragent,'Android')!==false){

                    if(stripos($useragent,'Mobile')!==false)
                        $list['is_mobile']=true;
                    else{

                        $list['is_mobile']  =true;
                        $list['is_tablet']  =true;

                    }

                }

                return $list;

            }

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_omniweb(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'omniweb')!==false){

            $r          =explode('/',stristr($useragent,'omniweb'));
            $version    =explode(' ',isset($r[1])?$r[1]:"");

            return array(
                'browser_name'      =>'OmniWeb',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
            );

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_for_android(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'Android')!==false){

            $r=explode(' ',stristr($useragent,'Android'));

            $list=[];

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                $list['browser_version']=BrowserVersion::get_prepare_browser_version($version[0]);

            }

            if(stripos($useragent,'Mobile')!==false)
                $list['is_mobile']=true;
            else{

                $list['is_mobile']  =true;
                $list['is_tablet']  =true;

            }

            $list['browser_name']='Android';

            return $list;

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_for_ipad(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'iPad')!==false){

            $list=array(
                'device_type'       =>'iPad',
                'is_mobile'         =>true,
                'is_tablet'         =>true
            );

            $r=explode('/',stristr($useragent,'Version'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                $list['browser_name']       ='Safari';
                $list['browser_version']    =BrowserVersion::get_prepare_browser_version($version[0]);

            }

            $r=explode('/',stristr($useragent,'CriOS'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                $list['browser_name']       ='Chrome';
                $list['browser_version']    =BrowserVersion::get_prepare_browser_version($version[0]);

            }

            if(stristr($useragent,'FBIOS'))
                $list['is_facebook']=true;

            return $list;

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_for_ipod(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'iPod')!==false){

            $list=array(
                'device_type'       =>'iPod',
                'is_mobile'         =>true
            );

            $r=explode('/',stristr($useragent,'Safari'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                $list['browser_name']       ='Safari';
                $list['browser_version']    =BrowserVersion::get_prepare_browser_version($version[0]);

            }

            $r=explode('/',stristr($useragent,'CriOS'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                $list['browser_name']       ='Chrome';
                $list['browser_version']    =BrowserVersion::get_prepare_browser_version($version[0]);

            }

            if(stristr($useragent,'FBIOS'))
                $list['is_facebook']=true;

            return $list;

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_for_iphone(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'iPhone')!==false){

            $list=array(
                'device_type'       =>'iPhone',
                'is_mobile'         =>true
            );

            $r=explode('/',stristr($useragent,'Safari'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                $list['browser_name']       ='Safari';
                $list['browser_version']    =BrowserVersion::get_prepare_browser_version($version[0]);

            }

            $r=explode('/',stristr($useragent,'CriOS'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                $list['browser_name']       ='Chrome';
                $list['browser_version']    =BrowserVersion::get_prepare_browser_version($version[0]);

            }

            if(stristr($useragent,'FBIOS'))
                $list['is_facebook']=true;

            return $list;

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_blackberry(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'blackberry')!==false){

            $r=explode("/",stristr($useragent,"BlackBerry"));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                return array(
                    'browser_name'      =>'BlackBerry',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0]),
                    'device_type'       =>'BlackBerry',
                    'is_mobile'         =>true
                );

            }

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_nokia(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(preg_match('/Nokia([^\/]+)\/([^ SP]+)/i',$useragent,$r)){

            $list=array(
                'browser_version'=>$r[2]
            );

            if(
                  stripos($useragent,'Series60')!==false
                ||strpos($useragent,'S60')!==false
            )
                $list['browser_name']='Nokia S60 OSS Browser';
            else
                $list['browser_name']='Nokia Browser';

            $list['is_mobile']=true;

            return $list;

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_google_bot(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'googlebot')!==false){

            $r=explode('/',stristr($useragent,'googlebot'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                return array(
                    'browser_name'      =>'Google Bot',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0]),
                    'is_bot'            =>true
                );

            }

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_msn_bot(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,"msnbot")===false)
            return NULL;
        else{

            $r=explode("/",stristr($useragent,"msnbot"));

            if(isset($r[1])){

                $version=explode(" ",$r[1]);

                return array(
                    'browser_name'      =>'MSN Bot',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0]),
                    'is_bot'            =>true
                );

            }

            return NULL;

        }

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_bing_bot(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,"bingbot")===false)
            return NULL;
        else{

            $r=explode("/",stristr($useragent,"bingbot"));

            if(isset($r[1])){

                $version=explode(" ",$r[1]);

                return array(
                    'browser_name'      =>'Bing Bot',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0]),
                    'is_bot'            =>true
                );

            }

            return NULL;

        }

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_slurp(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'slurp')===false)
            return NULL;
        else{

            $r=explode('/',stristr($useragent,'Slurp'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                return array(
                    'browser_name'      =>'Yahoo Slurp',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0]),
                    'is_mobile'         =>true,
                    'is_bot'            =>true
                );

            }

            return NULL;

        }

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_safari(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(
              stripos($useragent,'Safari')!==false
            &&stripos($useragent,'iPhone')===false
            &&stripos($useragent,'iPod')===false
        ){

            $r      =explode('/',stristr($useragent,'Version'));
            $list   =[];

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                $list['browser_version']=BrowserVersion::get_prepare_browser_version($version[0]);

            }

            $list['browser_name']='Safari';

            return $list;

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_new_positive(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'NetPositive')===false)
            return NULL;

        $r=explode('/',stristr($useragent,'NetPositive'));

        if(isset($r[1])){

            $version=explode(' ',$r[1]);

            return array(
                'browser_name'      =>'NetPositive',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
            );

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_firebird(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'Firebird')===false)
            return NULL;

        $version=explode('/',stristr($useragent,'Firebird'));

        if(isset($version[1]))
            return array(
                'browser_name'      =>'Firebird',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[1])
            );

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_konqueror(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'Konqueror')===false)
            return NULL;

        $r          =explode(' ',stristr($useragent,'Konqueror'));
        $version    =explode('/',$r[0]);

        if(isset($version[1]))
            return array(
                'browser_name'      =>'Konqueror',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[1])
            );

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_icab(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'icab')===false)
            return NULL;

        $r=explode(' ',stristr(str_replace('/',' ',$useragent),'icab'));

        if(isset($r[1]))
            return array(
                'browser_name'      =>'iCab',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($r[1])
            );

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_phoenix(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'Phoenix')===false)
            return NULL;

        $r=explode('/',stristr($useragent,'Phoenix'));

        if(isset($r[1]))
            return array(
                'browser_name'      =>'Phoenix',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($r[1])
            );

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_amaya(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'amaya')===false)
            return NULL;

        $r=explode('/',stristr($useragent,'Amaya'));

        if(isset($r[1])){

            $version=explode(' ',$r[1]);

            return array(
                'browser_name'      =>'Amaya',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
            );

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_lynx(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'lynx')===false)
            return NULL;

        $r          =explode('/',stristr($useragent,'Lynx'));
        $version    =explode(' ',(isset($r[1])?$r[1]:""));

        return array(
            'browser_name'      =>'Lynx',
            'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
        );

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_shiretoko(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'Mozilla')===false)
            return NULL;
        else if(preg_match('/Shiretoko\/([^ ]*)/i',$useragent,$r))
            return array(
                'browser_name'      =>'Shiretoko',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($r[1])
            );

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_icecat(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'Mozilla')===false)
            return NULL;
        else if(preg_match('/IceCat\/([^ ]*)/i',$useragent,$r))
            return array(
                'browser_name'      =>'IceCat',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($r[1])
            );

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_iceweasel(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'Iceweasel')===false)
            return NULL;

        $r=explode('/',stristr($useragent,'Iceweasel'));

        if(isset($r[1])){

            $version=explode(' ',$r[1]);

            return array(
                'browser_name'      =>'Iceweasel',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
            );

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_w3cvalidator(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'W3C-checklink')!==false){

            $r=explode('/',stristr($useragent,'W3C-checklink'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                return array(
                    'browser_name'      =>'W3C Validator',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
                );

            }

        }
        else if(stripos($useragent,'W3C_Validator')!==false){

            $useragent  =str_replace("W3C_Validator ","W3C_Validator/",$useragent);
            $r          =explode('/',stristr($useragent,'W3C_Validator'));

            if(isset($r[1])){

                $version=explode(' ',$r[1]);

                return array(
                    'browser_name'      =>'W3C Validator',
                    'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
                );

            }

        }
        else if(stripos($useragent,'W3C-mobileOK')!==false)
            return array(
                'browser_name'      =>'W3C Validator',
                'is_mobile'         =>true
            );

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_playstation(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stripos($useragent,'PlayStation ')!==false){

            $r      =explode(' ',stristr($useragent,'PlayStation '));
            $list   =array(
                'browser_name'=>'PlayStation'
            );

            if(isset($r[0])){

                $version=explode(')',$r[2]);

                $list['browser_version']=BrowserVersion::get_prepare_browser_version($version[0]);

                if(
                      stripos($useragent,'Portable)')!==false
                    ||stripos($useragent,'Vita')!==false
                )
                    $list['is_mobile']=true;

                return $list;

            }

        }

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_browser_mozilla(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(
              stripos($useragent,'mozilla')!==false
            &&preg_match('/rv:[0-9].[0-9][a-b]?/i',$useragent)
            &&stripos($useragent,'netscape')===false
        ){

            $version=explode(' ',stristr($useragent,'rv:'));

            preg_match('/rv:[0-9].[0-9][a-b]?/i',$useragent,$version);

            return array(
                'browser_name'      =>'Mozilla',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
            );

        }
        else if(
              stripos($useragent,'mozilla')!==false
            &&preg_match('/rv:[0-9]\.[0-9]/i',$useragent)
            &&stripos($useragent,'netscape')===false
        ){

            $version=explode('',stristr($useragent,'rv:'));

            return array(
                'browser_name'      =>'Mozilla',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
            );

        }
        else if(
              stripos($useragent,'mozilla')!==false
            &&stripos($useragent,'netscape')===false
            &&preg_match('/mozilla\/([^ ]*)/i',$useragent,$version)
        )
            return array(
                'browser_name'      =>'Mozilla',
                'browser_version'   =>BrowserVersion::get_prepare_browser_version($version[0])
            );

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_facebook_bot(string $useragent=NULL){

        if(empty($useragent)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Useragent is empty'
            );

            throw new ParametersException($error);

        }

        if(stristr($useragent,'FacebookExternalHit'))
            return array(
                'is_bot'        =>true,
                'is_facebook'   =>true
            );

        return NULL;

    }

    /**
     * @param string|NULL $useragent
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_determine_data(string $useragent=NULL){

        $data=self::check_browser_webtv($useragent);
        
        if(!empty($data))
            return $data;

        $data=self::check_browser_edge($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_ie($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_opera($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_galeon($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_netscape($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_vivaldi($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_firefox($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_chrome($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_omniweb($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_for_android($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_for_ipad($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_for_ipod($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_for_iphone($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_blackberry($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_nokia($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_google_bot($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_msn_bot($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_bing_bot($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_slurp($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_safari($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_new_positive($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_firebird($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_konqueror($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_icab($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_phoenix($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_amaya($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_lynx($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_shiretoko($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_icecat($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_iceweasel($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_w3cvalidator($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_playstation($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_browser_mozilla($useragent);

        if(!empty($data))
            return $data;

        $data=self::check_facebook_bot($useragent);

        if(!empty($data))
            return $data;

        return NULL;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function determine_browser(){

        $data=self::get_determine_data(Useragent::$useragent);

        if(empty($data))
            return false;

        if(isset($data['is_bot']))
            UserAccess::$is_bot=true;

        if(isset($data['is_facebook']))
            Bot::$is_facebook=true;

        if(isset($data['is_vk']))
            Bot::$is_vk=true;

        if(isset($data['is_twitter']))
            Bot::$is_twitter=true;

        if(isset($data['is_google']))
            Bot::$is_google=true;

        if(isset($data['is_yandex']))
            Bot::$is_yandex=true;

        if(isset($data['is_yahoo']))
            Bot::$is_yahoo=true;

        if(isset($data['browser_name']))
            Browser::set_browser_name_default($data['browser_name']);

        if(isset($data['browser_version']))
            BrowserVersion::set_browser_version_default($data['browser_version']);

        if(isset($data['is_mobile']))
            DeviceType::$is_mobile=true;

        if(isset($data['is_tablet']))
            DeviceType::$is_tablet=true;

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_client_browser_ID_to_default(){

        if(!empty(Browser::$browser_ID))
            return true;

        if(empty(Browser::$browser_name))
            return false;

        $browser_ID=Browser::get_browser_ID(Browser::$browser_name);

        if(empty($browser_ID))
            $browser_ID=Browser::add_browser(Browser::$browser_name);

        if(empty($browser_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Browser was not added'
            );

            throw new DbQueryException($error);

        }

        Browser::set_browser_ID_default($browser_ID);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_client_browser_version_to_default(){

        if(!empty(BrowserVersion::$browser_version_ID))
            return true;

        if(empty(BrowserVersion::$browser_version))
            return false;

        if(empty(Browser::$browser_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Browser ID is empty'
            );

            throw new ParametersException($error);

        }

        $browser_version_ID=BrowserVersion::get_browser_version_ID(Browser::$browser_ID,BrowserVersion::$browser_version);

        if(empty($browser_version_ID))
            $browser_version_ID=BrowserVersion::add_browser_version(Browser::$browser_ID,BrowserVersion::$browser_version);

        if(empty($browser_version_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Browser version was not added'
            );

            throw new DbQueryException($error);

        }

        BrowserVersion::set_browser_version_ID_default($browser_version_ID);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(!self::determine_browser())
            return false;

        self::set_client_browser_ID_to_default();
        self::set_client_browser_version_to_default();

        return true;

    }

    /**
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Worktime::set_timestamp_point('Browser Determine Start');

        self::set();

        Worktime::set_timestamp_point('Browser Determine Finish');

    }

}