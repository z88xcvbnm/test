<?php

namespace Core\Module\Tor;

class Tor{

    /** @var string */
    public  static $ip_address          ='127.0.0.1';

    /** @var int */
    public  static $port                =9050;
    
    /** @var int */
    public  static $proxy;

    /** @var string */
    private static $password            ='leR0VLYsGFvngr98VTZGP6RMHR6kFp2Z';

    /**
     * @return bool
     */
	public static function change_proxy_identity(){

        $fp = fsockopen('127.0.0.1', 9050, $errno, $errstr, 30);
        $auth_code = self::$password;
        if ($fp) {
            echo "Connected to TOR port\n";
        }
        else {
            echo "Cant connect to TOR port\n";
        }

        fputs($fp, "AUTHENTICATE \"".$auth_code."\"\r\n");
        $response = fread($fp, 1024);
        print_r($response);
//            list($code, $text) = explode(' ', $response, 2);
        if ($code = '250') {
            echo "Authenticated 250 OK\n";
        }
        else {
            echo "Authentication failed\n";
        }

        fputs($fp, "SIGNAL NEWNYM\r\n");
        $response = fread($fp, 1024);
        print_r($response);
//            list($code, $text) = explode(' ', $response, 2);
        if ($code = '250') {
            echo "New Identity OK\n";
        }
        else {
            echo "SIGNAL NEWNYM failed\n";
            die();
        }
        fclose($fp);

        return true;

	}

}