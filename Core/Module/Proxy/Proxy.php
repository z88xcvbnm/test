<?php

namespace Core\Module\Proxy;

use Core\Module\Curl\CurlPost;
use Core\Module\Db\Db;
use Core\Module\Dir\Dir;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\User\User;

class Proxy{

    // http://free-proxy.cz
    // http://free-proxy.cz/ru/proxylist/country/RU/https/speed/all

    /** @var string */
    private static $table_name      ='proxy';

    /** @var string */
//    private static $url_check       ='https://pikabu.ru/';
    private static $url_check       ='http://dynupdate.no-ip.com/ip.php';

    /** @var string */
    private static $url             ='https://api.getproxylist.com/proxy?allowsHttps=1&protocol[]=http&country[]=BE';

    /** @var string */
    public  static $hash;

    /** @var string */
    public  static $proxy;

    /** @var int */
    public  static $port;

    /**
     * @param int|NULL $source_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_proxy_http_list(int $source_ID=NULL){

        foreach(ProxyConfig::$proxy_http_list as $row){

            if(!self::isset_proxy($source_ID,$row)){

                $list=mb_split('\:',$row);

                if(self::check_connect($list[0],$list[1])){

                    self::$hash         =$row;
                    self::$proxy        =$list[0];
                    self::$port         =$list[1];

                    return true;

                }

            }
            else if((bool)$_POST['need_log'])
                echo"Error: proxy already exists: ".$row."\n";

        }

        return false;

    }

    /**
     * @param int|NULL $source_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_proxy_list(int $source_ID=NULL){

        foreach(ProxyConfig::$proxy_list as $row){

            if(!self::isset_proxy($source_ID,$row)){

                $list=mb_split('\:',$row);

                if((bool)$_POST['need_log'])
                    echo"Check connect...\n";

                if(self::check_connect($list[0],$list[1])){

                    self::$hash         =$row;
                    self::$proxy        =$list[0];
                    self::$port         =$list[1];

                    return true;

                }

                if((bool)$_POST['need_log'])
                    echo"Connect fail\n";

            }
            else if((bool)$_POST['need_log'])
                echo"Error: proxy already exists: ".$row."\n";

        }

        return false;

    }

    /**
     * @param int|NULL $source_ID
     * @param string|NULL $hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_proxy(int $source_ID=NULL,string $hash=NULL){

        if((bool)$_POST['need_log'])
            echo"Check isset proxy: ".$hash."\n";

        $q=[
            'table'     =>'proxy',
            'select'    =>[
                'hash'
            ],
            'where'=>[
                'source_id'     =>$source_ID,
                'hash'          =>$hash,
                'type'          =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return false;

        return true;

    }

    /**
     * @param string|NULL $ip
     * @param int|NULL $port
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function check_connect(string $ip=NULL,int $port=NULL){

        $r      =CurlPost::init(self::$url_check,[],[],false,NULL,false,$ip,$port);
        $dir    ='Temp/Proxy';

        if(!empty($_POST['need_log']))
            echo 'Status: '.$r['status']."\n";

        Dir::create_dir($dir);

        file_put_contents('/var/www/public/Temp/Proxy/proxy_connect_'.time().'.log',print_r($r,true),FILE_APPEND);

        if(
              $r['status']==200
            ||$r['status']==403
        )
            return true;

        if((bool)$_POST['need_log'])
            echo"Connect fail:\n";

        return false;

    }

    /**
     * @param int|NULL $source_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_proxy(int $source_ID=NULL){

        $n=0;

        do{

            if(self::get_proxy_list($source_ID))
                return[
                    'hash'      =>self::$hash,
                    'proxy'     =>self::$proxy,
                    'port'      =>self::$port
                ];
            else if(self::get_proxy_http_list($source_ID))
                return[
                    'hash'      =>self::$hash,
                    'proxy'     =>self::$proxy,
                    'port'      =>self::$port
                ];

            $n++;

            if($n>20){

                $error=[
                    'title'     =>PhpException::$title,
                    'info'      =>'Problem with get Proxy ips'
                ];

                throw new PhpException($error);

            }

        }while(true);

        return NULL;

    }

    /**
     * @param int|NULL $source_ID
     * @param string|NULL $proxy
     * @param int|NULL $port
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_used_proxy(int $source_ID=NULL,string $proxy=NULL,int $port=NULL){

        $hash=$proxy.':'.$port;

        self::add_proxy($source_ID,$hash,$proxy,$port);

        return true;

    }

    /**
     * @param int|NULL $source_ID
     * @param string|NULL $hash
     * @param string|NULL $proxy
     * @param int|NULL $port
     * @param string|NULL $login
     * @param string|NULL $passwrod
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_proxy(int $source_ID=NULL,string $hash=NULL,string $proxy=NULL,int $port=NULL,string $login=NULL,string $passwrod=NULL){

        $error_info_list=[];

        if(empty($source_ID))
            $error_info_list[]='Source ID is empty';

        if(empty($proxy))
            $error_info_list[]='Proxy is empty';

        if(empty($port))
            $error_info_list[]='Port is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'values'=>[
                'user_id'           =>User::$user_ID,
                'source_id'         =>$source_ID,
                'hash'              =>$hash,
                'proxy'             =>$proxy,
                'port'              =>$port,
                'login'             =>empty($login)?NULL:$login,
                'password'          =>empty($passwrod)?NULL:$passwrod,
                'date_create'       =>'NOW()',
                'date_update'       =>'NOW()',
                'type'              =>0
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Proxy was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

}