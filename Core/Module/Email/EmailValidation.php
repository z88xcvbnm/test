<?php

namespace Core\Module\Email;

class EmailValidation{

    /**
     * @param string $email
     * @return bool
     */
    public  static function is_valid_email(string $email){

        if(empty($email))
            return false;

        return (bool)filter_var($email,FILTER_VALIDATE_EMAIL);

//        Old validation version
//        return preg_match('/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/',$email);

    }

    /**
     * @param array $email_list
     * @return array
     */
    public  static function get_valid_email_list(array $email_list=[]){

        if(count($email_list)==0)
            return[];

        $list=[];

        foreach($email_list as $email)
            if(self::is_valid_email($email))
                $list[]=strtolower($email);

        return $list;

    }

}