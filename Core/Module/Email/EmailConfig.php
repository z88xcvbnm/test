<?php

namespace Core\Module\Email;

class EmailConfig{

    /** @var string */
    public  static $email_name_default      ='info';

    /** @var string */
    public  static $email_eol               =PHP_EOL;

}