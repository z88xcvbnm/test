<?php

namespace Core\Module\Email;

use Core\Module\Exception\ParametersException;

class EmailSend{

    /** @var array */
    private static $email_list;

    /** @var string */
    private static $email_default;

    /** @var string */
    private static $title;

    /** @var string */
    private static $message;

    /** @var array */
    private static $file_list;

    /** @var array */
    private static $content;

    /** @var string */
    private static $separator;

    /** @var string */
    private static $headers;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$email_list               =NULL;
        self::$email_default            =NULL;
        self::$title                    =NULL;
        self::$message                  =NULL;
        self::$file_list                =NULL;
        self::$content                  =NULL;
        self::$separator                =NULL;
        self::$headers                  =NULL;

        return true;

    }

    /**
     * @return bool
     */
    private static function set_header_mime(){

        self::$headers       ="From: ".\Config::$project_name." <".self::$email_default.">".EmailConfig::$email_eol;
        self::$headers      .="MIME-Version: 1.0".EmailConfig::$email_eol;
        self::$headers      .="Content-Type: multipart/mixed; boundary=\"".self::$separator."\"".EmailConfig::$email_eol;
        self::$headers      .="Content-Transfer-Encoding: 7bit".EmailConfig::$email_eol;
        self::$headers      .="This is a MIME encoded message.".EmailConfig::$email_eol;

        return true;

    }

    /**
     * @param string|NULL $email
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_header_html(string $email=NULL){

        if(empty($email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Email is empty'
            );

            throw new ParametersException($error);

        }

        $list=array(
            "Reply-To: ".self::$email_default,
            "Return-Path: ".self::$email_default,
            "MIME-Version: 1.0",
            "Content-Type: text/html; charset=\"utf-8\"",
            "Content-Transfer-Encoding: 8bit",
            "From: ".\Config::$project_name." <".self::$email_default.">",
            "To: ".$email." <".$email.">",
            "X-Mailer: PHP/".phpversion(),
            "X-Priority: 3"
        );

        self::$headers=implode(EmailConfig::$email_eol,$list);

        return true;

    }

    /**
     * @return bool
     */
    private static function set_content(){

        self::$content       ="--".self::$separator.EmailConfig::$email_eol;
        self::$content      .="Content-Type: text/html; charset=\"utf-8\"".EmailConfig::$email_eol;
//        self::$content      .= "Content-Type: text/html; charset=\"iso-8859-1\"".EmailConfig::$email_eol;
        self::$content      .="Content-Transfer-Encoding: 8bit".EmailConfig::$email_eol;
        self::$content      .="<html>".EmailConfig::$email_eol;
        self::$content      .="<head>".EmailConfig::$email_eol;
        self::$content      .="".EmailConfig::$email_eol;
        self::$content      .="<title>".self::$title."</title>".EmailConfig::$email_eol;
        self::$content      .="</head>".EmailConfig::$email_eol;
        self::$content      .="<body>";
        self::$content      .=self::$message.EmailConfig::$email_eol;
        self::$content      .="</body>";
        self::$content      .="</html>";

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function send(){

        $send_list=[];

        foreach(self::$email_list as $email){

            self::set_header_html($email);

            $send_list[$email]=mail($email,self::$title,self::$message,self::$headers);

        }

        return count($send_list)>0;

    }

    /**
     * @return bool
     */
    private static function attach_file_list(){

        foreach(self::$file_list as $file_path)
            if(file_exists($file_path)){

                $path_list          =mb_split('\/',$file_path);
                $file_name          =end($path_list);
                $content            =file_get_contents($file_path);
                $content            =chunk_split(base64_encode($content));
                
                self::$content      .= "--".self::$separator.EmailConfig::$email_eol;
                self::$content      .= "Content-Type: application/octet-stream; name=\"".$file_name."\"".EmailConfig::$email_eol;
                self::$content      .= "Content-Transfer-Encoding: base64".EmailConfig::$email_eol;
                self::$content      .= "Content-Disposition: attachment".EmailConfig::$email_eol;
                self::$content      .= $content.EmailConfig::$email_eol;
                self::$content      .= "--".self::$separator."--";

            }

        return true;
    
    }

    /**
     * @param array $email_list
     * @param string|NULL $title
     * @param string|NULL $message
     * @param string|NULL $email_default
     * @param array $file_list
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(array $email_list=[],string $title=NULL,string $message=NULL,string $email_default=NULL,array $file_list=[]){

        $error_info_list=[];

        if(count($email_list)==0)
            $error_info_list[]='Email list is empty';

        if(empty($title))
            $error_info_list[]='Title is empty';

        if(empty($message))
            $error_info_list[]='Message is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$email_list=EmailValidation::get_valid_email_list($email_list);

        if(count(self::$email_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Email list is empty'
            );

            throw new ParametersException($error);

        }

        self::$title            =$title;
        self::$message          =$message;
        self::$file_list        =empty($file_list)?[]:$file_list;
        self::$email_default    =empty($email_default)?(\Config::$email_default):strtolower($email_default);

        self::$separator        =md5(time());
        self::$content          ='';

        self::set_content();
        self::attach_file_list();

        return self::send();

    }

}