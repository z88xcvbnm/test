<?php

namespace Core\Module\Google;

use Core\Module\Exception\ParametersException;

class GooglePlay{

    /**
     * @param string|NULL $name
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_app_name(string $name=NULL){

        if(empty($name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'App name is empty'
            );

            throw new ParametersException($error);

        }

        return array_search($name,GooglePlayConfig::$app_name_list)!==false;

    }

    /**
     * @param string|NULL $purchase
     * @param string|NULL $signature
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_valid_purchase(string $purchase=NULL,string $signature=NULL){

        $error_info_list=[];

        if(empty($purchase))
            $error_info_list[]='Purchase is empty';

        if(empty($signature))
            $error_info_list[]='Signature is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $key        ="-----BEGIN PUBLIC KEY-----\n".chunk_split(GooglePlayConfig::$public_key,64,"\n").'-----END PUBLIC KEY-----';
        $key        =openssl_get_publickey($key);
        $binary     =base64_decode($signature);
        $r          =openssl_verify($purchase,$binary,$key,OPENSSL_ALGO_SHA1);

        return($r===1);

    }

}