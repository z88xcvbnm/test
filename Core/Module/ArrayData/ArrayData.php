<?php

namespace Core\Module\ArrayData;

use Core\Module\Data\Data;
use Core\Module\Exception\ParametersException;

class ArrayData{

    /**
     * @param null $needle
     * @param array $array
     * @return false|int|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_index_for_value_in_array($needle=NULL,array $array=[]){

        if(empty($needle)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Needle is empty'
            );

            throw new ParametersException($error);

        }

        return array_search($needle,$array);

    }

    /**
     * @param int|NULL $index
     * @param array $array
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_index_from_array(int $index=NULL,array $array=[]){

        $error_info_list=[];

        if(is_null($index))
            $error_info_list[]='Index is NULL';

        if(count($array)==0)
            $error_info_list[]='Array is empty';
        else if($index>=count($array))
            $error_info_list[]='Index is not exists in array';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        unset($array[$index]);

        return array_values($array);

    }

    /**
     * @param null $needle
     * @param array $array
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_value_from_array($needle=NULL,array $array=[]){

        $error_info_list=[];

        if(empty($needle))
            $error_info_list[]='Needle is empty';

        if(count($array)==0)
            $error_info_list[]='Array is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $index=self::get_index_for_value_in_array($needle,$array);

        unset($array[$index]);

        if(!Data::is_associative_array($array))
            $array=array_values($array);

        return $array;

    }

    /**
     * @param array $array
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_last_element_in_array(array $array=[]){

        if(count($array)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Array is empty'
            );

            throw new ParametersException($error);

        }

        if(is_null(array_pop($array))){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Last element in array cannot remove'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @param array $array
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_empty_elements_in_array(array $array=[]){

        if(count($array)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Array is empty'
            );

            throw new ParametersException($error);

        }

        if(Data::is_associative_array($array)){

            foreach($array as $key=>$value)
                if(empty($value))
                    unset($array[$key]);

        }

        return $array;

    }

    /**
     * @param array $array
     * @return array
     */
    public  static function remove_repeat_elements_in_array(array $array=[]){

        if(count($array)==0)
            return $array;

        return array_unique($array);

    }

}