<?php

namespace Core\Module\Auth;

use Core\Module\Encrypt\Hash;
use Core\Module\Exception\AccessDeniedException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\UnknownException;
use Core\Module\Token\Token;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserBlock;
use Core\Module\User\UserLogin;
use Core\Module\User\UserLoginValidation;
use Core\Module\User\UserRemove;

class UserAuth{

    /** @var int */
    private static $user_ID;

    /** @var string */
    private static $login;

    /** @var string */
    private static $password;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$user_ID          =NULL;
        self::$login            =NULL;
        self::$password         =NULL;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_access(){

        if(empty(User::$user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $user_access_type_name_list=UserAccess::get_user_access_type_name_list(User::$user_ID);

        if(count($user_access_type_name_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Access denied'
            );

            throw new ParametersException($error);

        }

        return UserAccess::set_user_access_type_name_list_default($user_access_type_name_list);

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws UnknownException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_user_ID(){

        $user_data=UserLogin::get_user_login_info(self::$login,self::$password);

        if(empty($user_data)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Access denied'
            );

            throw new ParametersException($error);

        }

        if(!empty($user_data['type'])){

            $user_block=UserBlock::get_user_block_user_info($user_data['user_ID']);

            if(!empty($user_block)){

                $error=array(
                    'title'     =>'Access denied',
                    'info'      =>$user_block['user_info']
                );

                throw new AccessDeniedException($error);

            }

            $user_remove=UserRemove::get_user_remove_user_info($user_data['user_ID']);

            if(!empty($user_remove)){

                $error=array(
                    'title'     =>'Access denied',
                    'info'      =>$user_remove['user_info']
                );

                throw new AccessDeniedException($error);

            }

            throw new UnknownException();

        }

        User::$user_ID=$user_data['user_ID'];

        return true;

    }

    /**
     * @return bool|int
     */
    private static function check_user_login(){

        return UserLoginValidation::is_valid_user_login(self::$login);

    }

    /**
     * @return bool|int
     */
    private static function check_user_password(){

        return UserLoginValidation::is_valid_user_password(self::$password);

    }

    /**
     * @return bool
     */
    private static function check_parameters(){

        return
                  self::check_user_login()
                &&self::check_user_password();

    }

    /**
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws UnknownException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::check_parameters())
            if(self::set_user_ID())
                if(self::set_user_access())
                    if(Token::update_token_user_ID_default())
                        return true;

        $error=array(
            'title'     =>'Parameters problem',
            'info'      =>'Access denied'
        );

        throw new ParametersException($error);

    }

    /**
     * @param string|NULL $login
     * @param string|NULL $pass
     * @return bool
     * @throws AccessDeniedException
     * @throws ParametersException
     * @throws UnknownException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $login=NULL,string $pass=NULL){

        $error_info_list=[];

        if(empty($login))
            $error_info_list[]='Login is empty';

        if(empty($pass))
            $error_info_list[]='Pass is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$login        =$login;
        self::$password     =Hash::get_sha1_encode($pass);

        return self::set();

    }

}