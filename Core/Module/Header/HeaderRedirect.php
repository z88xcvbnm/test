<?php

namespace Core\Module\Header;

use Core\Module\Exception\ParametersException;

class HeaderRedirect{

    /**
     * @param int|NULL $code
     * @param string|NULL $link
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $code=NULL,string $link=NULL){

        if(empty($code))
            $code=301;

        if(empty($link))
            $link='/';

        switch($code){

            case 301:
            case 302:
            case 303:
            case 307:{

                header('Location: '.$link,true,$code);

                return true;

            }

            default:{

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>'Header code for redirect is not valid'
                );

                throw new ParametersException($error);

            }

        }

    }

}