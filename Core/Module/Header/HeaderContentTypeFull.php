<?php

namespace Core\Module\Header;

class HeaderContentTypeFull{

    /** @var array */
    public  static $content_type_list=array(
        'application/vnd.hzn-3d-crossword'=>array(
			'title'				=>'3D Crossword Plugin',
			'content_type'		=>'application/vnd.hzn-3d-crossword',
			'file_extension'			=>'x3d'
		),
		'video/3gpp'=>array(
			'title'				=>'3GP',
			'content_type'		=>'video/3gpp',
			'file_extension'			=>'3gp'
		),
		'video/3gpp2'=>array(
			'title'				=>'3GP2',
			'content_type'		=>'video/3gpp2',
			'file_extension'			=>'3g2'
		),
		'application/vnd.mseq'=>array(
			'title'				=>'3GPP MSEQ File',
			'content_type'		=>'application/vnd.mseq',
			'file_extension'			=>'seq'
		),
		'application/vnd.3m.post-it-notes'=>array(
			'title'				=>'3M Post It Notes',
			'content_type'		=>'application/vnd.3m.post-it-notes',
			'file_extension'			=>'pwn'
		),
		'application/vnd.3gpp.pic-bw-large'=>array(
			'title'				=>'3rd Generation Partnership Project - Pic Large',
			'content_type'		=>'application/vnd.3gpp.pic-bw-large',
			'file_extension'			=>'plb'
		),
		'application/vnd.3gpp.pic-bw-small'=>array(
			'title'				=>'3rd Generation Partnership Project - Pic Small',
			'content_type'		=>'application/vnd.3gpp.pic-bw-small',
			'file_extension'			=>'psb'
		),
		'application/vnd.3gpp.pic-bw-var'=>array(
			'title'				=>'3rd Generation Partnership Project - Pic Var',
			'content_type'		=>'application/vnd.3gpp.pic-bw-var',
			'file_extension'			=>'pvb'
		),
		'application/vnd.3gpp2.tcap'=>array(
			'title'				=>'3rd Generation Partnership Project - Transaction Capabilities Application Part',
			'content_type'		=>'application/vnd.3gpp2.tcap',
			'file_extension'			=>'cap'
		),
		'application/x-7z-compressed'=>array(
			'title'				=>'7-Zip',
			'content_type'		=>'application/x-7z-compressed',
			'file_extension'			=>'7z'
		),
		'application/x-abiword'=>array(
			'title'				=>'AbiWord',
			'content_type'		=>'application/x-abiword',
			'file_extension'			=>'abw'
		),
		'application/x-ace-compressed'=>array(
			'title'				=>'Ace Archive',
			'content_type'		=>'application/x-ace-compressed',
			'file_extension'			=>'ace'
		),
		'application/vnd.americandynamics.acc'=>array(
			'title'				=>'Active Content Compression',
			'content_type'		=>'application/vnd.americandynamics.acc',
			'file_extension'			=>'acc'
		),
		'application/vnd.acucobol'=>array(
			'title'				=>'ACU Cobol',
			'content_type'		=>'application/vnd.acucobol',
			'file_extension'			=>'acu'
		),
		'application/vnd.acucorp'=>array(
			'title'				=>'ACU Cobol',
			'content_type'		=>'application/vnd.acucorp',
			'file_extension'			=>'atc'
		),
		'audio/adpcm'=>array(
			'title'				=>'Adaptive differential pulse-code modulation',
			'content_type'		=>'audio/adpcm',
			'file_extension'			=>'adp'
		),
		'application/x-authorware-bin'=>array(
			'title'				=>'Adobe (Macropedia) Authorware - Binary File',
			'content_type'		=>'application/x-authorware-bin',
			'file_extension'			=>'aab'
		),
		'application/x-authorware-map'=>array(
			'title'				=>'Adobe (Macropedia) Authorware - Map',
			'content_type'		=>'application/x-authorware-map',
			'file_extension'			=>'aam'
		),
		'application/x-authorware-seg'=>array(
			'title'				=>'Adobe (Macropedia) Authorware - Segment File',
			'content_type'		=>'application/x-authorware-seg',
			'file_extension'			=>'aas'
		),
		'application/vnd.adobe.air-application-installer-package+zip'=>array(
			'title'				=>'Adobe AIR Application',
			'content_type'		=>'application/vnd.adobe.air-application-installer-package+zip',
			'file_extension'			=>'air'
		),
		'application/x-shockwave-flash'=>array(
			'title'				=>'Adobe Flash',
			'content_type'		=>'application/x-shockwave-flash',
			'file_extension'			=>'swf'
		),
		'application/vnd.adobe.fxp'=>array(
			'title'				=>'Adobe Flex Project',
			'content_type'		=>'application/vnd.adobe.fxp',
			'file_extension'			=>'fxp'
		),
		'application/pdf'=>array(
			'title'				=>'Adobe Portable Document Format',
			'content_type'		=>'application/pdf',
			'file_extension'			=>'pdf'
		),
		'application/vnd.cups-ppd'=>array(
			'title'				=>'Adobe PostScript Printer Description File Format',
			'content_type'		=>'application/vnd.cups-ppd',
			'file_extension'			=>'ppd'
		),
		'application/x-director'=>array(
			'title'				=>'Adobe Shockwave Player',
			'content_type'		=>'application/x-director',
			'file_extension'			=>'dir'
		),
		'application/vnd.adobe.xdp+xml'=>array(
			'title'				=>'Adobe XML Data Package',
			'content_type'		=>'application/vnd.adobe.xdp+xml',
			'file_extension'			=>'xdp'
		),
		'application/vnd.adobe.xfdf'=>array(
			'title'				=>'Adobe XML Forms Data Format',
			'content_type'		=>'application/vnd.adobe.xfdf',
			'file_extension'			=>'fdf'
		),
		'audio/x-aac'=>array(
			'title'				=>'Advanced Audio Coding (AAC)',
			'content_type'		=>'audio/x-aac',
			'file_extension'			=>'aac'
		),
		'application/vnd.ahead.space'=>array(
			'title'				=>'Ahead AIR Application',
			'content_type'		=>'application/vnd.ahead.space',
			'file_extension'			=>'ead'
		),
		'application/vnd.airzip.filesecure.azf'=>array(
			'title'				=>'AirZip FileSECURE',
			'content_type'		=>'application/vnd.airzip.filesecure.azf',
			'file_extension'			=>'azf'
		),
		'application/vnd.airzip.filesecure.azs'=>array(
			'title'				=>'AirZip FileSECURE',
			'content_type'		=>'application/vnd.airzip.filesecure.azs',
			'file_extension'			=>'azs'
		),
		'application/vnd.amazon.ebook'=>array(
			'title'				=>'Amazon Kindle eBook format',
			'content_type'		=>'application/vnd.amazon.ebook',
			'file_extension'			=>'azw'
		),
		'application/vnd.amiga.ami'=>array(
			'title'				=>'AmigaDE',
			'content_type'		=>'application/vnd.amiga.ami',
			'file_extension'			=>'ami'
		),
		'application/andrew-inset'=>array(
			'title'				=>'Andrew Toolkit',
			'content_type'		=>'application/andrew-inset',
			'file_extension'			=>'N/A'
		),
		'application/vnd.android.package-archive'=>array(
			'title'				=>'Android Package Archive',
			'content_type'		=>'application/vnd.android.package-archive',
			'file_extension'			=>'apk'
		),
		'application/vnd.anser-web-certificate-issue-initiation'=>array(
			'title'				=>'ANSER-WEB Terminal Client - Certificate Issue',
			'content_type'		=>'application/vnd.anser-web-certificate-issue-initiation',
			'file_extension'			=>'cii'
		),
		'application/vnd.anser-web-funds-transfer-initiation'=>array(
			'title'				=>'ANSER-WEB Terminal Client - Web Funds Transfer',
			'content_type'		=>'application/vnd.anser-web-funds-transfer-initiation',
			'file_extension'			=>'fti'
		),
		'application/vnd.antix.game-component'=>array(
			'title'				=>'Antix Game Player',
			'content_type'		=>'application/vnd.antix.game-component',
			'file_extension'			=>'atx'
		),
		'application/vnd.apple.installer+xml'=>array(
			'title'				=>'Apple Installer Package',
			'content_type'		=>'application/vnd.apple.installer+xml',
			'file_extension'			=>'pkg'
		),
		'application/applixware'=>array(
			'title'				=>'Applixware',
			'content_type'		=>'application/applixware',
			'file_extension'			=>'aw'
		),
		'application/vnd.hhe.lesson-player'=>array(
			'title'				=>'Archipelago Lesson Player',
			'content_type'		=>'application/vnd.hhe.lesson-player',
			'file_extension'			=>'les'
		),
		'application/vnd.aristanetworks.swi'=>array(
			'title'				=>'Arista Networks Software Image',
			'content_type'		=>'application/vnd.aristanetworks.swi',
			'file_extension'			=>'swi'
		),
		'text/x-asm'=>array(
			'title'				=>'Assembler Source File',
			'content_type'		=>'text/x-asm',
			'file_extension'			=>'s'
		),
		'application/atomcat+xml'=>array(
			'title'				=>'Atom Publishing Protocol',
			'content_type'		=>'application/atomcat+xml',
			'file_extension'			=>'cat'
		),
		'application/atomsvc+xml'=>array(
			'title'				=>'Atom Publishing Protocol Service Document',
			'content_type'		=>'application/atomsvc+xml',
			'file_extension'			=>'svc'
		),
		'application/atom+xml'=>array(
			'title'				=>'Atom Syndication Format',
			'content_type'		=>'application/atom+xml',
			'file_extension'			=>'xml'
		),
		'application/pkix-attr-cert'=>array(
			'title'				=>'Attribute Certificate',
			'content_type'		=>'application/pkix-attr-cert',
			'file_extension'			=>'ac'
		),
		'audio/x-aiff'=>array(
			'title'				=>'Audio Interchange File Format',
			'content_type'		=>'audio/x-aiff',
			'file_extension'			=>'aif'
		),
		'video/x-msvideo'=>array(
			'title'				=>'Audio Video Interleave (AVI)',
			'content_type'		=>'video/x-msvideo',
			'file_extension'			=>'avi'
		),
		'application/vnd.audiograph'=>array(
			'title'				=>'Audiograph',
			'content_type'		=>'application/vnd.audiograph',
			'file_extension'			=>'aep'
		),
		'image/vnd.dxf'=>array(
			'title'				=>'AutoCAD DXF',
			'content_type'		=>'image/vnd.dxf',
			'file_extension'			=>'dxf'
		),
		'model/vnd.dwf'=>array(
			'title'				=>'Autodesk Design Web Format (DWF)',
			'content_type'		=>'model/vnd.dwf',
			'file_extension'			=>'dwf'
		),
		'text/plain-bas'=>array(
			'title'				=>'BAS Partitur Format',
			'content_type'		=>'text/plain-bas',
			'file_extension'			=>'par'
		),
		'application/x-bcpio'=>array(
			'title'				=>'Binary CPIO Archive',
			'content_type'		=>'application/x-bcpio',
			'file_extension'			=>'pio'
		),
		'application/octet-stream'=>array(
			'title'				=>'Binary Data',
			'content_type'		=>'application/octet-stream',
			'file_extension'			=>'bin'
		),
		'image/bmp'=>array(
			'title'				=>'Bitmap Image File',
			'content_type'		=>'image/bmp',
			'file_extension'			=>'bmp'
		),
		'application/x-bittorrent'=>array(
			'title'				=>'BitTorrent',
			'content_type'		=>'application/x-bittorrent',
			'file_extension'			=>'ent'
		),
		'application/vnd.rim.cod'=>array(
			'title'				=>'Blackberry COD File',
			'content_type'		=>'application/vnd.rim.cod',
			'file_extension'			=>'cod'
		),
		'application/vnd.blueice.multipass'=>array(
			'title'				=>'Blueice Research Multipass',
			'content_type'		=>'application/vnd.blueice.multipass',
			'file_extension'			=>'mpm'
		),
		'application/vnd.bmi'=>array(
			'title'				=>'BMI Drawing Data Interchange',
			'content_type'		=>'application/vnd.bmi',
			'file_extension'			=>'bmi'
		),
		'application/x-sh'=>array(
			'title'				=>'Bourne Shell Script',
			'content_type'		=>'application/x-sh',
			'file_extension'			=>'sh'
		),
		'image/prs.btif'=>array(
			'title'				=>'BTIF',
			'content_type'		=>'image/prs.btif',
			'file_extension'			=>'tif'
		),
		'application/vnd.businessobjects'=>array(
			'title'				=>'BusinessObjects',
			'content_type'		=>'application/vnd.businessobjects',
			'file_extension'			=>'rep'
		),
		'application/x-bzip'=>array(
			'title'				=>'Bzip Archive',
			'content_type'		=>'application/x-bzip',
			'file_extension'			=>'bz'
		),
		'application/x-bzip2'=>array(
			'title'				=>'Bzip2 Archive',
			'content_type'		=>'application/x-bzip2',
			'file_extension'			=>'bz2'
		),
		'application/x-csh'=>array(
			'title'				=>'C Shell Script',
			'content_type'		=>'application/x-csh',
			'file_extension'			=>'csh'
		),
		'text/x-c'=>array(
			'title'				=>'C Source File',
			'content_type'		=>'text/x-c',
			'file_extension'			=>'c'
		),
		'application/vnd.chemdraw+xml'=>array(
			'title'				=>'CambridgeSoft Chem Draw',
			'content_type'		=>'application/vnd.chemdraw+xml',
			'file_extension'			=>'xml'
		),
		'text/css'=>array(
			'title'				=>'Cascading Style Sheets (CSS)',
			'content_type'		=>'text/css',
			'file_extension'			=>'css'
		),
		'chemical/x-cdx'=>array(
			'title'				=>'ChemDraw eXchange file',
			'content_type'		=>'chemical/x-cdx',
			'file_extension'			=>'cdx'
		),
		'chemical/x-cml'=>array(
			'title'				=>'Chemical Markup Language',
			'content_type'		=>'chemical/x-cml',
			'file_extension'			=>'cml'
		),
		'chemical/x-csml'=>array(
			'title'				=>'Chemical Style Markup Language',
			'content_type'		=>'chemical/x-csml',
			'file_extension'			=>'sml'
		),
		'application/vnd.contact.cmsg'=>array(
			'title'				=>'CIM Database',
			'content_type'		=>'application/vnd.contact.cmsg',
			'file_extension'			=>'msg'
		),
		'application/vnd.claymore'=>array(
			'title'				=>'Claymore Data Files',
			'content_type'		=>'application/vnd.claymore',
			'file_extension'			=>'cla'
		),
		'application/vnd.clonk.c4group'=>array(
			'title'				=>'Clonk Game',
			'content_type'		=>'application/vnd.clonk.c4group',
			'file_extension'			=>'c4g'
		),
		'image/vnd.dvb.subtitle'=>array(
			'title'				=>'Close Captioning - Subtitle',
			'content_type'		=>'image/vnd.dvb.subtitle',
			'file_extension'			=>'sub'
		),
		'application/cdmi-capability'=>array(
			'title'				=>'Cloud Data Management Interface (CDMI) - Capability',
			'content_type'		=>'application/cdmi-capability',
			'file_extension'			=>'mia'
		),
		'application/cdmi-container'=>array(
			'title'				=>'Cloud Data Management Interface (CDMI) - Contaimer',
			'content_type'		=>'application/cdmi-container',
			'file_extension'			=>'mic'
		),
		'application/cdmi-domain'=>array(
			'title'				=>'Cloud Data Management Interface (CDMI) - Domain',
			'content_type'		=>'application/cdmi-domain',
			'file_extension'			=>'mid'
		),
		'application/cdmi-object'=>array(
			'title'				=>'Cloud Data Management Interface (CDMI) - Object',
			'content_type'		=>'application/cdmi-object',
			'file_extension'			=>'mio'
		),
		'application/cdmi-queue'=>array(
			'title'				=>'Cloud Data Management Interface (CDMI) - Queue',
			'content_type'		=>'application/cdmi-queue',
			'file_extension'			=>'miq'
		),
		'application/vnd.cluetrust.cartomobile-config'=>array(
			'title'				=>'ClueTrust CartoMobile - Config',
			'content_type'		=>'application/vnd.cluetrust.cartomobile-config',
			'file_extension'			=>'amc'
		),
		'application/vnd.cluetrust.cartomobile-config-pkg'=>array(
			'title'				=>'ClueTrust CartoMobile - Config Package',
			'content_type'		=>'application/vnd.cluetrust.cartomobile-config-pkg',
			'file_extension'			=>'amz'
		),
		'image/x-cmu-raster'=>array(
			'title'				=>'CMU Image',
			'content_type'		=>'image/x-cmu-raster',
			'file_extension'			=>'ras'
		),
		'model/vnd.collada+xml'=>array(
			'title'				=>'COLLADA',
			'content_type'		=>'model/vnd.collada+xml',
			'file_extension'			=>'dae'
		),
		'text/csv'=>array(
			'title'				=>'Comma-Seperated Values',
			'content_type'		=>'text/csv',
			'file_extension'			=>'csv'
		),
		'application/mac-compactpro'=>array(
			'title'				=>'Compact Pro',
			'content_type'		=>'application/mac-compactpro',
			'file_extension'			=>'cpt'
		),
		'application/vnd.wap.wmlc'=>array(
			'title'				=>'Compiled Wireless Markup Language (WMLC)',
			'content_type'		=>'application/vnd.wap.wmlc',
			'file_extension'			=>'mlc'
		),
		'image/cgm'=>array(
			'title'				=>'Computer Graphics Metafile',
			'content_type'		=>'image/cgm',
			'file_extension'			=>'cgm'
		),
		'x-conference/x-cooltalk'=>array(
			'title'				=>'CoolTalk',
			'content_type'		=>'x-conference/x-cooltalk',
			'file_extension'			=>'ice'
		),
		'image/x-cmx'=>array(
			'title'				=>'Corel Metafile Exchange (CMX)',
			'content_type'		=>'image/x-cmx',
			'file_extension'			=>'cmx'
		),
		'application/vnd.xara'=>array(
			'title'				=>'CorelXARA',
			'content_type'		=>'application/vnd.xara',
			'file_extension'			=>'xar'
		),
		'application/vnd.cosmocaller'=>array(
			'title'				=>'CosmoCaller',
			'content_type'		=>'application/vnd.cosmocaller',
			'file_extension'			=>'cmc'
		),
		'application/x-cpio'=>array(
			'title'				=>'CPIO Archive',
			'content_type'		=>'application/x-cpio',
			'file_extension'			=>'pio'
		),
		'application/vnd.crick.clicker'=>array(
			'title'				=>'CrickSoftware - Clicker',
			'content_type'		=>'application/vnd.crick.clicker',
			'file_extension'			=>'lkx'
		),
		'application/vnd.crick.clicker.keyboard'=>array(
			'title'				=>'CrickSoftware - Clicker - Keyboard',
			'content_type'		=>'application/vnd.crick.clicker.keyboard',
			'file_extension'			=>'lkk'
		),
		'application/vnd.crick.clicker.palette'=>array(
			'title'				=>'CrickSoftware - Clicker - Palette',
			'content_type'		=>'application/vnd.crick.clicker.palette',
			'file_extension'			=>'lkp'
		),
		'application/vnd.crick.clicker.template'=>array(
			'title'				=>'CrickSoftware - Clicker - Template',
			'content_type'		=>'application/vnd.crick.clicker.template',
			'file_extension'			=>'lkt'
		),
		'application/vnd.crick.clicker.wordbank'=>array(
			'title'				=>'CrickSoftware - Clicker - Wordbank',
			'content_type'		=>'application/vnd.crick.clicker.wordbank',
			'file_extension'			=>'lkw'
		),
		'application/vnd.criticaltools.wbs+xml'=>array(
			'title'				=>'Critical Tools - PERT Chart EXPERT',
			'content_type'		=>'application/vnd.criticaltools.wbs+xml',
			'file_extension'			=>'wbs'
		),
		'application/vnd.rig.cryptonote'=>array(
			'title'				=>'CryptoNote',
			'content_type'		=>'application/vnd.rig.cryptonote',
			'file_extension'			=>'ote'
		),
		'chemical/x-cif'=>array(
			'title'				=>'Crystallographic Interchange Format',
			'content_type'		=>'chemical/x-cif',
			'file_extension'			=>'cif'
		),
		'chemical/x-cmdf'=>array(
			'title'				=>'CrystalMaker Data Format',
			'content_type'		=>'chemical/x-cmdf',
			'file_extension'			=>'mdf'
		),
		'application/cu-seeme'=>array(
			'title'				=>'CU-SeeMe',
			'content_type'		=>'application/cu-seeme',
			'file_extension'			=>'cu'
		),
		'application/prs.cww'=>array(
			'title'				=>'CU-Writer',
			'content_type'		=>'application/prs.cww',
			'file_extension'			=>'cww'
		),
		'text/vnd.curl'=>array(
			'title'				=>'Curl - Applet',
			'content_type'		=>'text/vnd.curl',
			'file_extension'			=>'url'
		),
		'text/vnd.curl.dcurl'=>array(
			'title'				=>'Curl - Detached Applet',
			'content_type'		=>'text/vnd.curl.dcurl',
			'file_extension'			=>'url'
		),
		'text/vnd.curl.mcurl'=>array(
			'title'				=>'Curl - Manifest File',
			'content_type'		=>'text/vnd.curl.mcurl',
			'file_extension'			=>'url'
		),
		'text/vnd.curl.scurl'=>array(
			'title'				=>'Curl - Source Code',
			'content_type'		=>'text/vnd.curl.scurl',
			'file_extension'			=>'url'
		),
		'application/vnd.curl.car'=>array(
			'title'				=>'CURL Applet',
			'content_type'		=>'application/vnd.curl.car',
			'file_extension'			=>'car'
		),
		'application/vnd.curl.pcurl'=>array(
			'title'				=>'CURL Applet',
			'content_type'		=>'application/vnd.curl.pcurl',
			'file_extension'			=>'url'
		),
		'application/vnd.yellowriver-custom-menu'=>array(
			'title'				=>'CustomMenu',
			'content_type'		=>'application/vnd.yellowriver-custom-menu',
			'file_extension'			=>'cmp'
		),
		'application/dssc+der'=>array(
			'title'				=>'Data Structure for the Security Suitability of Cryptographic Algorithms',
			'content_type'		=>'application/dssc+der',
			'file_extension'			=>'ssc'
		),
		'application/dssc+xml'=>array(
			'title'				=>'Data Structure for the Security Suitability of Cryptographic Algorithms',
			'content_type'		=>'application/dssc+xml',
			'file_extension'			=>'ssc'
		),
		'application/x-debian-package'=>array(
			'title'				=>'Debian Package',
			'content_type'		=>'application/x-debian-package',
			'file_extension'			=>'deb'
		),
		'audio/vnd.dece.audio'=>array(
			'title'				=>'DECE Audio',
			'content_type'		=>'audio/vnd.dece.audio',
			'file_extension'			=>'uva'
		),
		'image/vnd.dece.graphic'=>array(
			'title'				=>'DECE Graphic',
			'content_type'		=>'image/vnd.dece.graphic',
			'file_extension'			=>'uvi'
		),
		'video/vnd.dece.hd'=>array(
			'title'				=>'DECE High Definition Video',
			'content_type'		=>'video/vnd.dece.hd',
			'file_extension'			=>'uvh'
		),
		'video/vnd.dece.mobile'=>array(
			'title'				=>'DECE Mobile Video',
			'content_type'		=>'video/vnd.dece.mobile',
			'file_extension'			=>'uvm'
		),
		'video/vnd.uvvu.mp4'=>array(
			'title'				=>'DECE MP4',
			'content_type'		=>'video/vnd.uvvu.mp4',
			'file_extension'			=>'uvu'
		),
		'video/vnd.dece.pd'=>array(
			'title'				=>'DECE PD Video',
			'content_type'		=>'video/vnd.dece.pd',
			'file_extension'			=>'uvp'
		),
		'video/vnd.dece.sd'=>array(
			'title'				=>'DECE SD Video',
			'content_type'		=>'video/vnd.dece.sd',
			'file_extension'			=>'uvs'
		),
		'video/vnd.dece.video'=>array(
			'title'				=>'DECE Video',
			'content_type'		=>'video/vnd.dece.video',
			'file_extension'			=>'uvv'
		),
		'application/x-dvi'=>array(
			'title'				=>'Device Independent File Format (DVI)',
			'content_type'		=>'application/x-dvi',
			'file_extension'			=>'dvi'
		),
		'application/vnd.fdsn.seed'=>array(
			'title'				=>'Digital Siesmograph Networks - SEED Datafiles',
			'content_type'		=>'application/vnd.fdsn.seed',
			'file_extension'			=>'eed'
		),
		'application/x-dtbook+xml'=>array(
			'title'				=>'Digital Talking Book',
			'content_type'		=>'application/x-dtbook+xml',
			'file_extension'			=>'dtb'
		),
		'application/x-dtbresource+xml'=>array(
			'title'				=>'Digital Talking Book - Resource File',
			'content_type'		=>'application/x-dtbresource+xml',
			'file_extension'			=>'res'
		),
		'application/vnd.dvb.ait'=>array(
			'title'				=>'Digital Video Broadcasting',
			'content_type'		=>'application/vnd.dvb.ait',
			'file_extension'			=>'ait'
		),
		'application/vnd.dvb.service'=>array(
			'title'				=>'Digital Video Broadcasting',
			'content_type'		=>'application/vnd.dvb.service',
			'file_extension'			=>'svc'
		),
		'audio/vnd.digital-winds'=>array(
			'title'				=>'Digital Winds Music',
			'content_type'		=>'audio/vnd.digital-winds',
			'file_extension'			=>'eol'
		),
		'image/vnd.djvu'=>array(
			'title'				=>'DjVu',
			'content_type'		=>'image/vnd.djvu',
			'file_extension'			=>'jvu'
		),
		'application/xml-dtd'=>array(
			'title'				=>'Document Type Definition',
			'content_type'		=>'application/xml-dtd',
			'file_extension'			=>'dtd'
		),
		'application/vnd.dolby.mlp'=>array(
			'title'				=>'Dolby Meridian Lossless Packing',
			'content_type'		=>'application/vnd.dolby.mlp',
			'file_extension'			=>'mlp'
		),
		'application/x-doom'=>array(
			'title'				=>'Doom Video Game',
			'content_type'		=>'application/x-doom',
			'file_extension'			=>'wad'
		),
		'application/vnd.dpgraph'=>array(
			'title'				=>'DPGraph',
			'content_type'		=>'application/vnd.dpgraph',
			'file_extension'			=>'dpg'
		),
		'audio/vnd.dra'=>array(
			'title'				=>'DRA Audio',
			'content_type'		=>'audio/vnd.dra',
			'file_extension'			=>'dra'
		),
		'application/vnd.dreamfactory'=>array(
			'title'				=>'DreamFactory',
			'content_type'		=>'application/vnd.dreamfactory',
			'file_extension'			=>'fac'
		),
		'audio/vnd.dts'=>array(
			'title'				=>'DTS Audio',
			'content_type'		=>'audio/vnd.dts',
			'file_extension'			=>'dts'
		),
		'audio/vnd.dts.hd'=>array(
			'title'				=>'DTS High Definition Audio',
			'content_type'		=>'audio/vnd.dts.hd',
			'file_extension'			=>'shd'
		),
		'image/vnd.dwg'=>array(
			'title'				=>'DWG Drawing',
			'content_type'		=>'image/vnd.dwg',
			'file_extension'			=>'dwg'
		),
		'application/vnd.dynageo'=>array(
			'title'				=>'DynaGeo',
			'content_type'		=>'application/vnd.dynageo',
			'file_extension'			=>'geo'
		),
		'application/ecmascript'=>array(
			'title'				=>'ECMAScript',
			'content_type'		=>'application/ecmascript',
			'file_extension'			=>'es'
		),
		'application/vnd.ecowin.chart'=>array(
			'title'				=>'EcoWin Chart',
			'content_type'		=>'application/vnd.ecowin.chart',
			'file_extension'			=>'mag'
		),
		'image/vnd.fujixerox.edmics-mmr'=>array(
			'title'				=>'EDMICS 2000',
			'content_type'		=>'image/vnd.fujixerox.edmics-mmr',
			'file_extension'			=>'mmr'
		),
		'image/vnd.fujixerox.edmics-rlc'=>array(
			'title'				=>'EDMICS 2000',
			'content_type'		=>'image/vnd.fujixerox.edmics-rlc',
			'file_extension'			=>'rlc'
		),
		'application/exi'=>array(
			'title'				=>'Efficient XML Interchange',
			'content_type'		=>'application/exi',
			'file_extension'			=>'exi'
		),
		'application/vnd.proteus.magazine'=>array(
			'title'				=>'EFI Proteus',
			'content_type'		=>'application/vnd.proteus.magazine',
			'file_extension'			=>'mgz'
		),
		'application/epub+zip'=>array(
			'title'				=>'Electronic Publication',
			'content_type'		=>'application/epub+zip',
			'file_extension'			=>'pub'
		),
		'message/rfc822'=>array(
			'title'				=>'Email Message',
			'content_type'		=>'message/rfc822',
			'file_extension'			=>'eml'
		),
		'application/vnd.enliven'=>array(
			'title'				=>'Enliven Viewer',
			'content_type'		=>'application/vnd.enliven',
			'file_extension'			=>'nml'
		),
		'application/vnd.is-xpr'=>array(
			'title'				=>'Express by Infoseek',
			'content_type'		=>'application/vnd.is-xpr',
			'file_extension'			=>'xpr'
		),
		'image/vnd.xiff'=>array(
			'title'				=>'eXtended Image File Format (XIFF)',
			'content_type'		=>'image/vnd.xiff',
			'file_extension'			=>'xif'
		),
		'application/vnd.xfdl'=>array(
			'title'				=>'Extensible Forms Description Language',
			'content_type'		=>'application/vnd.xfdl',
			'file_extension'			=>'fdl'
		),
		'application/emma+xml'=>array(
			'title'				=>'Extensible MultiModal Annotation',
			'content_type'		=>'application/emma+xml',
			'file_extension'			=>'mma'
		),
		'application/vnd.ezpix-album'=>array(
			'title'				=>'EZPix Secure Photo Album',
			'content_type'		=>'application/vnd.ezpix-album',
			'file_extension'			=>'ez2'
		),
		'application/vnd.ezpix-package'=>array(
			'title'				=>'EZPix Secure Photo Album',
			'content_type'		=>'application/vnd.ezpix-package',
			'file_extension'			=>'ez3'
		),
		'image/vnd.fst'=>array(
			'title'				=>'FAST Search & Transfer ASA',
			'content_type'		=>'image/vnd.fst',
			'file_extension'			=>'fst'
		),
		'video/vnd.fvt'=>array(
			'title'				=>'FAST Search & Transfer ASA',
			'content_type'		=>'video/vnd.fvt',
			'file_extension'			=>'fvt'
		),
		'image/vnd.fastbidsheet'=>array(
			'title'				=>'FastBid Sheet',
			'content_type'		=>'image/vnd.fastbidsheet',
			'file_extension'			=>'fbs'
		),
		'application/vnd.denovo.fcselayout-link'=>array(
			'title'				=>'FCS Express Layout Link',
			'content_type'		=>'application/vnd.denovo.fcselayout-link',
			'file_extension'			=>'nch'
		),
		'video/x-f4v'=>array(
			'title'				=>'Flash Video',
			'content_type'		=>'video/x-f4v',
			'file_extension'			=>'f4v'
		),
		'video/x-flv'=>array(
			'title'				=>'Flash Video',
			'content_type'		=>'video/x-flv',
			'file_extension'			=>'flv'
		),
		'image/vnd.fpx'=>array(
			'title'				=>'FlashPix',
			'content_type'		=>'image/vnd.fpx',
			'file_extension'			=>'fpx'
		),
		'image/vnd.net-fpx'=>array(
			'title'				=>'FlashPix',
			'content_type'		=>'image/vnd.net-fpx',
			'file_extension'			=>'npx'
		),
		'text/vnd.fmi.flexstor'=>array(
			'title'				=>'FLEXSTOR',
			'content_type'		=>'text/vnd.fmi.flexstor',
			'file_extension'			=>'flx'
		),
		'video/x-fli'=>array(
			'title'				=>'FLI/FLC Animation Format',
			'content_type'		=>'video/x-fli',
			'file_extension'			=>'fli'
		),
		'application/vnd.fluxtime.clip'=>array(
			'title'				=>'FluxTime Clip',
			'content_type'		=>'application/vnd.fluxtime.clip',
			'file_extension'			=>'ftc'
		),
		'application/vnd.fdf'=>array(
			'title'				=>'Forms Data Format',
			'content_type'		=>'application/vnd.fdf',
			'file_extension'			=>'fdf'
		),
		'text/x-fortran'=>array(
			'title'				=>'Fortran Source File',
			'content_type'		=>'text/x-fortran',
			'file_extension'			=>'f'
		),
		'application/vnd.mif'=>array(
			'title'				=>'FrameMaker Interchange Format',
			'content_type'		=>'application/vnd.mif',
			'file_extension'			=>'mif'
		),
		'application/vnd.framemaker'=>array(
			'title'				=>'FrameMaker Normal Format',
			'content_type'		=>'application/vnd.framemaker',
			'file_extension'			=>'fm'
		),
		'image/x-freehand'=>array(
			'title'				=>'FreeHand MX',
			'content_type'		=>'image/x-freehand',
			'file_extension'			=>'fh'
		),
		'application/vnd.fsc.weblaunch'=>array(
			'title'				=>'Friendly Software Corporation',
			'content_type'		=>'application/vnd.fsc.weblaunch',
			'file_extension'			=>'fsc'
		),
		'application/vnd.frogans.fnc'=>array(
			'title'				=>'Frogans Player',
			'content_type'		=>'application/vnd.frogans.fnc',
			'file_extension'			=>'fnc'
		),
		'application/vnd.frogans.ltf'=>array(
			'title'				=>'Frogans Player',
			'content_type'		=>'application/vnd.frogans.ltf',
			'file_extension'			=>'ltf'
		),
		'application/vnd.fujixerox.ddd'=>array(
			'title'				=>'Fujitsu - Xerox 2D CAD Data',
			'content_type'		=>'application/vnd.fujixerox.ddd',
			'file_extension'			=>'ddd'
		),
		'application/vnd.fujixerox.docuworks'=>array(
			'title'				=>'Fujitsu - Xerox DocuWorks',
			'content_type'		=>'application/vnd.fujixerox.docuworks',
			'file_extension'			=>'xdw'
		),
		'application/vnd.fujixerox.docuworks.binder'=>array(
			'title'				=>'Fujitsu - Xerox DocuWorks Binder',
			'content_type'		=>'application/vnd.fujixerox.docuworks.binder',
			'file_extension'			=>'xbd'
		),
		'application/vnd.fujitsu.oasys'=>array(
			'title'				=>'Fujitsu Oasys',
			'content_type'		=>'application/vnd.fujitsu.oasys',
			'file_extension'			=>'oas'
		),
		'application/vnd.fujitsu.oasys2'=>array(
			'title'				=>'Fujitsu Oasys',
			'content_type'		=>'application/vnd.fujitsu.oasys2',
			'file_extension'			=>'oa2'
		),
		'application/vnd.fujitsu.oasys3'=>array(
			'title'				=>'Fujitsu Oasys',
			'content_type'		=>'application/vnd.fujitsu.oasys3',
			'file_extension'			=>'oa3'
		),
		'application/vnd.fujitsu.oasysgp'=>array(
			'title'				=>'Fujitsu Oasys',
			'content_type'		=>'application/vnd.fujitsu.oasysgp',
			'file_extension'			=>'fg5'
		),
		'application/vnd.fujitsu.oasysprs'=>array(
			'title'				=>'Fujitsu Oasys',
			'content_type'		=>'application/vnd.fujitsu.oasysprs',
			'file_extension'			=>'bh2'
		),
		'application/x-futuresplash'=>array(
			'title'				=>'FutureSplash Animator',
			'content_type'		=>'application/x-futuresplash',
			'file_extension'			=>'spl'
		),
		'application/vnd.fuzzysheet'=>array(
			'title'				=>'FuzzySheet',
			'content_type'		=>'application/vnd.fuzzysheet',
			'file_extension'			=>'fzs'
		),
		'image/g3fax'=>array(
			'title'				=>'G3 Fax Image',
			'content_type'		=>'image/g3fax',
			'file_extension'			=>'g3'
		),
		'application/vnd.gmx'=>array(
			'title'				=>'GameMaker ActiveX',
			'content_type'		=>'application/vnd.gmx',
			'file_extension'			=>'gmx'
		),
		'model/vnd.gtw'=>array(
			'title'				=>'Gen-Trix Studio',
			'content_type'		=>'model/vnd.gtw',
			'file_extension'			=>'gtw'
		),
		'application/vnd.genomatix.tuxedo'=>array(
			'title'				=>'Genomatix Tuxedo Framework',
			'content_type'		=>'application/vnd.genomatix.tuxedo',
			'file_extension'			=>'txd'
		),
		'application/vnd.geogebra.file'=>array(
			'title'				=>'GeoGebra',
			'content_type'		=>'application/vnd.geogebra.file',
			'file_extension'			=>'ggb'
		),
		'application/vnd.geogebra.tool'=>array(
			'title'				=>'GeoGebra',
			'content_type'		=>'application/vnd.geogebra.tool',
			'file_extension'			=>'ggt'
		),
		'model/vnd.gdl'=>array(
			'title'				=>'Geometric Description Language (GDL)',
			'content_type'		=>'model/vnd.gdl',
			'file_extension'			=>'gdl'
		),
		'application/vnd.geometry-explorer'=>array(
			'title'				=>'GeoMetry Explorer',
			'content_type'		=>'application/vnd.geometry-explorer',
			'file_extension'			=>'gex'
		),
		'application/vnd.geonext'=>array(
			'title'				=>'GEONExT and JSXGraph',
			'content_type'		=>'application/vnd.geonext',
			'file_extension'			=>'gxt'
		),
		'application/vnd.geoplan'=>array(
			'title'				=>'GeoplanW',
			'content_type'		=>'application/vnd.geoplan',
			'file_extension'			=>'g2w'
		),
		'application/vnd.geospace'=>array(
			'title'				=>'GeospacW',
			'content_type'		=>'application/vnd.geospace',
			'file_extension'			=>'g3w'
		),
		'application/x-font-ghostscript'=>array(
			'title'				=>'Ghostscript Font',
			'content_type'		=>'application/x-font-ghostscript',
			'file_extension'			=>'gsf'
		),
		'application/x-font-bdf'=>array(
			'title'				=>'Glyph Bitmap Distribution Format',
			'content_type'		=>'application/x-font-bdf',
			'file_extension'			=>'bdf'
		),
		'application/x-gtar'=>array(
			'title'				=>'GNU Tar Files',
			'content_type'		=>'application/x-gtar',
			'file_extension'			=>'tar'
		),
		'application/x-texinfo'=>array(
			'title'				=>'GNU Texinfo Document',
			'content_type'		=>'application/x-texinfo',
			'file_extension'			=>'nfo'
		),
		'application/x-gnumeric'=>array(
			'title'				=>'Gnumeric',
			'content_type'		=>'application/x-gnumeric',
			'file_extension'			=>'ric'
		),
		'application/vnd.google-earth.kml+xml'=>array(
			'title'				=>'Google Earth - KML',
			'content_type'		=>'application/vnd.google-earth.kml+xml',
			'file_extension'			=>'kml'
		),
		'application/vnd.google-earth.kmz'=>array(
			'title'				=>'Google Earth - Zipped KML',
			'content_type'		=>'application/vnd.google-earth.kmz',
			'file_extension'			=>'kmz'
		),
		'application/vnd.grafeq'=>array(
			'title'				=>'GrafEq',
			'content_type'		=>'application/vnd.grafeq',
			'file_extension'			=>'gqf'
		),
		'image/gif'=>array(
			'title'				=>'Graphics Interchange Format',
			'content_type'		=>'image/gif',
			'file_extension'			=>'gif'
		),
		'text/vnd.graphviz'=>array(
			'title'				=>'Graphviz',
			'content_type'		=>'text/vnd.graphviz',
			'file_extension'			=>'gv'
		),
		'application/vnd.groove-account'=>array(
			'title'				=>'Groove - Account',
			'content_type'		=>'application/vnd.groove-account',
			'file_extension'			=>'gac'
		),
		'application/vnd.groove-help'=>array(
			'title'				=>'Groove - Help',
			'content_type'		=>'application/vnd.groove-help',
			'file_extension'			=>'ghf'
		),
		'application/vnd.groove-identity-message'=>array(
			'title'				=>'Groove - Identity Message',
			'content_type'		=>'application/vnd.groove-identity-message',
			'file_extension'			=>'gim'
		),
		'application/vnd.groove-injector'=>array(
			'title'				=>'Groove - Injector',
			'content_type'		=>'application/vnd.groove-injector',
			'file_extension'			=>'grv'
		),
		'application/vnd.groove-tool-message'=>array(
			'title'				=>'Groove - Tool Message',
			'content_type'		=>'application/vnd.groove-tool-message',
			'file_extension'			=>'gtm'
		),
		'application/vnd.groove-tool-template'=>array(
			'title'				=>'Groove - Tool Template',
			'content_type'		=>'application/vnd.groove-tool-template',
			'file_extension'			=>'tpl'
		),
		'application/vnd.groove-vcard'=>array(
			'title'				=>'Groove - Vcard',
			'content_type'		=>'application/vnd.groove-vcard',
			'file_extension'			=>'vcg'
		),
		'video/h261'=>array(
			'title'				=>'H.261',
			'content_type'		=>'video/h261',
			'file_extension'			=>'261'
		),
		'video/h263'=>array(
			'title'				=>'H.263',
			'content_type'		=>'video/h263',
			'file_extension'			=>'263'
		),
		'video/h264'=>array(
			'title'				=>'H.264',
			'content_type'		=>'video/h264',
			'file_extension'			=>'264'
		),
		'application/vnd.hp-hpid'=>array(
			'title'				=>'Hewlett Packard Instant Delivery',
			'content_type'		=>'application/vnd.hp-hpid',
			'file_extension'			=>'pid'
		),
		'application/vnd.hp-hps'=>array(
			'title'				=>'Hewlett-Packard\'s WebPrintSmart',
			'content_type'		=>'application/vnd.hp-hps',
			'file_extension'			=>'hps'
		),
		'application/x-hdf'=>array(
			'title'				=>'Hierarchical Data Format',
			'content_type'		=>'application/x-hdf',
			'file_extension'			=>'hdf'
		),
		'audio/vnd.rip'=>array(
			'title'				=>'Hit\'n\'Mix',
			'content_type'		=>'audio/vnd.rip',
			'file_extension'			=>'rip'
		),
		'application/vnd.hbci'=>array(
			'title'				=>'Homebanking Computer Interface (HBCI)',
			'content_type'		=>'application/vnd.hbci',
			'file_extension'			=>'bci'
		),
		'application/vnd.hp-jlyt'=>array(
			'title'				=>'HP Indigo Digital Press - Job Layout Languate',
			'content_type'		=>'application/vnd.hp-jlyt',
			'file_extension'			=>'jlt'
		),
		'application/vnd.hp-pcl'=>array(
			'title'				=>'HP Printer Command Language',
			'content_type'		=>'application/vnd.hp-pcl',
			'file_extension'			=>'pcl'
		),
		'application/vnd.hp-hpgl'=>array(
			'title'				=>'HP-GL/2 and HP RTL',
			'content_type'		=>'application/vnd.hp-hpgl',
			'file_extension'			=>'pgl'
		),
		'application/vnd.yamaha.hv-script'=>array(
			'title'				=>'HV Script',
			'content_type'		=>'application/vnd.yamaha.hv-script',
			'file_extension'			=>'hvs'
		),
		'application/vnd.yamaha.hv-dic'=>array(
			'title'				=>'HV Voice Dictionary',
			'content_type'		=>'application/vnd.yamaha.hv-dic',
			'file_extension'			=>'hvd'
		),
		'application/vnd.yamaha.hv-voice'=>array(
			'title'				=>'HV Voice Parameter',
			'content_type'		=>'application/vnd.yamaha.hv-voice',
			'file_extension'			=>'hvp'
		),
		'application/vnd.hydrostatix.sof-data'=>array(
			'title'				=>'Hydrostatix Master Suite',
			'content_type'		=>'application/vnd.hydrostatix.sof-data',
			'file_extension'			=>'stx'
		),
		'application/hyperstudio'=>array(
			'title'				=>'Hyperstudio',
			'content_type'		=>'application/hyperstudio',
			'file_extension'			=>'stk'
		),
		'application/vnd.hal+xml'=>array(
			'title'				=>'Hypertext Application Language',
			'content_type'		=>'application/vnd.hal+xml',
			'file_extension'			=>'hal'
		),
		'text/html'=>array(
			'title'				=>'HyperText Markup Language (HTML)',
			'content_type'		=>'text/html',
			'file_extension'			=>'tml'
		),
		'application/vnd.ibm.rights-management'=>array(
			'title'				=>'IBM DB2 Rights Manager',
			'content_type'		=>'application/vnd.ibm.rights-management',
			'file_extension'			=>'irm'
		),
		'application/vnd.ibm.secure-container'=>array(
			'title'				=>'IBM Electronic Media Management System - Secure Container',
			'content_type'		=>'application/vnd.ibm.secure-container',
			'file_extension'			=>'sc'
		),
		'text/calendar'=>array(
			'title'				=>'iCalendar',
			'content_type'		=>'text/calendar',
			'file_extension'			=>'ics'
		),
		'application/vnd.iccprofile'=>array(
			'title'				=>'ICC profile',
			'content_type'		=>'application/vnd.iccprofile',
			'file_extension'			=>'icc'
		),
		'image/x-icon'=>array(
			'title'				=>'Icon Image',
			'content_type'		=>'image/x-icon',
			'file_extension'			=>'ico'
		),
		'application/vnd.igloader'=>array(
			'title'				=>'igLoader',
			'content_type'		=>'application/vnd.igloader',
			'file_extension'			=>'igl'
		),
		'image/ief'=>array(
			'title'				=>'Image Exchange Format',
			'content_type'		=>'image/ief',
			'file_extension'			=>'ief'
		),
		'application/vnd.immervision-ivp'=>array(
			'title'				=>'ImmerVision PURE Players',
			'content_type'		=>'application/vnd.immervision-ivp',
			'file_extension'			=>'ivp'
		),
		'application/vnd.immervision-ivu'=>array(
			'title'				=>'ImmerVision PURE Players',
			'content_type'		=>'application/vnd.immervision-ivu',
			'file_extension'			=>'ivu'
		),
		'application/reginfo+xml'=>array(
			'title'				=>'IMS Networks',
			'content_type'		=>'application/reginfo+xml',
			'file_extension'			=>'rif'
		),
		'text/vnd.in3d.3dml'=>array(
			'title'				=>'In3D - 3DML',
			'content_type'		=>'text/vnd.in3d.3dml',
			'file_extension'			=>'dml'
		),
		'text/vnd.in3d.spot'=>array(
			'title'				=>'In3D - 3DML',
			'content_type'		=>'text/vnd.in3d.spot',
			'file_extension'			=>'pot'
		),
		'model/iges'=>array(
			'title'				=>'Initial Graphics Exchange Specification (IGES)',
			'content_type'		=>'model/iges',
			'file_extension'			=>'igs'
		),
		'application/vnd.intergeo'=>array(
			'title'				=>'Interactive Geometry Software',
			'content_type'		=>'application/vnd.intergeo',
			'file_extension'			=>'i2g'
		),
		'application/vnd.cinderella'=>array(
			'title'				=>'Interactive Geometry Software Cinderella',
			'content_type'		=>'application/vnd.cinderella',
			'file_extension'			=>'cdy'
		),
		'application/vnd.intercon.formnet'=>array(
			'title'				=>'Intercon FormNet',
			'content_type'		=>'application/vnd.intercon.formnet',
			'file_extension'			=>'xpw'
		),
		'application/vnd.isac.fcs'=>array(
			'title'				=>'International Society for Advancement of Cytometry',
			'content_type'		=>'application/vnd.isac.fcs',
			'file_extension'			=>'fcs'
		),
		'application/ipfix'=>array(
			'title'				=>'Internet Protocol Flow Information Export',
			'content_type'		=>'application/ipfix',
			'file_extension'			=>'fix'
		),
		'application/pkix-cert'=>array(
			'title'				=>'Internet Public Key Infrastructure - Certificate',
			'content_type'		=>'application/pkix-cert',
			'file_extension'			=>'cer'
		),
		'application/pkixcmp'=>array(
			'title'				=>'Internet Public Key Infrastructure - Certificate Management Protocole',
			'content_type'		=>'application/pkixcmp',
			'file_extension'			=>'pki'
		),
		'application/pkix-crl'=>array(
			'title'				=>'Internet Public Key Infrastructure - Certificate Revocation Lists',
			'content_type'		=>'application/pkix-crl',
			'file_extension'			=>'crl'
		),
		'application/pkix-pkipath'=>array(
			'title'				=>'Internet Public Key Infrastructure - Certification Path',
			'content_type'		=>'application/pkix-pkipath',
			'file_extension'			=>'ath'
		),
		'application/vnd.insors.igm'=>array(
			'title'				=>'IOCOM Visimeet',
			'content_type'		=>'application/vnd.insors.igm',
			'file_extension'			=>'igm'
		),
		'application/vnd.ipunplugged.rcprofile'=>array(
			'title'				=>'IP Unplugged Roaming Client',
			'content_type'		=>'application/vnd.ipunplugged.rcprofile',
			'file_extension'			=>'ile'
		),
		'application/vnd.irepository.package+xml'=>array(
			'title'				=>'iRepository / Lucidoc Editor',
			'content_type'		=>'application/vnd.irepository.package+xml',
			'file_extension'			=>'irp'
		),
		'text/vnd.sun.j2me.app-descriptor'=>array(
			'title'				=>'J2ME App Descriptor',
			'content_type'		=>'text/vnd.sun.j2me.app-descriptor',
			'file_extension'			=>'jad'
		),
		'application/java-archive'=>array(
			'title'				=>'Java Archive',
			'content_type'		=>'application/java-archive',
			'file_extension'			=>'jar'
		),
		'application/java-vm'=>array(
			'title'				=>'Java Bytecode File',
			'content_type'		=>'application/java-vm',
			'file_extension'			=>'ass'
		),
		'application/x-java-jnlp-file'=>array(
			'title'				=>'Java Network Launching Protocol',
			'content_type'		=>'application/x-java-jnlp-file',
			'file_extension'			=>'nlp'
		),
		'application/java-serialized-object'=>array(
			'title'				=>'Java Serialized Object',
			'content_type'		=>'application/java-serialized-object',
			'file_extension'			=>'ser'
		),
		'text/x-java-source,java'=>array(
			'title'				=>'Java Source File',
			'content_type'		=>'text/x-java-source,java',
			'file_extension'			=>'ava'
		),
		'application/javascript'=>array(
			'title'				=>'JavaScript',
			'content_type'		=>'application/javascript',
			'file_extension'			=>'js'
		),
		'application/json'=>array(
			'title'				=>'JavaScript Object Notation (JSON)',
			'content_type'		=>'application/json',
			'file_extension'			=>'son'
		),
		'application/vnd.joost.joda-archive'=>array(
			'title'				=>'Joda Archive',
			'content_type'		=>'application/vnd.joost.joda-archive',
			'file_extension'			=>'oda'
		),
		'video/jpm'=>array(
			'title'				=>'JPEG 2000 Compound Image File Format',
			'content_type'		=>'video/jpm',
			'file_extension'			=>'jpm'
		),
		'image/jpeg'=>array(
			'title'				=>'JPEG Image',
			'content_type'		=>'image/jpeg',
			'file_extension'			=>'jpg'
		),
		'image/x-citrix-jpeg'=>array(
			'title'				=>'JPEG Image (Citrix client)',
			'content_type'		=>'image/x-citrix-jpeg',
			'file_extension'			=>'jpg'
		),
		'image/pjpeg'=>array(
			'title'				=>'JPEG Image (Progressive)',
			'content_type'		=>'image/pjpeg',
			'file_extension'			=>'peg'
		),
		'video/jpeg'=>array(
			'title'				=>'JPGVideo',
			'content_type'		=>'video/jpeg',
			'file_extension'			=>'pgv'
		),
		'application/vnd.kahootz'=>array(
			'title'				=>'Kahootz',
			'content_type'		=>'application/vnd.kahootz',
			'file_extension'			=>'ktz'
		),
		'application/vnd.chipnuts.karaoke-mmd'=>array(
			'title'				=>'Karaoke on Chipnuts Chipsets',
			'content_type'		=>'application/vnd.chipnuts.karaoke-mmd',
			'file_extension'			=>'mmd'
		),
		'application/vnd.kde.karbon'=>array(
			'title'				=>'KDE KOffice Office Suite - Karbon',
			'content_type'		=>'application/vnd.kde.karbon',
			'file_extension'			=>'bon'
		),
		'application/vnd.kde.kchart'=>array(
			'title'				=>'KDE KOffice Office Suite - KChart',
			'content_type'		=>'application/vnd.kde.kchart',
			'file_extension'			=>'hrt'
		),
		'application/vnd.kde.kformula'=>array(
			'title'				=>'KDE KOffice Office Suite - Kformula',
			'content_type'		=>'application/vnd.kde.kformula',
			'file_extension'			=>'kfo'
		),
		'application/vnd.kde.kivio'=>array(
			'title'				=>'KDE KOffice Office Suite - Kivio',
			'content_type'		=>'application/vnd.kde.kivio',
			'file_extension'			=>'flw'
		),
		'application/vnd.kde.kontour'=>array(
			'title'				=>'KDE KOffice Office Suite - Kontour',
			'content_type'		=>'application/vnd.kde.kontour',
			'file_extension'			=>'kon'
		),
		'application/vnd.kde.kpresenter'=>array(
			'title'				=>'KDE KOffice Office Suite - Kpresenter',
			'content_type'		=>'application/vnd.kde.kpresenter',
			'file_extension'			=>'kpr'
		),
		'application/vnd.kde.kspread'=>array(
			'title'				=>'KDE KOffice Office Suite - Kspread',
			'content_type'		=>'application/vnd.kde.kspread',
			'file_extension'			=>'ksp'
		),
		'application/vnd.kde.kword'=>array(
			'title'				=>'KDE KOffice Office Suite - Kword',
			'content_type'		=>'application/vnd.kde.kword',
			'file_extension'			=>'kwd'
		),
		'application/vnd.kenameaapp'=>array(
			'title'				=>'Kenamea App',
			'content_type'		=>'application/vnd.kenameaapp',
			'file_extension'			=>'tke'
		),
		'application/vnd.kidspiration'=>array(
			'title'				=>'Kidspiration',
			'content_type'		=>'application/vnd.kidspiration',
			'file_extension'			=>'kia'
		),
		'application/vnd.kinar'=>array(
			'title'				=>'Kinar Applications',
			'content_type'		=>'application/vnd.kinar',
			'file_extension'			=>'kne'
		),
		'application/vnd.kodak-descriptor'=>array(
			'title'				=>'Kodak Storyshare',
			'content_type'		=>'application/vnd.kodak-descriptor',
			'file_extension'			=>'sse'
		),
		'application/vnd.las.las+xml'=>array(
			'title'				=>'Laser App Enterprise',
			'content_type'		=>'application/vnd.las.las+xml',
			'file_extension'			=>'xml'
		),
		'application/x-latex'=>array(
			'title'				=>'LaTeX',
			'content_type'		=>'application/x-latex',
			'file_extension'			=>'tex'
		),
		'application/vnd.llamagraphics.life-balance.desktop'=>array(
			'title'				=>'Life Balance - Desktop Edition',
			'content_type'		=>'application/vnd.llamagraphics.life-balance.desktop',
			'file_extension'			=>'lbd'
		),
		'application/vnd.llamagraphics.life-balance.exchange+xml'=>array(
			'title'				=>'Life Balance - Exchange Format',
			'content_type'		=>'application/vnd.llamagraphics.life-balance.exchange+xml',
			'file_extension'			=>'lbe'
		),
		'application/vnd.jam'=>array(
			'title'				=>'Lightspeed Audio Lab',
			'content_type'		=>'application/vnd.jam',
			'file_extension'			=>'jam'
		),
		'application/vnd.lotus-1-2-3'=>array(
			'title'				=>'Lotus 1-2-3',
			'content_type'		=>'application/vnd.lotus-1-2-3',
			'file_extension'			=>'123'
		),
		'application/vnd.lotus-approach'=>array(
			'title'				=>'Lotus Approach',
			'content_type'		=>'application/vnd.lotus-approach',
			'file_extension'			=>'apr'
		),
		'application/vnd.lotus-freelance'=>array(
			'title'				=>'Lotus Freelance',
			'content_type'		=>'application/vnd.lotus-freelance',
			'file_extension'			=>'pre'
		),
		'application/vnd.lotus-notes'=>array(
			'title'				=>'Lotus Notes',
			'content_type'		=>'application/vnd.lotus-notes',
			'file_extension'			=>'nsf'
		),
		'application/vnd.lotus-organizer'=>array(
			'title'				=>'Lotus Organizer',
			'content_type'		=>'application/vnd.lotus-organizer',
			'file_extension'			=>'org'
		),
		'application/vnd.lotus-screencam'=>array(
			'title'				=>'Lotus Screencam',
			'content_type'		=>'application/vnd.lotus-screencam',
			'file_extension'			=>'scm'
		),
		'application/vnd.lotus-wordpro'=>array(
			'title'				=>'Lotus Wordpro',
			'content_type'		=>'application/vnd.lotus-wordpro',
			'file_extension'			=>'lwp'
		),
		'audio/vnd.lucent.voice'=>array(
			'title'				=>'Lucent Voice',
			'content_type'		=>'audio/vnd.lucent.voice',
			'file_extension'			=>'lvp'
		),
		'audio/x-mpegurl'=>array(
			'title'				=>'M3U (Multimedia Playlist)',
			'content_type'		=>'audio/x-mpegurl',
			'file_extension'			=>'m3u'
		),
		'video/x-m4v'=>array(
			'title'				=>'M4v',
			'content_type'		=>'video/x-m4v',
			'file_extension'			=>'m4v'
		),
		'application/mac-binhex40'=>array(
			'title'				=>'Macintosh BinHex 4.0',
			'content_type'		=>'application/mac-binhex40',
			'file_extension'			=>'hqx'
		),
		'application/vnd.macports.portpkg'=>array(
			'title'				=>'MacPorts Port System',
			'content_type'		=>'application/vnd.macports.portpkg',
			'file_extension'			=>'pkg'
		),
		'application/vnd.osgeo.mapguide.package'=>array(
			'title'				=>'MapGuide DBXML',
			'content_type'		=>'application/vnd.osgeo.mapguide.package',
			'file_extension'			=>'mgp'
		),
		'application/marc'=>array(
			'title'				=>'MARC Formats',
			'content_type'		=>'application/marc',
			'file_extension'			=>'mrc'
		),
		'application/marcxml+xml'=>array(
			'title'				=>'MARC21 XML Schema',
			'content_type'		=>'application/marcxml+xml',
			'file_extension'			=>'rcx'
		),
		'application/mxf'=>array(
			'title'				=>'Material Exchange Format',
			'content_type'		=>'application/mxf',
			'file_extension'			=>'mxf'
		),
		'application/vnd.wolfram.player'=>array(
			'title'				=>'Mathematica Notebook Player',
			'content_type'		=>'application/vnd.wolfram.player',
			'file_extension'			=>'nbp'
		),
		'application/mathematica'=>array(
			'title'				=>'Mathematica Notebooks',
			'content_type'		=>'application/mathematica',
			'file_extension'			=>'ma'
		),
		'application/mathml+xml'=>array(
			'title'				=>'Mathematical Markup Language',
			'content_type'		=>'application/mathml+xml',
			'file_extension'			=>'hml'
		),
		'application/mbox'=>array(
			'title'				=>'Mbox database files',
			'content_type'		=>'application/mbox',
			'file_extension'			=>'box'
		),
		'application/vnd.medcalcdata'=>array(
			'title'				=>'MedCalc',
			'content_type'		=>'application/vnd.medcalcdata',
			'file_extension'			=>'mc1'
		),
		'application/mediaservercontrol+xml'=>array(
			'title'				=>'Media Server Control Markup Language',
			'content_type'		=>'application/mediaservercontrol+xml',
			'file_extension'			=>'cml'
		),
		'application/vnd.mediastation.cdkey'=>array(
			'title'				=>'MediaRemote',
			'content_type'		=>'application/vnd.mediastation.cdkey',
			'file_extension'			=>'key'
		),
		'application/vnd.mfer'=>array(
			'title'				=>'Medical Waveform Encoding Format',
			'content_type'		=>'application/vnd.mfer',
			'file_extension'			=>'mwf'
		),
		'application/vnd.mfmp'=>array(
			'title'				=>'Melody Format for Mobile Platform',
			'content_type'		=>'application/vnd.mfmp',
			'file_extension'			=>'mfm'
		),
		'model/mesh'=>array(
			'title'				=>'Mesh Data Type',
			'content_type'		=>'model/mesh',
			'file_extension'			=>'msh'
		),
		'application/mads+xml'=>array(
			'title'				=>'Metadata Authority  Description Schema',
			'content_type'		=>'application/mads+xml',
			'file_extension'			=>'ads'
		),
		'application/mets+xml'=>array(
			'title'				=>'Metadata Encoding and Transmission Standard',
			'content_type'		=>'application/mets+xml',
			'file_extension'			=>'ets'
		),
		'application/mods+xml'=>array(
			'title'				=>'Metadata Object Description Schema',
			'content_type'		=>'application/mods+xml',
			'file_extension'			=>'ods'
		),
		'application/metalink4+xml'=>array(
			'title'				=>'Metalink',
			'content_type'		=>'application/metalink4+xml',
			'file_extension'			=>'ta4'
		),
		'application/vnd.mcd'=>array(
			'title'				=>'Micro CADAM Helix D&D',
			'content_type'		=>'application/vnd.mcd',
			'file_extension'			=>'mcd'
		),
		'application/vnd.micrografx.flo'=>array(
			'title'				=>'Micrografx',
			'content_type'		=>'application/vnd.micrografx.flo',
			'file_extension'			=>'flo'
		),
		'application/vnd.micrografx.igx'=>array(
			'title'				=>'Micrografx iGrafx Professional',
			'content_type'		=>'application/vnd.micrografx.igx',
			'file_extension'			=>'igx'
		),
		'application/vnd.eszigno3+xml'=>array(
			'title'				=>'MICROSEC e-Szign¢',
			'content_type'		=>'application/vnd.eszigno3+xml',
			'file_extension'			=>'es3'
		),
		'application/x-msaccess'=>array(
			'title'				=>'Microsoft Access',
			'content_type'		=>'application/x-msaccess',
			'file_extension'			=>'mdb'
		),
		'video/x-ms-asf'=>array(
			'title'				=>'Microsoft Advanced Systems Format (ASF)',
			'content_type'		=>'video/x-ms-asf',
			'file_extension'			=>'asf'
		),
		'application/x-msdownload'=>array(
			'title'				=>'Microsoft Application',
			'content_type'		=>'application/x-msdownload',
			'file_extension'			=>'exe'
		),
		'application/vnd.ms-artgalry'=>array(
			'title'				=>'Microsoft Artgalry',
			'content_type'		=>'application/vnd.ms-artgalry',
			'file_extension'			=>'cil'
		),
		'application/vnd.ms-cab-compressed'=>array(
			'title'				=>'Microsoft Cabinet File',
			'content_type'		=>'application/vnd.ms-cab-compressed',
			'file_extension'			=>'cab'
		),
		'application/vnd.ms-ims'=>array(
			'title'				=>'Microsoft Class Server',
			'content_type'		=>'application/vnd.ms-ims',
			'file_extension'			=>'ims'
		),
		'application/x-ms-application'=>array(
			'title'				=>'Microsoft ClickOnce',
			'content_type'		=>'application/x-ms-application',
			'file_extension'			=>'ion'
		),
		'application/x-msclip'=>array(
			'title'				=>'Microsoft Clipboard Clip',
			'content_type'		=>'application/x-msclip',
			'file_extension'			=>'clp'
		),
		'image/vnd.ms-modi'=>array(
			'title'				=>'Microsoft Document Imaging Format',
			'content_type'		=>'image/vnd.ms-modi',
			'file_extension'			=>'mdi'
		),
		'application/vnd.ms-fontobject'=>array(
			'title'				=>'Microsoft Embedded OpenType',
			'content_type'		=>'application/vnd.ms-fontobject',
			'file_extension'			=>'eot'
		),
		'application/vnd.ms-excel'=>array(
			'title'				=>'Microsoft Excel',
			'content_type'		=>'application/vnd.ms-excel',
			'file_extension'			=>'xls'
		),
		'application/vnd.ms-excel.addin.macroenabled.12'=>array(
			'title'				=>'Microsoft Excel - Add-In File',
			'content_type'		=>'application/vnd.ms-excel.addin.macroenabled.12',
			'file_extension'			=>'lam'
		),
		'application/vnd.ms-excel.sheet.binary.macroenabled.12'=>array(
			'title'				=>'Microsoft Excel - Binary Workbook',
			'content_type'		=>'application/vnd.ms-excel.sheet.binary.macroenabled.12',
			'file_extension'			=>'lsb'
		),
		'application/vnd.ms-excel.template.macroenabled.12'=>array(
			'title'				=>'Microsoft Excel - Macro-Enabled Template File',
			'content_type'		=>'application/vnd.ms-excel.template.macroenabled.12',
			'file_extension'			=>'ltm'
		),
		'application/vnd.ms-excel.sheet.macroenabled.12'=>array(
			'title'				=>'Microsoft Excel - Macro-Enabled Workbook',
			'content_type'		=>'application/vnd.ms-excel.sheet.macroenabled.12',
			'file_extension'			=>'lsm'
		),
		'application/vnd.ms-htmlhelp'=>array(
			'title'				=>'Microsoft Html Help File',
			'content_type'		=>'application/vnd.ms-htmlhelp',
			'file_extension'			=>'chm'
		),
		'application/x-mscardfile'=>array(
			'title'				=>'Microsoft Information Card',
			'content_type'		=>'application/x-mscardfile',
			'file_extension'			=>'crd'
		),
		'application/vnd.ms-lrm'=>array(
			'title'				=>'Microsoft Learning Resource Module',
			'content_type'		=>'application/vnd.ms-lrm',
			'file_extension'			=>'lrm'
		),
		'application/x-msmediaview'=>array(
			'title'				=>'Microsoft MediaView',
			'content_type'		=>'application/x-msmediaview',
			'file_extension'			=>'mvb'
		),
		'application/x-msmoney'=>array(
			'title'				=>'Microsoft Money',
			'content_type'		=>'application/x-msmoney',
			'file_extension'			=>'mny'
		),
		'application/vnd.openxmlformats-officedocument.presentationml.presentation'=>array(
			'title'				=>'Microsoft Office - OOXML - Presentation',
			'content_type'		=>'application/vnd.openxmlformats-officedocument.presentationml.presentation',
			'file_extension'			=>'ptx'
		),
		'application/vnd.openxmlformats-officedocument.presentationml.slide'=>array(
			'title'				=>'Microsoft Office - OOXML - Presentation (Slide)',
			'content_type'		=>'application/vnd.openxmlformats-officedocument.presentationml.slide',
			'file_extension'			=>'ldx'
		),
		'application/vnd.openxmlformats-officedocument.presentationml.slideshow'=>array(
			'title'				=>'Microsoft Office - OOXML - Presentation (Slideshow)',
			'content_type'		=>'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
			'file_extension'			=>'psx'
		),
		'application/vnd.openxmlformats-officedocument.presentationml.template'=>array(
			'title'				=>'Microsoft Office - OOXML - Presentation Template',
			'content_type'		=>'application/vnd.openxmlformats-officedocument.presentationml.template',
			'file_extension'			=>'otx'
		),
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'=>array(
			'title'				=>'Microsoft Office - OOXML - Spreadsheet',
			'content_type'		=>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'file_extension'			=>'lsx'
		),
		'application/vnd.openxmlformats-officedocument.spreadsheetml.template'=>array(
			'title'				=>'Microsoft Office - OOXML - Spreadsheet Template',
			'content_type'		=>'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
			'file_extension'			=>'ltx'
		),
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document'=>array(
			'title'				=>'Microsoft Office - OOXML - Word Document',
			'content_type'		=>'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'file_extension'			=>'ocx'
		),
		'application/vnd.openxmlformats-officedocument.wordprocessingml.template'=>array(
			'title'				=>'Microsoft Office - OOXML - Word Document Template',
			'content_type'		=>'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
			'file_extension'			=>'otx'
		),
		'application/x-msbinder'=>array(
			'title'				=>'Microsoft Office Binder',
			'content_type'		=>'application/x-msbinder',
			'file_extension'			=>'obd'
		),
		'application/vnd.ms-officetheme'=>array(
			'title'				=>'Microsoft Office System Release Theme',
			'content_type'		=>'application/vnd.ms-officetheme',
			'file_extension'			=>'hmx'
		),
		'application/onenote'=>array(
			'title'				=>'Microsoft OneNote',
			'content_type'		=>'application/onenote',
			'file_extension'			=>'toc'
		),
		'audio/vnd.ms-playready.media.pya'=>array(
			'title'				=>'Microsoft PlayReady Ecosystem',
			'content_type'		=>'audio/vnd.ms-playready.media.pya',
			'file_extension'			=>'pya'
		),
		'video/vnd.ms-playready.media.pyv'=>array(
			'title'				=>'Microsoft PlayReady Ecosystem Video',
			'content_type'		=>'video/vnd.ms-playready.media.pyv',
			'file_extension'			=>'pyv'
		),
		'application/vnd.ms-powerpoint'=>array(
			'title'				=>'Microsoft PowerPoint',
			'content_type'		=>'application/vnd.ms-powerpoint',
			'file_extension'			=>'ppt'
		),
		'application/vnd.ms-powerpoint.addin.macroenabled.12'=>array(
			'title'				=>'Microsoft PowerPoint - Add-in file',
			'content_type'		=>'application/vnd.ms-powerpoint.addin.macroenabled.12',
			'file_extension'			=>'pam'
		),
		'application/vnd.ms-powerpoint.slide.macroenabled.12'=>array(
			'title'				=>'Microsoft PowerPoint - Macro-Enabled Open XML Slide',
			'content_type'		=>'application/vnd.ms-powerpoint.slide.macroenabled.12',
			'file_extension'			=>'ldm'
		),
		'application/vnd.ms-powerpoint.presentation.macroenabled.12'=>array(
			'title'				=>'Microsoft PowerPoint - Macro-Enabled Presentation File',
			'content_type'		=>'application/vnd.ms-powerpoint.presentation.macroenabled.12',
			'file_extension'			=>'ptm'
		),
		'application/vnd.ms-powerpoint.slideshow.macroenabled.12'=>array(
			'title'				=>'Microsoft PowerPoint - Macro-Enabled Slide Show File',
			'content_type'		=>'application/vnd.ms-powerpoint.slideshow.macroenabled.12',
			'file_extension'			=>'psm'
		),
		'application/vnd.ms-powerpoint.template.macroenabled.12'=>array(
			'title'				=>'Microsoft PowerPoint - Macro-Enabled Template File',
			'content_type'		=>'application/vnd.ms-powerpoint.template.macroenabled.12',
			'file_extension'			=>'otm'
		),
		'application/vnd.ms-project'=>array(
			'title'				=>'Microsoft Project',
			'content_type'		=>'application/vnd.ms-project',
			'file_extension'			=>'mpp'
		),
		'application/x-mspublisher'=>array(
			'title'				=>'Microsoft Publisher',
			'content_type'		=>'application/x-mspublisher',
			'file_extension'			=>'pub'
		),
		'application/x-msschedule'=>array(
			'title'				=>'Microsoft Schedule+',
			'content_type'		=>'application/x-msschedule',
			'file_extension'			=>'scd'
		),
		'application/x-silverlight-app'=>array(
			'title'				=>'Microsoft Silverlight',
			'content_type'		=>'application/x-silverlight-app',
			'file_extension'			=>'xap'
		),
		'application/vnd.ms-pki.stl'=>array(
			'title'				=>'Microsoft Trust UI Provider - Certificate Trust Link',
			'content_type'		=>'application/vnd.ms-pki.stl',
			'file_extension'			=>'stl'
		),
		'application/vnd.ms-pki.seccat'=>array(
			'title'				=>'Microsoft Trust UI Provider - Security Catalog',
			'content_type'		=>'application/vnd.ms-pki.seccat',
			'file_extension'			=>'cat'
		),
		'application/vnd.visio'=>array(
			'title'				=>'Microsoft Visio',
			'content_type'		=>'application/vnd.visio',
			'file_extension'			=>'vsd'
		),
		'application/vnd.visio2013'=>array(
			'title'				=>'Microsoft Visio 2013',
			'content_type'		=>'application/vnd.visio2013',
			'file_extension'			=>'sdx'
		),
		'video/x-ms-wm'=>array(
			'title'				=>'Microsoft Windows Media',
			'content_type'		=>'video/x-ms-wm',
			'file_extension'			=>'wm'
		),
		'audio/x-ms-wma'=>array(
			'title'				=>'Microsoft Windows Media Audio',
			'content_type'		=>'audio/x-ms-wma',
			'file_extension'			=>'wma'
		),
		'audio/x-ms-wax'=>array(
			'title'				=>'Microsoft Windows Media Audio Redirector',
			'content_type'		=>'audio/x-ms-wax',
			'file_extension'			=>'wax'
		),
		'video/x-ms-wmx'=>array(
			'title'				=>'Microsoft Windows Media Audio/Video Playlist',
			'content_type'		=>'video/x-ms-wmx',
			'file_extension'			=>'wmx'
		),
		'application/x-ms-wmd'=>array(
			'title'				=>'Microsoft Windows Media Player Download Package',
			'content_type'		=>'application/x-ms-wmd',
			'file_extension'			=>'wmd'
		),
		'application/vnd.ms-wpl'=>array(
			'title'				=>'Microsoft Windows Media Player Playlist',
			'content_type'		=>'application/vnd.ms-wpl',
			'file_extension'			=>'wpl'
		),
		'application/x-ms-wmz'=>array(
			'title'				=>'Microsoft Windows Media Player Skin Package',
			'content_type'		=>'application/x-ms-wmz',
			'file_extension'			=>'wmz'
		),
		'video/x-ms-wmv'=>array(
			'title'				=>'Microsoft Windows Media Video',
			'content_type'		=>'video/x-ms-wmv',
			'file_extension'			=>'wmv'
		),
		'video/x-ms-wvx'=>array(
			'title'				=>'Microsoft Windows Media Video Playlist',
			'content_type'		=>'video/x-ms-wvx',
			'file_extension'			=>'wvx'
		),
		'application/x-msmetafile'=>array(
			'title'				=>'Microsoft Windows Metafile',
			'content_type'		=>'application/x-msmetafile',
			'file_extension'			=>'wmf'
		),
		'application/x-msterminal'=>array(
			'title'				=>'Microsoft Windows Terminal Services',
			'content_type'		=>'application/x-msterminal',
			'file_extension'			=>'trm'
		),
		'application/msword'=>array(
			'title'				=>'Microsoft Word',
			'content_type'		=>'application/msword',
			'file_extension'			=>'doc'
		),
		'application/vnd.ms-word.document.macroenabled.12'=>array(
			'title'				=>'Microsoft Word - Macro-Enabled Document',
			'content_type'		=>'application/vnd.ms-word.document.macroenabled.12',
			'file_extension'			=>'ocm'
		),
		'application/vnd.ms-word.template.macroenabled.12'=>array(
			'title'				=>'Microsoft Word - Macro-Enabled Template',
			'content_type'		=>'application/vnd.ms-word.template.macroenabled.12',
			'file_extension'			=>'otm'
		),
		'application/x-mswrite'=>array(
			'title'				=>'Microsoft Wordpad',
			'content_type'		=>'application/x-mswrite',
			'file_extension'			=>'wri'
		),
		'application/vnd.ms-works'=>array(
			'title'				=>'Microsoft Works',
			'content_type'		=>'application/vnd.ms-works',
			'file_extension'			=>'wps'
		),
		'application/x-ms-xbap'=>array(
			'title'				=>'Microsoft XAML Browser Application',
			'content_type'		=>'application/x-ms-xbap',
			'file_extension'			=>'bap'
		),
		'application/vnd.ms-xpsdocument'=>array(
			'title'				=>'Microsoft XML Paper Specification',
			'content_type'		=>'application/vnd.ms-xpsdocument',
			'file_extension'			=>'xps'
		),
		'audio/midi'=>array(
			'title'				=>'MIDI - Musical Instrument Digital Interface',
			'content_type'		=>'audio/midi',
			'file_extension'			=>'mid'
		),
		'application/vnd.ibm.minipay'=>array(
			'title'				=>'MiniPay',
			'content_type'		=>'application/vnd.ibm.minipay',
			'file_extension'			=>'mpy'
		),
		'application/vnd.ibm.modcap'=>array(
			'title'				=>'MO:DCA-P',
			'content_type'		=>'application/vnd.ibm.modcap',
			'file_extension'			=>'afp'
		),
		'application/vnd.jcp.javame.midlet-rms'=>array(
			'title'				=>'Mobile Information Device Profile',
			'content_type'		=>'application/vnd.jcp.javame.midlet-rms',
			'file_extension'			=>'rms'
		),
		'application/vnd.tmobile-livetv'=>array(
			'title'				=>'MobileTV',
			'content_type'		=>'application/vnd.tmobile-livetv',
			'file_extension'			=>'tmo'
		),
		'application/x-mobipocket-ebook'=>array(
			'title'				=>'Mobipocket',
			'content_type'		=>'application/x-mobipocket-ebook',
			'file_extension'			=>'prc'
		),
		'application/vnd.mobius.mbk'=>array(
			'title'				=>'Mobius Management Systems - Basket file',
			'content_type'		=>'application/vnd.mobius.mbk',
			'file_extension'			=>'mbk'
		),
		'application/vnd.mobius.dis'=>array(
			'title'				=>'Mobius Management Systems - Distribution Database',
			'content_type'		=>'application/vnd.mobius.dis',
			'file_extension'			=>'dis'
		),
		'application/vnd.mobius.plc'=>array(
			'title'				=>'Mobius Management Systems - Policy Definition Language File',
			'content_type'		=>'application/vnd.mobius.plc',
			'file_extension'			=>'plc'
		),
		'application/vnd.mobius.mqy'=>array(
			'title'				=>'Mobius Management Systems - Query File',
			'content_type'		=>'application/vnd.mobius.mqy',
			'file_extension'			=>'mqy'
		),
		'application/vnd.mobius.msl'=>array(
			'title'				=>'Mobius Management Systems - Script Language',
			'content_type'		=>'application/vnd.mobius.msl',
			'file_extension'			=>'msl'
		),
		'application/vnd.mobius.txf'=>array(
			'title'				=>'Mobius Management Systems - Topic Index File',
			'content_type'		=>'application/vnd.mobius.txf',
			'file_extension'			=>'txf'
		),
		'application/vnd.mobius.daf'=>array(
			'title'				=>'Mobius Management Systems - UniversalArchive',
			'content_type'		=>'application/vnd.mobius.daf',
			'file_extension'			=>'daf'
		),
		'text/vnd.fly'=>array(
			'title'				=>'mod_fly / fly.cgi',
			'content_type'		=>'text/vnd.fly',
			'file_extension'			=>'fly'
		),
		'application/vnd.mophun.certificate'=>array(
			'title'				=>'Mophun Certificate',
			'content_type'		=>'application/vnd.mophun.certificate',
			'file_extension'			=>'mpc'
		),
		'application/vnd.mophun.application'=>array(
			'title'				=>'Mophun VM',
			'content_type'		=>'application/vnd.mophun.application',
			'file_extension'			=>'mpn'
		),
		'video/mj2'=>array(
			'title'				=>'Motion JPEG 2000',
			'content_type'		=>'video/mj2',
			'file_extension'			=>'mj2'
		),
		'audio/mpeg'=>array(
			'title'				=>'MPEG Audio',
			'content_type'		=>'audio/mpeg',
			'file_extension'			=>'pga'
		),
		'video/vnd.mpegurl'=>array(
			'title'				=>'MPEG Url',
			'content_type'		=>'video/vnd.mpegurl',
			'file_extension'			=>'mxu'
		),
		'video/mpeg'=>array(
			'title'				=>'MPEG Video',
			'content_type'		=>'video/mpeg',
			'file_extension'			=>'peg'
		),
		'application/mp21'=>array(
			'title'				=>'MPEG-21',
			'content_type'		=>'application/mp21',
			'file_extension'			=>'m21'
		),
		'audio/mp4'=>array(
			'title'				=>'MPEG-4 Audio',
			'content_type'		=>'audio/mp4',
			'file_extension'			=>'p4a'
		),
		'video/mp4'=>array(
			'title'				=>'MPEG-4 Video',
			'content_type'		=>'video/mp4',
			'file_extension'			=>'mp4'
		),
		'application/mp4'=>array(
			'title'				=>'MPEG4',
			'content_type'		=>'application/mp4',
			'file_extension'			=>'mp4'
		),
		'application/vnd.apple.mpegurl'=>array(
			'title'				=>'Multimedia Playlist Unicode',
			'content_type'		=>'application/vnd.apple.mpegurl',
			'file_extension'			=>'3u8'
		),
		'application/vnd.musician'=>array(
			'title'				=>'MUsical Score Interpreted Code Invented  for the ASCII designation of Notation',
			'content_type'		=>'application/vnd.musician',
			'file_extension'			=>'mus'
		),
		'application/vnd.muvee.style'=>array(
			'title'				=>'Muvee Automatic Video Editing',
			'content_type'		=>'application/vnd.muvee.style',
			'file_extension'			=>'sty'
		),
		'application/xv+xml'=>array(
			'title'				=>'MXML',
			'content_type'		=>'application/xv+xml',
			'file_extension'			=>'xml'
		),
		'application/vnd.nokia.n-gage.data'=>array(
			'title'				=>'N-Gage Game Data',
			'content_type'		=>'application/vnd.nokia.n-gage.data',
			'file_extension'			=>'dat'
		),
		'application/vnd.nokia.n-gage.symbian.install'=>array(
			'title'				=>'N-Gage Game Installer',
			'content_type'		=>'application/vnd.nokia.n-gage.symbian.install',
			'file_extension'			=>'age'
		),
		'application/x-dtbncx+xml'=>array(
			'title'				=>'Navigation Control file for XML (for ePub)',
			'content_type'		=>'application/x-dtbncx+xml',
			'file_extension'			=>'ncx'
		),
		'application/x-netcdf'=>array(
			'title'				=>'Network Common Data Form (NetCDF)',
			'content_type'		=>'application/x-netcdf',
			'file_extension'			=>'nc'
		),
		'application/vnd.neurolanguage.nlu'=>array(
			'title'				=>'neuroLanguage',
			'content_type'		=>'application/vnd.neurolanguage.nlu',
			'file_extension'			=>'nlu'
		),
		'application/vnd.dna'=>array(
			'title'				=>'New Moon Liftoff/DNA',
			'content_type'		=>'application/vnd.dna',
			'file_extension'			=>'dna'
		),
		'application/vnd.noblenet-directory'=>array(
			'title'				=>'NobleNet Directory',
			'content_type'		=>'application/vnd.noblenet-directory',
			'file_extension'			=>'nnd'
		),
		'application/vnd.noblenet-sealer'=>array(
			'title'				=>'NobleNet Sealer',
			'content_type'		=>'application/vnd.noblenet-sealer',
			'file_extension'			=>'nns'
		),
		'application/vnd.noblenet-web'=>array(
			'title'				=>'NobleNet Web',
			'content_type'		=>'application/vnd.noblenet-web',
			'file_extension'			=>'nnw'
		),
		'application/vnd.nokia.radio-preset'=>array(
			'title'				=>'Nokia Radio Application - Preset',
			'content_type'		=>'application/vnd.nokia.radio-preset',
			'file_extension'			=>'pst'
		),
		'application/vnd.nokia.radio-presets'=>array(
			'title'				=>'Nokia Radio Application - Preset',
			'content_type'		=>'application/vnd.nokia.radio-presets',
			'file_extension'			=>'pss'
		),
		'text/n3'=>array(
			'title'				=>'Notation3',
			'content_type'		=>'text/n3',
			'file_extension'			=>'n3'
		),
		'application/vnd.novadigm.edm'=>array(
			'title'				=>'Novadigm\'s RADIA and EDM products',
			'content_type'		=>'application/vnd.novadigm.edm',
			'file_extension'			=>'edm'
		),
		'application/vnd.novadigm.edx'=>array(
			'title'				=>'Novadigm\'s RADIA and EDM products',
			'content_type'		=>'application/vnd.novadigm.edx',
			'file_extension'			=>'edx'
		),
		'application/vnd.novadigm.ext'=>array(
			'title'				=>'Novadigm\'s RADIA and EDM products',
			'content_type'		=>'application/vnd.novadigm.ext',
			'file_extension'			=>'ext'
		),
		'application/vnd.flographit'=>array(
			'title'				=>'NpGraphIt',
			'content_type'		=>'application/vnd.flographit',
			'file_extension'			=>'gph'
		),
		'audio/vnd.nuera.ecelp4800'=>array(
			'title'				=>'Nuera ECELP 4800',
			'content_type'		=>'audio/vnd.nuera.ecelp4800',
			'file_extension'			=>'800'
		),
		'audio/vnd.nuera.ecelp7470'=>array(
			'title'				=>'Nuera ECELP 7470',
			'content_type'		=>'audio/vnd.nuera.ecelp7470',
			'file_extension'			=>'470'
		),
		'audio/vnd.nuera.ecelp9600'=>array(
			'title'				=>'Nuera ECELP 9600',
			'content_type'		=>'audio/vnd.nuera.ecelp9600',
			'file_extension'			=>'600'
		),
		'application/oda'=>array(
			'title'				=>'Office Document Architecture',
			'content_type'		=>'application/oda',
			'file_extension'			=>'oda'
		),
		'application/ogg'=>array(
			'title'				=>'Ogg',
			'content_type'		=>'application/ogg',
			'file_extension'			=>'ogx'
		),
		'audio/ogg'=>array(
			'title'				=>'Ogg Audio',
			'content_type'		=>'audio/ogg',
			'file_extension'			=>'oga'
		),
		'video/ogg'=>array(
			'title'				=>'Ogg Video',
			'content_type'		=>'video/ogg',
			'file_extension'			=>'ogv'
		),
		'application/vnd.oma.dd2+xml'=>array(
			'title'				=>'OMA Download Agents',
			'content_type'		=>'application/vnd.oma.dd2+xml',
			'file_extension'			=>'dd2'
		),
		'application/vnd.oasis.opendocument.text-web'=>array(
			'title'				=>'Open Document Text Web',
			'content_type'		=>'application/vnd.oasis.opendocument.text-web',
			'file_extension'			=>'oth'
		),
		'application/oebps-package+xml'=>array(
			'title'				=>'Open eBook Publication Structure',
			'content_type'		=>'application/oebps-package+xml',
			'file_extension'			=>'opf'
		),
		'application/vnd.intu.qbo'=>array(
			'title'				=>'Open Financial Exchange',
			'content_type'		=>'application/vnd.intu.qbo',
			'file_extension'			=>'qbo'
		),
		'application/vnd.openofficeorg.extension'=>array(
			'title'				=>'Open Office Extension',
			'content_type'		=>'application/vnd.openofficeorg.extension',
			'file_extension'			=>'oxt'
		),
		'application/vnd.yamaha.openscoreformat'=>array(
			'title'				=>'Open Score Format',
			'content_type'		=>'application/vnd.yamaha.openscoreformat',
			'file_extension'			=>'osf'
		),
		'audio/webm'=>array(
			'title'				=>'Open Web Media Project - Audio',
			'content_type'		=>'audio/webm',
			'file_extension'			=>'eba'
		),
		'video/webm'=>array(
			'title'				=>'Open Web Media Project - Video',
			'content_type'		=>'video/webm',
			'file_extension'			=>'ebm'
		),
		'application/vnd.oasis.opendocument.chart'=>array(
			'title'				=>'OpenDocument Chart',
			'content_type'		=>'application/vnd.oasis.opendocument.chart',
			'file_extension'			=>'odc'
		),
		'application/vnd.oasis.opendocument.chart-template'=>array(
			'title'				=>'OpenDocument Chart Template',
			'content_type'		=>'application/vnd.oasis.opendocument.chart-template',
			'file_extension'			=>'otc'
		),
		'application/vnd.oasis.opendocument.database'=>array(
			'title'				=>'OpenDocument Database',
			'content_type'		=>'application/vnd.oasis.opendocument.database',
			'file_extension'			=>'odb'
		),
		'application/vnd.oasis.opendocument.formula'=>array(
			'title'				=>'OpenDocument Formula',
			'content_type'		=>'application/vnd.oasis.opendocument.formula',
			'file_extension'			=>'odf'
		),
		'application/vnd.oasis.opendocument.formula-template'=>array(
			'title'				=>'OpenDocument Formula Template',
			'content_type'		=>'application/vnd.oasis.opendocument.formula-template',
			'file_extension'			=>'dft'
		),
		'application/vnd.oasis.opendocument.graphics'=>array(
			'title'				=>'OpenDocument Graphics',
			'content_type'		=>'application/vnd.oasis.opendocument.graphics',
			'file_extension'			=>'odg'
		),
		'application/vnd.oasis.opendocument.graphics-template'=>array(
			'title'				=>'OpenDocument Graphics Template',
			'content_type'		=>'application/vnd.oasis.opendocument.graphics-template',
			'file_extension'			=>'otg'
		),
		'application/vnd.oasis.opendocument.image'=>array(
			'title'				=>'OpenDocument Image',
			'content_type'		=>'application/vnd.oasis.opendocument.image',
			'file_extension'			=>'odi'
		),
		'application/vnd.oasis.opendocument.image-template'=>array(
			'title'				=>'OpenDocument Image Template',
			'content_type'		=>'application/vnd.oasis.opendocument.image-template',
			'file_extension'			=>'oti'
		),
		'application/vnd.oasis.opendocument.presentation'=>array(
			'title'				=>'OpenDocument Presentation',
			'content_type'		=>'application/vnd.oasis.opendocument.presentation',
			'file_extension'			=>'odp'
		),
		'application/vnd.oasis.opendocument.presentation-template'=>array(
			'title'				=>'OpenDocument Presentation Template',
			'content_type'		=>'application/vnd.oasis.opendocument.presentation-template',
			'file_extension'			=>'otp'
		),
		'application/vnd.oasis.opendocument.spreadsheet'=>array(
			'title'				=>'OpenDocument Spreadsheet',
			'content_type'		=>'application/vnd.oasis.opendocument.spreadsheet',
			'file_extension'			=>'ods'
		),
		'application/vnd.oasis.opendocument.spreadsheet-template'=>array(
			'title'				=>'OpenDocument Spreadsheet Template',
			'content_type'		=>'application/vnd.oasis.opendocument.spreadsheet-template',
			'file_extension'			=>'ots'
		),
		'application/vnd.oasis.opendocument.text'=>array(
			'title'				=>'OpenDocument Text',
			'content_type'		=>'application/vnd.oasis.opendocument.text',
			'file_extension'			=>'odt'
		),
		'application/vnd.oasis.opendocument.text-master'=>array(
			'title'				=>'OpenDocument Text Master',
			'content_type'		=>'application/vnd.oasis.opendocument.text-master',
			'file_extension'			=>'odm'
		),
		'application/vnd.oasis.opendocument.text-template'=>array(
			'title'				=>'OpenDocument Text Template',
			'content_type'		=>'application/vnd.oasis.opendocument.text-template',
			'file_extension'			=>'ott'
		),
		'image/ktx'=>array(
			'title'				=>'OpenGL Textures (KTX)',
			'content_type'		=>'image/ktx',
			'file_extension'			=>'ktx'
		),
		'application/vnd.sun.xml.calc'=>array(
			'title'				=>'OpenOffice - Calc (Spreadsheet)',
			'content_type'		=>'application/vnd.sun.xml.calc',
			'file_extension'			=>'sxc'
		),
		'application/vnd.sun.xml.calc.template'=>array(
			'title'				=>'OpenOffice - Calc Template (Spreadsheet)',
			'content_type'		=>'application/vnd.sun.xml.calc.template',
			'file_extension'			=>'stc'
		),
		'application/vnd.sun.xml.draw'=>array(
			'title'				=>'OpenOffice - Draw (Graphics)',
			'content_type'		=>'application/vnd.sun.xml.draw',
			'file_extension'			=>'sxd'
		),
		'application/vnd.sun.xml.draw.template'=>array(
			'title'				=>'OpenOffice - Draw Template (Graphics)',
			'content_type'		=>'application/vnd.sun.xml.draw.template',
			'file_extension'			=>'std'
		),
		'application/vnd.sun.xml.impress'=>array(
			'title'				=>'OpenOffice - Impress (Presentation)',
			'content_type'		=>'application/vnd.sun.xml.impress',
			'file_extension'			=>'sxi'
		),
		'application/vnd.sun.xml.impress.template'=>array(
			'title'				=>'OpenOffice - Impress Template (Presentation)',
			'content_type'		=>'application/vnd.sun.xml.impress.template',
			'file_extension'			=>'sti'
		),
		'application/vnd.sun.xml.math'=>array(
			'title'				=>'OpenOffice - Math (Formula)',
			'content_type'		=>'application/vnd.sun.xml.math',
			'file_extension'			=>'sxm'
		),
		'application/vnd.sun.xml.writer'=>array(
			'title'				=>'OpenOffice - Writer (Text - HTML)',
			'content_type'		=>'application/vnd.sun.xml.writer',
			'file_extension'			=>'sxw'
		),
		'application/vnd.sun.xml.writer.global'=>array(
			'title'				=>'OpenOffice - Writer (Text - HTML)',
			'content_type'		=>'application/vnd.sun.xml.writer.global',
			'file_extension'			=>'sxg'
		),
		'application/vnd.sun.xml.writer.template'=>array(
			'title'				=>'OpenOffice - Writer Template (Text - HTML)',
			'content_type'		=>'application/vnd.sun.xml.writer.template',
			'file_extension'			=>'stw'
		),
		'application/x-font-otf'=>array(
			'title'				=>'OpenType Font File',
			'content_type'		=>'application/x-font-otf',
			'file_extension'			=>'otf'
		),
		'application/vnd.yamaha.openscoreformat.osfpvg+xml'=>array(
			'title'				=>'OSFPVG',
			'content_type'		=>'application/vnd.yamaha.openscoreformat.osfpvg+xml',
			'file_extension'			=>'pvg'
		),
		'application/vnd.osgi.dp'=>array(
			'title'				=>'OSGi Deployment Package',
			'content_type'		=>'application/vnd.osgi.dp',
			'file_extension'			=>'dp'
		),
		'application/vnd.palm'=>array(
			'title'				=>'PalmOS Data',
			'content_type'		=>'application/vnd.palm',
			'file_extension'			=>'pdb'
		),
		'text/x-pascal'=>array(
			'title'				=>'Pascal Source File',
			'content_type'		=>'text/x-pascal',
			'file_extension'			=>'p'
		),
		'application/vnd.pawaafile'=>array(
			'title'				=>'PawaaFILE',
			'content_type'		=>'application/vnd.pawaafile',
			'file_extension'			=>'paw'
		),
		'application/vnd.hp-pclxl'=>array(
			'title'				=>'PCL 6 Enhanced (Formely PCL XL)',
			'content_type'		=>'application/vnd.hp-pclxl',
			'file_extension'			=>'lxl'
		),
		'application/vnd.picsel'=>array(
			'title'				=>'Pcsel eFIF File',
			'content_type'		=>'application/vnd.picsel',
			'file_extension'			=>'fif'
		),
		'image/x-pcx'=>array(
			'title'				=>'PCX Image',
			'content_type'		=>'image/x-pcx',
			'file_extension'			=>'pcx'
		),
		'image/vnd.adobe.photoshop'=>array(
			'title'				=>'Photoshop Document',
			'content_type'		=>'image/vnd.adobe.photoshop',
			'file_extension'			=>'psd'
		),
		'application/pics-rules'=>array(
			'title'				=>'PICSRules',
			'content_type'		=>'application/pics-rules',
			'file_extension'			=>'prf'
		),
		'image/x-pict'=>array(
			'title'				=>'PICT Image',
			'content_type'		=>'image/x-pict',
			'file_extension'			=>'pic'
		),
		'application/x-chat'=>array(
			'title'				=>'pIRCh',
			'content_type'		=>'application/x-chat',
			'file_extension'			=>'hat'
		),
		'application/pkcs10'=>array(
			'title'				=>'PKCS #10 - Certification Request Standard',
			'content_type'		=>'application/pkcs10',
			'file_extension'			=>'p10'
		),
		'application/x-pkcs12'=>array(
			'title'				=>'PKCS #12 - Personal Information Exchange Syntax Standard',
			'content_type'		=>'application/x-pkcs12',
			'file_extension'			=>'p12'
		),
		'application/pkcs7-mime'=>array(
			'title'				=>'PKCS #7 - Cryptographic Message Syntax Standard',
			'content_type'		=>'application/pkcs7-mime',
			'file_extension'			=>'p7m'
		),
		'application/pkcs7-signature'=>array(
			'title'				=>'PKCS #7 - Cryptographic Message Syntax Standard',
			'content_type'		=>'application/pkcs7-signature',
			'file_extension'			=>'p7s'
		),
		'application/x-pkcs7-certreqresp'=>array(
			'title'				=>'PKCS #7 - Cryptographic Message Syntax Standard (Certificate Request Response)',
			'content_type'		=>'application/x-pkcs7-certreqresp',
			'file_extension'			=>'p7r'
		),
		'application/x-pkcs7-certificates'=>array(
			'title'				=>'PKCS #7 - Cryptographic Message Syntax Standard (Certificates)',
			'content_type'		=>'application/x-pkcs7-certificates',
			'file_extension'			=>'p7b'
		),
		'application/pkcs8'=>array(
			'title'				=>'PKCS #8 - Private-Key Information Syntax Standard',
			'content_type'		=>'application/pkcs8',
			'file_extension'			=>'p8'
		),
		'application/vnd.pocketlearn'=>array(
			'title'				=>'PocketLearn Viewers',
			'content_type'		=>'application/vnd.pocketlearn',
			'file_extension'			=>'plf'
		),
		'image/x-portable-anymap'=>array(
			'title'				=>'Portable Anymap Image',
			'content_type'		=>'image/x-portable-anymap',
			'file_extension'			=>'pnm'
		),
		'image/x-portable-bitmap'=>array(
			'title'				=>'Portable Bitmap Format',
			'content_type'		=>'image/x-portable-bitmap',
			'file_extension'			=>'pbm'
		),
		'application/x-font-pcf'=>array(
			'title'				=>'Portable Compiled Format',
			'content_type'		=>'application/x-font-pcf',
			'file_extension'			=>'pcf'
		),
		'application/font-tdpfr'=>array(
			'title'				=>'Portable Font Resource',
			'content_type'		=>'application/font-tdpfr',
			'file_extension'			=>'pfr'
		),
		'application/x-chess-pgn'=>array(
			'title'				=>'Portable Game Notation (Chess Games)',
			'content_type'		=>'application/x-chess-pgn',
			'file_extension'			=>'pgn'
		),
		'image/x-portable-graymap'=>array(
			'title'				=>'Portable Graymap Format',
			'content_type'		=>'image/x-portable-graymap',
			'file_extension'			=>'pgm'
		),
		'image/png'=>array(
			'title'				=>'Portable Network Graphics (PNG)',
			'content_type'		=>'image/png',
			'file_extension'			=>'png'
		),
		'image/x-citrix-png'=>array(
			'title'				=>'Portable Network Graphics (PNG) (Citrix client)',
			'content_type'		=>'image/x-citrix-png',
			'file_extension'			=>'png'
		),
		'image/x-png'=>array(
			'title'				=>'Portable Network Graphics (PNG) (x-token)',
			'content_type'		=>'image/x-png',
			'file_extension'			=>'png'
		),
		'image/x-portable-pixmap'=>array(
			'title'				=>'Portable Pixmap Format',
			'content_type'		=>'image/x-portable-pixmap',
			'file_extension'			=>'ppm'
		),
		'application/pskc+xml'=>array(
			'title'				=>'Portable Symmetric Key Container',
			'content_type'		=>'application/pskc+xml',
			'file_extension'			=>'xml'
		),
		'application/vnd.ctc-posml'=>array(
			'title'				=>'PosML',
			'content_type'		=>'application/vnd.ctc-posml',
			'file_extension'			=>'pml'
		),
		'application/postscript'=>array(
			'title'				=>'PostScript',
			'content_type'		=>'application/postscript',
			'file_extension'			=>'ai'
		),
		'application/x-font-type1'=>array(
			'title'				=>'PostScript Fonts',
			'content_type'		=>'application/x-font-type1',
			'file_extension'			=>'pfa'
		),
		'application/vnd.powerbuilder6'=>array(
			'title'				=>'PowerBuilder',
			'content_type'		=>'application/vnd.powerbuilder6',
			'file_extension'			=>'pbd'
		),
		'application/pgp-encrypted'=>array(
			'title'				=>'Pretty Good Privacy',
			'content_type'		=>'application/pgp-encrypted',
			'file_extension'			=>'pgp'
		),
		'application/pgp-signature'=>array(
			'title'				=>'Pretty Good Privacy - Signature',
			'content_type'		=>'application/pgp-signature',
			'file_extension'			=>'pgp'
		),
		'application/vnd.previewsystems.box'=>array(
			'title'				=>'Preview Systems ZipLock/VBox',
			'content_type'		=>'application/vnd.previewsystems.box',
			'file_extension'			=>'box'
		),
		'application/vnd.pvi.ptid1'=>array(
			'title'				=>'Princeton Video Image',
			'content_type'		=>'application/vnd.pvi.ptid1',
			'file_extension'			=>'tid'
		),
		'application/pls+xml'=>array(
			'title'				=>'Pronunciation Lexicon Specification',
			'content_type'		=>'application/pls+xml',
			'file_extension'			=>'pls'
		),
		'application/vnd.pg.format'=>array(
			'title'				=>'Proprietary P&G Standard Reporting System',
			'content_type'		=>'application/vnd.pg.format',
			'file_extension'			=>'str'
		),
		'application/vnd.pg.osasli'=>array(
			'title'				=>'Proprietary P&G Standard Reporting System',
			'content_type'		=>'application/vnd.pg.osasli',
			'file_extension'			=>'ei6'
		),
		'text/prs.lines.tag'=>array(
			'title'				=>'PRS Lines Tag',
			'content_type'		=>'text/prs.lines.tag',
			'file_extension'			=>'dsc'
		),
		'application/x-font-linux-psf'=>array(
			'title'				=>'PSF Fonts',
			'content_type'		=>'application/x-font-linux-psf',
			'file_extension'			=>'psf'
		),
		'application/vnd.publishare-delta-tree'=>array(
			'title'				=>'PubliShare Objects',
			'content_type'		=>'application/vnd.publishare-delta-tree',
			'file_extension'			=>'qps'
		),
		'application/vnd.pmi.widget'=>array(
			'title'				=>'Qualcomm\'s Plaza Mobile Internet',
			'content_type'		=>'application/vnd.pmi.widget',
			'file_extension'			=>'wg'
		),
		'application/vnd.quark.quarkxpress'=>array(
			'title'				=>'QuarkXpress',
			'content_type'		=>'application/vnd.quark.quarkxpress',
			'file_extension'			=>'qxd'
		),
		'application/vnd.epson.esf'=>array(
			'title'				=>'QUASS Stream Player',
			'content_type'		=>'application/vnd.epson.esf',
			'file_extension'			=>'esf'
		),
		'application/vnd.epson.msf'=>array(
			'title'				=>'QUASS Stream Player',
			'content_type'		=>'application/vnd.epson.msf',
			'file_extension'			=>'msf'
		),
		'application/vnd.epson.ssf'=>array(
			'title'				=>'QUASS Stream Player',
			'content_type'		=>'application/vnd.epson.ssf',
			'file_extension'			=>'ssf'
		),
		'application/vnd.epson.quickanime'=>array(
			'title'				=>'QuickAnime Player',
			'content_type'		=>'application/vnd.epson.quickanime',
			'file_extension'			=>'qam'
		),
		'application/vnd.intu.qfx'=>array(
			'title'				=>'Quicken',
			'content_type'		=>'application/vnd.intu.qfx',
			'file_extension'			=>'qfx'
		),
		'video/quicktime'=>array(
			'title'				=>'Quicktime Video',
			'content_type'		=>'video/quicktime',
			'file_extension'			=>'qt'
		),
		'application/x-rar-compressed'=>array(
			'title'				=>'RAR Archive',
			'content_type'		=>'application/x-rar-compressed',
			'file_extension'			=>'rar'
		),
		'audio/x-pn-realaudio'=>array(
			'title'				=>'Real Audio Sound',
			'content_type'		=>'audio/x-pn-realaudio',
			'file_extension'			=>'ram'
		),
		'audio/x-pn-realaudio-plugin'=>array(
			'title'				=>'Real Audio Sound',
			'content_type'		=>'audio/x-pn-realaudio-plugin',
			'file_extension'			=>'rmp'
		),
		'application/rsd+xml'=>array(
			'title'				=>'Really Simple Discovery',
			'content_type'		=>'application/rsd+xml',
			'file_extension'			=>'rsd'
		),
		'application/vnd.rn-realmedia'=>array(
			'title'				=>'RealMedia',
			'content_type'		=>'application/vnd.rn-realmedia',
			'file_extension'			=>'rm'
		),
		'application/vnd.realvnc.bed'=>array(
			'title'				=>'RealVNC',
			'content_type'		=>'application/vnd.realvnc.bed',
			'file_extension'			=>'bed'
		),
		'application/vnd.recordare.musicxml'=>array(
			'title'				=>'Recordare Applications',
			'content_type'		=>'application/vnd.recordare.musicxml',
			'file_extension'			=>'mxl'
		),
		'application/vnd.recordare.musicxml+xml'=>array(
			'title'				=>'Recordare Applications',
			'content_type'		=>'application/vnd.recordare.musicxml+xml',
			'file_extension'			=>'xml'
		),
		'application/relax-ng-compact-syntax'=>array(
			'title'				=>'Relax NG Compact Syntax',
			'content_type'		=>'application/relax-ng-compact-syntax',
			'file_extension'			=>'rnc'
		),
		'application/vnd.data-vision.rdz'=>array(
			'title'				=>'RemoteDocs R-Viewer',
			'content_type'		=>'application/vnd.data-vision.rdz',
			'file_extension'			=>'rdz'
		),
		'application/rdf+xml'=>array(
			'title'				=>'Resource Description Framework',
			'content_type'		=>'application/rdf+xml',
			'file_extension'			=>'rdf'
		),
		'application/vnd.cloanto.rp9'=>array(
			'title'				=>'RetroPlatform Player',
			'content_type'		=>'application/vnd.cloanto.rp9',
			'file_extension'			=>'rp9'
		),
		'application/vnd.jisp'=>array(
			'title'				=>'RhymBox',
			'content_type'		=>'application/vnd.jisp',
			'file_extension'			=>'isp'
		),
		'application/rtf'=>array(
			'title'				=>'Rich Text Format',
			'content_type'		=>'application/rtf',
			'file_extension'			=>'rtf'
		),
		'text/richtext'=>array(
			'title'				=>'Rich Text Format (RTF)',
			'content_type'		=>'text/richtext',
			'file_extension'			=>'rtx'
		),
		'application/vnd.route66.link66+xml'=>array(
			'title'				=>'ROUTE 66 Location Based Services',
			'content_type'		=>'application/vnd.route66.link66+xml',
			'file_extension'			=>'k66'
		),
		'application/rss+xml'=>array(
			'title'				=>'RSS - Really Simple Syndication',
			'content_type'		=>'application/rss+xml',
			'file_extension'			=>'xml'
		),
		'application/shf+xml'=>array(
			'title'				=>'S Hexdump Format',
			'content_type'		=>'application/shf+xml',
			'file_extension'			=>'shf'
		),
		'application/vnd.sailingtracker.track'=>array(
			'title'				=>'SailingTracker',
			'content_type'		=>'application/vnd.sailingtracker.track',
			'file_extension'			=>'st'
		),
		'image/svg+xml'=>array(
			'title'				=>'Scalable Vector Graphics (SVG)',
			'content_type'		=>'image/svg+xml',
			'file_extension'			=>'svg'
		),
		'application/vnd.sus-calendar'=>array(
			'title'				=>'ScheduleUs',
			'content_type'		=>'application/vnd.sus-calendar',
			'file_extension'			=>'sus'
		),
		'application/sru+xml'=>array(
			'title'				=>'Search/Retrieve via URL Response Format',
			'content_type'		=>'application/sru+xml',
			'file_extension'			=>'sru'
		),
		'application/set-payment-initiation'=>array(
			'title'				=>'Secure Electronic Transaction - Payment',
			'content_type'		=>'application/set-payment-initiation',
			'file_extension'			=>'pay'
		),
		'application/set-registration-initiation'=>array(
			'title'				=>'Secure Electronic Transaction - Registration',
			'content_type'		=>'application/set-registration-initiation',
			'file_extension'			=>'reg'
		),
		'application/vnd.sema'=>array(
			'title'				=>'Secured eMail',
			'content_type'		=>'application/vnd.sema',
			'file_extension'			=>'ema'
		),
		'application/vnd.semd'=>array(
			'title'				=>'Secured eMail',
			'content_type'		=>'application/vnd.semd',
			'file_extension'			=>'emd'
		),
		'application/vnd.semf'=>array(
			'title'				=>'Secured eMail',
			'content_type'		=>'application/vnd.semf',
			'file_extension'			=>'emf'
		),
		'application/vnd.seemail'=>array(
			'title'				=>'SeeMail',
			'content_type'		=>'application/vnd.seemail',
			'file_extension'			=>'see'
		),
		'application/x-font-snf'=>array(
			'title'				=>'Server Normal Format',
			'content_type'		=>'application/x-font-snf',
			'file_extension'			=>'snf'
		),
		'application/scvp-vp-request'=>array(
			'title'				=>'Server-Based Certificate Validation Protocol - Validation Policies - Request',
			'content_type'		=>'application/scvp-vp-request',
			'file_extension'			=>'spq'
		),
		'application/scvp-vp-response'=>array(
			'title'				=>'Server-Based Certificate Validation Protocol - Validation Policies - Response',
			'content_type'		=>'application/scvp-vp-response',
			'file_extension'			=>'spp'
		),
		'application/scvp-cv-request'=>array(
			'title'				=>'Server-Based Certificate Validation Protocol - Validation Request',
			'content_type'		=>'application/scvp-cv-request',
			'file_extension'			=>'scq'
		),
		'application/scvp-cv-response'=>array(
			'title'				=>'Server-Based Certificate Validation Protocol - Validation Response',
			'content_type'		=>'application/scvp-cv-response',
			'file_extension'			=>'scs'
		),
		'application/sdp'=>array(
			'title'				=>'Session Description Protocol',
			'content_type'		=>'application/sdp',
			'file_extension'			=>'sdp'
		),
		'text/x-setext'=>array(
			'title'				=>'Setext',
			'content_type'		=>'text/x-setext',
			'file_extension'			=>'etx'
		),
		'video/x-sgi-movie'=>array(
			'title'				=>'SGI Movie',
			'content_type'		=>'video/x-sgi-movie',
			'file_extension'			=>'vie'
		),
		'application/vnd.shana.informed.formdata'=>array(
			'title'				=>'Shana Informed Filler',
			'content_type'		=>'application/vnd.shana.informed.formdata',
			'file_extension'			=>'ifm'
		),
		'application/vnd.shana.informed.formtemplate'=>array(
			'title'				=>'Shana Informed Filler',
			'content_type'		=>'application/vnd.shana.informed.formtemplate',
			'file_extension'			=>'itp'
		),
		'application/vnd.shana.informed.interchange'=>array(
			'title'				=>'Shana Informed Filler',
			'content_type'		=>'application/vnd.shana.informed.interchange',
			'file_extension'			=>'iif'
		),
		'application/vnd.shana.informed.package'=>array(
			'title'				=>'Shana Informed Filler',
			'content_type'		=>'application/vnd.shana.informed.package',
			'file_extension'			=>'ipk'
		),
		'application/thraud+xml'=>array(
			'title'				=>'Sharing Transaction Fraud Data',
			'content_type'		=>'application/thraud+xml',
			'file_extension'			=>'tfi'
		),
		'application/x-shar'=>array(
			'title'				=>'Shell Archive',
			'content_type'		=>'application/x-shar',
			'file_extension'			=>'har'
		),
		'image/x-rgb'=>array(
			'title'				=>'Silicon Graphics RGB Bitmap',
			'content_type'		=>'image/x-rgb',
			'file_extension'			=>'rgb'
		),
		'application/vnd.epson.salt'=>array(
			'title'				=>'SimpleAnimeLite Player',
			'content_type'		=>'application/vnd.epson.salt',
			'file_extension'			=>'slt'
		),
		'application/vnd.accpac.simply.aso'=>array(
			'title'				=>'Simply Accounting',
			'content_type'		=>'application/vnd.accpac.simply.aso',
			'file_extension'			=>'aso'
		),
		'application/vnd.accpac.simply.imp'=>array(
			'title'				=>'Simply Accounting - Data Import',
			'content_type'		=>'application/vnd.accpac.simply.imp',
			'file_extension'			=>'imp'
		),
		'application/vnd.simtech-mindmapper'=>array(
			'title'				=>'SimTech MindMapper',
			'content_type'		=>'application/vnd.simtech-mindmapper',
			'file_extension'			=>'twd'
		),
		'application/vnd.commonspace'=>array(
			'title'				=>'Sixth Floor Media - CommonSpace',
			'content_type'		=>'application/vnd.commonspace',
			'file_extension'			=>'csp'
		),
		'application/vnd.yamaha.smaf-audio'=>array(
			'title'				=>'SMAF Audio',
			'content_type'		=>'application/vnd.yamaha.smaf-audio',
			'file_extension'			=>'saf'
		),
		'application/vnd.smaf'=>array(
			'title'				=>'SMAF File',
			'content_type'		=>'application/vnd.smaf',
			'file_extension'			=>'mmf'
		),
		'application/vnd.yamaha.smaf-phrase'=>array(
			'title'				=>'SMAF Phrase',
			'content_type'		=>'application/vnd.yamaha.smaf-phrase',
			'file_extension'			=>'spf'
		),
		'application/vnd.smart.teacher'=>array(
			'title'				=>'SMART Technologies Apps',
			'content_type'		=>'application/vnd.smart.teacher',
			'file_extension'			=>'her'
		),
		'application/vnd.svd'=>array(
			'title'				=>'SourceView Document',
			'content_type'		=>'application/vnd.svd',
			'file_extension'			=>'svd'
		),
		'application/sparql-query'=>array(
			'title'				=>'SPARQL - Query',
			'content_type'		=>'application/sparql-query',
			'file_extension'			=>'rq'
		),
		'application/sparql-results+xml'=>array(
			'title'				=>'SPARQL - Results',
			'content_type'		=>'application/sparql-results+xml',
			'file_extension'			=>'srx'
		),
		'application/srgs'=>array(
			'title'				=>'Speech Recognition Grammar Specification',
			'content_type'		=>'application/srgs',
			'file_extension'			=>'ram'
		),
		'application/srgs+xml'=>array(
			'title'				=>'Speech Recognition Grammar Specification - XML',
			'content_type'		=>'application/srgs+xml',
			'file_extension'			=>'xml'
		),
		'application/ssml+xml'=>array(
			'title'				=>'Speech Synthesis Markup Language',
			'content_type'		=>'application/ssml+xml',
			'file_extension'			=>'sml'
		),
		'application/vnd.koan'=>array(
			'title'				=>'SSEYO Koan Play File',
			'content_type'		=>'application/vnd.koan',
			'file_extension'			=>'skp'
		),
		'text/sgml'=>array(
			'title'				=>'Standard Generalized Markup Language (SGML)',
			'content_type'		=>'text/sgml',
			'file_extension'			=>'gml'
		),
		'application/vnd.stardivision.calc'=>array(
			'title'				=>'StarOffice - Calc',
			'content_type'		=>'application/vnd.stardivision.calc',
			'file_extension'			=>'sdc'
		),
		'application/vnd.stardivision.draw'=>array(
			'title'				=>'StarOffice - Draw',
			'content_type'		=>'application/vnd.stardivision.draw',
			'file_extension'			=>'sda'
		),
		'application/vnd.stardivision.impress'=>array(
			'title'				=>'StarOffice - Impress',
			'content_type'		=>'application/vnd.stardivision.impress',
			'file_extension'			=>'sdd'
		),
		'application/vnd.stardivision.math'=>array(
			'title'				=>'StarOffice - Math',
			'content_type'		=>'application/vnd.stardivision.math',
			'file_extension'			=>'smf'
		),
		'application/vnd.stardivision.writer'=>array(
			'title'				=>'StarOffice - Writer',
			'content_type'		=>'application/vnd.stardivision.writer',
			'file_extension'			=>'sdw'
		),
		'application/vnd.stardivision.writer-global'=>array(
			'title'				=>'StarOffice - Writer  (Global)',
			'content_type'		=>'application/vnd.stardivision.writer-global',
			'file_extension'			=>'sgl'
		),
		'application/vnd.stepmania.stepchart'=>array(
			'title'				=>'StepMania',
			'content_type'		=>'application/vnd.stepmania.stepchart',
			'file_extension'			=>'sm'
		),
		'application/x-stuffit'=>array(
			'title'				=>'Stuffit Archive',
			'content_type'		=>'application/x-stuffit',
			'file_extension'			=>'sit'
		),
		'application/x-stuffitx'=>array(
			'title'				=>'Stuffit Archive',
			'content_type'		=>'application/x-stuffitx',
			'file_extension'			=>'itx'
		),
		'application/vnd.solent.sdkm+xml'=>array(
			'title'				=>'SudokuMagic',
			'content_type'		=>'application/vnd.solent.sdkm+xml',
			'file_extension'			=>'dkm'
		),
		'application/vnd.olpc-sugar'=>array(
			'title'				=>'Sugar Linux Application Bundle',
			'content_type'		=>'application/vnd.olpc-sugar',
			'file_extension'			=>'xo'
		),
		'audio/basic'=>array(
			'title'				=>'Sun Audio - Au file format',
			'content_type'		=>'audio/basic',
			'file_extension'			=>'au'
		),
		'application/vnd.wqd'=>array(
			'title'				=>'SundaHus WQ',
			'content_type'		=>'application/vnd.wqd',
			'file_extension'			=>'wqd'
		),
		'application/vnd.symbian.install'=>array(
			'title'				=>'Symbian Install Package',
			'content_type'		=>'application/vnd.symbian.install',
			'file_extension'			=>'sis'
		),
		'application/smil+xml'=>array(
			'title'				=>'Synchronized Multimedia Integration Language',
			'content_type'		=>'application/smil+xml',
			'file_extension'			=>'smi'
		),
		'application/vnd.syncml+xml'=>array(
			'title'				=>'SyncML',
			'content_type'		=>'application/vnd.syncml+xml',
			'file_extension'			=>'xsm'
		),
		'application/vnd.syncml.dm+wbxml'=>array(
			'title'				=>'SyncML - Device Management',
			'content_type'		=>'application/vnd.syncml.dm+wbxml',
			'file_extension'			=>'bdm'
		),
		'application/vnd.syncml.dm+xml'=>array(
			'title'				=>'SyncML - Device Management',
			'content_type'		=>'application/vnd.syncml.dm+xml',
			'file_extension'			=>'xdm'
		),
		'application/x-sv4cpio'=>array(
			'title'				=>'System V Release 4 CPIO Archive',
			'content_type'		=>'application/x-sv4cpio',
			'file_extension'			=>'pio'
		),
		'application/x-sv4crc'=>array(
			'title'				=>'System V Release 4 CPIO Checksum Data',
			'content_type'		=>'application/x-sv4crc',
			'file_extension'			=>'crc'
		),
		'application/sbml+xml'=>array(
			'title'				=>'Systems Biology Markup Language',
			'content_type'		=>'application/sbml+xml',
			'file_extension'			=>'bml'
		),
		'text/tab-separated-values'=>array(
			'title'				=>'Tab Seperated Values',
			'content_type'		=>'text/tab-separated-values',
			'file_extension'			=>'tsv'
		),
		'image/tiff'=>array(
			'title'				=>'Tagged Image File Format',
			'content_type'		=>'image/tiff',
			'file_extension'			=>'tiff'
		),
		'application/vnd.tao.intent-module-archive'=>array(
			'title'				=>'Tao Intent',
			'content_type'		=>'application/vnd.tao.intent-module-archive',
			'file_extension'			=>'tao'
		),
		'application/x-tar'=>array(
			'title'				=>'Tar File (Tape Archive)',
			'content_type'		=>'application/x-tar',
			'file_extension'			=>'tar'
		),
		'application/x-tcl'=>array(
			'title'				=>'Tcl Script',
			'content_type'		=>'application/x-tcl',
			'file_extension'			=>'tcl'
		),
		'application/x-tex'=>array(
			'title'				=>'TeX',
			'content_type'		=>'application/x-tex',
			'file_extension'			=>'tex'
		),
		'application/x-tex-tfm'=>array(
			'title'				=>'TeX Font Metric',
			'content_type'		=>'application/x-tex-tfm',
			'file_extension'			=>'tfm'
		),
		'application/tei+xml'=>array(
			'title'				=>'Text Encoding and Interchange',
			'content_type'		=>'application/tei+xml',
			'file_extension'			=>'tei'
		),
		'text/plain'=>array(
			'title'				=>'Text File',
			'content_type'		=>'text/plain',
			'file_extension'			=>'txt'
		),
		'application/vnd.spotfire.dxp'=>array(
			'title'				=>'TIBCO Spotfire',
			'content_type'		=>'application/vnd.spotfire.dxp',
			'file_extension'			=>'dxp'
		),
		'application/vnd.spotfire.sfs'=>array(
			'title'				=>'TIBCO Spotfire',
			'content_type'		=>'application/vnd.spotfire.sfs',
			'file_extension'			=>'sfs'
		),
		'application/timestamped-data'=>array(
			'title'				=>'Time Stamped Data Envelope',
			'content_type'		=>'application/timestamped-data',
			'file_extension'			=>'tsd'
		),
		'application/vnd.trid.tpt'=>array(
			'title'				=>'TRI Systems Config',
			'content_type'		=>'application/vnd.trid.tpt',
			'file_extension'			=>'tpt'
		),
		'application/vnd.triscape.mxs'=>array(
			'title'				=>'Triscape Map Explorer',
			'content_type'		=>'application/vnd.triscape.mxs',
			'file_extension'			=>'mxs'
		),
		'text/troff'=>array(
			'title'				=>'troff',
			'content_type'		=>'text/troff',
			'file_extension'			=>'t'
		),
		'application/vnd.trueapp'=>array(
			'title'				=>'True BASIC',
			'content_type'		=>'application/vnd.trueapp',
			'file_extension'			=>'tra'
		),
		'application/x-font-ttf'=>array(
			'title'				=>'TrueType Font',
			'content_type'		=>'application/x-font-ttf',
			'file_extension'			=>'ttf'
		),
		'text/turtle'=>array(
			'title'				=>'Turtle (Terse RDF Triple Language)',
			'content_type'		=>'text/turtle',
			'file_extension'			=>'ttl'
		),
		'application/vnd.umajin'=>array(
			'title'				=>'UMAJIN',
			'content_type'		=>'application/vnd.umajin',
			'file_extension'			=>'umj'
		),
		'application/vnd.uoml+xml'=>array(
			'title'				=>'Unique Object Markup Language',
			'content_type'		=>'application/vnd.uoml+xml',
			'file_extension'			=>'oml'
		),
		'application/vnd.unity'=>array(
			'title'				=>'Unity 3d',
			'content_type'		=>'application/vnd.unity',
			'file_extension'			=>'web'
		),
		'application/vnd.ufdl'=>array(
			'title'				=>'Universal Forms Description Language',
			'content_type'		=>'application/vnd.ufdl',
			'file_extension'			=>'ufd'
		),
		'text/uri-list'=>array(
			'title'				=>'URI Resolution Services',
			'content_type'		=>'text/uri-list',
			'file_extension'			=>'uri'
		),
		'application/vnd.uiq.theme'=>array(
			'title'				=>'User Interface Quartz - Theme (Symbian)',
			'content_type'		=>'application/vnd.uiq.theme',
			'file_extension'			=>'utz'
		),
		'application/x-ustar'=>array(
			'title'				=>'Ustar (Uniform Standard Tape Archive)',
			'content_type'		=>'application/x-ustar',
			'file_extension'			=>'tar'
		),
		'text/x-uuencode'=>array(
			'title'				=>'UUEncode',
			'content_type'		=>'text/x-uuencode',
			'file_extension'			=>'uu'
		),
		'text/x-vcalendar'=>array(
			'title'				=>'vCalendar',
			'content_type'		=>'text/x-vcalendar',
			'file_extension'			=>'vcs'
		),
		'text/x-vcard'=>array(
			'title'				=>'vCard',
			'content_type'		=>'text/x-vcard',
			'file_extension'			=>'vcf'
		),
		'application/x-cdlink'=>array(
			'title'				=>'Video CD',
			'content_type'		=>'application/x-cdlink',
			'file_extension'			=>'vcd'
		),
		'application/vnd.vsf'=>array(
			'title'				=>'Viewport+',
			'content_type'		=>'application/vnd.vsf',
			'file_extension'			=>'vsf'
		),
		'model/vrml'=>array(
			'title'				=>'Virtual Reality Modeling Language',
			'content_type'		=>'model/vrml',
			'file_extension'			=>'wrl'
		),
		'application/vnd.vcx'=>array(
			'title'				=>'VirtualCatalog',
			'content_type'		=>'application/vnd.vcx',
			'file_extension'			=>'vcx'
		),
		'model/vnd.mts'=>array(
			'title'				=>'Virtue MTS',
			'content_type'		=>'model/vnd.mts',
			'file_extension'			=>'mts'
		),
		'model/vnd.vtu'=>array(
			'title'				=>'Virtue VTU',
			'content_type'		=>'model/vnd.vtu',
			'file_extension'			=>'vtu'
		),
		'application/vnd.visionary'=>array(
			'title'				=>'Visionary',
			'content_type'		=>'application/vnd.visionary',
			'file_extension'			=>'vis'
		),
		'video/vnd.vivo'=>array(
			'title'				=>'Vivo',
			'content_type'		=>'video/vnd.vivo',
			'file_extension'			=>'viv'
		),
		'application/ccxml+xml,'=>array(
			'title'				=>'Voice Browser Call Control',
			'content_type'		=>'application/ccxml+xml,',
			'file_extension'			=>'xml'
		),
		'application/voicexml+xml'=>array(
			'title'				=>'VoiceXML',
			'content_type'		=>'application/voicexml+xml',
			'file_extension'			=>'xml'
		),
		'application/x-wais-source'=>array(
			'title'				=>'WAIS Source',
			'content_type'		=>'application/x-wais-source',
			'file_extension'			=>'src'
		),
		'application/vnd.wap.wbxml'=>array(
			'title'				=>'WAP Binary XML (WBXML)',
			'content_type'		=>'application/vnd.wap.wbxml',
			'file_extension'			=>'xml'
		),
		'image/vnd.wap.wbmp'=>array(
			'title'				=>'WAP Bitamp (WBMP)',
			'content_type'		=>'image/vnd.wap.wbmp',
			'file_extension'			=>'bmp'
		),
		'audio/x-wav'=>array(
			'title'				=>'Waveform Audio File Format (WAV)',
			'content_type'		=>'audio/x-wav',
			'file_extension'			=>'wav'
		),
		'application/davmount+xml'=>array(
			'title'				=>'Web Distributed Authoring and Versioning',
			'content_type'		=>'application/davmount+xml',
			'file_extension'			=>'unt'
		),
		'application/x-font-woff'=>array(
			'title'				=>'Web Open Font Format',
			'content_type'		=>'application/x-font-woff',
			'file_extension'			=>'off'
		),
		'application/wspolicy+xml'=>array(
			'title'				=>'Web Services Policy',
			'content_type'		=>'application/wspolicy+xml',
			'file_extension'			=>'icy'
		),
		'image/webp'=>array(
			'title'				=>'WebP Image',
			'content_type'		=>'image/webp',
			'file_extension'			=>'ebp'
		),
		'application/vnd.webturbo'=>array(
			'title'				=>'WebTurbo',
			'content_type'		=>'application/vnd.webturbo',
			'file_extension'			=>'wtb'
		),
		'application/widget'=>array(
			'title'				=>'Widget Packaging and XML Configuration',
			'content_type'		=>'application/widget',
			'file_extension'			=>'wgt'
		),
		'application/winhlp'=>array(
			'title'				=>'WinHelp',
			'content_type'		=>'application/winhlp',
			'file_extension'			=>'hlp'
		),
		'text/vnd.wap.wml'=>array(
			'title'				=>'Wireless Markup Language (WML)',
			'content_type'		=>'text/vnd.wap.wml',
			'file_extension'			=>'wml'
		),
		'text/vnd.wap.wmlscript'=>array(
			'title'				=>'Wireless Markup Language Script (WMLScript)',
			'content_type'		=>'text/vnd.wap.wmlscript',
			'file_extension'			=>'mls'
		),
		'application/vnd.wap.wmlscriptc'=>array(
			'title'				=>'WMLScript',
			'content_type'		=>'application/vnd.wap.wmlscriptc',
			'file_extension'			=>'lsc'
		),
		'application/vnd.wordperfect'=>array(
			'title'				=>'Wordperfect',
			'content_type'		=>'application/vnd.wordperfect',
			'file_extension'			=>'wpd'
		),
		'application/vnd.wt.stf'=>array(
			'title'				=>'Worldtalk',
			'content_type'		=>'application/vnd.wt.stf',
			'file_extension'			=>'stf'
		),
		'application/wsdl+xml'=>array(
			'title'				=>'WSDL - Web Services Description Language',
			'content_type'		=>'application/wsdl+xml',
			'file_extension'			=>'sdl'
		),
		'image/x-xbitmap'=>array(
			'title'				=>'X BitMap',
			'content_type'		=>'image/x-xbitmap',
			'file_extension'			=>'xbm'
		),
		'image/x-xpixmap'=>array(
			'title'				=>'X PixMap',
			'content_type'		=>'image/x-xpixmap',
			'file_extension'			=>'xpm'
		),
		'image/x-xwindowdump'=>array(
			'title'				=>'X Window Dump',
			'content_type'		=>'image/x-xwindowdump',
			'file_extension'			=>'xwd'
		),
		'application/x-x509-ca-cert'=>array(
			'title'				=>'X.509 Certificate',
			'content_type'		=>'application/x-x509-ca-cert',
			'file_extension'			=>'der'
		),
		'application/x-xfig'=>array(
			'title'				=>'Xfig',
			'content_type'		=>'application/x-xfig',
			'file_extension'			=>'fig'
		),
		'application/xhtml+xml'=>array(
			'title'				=>'XHTML - The Extensible HyperText Markup Language',
			'content_type'		=>'application/xhtml+xml',
			'file_extension'			=>'tml'
		),
		'application/xml'=>array(
			'title'				=>'XML - Extensible Markup Language',
			'content_type'		=>'application/xml',
			'file_extension'			=>'xml'
		),
		'application/xcap-diff+xml'=>array(
			'title'				=>'XML Configuration Access Protocol - XCAP Diff',
			'content_type'		=>'application/xcap-diff+xml',
			'file_extension'			=>'xdf'
		),
		'application/xenc+xml'=>array(
			'title'				=>'XML Encryption Syntax and Processing',
			'content_type'		=>'application/xenc+xml',
			'file_extension'			=>'enc'
		),
		'application/patch-ops-error+xml'=>array(
			'title'				=>'XML Patch Framework',
			'content_type'		=>'application/patch-ops-error+xml',
			'file_extension'			=>'xer'
		),
		'application/resource-lists+xml'=>array(
			'title'				=>'XML Resource Lists',
			'content_type'		=>'application/resource-lists+xml',
			'file_extension'			=>'rl'
		),
		'application/rls-services+xml'=>array(
			'title'				=>'XML Resource Lists',
			'content_type'		=>'application/rls-services+xml',
			'file_extension'			=>'rs'
		),
		'application/resource-lists-diff+xml'=>array(
			'title'				=>'XML Resource Lists Diff',
			'content_type'		=>'application/resource-lists-diff+xml',
			'file_extension'			=>'rld'
		),
		'application/xslt+xml'=>array(
			'title'				=>'XML Transformations',
			'content_type'		=>'application/xslt+xml',
			'file_extension'			=>'slt'
		),
		'application/xop+xml'=>array(
			'title'				=>'XML-Binary Optimized Packaging',
			'content_type'		=>'application/xop+xml',
			'file_extension'			=>'xop'
		),
		'application/x-xpinstall'=>array(
			'title'				=>'XPInstall - Mozilla',
			'content_type'		=>'application/x-xpinstall',
			'file_extension'			=>'xpi'
		),
		'application/xspf+xml'=>array(
			'title'				=>'XSPF - XML Shareable Playlist Format',
			'content_type'		=>'application/xspf+xml',
			'file_extension'			=>'spf'
		),
		'application/vnd.mozilla.xul+xml'=>array(
			'title'				=>'XUL - XML User Interface Language',
			'content_type'		=>'application/vnd.mozilla.xul+xml',
			'file_extension'			=>'xul'
		),
		'chemical/x-xyz'=>array(
			'title'				=>'XYZ File Format',
			'content_type'		=>'chemical/x-xyz',
			'file_extension'			=>'xyz'
		),
		'text/yaml'=>array(
			'title'				=>'YAML Ain\'t Markup Language / Yet Another Markup Language',
			'content_type'		=>'text/yaml',
			'file_extension'			=>'aml'
		),
		'application/yang'=>array(
			'title'				=>'YANG Data Modeling Language',
			'content_type'		=>'application/yang',
			'file_extension'			=>'ang'
		),
		'application/yin+xml'=>array(
			'title'				=>'YIN (YANG - XML)',
			'content_type'		=>'application/yin+xml',
			'file_extension'			=>'yin'
		),
		'application/vnd.zul'=>array(
			'title'				=>'Z.U.L. Geometry',
			'content_type'		=>'application/vnd.zul',
			'file_extension'			=>'zir'
		),
		'application/zip'=>array(
			'title'				=>'Zip Archive',
			'content_type'		=>'application/zip',
			'file_extension'			=>'zip'
		),
		'application/vnd.handheld-entertainment+xml'=>array(
			'title'				=>'ZVUE Media Manager',
			'content_type'		=>'application/vnd.handheld-entertainment+xml',
			'file_extension'			=>'zmm'
		),
		'application/vnd.zzazz.deck+xml'=>array(
			'title'				=>'Zzazz Deck',
			'content_type'		=>'application/vnd.zzazz.deck+xml',
			'file_extension'			=>'zaz'
		)
    );

}