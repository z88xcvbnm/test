<?php

namespace Core\Module\Header;

use Core\Module\Exception\ParametersException;

class HeaderContentType{

    /** @var array */
    public  static $content_type_list=array(
		'video/3gpp'=>array(
			'title'				=>'3GP',
			'content_type'		=>'video/3gpp',
			'file_extension'			=>'3gp'
		),
		'video/3gpp2'=>array(
			'title'				=>'3GP2',
			'content_type'		=>'video/3gpp2',
			'file_extension'			=>'3g2'
		),
		'application/x-shockwave-flash'=>array(
			'title'				=>'Adobe Flash',
			'content_type'		=>'application/x-shockwave-flash',
			'file_extension'			=>'swf'
		),
		'application/pdf'=>array(
			'title'				=>'Adobe Portable Document Format',
			'content_type'		=>'application/pdf',
			'file_extension'			=>'pdf'
		),
		'application/vnd.cups-ppd'=>array(
			'title'				=>'Adobe PostScript Printer Description File Format',
			'content_type'		=>'application/vnd.cups-ppd',
			'file_extension'			=>'ppd'
		),
		'audio/x-aac'=>array(
			'title'				=>'Advanced Audio Coding (AAC)',
			'content_type'		=>'audio/x-aac',
			'file_extension'			=>'aac'
		),
		'application/vnd.android.package-archive'=>array(
			'title'				=>'Android Package Archive',
			'content_type'		=>'application/vnd.android.package-archive',
			'file_extension'			=>'apk'
		),
		'video/x-msvideo'=>array(
			'title'				=>'Audio Video Interleave (AVI)',
			'content_type'		=>'video/x-msvideo',
			'file_extension'			=>'avi'
		),
		'image/prs.btif'=>array(
			'title'				=>'BTIF',
			'content_type'		=>'image/prs.btif',
			'file_extension'			=>'tif'
		),
		'application/vnd.chemdraw+xml'=>array(
			'title'				=>'CambridgeSoft Chem Draw',
			'content_type'		=>'application/vnd.chemdraw+xml',
			'file_extension'			=>'xml'
		),
		'text/css'=>array(
			'title'				=>'Cascading Style Sheets (CSS)',
			'content_type'		=>'text/css',
			'file_extension'			=>'css'
		),
		'image/bmp'=>array(
			'title'				=>'Bitmap Image File',
			'content_type'		=>'image/bmp',
			'file_extension'			=>'bmp'
		),
		'image/jpeg'=>array(
			'title'				=>'JPEG Image',
			'content_type'		=>'image/jpeg',
			'file_extension'			=>'jpg'
		),
		'image/x-citrix-jpeg'=>array(
			'title'				=>'JPEG Image (Citrix client)',
			'content_type'		=>'image/x-citrix-jpeg',
			'file_extension'			=>'jpg'
		),
		'image/pjpeg'=>array(
			'title'				=>'JPEG Image (Progressive)',
			'content_type'		=>'image/pjpeg',
			'file_extension'			=>'peg'
		),
		'video/jpeg'=>array(
			'title'				=>'JPGVideo',
			'content_type'		=>'video/jpeg',
			'file_extension'			=>'pgv'
		),
		'image/png'=>array(
			'title'				=>'Portable Network Graphics (PNG)',
			'content_type'		=>'image/png',
			'file_extension'			=>'png'
		),
		'image/x-citrix-png'=>array(
			'title'				=>'Portable Network Graphics (PNG) (Citrix client)',
			'content_type'		=>'image/x-citrix-png',
			'file_extension'			=>'png'
		),
		'image/x-png'=>array(
			'title'				=>'Portable Network Graphics (PNG) (x-token)',
			'content_type'		=>'image/x-png',
			'file_extension'			=>'png'
		),
		'image/gif'=>array(
			'title'				=>'Graphics Interchange Format',
			'content_type'		=>'image/gif',
			'file_extension'			=>'gif'
		),
		'application/zip'=>array(
			'title'				=>'Zip Archive',
			'content_type'		=>'application/zip',
			'file_extension'			=>'zip'
		),
		'application/x-rar-compressed'=>array(
			'title'				=>'RAR Archive',
			'content_type'		=>'application/x-rar-compressed',
			'file_extension'			=>'rar'
		),
		'application/json'=>array(
			'title'				=>'JavaScript Object Notation (JSON)',
			'content_type'		=>'application/json',
			'file_extension'			=>'son'
		),
    );

    /**
     * @param string|NULL $type
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
	public  static function init(string $type=NULL){

		if(empty(self::$content_type_list[$type])){

			$error=array(
				'title'		=>'Parameters problem',
				'info'		=>'Content type is not exists in content type list',
				'data'		=>array(
					'type'		=>$type
				)
			);

			throw new ParametersException($error);

		}

		header('Content-Type: '.self::$content_type_list[$type]['content_type']);

		return true;

	}

}