<?php

namespace Core\Module\Header;

use Core\Module\Device\DeviceType;
use Core\Module\Exception\ParametersException;
use Core\Module\Response\Response;

class HeaderCode{

    /** @var array */
    public static $code_list=array(
        '100'=>array(
			'code'				=>'100',
			'title'			    =>'100 Continue',
			'content_type'		=>'This interim response indicates that everything so far is OK and that the client should continue with the request or ignore it if it is already finished.'
		),
		'101'=>array(
			'code'				=>'101',
			'title'			    =>'101 Switching Protocol',
			'content_type'		=>'This code is sent in response to an '
		),
		'200'=>array(
			'code'				=>'200',
			'title'			    =>'200 OK',
			'content_type'		=>'The request has succeeded. The meaning of a success varies depending on the HTTP method:'
		),
		'201'=>array(
			'code'				=>'201',
			'title'			    =>'201 Created',
			'content_type'		=>'The request has succeeded and a new resource has been created as a result of it. This is typically the response sent after a PUT request.'
		),
		'202'=>array(
			'code'				=>'202',
			'title'			    =>'202 Accepted',
			'content_type'		=>'The request has been received but not yet acted upon. It is non-committal, meaning that there is no way in HTTP to later send an asynchronous response indicating the outcome of processing the request. It is intended for cases where another process or server handles the request, or for batch processing.'
		),
		'203'=>array(
			'code'				=>'203',
			'title'			    =>'203 Non-Authoritative Information',
			'content_type'		=>'This response code means returned meta-information set is not exact set as available from the origin server, but collected from a local or a third party copy. Except this condition, 200 OK response should be preferred instead of this response.'
		),
		'204'=>array(
			'code'				=>'204',
			'title'			    =>'204 No Content',
			'content_type'		=>'There is no content to send for this request, but the headers may be useful. The user-agent may update its cached headers for this resource with the new ones.'
		),
		'205'=>array(
			'code'				=>'205',
			'title'			    =>'205 Reset Content',
			'content_type'		=>'This response code is sent after accomplishing request to tell user agent reset document view which sent this request.'
		),
		'206'=>array(
			'code'				=>'206',
			'title'			    =>'206 Partial Content',
			'content_type'		=>'This response code is used because of range header sent by the client to separate download into multiple streams.'
		),
		'300'=>array(
			'code'				=>'300',
			'title'			    =>'300 Multiple Choice',
			'content_type'		=>'The request has more than one possible responses. User-agent or user should choose one of them. There is no standardized way to choose one of the responses.'
		),
		'301'=>array(
			'code'				=>'301',
			'title'			    =>'301 Moved Permanently',
			'content_type'		=>'This response code means that URI of requested resource has been changed. Probably, new URI would be given in the response.'
		),
		'302'=>array(
			'code'				=>'302',
			'title'			    =>'302 Found',
			'content_type'		=>'This response code means that URI of requested resource has been changed '
		),
		'303'=>array(
			'code'				=>'303',
			'title'			    =>'303 See Other',
			'content_type'		=>'Server sent this response to directing client to get requested resource to another URI with an GET request.'
		),
		'304'=>array(
			'code'				=>'304',
			'title'			    =>'304 Not Modified',
			'content_type'		=>'This is used for caching purposes. It is telling to client that response has not been modified. So, client can continue to use same cached version of response.'
		),
		'305'=>array(
			'code'				=>'305',
			'title'			    =>'305 Use Proxy',
			'content_type'		=>'This means requested response must be accessed by a proxy. This response code is not largely supported because security reasons.'
		),
		'306'=>array(
			'code'				=>'306',
			'title'			    =>'306 Unused',
			'content_type'		=>'This response code is no longer used, it is just reserved currently. It was used in a previous version of the HTTP 1.1 specification.'
		),
		'307'=>array(
			'code'				=>'307',
			'title'			    =>'307 Temporary Redirect',
			'content_type'		=>'Server sent this response to directing client to get requested resource to another URI with same method that used prior request. This has the same semantic than the '
		),
		'308'=>array(
			'code'				=>'308',
			'title'			    =>'308 Permanent Redirect',
			'content_type'		=>'This means that the resource is now permanently located at another URI, specified by the '
		),
		'400'=>array(
			'code'				=>'400',
			'title'			    =>'400 Bad Request',
			'content_type'		=>'This response means that server could not understand the request due to invalid syntax.'
		),
		'401'=>array(
			'code'				=>'401',
			'title'			    =>'401 Unauthorized',
			'content_type'		=>'Authentication is needed to get requested response. This is similar to 403, but in this case, authentication is possible.'
		),
		'402'=>array(
			'code'				=>'402',
			'title'			    =>'402 Payment Required',
			'content_type'		=>'This response code is reserved for future use. Initial aim for creating this code was using it for digital payment systems however this is not used currently.'
		),
		'403'=>array(
			'code'				=>'403',
			'title'			    =>'403 Forbidden',
			'content_type'		=>'Client does not have access rights to the content so server is rejecting to give proper response.'
		),
		'404'=>array(
			'code'				=>'404',
			'title'			    =>'404 Not Found',
			'content_type'		=>'Server can not find requested resource. This response code probably is most famous one due to its frequency to occur in web.'
		),
		'405'=>array(
			'code'				=>'405',
			'title'			    =>'405 Method Not Allowed',
			'content_type'		=>'The request method is known by the server but has been disabled and cannot be used. The two mandatory methods, '
		),
		'406'=>array(
			'code'				=>'406',
			'title'			    =>'406 Not Acceptable',
			'content_type'		=>'This response is sent when the web server, after performing '
		),
		'407'=>array(
			'code'				=>'407',
			'title'			    =>'407 Proxy Authentication Required',
			'content_type'		=>'This is similar to 401 but authentication is needed to be done by a proxy.'
		),
		'408'=>array(
			'code'				=>'408',
			'title'			    =>'408 Request Timeout',
			'content_type'		=>'This response is sent on an idle connection by some servers, even without any previous request by the client. It means that the server would like to shut down this unused connection. This response is used much more since some browsers, like Chrome or IE9, use '
		),
		'409'=>array(
			'code'				=>'409',
			'title'			    =>'409 Conflict',
			'content_type'		=>'This response would be sent when a request conflict with current state of server.'
		),
		'410'=>array(
			'code'				=>'410',
			'title'			    =>'410 Gone',
			'content_type'		=>'This response would be sent when requested content has been deleted from server.'
		),
		'411'=>array(
			'code'				=>'411',
			'title'			    =>'411 Length Required',
			'content_type'		=>'Server rejected the request because the '
		),
		'412'=>array(
			'code'				=>'412',
			'title'			    =>'412 Precondition Failed',
			'content_type'		=>'The client has indicated preconditions in its headers which the server does not meet.'
		),
		'413'=>array(
			'code'				=>'413',
			'title'			    =>'413 Payload Too Large',
			'content_type'		=>'Request entity is larger than limits defined by server; the server might close the connection or return an '
		),
		'414'=>array(
			'code'				=>'414',
			'title'			    =>'414 URI Too Long',
			'content_type'		=>'The URI requested by the client is longer than the server is willing to interpret.'
		),
		'415'=>array(
			'code'				=>'415',
			'title'			    =>'415 Unsupported Media Type',
			'content_type'		=>'The media format of the requested data is not supported by the server, so the server is rejecting the request.'
		),
		'416'=>array(
			'code'				=>'416',
			'title'			    =>'416 Requested Range Not Satisfiable',
			'content_type'		=>'The range specified by the '
		),
		'417'=>array(
			'code'				=>'417',
			'title'			    =>'417 Expectation Failed',
			'content_type'		=>'This response code means the expectation indicated by the '
		),
		'421'=>array(
			'code'				=>'421',
			'title'			    =>'421 Misdirected Request',
			'content_type'		=>'The request was directed at a server that is not able to produce a response. This can be sent by a server that is not configured to produce responses for the combination of scheme and authority that are included in the request URI.'
		),
		'422'=>array(
			'code'				=>'422',
			'title'			    =>'422 Unprocessable Entity',
			'content_type'		=>'The request was well-formed but was unable to be followed due to semantic errors.'
		),
		'423'=>array(
			'code'				=>'423',
			'title'			    =>'423 Locked',
			'content_type'		=>'The resource that is being accessed is locked.'
		),
		'424'=>array(
			'code'				=>'424',
			'title'			    =>'424 Failed Dependency',
			'content_type'		=>'The request failed due to failure of a previous request.'
		),
		'425'=>array(
			'code'				=>'425',
			'title'			    =>'425 Upgrade Required',
			'content_type'		=>'The client should switch to a different protocol such as TLS/1.0, given in the Upgrade header field.'
		),
		'426'=>array(
			'code'				=>'426',
			'title'			    =>'426 Upgrade Required',
			'content_type'		=>'The server refuses to perform the request using the current protocol but might be willing to do so after the client upgrades to a different protocol. The server MUST send an Upgrade header field in a 426 response to indicate the required protocol(s) ('
		),
		'428'=>array(
			'code'				=>'428',
			'title'			    =>'428 Precondition Required',
			'content_type'		=>'The origin server requires the request to be conditional. Intended to prevent "the \'lost update\' problem, where a client GETs a resource\'s state, modifies it, and PUTs it back to the server, when meanwhile a third party has modified the state on the server, leading to a conflict.'
		),
		'429'=>array(
			'code'				=>'429',
			'title'			    =>'429 Too Many Requests',
			'content_type'		=>'The user has sent too many requests in a given amount of time ("rate limiting").'
		),
		'431'=>array(
			'code'				=>'431',
			'title'			    =>'431 Request Header Fields Too Large',
			'content_type'		=>'The server is unwilling to process the request because its header fields are too large. The request MAY be resubmitted after reducing the size of the request header fields.'
		),
		'451'=>array(
			'code'				=>'451',
			'title'			    =>'451 Unavailable For Legal Reasons',
			'content_type'		=>'The user requests an illegal resource, such as a web page censored by a government.'
		),
		'500'=>array(
			'code'				=>'500',
			'title'			    =>'500 Internal Server Error',
			'content_type'		=>'The server has encountered a situation it doesn\'t know how to handle.'
		),
		'501'=>array(
			'code'				=>'501',
			'title'			    =>'501 Not Implemented',
			'content_type'		=>'The request method is not supported by the server and cannot be handled. The only methods that servers are required to support (and therefore that must not return this code) are '
		),
		'502'=>array(
			'code'				=>'502',
			'title'			    =>'502 Bad Nettopway',
			'content_type'		=>'This error response means that the server, while working as a nettopway to get a response needed to handle the request, got an invalid response.'
		),
		'503'=>array(
			'code'				=>'503',
			'title'			    =>'503 Service Unavailable',
			'content_type'		=>'The server is not ready to handle the request. Common causes are a server that is down for maintenance or that is overloaded. Note that together with this response, a user-friendly page explaining the problem should be sent. This responses should be used for temporary conditions and the '
		),
		'504'=>array(
			'code'				=>'504',
			'title'			    =>'504 Nettopway Timeout',
			'content_type'		=>'This error response is given when the server is acting as a nettopway and cannot get a response in time.'
		),
		'505'=>array(
			'code'				=>'505',
			'title'			    =>'505 HTTP Version Not Supported',
			'content_type'		=>'The HTTP version used in the request is not supported by the server.'
		),
		'506'=>array(
			'code'				=>'506',
			'title'			    =>'506 Variant Also Negotiates',
			'content_type'		=>'The server has an internal configuration error: transparent content negotiation for the request results in a circular reference.'
		),
		'507'=>array(
			'code'				=>'507',
			'title'			    =>'507 Variant Also Negotiates',
			'content_type'		=>'The server has an internal configuration error: the chosen variant resource is configured to engage in transparent content negotiation itself, and is therefore not a proper end point in the negotiation process.'
		),
		'511'=>array(
			'code'				=>'511',
			'title'			    =>'511 Network Authentication Required',
			'content_type'		=>'The 511 status code indicates that the client needs to authenticate to gain network access.'
		)
    );

    /**
     * @param int|NULL $code
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
	public  static function init(int $code=NULL){

//		if(empty($code)||(!DeviceType::$is_mobile&&Response::$type!='page'))
//			$code=200;

//		if(
//		    empty($code)
//            ||(
//                  DeviceType::$is_mobile
//                &&Response::$type!='page'
//            )
//        )
        $code=200;

		if(empty(self::$code_list[$code])){

			$error=array(
				'title'		=>'Parameters problem',
				'info'		=>'Header is not exists in code list'
			);

			throw new ParametersException($error);

		}

		header('HTTP/1.0 '.self::$code_list[$code]['title']);

		return true;

	}

}