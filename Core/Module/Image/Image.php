<?php

namespace Core\Module\Image;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\DbParametersException;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\FileException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\User\User;

class Image{

    /**
     * @param int|NULL $image_ID
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_image_ID(int $image_ID=NULL){

        if(empty($image_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($image_ID,'_image',0);

    }

    /**
     * @param array $image_ID_list
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_image_ID_list(array $image_ID_list=[]){

        if(count($image_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Image ID list is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID_list($image_ID_list,'_image');

    }

    /**
     * @param array $image_list
     * @return array
     */
    public  static function get_image_ID_from_image_list(array $image_list=[]){

        if(count($image_list)==0)
            return[];

        $list=[];

        foreach($image_list as $row)
            if(!empty($row['image_ID']))
                $list[]=$row['image_ID'];

        return $list;

    }

    /**
     * @param array $image_ID_list
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_list(array $image_ID_list=[]){

        if(count($image_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Image ID list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'    =>array(
                'id'
            ),
            'table'     =>'_image',
            'where'     =>array(
                'id'        =>$image_ID_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['id'];

        return $list;

    }

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $file_extension
     * @param string|NULL $date_create
     * @param string|NULL $prefix
     * @return string|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_file_path(int $image_ID=NULL,string $file_extension=NULL,string $date_create=NULL,string $prefix=NULL){

        $error_info_list=[];

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($file_extension))
            $error_info_list[]='File type is empty';

        if(empty($date_create))
            $error_info_list[]='Date create is empty';

        if(ImageCash::isset_image_parameter_in_cash($image_ID,'file_path'))
            return ImageCash::get_image_parameter_in_cash($image_ID,'file_path');

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'image_ID'      =>$image_ID,
                    'file_extension'     =>$file_extension,
                    'date_create'   =>$date_create
                )
            );

            throw new ParametersException($error);

        }

        $date_path      =Date::get_date_path($date_create,DirConfig::$dir_image);
        $file_path      =$date_path.'/'.$image_ID.'/'.$image_ID.(empty($prefix)?'':('_'.$prefix)).'.'.$file_extension;

        ImageCash::add_image_parameter_in_cash($image_ID,$file_path);

        return $file_path;

    }

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $file_extension
     * @param string|NULL $date_create
     * @return string|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_dir(int $image_ID=NULL,string $file_extension=NULL,string $date_create=NULL){

        $error_info_list=[];

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($file_extension))
            $error_info_list[]='File type is empty';

        if(empty($date_create))
            $error_info_list[]='Date create is empty';

        if(ImageCash::isset_image_parameter_in_cash($image_ID,'image_dir'))
            return ImageCash::get_image_parameter_in_cash($image_ID,'image_dir');

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'image_ID'      =>$image_ID,
                    'file_extension'     =>$file_extension,
                    'date_create'   =>$date_create
                )
            );

            throw new ParametersException($error);

        }

        $date_path      =Date::get_date_path($date_create,DirConfig::$dir_image);
        $file_path      =$date_path.'/'.$image_ID;

        ImageCash::add_image_parameter_in_cash($image_ID,$file_path);

        return $file_path;

    }

    /**
     * @param int|NULL $image_ID
     * @return int|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_size(int $image_ID=NULL){

        if(empty($image_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        if(ImageCash::isset_image_parameter_in_cash($image_ID,'file_size'))
            return ImageCash::get_image_parameter_in_cash($image_ID,'file_size');

        $r=Db::get_data_from_ID($image_ID,'_image',array('file_size'),0);

        if(count($r)==0)
            return NULL;

        $file_size=empty($r[0]['file_size'])?NULL:$r[0]['file_size'];

        ImageCash::add_image_parameter_in_cash($image_ID,'file_size',$file_size);

        return $file_size;

    }

    /**
     * @param int|NULL $image_ID
     * @return int|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_date_create(int $image_ID=NULL){

        if(empty($image_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $r=Db::get_data_from_ID($image_ID,'_image',array('date_create'),0);

        if(count($r)==0)
            return NULL;

        $date_create=empty($r[0]['date_create'])?NULL:$r[0]['date_create'];

        ImageCash::add_image_parameter_in_cash($image_ID,'date_create',$date_create);

        return $date_create;

    }

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $prefix
     * @return null|string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_file_path_from_image_ID(int $image_ID=NULL,string $prefix=NULL){

        if(empty($image_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        if(ImageCash::isset_image_parameter_in_cash($image_ID,'file_path'))
            return ImageCash::get_image_parameter_in_cash($image_ID,'file_path');

        $column_list    =[];
        $file_extension      =NULL;
        $date_create    =NULL;

        if(ImageCash::isset_image_parameter_in_cash($image_ID,'file_extension'))
            $file_extension=ImageCash::get_image_parameter_in_cash($image_ID,'file_extension');
        else
            $column_list[]='file_extension';

        if(ImageCash::isset_image_parameter_in_cash($image_ID,'date_create'))
            $date_create=ImageCash::get_image_parameter_in_cash($image_ID,'date_create');
        else
            $column_list[]='date_create';

        if(count($column_list)>0){

            $r=Db::get_data_from_ID($image_ID,'_image',$column_list,0);

            if(!empty($r['file_extension'])){

                $file_extension=$r['file_extension'];

                ImageCash::add_image_parameter_in_cash($image_ID,'file_extension',$file_extension);

            }

            if(!empty($r['date_create'])){

                $date_create=$r['date_create'];

                ImageCash::add_image_parameter_in_cash($image_ID,'date_create',$date_create);

            }

        }

        if(
              empty($date_create)
            ||empty($file_extension)
        )
            return NULL;
        else{

            $image_file_path=self::get_image_file_path($image_ID,$file_extension,$date_create,$prefix);

            if(!file_exists($image_file_path)){

                $error=array(
                    'title'=>'File path problem',
                    'info'=>'image file is not exists',
                    'data'=>array(
                        'image_ID'      =>$image_ID,
                        'file_path'     =>$image_file_path
                    )
                );

                throw new PathException($error);

            }

            ImageCash::add_image_parameter_in_cash($image_ID,'file_path',$image_file_path);

            return $image_file_path;

        }

    }

    /**
     * @param int|NULL $image_ID
     * @return null|string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_dir_from_image_ID(int $image_ID=NULL){

        if(empty($image_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        if(ImageCash::isset_image_parameter_in_cash($image_ID,'image_dir'))
            return ImageCash::get_image_parameter_in_cash($image_ID,'image_dir');

        $column_list        =[];
        $file_extension     =NULL;
        $date_create        =NULL;

        if(ImageCash::isset_image_parameter_in_cash($image_ID,'file_extension'))
            $file_extension=ImageCash::get_image_parameter_in_cash($image_ID,'file_extension');
        else
            $column_list[]='file_extension';

        if(ImageCash::isset_image_parameter_in_cash($image_ID,'date_create'))
            $date_create=ImageCash::get_image_parameter_in_cash($image_ID,'date_create');
        else
            $column_list[]='date_create';

        if(count($column_list)>0){

            $r=Db::get_data_from_ID($image_ID,'_image',$column_list,0);

            if(!empty($r['file_extension'])){

                $file_extension=$r['file_extension'];

                ImageCash::add_image_parameter_in_cash($image_ID,'file_extension',$file_extension);

            }

            if(!empty($r['date_create'])){

                $date_create=$r['date_create'];

                ImageCash::add_image_parameter_in_cash($image_ID,'date_create',$date_create);

            }

        }

        if(
              empty($date_create)
            ||empty($file_extension)
        )
            return NULL;
        else{

            $image_file_path    =self::get_image_dir($image_ID,$file_extension,$date_create);
            $image_file_path    =Dir::get_global_dir($image_file_path);

            if(!file_exists($image_file_path)){

                $error=array(
                    'title'=>'File path problem',
                    'info'=>'Image dir is not exists',
                    'data'=>array(
                        'image_ID'      =>$image_ID,
                        'file_path'     =>$image_file_path
                    )
                );

                throw new PathException($error);

            }

            ImageCash::add_image_parameter_in_cash($image_ID,'image_dir',$image_file_path);

            return $image_file_path;

        }

    }

    /**
     * @param array $image_ID_list
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_dir_list(array $image_ID_list=[]){

        if(count($image_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Image ID list is empty'
            );

            throw new ParametersException($error);

        }
        
        $q=array(
            'select'=>array(
                'id',
                'file_extension',
                'date_create'
            ),
            'table'=>'_image',
            'where'=>array(
                'id'        =>$image_ID_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];
        
        $list=[];
        
        foreach($r as $row){

            $image_file_path=self::get_image_dir($row['id'],$row['file_extension'],$row['date_create']);

            if(file_exists($image_file_path)){

                $list[$row['id']]=$image_file_path;

                ImageCash::add_image_parameter_in_cash($row['id'],'image_dir',$image_file_path);

            }
            
        }

        return $list;

    }

    /**
     * @param array $image_ID_list
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_file_path_from_image_ID_list(array $image_ID_list=[]){

        if(count($image_ID_list)==0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'image ID list is empty'
            );

            throw new ParametersException($error);

        }

        $file_extension              =NULL;
        $file_path              =NULL;
        $date_create            =NULL;
        $image_list             =[];
        $select_image_ID_list   =[];

        foreach($image_ID_list as $image_ID)
            if(!isset($image_list[$image_ID])){

                if(ImageCash::isset_image_parameter_in_cash($image_ID,'file_path'))
                    $image_list[$image_ID]=ImageCash::get_image_parameter_in_cash($image_ID,'file_path');
                else{

                    if(ImageCash::isset_image_parameter_in_cash($image_ID,'file_extension'))
                        $file_extension=ImageCash::get_image_parameter_in_cash($image_ID,'file_extension');

                    if(ImageCash::isset_image_parameter_in_cash($image_ID,'date_create'))
                        $date_create=ImageCash::get_image_parameter_in_cash($image_ID,'date_create');

                    if(
                          empty($file_extension)
                        ||empty($date_create)
                    )
                        $select_image_ID_list[]=$image_ID;
                    else
                        $image_list[$image_ID]=self::get_image_file_path($image_ID,$file_extension,$date_create);

                }

            }

        if(count($select_image_ID_list)>0){

            $select_list=array(
                'id',
                'file_extension',
                'date_create'
            );

            $r=Db::get_data_from_ID_list($select_image_ID_list,'_image',$select_list,0);

            foreach($r as $row){

                $image_ID               =$row['id'];
                $file_extension              =$row['file_extension'];
                $date_create            =$row['date_create'];
                $file_path              =self::get_image_file_path($image_ID,$file_extension,$date_create);
                $image_list[$image_ID]  =$file_path;

                ImageCash::add_image_parameter_in_cash($image_ID,'file_extension',$file_extension);
                ImageCash::add_image_parameter_in_cash($image_ID,'date_create',$date_create);
                ImageCash::add_image_parameter_in_cash($image_ID,'file_path',$file_path);

            }

        }

        return $image_list;

    }

    /**
     * @param string|NULL $file_path
     * @return array|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function get_image_pixel_size_from_file_path(string $file_path=NULL){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='Image path is empty';

        if(!file_exists($file_path))
            $error_info_list[]='Image path is not exists';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        if(ImagePathCash::isset_image_parameter_in_cash($file_path,'pixel_size'))
            return ImagePathCash::get_image_parameter_in_cash($file_path,'pixel_size');

        if(!file_exists($file_path)){

            $error=array(
                'title'     =>'Path problem',
                'info'      =>'File is not exists',
                'data'      =>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $image_object=self::get_image_object_from_file_path($file_path);

        if(empty($image_object)){

            $error=array(
                'title'     =>'File problem',
                'info'      =>'Prepare file fail',
                'data'      =>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $data=self::get_image_pixel_size_from_image_object($image_object);

        ImagePathCash::add_image_parameter_in_cash($file_path,'pixel_size',$data);

        unset($image_object);

        return $data;

    }

    /**
     * @param null $image_object
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_pixel_size_from_image_object($image_object=NULL){

        if(empty($image_object)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image object is empty'
            );

            throw new ParametersException($error);

        }

        $width      =imagesx($image_object);
        $height     =imagesy($image_object);
        $data       =array(
            'width'     =>$width,
            'height'    =>$height
        );

        return $data;

    }

    /**
     * @param string|NULL $file_path
     * @return resource
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function get_image_object_from_file_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Image path is empty'
            );

            throw new ParametersException($error);

        }

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'File path problem',
                'info'  =>'Image path is empty'
            );

            throw new PathException($error);

        }

//        echo 'start exif image type'."\n";
//        echo exif_imagetype($file_path)."\n";

//        file_put_contents(GetSourceParsingModelZoneLocalToLocalApi::$file_log_path,"\n exif type -> ".exif_imagetype($file_path)."\n",FILE_APPEND);

        switch(exif_imagetype($file_path)){

            case 1:
                return @imagecreatefromgif($file_path);

            case 2:
                return @imagecreatefromjpeg($file_path);

            case 3:{

                $image_object   =@imagecreatefrompng($file_path);
                $width          =@imagesx($image_object);
                $height         =@imagesy($image_object);
                $image          =@imagecreatetruecolor($width,$height);

                imagealphablending($image,false);
                imagesavealpha($image,true);

                imagecopyresampled($image,$image_object,0,0,0,0,$width,$height,$width,$height);

                return $image;

            }

//            case 4:{
//
//                // IMAGETYPE_SWF
//
//                break;
//
//            }
//
//            case 5:{
//
//                // IMAGETYPE_PSD
//
//                break;
//
//            }
//
//            case 6:{
//
//                // IMAGETYPE_BMP
//
//                break;
//
//            }
//
            case 7:
            case 8:{

                Dir::create_dir(DirConfig::$dir_image_temp);
                Dir::create_dir(DirConfig::$dir_image_tiff_temp);

                $image_temp_path=DirConfig::$dir_image_tiff_temp.'/'.Hash::get_random_hash().'.jpg';

                $image=new \Imagick($file_path);

                $image->setImageFormat('jpeg');
                $image->writeImage($image_temp_path);

                $image_object=@imagecreatefromjpeg($image_temp_path);

                unlink($image_temp_path);

                return $image_object;

            }
//
//            case 9:{
//
//                // IMAGETYPE_JPC
//
//                break;
//
//            }
//
//            case 10:{
//
//                // IMAGETYPE_JP2
//
//                break;
//
//            }
//
//            case 11:{
//
//                // IMAGETYPE_JPX
//
//                break;
//
//            }
//
//            case 12:{
//
//                // IMAGETYPE_JB2
//
//                break;
//
//            }
//
//            case 13:{
//
//                // IMAGETYPE_SWC
//
//                break;
//
//            }
//
//            case 14:{
//
//                // IMAGETYPE_IFF
//
//                break;
//
//            }
//
//            case 15:{
//
//                // IMAGETYPE_WBMP
//
//                break;
//
//            }
//
//            case 16:{
//
//                // IMAGETYPE_XBM
//
//                break;
//
//            }
//
//            case 17:{
//
//                // IMAGETYPE_ICO
//
//                break;
//
//            }
//
//            case 18:
//                return imagecreatefromwebp($file_path);

            default:{

                $error=array(
                    'title' =>'File type problem',
                    'info'  =>'File exif image type is not valid',
                    'data'  =>array(
                        'image_path'        =>$file_path,
                        'exif_image_type'   =>exif_imagetype($file_path)
                    )
                );

                throw new FileException($error);

                break;

            }

        }

    }

    /**
     * @param string|NULL $file_path
     * @return null|string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_type_from_file_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Image path is empty'
            );

            throw new ParametersException($error);

        }

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'File path problem',
                'info'  =>'Image path is empty'
            );

            throw new PathException($error);

        }

        switch(exif_imagetype($file_path)){

            case 1:
                return'gif';

            case 2:
                return'jpg';

            case 3:
                return'png';

//            case 4:{
//
//                // IMAGETYPE_SWF
//
//                break;
//
//            }
//
//            case 5:{
//
//                // IMAGETYPE_PSD
//
//                break;
//
//            }
//
//            case 6:{
//
//                // IMAGETYPE_BMP
//
//                break;
//
//            }
//
            case 7:
            case 8:
                return'tiff';
//
//            case 9:{
//
//                // IMAGETYPE_JPC
//
//                break;
//
//            }
//
//            case 10:{
//
//                // IMAGETYPE_JP2
//
//                break;
//
//            }
//
//            case 11:{
//
//                // IMAGETYPE_JPX
//
//                break;
//
//            }
//
//            case 12:{
//
//                // IMAGETYPE_JB2
//
//                break;
//
//            }
//
//            case 13:{
//
//                // IMAGETYPE_SWC
//
//                break;
//
//            }
//
//            case 14:{
//
//                // IMAGETYPE_IFF
//
//                break;
//
//            }
//
//            case 15:{
//
//                // IMAGETYPE_WBMP
//
//                break;
//
//            }
//
//            case 16:{
//
//                // IMAGETYPE_XBM
//
//                break;
//
//            }
//
//            case 17:{
//
//                // IMAGETYPE_ICO
//
//                break;
//
//            }
//
            case 18:
                return'webp';

            default:
                return NULL;

        }

    }

    /**
     * @param array $image_ID_list
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_image_date_update(array $image_ID_list=[]){

        if(count($image_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_image',
            'set'       =>array(
                'date_update'=>'NOW()'
            ),
            'where'     =>array(
                'id'        =>$image_ID_list,
                'type'      =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Image list date update was not update',
                'data'  =>array(
                    'image_ID_list'     =>$image_ID_list,
                    'query'             =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $image_ID
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_image_ID_date_update(int $image_ID=NULL){

        if(empty($image_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_image',
            'set'       =>array(
                'date_update'=>'NOW()'
            ),
            'where'     =>array(
                'id'        =>$image_ID,
                'type'      =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Image list date update was not update',
                'data'  =>array(
                    'image_ID'      =>$image_ID,
                    'query'         =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $hash
     * @param int|NULL $width
     * @param int|NULL $height
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param string|NULL $source_link
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_image_prepared(int $image_ID=NULL,string $hash=NULL,int $width=NULL,int $height=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL,string $source_link=NULL){

        if(empty($image_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'image_ID si empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_image',
            'set'=>array(
                'hash'                  =>$hash,
                'width'                 =>$width,
                'height'                =>$height,
                'file_size'             =>$file_size,
                'file_mime_type'        =>$file_mime_type,
                'file_extension'        =>$file_extension,
                'preparing'             =>0,
                'prepared'              =>1,
                'public'                =>1,
                'hide'                  =>0,
                'date_update'           =>'NOW()',
                'type'                  =>0
            ),
            'where'=>array(
                'id'    =>$image_ID,
                'type'  =>0
            )
        );

        if(!empty($source_link))
            $q['set']['source_link']=$source_link;

        if(!Db::update($q)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'image ID is empty'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $image_type
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param bool $preparing
     * @param bool $prepared
     * @param bool $public
     * @param bool $hide
     * @param bool $need_return_date_create
     * @param string|NULL $source_link
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_image(int $file_ID=NULL,string $image_type=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL,bool $preparing=false,bool $prepared=false,bool $public=false,bool $hide=false,bool $need_return_date_create=false,string $source_link=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($image_type))
            $error_info_list[]='Image type is empty';;

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_image',
            'values'=>array(
                'user_id'           =>User::$user_ID,
                'file_id'           =>$file_ID,
                'image_type'        =>$image_type,
                'file_size'         =>$file_size,
                'file_mime_type'    =>$file_mime_type,
                'file_extension'    =>$file_extension,
                'source_link'       =>empty($source_link)?NULL:$source_link,
                'preparing'         =>(int)$preparing,
                'prepared'          =>(int)$prepared,
                'public'            =>(int)$public,
                'hide'              =>(int)$hide,
                'date_create'       =>'NOW()',
                'date_update'       =>'NOW()',
                'type'              =>0
            )
        );

        $return_list=[];

        if($need_return_date_create)
            $return_list[]='date_create';

        $r=Db::insert($q,true,$return_list);

        if(count($r)==0){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'image was not added'
            );

            throw new DbQueryException($error);

        }

        if(count($return_list)==0)
            return $r[0]['id'];
        else
            return array(
                'ID'            =>$r[0]['id'],
                'date_create'   =>$r[0]['date_create']
            );

    }

    /**
     * @param int|NULL $file_ID
     * @param int|NULL $image_ID
     * @param string|NULL $hash
     * @param int|NULL $width
     * @param int|NULL $height
     * @param string|NULL $image_type
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param bool $preparing
     * @param bool $prepared
     * @param bool $public
     * @param bool $hide
     * @param bool $need_return_date_create
     * @param string|NULL $source_link
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_image_full(int $file_ID=NULL,int $image_ID=NULL,string $hash=NULL,int $width=NULL,int $height=NULL,string $image_type=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL,bool $preparing=false,bool $prepared=false,bool $public=false,bool $hide=false,bool $need_return_date_create=false,string $source_link=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($image_type))
            $error_info_list[]='Image type is empty';;

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_image',
            'values'=>array(
                'user_id'           =>User::$user_ID,
                'file_id'           =>$file_ID,
                'image_id'          =>$image_ID,
                'hash'              =>$hash,
                'width'             =>$width,
                'height'            =>$height,
                'image_type'        =>$image_type,
                'file_size'         =>$file_size,
                'file_mime_type'    =>$file_mime_type,
                'file_extension'    =>$file_extension,
                'source_link'       =>empty($source_link)?NULL:$source_link,
                'preparing'         =>(int)$preparing,
                'prepared'          =>(int)$prepared,
                'public'            =>(int)$public,
                'hide'              =>(int)$hide,
                'date_create'       =>'NOW()',
                'date_update'       =>'NOW()',
                'type'              =>0
            )
        );

        $return_list=[];

        if($need_return_date_create)
            $return_list[]='date_create';

        $r=Db::insert($q,true,$return_list);

        if(count($r)==0){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'image was not added'
            );

            throw new DbQueryException($error);

        }

        if(count($return_list)==0)
            return $r[0]['id'];
        else
            return array(
                'ID'            =>$r[0]['id'],
                'date_create'   =>$r[0]['date_create']
            );

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $image_type
     * @param bool $need_return_date_create
     * @param string|NULL $source_link
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_image_preparing(int $file_ID=NULL,string $image_type=NULL,bool $need_return_date_create=false,string $source_link=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($image_type))
            $error_info_list[]='Image type is empty';;

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        return self::add_image($file_ID,$image_type,NULL,NULL,NULL,true,false,false,true,$need_return_date_create,$source_link);

    }

    /**
     * @param int|NULL $image_ID
     * @param bool $is_need_remove_image_item
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_image_ID(int $image_ID=NULL,bool $is_need_remove_image_item=true){

        if(empty($image_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        if($is_need_remove_image_item)
            ImageItem::remove_image_item($image_ID);

        if(!Db::delete_from_ID($image_ID,'_image',0)){

            $error=array(
                'title' =>'Db query problem',
                'info'  =>'Image ID was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param array $image_ID_list
     * @param bool $is_need_remove_image_item
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_image_ID_list(array $image_ID_list=[],bool $is_need_remove_image_item=true){

        if(count($image_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Image ID list is empty'
            );

            throw new ParametersException($error);

        }

        $ID_list=[];

        foreach($image_ID_list as $image_ID)
            if(!empty($image_ID))
                $ID_list[]=$image_ID;

        if(count($ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Image ID list is empty'
            );

            throw new ParametersException($error);

        }

        if($is_need_remove_image_item)
            ImageItem::remove_image_item_list($ID_list);

        if(!Db::delete_from_ID_list($ID_list,'_image',0)){

            $error=array(
                'title' =>'Db query problem',
                'info'  =>'Image ID was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

}