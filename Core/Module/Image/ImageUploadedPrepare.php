<?php

namespace Core\Module\Image;

use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Encrypt\Hash;
use Core\Module\Encrypt\HashFile;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Exception\PhpException;
use Core\Module\File\File;
use Core\Module\File\FileType;
use Project\Whore\Admin\Action\Image\ImagePathAction;

class ImageUploadedPrepare{

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $image_ID;

    /** @var string */
    private static $image_date_create;

    /** @var array */
    private static $image_item_ID_list=[];

    /** @var string */
    private static $file_extension;

    /** @var string */
    private static $file_content_type;

    /** @var string */
    private static $source_image_link;

    /** bool */
    private static $is_server_link=true;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$file_ID                  =NULL;
        self::$image_ID                 =NULL;
        self::$image_date_create        =NULL;
        self::$image_item_ID_list       =[];
        self::$file_extension           =NULL;
        self::$file_content_type        =NULL;
        self::$source_image_link        =NULL;
        self::$is_server_link           =true;

        return true;

    }

    /**
     * @return string
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_file_source_path(){

        return File::get_file_path_from_file_ID(self::$file_ID);

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_convert_type(){

        if(!isset(ImageConfig::$image_convert_type_list[self::$file_content_type])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'File type is not exists'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function prepare_image(){

        $image_data=Image::add_image_preparing(self::$file_ID,self::$file_extension,true,self::$source_image_link);

        if(empty($image_data)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Image was not added'
            );

            throw new DbQueryException($error);

        }

        self::$image_ID             =$image_data['ID'];
        self::$image_date_create    =$image_data['date_create'];

        $file_result_path           =self::get_file_source_path();
        $dir_date_image_result      =Dir::get_dir_from_date(DirConfig::$dir_image,self::$image_date_create);

        $dir_image_result           =$dir_date_image_result.'/'.self::$image_ID;
        $image_source_path          =$dir_image_result.'/source.'.self::$file_extension;

        Image::get_image_date_create(self::$image_ID);

        Dir::create_dir($dir_image_result);

        Image::get_image_object_from_file_path($file_result_path);

        $image_object=ImageConvert::get_image_object_with_exif_orientation($file_result_path);

        if(!ImageConvert::save_image($image_object,$image_source_path,self::$file_extension,75)){

            $error=array(
                'title' =>'File problem',
                'info'  =>'File was not copied'
            );

            throw new PathException($error);

        }

        $file_image_hash        =NULL;
        $image_item_width       =NULL;
        $image_item_height      =NULL;
        $file_image_size        =NULL;
        $file_image_mime_type   =NULL;
        $file_image_type        =NULL;

        switch(self::$file_content_type){

            case'logo':
            case'image_ico':{

                $file_extension='png';

                break;

            }

            default:{

                $file_extension='jpg';

                break;

            }

        }

        $image_size=Image::get_image_pixel_size_from_file_path($image_source_path);

        foreach(ImageConfig::$image_convert_type_list[self::$file_content_type] as $resolution_type=>$image_size_row){

            $image_item_ID=ImageItem::add_image_item_preparing(self::$file_ID,self::$image_ID,$resolution_type,self::$file_extension);

            if(isset($image_size_row['width'])&&isset($image_size_row['height'])){

                $image_size_list=ImageConvert::get_optimal_image_crop_size($image_size['width'],$image_size['height'],NULL,$image_size_row['height']);

                if($image_size_list['width']<$image_size_row['width'])
                    $image_resize_object=ImageConvert::resize_image_from_file_path($image_source_path,$image_size_row['width'],NULL);
                else
                    $image_resize_object=ImageConvert::resize_image_from_file_path($image_source_path,NULL,$image_size_row['height']);

                $image_crop_object=ImageConvert::crop_image_from_image_object($image_resize_object,NULL,NULL,$image_size_row['width'],$image_size_row['height'],$image_size_row['width'],$image_size_row['height']);

            }
            else if(isset($image_size_row['width']))
                $image_crop_object=ImageConvert::resize_image_from_file_path($image_source_path,$image_size_row['width'],NULL);
            else if(isset($image_size_row['height']))
                $image_crop_object=ImageConvert::resize_image_from_file_path($image_source_path,NULL,$image_size_row['height']);
            else
                $image_crop_object=Image::get_image_object_from_file_path($image_source_path);

//            self::$image_item_ID_list[$resolution_type]=$image_item_ID;
            self::$image_item_ID_list[$resolution_type]=$image_item_ID.'.'.$file_extension;

            $image_item_path=$dir_image_result.'/'.$image_item_ID.'.'.$file_extension;

            ImageConvert::save_image($image_crop_object,$image_item_path,$file_extension);

            $file_image_size                =filesize($image_item_path);
            $file_image_mime_type           =FileType::get_file_mime_type($image_item_path);
            $file_image_type                =FileType::get_file_extension($image_item_path);
            $image_size_data                =Image::get_image_pixel_size_from_file_path($image_item_path);
            $file_image_hash                =HashFile::get_sha1_encode($image_item_path);
            $file_image_item_hash_link      =Hash::get_sha1_encode($image_item_path);
            $image_item_width               =$image_size_data['width'];
            $image_item_height              =$image_size_data['height'];

            if(!ImageItem::update_image_item_prepared($image_item_ID,$file_image_hash,$file_image_item_hash_link,$image_item_width,$image_item_height,$file_image_size,$file_image_mime_type,$file_image_type)){

                $error=array(
                    'title'     =>'System problem',
                    'info'      =>'Image item was not update to prepared'
                );

                throw new PhpException($error);

            }

        }

        if(!empty($file_image_hash))
            if(!Image::update_image_prepared(self::$image_ID,$file_image_hash,$image_item_width,$image_item_height,$file_image_size,$file_image_mime_type,$file_image_type)){

                $error=array(
                    'title'     =>'System problem',
                    'info'      =>'Image item was not update to prepared'
                );

                throw new PhpException($error);

            }

        unset($image_object);
        unset($image_resize_object);
        unset($image_crop_object);

        if(self::$is_server_link)
            return array(
                'image_ID'              =>self::$image_ID,
                'file_ID'               =>self::$file_ID,
                'image_item_ID_list'    =>self::$image_item_ID_list,
                'image_dir'             =>$dir_image_result
            );

        $data=ImagePathAction::get_image_path_data(self::$image_ID,true,true,self::$is_server_link);

        return $data;

    }

    /**
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        if(self::isset_convert_type())
            return self::prepare_image();

        return NULL;

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @param string|NULL $image_link
     * @param bool $is_server_link
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(int $file_ID=NULL,string $file_content_type=NULL,string $file_extension=NULL,string $image_link=NULL,bool $is_server_link=true){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list['file_ID']='File ID is empty';

        if(empty($file_content_type))
            $error_info_list['file_content_type']='File content type is empty';

        if(empty($file_extension))
            $error_info_list['file_extension']='File extension is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$file_ID              =$file_ID;
        self::$file_content_type    =$file_content_type;
        self::$file_extension       =mb_strtolower($file_extension,'utf-8');
        self::$source_image_link    =empty($image_link)?NULL:$image_link;
        self::$is_server_link       =$is_server_link;

        return self::set();

    }

}