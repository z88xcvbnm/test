<?php

namespace Core\Module\Image;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;

class ImageItem{

    /** @var string */
    public  static $table_name='_image_item';

    /**
     * @param array $image_ID_list
     * @param string $resolution
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_item_hash_link_list(array $image_ID_list=[],string $resolution='small'){

        if(count($image_ID_list)==0)
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'image_id',
                'image_type',
                'date_create',
                'hash_link'
            ],
            'where'     =>[
                'image_id'          =>$image_ID_list,
                'resolution_type'   =>$resolution,
                'type'              =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['image_id']]=[
                'ID'            =>$row['id'],
                'image_ID'      =>$row['image_id'],
                'image_type'    =>$row['image_type'],
                'date_create'   =>$row['date_create'],
                'hash_link'     =>$row['hash_link']
            ];

        return $list;

    }

    /**
     * @param array $image_ID_list
     * @param array $resolution_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_item_hash_link_resolution_list(array $image_ID_list=[],array $resolution_list=[]){

        if(count($image_ID_list)==0)
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'image_id',
                'image_type',
                'resolution_type',
                'date_create',
                'hash_link',
                'width',
                'height',
                'file_size',
                'file_extension'
            ],
            'where'     =>[
                'image_id'          =>$image_ID_list,
                'type'              =>0
            ]
        ];

        if(count($resolution_list)>0)
            $q['where']['resolution_type']=$resolution_list;

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row){

            if(!isset($list[$row['image_id']]))
                $list[$row['image_id']]=[];

            $list[$row['image_id']][$row['resolution_type']]=[
                'ID'                    =>$row['id'],
                'image_ID'              =>$row['image_id'],
                'resolution_type'       =>$row['resolution_type'],
                'image_type'            =>$row['image_type'],
                'date_create'           =>$row['date_create'],
                'width'                 =>$row['width'],
                'height'                =>$row['height'],
                'file_size'             =>$row['file_size'],
                'hash_link'             =>$row['hash_link'],
                'file_extension'        =>$row['file_extension']
            ];

        }

        return $list;

    }

    /**
     * @param int|NULL $image_item_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_item_data(int $image_item_ID=NULL){

        if(empty($image_item_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Image item ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'file_id',
                'image_id',
                'image_type',
                'date_create'
            ],
            'where'     =>[
                'id'        =>$image_item_ID,
                'type'      =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'file_ID'           =>$r[0]['file_id'],
            'image_ID'          =>$r[0]['image_id'],
            'image_type'        =>$r[0]['image_type'],
            'date_create'       =>$r[0]['date_create']
        ];

    }

    /**
     * @param array $image_ID_list
     * @param string|NULL $resolution_type
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_item_hash_list(array $image_ID_list=[],string $resolution_type=NULL){

        if(count($image_ID_list)==0)
            return[];

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'image_id',
                'hash'
            ],
            'where'     =>[
                'image_id'          =>$image_ID_list,
                'resolution_type'   =>$resolution_type,
                'type'              =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'image_ID'      =>$row['image_id'],
                'hash'          =>$row['hash']
            ];

        return $list;

    }

    /**
     * @param string|NULL $hash_link
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_item_data_from_hash_link(string $hash_link=NULL){

        if(empty($hash_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash link is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'user_id',
                'file_id',
                'image_id',
                'image_type',
                'resolution_type',
                'file_extension',
                'date_create'
            ],
            'where'     =>[
                'hash_link'     =>$hash_link,
                'type'          =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'ID'                    =>$r[0]['id'],
            'user_ID'               =>$r[0]['user_id'],
            'file_ID'               =>$r[0]['file_id'],
            'image_ID'              =>$r[0]['image_id'],
            'file_extension'        =>$r[0]['file_extension'],
            'resolution_type'       =>$r[0]['resolution_type'],
            'date_create'           =>$r[0]['date_create']
        ];

    }

    /**
     * @param array $file_ID_list
     * @param array $resolution_type_list
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_list_from_file_ID_list(array $file_ID_list=[],array $resolution_type_list=[]){

        $error_info_list=[];

        if(count($file_ID_list)==0)
            $error_info_list[]='File ID list is empty';

        if(count($resolution_type_list)==0)
            $error_info_list[]='Resolution type list is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'image_id'
            ],
            'where'=>[
                'file_id'           =>$file_ID_list,
                'resolution_type'   =>$resolution_type_list,
                'type'              =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return false;

        $list=[];

        foreach($r as $row)
            $list[]=$row['image_id'];

        return $list;

    }

    /**
     * @param array $image_ID_list
     * @param array $resolution_type_list
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_ID_list_from_image_ID_list(array $image_ID_list=[],array $resolution_type_list=[]){

        $error_info_list=[];

        if(count($image_ID_list)==0)
            $error_info_list[]='File ID list is empty';

        if(count($resolution_type_list)==0)
            $error_info_list[]='Resolution type list is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'image_id'
            ],
            'where'=>[
                'image_id'           =>$image_ID_list,
                'resolution_type'   =>$resolution_type_list,
                'type'              =>0
            ]
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return false;

        $list=[];

        foreach($r as $row)
            $list[]=$row['image_id'];

        return $list;

    }

    /**
     * @param int|NULL $image_ID
     * @param bool $is_assoc_resolution_type
     * @param bool $is_file_extension
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_item_ID_list(int $image_ID=NULL,bool $is_assoc_resolution_type=false,bool $is_file_extension=false){

        if(empty($image_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_image_item',
            'where'=>array(
                'image_id'  =>$image_ID,
                'type'      =>0
            )
        );

        if($is_assoc_resolution_type)
            $q['select'][]='resolution_type';

        if($is_file_extension)
            $q['select'][]='file_extension';

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        if($is_assoc_resolution_type)
            foreach($r as $row)
                $list[$row['resolution_type']]=$row['id'].($is_file_extension?('.'.$row['file_extension']):'');
        else
            foreach($r as $row)
                $list[]=$row['id'].($is_file_extension?('.'.$row['file_extension']):'');

        return $list;

    }

    /**
     * @param int|NULL $image_ID
     * @param bool $is_assoc_resolution_type
     * @param bool $is_file_extension
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_item_ID_list_width_size(int $image_ID=NULL,bool $is_assoc_resolution_type=false,bool $is_file_extension=false){

        if(empty($image_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'width',
                'height'
            ),
            'table'=>'_image_item',
            'where'=>array(
                'image_id'  =>$image_ID,
                'type'      =>0
            )
        );

        if($is_assoc_resolution_type)
            $q['select'][]='resolution_type';

        if($is_file_extension)
            $q['select'][]='file_extension';

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        if($is_assoc_resolution_type)
            foreach($r as $row)
                $list[$row['resolution_type']]=[
                    'ID'        =>$row['id'].($is_file_extension?('.'.$row['file_extension']):''),
                    'width'     =>$row['width'],
                    'height'    =>$row['height']
                ];
        else
            foreach($r as $row)
                $list[]=[
                    'ID'        =>$row['id'].($is_file_extension?('.'.$row['file_extension']):''),
                    'width'     =>$row['width'],
                    'height'    =>$row['height']
                ];

        return $list;

    }

    /**
     * @param array $image_ID_list
     * @param bool $is_assoc_resolution_type
     * @param bool $is_file_extension
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_item_ID_list_from_image_ID_list(array $image_ID_list=[],bool $is_assoc_resolution_type=false,bool $is_file_extension=false){

        if(count($image_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'image_id'
            ),
            'table'=>'_image_item',
            'where'=>array(
                'image_id'  =>$image_ID_list,
                'type'      =>0
            )
        );

        if($is_assoc_resolution_type)
            $q['select'][]='resolution_type';

        if($is_file_extension)
            $q['select'][]='file_extension';

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        if($is_assoc_resolution_type)
            foreach($r as $row){

                if(!isset($list[$row['image_id']]))
                    $list[$row['image_id']]=[];

                $list[$row['image_id']][$row['resolution_type']]=$row['id'].($is_file_extension?('.'.$row['file_extension']):'');

            }
        else
            foreach($r as $row){

                if(!isset($list[$row['image_id']]))
                    $list[$row['image_id']]=[];

                $list[$row['image_id']][]=$row['id'].($is_file_extension?('.'.$row['file_extension']):'');

            }

        return $list;

    }

    /**
     * @param array $image_ID_list
     * @param bool $is_assoc_resolution_type
     * @param bool $is_file_extension
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_item_ID_list_with_size_from_image_ID_list(array $image_ID_list=[],bool $is_assoc_resolution_type=false,bool $is_file_extension=false){

        if(count($image_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'image_id',
                'width',
                'height',
                'file_size'
            ),
            'table'=>'_image_item',
            'where'=>array(
                'image_id'  =>$image_ID_list,
                'type'      =>0
            )
        );

        if($is_assoc_resolution_type)
            $q['select'][]='resolution_type';

        if($is_file_extension)
            $q['select'][]='file_extension';

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        if($is_assoc_resolution_type)
            foreach($r as $row){

                if(!isset($list[$row['image_id']]))
                    $list[$row['image_id']]=[];

                $list[$row['image_id']][$row['resolution_type']]=[
                    'ID'        =>$row['id'].($is_file_extension?('.'.$row['file_extension']):''),
                    'width'     =>$row['width'],
                    'height'    =>$row['height'],
                    'file_size' =>$row['file_size']
                ];

            }
        else
            foreach($r as $row){

                if(!isset($list[$row['image_id']]))
                    $list[$row['image_id']]=[];

                $list[$row['image_id']][]=[
                    'ID'        =>$row['id'].($is_file_extension?('.'.$row['file_extension']):''),
                    'width'     =>$row['width'],
                    'height'    =>$row['height'],
                    'file_size' =>$row['file_size']
                ];

            }

        return $list;

    }

    /**
     * @param array $image_ID_list
     * @param bool $is_assoc
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_item_list(array $image_ID_list=[],bool $is_assoc=false){

        if(count($image_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $ID_list=[];

        foreach($image_ID_list as $image_ID)
            $ID_list[]=$image_ID;

        if(count($ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'image_id'
            ),
            'table'=>'_image_item',
            'where'=>array(
                'image_id'  =>$ID_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        if($is_assoc)
            foreach($r as $row){

                if(!isset($list[$row['image_id']]))
                    $list[$row['image_id']]=[];

                $list[$row['image_id']][]=$row['id'];

            }
        else
            foreach($r as $row)
                $list[]=$row['id'];

        return $list;

    }

    /**
     * @param array $image_ID_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_unical_image_ID_list(array $image_ID_list=[]){

        if(count($image_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'image_id',
                'hash'
            ),
            'table'=>'_image_item',
            'where'=>array(
                'image_id'  =>$image_ID_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $hash_list=[];

        foreach($r as $row)
            $hash_list[$row['hash']]=$row['image_id'];

        if(count($hash_list)==0)
            return[];

        return array_values($hash_list);

//        $q=array(
//            'select'=>array(
//                'hash'
//            ),
//            'table'=>'_image_item',
//            'where'=>array(
//                'hash'  =>array_keys($hash_list),
//                'type'  =>0
//            ),
//            'group'=>[
//                [
//                    'column'=>'hash'
//                ]
//            ]
//        );
//
//        $r=Db::select($q);
//
//        if(count($r)==0)
//            return[];
//
//        $list=[];
//
//        foreach($r as $row)
//            $list[]=$row['image_id'];
//
//        return $list;

    }

    /**
     * @param array $image_hash_list
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_duplicate_image_ID_list(array $image_hash_list=[]){

        if(count($image_hash_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'image_id'
            ),
            'table'=>'_image_item',
            'where'=>array(
                'hash'      =>$image_hash_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['image_id'];

        return $list;

    }

    /**
     * @param int|NULL $file_ID
     * @param int|NULL $image_ID
     * @param int|NULL $width
     * @param int|NULL $height
     * @param string|NULL $resolution_type
     * @param string|NULL $image_type
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param bool $preparing
     * @param bool $prepared
     * @param bool $public
     * @param bool $hide
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_image_item(int $file_ID=NULL,int $image_ID=NULL,int $width=NULL,int $height=NULL,string $resolution_type=NULL,string $image_type=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL,bool $preparing=false,bool $prepared=false,bool $public=false,bool $hide=false){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($image_ID))
            $error_info_list[]='image ID is empty';

        if(empty($resolution_type))
            $error_info_list[]='Resolution type is empty';

        if(empty($image_type))
            $error_info_list[]='Image type is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_image_item',
            'values'=>array(
                'user_id'               =>User::$user_ID,
                'file_id'               =>$file_ID,
                'image_id'              =>$image_ID,
                'width'                 =>$width,
                'height'                =>$height,
                'resolution_type'       =>$resolution_type,
                'image_type'            =>$image_type,
                'file_size'             =>$file_size,
                'file_mime_type'        =>$file_mime_type,
                'file_extension'        =>$file_extension,
                'preparing'             =>(int)$preparing,
                'prepared'              =>(int)$prepared,
                'public'                =>(int)$public,
                'hide'                  =>(int)$hide,
                'date_create'           =>'NOW()',
                'date_update'           =>'NOW()',
                'type'                  =>0
            )
        );

        $r=Db::insert($q,true);

        if(count($r)==0){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'image item was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $file_ID
     * @param int|NULL $image_ID
     * @param string|NULL $resolution_type
     * @param string|NULL $image_type
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_image_item_preparing(int $file_ID=NULL,int $image_ID=NULL,string $resolution_type=NULL,string $image_type=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($image_ID))
            $error_info_list[]='image ID is empty';

        if(empty($resolution_type))
            $error_info_list[]='Resolution type is empty';

        if(empty($image_type))
            $error_info_list[]='Image type is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        return self::add_image_item($file_ID,$image_ID,NULL,NULL,$resolution_type,$image_type,NULL,NULL,NULL,true,false,false,true);

    }

    /**
     * @param int|NULL $image_item_ID
     * @param string|NULL $hash
     * @param int|NULL $width
     * @param int|NULL $height
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_image_item_prepared(int $image_item_ID=NULL,string $hash=NULL,string $hash_link=NULL,int $width=NULL,int $height=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL){

        if(empty($image_item_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_image_item',
            'set'=>array(
                'hash'                  =>$hash,
                'hash_link'             =>$hash_link,
                'width'                 =>$width,
                'height'                =>$height,
                'file_size'             =>$file_size,
                'file_mime_type'        =>$file_mime_type,
                'file_extension'        =>$file_extension,
                'preparing'             =>0,
                'prepared'              =>1,
                'public'                =>1,
                'hide'                  =>0,
                'date_update'           =>'NOW()',
                'type'                  =>0
            ),
            'where'=>array(
                'id'    =>$image_item_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'image ID is empty'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $image_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_image_item(int $image_ID=NULL){

        if(empty($image_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'image_id'=>$image_ID
        );

        if(!Db::delete_from_where_list('_image_item',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Image item was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param array $image_item_ID_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_image_item_ID_list(array $image_item_ID_list=[]){

        if(count($image_item_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image item ID list is empty'
            );

            throw new ParametersException($error);

        }

        $ID_list=[];

        foreach($image_item_ID_list as $image_item_ID)
            if(!empty($image_item_ID))
                $ID_list[]=$image_item_ID;

        if(count($ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID list is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID_list($ID_list,'_image_item',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Image item was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param array $image_ID_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_image_item_list(array $image_ID_list=[]){

        if(count($image_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID list is empty'
            );

            throw new ParametersException($error);

        }

        $ID_list=[];

        foreach($image_ID_list as $image_ID)
            if(!empty($image_ID))
                $ID_list[]=$image_ID;

        if(count($ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image ID list is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'image_id'=>$ID_list
        );

        if(!Db::delete_from_where_list('_image_item',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Image item was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $image_item_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_image_item_ID(int $image_item_ID=NULL){

        if(empty($image_item_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Image item ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($image_item_ID,'_image_item',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Image item was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

}