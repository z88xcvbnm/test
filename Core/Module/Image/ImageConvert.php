<?php

namespace Core\Module\Image;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\File\File;

class ImageConvert{

    /**
     * @param string|NULL $file_path
     * @param int|NULL $width_result
     * @param int|NULL $height_result
     * @return resource
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function resize_image_from_file_path(string $file_path=NULL,int $width_result=NULL,int $height_result=NULL){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($width_result)&&empty($height_result))
            $error_info_list[]='Width & height is empty';

        if(!file_exists($file_path))
            $error_info_list[]='File is not exists';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $image_size_source      =Image::get_image_pixel_size_from_file_path($file_path);
        $width_source           =$image_size_source['width'];
        $height_source          =$image_size_source['height'];
        $image_object           =Image::get_image_object_from_file_path($file_path);

        if(empty($width_result))
            $width_result=floor($height_result*$width_source/$height_source);

        if(empty($height_result))
            $height_result=floor($width_result*$height_source/$width_source);

        if($width_source<$width_result)
            $width_result=$width_source;

        if($height_source<$height_result)
            $height_result=$height_source;

        return self::resize_image_from_image_object($image_object,$width_result,$height_result,$width_source,$height_source);

    }

    /**
     * @param string|NULL $file_path
     * @return string
     */
    public  static function get_base64_encode_image(string $file_path=NULL){

        $image_data         =base64_encode(file_get_contents($file_path));
        $result             =new \finfo();
        $file_mime_type     =$result->file($file_path,FILEINFO_MIME_TYPE);

        return'data:'.$file_mime_type.';base64,'.$image_data;

    }

    /**
     * @param int|NULL $width
     * @param int|NULL $height
     * @return resource
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_new_image_object(int $width=NULL,int $height=NULL){

        $error_info_list=[];

        if(empty($width))
            $error_info_list[]='Width is empty';
        else if($width<0)
            $error_info_list[]='Width not valid';

        if(empty($height))
            $error_info_list[]='Height is empty';
        else if($height<0)
            $error_info_list[]='Height not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $image=imagecreatetruecolor($width,$height);

        imagealphablending($image,false);
        imagesavealpha($image,true);

        return $image;

    }

    /**
     * @param null $image_source_object
     * @param int|NULL $width_result
     * @param int|NULL $height_result
     * @param int|NULL $width_source
     * @param int|NULL $height_source
     * @return resource
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function resize_image_from_image_object($image_source_object=NULL,int $width_result=NULL,int $height_result=NULL,int $width_source=NULL,int $height_source=NULL){

        $error_info_list=[];

        if(empty($image_source_object))
            $error_info_list[]='Image object is empty';

        if(empty($width_result)&&empty($height_result))
            $error_info_list[]='Width & height is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        if(empty($width_source)||empty($height_source)){

            $image_size_source      =Image::get_image_pixel_size_from_image_object($image_source_object);
            $width_source           =$image_size_source['width'];
            $height_source          =$image_size_source['height'];

        }

        if(empty($width_result))
            $width_result=floor($height_result*$width_source/$height_source);

        if(empty($height_result))
            $height_result=floor($width_result*$height_source/$width_source);

        if($width_source<$width_result)
            $width_result=$width_source;

        if($height_source<$height_result)
            $height_result=$height_source;

        $image_result_object=self::get_new_image_object($width_result,$height_result);

        imagecopyresampled($image_result_object,$image_source_object,0,0,0,0,$width_result,$height_result,$width_source,$height_source);

        return $image_result_object;

    }

    /**
     * @param string|NULL $file_path
     * @param int|NULL $x
     * @param int|NULL $y
     * @param int|NULL $width_crop
     * @param int|NULL $height_crop
     * @param int|NULL $width_result
     * @param int|NULL $height_result
     * @return resource
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function crop_image_from_file_path(string $file_path=NULL,int $x=NULL,int $y=NULL,int $width_crop=NULL,int $height_crop=NULL,int $width_result=NULL,int $height_result=NULL){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($width_crop)&&empty($height_crop))
            $error_info_list[]='Width & height is empty';

        if(!file_exists($file_path))
            $error_info_list[]='File is not exists';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $image_size_source      =Image::get_image_pixel_size_from_file_path($file_path);
        $width_source           =$image_size_source['width'];
        $height_source          =$image_size_source['height'];
        $image_object           =Image::get_image_object_from_file_path($file_path);

        if(empty($width_crop))
            $width_crop=$width_source;

        if(empty($height_crop))
            $height_crop=$height_source;

        if(empty($width_result))
            $width_result=$width_crop;

        if(empty($height_result))
            $height_result=$height_crop;

        if($width_source<$width_result){

            $width_result   =$width_source;
            $width_crop     =$width_source;
            $x              =0;

        }

        if($height_source<$height_result){

            $height_result  =$height_source;
            $height_crop    =$height_source;
            $y              =0;

        }

        if(is_null($x))
            $x=floor(($width_source-$width_crop)/2);

        if(is_null($y))
            $y=floor(($height_source-$height_crop)/2);

        return self::crop_image_from_image_object($image_object,$x,$y,$width_crop,$height_crop,$width_result,$height_result,$width_source,$height_source);

    }

    /**
     * @param null $image_source_object
     * @param int|NULL $x
     * @param int|NULL $y
     * @param int|NULL $width_crop
     * @param int|NULL $height_crop
     * @param int|NULL $width_result
     * @param int|NULL $height_result
     * @param int|NULL $width_source
     * @param int|NULL $height_source
     * @return resource
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function crop_image_from_image_object($image_source_object=NULL,int $x=NULL,int $y=NULL,int $width_crop=NULL,int $height_crop=NULL,int $width_result=NULL,int $height_result=NULL,int $width_source=NULL,int $height_source=NULL){

        $error_info_list=[];

        if(empty($image_source_object))
            $error_info_list[]='Image object is empty';

        if(empty($width_result)&&empty($height_result))
            $error_info_list[]='Width & height is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        if(empty($width_source)||empty($height_source)){

            $image_size_source      =Image::get_image_pixel_size_from_image_object($image_source_object);
            $width_source           =$image_size_source['width'];
            $height_source          =$image_size_source['height'];

        }

        if(empty($width_crop))
            $width_crop=$width_source;

        if(empty($height_crop))
            $height_crop=$height_source;

        if(empty($width_result))
            $width_result=$width_crop;

        if(empty($height_result))
            $height_result=$height_crop;

        if($width_source<$width_result){

            $width_result   =$width_source;
            $width_crop     =$width_source;
            $x              =0;

        }

        if($height_source<$height_result){

            $height_result  =$height_source;
            $height_crop    =$height_source;
            $y              =0;

        }

        if(is_null($x))
            $x=floor(($width_source-$width_crop)/2);

        if(is_null($y))
            $y=floor(($height_source-$height_crop)/2);

        $image_result_object=self::get_new_image_object($width_result,$height_result);

        imagecopy($image_result_object,$image_source_object,0,0,$x,$y,$width_crop,$height_crop);

        return $image_result_object;

    }

    /**
     * @param null $image_object
     * @param string|NULL $file_path
     * @param string $type
     * @param int $quality
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function save_image($image_object=NULL,string $file_path=NULL,string $type='jpg',int $quality=75){

        $error_info_list=[];

        if(empty($image_object))
            $error_info_list[]='Image object is empty';

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($type))
            $error_info_list[]='File type is empty';

        if(empty($quality))
            $error_info_list[]='Quality is empty';
        else if($quality<0||$quality>100)
            $error_info_list[]='Quality is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        if(file_exists($file_path))
            unlink($file_path);

        switch($type){

            case'jpg':
            case'jpeg':
            case'jfif':
                return imagejpeg($image_object,$file_path,$quality);

            case'png':{

                $quality=9-ceil($quality/10)-1;

                return imagepng($image_object,$file_path,$quality,PNG_ALL_FILTERS);

            }

            case'gif':
                return imagegif($image_object,$file_path);

            default:{

                return imagejpeg($image_object,$file_path,$quality);

//                $error=array(
//                    'title' =>'Parameters problem',
//                    'info'  =>'File type is not valid'
//                );
//
//                throw new ParametersException($error);

            }

        }

    }

    /**
     * @param int|NULL $width_source
     * @param int|NULL $height_source
     * @param int|NULL $width_result
     * @param int|NULL $height_result
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_optimal_image_crop_size(int $width_source=NULL,int $height_source=NULL,int $width_result=NULL,int $height_result=NULL){

        $error_info_list=[];

        if(empty($width_source))
            $error_info_list[]='Width source is empty';

        if(empty($height_source))
            $error_info_list[]='Height source is empty';

        if(empty($width_result)&&empty($height_result))
            $error_info_list[]='Width and height result is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        if(empty($width_result))
            $width_result=floor($height_result*$width_source/$height_source);

        if(empty($height_result))
            $height_result=floor($width_result*$height_source/$width_source);

        if($width_source<$width_result)
            $width_result=$width_source;

        if($height_source<$height_result)
            $height_result=$height_source;

        return array(
            'width'     =>$width_result,
            'height'    =>$height_result
        );

    }

    /**
     * @param string|NULL $file_path
     * @return resource
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function get_image_object_with_exif_orientation(string $file_path=NULL){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';
        else if(!file_exists($file_path))
            $error_info_list[]='File is not exists';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $exif           =NULL;
        $image_object   =Image::get_image_object_from_file_path($file_path);

        try{

            $exif=@exif_read_data($file_path);

        }
        catch (\Exception $error){

            return $image_object;

        }

        if(!isset($exif['Orientation']))
            return $image_object;

        $orientation=$exif['Orientation'];

        switch($orientation){

            case 0:
            case 1:
                return $image_object;

            case 2:{

                if(!imageflip($image_object,IMG_FLIP_HORIZONTAL)){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Image cannot be flip'
                    ];

                    throw new PhpException($error);

                }

                return $image_object;

            }

            case 3:
                return imagerotate($image_object,180, 0);

            case 4:{

                if(!imageflip($image_object, IMG_FLIP_VERTICAL)){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Image cannot be flip'
                    ];

                    throw new PhpException($error);

                }

                return $image_object;

            }
            case 5:{

                $image_object=imagerotate($image_object,-90,0);

                if(!imageflip($image_object,IMG_FLIP_HORIZONTAL)){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Image cannot be flip'
                    ];

                    throw new PhpException($error);

                }

                return $image_object;

            }
            case 6:
                return imagerotate($image_object,-90,0);

            case 7:{

                $image_object=imagerotate($image_object,90,0);

                if(!imageflip($image_object,IMG_FLIP_HORIZONTAL)){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Image cannot be flip'
                    ];

                    throw new PhpException($error);

                }

                return $image_object;

            }
            case 8:
                return imagerotate($image_object, 90, 0);

            default:{

                $error=[
                    'title'     =>PhpException::$title,
                    'info'      =>'Image exif is not correct',
                    'data'      =>$orientation
                ];

                throw new PhpException($error);

            }

        }

    }

}