<?php

namespace Core\Module\Image;

use Core\Module\Exception\ParametersException;

class ImageCash{

    /** @var array */
    public  static $image_list=[];

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$image_list=[];

        return true;

    }

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $param
     * @return bool
     */
    public  static function isset_image_parameter_in_cash(int $image_ID=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(empty(self::$image_list[$image_ID]))
            return false;

        return !empty(self::$image_list[$image_ID][$param]);

    }

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $param
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_image_parameter_in_cash(int $image_ID=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'image_ID'      =>$image_ID,
                    'param'         =>$param
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$image_list[$image_ID]))
            return NULL;

        return empty(self::$image_list[$image_ID][$param])?NULL:self::$image_list[$image_ID][$param];

    }

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $param
     * @param null $value
     * @param bool $is_rewrite
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_image_parameter_in_cash(int $image_ID=NULL,string $param=NULL,$value=NULL,bool $is_rewrite=true){

        $error_info_list=[];

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($param))
            $error_info_list[]='Parameters is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'image_ID'      =>$image_ID,
                    'param'         =>$param,
                    'value'         =>$value,
                    'is_rewrite'    =>$is_rewrite
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$image_list[$image_ID]))
            self::$image_list[$image_ID]=[];

        if($is_rewrite)
            self::$image_list[$image_ID][$param]=$value;

        return true;

    }

}