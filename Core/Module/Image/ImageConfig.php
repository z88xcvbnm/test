<?php

namespace Core\Module\Image;

class ImageConfig{

    /** @var array */
    public  static $image_convert_type_list=array(
        'slide'=>array(
            'preview'=>array(
                'width'     =>220,
                'height'    =>210
            ),
            'small'=>array(
                'width'     =>502,
                'height'    =>480
            ),
            'middle'=>array(
                'width'     =>754,
                'height'    =>720
            ),
            'large'=>array(
                'width'     =>1131,
                'height'    =>1080
            )
        ),
        'image'=>array(
            'preview'=>array(
                'width'     =>200
            ),
            'small'=>array(
                'width'     =>400
            ),
            'middle'=>array(
                'width'     =>800
            ),
            'large'=>array(
                'width'     =>1920
            )
        ),
        'image_ico'=>array(
            'preview'=>array(
                'width'     =>140,
                'height'    =>140
            ),
            'small'=>array(
                'width'     =>300,
                'height'    =>300
            ),
            'middle'=>array(
                'width'     =>400,
                'height'    =>400
            ),
            'large'=>array(
                'width'     =>720,
                'height'    =>720
            )
        ),
        'logo'=>array(
            'preview'=>array(
                'width'     =>340,
                'height'    =>340
            ),
            'small'=>array(
                'width'     =>480,
                'height'    =>480
            ),
            'middle'=>array(
                'width'     =>720,
                'height'    =>720
            )
        ),
    );

}