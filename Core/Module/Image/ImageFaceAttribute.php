<?php

namespace Core\Module\Image;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;
use Core\Module\User\User;

class ImageFaceAttribute{

    /** @var string */
    public  static $table_name='_image_face_attribute';

    /**
     * @param int|NULL $image_ID
     * @param int|NULL $image_face_ID
     * @param array $key_list
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_image_face_attribute(int $image_ID=NULL,int $image_face_ID=NULL,array $key_list=[]){

        $error_info_list=[];

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($image_face_ID))
            $error_info_list[]='Image face ID is empty';

        if(count($key_list)==0)
            return[];

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $value_list=[];

        foreach($key_list as $key=>$value)
            if(!empty($key)){

                if(count($value)==0)
                    $value=NULL;
                else if($value==1)
                    $value=array_shift($value);
                else
                    $value=Json::encode($value);

                $value_list[]=[
                    'user_id'           =>User::$user_ID,
                    'image_id'          =>$image_ID,
                    'image_face_id'     =>$image_face_ID,
                    'key'               =>$key,
                    'value'             =>$value,
                    'date_create'       =>'NOW()',
                    'date_update'       =>'NOW()'
                ];

            }

        if(count($value_list)==0)
            return[];

        $q=[
            'table'     =>self::$table_name,
            'values'    =>$value_list
        ];

        $r=Db::insert($q);

        if(count($r)==0)
            return[];

        $id_list=[];

        foreach($r as $row)
            $id_list[]=$row['id'];

        return $id_list;

    }

}