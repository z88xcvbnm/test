<?php

namespace Core\Module\Image;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Json\Json;
use Core\Module\User\User;

class ImageFace{

    /** @var string */
    public  static $table_name='_image_face';

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $face_token
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_image_face(int $image_ID=NULL,string $face_token=NULL){

        $error_info_list=[];

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($face_token))
            $error_info_list[]='Face token is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'image_id'              =>$image_ID,
            'face_pp_face_token'    =>$face_token
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $image_ID
     * @param string|NULL $face_plus_plus_request_ID
     * @param string|NULL $face_plus_plus_image_ID
     * @param string|NULL $face_plus_plus_face_token
     * @param array|NULL $coords
     * @param array|NULL $landmark
     * @return |null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_image_face(int $image_ID=NULL,string $face_plus_plus_request_ID=NULL,string $face_plus_plus_image_ID=NULL,string $face_plus_plus_face_token=NULL,array $coords=NULL,array $landmark=NULL){

        $error_info_list=[];

        if(empty($image_ID))
            $error_info_list[]='Image ID is empty';

        if(empty($face_plus_plus_request_ID))
            $error_info_list[]='Face plus plus request ID is empty';

        if(empty($face_plus_plus_image_ID))
            $error_info_list[]='Face plus plus image ID is empty';

        if(empty($face_plus_plus_face_token))
            $error_info_list[]='Face plus plus face token is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'                       =>User::$user_ID,
                'image_id'                      =>$image_ID,
                'face_pp_request_id'            =>$face_plus_plus_request_ID,
                'face_pp_image_id'              =>$face_plus_plus_image_ID,
                'face_pp_face_token'            =>$face_plus_plus_face_token,
                'coords'                        =>empty($coords)?NULL:Json::encode($coords),
                'landmark'                      =>empty($landmark)?NULL:Json::encode($landmark),
                'date_create'                   =>'NOW()',
                'date_update'                   =>'NOW()'
            ]
        ];

        $r=Db::insert($q);

        if(empty($r))
            return NULL;

        return $r[0]['id'];

    }

}