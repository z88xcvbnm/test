<?php

namespace Core\Module\Dir;

use Core\Module\Date\Date;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Exception\SystemException;
use Core\Module\Php\PhpValidation;

class Dir{

    /**
     * @param string|NULL $path
     * @return string
     */
    public  static function get_global_dir(string $path=NULL){

        if(!PhpValidation::is_console_running())
            return $path;

        $pos=mb_strripos($path,DIR_ROOT,0,'utf-8');

        if($pos===false)
            return DIR_ROOT.'/'.$path;

        return $path;

    }

    /**
     * @param string|NULL $path
     * @return array
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function get_file_list_from_dir(string $path=NULL){

        if(empty($path)){

            $error=array(
                'title'     =>'Dir path problem',
                'info'      =>'Dir path is empty'
            );

            throw new ParametersException($error);

        }

        $path=self::get_global_dir($path);

        if(!is_dir($path)){

            $error=array(
                'title'     =>'Path problem',
                'info'      =>'Path is nor dir',
                'data'      =>array(
                    'path'      =>$path
                )
            );

            throw new PathException($error);

        }

        return scandir($path);

    }

    /**
     * @return string
     */
    public  static function get_root_dir(){

        return $_SERVER['DOCUMENT_ROOT'];

    }

    /**
     * @return string
     */
    public  static function get_current_dir(){

        return getcwd();

    }

    /**
     * @param string|NULL $path
     * @return bool
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function set_current_dir(string $path=NULL){

        if(empty($path)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Dir path is empty'
            );

            throw new ParametersException($error);

        }

        $path=self::get_global_dir($path);

        if(!is_dir($path)){

            $error=array(
                'title'     =>'Path problem',
                'info'      =>'Path is nor dir',
                'data'      =>array(
                    'path'      =>$path
                )
            );

            throw new PathException($error);

        }

        return chdir($path);

    }

    /**
     * @param string|NULL $path
     * @param int $access
     * @return bool
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function create_dir(string $path=NULL,int $access=0755){

        if(empty($path)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Dir path is empty'
            );

            throw new ParametersException($error);

        }

        $path=self::get_global_dir($path);

        if(file_exists($path))
            return false;

        try{

            return mkdir($path,$access);

        }catch(\Exception $error){

            throw new SystemException($error);

        }

    }

    /**
     * @param string|NULL $dir
     * @return bool
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function remove_dir(string $dir=NULL){

        if(empty($dir)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Dir path is empty'
            );

            throw new ParametersException($error);

        }

        $dir=self::get_global_dir($dir);

        if(is_dir($dir)){

            self::clean_dir($dir);

            if(count(scandir($dir))==2)
                return rmdir($dir);

            return false;

        }

        return true;

    }

    /**
     * @param string|NULL $dir
     * @return bool
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function clean_dir(string $dir=NULL){

        if(empty($dir)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Dir path is empty'
            );

            throw new ParametersException($error);

        }

        $dir=self::get_global_dir($dir);

        if(!is_dir($dir)){

            $error=array(
                'title'     =>'Dir path problem',
                'info'      =>'Path is nor dir',
                'path'      =>$dir
            );

            throw new PathException($error);

        }

        if(is_dir($dir)){

            foreach(scandir($dir) as $file_name)
                if($file_name!='.'&&$file_name!='..'){

                    $file_path=$dir.'/'.$file_name;

                    if(is_file($file_path))
                        unlink($file_path);
                    else if(is_dir($file_path))
                        self::remove_dir($file_path);

                }

            return true;

        }

        return false;

    }

    /**
     * @param string $from_dir
     * @param string $to_dir
     * @return bool
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function copy_dir(string $from_dir,string $to_dir){

        if(empty($from_dir)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Dir path is empty',
                'data'      =>array(
                    'path'      =>$from_dir
                )
            );

            throw new ParametersException($error);

        }

        if(empty($to_dir)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Dir path is empty',
                'data'      =>array(
                    'path'      =>$to_dir
                )
            );

            throw new ParametersException($error);

        }

        $from_dir   =self::get_global_dir($from_dir);
        $to_dir     =self::get_global_dir($to_dir);

        if(!is_dir($from_dir)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Path for copy is not dir',
                'data'      =>array(
                    'path'      =>$from_dir
                )
            );

            throw new ParametersException($error);

        }

        if(!file_exists($to_dir))
            self::create_dir($to_dir);
        else if(!is_dir($to_dir)){

            $error=array(
                'title'     =>'Path problem',
                'info'      =>'Copy path is not dir',
                'data'      =>array(
                    'path'      =>$to_dir
                )
            );

            throw new PathException($error);

        }

        foreach(scandir($from_dir) as $file_name)
            if($file_name!='.'&&$file_name!='..'){

                $file_path=$from_dir.'/'.$file_name;

                if(is_file($file_path))
                    copy($file_path,$to_dir.'/'.$file_name);
                else if(is_dir($file_path)){

                    self::create_dir($to_dir.'/'.$file_name);

                    self::copy_dir($file_path,$to_dir.'/'.$file_name);

                }

            }

        return true;

    }

    /**
     * @param string|NULL $path_default
     * @param int|NULL $timestamp
     * @return string
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function get_dir_from_timestamp(string $path_default=NULL,int $timestamp=NULL){

        $error_info_list=[];

        if(empty($path_default))
            $error_info_list[]='Path default is empty';

        if(empty($timestamp))
            $error_info_list[]='Timestamp is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'path_default'  =>$path_default,
                    'timestamp'     =>$timestamp
                )
            );

            throw new ParametersException($error);

        }

        $path_default=self::get_global_dir($path_default);

        return self::get_dir_from_date($path_default,Date::get_date_time_full($timestamp));

    }

    /**
     * @param string|NULL $path_default
     * @param string|NULL $date
     * @return string
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function get_dir_from_date(string $path_default=NULL,string $date=NULL){

        $error_info_list=[];

        if(empty($path_default))
            $error_info_list[]='Path default is empty';

        if(empty($date))
            $error_info_list[]='Date is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'path_default'=>$path_default,
                    'date'=>$date
                )
            );

            throw new ParametersException($error);

        }

        $path_default=self::get_global_dir($path_default);

        list($year,$month,$day)=Date::get_date_split_from_string($date);

        self::create_dir($path_default);

        $path_default.='/'.$year;

        self::create_dir($path_default);

        $path_default.='/'.$month;

        self::create_dir($path_default);

        $path_default.='/'.$day;

        self::create_dir($path_default);

        return $path_default;

    }

    /**
     * @param string|NULL $path_default
     * @param string|NULL $path_date
     * @return string
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function get_dir_from_date_path(string $path_default=NULL,string $path_date=NULL){

        $error_info_list=[];

        if(empty($path_default))
            $error_info_list[]='Path default is empty';

        if(empty($path_date))
            $error_info_list[]='Path date is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'path_default'  =>$path_default,
                    'path_date'     =>$path_date
                )
            );

            throw new ParametersException($error);

        }

        $path_default=self::get_global_dir($path_default);

        $list=mb_split('/',$path_date);

        if(count($list)!=3){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Path date is not valid',
                'data'=>array(
                    'path_default'  =>$path_default,
                    'path_date'     =>$path_date
                )
            );

            throw new ParametersException($error);

        }

        self::create_dir($path_default);

        $path_default.='/'.$list[0];

        self::create_dir($path_default);

        $path_default.='/'.$list[1];

        self::create_dir($path_default);

        $path_default.='/'.$list[2];

        self::create_dir($path_default);

        return $path_default;

    }

}