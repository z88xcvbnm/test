<?php

namespace Core\Module\Dir;

class DirConfig{

    /** @var string */
    public  static $dir_root                            =NULL;

    /** @var string */
    public  static $dir_resource                        ='Resource';

    /** @var string */
    public  static $dir_audio                           ='Resource/Audio';

    /** @var string */
    public  static $dir_video                           ='Resource/Video';

    /** @var string */
    public  static $dir_map                             ='Resource/Map';

    /** @var string */
    public  static $dir_image                           ='Resource/Image';

    /** @var string */
    public  static $dir_image_hash_show                 ='show/image/content';

    /** @var string */
    public  static $dir_video_hash_show                 ='show/video/content';

    /** @var string */
    public  static $dir_image_hash_download             ='download/image/content';

    /** @var string */
    public  static $dir_avatar                          ='Resource/Avatar';

    /** @var string */
    public  static $dir_upload                          ='Resource/Upload';

    /** @var string */
    public  static $dir_sqlite                          ='Resource/Sqlite';

    /** @var string */
    public  static $dir_sqlite_general                  ='Resource/Sqlite/General';

    /** @var string */
    public  static $dir_sqlite_general_update_pack      ='Resource/Sqlite/GeneralUpdatePack';

    /** @var string */
    public  static $dir_sqlite_excursion                ='Resource/Sqlite/Excursion';

    /** @var string */
    public  static $dir_sqlite_excursion_update_pack    ='Resource/Sqlite/ExcursionUpdatePack';

    /** @var string */
    public  static $dir_sqlite_place                    ='Resource/Sqlite/Place';

    /** @var string */
    public  static $dir_sqlite_place_update_pack        ='Resource/Sqlite/PlaceUpdatePack';

    /** @var string */
    public  static $dir_temp                            ='Temp';

    /** @var string */
    public  static $dir_log                             ='Temp/Log';

    /** @var string */
    public  static $dir_video_temp                      ='Temp/Video';

    /** @var string */
    public  static $dir_image_temp                      ='Temp/Image';

    /** @var string */
    public  static $dir_avatar_temp                     ='Temp/Avatar';

    /** @var string */
    public  static $dir_image_tiff_temp                 ='Temp/Image/Tiff';

    /** @var string */
    public  static $dir_avatar_tiff_temp                ='Temp/Avatar/Tiff';

    /** @var string */
    public  static $dir_db_dump                         ='Temp/DbDump';

    /** @var string */
    public  static $dir_migrate                         ='Temp/Migrate';

}