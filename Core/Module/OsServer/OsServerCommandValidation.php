<?php

namespace Core\Module\OsServer;

use Core\Module\Exception\ParametersException;

class OsServerCommandValidation{

    /**
     * @param string|NULL $command
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_shell_command(string $command=NULL){

        if(empty($command)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Command is empty'
            );

            throw new ParametersException($error);

        }

        if(OsServerCommandCash::isset_os_shell_command($command))
            return OsServerCommandCash::get_os_shell_command($command);

        $r                  =shell_exec('which '.escapeshellarg($command));
        $is_isset_command   =!empty($r);

        OsServerCommandCash::add_os_shell_command($command,$is_isset_command);

        return $is_isset_command;

    }

}