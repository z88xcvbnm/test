<?php

namespace Core\Module\OsServer;

use Core\Module\Exception\ParametersException;

class OsServerCommandCash{

    /** @var array */
    public  static $os_shell_command_list=[];

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$os_shell_command_list=[];

        return true;

    }

    /**
     * @param string|NULL $command
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_os_shell_command(string $command=NULL){

        if(empty($command)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Command is empty'
            );

            throw new ParametersException($error);

        }

        return isset(self::$os_shell_command_list[$command]);

    }

    /**
     * @param string|NULL $command
     * @return bool
     * @throws ParametersException
     */
    public  static function get_os_shell_command(string $command=NULL){

        if(empty($command)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Command is empty'
            );

            throw new ParametersException($error);

        }

        if(!isset(self::$os_shell_command_list[$command]))
            return false;

        return self::$os_shell_command_list[$command];

    }

    /**
     * @param string|NULL $command
     * @param bool|false $is_isset_command
     * @return bool
     * @throws ParametersException
     */
    public  static function add_os_shell_command(string $command=NULL,bool $is_isset_command=false){

        if(empty($command)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Command is empty'
            );

            throw new ParametersException($error);

        }

        self::$os_shell_command_list[$command]=$is_isset_command;

        return true;

    }

}