<?php

namespace Core\Module\OsServer;

use Core\Module\Exception\PhpException;

class OsServer{

    /** @var string */
    public  static $os_server_name;

    /** @var bool */
    public  static $is_linux;

    /** @var bool */
    public  static $is_windows;

    /** @var bool */
    public  static $is_freebsd;

    /**
     * @return bool
     */
    public  static function set_data_default(){

        self::$is_linux         =false;
        self::$is_freebsd       =false;
        self::$is_windows       =false;

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_os_type(){

        if(empty(self::$os_server_name))
            self::set_os_server_name();

        self::set_data_default();

        $os_server_name_short=strtolower(mb_substr(self::$os_server_name,0,3,'utf-8'));

        switch($os_server_name_short){

            case'lin':{

                self::$is_linux=true;

                break;

            }

            case'win':{

                self::$is_windows=true;

                break;

            }

            case'fre':{

                self::$is_freebsd=true;

                break;

            }

            default:{

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>'Os server name is not valid'
                );

                throw new PhpException($error);

            }

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_os_server_name(){

        self::$os_server_name=php_uname('s');

        self::set_os_type();

        return true;

    }

    /**
     * @return string
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_os_server_name(){

        if(!empty(self::$os_server_name))
            return self::$os_server_name;

        self::set_os_server_name();

        return self::$os_server_name;

    }

}