<?php

namespace Core\Module\OsServer;

class OsServerDetermine{

    /**
     * @return bool
     */
    private static function set_http_type(){

        if(
              OsServer::$is_windows
            ||\Config::$http_type=='http'
        )
            \Config::$http_type='http';
        else
            \Config::$http_type='https';

        return true;

    }

    /**
     * @return bool
     */
    private static function set_debug(){

//        if(!OsServer::$is_windows)
//            \Config::$is_debug=false;

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        OsServer::set_os_server_name();

        self::set_http_type();
        self::set_debug();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}