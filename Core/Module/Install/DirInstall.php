<?php

namespace Core\Module\Install;

use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;

class DirInstall{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Dir::create_dir(DirConfig::$dir_resource);
        Dir::create_dir(DirConfig::$dir_audio);
        Dir::create_dir(DirConfig::$dir_image);
        Dir::create_dir(DirConfig::$dir_video);
        Dir::create_dir(DirConfig::$dir_map);
        Dir::create_dir(DirConfig::$dir_avatar);
        Dir::create_dir(DirConfig::$dir_upload);
        Dir::create_dir(DirConfig::$dir_temp);
        Dir::create_dir(DirConfig::$dir_log);
        Dir::create_dir(DirConfig::$dir_video_temp);
        Dir::create_dir(DirConfig::$dir_image_temp);
        Dir::create_dir(DirConfig::$dir_image_tiff_temp);

        return true;

    }

}