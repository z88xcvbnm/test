<?php

namespace Core\Module\PromoCode;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class PromoCode{

    /** @var string */
    public  static $table_name='_promo_code';

    /**
     * @param string|NULL $code
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_promo_code(string $code=NULL){

        if(empty($code))
            return false;

        $where_list=[
            'code'=>$code
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param string|NULL $code
     * @return array|bool|null
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_promo_code_data(string $code=NULL){

        if(empty($code))
            return false;

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'date_start',
                'date_finish',
                'use_len',
                'use_max_len',
                'is_unique',
                'money'
            ],
            'where'=>[
                'code'          =>$code,
                'date_start'=>[
                    'method'    =>'<=',
                    'value'     =>'NOW()'
                ],
                'date_finish'=>[
                    'method'    =>'>=',
                    'value'     =>'NOW()'
                ],
                'type'      =>0
            ],
            'order'=>[
                [
                    'column'    =>'id',
                    'direction' =>'desc'
                ]
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'ID'            =>$r[0]['id'],
            'date_start'    =>$r[0]['date_start'],
            'date_finish'   =>$r[0]['date_finish'],
            'use_len'       =>$r[0]['use_len'],
            'use_max_len'   =>$r[0]['use_max_len'],
            'is_unique'     =>(bool)$r[0]['is_unique'],
            'money'         =>(float)$r[0]['money']
        ];

    }

    /**
     * @param int|NULL $promo_code_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_use_len(int $promo_code_ID=NULL){

        if(empty($promo_code_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Promo code ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'use_len'=>[
                    'function'=>'use_len + 1'
                ],
                'date_update'=>'NOW()'
            ],
            'where'=>[
                'id'        =>$promo_code_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Use len was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $promo_code_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_to_used(int $promo_code_ID=NULL){

        if(empty($promo_code_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Promo code ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'set'=>[
                'date_use'      =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>2
            ],
            'where'=>[
                'id'        =>$promo_code_ID,
                'type'      =>0
            ]
        ];

        if(!Db::update($q)){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Use len was not update'
            ];

            throw new DbQueryException($error);

        }

        return true;

    }

}