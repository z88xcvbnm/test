<?php

namespace Core\Module\PromoCode;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;

class PromoCodeItem{

    /** @var string */
    public  static $table_name='_promo_code_item';

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $promo_code_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_promo_code_item(int $user_ID=NULL,int $promo_code_ID=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($promo_code_ID))
            $error_info_list[]='Promo code ID is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $where_list=[
            'user_id'           =>$user_ID,
            'promo_code_id'     =>$promo_code_ID
        ];

        return Db::isset_row(self::$table_name,0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $promo_code_ID
     * @param string|NULL $code
     * @param float|NULL $money
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_promo_code_item(int $user_ID=NULL,int $promo_code_ID=NULL,string $code=NULL,float $money=NULL){

        $error_info_list=[];

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(empty($promo_code_ID))
            $error_info_list[]='Promo code ID is empty';

        if(empty($code))
            $error_info_list[]='Code is empty';

        if(empty($money))
            $error_info_list[]='Money is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'values'=>[
                'user_id'           =>$user_ID,
                'promo_code_id'     =>$promo_code_ID,
                'code'              =>$code,
                'money'             =>$money,
                'date_create'       =>'NOW()',
                'date_update'       =>'NOW()',
                'type'              =>0
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'Promo code item was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

}