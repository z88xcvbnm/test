<?php

namespace Core\Module\Avatar;

use Core\Module\Exception\ParametersException;

class AvatarCash{

    /** @var array */
    public  static $avatar_list=[];

    /**
     * @param int|NULL $avatar_ID
     * @param string|NULL $param
     * @return bool
     */
    public  static function isset_avatar_parameter_in_cash(int $avatar_ID=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($avatar_ID))
            $error_info_list[]='Avatar ID is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(empty(self::$avatar_list[$avatar_ID]))
            return false;

        return !empty(self::$avatar_list[$avatar_ID][$param]);

    }

    /**
     * @param int|NULL $avatar_ID
     * @param string|NULL $param
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_parameter_in_cash(int $avatar_ID=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($avatar_ID))
            $error_info_list[]='Avatar ID is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'avatar_ID'      =>$avatar_ID,
                    'param'         =>$param
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$avatar_list[$avatar_ID]))
            return NULL;

        return empty(self::$avatar_list[$avatar_ID][$param])?NULL:self::$avatar_list[$avatar_ID][$param];

    }

    /**
     * @param int|NULL $avatar_ID
     * @param string|NULL $param
     * @param null $value
     * @param bool $is_rewrite
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_avatar_parameter_in_cash(int $avatar_ID=NULL,string $param=NULL,$value=NULL,bool $is_rewrite=true){

        $error_info_list=[];

        if(empty($avatar_ID))
            $error_info_list[]='Avatar ID is empty';

        if(empty($param))
            $error_info_list[]='Parameters is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'avatar_ID'      =>$avatar_ID,
                    'param'         =>$param,
                    'value'         =>$value,
                    'is_rewrite'    =>$is_rewrite
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$avatar_list[$avatar_ID]))
            self::$avatar_list[$avatar_ID]=[];

        if($is_rewrite)
            self::$avatar_list[$avatar_ID][$param]=$value;

        return true;

    }

}