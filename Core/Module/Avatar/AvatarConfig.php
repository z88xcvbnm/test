<?php

namespace Core\Module\Avatar;

class AvatarConfig{

    /** @var array */
    public  static $avatar_convert_type_list=array(
        'slide'=>array(
            'preview'=>array(
                'width'     =>220,
                'height'    =>210
            ),
            'small'=>array(
                'width'     =>502,
                'height'    =>480
            ),
            'middle'=>array(
                'width'     =>754,
                'height'    =>720
            ),
            'large'=>array(
                'width'     =>1131,
                'height'    =>1080
            )
        ),
        'avatar'=>array(
            'preview'=>array(
                'width'     =>135,
                'height'    =>197
            ),
            'small'=>array(
                'width'     =>329,
                'height'    =>480
            ),
            'middle'=>array(
                'width'     =>494,
                'height'    =>720
            ),
            'large'=>array(
                'width'     =>740,
                'height'    =>1080
            )
        ),
        'avatar_ico'=>array(
            'preview'=>array(
                'width'     =>140,
                'height'    =>140
            ),
            'small'=>array(
                'width'     =>300,
                'height'    =>300
            ),
            'middle'=>array(
                'width'     =>400,
                'height'    =>400
            ),
            'large'=>array(
                'width'     =>720,
                'height'    =>720
            )
        ),
        'logo'=>array(
            'preview'=>array(
                'width'     =>340,
                'height'    =>340
            ),
            'small'=>array(
                'width'     =>480,
                'height'    =>480
            ),
            'middle'=>array(
                'width'     =>720,
                'height'    =>720
            )
        ),
    );

}