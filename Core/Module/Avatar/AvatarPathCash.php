<?php

namespace Core\Module\Avatar;

use Core\Module\Exception\ParametersException;

class AvatarPathCash{

    /** @var array */
    public  static $avatar_list=[];

    /**
     * @param string|NULL $file_path
     * @param string|NULL $param
     * @return bool
     */
    public  static function isset_avatar_parameter_in_cash(string $file_path=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(empty(self::$avatar_list[$file_path]))
            return false;

        return !empty(self::$avatar_list[$file_path][$param]);

    }

    /**
     * @param string|NULL $file_path
     * @param string|NULL $param
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_parameter_in_cash(string $file_path=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'avatar_ID'      =>$file_path,
                    'param'         =>$param
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$avatar_list[$file_path]))
            return NULL;

        return empty(self::$avatar_list[$file_path][$param])?NULL:self::$avatar_list[$file_path][$param];

    }

    /**
     * @param string|NULL $file_path
     * @param string|NULL $param
     * @param null $value
     * @param bool $is_rewrite
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_avatar_parameter_in_cash(string $file_path=NULL,string $param=NULL,$value=NULL,bool $is_rewrite=true){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($param))
            $error_info_list[]='Parameters is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'avatar_ID'      =>$file_path,
                    'param'         =>$param,
                    'value'         =>$value,
                    'is_rewrite'    =>$is_rewrite
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$avatar_list[$file_path]))
            self::$avatar_list[$file_path]=[];

        if($is_rewrite)
            self::$avatar_list[$file_path][$param]=$value;

        return true;

    }

}