<?php

namespace Core\Module\Avatar;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Encrypt\Hash;
use Core\Module\Exception\DbParametersException;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\FileException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Exception\PhpException;

class Avatar{

    /**
     * @param int|NULL $avatar_ID
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_avatar_ID(int $avatar_ID=NULL){

        if(empty($avatar_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Avatar ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($avatar_ID,'_avatar');

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $file_ID
     * @param string|NULL $hash
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_avatar(int $user_ID=NULL,int $file_ID=NULL,string $hash=NULL){

        if(empty($user_ID)&&empty($file_ID)&&empty($hash)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($file_ID))
            $where_list['file_id']=$file_ID;

        if(!empty($hash))
            $where_list['hash']=$hash;

        if(count($where_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row('_avatar',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @param int|NULL $file_ID
     * @param string|NULL $hash
     * @return null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_ID(int $user_ID=NULL,int $file_ID=NULL,string $hash=NULL){

        if(empty($user_ID)&&empty($file_ID)&&empty($hash)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list=[];

        if(!empty($user_ID))
            $where_list['user_id']=$user_ID;

        if(!empty($file_ID))
            $where_list['file_id']=$file_ID;

        if(!empty($hash))
            $where_list['hash']=$hash;

        if(count($where_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $where_list['type']=0;

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_avatar',
            'where'=>$where_list,
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param array $avatar_ID_list
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_ID_list(array $avatar_ID_list=[]){

        if(count($avatar_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Avatar ID list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'    =>array(
                'id'
            ),
            'table'     =>'_avatar',
            'where'     =>array(
                'id'        =>$avatar_ID_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['id'];

        return $list;

    }

    /**
     * @param int|NULL $avatar_ID
     * @param string|NULL $file_extension
     * @param string|NULL $date_create
     * @return null|string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_file_path(int $avatar_ID=NULL,string $file_extension=NULL,string $date_create=NULL){

        $error_info_list=[];

        if(empty($avatar_ID))
            $error_info_list[]='Avatar ID is empty';

        if(empty($file_extension))
            $error_info_list[]='File type is empty';

        if(empty($date_create))
            $error_info_list[]='Date create is empty';

        if(AvatarCash::isset_avatar_parameter_in_cash($avatar_ID,'file_path'))
            return AvatarCash::get_avatar_parameter_in_cash($avatar_ID,'file_path');

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'avatar_ID'      =>$avatar_ID,
                    'file_extension'     =>$file_extension,
                    'date_create'   =>$date_create
                )
            );

            throw new ParametersException($error);

        }

        $date_path      =Date::get_date_path($date_create,DirConfig::$dir_avatar);
        $file_path      =$date_path.'/'.$avatar_ID.'.'.$file_extension;

        AvatarCash::add_avatar_parameter_in_cash($avatar_ID,$file_path);

        return $file_path;

    }

    /**
     * @param int|NULL $avatar_ID
     * @return null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_duration(int $avatar_ID=NULL){

        if(empty($avatar_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Avatar ID is empty'
            );

            throw new ParametersException($error);

        }

        if(AvatarCash::isset_avatar_parameter_in_cash($avatar_ID,'duration'))
            return AvatarCash::get_avatar_parameter_in_cash($avatar_ID,'duration');

        $r=Db::get_data_from_ID($avatar_ID,'_avatar',array('duration'),0);

        if(count($r)==0)
            return NULL;

        $duration=empty($r[0]['duration'])?NULL:$r[0]['duration'];

        AvatarCash::add_avatar_parameter_in_cash($avatar_ID,'duration',$duration);

        return $duration;

    }

    /**
     * @param int|NULL $avatar_ID
     * @return null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_size(int $avatar_ID=NULL){

        if(empty($avatar_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Avatar ID is empty'
            );

            throw new ParametersException($error);

        }

        if(AvatarCash::isset_avatar_parameter_in_cash($avatar_ID,'file_size'))
            return AvatarCash::get_avatar_parameter_in_cash($avatar_ID,'file_size');

        $r=Db::get_data_from_ID($avatar_ID,'_avatar',array('file_size'),0);

        if(count($r)==0)
            return NULL;

        $file_size=empty($r[0]['file_size'])?NULL:$r[0]['file_size'];

        AvatarCash::add_avatar_parameter_in_cash($avatar_ID,'file_size',$file_size);

        return $file_size;

    }

    /**
     * @param int|NULL $avatar_ID
     * @return null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_date_create(int $avatar_ID=NULL){

        if(empty($avatar_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Avatar ID is empty'
            );

            throw new ParametersException($error);

        }

        $r=Db::get_data_from_ID($avatar_ID,'_avatar',array('date_create'),0);

        if(count($r)==0)
            return NULL;

        $date_create=empty($r[0]['date_create'])?NULL:$r[0]['date_create'];

        if(!AvatarCash::add_avatar_parameter_in_cash($avatar_ID,'date_create',$date_create)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Date create was not add to cash'
            ];

            throw new PhpException($error);

        }

        return $date_create;

    }

    /**
     * @param int|NULL $avatar_ID
     * @return null|string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_file_path_from_avatar_ID(int $avatar_ID=NULL){

        if(empty($avatar_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Avatar ID is empty'
            );

            throw new ParametersException($error);

        }

        if(AvatarCash::isset_avatar_parameter_in_cash($avatar_ID,'file_path'))
            return AvatarCash::get_avatar_parameter_in_cash($avatar_ID,'file_path');

        $column_list    =[];
        $file_extension      =NULL;
        $date_create    =NULL;

        if(AvatarCash::isset_avatar_parameter_in_cash($avatar_ID,'file_extension'))
            $file_extension=AvatarCash::get_avatar_parameter_in_cash($avatar_ID,'file_extension');
        else
            $column_list[]='file_extension';

        if(AvatarCash::isset_avatar_parameter_in_cash($avatar_ID,'date_create'))
            $date_create=AvatarCash::get_avatar_parameter_in_cash($avatar_ID,'date_create');
        else
            $column_list[]='date_create';

        if(count($column_list)>0){

            $r=Db::get_data_from_ID($avatar_ID,'_avatar',$column_list,0);

            if(!empty($r['file_extension'])){

                $file_extension=$r['file_extension'];

                if(!AvatarCash::add_avatar_parameter_in_cash($avatar_ID,'file_extension',$file_extension)){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Date create was not add to cash'
                    ];

                    throw new PhpException($error);

                }

            }

            if(!empty($r['date_create'])){

                $date_create=$r['date_create'];

                if(!AvatarCash::add_avatar_parameter_in_cash($avatar_ID,'date_create',$date_create)){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Date create was not add to cash'
                    ];

                    throw new PhpException($error);

                }

            }

        }

        if(
              empty($date_create)
            ||empty($file_extension)
        ){

            $error=array(
                'title'=>'DB query problem',
                'info'=>'System cannot get avatar date create',
                'data'=>array(
                    'avatar_ID'      =>$avatar_ID,
                    'file_extension'     =>$file_extension,
                    'date_create'   =>$date_create
                )
            );

            throw new DbParametersException($error);

        }

        $avatar_file_path=self::get_avatar_file_path($avatar_ID,$file_extension,$date_create);

        if(!file_exists($avatar_file_path)){

            $error=array(
                'title'=>'File path problem',
                'info'=>'avatar file is not exists',
                'data'=>array(
                    'avatar_ID'      =>$avatar_ID,
                    'file_path'     =>$avatar_file_path
                )
            );

            throw new PathException($error);

        }

        if(!AvatarCash::add_avatar_parameter_in_cash($avatar_ID,'file_path',$avatar_file_path)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Date create was not add to cash'
            ];

            throw new PhpException($error);

        }

        return $avatar_file_path;

    }

    /**
     * @param array $avatar_ID_list
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_file_path_from_avatar_ID_list(array $avatar_ID_list=[]){

        if(count($avatar_ID_list)==0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'avatar ID list is empty'
            );

            throw new ParametersException($error);

        }

        $file_extension              =NULL;
        $file_path              =NULL;
        $date_create            =NULL;
        $avatar_list             =[];
        $select_avatar_ID_list   =[];

        foreach($avatar_ID_list as $avatar_ID)
            if(!isset($avatar_list[$avatar_ID])){

                if(AvatarCash::isset_avatar_parameter_in_cash($avatar_ID,'file_path'))
                    $avatar_list[$avatar_ID]=AvatarCash::get_avatar_parameter_in_cash($avatar_ID,'file_path');
                else{

                    if(AvatarCash::isset_avatar_parameter_in_cash($avatar_ID,'file_extension'))
                        $file_extension=AvatarCash::get_avatar_parameter_in_cash($avatar_ID,'file_extension');

                    if(AvatarCash::isset_avatar_parameter_in_cash($avatar_ID,'date_create'))
                        $date_create=AvatarCash::get_avatar_parameter_in_cash($avatar_ID,'date_create');

                    if(
                          empty($file_extension)
                        ||empty($date_create)
                    )
                        $select_avatar_ID_list[]=$avatar_ID;
                    else
                        $avatar_list[$avatar_ID]=self::get_avatar_file_path($avatar_ID,$file_extension,$date_create);

                }

            }

        if(count($select_avatar_ID_list)>0){

            $select_list=array(
                'id',
                'file_extension',
                'date_create'
            );

            $r=Db::get_data_from_ID_list($select_avatar_ID_list,'_avatar',$select_list,0);

            foreach($r as $row){

                $avatar_ID               =$row['id'];
                $file_extension              =$row['file_extension'];
                $date_create            =$row['date_create'];
                $file_path              =self::get_avatar_file_path($avatar_ID,$file_extension,$date_create);
                $avatar_list[$avatar_ID]  =$file_path;

                if(
                      !AvatarCash::add_avatar_parameter_in_cash($avatar_ID,'file_extension',$file_extension)
                    ||!AvatarCash::add_avatar_parameter_in_cash($avatar_ID,'date_create',$date_create)
                    ||!AvatarCash::add_avatar_parameter_in_cash($avatar_ID,'file_path',$file_path)
                ){

                    $error=[
                        'title'     =>PhpException::$title,
                        'info'      =>'Date create was not add to cash'
                    ];

                    throw new PhpException($error);

                }

            }

        }

        return $avatar_list;

    }

    /**
     * @param string|NULL $file_path
     * @return array|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function get_avatar_pixel_size_from_file_path(string $file_path=NULL){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='Avatar path is empty';

        if(!file_exists($file_path))
            $error_info_list[]='Avatar path is not exists';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        if(AvatarPathCash::isset_avatar_parameter_in_cash($file_path,'pixel_size'))
            return AvatarPathCash::get_avatar_parameter_in_cash($file_path,'pixel_size');

        if(!file_exists($file_path)){

            $error=array(
                'title'     =>'Path problem',
                'info'      =>'File is not exists',
                'data'      =>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $avatar_object=self::get_avatar_object_from_file_path($file_path);

        if(empty($avatar_object)){

            $error=array(
                'title'     =>'File problem',
                'info'      =>'Prepare file fail',
                'data'      =>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $data=self::get_avatar_pixel_size_from_avatar_object($avatar_object);

        if(!AvatarPathCash::add_avatar_parameter_in_cash($file_path,'pixel_size',$data)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Date create was not add to cash'
            ];

            throw new PhpException($error);

        }

        return $data;

    }

    /**
     * @param null $avatar_object
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_pixel_size_from_avatar_object($avatar_object=NULL){

        if(empty($avatar_object)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Avatar object is empty'
            );

            throw new ParametersException($error);

        }

        $width      =imagesx($avatar_object);
        $height     =imagesy($avatar_object);
        $data       =array(
            'width'     =>$width,
            'height'    =>$height
        );

        return $data;

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function get_avatar_object_from_file_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Avatar path is empty'
            );

            throw new ParametersException($error);

        }

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'File path problem',
                'info'  =>'Avatar path is empty'
            );

            throw new PathException($error);

        }

        switch(exif_imagetype($file_path)){

            case 1:
                return imagecreatefromgif($file_path);

            case 2:
                return imagecreatefromjpeg($file_path);

            case 3:{

                $avatar_object  =imagecreatefrompng($file_path);
                $width          =imagesx($avatar_object);
                $height         =imagesy($avatar_object);
                $image          =imagecreatetruecolor($width,$height);

                imagealphablending($image,false);
                imagesavealpha($image,true);

                imagecopyresampled($image,$avatar_object,0,0,0,0,$width,$height,$width,$height);

                return $image;

            }

//            case 4:{
//
//                // IMAGETYPE_SWF
//
//                break;
//
//            }
//
//            case 5:{
//
//                // IMAGETYPE_PSD
//
//                break;
//
//            }
//
//            case 6:{
//
//                // IMAGETYPE_BMP
//
//                break;
//
//            }
//
            case 7:
            case 8:{

                Dir::create_dir(DirConfig::$dir_avatar_temp);
                Dir::create_dir(DirConfig::$dir_avatar_tiff_temp);

                $avatar_temp_path=DirConfig::$dir_avatar_tiff_temp.'/'.Hash::get_random_hash().'.jpg';

                $avatar=new \Imagick($file_path);

                $avatar->setAvatarFormat('jpeg');
                $avatar->writeAvatar($avatar_temp_path);

                $avatar_object=imagecreatefromjpeg($avatar_temp_path);

                unlink($avatar_temp_path);

                return $avatar_object;

            }
//
//            case 9:{
//
//                // IMAGETYPE_JPC
//
//                break;
//
//            }
//
//            case 10:{
//
//                // IMAGETYPE_JP2
//
//                break;
//
//            }
//
//            case 11:{
//
//                // IMAGETYPE_JPX
//
//                break;
//
//            }
//
//            case 12:{
//
//                // IMAGETYPE_JB2
//
//                break;
//
//            }
//
//            case 13:{
//
//                // IMAGETYPE_SWC
//
//                break;
//
//            }
//
//            case 14:{
//
//                // IMAGETYPE_IFF
//
//                break;
//
//            }
//
//            case 15:{
//
//                // IMAGETYPE_WBMP
//
//                break;
//
//            }
//
//            case 16:{
//
//                // IMAGETYPE_XBM
//
//                break;
//
//            }
//
//            case 17:{
//
//                // IMAGETYPE_ICO
//
//                break;
//
//            }

            default:{

                $error=array(
                    'title' =>'File type problem',
                    'info'  =>'File exif avatar type is not valid',
                    'data'  =>array(
                        'avatar_path'        =>$file_path,
                        'exif_avatar_type'   =>exif_imagetype($file_path)
                    )
                );

                throw new FileException($error);

                break;

            }

        }

    }

    /**
     * @param string|NULL $file_path
     * @return null|string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_avatar_type_from_file_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Avatar path is empty'
            );

            throw new ParametersException($error);

        }

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'File path problem',
                'info'  =>'Avatar path is empty'
            );

            throw new PathException($error);

        }

        switch(exif_imagetype($file_path)){

            case 1:
                return 'gif';

            case 2:
                return 'jpg';

            case 3:
                return 'png';

//            case 4:{
//
//                // IMAGETYPE_SWF
//
//                break;
//
//            }
//
//            case 5:{
//
//                // IMAGETYPE_PSD
//
//                break;
//
//            }
//
//            case 6:{
//
//                // IMAGETYPE_BMP
//
//                break;
//
//            }
//
            case 7:
            case 8:
                return 'tiff';
//
//            case 9:{
//
//                // IMAGETYPE_JPC
//
//                break;
//
//            }
//
//            case 10:{
//
//                // IMAGETYPE_JP2
//
//                break;
//
//            }
//
//            case 11:{
//
//                // IMAGETYPE_JPX
//
//                break;
//
//            }
//
//            case 12:{
//
//                // IMAGETYPE_JB2
//
//                break;
//
//            }
//
//            case 13:{
//
//                // IMAGETYPE_SWC
//
//                break;
//
//            }
//
//            case 14:{
//
//                // IMAGETYPE_IFF
//
//                break;
//
//            }
//
//            case 15:{
//
//                // IMAGETYPE_WBMP
//
//                break;
//
//            }
//
//            case 16:{
//
//                // IMAGETYPE_XBM
//
//                break;
//
//            }
//
//            case 17:{
//
//                // IMAGETYPE_ICO
//
//                break;
//
//            }

            default:
                return NULL;

        }

    }

    /**
     * @param array $avatar_ID_list
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_avatar_date_update(array $avatar_ID_list=[]){

        if(count($avatar_ID_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Avatar ID list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_avatar',
            'set'       =>array(
                'date_update'=>'NOW()'
            ),
            'where'     =>array(
                'id'        =>$avatar_ID_list,
                'type'      =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Avatar list date update was not update',
                'data'  =>array(
                    'avatar_ID_list'     =>$avatar_ID_list,
                    'query'             =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $avatar_ID
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_avatar_ID_date_update(int $avatar_ID=NULL){

        if(empty($avatar_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Avatar ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_avatar',
            'set'       =>array(
                'date_update'=>'NOW()'
            ),
            'where'     =>array(
                'id'        =>$avatar_ID,
                'type'      =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Avatar list date update was not update',
                'data'  =>array(
                    'avatar_ID'      =>$avatar_ID,
                    'query'         =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

}