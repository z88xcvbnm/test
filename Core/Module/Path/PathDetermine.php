<?php

namespace Core\Module\Path;

class PathDetermine{

    /**
     * @return bool
     */
    private static function set(){

        return Path::set_dir_root();

    }

    /**
     * @return bool
     */
    public  static function init(){

        return self::set();

    }

}