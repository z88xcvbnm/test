<?php

namespace Core\Module\Path;

use Core\Module\Dir\DirConfig;

class Path{

    /** @var string */
    public  static $dir_root;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$dir_root=NULL;

        return true;

    }

    /**
     * @return bool
     */
    public  static function set_dir_root(){

        self::$dir_root         =DIR_ROOT;
        DirConfig::$dir_root    =DIR_ROOT;

        return true;

    }

    /**
     * @return string
     */
    public  static function get_dir_root(){

        if(!empty(self::$dir_root))
            return self::$dir_root;

        self::set_dir_root();

        return self::$dir_root;

    }

}