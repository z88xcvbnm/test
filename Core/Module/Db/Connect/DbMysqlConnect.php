<?php

namespace Core\Module\Db\Connect;

use Core\Module\Db\Config\DbMySqlConfig;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryParametersException;
use Core\Module\Exception\SystemException;
use Core\Module\Worktime\Worktime;

class DbMysqlConnect{

    /**
     * @return bool
     * @throws DbQueryParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PhpException
     */
    public static function init(){

        Worktime::set_timestamp_point('MySQL Connect Start');

        $config_list=DbMySqlConfig::get_list();

        foreach($config_list as $name=>$config){

            if(!DbConnect::init($config)){

                $error=array(
                    'title'     =>'DB connect problem',
                    'info'      =>'DB was not connect'
                );

                throw new DbQueryParametersException($error);

            }

//            Db::$db_name_default          =$config['name'];
//            Db::$db_host_default          =$config['host'];
//            Db::$db_login_default         =$config['login'];
//            Db::$db_password_default      =$config['password'];
//
//            if(!empty($config['port']))
//                Db::$db_port_default=$config['port'];

//            DbMysqlLocalConfig::set_session_wait_timeout(NULL,$name);

            $q="
                SET
                    SESSION time_zone='+00:00',
                    character_set_database=utf8,
                    NAMES utf8
            ";

            Db::exec($q,$name);

        }

        Worktime::set_timestamp_point('DataBase Connect Done');

        return true;

    }

}