<?php

namespace Core\Module\Db\Connect;

use Core\Module\Db\Db;
use Core\Module\Exception\DbPdoConnectException;
use Core\Module\Exception\DbQueryParametersException;

class DbConnect{

    /**
     * @param array $db_config
     * @return bool
     * @throws DbQueryParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(array $db_config=[]){

        if(empty($db_config['type'])){

            $error=array(
                'title'     =>'DB problem with config',
                'info'      =>'DB config have not DB type',
            );

            throw new DbQueryParametersException($error);

        }

        if(empty(Db::$db_connect_list[$db_config['name']])){

            Db::$db_connect_list[$db_config['name']]=$db_config;

            switch($db_config['type']){

                case'postgresql':{

                    $type           ='pgsql';
                    $reconnect      =array(
                        \PDO::ATTR_PERSISTENT=>true
                    );

                    break;

                }

                case'mysql':{

                    $type           ='mysql';
                    $reconnect      =array(
                        \PDO::ATTR_PERSISTENT=>true
                    );

                    break;

                }

                default:{

                    $error=array(
                        'title'     =>'DB problem with config',
                        'info'      =>'DB type in config is not valid',
                    );

                    throw new DbQueryParametersException($error);

                }

            }

            if(empty($db_config['login'])){

                $error=array(
                    'title'     =>'DB problem with config',
                    'info'      =>'DB login in config is empty',
                );

                throw new DbQueryParametersException($error);

            }

            if(empty($db_config['name'])){

                $error=array(
                    'title'     =>'DB problem with config',
                    'info'      =>'DB name in config is empty',
                );

                throw new DbQueryParametersException($error);

            }

            $login          =$db_config['login'];
            $password       =empty($db_config['password'])?NULL:$db_config['password'];
            $port           =empty($db_config['port'])?Db::$db_port_default_list[$db_config['type']]:$db_config['port'];
            $name           =';dbname='.$db_config['name'];

            $connect_query=$type.':host='.$db_config['host'].';port='.$port.$name;

            try{

                Db::$db_connect_list[$db_config['name']]['connect']=new \PDO($connect_query,$login,$password,$reconnect);

                Db::$db_connect_list[$db_config['name']]['connect']->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);

                return true;

            }
            catch(\PDOException $error){

                $data=array(
                    'title'     =>'PDO connect exception',
                    'info'      =>'PDO cannot make connect'
                );

                throw new DbPdoConnectException($error,$data);

            }

        }

        return false;

    }

}