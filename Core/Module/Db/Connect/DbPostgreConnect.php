<?php

namespace Core\Module\Db\Connect;

use Core\Module\Db\Config\DbPostgreConfig;
use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Php\PhpValidation;
use Core\Module\Worktime\Worktime;

class DbPostgreConnect{

    /**
     * @return bool
     * @throws DbQueryParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Worktime::set_timestamp_point('DataBase Connect Start');

        if(PhpValidation::is_console_running())
            $db_name=\Config::$db_name_server;
        else
            switch($_SERVER['HTTP_HOST']){

                case'whore':
                case'api.whore':{

                    $db_name=\Config::$db_name_local;

                    break;

                }

                case'shluham.net':
                case'api.shluham.net':{

                    $db_name=\Config::$db_name_server;

                    break;

                }

                default:{

                    $error=array(
                        'title'     =>PhpException::$title,
                        'info'      =>'Domain name is mpt exists'
                    );

                    throw new PhpException($error);

                    break;

                }

            }

        $config=DbPostgreConfig::get_config($db_name);

        if(empty($config)){

            $error=array(
                'title'     =>'DB config problem',
                'info'      =>'DB config is empty'
            );

            throw new DbQueryParametersException($error);

        }

        if(!DbConnect::init($config)){

            $error=array(
                'title'     =>'DB connect problem',
                'info'      =>'DB was not connect'
            );

            throw new DbQueryParametersException($error);

        }

        Db::$db_name_default          =$config['name'];
        Db::$db_host_default          =$config['host'];
        Db::$db_login_default         =$config['login'];
        Db::$db_password_default      =$config['password'];

        if(!empty($config['port']))
            Db::$db_port_default=$config['port'];

        Db::set_timezone();

        Worktime::set_timestamp_point('DataBase Connect Done');

        return true;

    }

}