<?php

namespace Core\Module\Db;

use Core\Module\Exception\ParametersException;

class DbIndex{

    /**
     * @param string|NULL $table
     * @param string|NULL $column
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function cidr_index(string $table=NULL,string $column=NULL){

        $error_info_list=[];

        if(empty($table))
            $error_info_list[]='Table is empty';

        if(empty($column))
            $error_info_list[]='Column is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q='CREATE INDEX "'.$table.'_'.$column.'_idx" ON "'.$table.'" USING gist (inet("'.$column.'") inet_ops)';

        return Db::exec($q);

    }

    /**
     * @param string|NULL $table
     * @param string|NULL $column
     * @param string|NULL $name
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function drop_index(string $table=NULL,string $column=NULL,string $name=NULL){

        $error_info_list=[];

        if(empty($table))
            $error_info_list[]='Table is empty';

        if(empty($column))
            $error_info_list[]='Column is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q='DROP INDEX "'.$table.'_'.$column.'_idx"';

        return Db::exec($q,$name);

    }

}