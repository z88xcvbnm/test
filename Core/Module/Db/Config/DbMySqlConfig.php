<?php

namespace Core\Module\Db\Config;

use Core\Module\Exception\PhpException;

class DbMySqlConfig{

    /** @var int */
    public  static $port_default=3306;

    /**
     * @return array
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_list(){

        switch($_SERVER['HTTP_HOST']){

            case 0:
                return[];

            default:{

                $error=array(
                    'title'     =>'System error',
                    'info'      =>'Domain don\'t have config for connect to MySQL'
                );

                throw new PhpException($error);

            }

        }

    }

    /**
     * @param string|NULL $db_name
     * @return mixed
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public static function get_config(string $db_name=NULL){

        $list=self::get_list();

        if(isset($list[$db_name]))
            return $list[$db_name];

        $error=array(
            'title'     =>'System problem',
            'info'      =>'DB name is not exist in access data list'
        );

        throw new PhpException($error);

    }

}