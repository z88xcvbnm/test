<?php

namespace Core\Module\Db\Config\PostgreSQL;

class DbPostgreLocalConfig{

    /**
     * @var array
     */
    public  static $access_data_list=array(
        'whore'=>array(
            'type'                      =>'postgresql',
            'host'                      =>'localhost',
            'login'                     =>'postgres',
            'password'                  =>'postgres',
            'name'                      =>'whore'
        )
    );

}