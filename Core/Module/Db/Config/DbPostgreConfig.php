<?php

namespace Core\Module\Db\Config;

use Core\Module\Db\Config\PostgreSQL\DbPostgreLocalConfig;
use Core\Module\Db\Config\PostgreSQL\DbPostgreServerConfig;
use Core\Module\Exception\PhpException;
use Core\Module\Php\PhpValidation;

class DbPostgreConfig{

    /** @var int */
    public  static $port_default=5432;

    /**
     * @return array
     */
    private static function get_postgre_hecner_config(){

        $data=DbPostgreServerConfig::$access_data_list;

        foreach($data as $key=>$row)
            if(!isset($row['port']))
                $data[$key]['port']=self::$port_default;

        return $data;

    }

    /**
     * @return array
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_list(){

        if(PhpValidation::is_console_running())
            return self::get_postgre_hecner_config();

        switch($_SERVER['HTTP_HOST']){

            case'whore':
            case'api.whore':{

                $data=DbPostgreLocalConfig::$access_data_list;

                foreach($data as $key=>$row)
                    if(!isset($row['port']))
                        $data[$key]['port']=self::$port_default;

                return $data;

                break;

            }

            case'shluham.net':
            case'api.shluham.net':{

                $data=DbPostgreServerConfig::$access_data_list;

                foreach($data as $key=>$row)
                    if(!isset($row['port']))
                        $data[$key]['port']=self::$port_default;

                return $data;

            }

            default:{

                $error=array(
                    'title'     =>'System error',
                    'info'      =>'Domain don\'t have config for connect to PostgreSQL'
                );

                throw new PhpException($error);

            }

        }

    }

    /**
     * @param string|NULL $db_name
     * @return mixed
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public static function get_config(string $db_name=NULL){

        $list=self::get_list();

        if(isset($list[$db_name]))
            return $list[$db_name];

        $error=array(
            'title'     =>'System problem',
            'info'      =>'DB name is not exist in access data list'
        );

        throw new PhpException($error);

    }

}