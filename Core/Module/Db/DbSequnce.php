<?php

namespace Core\Module\Db;

use Core\Module\Exception\ParametersException;

class DbSequnce{

    /**
     * @param string|NULL $table
     * @param int|NULL $ID
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_table_autoincrement(string $table=NULL,int $ID=NULL){

        $error_info_list=[];

        if(empty($table))
            $error_info_list[]='Table is empty';

        if(empty($ID))
            $error_info_list[]='ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        return Db::exec('ALTER SEQUENCE "'.$table.'_id_seq" RESTART WITH '.$ID);

    }

    /**
     * @param string|NULL $table
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_table_autoincrement_max(string $table=NULL){

        if(empty($table)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Table is empty'
            );

            throw new ParametersException($error);

        }

        return Db::exec("SELECT setval('".$table."_id_seq',".'(SELECT MAX("id") FROM "'.$table.'"))');

    }

}