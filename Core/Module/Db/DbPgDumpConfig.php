<?php

namespace Core\Module\Db;

use Core\Module\Url\Url;

class DbPgDumpConfig {

    /** @var string */
    public  static $pg_dump_path                ='\usr\bin\pg_dump';

    /** @var string */
    public  static $pg_dump_for_win_path        ='D:\OpenServer\OpenServer\modules\database\PostgreSQL-9.5-x64\bin\pg_dump';

    /**
     * @var int
     * levels: 1 - 9
     */
    public  static $compress_level              =9;

    /**
     * @return string
     */
    public  static function get_pg_dump_path(){

        switch(Url::$host){

            case'core':
            case'Whore':
                return self::$pg_dump_for_win_path;

            default:
                return self::$pg_dump_path;

        }

    }

}