<?php

namespace Core\Module\Db\DbValueType;

abstract class DbModelValidValue{

    public  static $data=[];
    /** @var string */
    private static $column;
    private static $value;

    private static function isset_column(){

        return isset(static::$data[self::$column]);

    }
    private static function isset_type($type=NULL){

        if(is_array(static::$data[self::$column]['type']))
            return array_search($type,static::$data[self::$column]['type']);
        else
            return ($type==static::$data[self::$column]['type']);

    }
    private static function valid_type(){

        $type=gettype(self::$value);

        switch($type){

            case'integer':
            case'double':
            case'boolean':
            case'string':
            case'array':
            case'NULL':{

                $isset_type=self::isset_type($type);

                return !($isset_type===false);

            }

            default:
                return false;

        }

    }

    private static function is_valid(){

        if(self::isset_column())
            return self::valid_type();

        return false;

    }

    public  static function init(string $column,$value){

        if(empty($column))
            return false;

        self::$column       =$column;
        self::$value        =$value;

        return self::is_valid();

    }

}