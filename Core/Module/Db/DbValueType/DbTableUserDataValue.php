<?php

namespace Core\Module\Db\DbValueType;

class DbTableUserData extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'user_id'           =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'name'              =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'surname'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'sex'               =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>0,
            'max'       =>1
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}