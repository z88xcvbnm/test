<?php

namespace Core\Module\Db\DbValueType;

class DbTableHistoryValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'user_id'           =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'token_id'          =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'session_id'        =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'is_error'          =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'is_success'        =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'get'               =>array(
            'type'      =>array(
                'string',
                'NULL'
            )
        ),
        'post'              =>array(
            'type'      =>array(
                'string',
                'NULL'
            )
        ),
        'header'            =>array(
            'type'      =>array(
                'string',
                'NULL'
            )
        ),
        'response'          =>array(
            'type'      =>array(
                'string',
                'NULL'
            )
        ),
        'error'             =>array(
            'type'      =>array(
                'string',
                'NULL'
            )
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}