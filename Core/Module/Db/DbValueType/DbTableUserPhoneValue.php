<?php

namespace Core\Module\Db\DbValueType;

class DbTableUserPhoneValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'user_id'           =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'country_code'      =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>8
        ),
        'zone_code'         =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>8
        ),
        'number'            =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>16
        ),
        'number_full'       =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>16
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}