<?php

namespace Core\Module\Db\DbValueType;

class DbTableGeoIpValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'geo_id'            =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'country_geo_id'    =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'network'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'        =>32
        ),
        'ip_start_long'     =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'ip_finish_long'     =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'postal_code'       =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'latitude'          =>array(
            'type'      =>array(
                'float',
                'double',
                'NULL'
            ),
            'min'       =>-2147483647,
            'max'       =>2147483647
        ),
        'longitude'         =>array(
            'type'      =>array(
                'float',
                'double',
                'NULL'
            ),
            'min'       =>-2147483647,
            'max'       =>2147483647
        ),
        'radius'            =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>2147483647
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_update'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'              =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}