<?php

namespace Core\Module\Db\DbValueType;

class DbTableContinentLocalizationValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'continent_id'      =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>32768
        ),
        'lang_id'           =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>2147483647
        ),
        'name'              =>array(
            'type'      =>'string',
            'length'    =>64
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}