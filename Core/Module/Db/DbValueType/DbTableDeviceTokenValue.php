<?php

namespace Core\Module\Db\DbValueType;

class DbTableDeviceTokenValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'device_firm_id'    =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'device_model_id'   =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'os_id'             =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'os_version_id'     =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'hash'              =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>255
        ),
        'token'             =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>255
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}