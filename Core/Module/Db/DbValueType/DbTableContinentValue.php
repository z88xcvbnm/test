<?php

namespace Core\Module\Db\DbValueType;

class DbTableContinentValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>32768
        ),
        'code'              =>array(
            'type'      =>'string',
            'length'    =>2
        ),
        'name_ru'           =>array(
            'type'      =>'string',
            'length'    =>64
        ),
        'name_en'           =>array(
            'type'      =>'string',
            'length'    =>64
        ),
        'name_de'           =>array(
            'type'      =>'string',
            'length'    =>64
        ),
        'name_es'           =>array(
            'type'      =>'string',
            'length'    =>64
        ),
        'name_fr'           =>array(
            'type'      =>'string',
            'length'    =>64
        ),
        'name_ja'           =>array(
            'type'      =>'string',
            'length'    =>64
        ),
        'name_pt'           =>array(
            'type'      =>'string',
            'length'    =>64
        ),
        'name_zh'           =>array(
            'type'      =>'string',
            'length'    =>64
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}