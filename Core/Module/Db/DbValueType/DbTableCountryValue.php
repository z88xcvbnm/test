<?php

namespace Core\Module\Db\DbValueType;

class DbTableCountryValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'continent_id'      =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>32768
        ),
        'timezone_id'       =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>2147483648
        ),
        'code'              =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>2
        ),
        'code_full'         =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>3
        ),
        'code_iso'          =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>3
        ),
        'name_ru'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_full_ru'      =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'name_en'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_full_en'      =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'name_de'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_full_de'      =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'name_es'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_full_es'      =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'name_fr'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_full_fr'      =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'name_ja'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_full_ja'      =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'name_pt'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_full_pt'      =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'name_zh'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_full_zh'      =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'latitude'          =>array(
            'type'      =>array(
                'double',
                'NULL'
            ),
            'length'    =>53
        ),
        'langitude'         =>array(
            'type'      =>array(
                'double',
                'NULL'
            ),
            'length'    =>53
        ),
        'radius'            =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'length'    =>9223372036854775807
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}