<?php

namespace Core\Module\Db\DbValueType;

class DbTableAudioValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'user_id'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'file_id'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'duration'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'bitrate'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'hertz'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'file_size'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'file_content_type'=>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'file_extension'=>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>8
        ),
        'public'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'hide'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'date_create'=>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'=>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'=>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}