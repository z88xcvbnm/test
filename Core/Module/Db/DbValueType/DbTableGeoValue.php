<?php

namespace Core\Module\Db\DbValueType;

class DbTableGeoValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'continent_id'      =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>32767
        ),
        'country_id'        =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>2147483647
        ),
        'region_id'         =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'city_id'           =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'timezone_id'       =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>2147483647
        ),
        'postal_code'       =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'latitude'          =>array(
            'type'      =>array(
                'double',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>2147483647
        ),
        'longitude'         =>array(
            'type'      =>array(
                'double',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>2147483647
        ),
        'radius'            =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>2147483647
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_update'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'              =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}