<?php

namespace Core\Module\Db\DbValueType;

class DbTableSessionValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                        =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'user_id'                   =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'token_id'                  =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_update'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_live'         =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}