<?php

namespace Core\Module\Db\DbValueType;

class DbTableUserAccessValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'user_id'           =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'is_root'           =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'is_admin'          =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'is_manager'        =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'is_statistics'     =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'is_financial'      =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'is_bot'            =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'is_app'            =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'is_web'            =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'is_debug'          =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'is_test'           =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'is_guest'          =>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}