<?php

namespace Core\Module\Db\DbValueType;

class DbTableFileValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'user_id'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'hash'=>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>255
        ),
        'chunk_uploaded'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>9223372036854775807
        ),
        'chunk_len'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>9223372036854775807
        ),
        'chunk_size'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>9223372036854775807
        ),
        'file_size'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'file_mime_type'=>array(
            'type'=>array(
                'string',
                'NULL'
            ),
            'length'=>128
        ),
        'file_content_type'=>array(
            'type'=>array(
                'string',
                'NULL'
            ),
            'length'=>255
        ),
        'file_extension'=>array(
            'type'=>array(
                'string',
                'NULL'
            ),
            'length'=>8
        ),
        'date_create'=>array(
            'type'=>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'uploaded'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'preparing'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'prepared'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'date_remove'=>array(
            'type'=>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'=>array(
            'type'=>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}