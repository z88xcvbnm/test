<?php

namespace Core\Module\Db\DbValueType;

class DbTableFileChunkValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'user_id'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'file_id'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'index'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'size'=>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'date_create'=>array(
            'type'=>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'uploaded'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'preparing'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'prepared'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        ),
        'date_remove'=>array(
            'type'=>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'=>array(
            'type'=>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}