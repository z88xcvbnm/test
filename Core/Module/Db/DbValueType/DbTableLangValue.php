<?php

namespace Core\Module\Db\DbValueType;

class DbTableLangValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'code'              =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>3
        ),
        'key'               =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>2
        ),
        'key_full'          =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>3
        ),
        'name_ru'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'name_en'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'name_default'      =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}