<?php

namespace Core\Module\Db\DbValueType;

class DbTableIpValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'ip_address'        =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'ip_long'           =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'length'    =>9223372036854775807
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}