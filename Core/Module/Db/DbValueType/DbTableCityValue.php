<?php

namespace Core\Module\Db\DbValueType;

class DbTableCityValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'continent_id'      =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>32767
        ),
        'country_id'        =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>2147483647
        ),
        'region_id'         =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'timezone_id'       =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>2147483647
        ),
        'name_ru'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_en'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_de'           =>array(
            'type'      =>'string',
            'length'    =>64
        ),
        'name_es'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_fr'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_ja'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_pt'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'name_zh'           =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'postal_code'       =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>64
        ),
        'latitude'          =>array(
            'type'      =>array(
                'float',
                'double',
                'NULL'
            ),
            'min'       =>-2147483647,
            'max'       =>2147483647
        ),
        'longitude'         =>array(
            'type'      =>array(
                'float',
                'double',
                'NULL'
            ),
            'min'       =>-2147483647,
            'max'       =>2147483647
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_update'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}