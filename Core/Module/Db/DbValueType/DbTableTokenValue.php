<?php

namespace Core\Module\Db\DbValueType;

class DbTableTokenValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                        =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'user_id'                   =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'token_id'                  =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'ip_id'                     =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'continent_id'              =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>2147483648
        ),
        'country_id'               =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>2147483648
        ),
        'region_id'                =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'city_id'                  =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'timezone_id'               =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'os_id'                     =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'os_version_id'             =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'browser_id'                =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'browser_version_id'        =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'device_firm_id'            =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'device_model_id'           =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'device_token_id'           =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'app_id'           =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'app_version_id'    =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'useragent_id'              =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'lang_id'                   =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'lang_keyboard_id'          =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'is_keep'                   =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>1
        ),
        'hash'                      =>array(
            'type'      =>'string',
            'length'    =>40
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_update'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_live'         =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}