<?php

namespace Core\Module\Db\DbValueType;

class DbTableUserHashValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'user_id'           =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'is_invite'      =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>0,
            'max'       =>1
        ),
        'is_confirm_registration'=>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>0,
            'max'       =>1
        ),
        'is_recovery_password'=>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>0,
            'max'       =>1
        ),
        'is_email_redirect'=>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>0,
            'max'       =>1
        ),
        'is_promotion'      =>array(
            'type'      =>array(
                'integer',
                'NULL'
            ),
            'min'       =>0,
            'max'       =>1
        ),
        'hash'              =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>40
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_use'          =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}