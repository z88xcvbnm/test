<?php

namespace Core\Module\Db\DbValueType;

class DbTableUserAddressValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'user_id'           =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'continent_id'      =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>32768
        ),
        'country_id'        =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>2147483648
        ),
        'city_id'           =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775808
        ),
        'street_name'       =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>128
        ),
        'home'              =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>32
        ),
        'building'          =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>32
        ),
        'apartment'         =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>32
        ),
        'postcode'          =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>16
        ),
        'date_create'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}