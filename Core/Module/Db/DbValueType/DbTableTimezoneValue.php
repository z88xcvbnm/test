<?php

namespace Core\Module\Db\DbValueType;

class DbTableTimezoneValue extends DbModelValidValue{

    /** @var array */
    public static $data=array(
        'id'                =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'country_id'        =>array(
            'type'      =>'integer',
            'min'       =>1,
            'max'       =>9223372036854775807
        ),
        'country_key'       =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>2
        ),
        'name'              =>array(
            'type'      =>array(
                'string',
                'NULL'
            ),
            'length'    =>2
        ),
        'utc_delta'         =>array(
            'type'      =>'integer',
            'min'       =>-12,
            'max'       =>12
        ),
        'gmt_delta'         =>array(
            'type'      =>'integer',
            'min'       =>-12,
            'max'       =>12
        ),
        'date_remove'       =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'date_recovery'     =>array(
            'type'      =>array(
                'string',
                'function',
                'NULL'
            )
        ),
        'type'=>array(
            'type'      =>'integer',
            'min'       =>0,
            'max'       =>1
        )
    );

}