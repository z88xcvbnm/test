<?php

namespace Core\Module\Db;

use Core\Module\Data\Data;
use Core\Module\Db\Connect\DbPostgreConnect;
use Core\Module\Exception\DbPdoException;
use Core\Module\Exception\DbParametersException;
use Core\Module\Exception\DbQueryParametersException;
use Core\Module\Exception\DbValidationValueException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;

class Db{

    /** @var array */
    public  static $db_connect_list             =[];

    /** @var array  */
    public  static $db_port_default_list        =array(
        'postgresql'        =>5432,
        'mysql'             =>3306
    );

    /** @var string */
    public  static $db_name_default;

    /** @var string */
    public  static $db_host_default;

    /** @var string */
    public  static $db_login_default;

    /** @var string */
    public  static $db_password_default;

    /** @var string */
    public  static $db_port_default;

    /** @var string */
    public  static $db_type;

    /** @var int */
    public  static $limit_default               =10;

    /** @var string */
    public  static $query_last;

    /** @var array */
    public  static $value_last;

    /**
     * @param string|NULL $query
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_empty_query(string $query=NULL){

        if(empty($query)){

            $error=array(
                'title'     =>'DB parameters problem',
                'info'      =>'Query is empty'
            );

            throw new DbParametersException($error);

        }

        return false;

    }

    /**
     * @param string|NULL $name
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_postgresql(string $name=NULL){

        $name       =self::get_db_name($name);
        $db_type    =self::get_db_type($name);

        return $db_type=='postgresql';

    }

    /**
     * @param string|NULL $name
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_mysql(string $name=NULL){

        $name       =self::get_db_name($name);
        $db_type    =self::get_db_type($name);

        return $db_type=='mysql';

    }

    /**
     * @param int|NULL $ID
     * @param string|NULL $table
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_row_ID(int $ID=NULL,string $table=NULL,int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(empty($ID))
            $error_info_list[]='ID is empty';

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'ID'        =>$ID,
                    'table'     =>$table,
                    'name'      =>$name
                )
            );

            throw new ParametersException($error);

        }

        $where_temp_list=array(
            array(
                'column'    =>'id',
                'value'     =>$ID
            )
        );

        $where_list=array_merge($where_temp_list,$where_list);

        if(!is_null($type))
            $where_list[]=array(
                'column'    =>'type',
                'value'     =>$type
            );

        $q=array(
            'select'=>array(
                array(
                    'function'  =>'COUNT(*)',
                    'rename'    =>'len'
                )
            ),
            'table'=>$table,
            'where'=>$where_list,
            'limit'=>1
        );

        $r=Db::select($q,$name);

        if(count($r)==0)
            return false;

        return !empty($r[0]['len']);

    }

    /**
     * @param array $ID_list
     * @param string|NULL $table
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_row_ID_list(array $ID_list=[],string $table=NULL,int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(count($ID_list)==0)
            $error_info_list[]='ID list is empty';

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $list=[];

        foreach($ID_list as $ID)
            if(!empty($ID))
                $list[$ID]=$ID;

        $ID_list=array_keys($list);

        $where_temp_list=array(
            array(
                'column'    =>'id',
                'value'     =>$ID_list
            )
        );

        $where_list=array_merge($where_temp_list,$where_list);

        if(!is_null($type))
            $where_list[]=array(
                'column'    =>'type',
                'value'     =>$type
            );

        $q=array(
            'select'=>array(
                array(
                    'function'  =>'COUNT(*)',
                    'rename'    =>'len'
                )
            ),
            'table'=>$table,
            'where'=>$where_list,
            'limit'=>1
        );

        $r=Db::select($q,$name);

        if(count($r)==0)
            return false;

        return count($ID_list)==$r[0]['len'];

    }

    /**
     * @param string|NULL $table
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_row(string $table=NULL,int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($where_list)==0)
            $error_info_list[]='Where list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'table'     =>$table,
                    'name'      =>$name
                )
            );

            throw new ParametersException($error);

        }

        $where_temp_list    =[];
        $where_list         =array_merge($where_temp_list,$where_list);

        if(!is_null($type)){

            if(Data::is_associative_array($where_list)){

                if(!isset($where_list['type']))
                    $where_list['type']=$type;

            }
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'=>'type',
                    'value'=>$type
                );
            else{

                $error=array(
                    'title'     =>'Systme error',
                    'info'      =>'Where list is not array'
                );

                throw new PhpException($error);

            }

        }

        $q=array(
            'select'=>array(
                array(
                    'function'  =>'COUNT(*)',
                    'rename'    =>'len'
                )
            ),
            'table'=>$table,
            'where'=>$where_list,
            'limit'=>1
        );

        $r=Db::select($q,$name);

        if(count($r)==0)
            return false;

        return $r[0]['len']>0;

    }

    /**
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_select_parameters(array $data=[]){

        if(empty($data['select'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'DB query have not SELECT parameter',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(count($data['select'])==0){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'SELECT have not parameteres',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

    }

    /**
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_set_parameters(array $data=[]){

        if(empty($data['set'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'DB query have not SET parameter',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(count($data['set'])==0){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'SET have not parameteres',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

    }

    /**
     * @param array $data
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_table_parameters(array $data=[]){

        if(empty($data['table'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'DB query have not TABLE parameter',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(Data::is_array($data['table']))
            if(count($data['table'])==0){

                $error=array(
                    'title'     =>'DB query parameters error',
                    'info'      =>'TABLE have not parameteres',
                    'data'      =>$data
                );

                throw new DbQueryParametersException($error);

            }

        return true;

    }

    /**
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_values_parameters(array $data=[]){

        if(empty($data['values'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'DB query have not VALUES parameter',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(count($data['values'])==0){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'VALUES have not parameteres',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

    }

    /**
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_join_parameters(array $data=[]){

        if(empty($data['join'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'DB query have not JOIN parameter',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(count($data['join'])==0){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'JOIN have not parameteres',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

    }

    /**
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_where_parameters(array $data=[]){

        if(empty($data['where'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'DB query have not WHERE parameter',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(count($data['where'])==0){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'WHERE have not parameteres',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

    }

    /**
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_group_parameters(array $data=[]){

        if(empty($data['group'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'DB query have not GROUP parameter',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(count($data['group'])==0){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'GROUP have not parameters',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

    }

    /**
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_order_parameters(array $data=[]){

        if(empty($data['order'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'DB query have not ORDER parameter',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(count($data['order'])==0){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'ORDER have not parameteres',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

    }

    /**
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_limit_parameters(array $data=[]){

        if(empty($data['limit'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'DB query have not limit parameter',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }
        else if(!is_numeric($data['limit'])){

            if(is_array($data['limit'])){

                if(count($data['limit'])!=2){

                    $error=array(
                        'title'     =>'DB query parameters error',
                        'info'      =>'LIMIT must have 2 parameters in array',
                        'data'      =>$data
                    );

                    throw new DbQueryParametersException($error);

                }

            }

        }

    }

    /**
     * @param int|NULL $ID
     * @param string|NULL $table
     * @param int|NULL $type
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_date_update_from_ID(int $ID=NULL,string $table=NULL,int $type=NULL,string $name=NULL){

        $error_info_list=[];

        if(empty($ID))
            $error_info_list[]='ID is empty';

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>$table,
            'set'       =>[
                'date_update'=>'NOW()'
            ],
            'where'     =>[
                'id'        =>$ID
            ],
            'limit'     =>1
        );

        if(is_null($type))
            $q['where']['type']=0;

        return Db::update($q,$name);

    }

    /**
     * @param int|NULL $ID
     * @param string|NULL $table
     * @param array $set_list
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_data_from_ID(int $ID=NULL,string $table=NULL,array $set_list=[],int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(empty($ID))
            $error_info_list[]='ID is empty';

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($set_list)==0)
            $error_info_list[]='Set list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'ID'            =>$ID,
                    'table'         =>$table,
                    'column_list'   =>$set_list,
                    'name'          =>$name
                )
            );

            throw new ParametersException($error);

        }

        $update_set_list=[];

        if(Data::is_associative_array($where_list))
            $where_temp_list['id']=$ID;
        else if(Data::is_array($where_list))
            $where_temp_list[]=array(
                'column'    =>'id',
                'value'     =>$ID
            );
        else{

            $error=array(
                'title'     =>'Systme error',
                'info'      =>'Where list is not array'
            );

            throw new PhpException($error);

        }

        $where_list=array_merge($where_temp_list,$where_list);

        if(!is_null($type)){

            if(Data::is_associative_array($where_list))
                $where_list['type']=$type;
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'    =>'type',
                    'value'     =>$type
                );
            else{

                $error=array(
                    'title'     =>'Systme error',
                    'info'      =>'Where list is not array'
                );

                throw new PhpException($error);

            }

        }

        foreach($set_list as $column_name=>$value)
            $update_set_list[$column_name]=array(
                'column'    =>$column_name,
                'value'     =>$value
            );

        $q=array(
            'table'     =>$table,
            'set'       =>array_values($update_set_list),
            'where'     =>$where_list,
            'limit'     =>1
        );

        return Db::update($q,$name);

    }

    /**
     * @param string|NULL $table
     * @param array $set_list
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_data_from_where_list(string $table=NULL,array $set_list=[],int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($set_list)==0)
            $error_info_list[]='Set list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'table'         =>$table,
                    'column_list'   =>$set_list,
                    'name'          =>$name
                )
            );

            throw new ParametersException($error);

        }

        $update_set_list=[];

        if(!is_null($type)){

            if(Data::is_associative_array($where_list))
                $where_list['type']=$type;
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'    =>'type',
                    'value'     =>$type
                );
            else{

                $error=array(
                    'title'     =>'Systme error',
                    'info'      =>'Where list is not array'
                );

                throw new PhpException($error);

            }

        }

        foreach($set_list as $column_name=>$value)
            $update_set_list[$column_name]=array(
                'column'    =>$column_name,
                'value'     =>$value
            );

        $q=array(
            'table'     =>$table,
            'set'       =>$update_set_list,
            'where'     =>$where_list
        );

        return Db::update($q,$name);

    }

    /**
     * @param array $ID_list
     * @param string|NULL $table
     * @param array $set_list
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_data_from_ID_list(array $ID_list=[],string $table=NULL,array $set_list=[],int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(count($ID_list)==0)
            $error_info_list[]='ID list is empty';

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($set_list)==0)
            $error_info_list[]='Set list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'ID_list'       =>$ID_list,
                    'table'         =>$table,
                    'column_list'   =>$set_list,
                    'name'          =>$name
                )
            );

            throw new ParametersException($error);

        }

        $update_set_list    =[];
        $where_temp_list    =[];

        if(Data::is_associative_array($where_list))
            $where_temp_list['id']=$ID_list;
        else if(Data::is_array($where_list))
            $where_temp_list[]=array(
                'column'    =>'id',
                'value'     =>$ID_list
            );
        else{

            $error=array(
                'title'     =>'Systme error',
                'info'      =>'Where list is not array'
            );

            throw new PhpException($error);

        }

        $where_list=array_merge($where_temp_list,$where_list);

        if(!is_null($type)){

            if(Data::is_associative_array($where_list))
                $where_list['type']=$type;
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'    =>'type',
                    'value'     =>$type
                );
            else{

                $error=array(
                    'title'     =>'Systme error',
                    'info'      =>'Where list is not array'
                );

                throw new PhpException($error);

            }

        }

        foreach($set_list as $column_name=>$value)
            $update_set_list[$column_name]=array(
                'column'    =>$column_name,
                'value'     =>$value
            );

        $q=array(
            'table'     =>$table,
            'set'       =>$update_set_list,
            'where'     =>$where_list,
            'limit'     =>1
        );

        return Db::update($q,$name);

    }

    /**
     * @param int|NULL $ID
     * @param string|NULL $table
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function delete_from_ID(int $ID=NULL,string $table=NULL,int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(empty($ID))
            $error_info_list[]='ID is empty';

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'ID'            =>$ID,
                    'table'         =>$table,
                    'name'          =>$name
                )
            );

            throw new ParametersException($error);

        }

        $update_set_list=array(
            'date_update'       =>'NOW()',
            'date_remove'       =>'NOW()',
            'type'              =>1
        );

        $where_temp_list=[];

        if(Data::is_associative_array($where_list))
            $where_temp_list['id']=$ID;
        else if(Data::is_array($where_list))
            $where_temp_list[]=array(
                'column'    =>'id',
                'value'     =>$ID
            );
        else{

            $error=array(
                'title'     =>'Systme error',
                'info'      =>'Where list is not array'
            );

            throw new PhpException($error);

        }

        $where_list=array_merge($where_temp_list,$where_list);

        if(!is_null($type)){

            if(Data::is_associative_array($where_list))
                $where_list['type']=$type;
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'    =>'type',
                    'value'     =>$type
                );
            else{

                $error=array(
                    'title'     =>'Systme error',
                    'info'      =>'Where list is not array'
                );

                throw new PhpException($error);

            }

        }

        $q=array(
            'table' =>$table,
            'set'   =>$update_set_list,
            'where' =>$where_list
        );

        return Db::update($q,$name);

    }

    /**
     * @param string|NULL $table
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function delete_from_where_list(string $table=NULL,int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'table'         =>$table,
                    'name'          =>$name
                )
            );

            throw new ParametersException($error);

        }

        $update_set_list=array(
            'date_update'       =>'NOW()',
            'date_remove'       =>'NOW()',
            'type'              =>1
        );

        if(!is_null($type)){

            if(Data::is_associative_array($where_list))
                $where_list['type']=$type;
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'    =>'type',
                    'value'     =>$type
                );
            else{

                $error=array(
                    'title'     =>'Systme error',
                    'info'      =>'Where list is not array'
                );

                throw new PhpException($error);

            }

        }

        $q=array(
            'table' =>$table,
            'set'   =>$update_set_list,
            'where' =>$where_list
        );

        return Db::update($q,$name);

    }

    /**
     * @param array $ID_list
     * @param string|NULL $table
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function delete_from_ID_list(array $ID_list=[],string $table=NULL,int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(count($ID_list)==0)
            $error_info_list[]='ID list is empty';

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_temp_list=[];

        $update_set_list=array(
            'date_update'       =>'NOW()',
            'date_remove'       =>'NOW()',
            'type'              =>1
        );

        if(Data::is_associative_array($where_list))
            $where_temp_list['id']=$ID_list;
        else if(Data::is_array($where_list))
            $where_temp_list[]=array(
                'column'    =>'id',
                'value'     =>$ID_list
            );
        else{

            $error=array(
                'title'     =>'Systme error',
                'info'      =>'Where list is not array'
            );

            throw new PhpException($error);

        }

        $where_list=array_merge($where_temp_list,$where_list);

        if(!is_null($type)){

            if(Data::is_associative_array($where_list))
                $where_list['type']=$type;
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'    =>'type',
                    'value'     =>$type
                );
            else{

                $error=array(
                    'title'     =>'Systme error',
                    'info'      =>'Where list is not array'
                );

                throw new PhpException($error);

            }

        }

        $q=array(
            'table'     =>$table,
            'set'       =>$update_set_list,
            'where'     =>$where_list
        );

        return Db::update($q,$name);

    }

    /**
     * @param string|NULL $table
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return int
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_row_len(string $table=NULL,int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(empty($table))
            $error_info_list[]='Table name is empty';

//        if(count($where_list)==0)
//            $error_info_list[]='Where list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_temp_list    =[];
        $where_list         =array_merge($where_temp_list,$where_list);

        if(!is_null($type)){

            if(Data::is_associative_array($where_list)){

                if(!isset($where_list['type']))
                    $where_list['type']=$type;

            }
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'    =>'type',
                    'value'     =>$type
                );

        }

        $q=array(
            'select'=>array(
                array(
                    'function'  =>'COUNT(*)',
                    'rename'    =>'len'
                )
            ),
            'table'=>$table,
            'where'=>$where_list,
        );

        $r=Db::select($q,$name);

        if(count($r)==0)
            return 0;

        return $r[0]['len'];

    }

    /**
     * @param string|NULL $table
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return int
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_row_len_id(string $table=NULL,int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(empty($table))
            $error_info_list[]='Table name is empty';

//        if(count($where_list)==0)
//            $error_info_list[]='Where list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_temp_list    =[];
        $where_list         =array_merge($where_temp_list,$where_list);

        if(!is_null($type)){

            if(Data::is_associative_array($where_list)){

                if(!isset($where_list['type']))
                    $where_list['type']=$type;

            }
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'    =>'type',
                    'value'     =>$type
                );

        }

        $q=array(
            'select'=>array(
                array(
                    'function'  =>'COUNT(id)',
                    'rename'    =>'len'
                )
            ),
            'table'=>$table,
            'where'=>$where_list,
        );

        $r=Db::select($q,$name);

        if(count($r)==0)
            return 0;

        return $r[0]['len'];

    }

    /**
     * @param string|NULL $table
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return null
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_row_ID(string $table=NULL,int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($where_list)==0)
            $error_info_list[]='Where list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'table'     =>$table,
                    'name'      =>$name
                )
            );

            throw new ParametersException($error);

        }

        $where_temp_list    =[];
        $where_list         =array_merge($where_temp_list,$where_list);

        if(!is_null($type)){

            if(Data::is_associative_array($where_list)){

                if(!isset($where_list['type']))
                    $where_list['type']=$type;

            }
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'=>'type',
                    'value'=>$type
                );

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>$table,
            'where'=>$where_list,
            'limit'=>1
        );

        $r=Db::select($q,$name);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param string|NULL $table
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return array
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_row_ID_list(string $table=NULL,int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($where_list)==0)
            $error_info_list[]='Where list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'table'     =>$table,
                    'name'      =>$name
                )
            );

            throw new ParametersException($error);

        }

        $where_temp_list    =[];
        $where_list         =array_merge($where_temp_list,$where_list);

        if(!is_null($type)){

            if(Data::is_associative_array($where_list)){

                if(!isset($where_list['type']))
                    $where_list['type']=$type;

            }
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'    =>'type',
                    'value' =>$type
                );
            else{

                $error=array(
                    'title'     =>'Systme error',
                    'info'      =>'Where list is not array'
                );

                throw new PhpException($error);

            }

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>$table,
            'where'=>$where_list
        );

        $r=Db::select($q,$name);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['id'];

        return $list;

    }

    /**
     * @param int|NULL $ID
     * @param string|NULL $table
     * @param array $column_list
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return mixed|null
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_data_from_ID(int $ID=NULL,string $table=NULL,array $column_list=[],int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(empty($ID))
            $error_info_list[]='ID is empty';

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($column_list)==0)
            $error_info_list[]='Column name list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'ID'            =>$ID,
                    'table'         =>$table,
                    'column_list'   =>$column_list,
                    'name'          =>$name
                )
            );

            throw new ParametersException($error);

        }

        $select_list        =[];
        $where_temp_list    =[];

        if(Data::is_associative_array($where_list))
            $where_temp_list['id']=$ID;
        else if(Data::is_array($where_list))
            $where_temp_list[]=array(
                'column'    =>'id',
                'value'     =>$ID
            );
        else{

            $error=array(
                'title'     =>PhpException::$title,
                'info'      =>'Where list is not array'
            );

            throw new PhpException($error);

        }

        $where_list=array_merge($where_temp_list,$where_list);

        if(!is_null($type)){

            if(Data::is_associative_array($where_list)){

                if(!isset($where_list['type']))
                    $where_list['type']=$type;

            }
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'    =>'type',
                    'value'     =>$type
                );
            else{

                $error=array(
                    'title'     =>'Systme error',
                    'info'      =>'Where list is not array'
                );

                throw new PhpException($error);

            }

        }

        foreach($column_list as $column_name)
            $select_list[]=$column_name;

        $q=array(
            'select'=>$select_list,
            'table'=>$table,
            'where'=>$where_list,
            'limit'=>1
        );

        $r=Db::select($q,$name);

        if(count($r)==0)
            return NULL;

        return $r[0];

    }

    /**
     * @param array $ID_list
     * @param string|NULL $table
     * @param array $column_list
     * @param int|NULL $type
     * @param array $where_list
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_data_from_ID_list(array $ID_list=[],string $table=NULL,array $column_list=[],int $type=NULL,array $where_list=[],string $name=NULL){

        $error_info_list=[];

        if(count($ID_list)==0)
            $error_info_list[]='ID list is empty';

        if(empty($table))
            $error_info_list[]='Table name is empty';

        if(count($column_list)==0)
            $error_info_list[]='Column name list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'ID_list'       =>$ID_list,
                    'table'         =>$table,
                    'column_list'   =>$column_list,
                    'name'          =>$name
                )
            );

            throw new ParametersException($error);

        }

        $select_list        =array(
            array(
                'column'=>'id'
            )
        );
        $where_temp_list    =array(
            array(
                'column'=>'id',
                'method'=>'IN',
                'value'=>$ID_list
            )
        );

        $where_list=array_merge($where_temp_list,$where_list);

        if(!is_null($type)){

            if(Data::is_associative_array($where_list)){

                if(!isset($where_list['type']))
                    $where_list['type']=$type;

            }
            else if(Data::is_array($where_list))
                $where_list[]=array(
                    'column'    =>'type',
                    'value'     =>$type
                );
            else{

                $error=array(
                    'title'     =>'Systme error',
                    'info'      =>'Where list is not array'
                );

                throw new PhpException($error);

            }

        }

        foreach($column_list as $column_name)
            $select_list[$column_name]=array(
                'column'=>$column_name
            );

        foreach($column_list as $column_name)
            $select_list[$column_name]=array(
                'column'=>$column_name
            );

        $q=array(
            'select'=>$select_list,
            'table'=>$table,
            'where'=>$where_list,
            'limit'=>count($ID_list)
        );

        $r=Db::select($q,$name);

        return $r;

    }

    /**
     * @param string|NULL $name
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_db_type(string $name=NULL){

        $name=self::get_db_name($name);

        if(empty(self::$db_connect_list[$name])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'DB name is not exists in connect list',
                'data'      =>array(
                    'name'      =>$name
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$db_connect_list[$name]['type'])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'DB type is not exists in connect list',
                'data'      =>array(
                    'name'      =>$name
                )
            );

            throw new ParametersException($error);

        }

        return self::$db_connect_list[$name]['type'];

    }

    /**
     * @return string
     */
    public  static function get_db_name_default(){

        return self::$db_name_default;

    }

    /**
     * @return string
     */
    public  static function get_db_host_default(){

        return self::$db_host_default;

    }

    /**
     * @return array
     */
    public  static function get_db_name_connected_list(){

        return array_keys(self::$db_connect_list);

    }

    /**
     * @param string|NULL $name
     * @return string
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_db_name(string $name=NULL){

        if(empty($name)){

            if(empty(self::$db_name_default)){

                $error=array(
                    'title'     =>'DB parameters problem',
                    'info'      =>'DB name is empty'
                );

                throw new DbParametersException($error);

            }
            else
                return self::$db_name_default;

        }

        return $name;

    }

    /**
     * @param string|NULL $query
     * @param string|NULL $name
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_query(string $query=NULL,string $name=NULL){

        self::is_empty_query($query);

        $name=self::get_db_name($name);

        try{

            return self::$db_connect_list[$name]['connect']->query($query);

        }
        catch(\PDOException $error){

            $data=array(
                'title'     =>'PDO Exception',
                'info'      =>'PDO cannot make query',
                'data'      =>array(
                    'db_name'   =>$name,
                    'query'     =>$query
                )
            );

            throw new DbPdoException($error,$data);

        }

    }

    /**
     * @param string|NULL $query
     * @param string|NULL $name
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_results(string $query=NULL,string $name=NULL){

        self::is_empty_query($query);

        $name       =self::get_db_name($name);
        $result     =self::get_query($query,$name);

        try{

            return $result->fetchAll(\PDO::FETCH_ASSOC);

        }
        catch(\PDOException $error){

            $data=array(
                'title'     =>'PDO Exception',
                'data'      =>array(
                    'db_name'   =>$name,
                    'query'     =>$query
                )
            );

            throw new DbPdoException($error,$data);

        }

    }

    /**
     * @param string|NULL $query
     * @param string|NULL $name
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_result(string $query=NULL,string $name=NULL){

        self::is_empty_query($query);

        $name       =self::get_db_name($name);
        $result     =self::get_query($query,$name);

        try{

            return $result->fetch(\PDO::FETCH_ASSOC);

        }
        catch(\PDOException $error){

            $data=array(
                'title'     =>'DB bad query in get_result',
                'db_name'   =>$name,
                'query'     =>$query
            );

            throw new DbPdoException($error,$data);

        }

    }

    /**
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_insert_id(string $name=NULL){

        if(self::is_mysql($name)){

            $query='SELECT LAST_INSERT_ID() as `ID`';

            return self::prepare('select',$query,[],false,$name);

        }
        else if(self::is_postgresql($name)){

            try{

                return self::$db_connect_list[$name]['connect']->lastInsertId();

            }
            catch(\PDOException $error){

                $data=array(
                    'title'     =>'PDO Exception',
                    'info'      =>'Problem with last insert ID',
                    'data'      =>array(
                        'db_name'   =>$name
                    )
                );

                throw new DbPdoException($error,$data);

            }

        }

        return false;

    }

    /**
     * @param string|NULL $query
     * @param string|NULL $name
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_quote_string(string $query=NULL,string $name=NULL){

        self::is_empty_query($query);

        $name=self::get_db_name($name);

        try{

            return self::$db_connect_list[$name]['connect']->quote($query);

        }
        catch(\PDOException $error){

            $data=array(
                'title'     =>'PDO Exception',
                'info'      =>'PDO cannot make quote string',
                'data'      =>array(
                    'db_name'   =>$name,
                    'query'     =>$query
                )
            );

            throw new DbPdoException($error,$data);

        }

    }

    /**
     * @param $value
     * @return int
     */
    public  static function get_parameters_type($value){

        if(is_null($value))
            return \PDO::PARAM_NULL;

        if(is_string($value))
            switch($value){

                case'NULL':
                case'null':
                    return \PDO::PARAM_NULL;

            }

        $type=gettype($value);

        switch($type){

            case'boolean':{

                return \PDO::PARAM_BOOL;

                break;

            }

            case'integer':{

                return \PDO::PARAM_INT;

                break;

            }

            case'function':
            case'string':{

                return \PDO::PARAM_STR;

                break;

            }

            default:{

                return \PDO::PARAM_STR;

                break;

            }

        }

    }

    /**
     * @param string|NULL $name
     * @return string
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_quote_column(string $name=NULL){

        $name       =self::get_db_name($name);
        $db_type    =self::get_db_type($name);

        switch($db_type){

            case'mysql':
                return'`';

            case'postgresql':
                return'"';

            default:{

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>'DB type is not exists in connect list',
                    'data'      =>array(
                        'name'      =>$name
                    )
                );

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @param string|NULL $name
     * @return string
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_quote_value(string $name=NULL){

        $name       =self::get_db_name($name);
        $db_type    =self::get_db_type($name);

        switch($db_type){

            case'postgresql':
            case'mysql':
                return"'";

            default:{

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>'DB type is not exists in connect list',
                    'data'      =>array(
                        'name'      =>$name
                    )
                );

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return array
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_join(array $data=[],string $name=NULL){

        if(empty($data['join'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'DB query array have not JOIN key',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(!is_array($data['join'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of JOIN is not array',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        $join               ='';
        $value_list         =[];

        $name               =self::get_db_name($name);
        $column_quote       =self::get_quote_column($name);

        foreach($data['join'] as $row)
            if(!empty($row['table'])){

                $type       =empty($row['type'])?'INNER JOIN':(strtoupper($row['type']).' JOIN');
                $rename     =empty($row['rename'])?'':$row['rename'];
                $join_add   =NULL;

                list($where,$value_add_list)=self::get_where($row,$name);

                $value_list=array_merge($value_add_list,$value_list);

                if(!empty($row['join'])){

                    list($join_add,$value_add_list)=self::get_join($row,$name);

                    $value_list=array_merge($value_add_list,$value_list);

                }

                if(empty($join_add))
                    $join.=$type.' '.$column_quote.$row['table'].$column_quote.(empty($rename)?'':(' AS '.$column_quote.$rename.$column_quote)).' ON('.$where.')';
                else
                    $join.=$type.' ('.$column_quote.$row['table'].$column_quote.(empty($rename)?'':(' AS '.$column_quote.$rename.$column_quote)).' '.$join_add.')ON('.$where.')';

            }

        return array(
                (empty($join)?'':' ').$join,
                $value_list
            );

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return string
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_order(array $data=[],string $name=NULL){

        if(!is_array($data['order'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of ORDER is not array',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        $order_list     =[];
        $quote          =self::get_quote_column($name);
        $name           =self::get_db_name($name);

        foreach($data['order'] as $row)
            if(Data::is_associative_array($row)){

                $temp=self::get_column_name($row,false,$name);

                if(!empty($row['direction']))
                    switch(strtolower($row['direction'])){

                        case'desc':
                        case'asc':{

                            $temp.=' '.strtoupper($row['direction']);

                            break;

                        }

                    }

                $order_list[]=$temp;

            }
            else if(Data::is_string($row))
                $order_list[]=$quote.$row.$quote;

        return count($order_list)==0?'':implode(', ',$order_list);

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return string
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_group(array $data=[],string $name=NULL){

        if(!is_array($data['group'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of GROUP is not array'
            );

            throw new DbQueryParametersException($error);

        }

        $group_list =[];
        $quote      =self::get_quote_column($name);

        foreach($data['group'] as $row)
            if(Data::is_associative_array($row))
                $group_list[]=self::get_column_name($row,false,$name);
            else if(Data::is_string($row))
                $group_list[]=$quote.$row.$quote;

        return count($group_list)==0?'':implode(', ',$group_list);

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return int|mixed|string
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_limit(array $data=[],string $name=NULL){

        if(is_integer($data['limit']))
            return $data['limit'];

        if(empty($data['limit']))
            return '';

        if(!is_array($data['limit'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of LIMIT is not array',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(count($data['limit'])==0){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of LIMIT have not parameters',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(count($data['limit'])==1)
            return $data['limit'][0];

        if(count($data['limit'])==2){

            if(self::is_mysql($name))
                return implode(', ',array_values($data['limit']));
            else if(self::is_postgresql($name))
                return $data['limit'][1].' OFFSET '.$data['limit'][0];
            else{

                $error=array(
                    'title'     =>'DB query parameters error',
                    'info'      =>'DB type is not valid',
                    'data'      =>$data
                );

                throw new DbQueryParametersException($error);

            }

        }
        else{

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of LIMIT array have more 2 parameters',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return string
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_duplicate(array $data=[],string $name=NULL){

        if(empty($data['duplicate']))
            return '';

        if(!is_array($data['duplicate'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of DUPLICATE is not array',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(count($data['duplicate'])==0){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of DUPLICATE have not parameters',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(self::is_mysql($name))
            return'';
        else if(self::is_postgresql($name)){

            switch($data['duplicate']['type']){

                case'ignore':
                    return 'ON CONFLICT ("id") DO NOTHING';

                default:
                    return'';

            }

        }
        else{

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'DB type is not valid',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return string
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_select(array $data=[],string $name=NULL){

        if(!is_array($data['select'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of SELECT is not array',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        $select_list    =[];
        $quote          =self::get_quote_column($name);

        foreach($data['select'] as $row)
            if(!empty($row)){

                if(Data::is_associative_array($row)){

                    if(!empty($row['column'])||!empty($row['function']))
                        $select_list[]=self::get_column_name($row,false,$name);
                    else{

                        $error=array(
                            'title'     =>'DB query parameters error',
                            'info'      =>'Select column is empty',
                            'data'      =>$data
                        );

                        throw new DbQueryParametersException($error);

                    }

                }
                else if(is_string($row))
                    $select_list[]=$quote.$row.$quote;
                else{

                    $error=array(
                        'title'     =>'DB query parameters error',
                        'info'      =>'Select row have not valid type',
                        'data'      =>$data
                    );

                    throw new DbQueryParametersException($error);

                }

            }
            else{

                $error=array(
                    'title'     =>'DB query parameters error',
                    'info'      =>'Select row is empty',
                    'data'      =>$data
                );

                throw new DbQueryParametersException($error);

            }

        return implode(', ',$select_list);

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return array
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_set(array $data=[],string $name=NULL){

        if(!is_array($data['set'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of SET is not array',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        $set_list       =[];
        $value_list     =[];
        $name           =self::get_db_name($name);
        $column_quote   =self::get_quote_column($name);

        if(Data::is_associative_array($data['set'])){

            foreach($data['set'] as $key=>$value){

                $column=$column_quote.$key.$column_quote;

                if(Data::is_associative_array($value)){

                    if(!empty($value['function']))
                        $set_list[]=$column.' = '.$value['function'];
                    else if(!empty($value['value'])){

                        $set_list[]     =$column.' = ?';
                        $value_list[]   =$value['value'];

                    }

                }
                else{

                    $set_list[]     =$column.' = ?';
                    $value_list[]   =$value;

                }

            }

        }
        else if(Data::is_array($data['set'])){

            foreach($data['set'] as $row)
                if(!empty($row['column'])){

//                    $table          =empty($row['table'])?'':$row['table'];
                    $column         =self::get_column_name($row);
//                    $set_list[]     =$column.' = ?';
//                    $set_list[]     =(empty($table)?'':($column_quote.$table.$column_quote.'.')).$column_quote.$row['column'].$column_quote.' = ?';


                    if(!empty($row['function']))
                        $set_list[]=$column.' = '.$row['function'];
                    else if(!empty($row['value'])){

                        if(Data::is_associative_array($row['value'])){

                            if(!empty($row['value']['function']))
                                $set_list[]=$column.' = '.$row['value']['function'];

                        }
                        else{

                            $set_list[]     =$column.' = ?';
                            $value_list[]   =$row['value'];

                        }

                    }

                }
                else if(!empty($row['update'])){

                    list($select_query,$value_list_add)=self::get_select_query($row,$name);

                    $set_list[]     ='('.$column_quote.implode($column_quote.','.$column_quote,$row['update']).$column_quote.') = ('.$select_query.')';
                    $value_list     =array_merge($value_list,$value_list_add);

                }
                else if(!empty($row['function']))
                    $set_list[]=$row['function'];

        }

        return array(
            implode(', ',$set_list),
            $value_list
        );

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return array
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_having(array $data=[],string $name=NULL){

        if(empty($data['having'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Having is empty',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(!is_array($data['having'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of having is not array',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        return self::get_where_from_key('having',$data,$name);
        
    }

    /**
     * @param string|NULL $key
     * @param array $data
     * @param string|NULL $name
     * @return array
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_where_from_key(string $key=NULL,array $data=[],string $name=NULL){

        if($key!='where'&&$key!='having'){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Key is not valid',
                'data'      =>array(
                    'key'       =>$key,
                    'data'      =>$data
                )
            );

            throw new DbQueryParametersException($error);

        }

        if(empty($data[$key])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Where is empty',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(!is_array($data[$key])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of '.$key.' is not array',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        $where          ='';
        $value_list     =[];
        $name           =self::get_db_name($name);
        $column_quote   =self::get_quote_column($name);

        if(Data::is_associative_array($data[$key])){

            foreach($data[$key] as $column=>$value){

                $column=$column_quote.$column.$column_quote;

                if(isset($value['transform']))
                    switch($value['transform']){

                        case'lower':{

                            $column='LOWER('.$column.')';

                            break;

                        }

                    }

                if(is_null($value))
                    $where.=(empty($where)?'':(' AND ')).$column.' IS NULL';
                else{
                    
                    if(Data::is_associative_array($value)){

                        $union=empty($value['union'])?'AND':strtoupper($value['union']);
                        
                        if(!empty($value['method'])){

                            if(Data::is_array($value['value']))
                                if($value['method']=='!='||$value['method']=='<>')
                                    $value['method']='not in';

                            switch($value['method']){

                                case'in':
                                case'not in':
                                case'any':{

                                    if(isset($value['value'])){
    
                                        if(is_array($value['value'])){
    
                                            if(count($value['value'])==0)
                                                return[];
                                            else
                                                $where.=(empty($where)?'':(' '.$union.' ')).$column.' '.strtoupper($value['method']).'('.implode(', ',array_fill(0,count($value['value']),'?')).')';

                                            $value_list=array_merge($value_list,$value['value']);
    
                                        }
                                        else{
    
                                            $where          .=(empty($where)?'':(' '.$union.' ')).$column.' = ?';
                                            $value_list[]   =$value['value'];
    
                                        }
    
                                    }
    
                                    break;
    
                                }
    
                                case'between':{
    
                                    if(!is_array($value['column'])){
    
                                        $error=array(
                                            'title'=>'DB query parameters exception',
                                            'info'=>'Value is not valid',
                                            'data'=>array(
                                                'column'    =>$value['column'],
                                                'method'    =>$value['method'],
                                                'value'     =>$value['value']
                                            )
                                        );
    
                                        throw new DbQueryParametersException($error);
    
                                    }
    
                                    if(count($value['column'])!=2){
    
                                        $error=array(
                                            'title'=>'DB query parameters exception',
                                            'info'=>'Value list length is not valid',
                                            'data'=>array(
                                                'column'    =>$value['column'],
                                                'method'    =>$value['method'],
                                                'value'     =>$value['value']
                                            )
                                        );
    
                                        throw new DbQueryParametersException($error);
    
                                    }
    
                                    $where  .=(empty($where)?'':(' '.$union.' ')).' ? BETWEEN ';
                                    $where  .=(empty($value['table_join'])?'':($column_quote.$value['table_join'].$column_quote.'.')).$column_quote.$value['column'][0].$column_quote;
                                    $where  .=' AND '.(empty($value['table_join'])?'':($column_quote.$value['table_join'].$column_quote.'.')).$column_quote.$value['column'][1].$column_quote;
    
                                    $value_list[]=$value['value'];
    
                                    break;
    
                                }
    
                                case'=':
                                case'!=':
                                case'<':
                                case'>':
                                case'<=':
                                case'>=':
                                case'>>=':
                                case'<>':{
    
                                    if(!empty($value['column_join']))
                                        $where.=(empty($where)?'':(' '.$union.' ')).$column.' '.strtoupper($value['method']).' '.(empty($value['table_join'])?'':($column_quote.$value['table_join'].$column_quote.'.')).$column_quote.$value['column_join'].$column_quote;
                                    else if(!empty($value['function']))
                                        $where.=(empty($where)?'':(' '.$union.' ')).$column.'" '.strtoupper($value['method']).' '.$value['function'];
                                    else if(is_null($value['value']))
                                        $where.=(empty($where)?'':(' '.$union.' ')).$column.' IS'.($value['method']=='!='||$value['method']=='<>'?' NOT':'').' NULL';
                                    else{

                                        $where          .=(empty($where)?'':(' '.$union.' ')).$column.' '.$value['method'].' ?';
                                        $value_list[]   =$value['value'];
    
                                    }
    
                                    break;
    
                                }
    
                                case'like':
                                case'unlike':{
    
                                    if(isset($value['value'])){
    
                                        $where          .=(empty($where)?'':(' '.$union.' ')).$column.' '.strtoupper($value['method']).' ?';
                                        $value_list[]   ='%'.$value['value'].'%';
    
                                    }
    
                                    break;
    
                                }
    
                                default:{
    
                                    $error=array(
                                        'title'     =>'DB query parameters error',
                                        'info'      =>'Query parameters in where list have unknown method',
                                        'row'       =>$value,
                                        'data'      =>$data
                                    );
    
                                    throw new DbQueryParametersException($error);
    
                                    break;
    
                                }
    
                            }
                            
                        }
                        else if(!empty($value['function'])){

                            $where.=(empty($where)?'':(' '.$union.' ')).$column.' '.$value['function'];

                        }
                        else{

                            $value['method']='=';

                            if(!empty($value['column_join']))
                                $where.=(empty($where)?'':(' '.$union.' ')).$column.' '.strtoupper($value['method']).' '.(empty($value['table_join'])?'':($column_quote.$value['table_join'].$column_quote.'.')).$column_quote.$value['column_join'].$column_quote;
                            else if(!empty($value['function']))
                                $where.=(empty($where)?'':(' '.$union.' ')).$column.'" '.strtoupper($value['method']).' '.$value['function'];
                            else if(is_null($value['value']))
                                $where.=(empty($where)?'':(' '.$union.' ')).$column.' IS'.($value['method']=='!='||$value['method']=='<>'?' NOT':'').' NULL';
                            else{

                                $where          .=(empty($where)?'':(' '.$union.' ')).$column.' '.$value['method'].' ?';
                                $value_list[]   =$value['value'];

                            }

                        }
                        
                    }
                    else if(Data::is_array($value)){

                        $where          .=(empty($where)?'':(' AND ')).$column.' IN ('.implode(', ',array_fill(0,count($value),'?')).')';
                        $value_list     =array_merge($value_list,$value);

                    }
                    else{

                        $where          .=(empty($where)?'':(' AND ')).$column.' = ?';
                        $value_list[]   =$value;

                    }

                }

            }

        }
        else if(Data::is_array($data[$key])){

            foreach($data[$key] as $row)
                if(Data::is_array($row)){

                    $union=empty($row['union'])?'AND':strtoupper($row['union']);

                    if(isset($row['where'])){

                        list($where_add,$value_add_list)=self::get_where_from_key($key,$row,$name);

//                        $where          .=(empty($where)?'':(' '.$union.' (')).$where_add.(empty($where)?'':')');

                        $where          .=(empty($where)?'':(' '.$union.' '))
                                            .'('.$where_add.')';
                        $value_list     =array_merge($value_list,$value_add_list);

                    }
                    else if(!empty($row['column'])){

                        if(empty($row['method'])){

                            if(isset($row['value']))
                                $method=Data::is_array($row['value'])?(count($row['value'])>0?'in':'='):'=';
                            else
                                $method='=';

                        }
                        else
                            $method=strtolower($row['method']);

                        if(!is_array($row['column']))
                            $column=self::get_column_name($row,false,$name);
                        else
                            $column=$row['column'];

                        switch($method){

                            case'in':
                            case'not in':
                            case'any':{

                                if(isset($row['value'])){

                                    if(is_array($row['value'])){

                                        if(count($row['value'])==0)
                                            return[];
                                        else if(count($row['value'])==1)
                                            $where.=(empty($where)?'':(' '.$union.' ')).$column.' = ?';
                                        else
                                            $where.=(empty($where)?'':(' '.$union.' ')).$column.' '.strtoupper($method).'('.implode(', ',array_fill(0,count($row['value']),'?')).')';

                                        $value_list=array_merge($value_list,$row['value']);

                                    }
                                    else{

                                        $where          .=(empty($where)?'':(' '.$union.' ')).$column.' = ?';
                                        $value_list[]   =$row['value'];

                                    }

                                }

                                break;

                            }

                            case'between':{

                                if(!is_array($row['column'])){

                                    $error=array(
                                        'title'=>'DB query parameters exception',
                                        'info'=>'Value is not valid',
                                        'data'=>array(
                                            'column'    =>$row['column'],
                                            'method'    =>$method,
                                            'value'     =>$row['value']
                                        )
                                    );

                                    throw new DbQueryParametersException($error);

                                }

                                if(count($row['column'])!=2){

                                    $error=array(
                                        'title'=>'DB query parameters exception',
                                        'info'=>'Value list length is not valid',
                                        'data'=>array(
                                            'column'    =>$row['column'],
                                            'method'    =>$method,
                                            'value'     =>$row['value']
                                        )
                                    );

                                    throw new DbQueryParametersException($error);

                                }

                                $where  .=(empty($where)?'':(' '.$union.' ')).' ? BETWEEN ';
                                $where  .=(empty($row['table_join'])?'':($column_quote.$row['table_join'].$column_quote.'.')).$column_quote.$row['column'][0].$column_quote;
                                $where  .=' AND '.(empty($row['table_join'])?'':($column_quote.$row['table_join'].$column_quote.'.')).$column_quote.$row['column'][1].$column_quote;

                                $value_list[]=$row['value'];

                                break;

                            }

                            case'=':
                            case'!=':
                            case'<':
                            case'>':
                            case'<=':
                            case'>=':
                            case'>>=':
                            case'<>':{

                                if(!empty($row['column_join']))
                                    $where.=(empty($where)?'':(' '.$union.' ')).$column.' '.strtoupper($method).' '.(empty($row['table_join'])?'':($column_quote.$row['table_join'].$column_quote.'.')).$column_quote.$row['column_join'].$column_quote;
                                else if(!empty($row['function']))
                                    $where.=(empty($where)?'':(' '.$union.' ')).$column.'" '.strtoupper($method).' '.$row['function'];
                                else if(is_null($row['value']))
                                    $where.=(empty($where)?'':(' '.$union.' ')).$column.' IS'.($method=='!='||$method=='<>'?' NOT':'').' NULL';
                                else{

                                    $where          .=(empty($where)?'':(' '.$union.' ')).$column.' '.$method.' ?';
                                    $value_list[]   =$row['value'];

                                }

                                break;

                            }

                            case'like':
                            case'unlike':{

                                if(isset($row['value'])){

                                    $where          .=(empty($where)?'':(' '.$union.' ')).$column.' '.strtoupper($method).' ?';
                                    $value_list[]   ='%'.$row['value'].'%';

                                }

                                break;

                            }

                            default:{

                                $error=array(
                                    'title'     =>'DB query parameters error',
                                    'info'      =>'Query parameters in where list have unknown method',
                                    'row'       =>$row,
                                    'data'      =>$data
                                );

                                throw new DbQueryParametersException($error);

                                break;

                            }

                        }

                    }
                    else if(!empty($row['function'])){

                        $where.=(empty($where)?'':(' '.$union.' ')).$row['function'];

                    }

                }

        }

        return array($where,$value_list);

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return array
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_where(array $data=[],string $name=NULL){

        if(empty($data['where'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Where is empty',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(!is_array($data['where'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of WHERE is not array',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        return self::get_where_from_key('where',$data,$name);

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return string
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_table_name_full(array $data=[],string $name=NULL){

        if(empty($data['table'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of TABLE is empty',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        $name       =self::get_db_name($name);
        $quote      =self::get_quote_column($name);

        if(is_string($data['table']))
            return $quote.$data['table'].$quote;

        if(!is_array($data['table'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of TABLE is not array',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(empty($data['table']['name'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of TABLE in array is empty',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        $table=$quote.$data['table']['name'].$quote;

        if(!empty($data['table']['rename']))
            $table.=' AS '.$quote.$data['table']['rename'].$quote;

        return $table;

    }

    /**
     * @param array $data
     * @return bool|mixed|string
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_table_name(array $data=[]){

        if(empty($data['table'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of TABLE is empty',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        if(is_string($data['table']))
            return $data['table'];

        if(!is_array($data['table'])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of TABLE is not array',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        return empty($data['table']['name'])?false:$data['table']['name'];

    }

    /**
     * @param array $data
     * @param bool $is_join
     * @param string|NULL $name
     * @return mixed|string
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_column_name(array $data=[],bool $is_join=false,string $name=NULL){

        $join=$is_join?'_join':'';

        if(empty($data['column'.$join])&&empty($data['function'.$join])){

            $error=array(
                'title'     =>'DB query parameters error',
                'info'      =>'Query parameters of column name is empty',
                'data'      =>$data
            );

            throw new DbQueryParametersException($error);

        }

        $name       =self::get_db_name($name);
        $quote      =self::get_quote_column($name);

        if(isset($data['function'.$join]))
            $column=$data['function'.$join];
        else
            $column=$quote.$data['column'.$join].$quote;

        if(!empty($data['table'.$join]))
            $column=$quote.$data['table'.$join].$quote.'.'.$column;

        if(!empty($data['rename'.$join]))
            $column.=' AS '.$quote.$data['rename'.$join].$quote;

        return $column;

    }

    /**
     * @param string $table_default
     * @param array $list
     * @return bool
     */
    public  static function get_validation_value_list(string $table_default,array $list=[]){

        return true;

//        foreach($list as $row)
//            if(isset($row['value'])){
//
//                $table=empty($row['table'])?$table_default:self::get_table_name($row['table']);
//
//                if(!DbValidationValue::init($table,$row['column'],$row['value'])){
//
//                    $error=array(
//                        'title'     =>'DB query parameters error',
//                        'info'      =>'Query parameter of value list is not valid',
//                        'data'      =>array(
//                            'table'     =>$table,
//                            'row'       =>array(
//                                'table'     =>$table,
//                                'column'    =>$row['column'],
//                                'value'     =>$row['value']
//                            ),
//                            'list'      =>$list
//                        )
//                    );
//
//                    throw new DbValidationValueException($error);
//
//                }
//
//            }
//
//        return true;

    }

    /**
     * @param $table_default
     * @param array $list
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_validation_value_list_from_join($table_default,array $list=[]){

        foreach($list as $row){

            if(isset($row['join']))
                if(!self::get_validation_value_list_from_join($table_default,$row['join'])){

                    $error=array(
                        'title'     =>'DB query parameters error',
                        'info'      =>'Query parameters in JOIN list is not valid',
                        'data'      =>array(
                            'row'       =>$row,
                            'list'      =>$list
                        )
                    );

                    throw new DbValidationValueException($error);

                }

            if(isset($row['where']))
                if(!self::get_validation_value_list($table_default,$row['where'])){

                    $error=array(
                        'title'     =>'DB query parameters error',
                        'info'      =>'Query parameters in JOIN list is not valid for WHERE',
                        'data'      =>array(
                            'row'       =>$row,
                            'list'      =>$list
                        )
                    );

                    throw new DbValidationValueException($error);

                }

        }

        return true;

    }

    /**
     * @param array $data
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function validation_value(array $data=[]){

        $table_default=self::get_table_name($data);

        $is_valid_set       =true;
        $is_valid_where     =true;
        $is_valid_join      =true;

        if(isset($data['set']))
            $is_valid_set=self::get_validation_value_list($table_default,$data['set']);

        if(isset($data['join']))
            $is_valid_join=self::get_validation_value_list_from_join($table_default,$data['join']);

        if(isset($data['where']))
            $is_valid_where=self::get_validation_value_list($table_default,$data['where']);

        return $is_valid_set&&$is_valid_join&&$is_valid_where;

    }

    /**
     * @param string|NULL $name
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function disconnect(string $name=NULL){

        $name=self::get_db_name($name);

        if(empty(self::$db_connect_list[$name])){

            $error=array(
                'title'                 =>'DB problem with connect list',
                'info'                  =>'DB name is not exists in connect list',
                'db_name'               =>$name,
                'coonnect_key_list'     =>array_keys(self::$db_connect_list)
            );

            throw new DbQueryParametersException($error);

        }

        if(self::$db_name_default==$name){


            self::$db_name_default          =NULL;
            self::$db_host_default          =NULL;
            self::$db_login_default         =NULL;
            self::$db_password_default      =NULL;
            self::$db_port_default          =NULL;

        }

        self::$db_connect_list[$name]=NULL;

        return true;

    }

    /**
     * @param string|NULL $query
     * @param string|NULL $name
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function exec(string $query=NULL,string $name=NULL){

        self::is_empty_query($query);

        $name=self::get_db_name($name);

        try{

            return self::$db_connect_list[$name]['connect']->exec($query);

        }
        catch(\PDOException $error){

            $data=array(
                'title'     =>'PDO Exception',
                'info'      =>'PDO cannot make exec',
                'data'      =>array(
                    'db_name'       =>$name,
                    'query'         =>$query
                )
            );

            throw new DbPdoException($error,$data);

        }

    }

    /**
     * @param array $data
     * @param bool $need_insert_id
     * @param array $return_list
     * @param string|NULL $name
     * @param array|NULL $return_on_conflict_list
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function insert(array $data=[],bool $need_insert_id=true,array $return_list=[],string $name=NULL,array $return_on_conflict_list=[]){

        self::isset_table_parameters($data);
        self::isset_values_parameters($data);

        if(Data::is_associative_array($data['values'])){

            foreach($data['values'] as $column=>$value)
                if(!DbValidationValue::init(self::get_table_name($data),$column,$value)){

                    $error=array(
                        'title'     =>'DB query parameter valid problem',
                        'info'      =>'Parameter in value list is not valid',
                        'data'      =>array(
                            'line'      =>array(
                                'table'     =>$data['table'],
                                'column'    =>$column,
                                'value'     =>$value
                            ),
                            'data'      =>$data
                        )
                    );

                    throw new DbValidationValueException($error);

                }

            $value_len          =count($data['values']);
            $value_key_list     =array_keys($data['values']);
            $data['values']     =array($data['values']);

        }
        else if(Data::is_array($data['values'])){

            if(count($data['values'])==0){

                $error=array(
                    'title'     =>'DB query parameters problem',
                    'info'      =>'Values list is empty',
                    'data'      =>array(
                        'data'      =>$data
                    )
                );

                throw new DbQueryParametersException($error);

            }

            $current_len=count($data['values'][0]);

            for($i=1;$i<count($data['values']);$i++)
                if($current_len!=count($data['values'][$i])){

                    $error=array(
                        'title'     =>'DB query parameters problem',
                        'info'      =>'ProfileList parameters in values list have not same length',
                        'data'      =>array(
                            'data'      =>$data
                        )
                    );

                    throw new DbQueryParametersException($error);

                }

            foreach($data['values'] as $row)
                foreach($row as $column=>$value)
                    if(!DbValidationValue::init(self::get_table_name($data),$column,$value)){

                        $error=array(
                            'title'     =>'DB query parameter valid problem',
                            'info'      =>'Parameter in value list is not valid',
                            'data'      =>array(
                                'row'       =>$row,
                                'line'      =>array(
                                    'table'     =>$data['table'],
                                    'column'    =>$column,
                                    'value'     =>$value
                                ),
                                'data'      =>$data
                            )
                        );

                        throw new DbValidationValueException($error);

                    }

            $value_len          =count($data['values'][0]);
            $value_key_list     =array_keys($data['values'][0]);

        }
        else{

            $error=array(
                'title'     =>'DB query parameter problem',
                'info'      =>'Query is not valid',
                'data'      =>array(
                    'data'      =>$data
                )
            );

            throw new DbQueryParametersException($error);

        }

        $table          =self::get_table_name_full($data,$name);
        $name           =self::get_db_name($name);
        $quote          =self::get_quote_column($name);
        $duplicate      =self::get_duplicate($data,$name);
        $query          ='INSERT INTO '.$table.' ('.$quote.implode($quote.','.$quote,$value_key_list).$quote.') VALUES';
        $query_list     =[];
        $value_list     =[];

        foreach($data['values'] as $row){

            $query_list[]   ='('.implode(', ',array_fill(0,$value_len,'?')).')';
            $temp           =[];

            foreach($row as $key=>$value){

                if(is_null($value)||$value==='')
                    $value='NULL';

                $temp[]=$value;

            }

            $value_list=array_merge($value_list,$temp);

        }

        $query.=implode(',',$query_list);

        if(!empty($duplicate))
            $query.=' '.$duplicate;

        if(self::is_postgresql($name)){

            $return_query_list=[];

            if(count($return_on_conflict_list)==0){

                if($need_insert_id)
                    $return_query_list[]='id';

                if(count($return_list)>0)
                    $return_query_list=array_merge($return_query_list,$return_list);

                if(count($return_query_list)>0)
                    $query.=' RETURNING "'.implode('","',$return_query_list).'"';

            }
            else{

                $conflict_line       ='ON CONFLICT ("'.implode('","',$return_on_conflict_list['column_list']).'")';
                $conflict_line      .=' DO UPDATE SET ';

                $update_list=[];

                foreach($return_on_conflict_list['column_list'] as $column_name)
                    $update_list[]='"'.$column_name.'"=EXCLUDED."'.$column_name.'"';

                $set_list='';

                if(isset($return_on_conflict_list['update_list']))
                    if(count($return_on_conflict_list['update_list'])>0){

                        list($set_list,$set_value_list)=self::get_set($return_on_conflict_list['update_list'],$name);

                        $value_list=array_merge($value_list,$set_value_list);

                    }

                $conflict_line      .=implode(', ',$update_list).(!empty($set_list)?(','.$set_list):'');

                if(count($return_query_list)>0)
                    $return_on_conflict_list['return_list']=array_merge($return_query_list,$return_on_conflict_list['return_list']);

                $conflict_line      .=' RETURNING "'.implode('","',$return_on_conflict_list['return_list']).'"';

                $query.=' '.$conflict_line;

            }

        }

        $r=self::prepare('insert',$query,$value_list,$need_insert_id,$name);

        return $r;

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return array
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_select_query(array $data=[],string $name=NULL){

        self::isset_select_parameters($data);
        self::isset_table_parameters($data);

        $select         =self::get_select($data,$name);
        $name           =self::get_db_name($name);
        $db_type        =self::get_db_type($name);
        $table          =self::get_table_name_full($data,$name);
        $straight       ='';
        $join           ='';
        $where          ='';
        $group          ='';
        $order          ='';
//        $limit          =self::$limit_default;
        $value_list     =[];

        if(!empty($data['join'])){

            self::isset_join_parameters($data);

            list($join,$value_add_list)=self::get_join($data,$name);
            $value_list=array_merge($value_list,$value_add_list);

            if($db_type=='mysql')
                $straight=' STRAIGHT_JOIN';

        }

        if(!empty($data['where'])){

            list($where,$value_add_list)=self::get_where($data,$name);

            $value_list=array_merge($value_list,$value_add_list);

        }

        if(!empty($data['group'])){

            self::isset_group_parameters($data);

            $group=self::get_group($data,$name);

        }

        if(!empty($data['order'])){

            self::isset_order_parameters($data);

            $order=self::get_order($data,$name);

        }

        if(!empty($data['limit'])){

            self::isset_limit_parameters($data);

            $limit=self::get_limit($data,$name);

        }

        $query='SELECT'.$straight.' '.$select
            .' FROM '.$table
            .$join
            .(empty($where)?'':(' WHERE '.$where))
            .(empty($group)?'':(' GROUP BY '.$group))
            .(empty($order)?'':(' ORDER BY '.$order))
            .(empty($limit)?'':(' LIMIT '.$limit));

        return array($query,$value_list);

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function select(array $data=[],string $name=NULL){

        list($query,$value_list)=self::get_select_query($data,$name);

        return self::prepare('select',$query,$value_list,false,$name);

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function delete(array $data=[],string $name=NULL){

        self::isset_table_parameters($data);
        self::isset_where_parameters($data);

        if(!self::validation_value($data)){

            $error=array(
                'title'     =>'DB query parameter have valid problem',
                'info'      =>'Parameters is not valid',
                'data'      =>$data
            );

            throw new DbValidationValueException($error);

        }

        list($where,$value_list)=self::get_where($data,$name);

        $table          =self::get_table_name_full($data,$name);
        $name           =self::get_db_name($name);
        $db_type        =self::get_db_type($name);
        $limit          ='';

        if($db_type=='mysql'&&!empty($data['limit']))
            $limit=self::get_limit($data,$name);

        $query='DELETE FROM '.$table.' '.(empty($where)?'':(' WHERE '.$where)).(empty($limit)?'':(' LIMIT '.$limit));

        return self::prepare('delete',$query,$value_list,true,$name);

    }

    /**
     * @param string|NULL $table
     * @param string|NULL $name
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function vacuum_full(string $table=NULL,string $name=NULL){

        if(empty($table)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Table is empty'
            );

            throw new ParametersException($error);

        }

        $name       =self::get_db_name($name);
        $query      ='VACUUM FULL "'.$table.'"';

        return self::exec($query,$name);

    }

    /**
     * @param array $data
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update(array $data=[],string $name=NULL){

        self::isset_table_parameters($data);
        self::isset_set_parameters($data);
        self::isset_where_parameters($data);

        if(!self::validation_value($data)){

            $error=array(
                'title'     =>'DB query parameter have valid problem',
                'info'      =>'Parameters is not valid',
                'data'      =>$data
            );

            throw new DbValidationValueException($error);

        }

        list($set,$value_list)          =self::get_set($data,$name);
        list($where,$value_add_list)    =self::get_where($data,$name);

        $table          =self::get_table_name_full($data,$name);
        $name           =self::get_db_name($name);
        $db_type        =self::get_db_type($name);
        $limit          ='';
        $value_list     =array_merge($value_list,$value_add_list);

        if($db_type=='mysql'&&!empty($data['limit']))
            $limit=self::get_limit($data,$name);

        $query='UPDATE '.$table.' SET '.$set.(empty($where)?'':(' WHERE '.$where)).(empty($limit)?'':(' LIMIT '.$limit));

        return self::prepare('update',$query,$value_list,true,$name);

    }

    /**
     * @param string|NULL $query_type
     * @param string|NULL $query
     * @param array $value_list
     * @param bool $need_insert_id
     * @param string|NULL $name
     * @return array|bool
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function prepare(string $query_type=NULL,string $query=NULL,array $value_list=[],$need_insert_id=true,string $name=NULL){

        if(\Config::$is_query_debug){

            echo "=================================\n";
            echo $query."\n";
            print_r($value_list);
            echo "=================================\n";

        }

        self::$query_last=$query;
        self::$value_last=$value_list;

        self::is_empty_query($query);

        $name=self::get_db_name($name);

        try{

            $insertStatement=self::$db_connect_list[$name]['connect']->prepare($query);

            self::$db_connect_list[$name]['connect']->beginTransaction();

            $num=1;

            foreach($value_list as $index=>$row){

                $type=self::get_parameters_type($row);

                $insertStatement->bindValue($num,$row,$type);

                $num++;

            }

            $insertStatement->execute();

            self::$db_connect_list[$name]['connect']->commit();

            switch($query_type){

                case'select':
                    return $insertStatement->fetchAll(\PDO::FETCH_ASSOC);

                case'insert':{

                    if($need_insert_id){

                        if(self::is_postgresql($name))
                            $list=$insertStatement->fetchAll(\PDO::FETCH_ASSOC);
                        else if(self::is_mysql($name))
                            $list=array(self::get_insert_id($name));
                        else{

                            $error=array(
                                'title'=>'DB parameters problem',
                                'info'=>'Unknown DB type',
                                'data'=>array(
                                    'name'=>$name,
                                    'type'=>self::get_db_type($name)
                                )
                            );

                            throw new DbParametersException($error);

                        }

                        if(\Config::$is_query_debug)
                            print_r($list);

                        return $list;

                    }

                    return self::$db_connect_list[$name]['connect']->errorCode()==0000;

                }

                default:
                    return self::$db_connect_list[$name]['connect']->errorCode()==0000;

            }

        }
        catch(\PDOException $error){

            self::$db_connect_list[$name]['connect']->rollback();

            $data=array(
                'title'             =>'PDO Exception',
                'info'              =>'PDO cannot make transaction',
                'data'              =>array(
                    'query_type'        =>$query_type,
                    'query'             =>$query,
                    'value_list'        =>$value_list,
                    'need_insert_ID'    =>$need_insert_id,
                    'db_name'           =>$name
                )
            );

            throw new DbPdoException($error,$data);

        }

    }

    /**
     * @param string|NULL $table
     * @param string|NULL $name
     * @return bool|mixed
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_max_current_value_in_sequence(string $table=NULL,string $name=NULL){

        if(empty($table)){

            $error=array(
                'table'=>'DB parameters problem',
                'info'=>'Table name is empty'
            );

            throw new DbParametersException($error);

        }

        $db_type=self::get_db_type($name);

        switch($db_type){

            case'postgresql':
                return self::exec('SELECT setval(\''.$table.'_id_seq\', COALESCE((SELECT MAX("id")+1 FROM "'.$table.'"), 1), false)');

            default:
                return false;

        }

    }

    /**
     * @param string|NULL $table
     * @param string|NULL $name
     * @return bool|mixed
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function truncate_table(string $table=NULL,string $name=NULL){

        if(empty($table)){

            $error=array(
                'table' =>'DB parameters problem',
                'info'  =>'Table name is empty'
            );

            throw new DbParametersException($error);

        }

        $db_type=self::get_db_type($name);

        switch($db_type){

//            case'postgresql':
//                return self::exec('TRUNCATE "'.$table.'" RESTART IDENTITY');

            case'postgresql':
                return self::exec('TRUNCATE "'.$table.'" RESTART IDENTITY');

//            case'mysql':
//                return self::exec('TRUNCATE `'.$table.'`');

            default:
                return false;

        }

    }

    /**
     * @return mixed
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_timezone(){

        $q="SET TIME ZONE 'UTC'";

        return Db::exec($q);

    }

    /**
     * @throws DbParametersException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        DbPostgreConnect::init();

    }

}