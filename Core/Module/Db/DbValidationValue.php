<?php

namespace Core\Module\Db;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\SystemException;

class DbValidationValue{

    /** @var string */
    private static $table;

    /** @var string */
    private static $column;

    /** @var */
    private static $value;

    /**
     * @return string
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    private static function get_class_name_from_table_name(){

        $table_list     =mb_split('_',self::$table);
        $result         ='';

        foreach($table_list as $row)
            if(!empty($row))
                $result.=ucfirst($row);

        $file_path='Core/Module/Db/DbValueType/DbTable'.$result.'Value.php';

        if(file_exists($file_path))
            return __NAMESPACE__.'\DbValueType\DbTable'.$result.'Value';
        else{

            $file_path='Core/Project/Db/DbValueType/DbTable'.$result.'Value.php';

            if(file_exists($file_path))
                return '\Project\Whore\All\Module\Db\DbValueType\DbTable'.$result.'Value';
            else{

                $error=array(
                    'title' =>'Parameters problem',
                    'info'  =>'DB validation class is not exists',
                    'data'  =>array(
                        'table'     =>self::$table,
                        'column'        =>self::$column,
                        'value'     =>self::$value
                    )
                );

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @return mixed
     * @throws ParametersException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     */
    private static function is_valid(){

        $class_name=self::get_class_name_from_table_name();

        try{

            return $class_name::init(self::$column,self::$value);

        }
        catch(\Exception $error){

            throw new SystemException($error);

        }

    }

    /**
     * @param string|NULL $table
     * @param string|NULL $column
     * @param null $value
     * @return bool
     */
    public static function init(string $table=NULL,string $column=NULL,$value=NULL){

        return true;

//        if(empty($column))
//            return false;
//
//        self::$table        =empty($table)?NULL:$table;
//        self::$column       =$column;
//        self::$value        =$value;
//
//        return self::is_valid();

    }

}