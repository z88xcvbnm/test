<?php

namespace Core\Module\Date;

use Core\Module\Dir\Dir;
use Core\Module\Exception\ParametersException;

class Date{

    /**
     * @return double
     */
    public  static function get_timestamp_ms(){

        return microtime(true);

    }

    /**
     * @param string|NULL $date
     * @return false|int
     */
    public  static function get_timestamp(string $date=NULL){

        return empty($date)?time():strtotime($date);

    }

    /**
     * @param int|NULL $timestamp
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_leap_year(int $timestamp=NULL){

        if(is_null($timestamp)){

            $error=array(
                'title'     =>'Date parameter problem',
                'info'      =>'Timestamp is null'
            );

            throw new ParametersException($error);

        }

        return (bool)self::get_date_from_mask("L",$timestamp);

    }

    /**
     * @param string|NULL $mask
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_date_from_mask(string $mask=NULL,int $timestamp=NULL){

        if(is_null($mask)){

            $error=array(
                'title'     =>'Date parameter problem',
                'info'      =>'Date mask is null'
            );

            throw new ParametersException($error);

        }

        if(empty($mask))
            $mask="d-m-Y H:i:s";

        return empty($timestamp)?date($mask):date($mask,$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_day_length_of_month(int $timestamp=NULL){

        return self::get_date_from_mask("t",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_month_name(int $timestamp=NULL){

        return self::get_date_from_mask("F",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_weekday(int $timestamp=NULL){

        return self::get_date_from_mask("l",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_day_number_of_weekday(int $timestamp=NULL){

        return self::get_date_from_mask("N",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_week_number_of_year(int $timestamp=NULL){

        return self::get_date_from_mask("W",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_date_full(int $timestamp=NULL){

        return self::get_date_from_mask("d-m-Y",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_date_time_full(int $timestamp=NULL){

        return self::get_date_from_mask("Y-m-d H:i:s",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @param string $mask
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_date(int $timestamp=NULL,string $mask="d-m-Y"){

        if(empty($timestamp))
            $timestamp=self::get_timestamp();

        return self::get_date_from_mask($mask,$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_date_for_db(int $timestamp=NULL){

        return self::get_date_from_mask("Y-m-d",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     */
    public  static function get_last_day_in_month(int $timestamp=NULL){

        if(empty($timestamp))
            $timestamp=self::get_timestamp();

        return date("t",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_year(int $timestamp=NULL){

        return self::get_date_from_mask("y",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_year_full(int $timestamp=NULL){

        return self::get_date_from_mask("Y",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_month(int $timestamp=NULL){

        return self::get_date_from_mask("m",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_day(int $timestamp=NULL){

        return self::get_date_from_mask("d",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_hours(int $timestamp=NULL){

        return self::get_date_from_mask("H",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_minutes(int $timestamp=NULL){

        return self::get_date_from_mask("i",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_seconds(int $timestamp=NULL){

        return self::get_date_from_mask("s",$timestamp);

    }

    /**
     * @param int|NULL $timestamp
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_microseconds(int $timestamp=NULL){

        return self::get_date_from_mask("u",$timestamp);

    }

    /**
     * @param float|NULL $seconds
     * @return mixed|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_string_from_seconds(float $seconds=NULL){

        if(is_null($seconds)){

            $error=array(
                'title'     =>'Date parameter problem',
                'info'      =>'Seconds is NULL'
            );

            throw new ParametersException($error);

        }

        $parts      =[];
        $periods    =array(
            'day'       =>86400,
            'hour'      =>3600,
            'minute'    =>60,
            'second'    =>1
        );

        foreach($periods as $name=>$dur){

            $div=floor($seconds/$dur);

             if($div==0)
                continue;

             else if($div==1)
                $parts[]=$div.' '.$name;
             else
                $parts[]=$div.' '.$name.'s';

             $seconds%=$dur;

        }

        $last=array_pop($parts);

        return empty($parts)?$last:join(', ',$parts).' and ' .$last;

    }

    /**
     * @param string|NULL $timezone_name
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_offset_utc_from_timezone_name(string $timezone_name=NULL){

        if(empty($timezone_name)){

            $error=array(
                'title'     =>'Date parameter problem',
                'info'      =>'Timezone name is empty'
            );

            throw new ParametersException($error);

        }

        $date_timezone      =new \DateTimeZone($timezone_name);
        $timestamp          =new \DateTime('now',$date_timezone);

        return $date_timezone->getOffset($timestamp);

    }

    /**
     * @param string|NULL $date
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_date_split_from_string(string $date=NULL){

        if(empty($date)){

            $error=array(
                'title'     =>'Date parameter problem',
                'info'      =>'Date is empty'
            );

            throw new ParametersException($error);

        }

        list($year,$month,$day)=mb_split('-',mb_substr($date,0,10));

        return array($year,$month,$day);

    }

    /**
     * @param string|NULL $date
     * @param string|NULL $path
     * @return string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_date_path(string $date=NULL,string $path=NULL){

        if(empty($date)){

            $error=array(
                'title'     =>'Date parameter problem',
                'info'      =>'Date is empty'
            );

            throw new ParametersException($error);

        }

        list($year,$month,$day)=self::get_date_split_from_string($date);

        if(is_null($path))
            return $year.'/'.$month.'/'.$day;

        $path.='/'.$year;

        Dir::create_dir($path);

        $path.='/'.$month;

        Dir::create_dir($path);

        $path.='/'.$day;

        Dir::create_dir($path);

        return $path;

    }

    /**
     * @param string|NULL $date
     * @param int|NULL $month
     * @param string $format
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_month_to_date(string $date=NULL,int $month=NULL,string $format='Y-m-d'){

        $error_info_list=[];

        if(empty($date))
            $error_info_list[]='Date is empty';

        if(empty($month))
            $error_info_list[]='Month is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        return date($format,strtotime(($month>=0?'+':'').$month." months",strtotime($date)));

    }

    /**
     * @param string|NULL $date
     * @param int|NULL $day
     * @param string $format
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_day_to_date(string $date=NULL,int $day=NULL,string $format='Y-m-d'){

        $error_info_list=[];

        if(empty($date))
            $error_info_list[]='Date is empty';

        if(empty($day))
            $error_info_list[]='Day is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        return date($format,strtotime(($day>=0?'+':'').$day." days",strtotime($date)));

    }

    /**
     * @param string|NULL $date
     * @param int|NULL $year
     * @param string $format
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_year_to_date(string $date=NULL,int $year=NULL,string $format='Y-m-d'){

        $error_info_list=[];

        if(empty($date))
            $error_info_list[]='Date is empty';

        if(empty($year))
            $error_info_list[]='Year is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        return date($format,strtotime(($year>=0?'+':'').$year." years",strtotime($date)));

    }

    /**
     * @param string|NULL $date
     * @param int|NULL $hour
     * @param string $format
     * @return false|string
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_hour_to_date(string $date=NULL,int $hour=NULL,string $format='Y-m-d'){

        $error_info_list=[];

        if(empty($date))
            $error_info_list[]='Date is empty';

        if(empty($hour))
            $error_info_list[]='Year is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        return date($format,strtotime(($hour>=0?'+':'').$hour." hours",strtotime($date)));

    }

}