<?php

namespace Core\Module\Cookie;

use Core\Module\Date\Date;
use Core\Module\OsServer\OsServer;
use Core\Module\Token\Token;
use Core\Module\Token\TokenConfig;
use Core\Module\Url\Url;

class Cookie{

    /**
     * @return bool
     */
    public  static function isset_token(){

        switch(Url::$host){

            case'api.whore':
            case'api.shluham.net':
                return !empty($_GET['token'])||!empty($_POST['token']);

            default:
                return !empty($_COOKIE['token']);

        }

    }

    /**
     * @return string|null
     */
    public  static function get_token(){

        switch(Url::$host){

            case'api.whore':
            case'api.shluham.ru':{

                if(!empty($_GET['token']))
                    return $_GET['token'];

                if(!empty($_POST['token']))
                    return $_POST['token'];

                return NULL;

            }

            default:{

                if(!empty($_GET['token']))
                    return $_GET['token'];

                if(!empty($_POST['token']))
                    return $_POST['token'];

                return empty($_COOKIE['token'])?NULL:$_COOKIE['token'];

            }

        }

    }

    /**
     * @param int|NULL $timestamp
     * @param string $dir
     * @param string $host
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function create_token(int $timestamp=NULL,string $dir='',string $host=''){

        if(empty($timestamp))
            $timestamp=Date::get_timestamp(Token::get_token_date_live(TokenConfig::$is_keep));

        if(empty($dir))
            if(CookieConfig::$is_dir)
                $dir=CookieConfig::$dir_default;

        if(empty($host))
            if(CookieConfig::$is_host)
                $host=Url::$host;

        return setcookie('token',Token::$token_hash,$timestamp,$dir,$host,\Config::$http_type=='https');

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token(){

        return self::create_token();

    }

    /**
     * @param string $dir
     * @param string $host
     * @return bool
     */
    public  static function remove_token(string $dir='',string $host=''){

        if(empty($dir))
            if(CookieConfig::$is_dir)
                $dir=CookieConfig::$dir_default;

        if(empty($host))
            if(CookieConfig::$is_host)
                $host=Url::$host;

        return setcookie('token',NULL,1,$dir,$host);

    }

}