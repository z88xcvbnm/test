<?php

namespace Core\Module\Cookie;

use Core\Module\Date\Date;
use Core\Module\Exception\ParametersException;
use Core\Module\OsServer\OsServer;
use Core\Module\Token\Token;
use Core\Module\Token\TokenConfig;
use Core\Module\Url\Url;

class CookieFile{

    /**
     * @param string|NULL $file_path
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_cookie_list_from_file(string $file_path=NULL){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';
        else if(!file_exists($file_path))
            $error_info_list[]='File is not exists';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $lines      =file($file_path);
        $list       =[];

        foreach($lines as $line)
            if($line[0]!='#'&&substr_count($line,"\t")==6){

                $tokens     =explode("\t", $line);
                $tokens     =array_map('trim', $tokens);

                $list[]=[
                    'host'      =>$tokens[0],
                    'name'      =>$tokens[5],
                    'value'     =>$tokens[6]
                ];

            }

        return $list;

    }

}