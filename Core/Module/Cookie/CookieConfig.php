<?php

namespace Core\Module\Cookie;

class CookieConfig{

    /** @var bool */
    public  static $is_host         =true;

    /** @var bool */
    public  static $is_dir          =true;

    /** @var string */
    public  static $dir_default     ='/';

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$is_host          =true;
        self::$is_dir           =true;
        self::$dir_default      ='/';

        return true;

    }

}