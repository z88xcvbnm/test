<?php

namespace Core\Module\Notification\Email;

use Core\Module\ArrayData\ArrayData;
use Core\Module\Exception\ParametersException;

class EmailNotificationConfig{

    /**
     * @var array
     */
    public  static $email_list=array(
    );

    /**
     * @return array
     */
    public  static function get_email_list(){

        return self::$email_list;

    }

    /**
     * @param string|NULL $email
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_email_in_email_list(string $email=NULL){

        if(empty($email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Email is empty'
            );

            throw new ParametersException($error);

        }

        if(array_search($email,self::$email_list)!==false)
            return true;

        self::$email_list[]=$email;

        return true;

    }

    /**
     * @param string|NULL $email
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_email_in_email_list(string $email=NULL){

        if(empty($email)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Email is empty'
            );

            throw new ParametersException($error);

        }

        self::$email_list=ArrayData::remove_value_from_array($email,self::$email_list);

        return true;

    }

}