<?php

namespace Core\Module\Socket;

use Core\Module\Exception\ParametersException;
use Core\Module\Url\Url;

class WebSocketConfig{

    /** @var string */
    public  static $host        ='sockettest';

    /** @var int */
    public  static $port        =4041;

    /**
     * @return bool
     */
    private static function set_host(){

        self::$host=Url::$host;

        return true;

    }

    /**
     * @return string
     * @throws ParametersException
     */
    public  static function get_host(){

        if(empty(self::$host))
            self::set_host();

        if(empty(self::$host)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Host is empty'
            );

            throw new ParametersException($error);

        }

        return self::$host;

    }

}