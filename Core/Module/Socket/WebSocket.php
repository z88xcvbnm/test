<?php

namespace Core\Module\Socket;

use Core\Module\Exception\PhpException;

class WebSocket{

    /** @var string */
    public  static $host;

    /** @var int */
    public  static $port;

    /** @var bool */
    public  static $is_loop             =true;

    /** @var string */
    public  static $secret_key;

    /** @var string */
    public  static $key                 ='258EAFA5-E914-47DA-95CA-C5AB0DC85B11';

    /** @var array */
    public  static $client_keys_list    =[];

    /** @var array */
    public  static $socket_list         =[];

    /** @var array */
    public  static $write_list          =[];

    /** @var array */
    public  static $except_list         =[];

    /** @var resource */
    public  static $socket_object;

    /**
     * @return bool
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function set_settings(){

        self::$host     =WebSocketConfig::get_host();
        self::$port     =WebSocketConfig::$port;

        return true;

    }

    /**
     * @return bool
     */
    private static function set_data_default(){

        self::$host                     =NULL;
        self::$port                     =NULL;
        self::$is_loop                  =true;
        self::$socket_object            =NULL;

        self::$client_keys_list         =[];
        self::$socket_list              =[];
        self::$write_list               =[];
        self::$except_list              =[];

        return true;

    }

    /**
     * @return bool
     */
    private static function set_socket_list(){

        self::$socket_list=array(self::$socket_object);

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     */
    private static function set_socket(){

        self::$socket_object=socket_create(AF_INET,SOCK_STREAM,SOL_TCP);

        if(self::$socket_object===false){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Socket was not create'
            );

            throw new PhpException($error);

        }

        if(socket_set_option(self::$socket_object,SOL_SOCKET,SO_REUSEADDR,1)===false){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Socket option was not update'
            );

            throw new PhpException($error);

        }

        if(socket_bind(self::$socket_object,WebSocketConfig::$host,WebSocketConfig::$port)===false){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Socket client setting was not updated'
            );

            throw new PhpException($error);

        }

        if(socket_listen(self::$socket_object)===false){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'Socket was not start'
            );

            throw new PhpException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     */
    private static function set_loop(){

        while(self::$is_loop){

            $read       =self::$socket_list;
            $write      =[];
            $except     =[];

            if(socket_select($read,$write,$except,0,10)===false){

                $error=array(
                    'title'     =>'System error',
                    'info'      =>'Socket select was fail'
                );

                throw new PhpException($error);

            }

            if(in_array(self::$socket_object,$read)){

                $socket_new=socket_accept(self::$socket_object);

                if($socket_new===false){

                    $error=array(
                        'title'     =>'System error',
                        'info'      =>'New socket was not create'
                    );

                    throw new PhpException($error);

                }

                self::$socket_list[]    =$socket_new;
                $socket_header          =socket_read($socket_new,1024);

                self::set_first_response($socket_header,$socket_new,WebSocketConfig::$host,WebSocketConfig::$port);

                $found_socket_key=array_search(self::$socket_object,$read);

                unset($read[$found_socket_key]);

            }

            foreach($read as $socket_row){

                while(@socket_recv($socket_row,$buf,1024,0)>=1){

					$data_string=self::get_data_decode($buf);

					self::preview_send_data($data_string,$socket_row);

					break 2;

				}

				$buf=@socket_read($socket_row,1024,PHP_NORMAL_READ);

				if($buf===false)
					self::remove_socket($socket_row);

            }


        }

        socket_close(self::$socket_object);

        return true;

    }

    /**
     * @param $data_string
     * @param $socket
     * @return bool
     * @throws PhpException
     */
    private static function preview_send_data($data_string,$socket){

		$decode=json_decode($data_string);

		if(empty($decode->command))
		    return true;

		$func=$decode->command;

		if(method_exists('Start_socket',$func))
			self::$func($data_string,$decode,$socket);

		if(!file_exists('temp'))
		    mkdir('temp',0775);

		file_put_contents('temp/log_'.time().'.log',print_r($decode,true),FILE_APPEND);

		self::send_data('Test');

		return true;

    }

    /**
     * @param $data_string
     * @param bool $to
     * @return bool
     * @throws PhpException
     */
    private static function send_data($data_string,$to=false){

		$data=self::get_data_encode($data_string);

		if(!empty($to)){

			$to_socket_key  =array_search($to,self::$client_keys_list);
			$to_socket      =self::$socket_list[$to_socket_key];

			if(socket_write($to_socket,$data,strlen($data))===false){

			    $error=array(
			        'title'     =>'System error',
                    'info'      =>'Socket write was fail'
                );

                throw new PhpException($error);

            }

			return true;

		}

		foreach(self::$socket_list as $socket_row)
            @socket_write($socket_row,$data,strlen($data));

//        foreach(self::$socket_list as $socket_row){
//
//            $len=socket_write($socket_row,$data,strlen($data));
//
//            if($len)
//
//        }

		return true;

	}

    /**
     * @param $socket
     * @return bool
     */
    private static function remove_socket($socket){

        $found_socket = array_search($socket,self::$socket_list);

		unset(self::$socket_list[$found_socket]);
		unset(self::$client_keys_list[$found_socket]);

		return true;

    }

    /**
     * @param $string
     * @return string
     * @throws PhpException
     */
    private static function get_data_encode($string){

        $b1     =0x80 | (0x1 & 0x0f);
		$len    =strlen($string);

		if($len<=125)
			$header=pack('CC',$b1,$len);
		else if($len>125&&$len<65536)
			$header=pack('CCn',$b1,126,$len);
		else if($len>=65536)
			$header=pack('CCNN',$b1,127,$len);
		else{

		    $error=array(
		        'title'     =>'System error',
                'info'      =>'Header was not set'
            );

		    throw new PhpException($error);

        }

		return $header.$string;

    }

    /**
     * @param $string
     * @return string
     */
    private static function get_data_decode($string){

		$len=ord($string[1]) & 127;

		if($len==126){

			$masks  =substr($string,4,4);
			$data   =substr($string,8);

		}
		else if($len==127){

			$masks  =substr($string,10,4);
			$data   =substr($string,14);
		}
		else{

			$masks  =substr($string,2,4);
			$data   =substr($string,6);

		}

		$string='';

		for($i=0;$i<strlen($data);$i++)
			$string.=$data[$i] ^ $masks[$i % 4];

		return $string;

    }

    /**
     * @param $socket_header
     * @param $socket_new
     * @param $host
     * @param $port
     * @return bool
     * @throws PhpException
     */
    private static function set_first_response($socket_header,$socket_new,$host,$port){

		$headers    =[];
		$lines      =preg_split("/\r\n/",$socket_header);

		foreach($lines as $line){

			$line=chop($line);

			if(preg_match('/\A(\S+): (.*)\z/',$line,$matches))
				$headers[$matches[1]] = $matches[2];

		}

		$secKey             =$headers['Sec-WebSocket-Key'];
		$secAccept          =base64_encode(pack('H*',sha1($secKey.self::$key)));

		$upgrade_list       =[];
		$upgrade_list[]     ="HTTP/1.1 101 Web Socket Protocol Handshake";
        $upgrade_list[]     ="Upgrade: websocket";
        $upgrade_list[]     ="Connection: Upgrade";
        $upgrade_list[]     ="WebSocket-Origin: ".$host;
        $upgrade_list[]     ="WebSocket-Location: ws://".$host.":".$port;
        $upgrade_list[]     ="Sec-WebSocket-Accept:".$secAccept;
        $upgrade_list[]     ='';
        $upgrade_list[]     ='';

		$upgrade            =implode("\r\n",$upgrade_list);

		if(socket_write($socket_new,$upgrade,strlen($upgrade))===false){

		    $error=array(
		        'title'     =>'System error',
                'info'      =>'Socket write was fail'
            );

            throw new PhpException($error);

        }

		return true;

	}

    /**
     * @return bool
     */
    private static function set_return(){

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\ParametersException
     */
    private static function set(){

        self::set_data_default();
        self::set_settings();

        self::set_socket();
        self::set_socket_list();
        self::set_loop();

        return self::set_return();

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function init(){

        return self::set();

    }

}