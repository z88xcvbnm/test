<?php

namespace Core\Module\Phone;

class PhoneValidation{

    /**
     * @param string|NULL $phone_number
     * @return bool|int
     */
    public  static function is_valid_phone(string $phone_number=NULL){

        if(empty($phone_number))
            return false;

        return preg_match('^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$',$phone_number);

    }

}