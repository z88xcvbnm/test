<?php

namespace Core\Module\Json;

use Core\Module\Exception\ParametersValidationException;
use Core\Module\Exception\SystemException;

class Json{

    /**
     * @param array $array
     * @return string
     */
    public static function encode_cyrillic(array $array){

        $str=preg_replace_callback('/\\\\u([a-f0-9]{4})/i',create_function('$m','return chr(hexdec($m[1])-1072+224);'),$array);

        return iconv('cp1251','utf-8',$str);

    }

    /**
     * @param array $array
     * @return false|string
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public static function encode(array $array){

        try{

            return json_encode($array,JSON_UNESCAPED_UNICODE);

        }
        catch(\Exception $error){

            throw new SystemException($error);

        }

    }

    /**
     * @param string $string
     * @return bool
     */
    public static function is_json(string $string=NULL){

        return !preg_match('/[^,:{}\\[\\]0-9.\\-+Eaeflnr-u \\n\\r\\t]/',preg_replace('/"(\\.|[^"\\\\])*"/','',$string));

    }

    /**
     * @param string|NULL $string
     * @param bool $is_assoc
     * @return mixed
     * @throws ParametersValidationException
     * @throws SystemException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     */
    public  static function decode(string $string=NULL,bool $is_assoc=true){

        $array              =json_decode($string,$is_assoc);
        $decode_status      =json_last_error();

        switch($decode_status){

            case JSON_ERROR_NONE:{

                return $array;

                break;

            }

            case JSON_ERROR_DEPTH:{

                $log_content=array(
                    'json'              =>$string,
                    'decode_status'     =>'JSON_ERROR_DEPTH',
                    'error'             =>'Достигнута максимальная глубина стека'
                );

                break;

            }
            case JSON_ERROR_STATE_MISMATCH:{

                $log_content=array(
                    'json'              =>$string,
                    'decode_status'     =>'JSON_ERROR_STATE_MISMATCH',
                    'error'             =>'Некорректные разряды или не совпадение режимов'
                );

                break;

            }
            case JSON_ERROR_CTRL_CHAR:{

                $log_content=array(
                    'json'              =>$string,
                    'decode_status'     =>'JSON_ERROR_CTRL_CHAR',
                    'error'             =>'Некорректный управляющий символ'
                );

                break;

            }
            case JSON_ERROR_SYNTAX:{

                $log_content=array(
                    'json'              =>$string,
                    'decode_status'     =>'JSON_ERROR_SYNTAX',
                    'error'             =>'Синтаксическая ошибка, не корректный JSON'
                );

                break;

            }
            case JSON_ERROR_UTF8:{

                $log_content=array(
                    'json'              =>$string,
                    'decode_status'     =>'JSON_ERROR_UTF8',
                    'error'             =>'Некорректные символы UTF-8, возможно неверная кодировка'
                );

                break;

            }
            default:{

                $log_content=array(
                    'json'              =>$string,
                    'decode_status'     =>'JSON_ERROR_UTF8',
                    'error'             =>'Неизвестная ошибка'
                );

                break;

            }

        }

        $error=array(
            'title'     =>'Json Validation Problem',
            'info'      =>'String is not json format',
            'data'      =>array(
                'log'       =>$log_content
            )
        );

        throw new ParametersValidationException($error);

    }

}