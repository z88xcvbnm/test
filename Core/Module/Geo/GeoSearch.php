<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Ip\IpConvert;

class GeoSearch{

    /** @var string */
    private static $dir                     ='Core/Resources/Localization';

    /** @var string */
    private static $file_ip4_name           ='GeoLite2-City-Blocks-IPv4.csv';

    /** @var string */
    private static $file_ip6_name           ='GeoLite2-City-Blocks-IPv6.csv';

    /** @var array */
    private static $file_city_name_list     =array(
        'ru'        =>'GeoLite2-City-Locations-ru.csv',
        'de'        =>'GeoLite2-City-Locations-de.csv',
        'en'        =>'GeoLite2-City-Locations-en.csv',
        'es'        =>'GeoLite2-City-Locations-es.csv',
        'fr'        =>'GeoLite2-City-Locations-fr.csv',
        'ja'        =>'GeoLite2-City-Locations-ja.csv',
        'pt'        =>'GeoLite2-City-Locations-pt.csv',
        'zh'        =>'GeoLite2-City-Locations-zh.csv'
    );

    /**
     * @param int|NULL $geo_ID
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_geo_data(int $geo_ID=NULL){

        if(empty($geo_ID)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Geo ID is empty'
            ];

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id',
                ),
                array(
                    'column'=>'continent_id',
                ),
                array(
                    'column'=>'country_id',
                ),
                array(
                    'column'=>'region_id',
                ),
                array(
                    'column'=>'city_id',
                ),
                array(
                    'column'=>'timezone_id',
                )
            ),
            'table'=>'_geo',
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$geo_ID,
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return array(
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
            );
        else
            return array(
                $r[0]['id'],
                $r[0]['continent_id'],
                $r[0]['country_id'],
                $r[0]['region_id'],
                $r[0]['city_id'],
                $r[0]['timezone_id']
            );

    }

    /**
     * @param int|NULL $serach_geo_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function search_geo_ID_from_ipv4(int $serach_geo_ID=NULL){

        $file_object=fopen(self::$dir.'/'.self::$file_ip4_name,"r");

        $cols_name_list=mb_split(",",trim(fgets($file_object)));

        $row_index=0;

        while(!feof($file_object)){

            $value_list     =mb_split(",",trim(fgets($file_object)));
            $temp           =[];

            $row_index++;

            foreach($value_list as $index=>$value){

                $key        =trim($cols_name_list[$index]);
                $value      =trim($value);

                switch($key){

                    case'network':{

                        if(!empty($value)){

                            list($ip_start,$ip_finish)=IpConvert::get_cidr_to_ip_range($value);

                            $temp['ip_start']           =$ip_start;
                            $temp['ip_finish']          =$ip_finish;
                            $temp['ip_start_long']      =ip2long($ip_start);
                            $temp['ip_finish_long']     =ip2long($ip_finish);

                            $temp[$key]                 =$value;

                        }

                        break;

                    }

                    default:{

                        $temp[$key]=$value;

                        break;

                    }

                }

            }

            $geo_ID=empty($temp['geoname_id'])?NULL:(int)$temp['geoname_id'];

            if((int)$geo_ID==(int)$serach_geo_ID){

                $network                =$temp['network'];
                $country_geo_ID         =empty($temp['registered_country_geoname_id'])?NULL:(int)$temp['registered_country_geoname_id'];
                $postal_code            =empty($temp['postal_code'])?NULL:$temp['postal_code'];
                $latitude               =empty($temp['latitude'])?NULL:(double)$temp['latitude'];
                $longitude              =empty($temp['longitude'])?NULL:(double)$temp['longitude'];
                $radius                 =empty($temp['radius'])?NULL:(int)$temp['radius'];

                list($geo_ID,$continent_ID,$country_ID,$region_ID,$city_ID,$timezone_ID)=self::get_geo_data((int)$temp['geoname_id']);

                echo $geo_ID.' '.(int)$temp['geoname_id'].' '.(int)$temp['registered_country_geoname_id']."\n";

                if((int)$temp['geoname_id']!=(int)$geo_ID){

                    echo 'CSV geo_name_ID: '.$temp['geoname_id']."\n";
                    echo 'DB geo_name_ID: '.$geo_ID."\n";

                    print_r($temp);

                    exit;

                }

            }

        }

        fclose($file_object);

        echo 'complete: '.$row_index."\n";
        echo 'search do not give result';

        return true;

    }

}