<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Lang\LangConfig;

class CountryLocalization{

    /** @var int */
    public  static $country_localization_ID;

    /** @var int */
    public  static $lang_ID;

    /** @var string */
    public  static $country_name;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$country_localization_ID      =NULL;
        self::$lang_ID                      =NULL;
        self::$country_name                 =NULL;

        return true;

    }

    /**
     * @param int|NULL $country_localization_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_country_localization_ID(int $country_localization_ID=NULL){

        if(empty($country_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country localization ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($country_localization_ID,'_country_localization',0);

    }

    /**
     * @param int|NULL $country_ID
     * @param int|NULL $lang_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_country_name(int $country_ID=NULL,int $lang_ID=NULL){

        if(empty($country_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country ID is empty'
            );

            throw new ParametersException($error);

        }

        if(empty($lang_ID))
            $lang_ID=LangConfig::$lang_ID_default;

        $where_list=array(
            'country_id'    =>$country_ID,
            'lang_id'       =>$lang_ID
        );

        return Db::isset_row('_country_localization',0,$where_list);

    }

    /**
     * @param int|NULL $country_ID
     * @param int|NULL $lang_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_ID(int $country_ID=NULL,int $lang_ID=NULL){

        if(empty($country_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country ID is empty'
            );

            throw new ParametersException($error);

        }

        if(empty($lang_ID))
            $lang_ID=LangConfig::$lang_ID_default;

        $where_list=array(
            'country_id'    =>$country_ID,
            'lang_id'       =>$lang_ID
        );

        $country_ID=Db::get_row_ID('_country_localization',0,$where_list);

        if(empty($country_ID))
            return NULL;

        return $country_ID;

    }

    /**
     * @param string|NULL $country_name
     * @param int|NULL $lang_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_ID_from_name(string $country_name=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($country_name))
            $error_info_list[]='Country name is empty';

//        if(empty($lang_ID))
//            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'country_id'
            ),
            'table' =>'_country_localization',
            'where' =>array(
                'name'      =>[
                    'transform'     =>'lower',
                    'value'         =>$country_name
                ],
                'type'      =>0
            ),
            'limit'=>1
        );

        if(!is_null($lang_ID))
            $q['where']['lang_id']=$lang_ID;

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['country_id'];

    }

    /**
     * @param array $country_name_list
     * @param int|NULL $lang_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_ID_list_from_name_list(array $country_name_list=[],int $lang_ID=NULL){

        $error_info_list=[];

        if(count($country_name_list)==0)
            $error_info_list[]='Country name is empty';

//        if(empty($lang_ID))
//            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'country_id',
                'name'
            ),
            'table' =>'_country_localization',
            'where' =>array(
                'name'      =>$country_name_list,
                'type'      =>0
            ),
            'limit'=>1
        );

        if(!is_null($lang_ID))
            $q['where']['lang_id']=$lang_ID;

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['country_id']]=$row['name'];

        return $list;

    }

    /**
     * @param int|NULL $country_ID
     * @param int|NULL $lang_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_name(int $country_ID=NULL,int $lang_ID=NULL){

        if(empty($country_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country ID is empty'
            );

            throw new ParametersException($error);

        }

        if(empty($lang_ID))
            $lang_ID=LangConfig::$lang_ID_default;

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_country_localization',
            'where'=>array(
                'country_id'    =>$country_ID,
                'lang_id'       =>$lang_ID,
                'type'          =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @return null|string
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_name_default(){

        if(empty(self::$country_localization_ID)&&empty(self::$lang_ID)&&empty(Country::$country_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default country data is empty'
            );

            throw new ParametersException($error);

        }

        if(!empty(self::$country_localization_ID))
            return self::get_country_name_from_country_localization_ID(self::$country_localization_ID);
        else if(!empty(self::$lang_ID)&&!empty(Country::$country_ID)){

            if(empty(self::$lang_ID))
                self::set_lang_ID_default(LangConfig::$lang_ID_default);

            return self::get_country_name(Country::$country_ID,self::$lang_ID);

        }

        return NULL;

    }

    /**
     * @param int|NULL $country_localization_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_name_from_country_localization_ID(int $country_localization_ID=NULL){

        if(empty($country_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country localization ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_country_localization',
            'where'=>array(
                'id'    =>$country_localization_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param int|NULL $country_ID
     * @param int|NULL $lang_ID
     * @param string|NULL $country_name
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_country_localization(int $country_ID=NULL,int $lang_ID=NULL,string $country_name=NULL){

        $error_info_list=[];

        if(empty($country_ID))
            $error_info_list[]='Country ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(empty($country_name))
            $error_info_list[]='Country name is empty';
        else if(!CountryValidation::is_valid_country_name($country_name))
            $error_info_list[]='Country name is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'country_ID'        =>$country_ID,
                    'lang_ID'           =>$lang_ID,
                    'country_name'      =>$country_name
                )
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_country_localization',
            'values'    =>array(
                'country_id'        =>$country_ID,
                'lang_id'           =>$lang_ID,
                'name'              =>$country_name,
                'date'              =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Country localization was not added',
                'data'      =>array(
                    'country_ID'        =>$country_ID,
                    'lang_ID'           =>$lang_ID,
                    'country_name'      =>$country_name
                )
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $country_localization_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_country_localization_ID(int $country_localization_ID=NULL){

        if(empty($country_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country localization ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($country_localization_ID,'_country_localization',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Country localization was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $country_ID
     * @param int|NULL $lang_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_country_localization(int $country_ID=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($country_ID))
            $error_info_list[]='Country ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'country_id'    =>$country_ID,
            'lang_id'       =>$lang_ID
        );

        if(!Db::delete_from_where_list('_country_localization',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Country localization was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $country_localization_ID
     * @return bool
     */
    public  static function set_country_localization_ID_default(int $country_localization_ID=NULL){

        self::$country_localization_ID=empty($country_localization_ID)?NULL:$country_localization_ID;

        return true;

    }

    /**
     * @param int|NULL $lang_ID
     * @return bool
     */
    public  static function set_country_lang_ID_default(int $lang_ID=NULL){

        self::$lang_ID=empty($lang_ID)?NULL:$lang_ID;

        return true;

    }

    /**
     * @param string|NULL $country_name
     * @return bool
     */
    public  static function set_country_name_default(string $country_name=NULL){

        self::$country_name=empty($country_name)?NULL:$country_name;

        return true;

    }


}