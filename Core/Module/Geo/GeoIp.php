<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class GeoIp{

    /** @var int */
    public  static $geo_ip_ID;

    /** @var string */
    public  static $geo_ip_network;

    /** @var string */
    public  static $postal_code;

    /** @var int */
    public  static $ip_start_long;

    /** @var int */
    public  static $ip_finish_long;

    /** @var double */
    public  static $latitude;

    /** @var double */
    public  static $longitude;

    /** @var int */
    public  static $radius;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$geo_ip_ID    =NULL;
        self::$postal_code  =NULL;
        self::$latitude     =NULL;
        self::$longitude    =NULL;
        self::$radius       =NULL;

        return true;

    }

    /**
     * @param int|NULL $geo_ip_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_geo_ip_ID(int $geo_ip_ID=NULL){

        if(empty($geo_ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Geo IP ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($geo_ip_ID,'_geo_ip',0);

    }

    /**
     * @param int|NULL $geo_ip_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_geo_ip_data(int $geo_ip_ID=NULL){

        if(empty($geo_ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Geo IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'geo_id',
                'country_geo_id',
                'network',
                'ip_start_long',
                'ip_finish_long',
                'postal_code',
                'latitude',
                'longitude',
                'radius'
            ),
            'table'=>'_geo_ip',
            'where'=>array(
                'id'    =>$geo_ip_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'geo_ID'            =>$r[0]['geo_id'],
            'country_geo_ID'    =>$r[0]['country_geo_id'],
            'network'           =>$r[0]['network'],
            'ip_start_long'     =>$r[0]['ip_satrt_long'],
            'ip_finish_long'    =>$r[0]['ip_finish_long'],
            'postal_code'       =>$r[0]['postal_code'],
            'latitude'          =>$r[0]['latitude'],
            'longitude'         =>$r[0]['longitude'],
            'radius'            =>$r[0]['radius']
        );

    }

    /**
     * @param string|NULL $ip_address
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_geo_ip_data_from_ip_address(string $ip_address=NULL){

        if(empty($ip_address)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'geo_id',
                'country_geo_id',
                'network',
                'ip_start_long',
                'ip_finish_long',
                'postal_code',
                'latitude',
                'longitude',
                'radius'
            ),
            'table'=>'_geo_ip',
            'where'=>array(
                array(
                    'column'    =>'network',
                    'method'    =>'>>=',
                    'value'     =>$ip_address
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'geo_ID'            =>$r[0]['geo_id'],
            'geo_ip_ID'         =>$r[0]['id'],
            'country_geo_ID'    =>$r[0]['country_geo_id'],
            'network'           =>$r[0]['network'],
            'ip_start_long'     =>$r[0]['ip_start_long'],
            'ip_finish_long'    =>$r[0]['ip_finish_long'],
            'postal_code'       =>$r[0]['postal_code'],
            'latitude'          =>$r[0]['latitude'],
            'longitude'         =>$r[0]['longitude'],
            'radius'            =>$r[0]['radius']
        );

    }

    /**
     * @param int|NULL $ip_address_long
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_geo_ip_data_from_ip_address_long(int $ip_address_long=NULL){

        if(empty($ip_address_long)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address long is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'geo_id',
                'country_geo_id',
                'network',
                'ip_start_long',
                'ip_finish_long',
                'postal_code',
                'latitude',
                'longitude',
                'radius'
            ),
            'table'=>'_geo_ip',
            'where'=>array(
                array(
                    'column'    =>'ip_start_long',
                    'method'    =>'<=',
                    'value'     =>$ip_address_long
                ),
                array(
                    'column'    =>'ip_finish_long',
                    'method'    =>'>=',
                    'value'     =>$ip_address_long
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'geo_ID'            =>$r[0]['geo_id'],
            'geo_ip_ID'         =>$r[0]['id'],
            'country_geo_ID'    =>$r[0]['country_geo_id'],
            'network'           =>$r[0]['network'],
            'ip_start_long'     =>$r[0]['ip_start_long'],
            'ip_finish_long'    =>$r[0]['ip_finish_long'],
            'postal_code'       =>$r[0]['postal_code'],
            'latitude'          =>$r[0]['latitude'],
            'longitude'         =>$r[0]['longitude'],
            'radius'            =>$r[0]['radius']
        );

    }

    /**
     * @param int|NULL $geo_ip_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_geo_coordinates(int $geo_ip_ID=NULL){

        if(empty($geo_ip_ID)){

            $error=array(
                'title'     =>'Paraemters problem',
                'info'      =>'Geo IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'latitude',
                'longitude'
            ),
            'table'=>'_geo_ip',
            'where'=>array(
                'id'    =>$geo_ip_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'latitiude'     =>$r[0]['latitude'],
            'longitude'     =>$r[0]['longitude']
        );

    }

    /**
     * @param int|NULL $geo_ip_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_geo_network(int $geo_ip_ID=NULL){

        if(empty($geo_ip_ID)){

            $error=array(
                'title'     =>'Paraemters problem',
                'info'      =>'Geo IP ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'network'
            ),
            'table'=>'_geo_ip',
            'where'=>array(
                'id'    =>$geo_ip_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['network'];

    }

    /**
     * @param int|NULL $geo_ID
     * @param int|NULL $country_geo_ID
     * @param string|NULL $network
     * @param int|NULL $ip_start_long
     * @param int|NULL $ip_finish_long
     * @param string|NULL $postal_code
     * @param float|NULL $latitude
     * @param float|NULL $longitude
     * @param int|NULL $radius
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_geo_ip(int $geo_ID=NULL,int $country_geo_ID=NULL,string $network=NULL,int $ip_start_long=NULL,int $ip_finish_long=NULL,string $postal_code=NULL,float $latitude=NULL,float $longitude=NULL,int $radius=NULL){

        $error_info_list=[];

        if(empty($geo_ID))
            $error_info_list[]='Geo ID is empty';

        if(empty($network))
            $error_info_list[]='Network is empty';

        if(empty($ip_start_long))
            $error_info_list[]='IP long start is empty';

        if(empty($ip_finish_long))
            $error_info_list[]='IP long finish is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_geo_ip',
            'values'    =>array(
                'geo_id'            =>$geo_ID,
                'country_geo_id'    =>$country_geo_ID,
                'network'           =>$network,
                'ip_start_long'     =>$ip_start_long,
                'ip_finish_long'    =>$ip_finish_long,
                'postal_code'       =>$postal_code,
                'latitude'          =>$latitude,
                'longitude'         =>$longitude,
                'radius'            =>$radius
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Geo IP was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $geo_ip_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_geo_ip_ID(int $geo_ip_ID=NULL){

        if(empty($geo_ip_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Geo IP ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($geo_ip_ID,'_geo_ip',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Geo IP was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param array $geo_ip_data
     * @return bool
     */
    public  static function set_geo_ip_data_default(array $geo_ip_data=[]){

        if(!empty($geo_ip_data['geo_ID']))
            Geo::set_geo_ID_default($geo_ip_data['geo_ID']);

        if(!empty($geo_ip_data['country_geo_ID']))
            Geo::set_country_geo_ID_default($geo_ip_data['country_geo_ID']);

        if(!empty($geo_ip_data['geo_ip_ID']))
            self::set_geo_ip_ID_default($geo_ip_data['geo_ip_ID']);

        if(!empty($geo_ip_data['network']))
            self::set_geo_ip_network_default($geo_ip_data['network']);

        if(!empty($geo_ip_data['ip_start_long']))
            self::set_ip_start_long_default($geo_ip_data['ip_start_long']);

        if(!empty($geo_ip_data['ip_finish_long']))
            self::set_ip_finish_long_default($geo_ip_data['ip_finish_long']);

        if(!empty($geo_ip_data['postal_code']))
            self::set_postal_code_default($geo_ip_data['postal_code']);

        if(!empty($geo_ip_data['latitude']))
            self::set_latitude_default($geo_ip_data['latitude']);

        if(!empty($geo_ip_data['longitude']))
            self::set_longitude_default($geo_ip_data['longitude']);

        if(!empty($geo_ip_data['latitude']))
            self::set_latitude_default($geo_ip_data['latitude']);

        if(!empty($geo_ip_data['radius']))
            self::set_radius_default($geo_ip_data['radius']);

        return true;

    }

    /**
     * @param int|NULL $geo_ip_ID
     * @return bool
     */
    public  static function set_geo_ip_ID_default(int $geo_ip_ID=NULL){

        self::$geo_ip_ID=empty($geo_ip_ID)?NULL:$geo_ip_ID;

        return true;

    }

    /**
     * @param string|NULL $geo_ip_network
     * @return bool
     */
    public  static function set_geo_ip_network_default(string $geo_ip_network=NULL){

        self::$geo_ip_network=empty($geo_ip_network)?NULL:$geo_ip_network;

        return true;

    }

    /**
     * @param string|NULL $postal_code
     * @return bool
     */
    public  static function set_postal_code_default(string $postal_code=NULL){

        self::$postal_code=empty($postal_code)?NULL:$postal_code;

        return true;

    }

    /**
     * @param int|NULL $ip_start_long
     * @return bool
     */
    public  static function set_ip_start_long_default(int $ip_start_long=NULL){

        self::$ip_start_long=empty($ip_start_long)?NULL:$ip_start_long;

        return true;

    }

    /**
     * @param int|NULL $ip_finish_long
     * @return bool
     */
    public  static function set_ip_finish_long_default(int $ip_finish_long=NULL){

        self::$ip_finish_long=empty($ip_finish_long)?NULL:$ip_finish_long;

        return true;

    }

    /**
     * @param float|NULL $latitude
     * @return bool
     */
    public  static function set_latitude_default(float $latitude=NULL){

        self::$latitude=empty($latitude)?NULL:$latitude;

        return true;

    }

    /**
     * @param float|NULL $longitude
     * @return bool
     */
    public  static function set_longitude_default(float $longitude=NULL){

        self::$longitude=empty($longitude)?NULL:$longitude;

        return true;

    }

    /**
     * @param int|NULL $radius
     * @return bool
     */
    public  static function set_radius_default(int $radius=NULL){

        self::$radius=empty($radius)?NULL:$radius;

        return true;

    }

}