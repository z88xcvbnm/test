<?php

namespace Core\Module\Geo;

class CountryValidation{

    /** @var int */
    private static $country_code_len        =2;
    
    /** @var int */
    private static $country_code_full_len   =3;
    
    /** @var int */
    private static $country_code_iso_len    =3;

    /** @var int */
    private static $country_name_len        =64;

    /**
     * @param string|NULL $country_code
     * @return bool
     */
    public  static function is_valid_country_code(string $country_code=NULL){

        if(empty($country_code))
            return false;

        $country_code_len=mb_strlen($country_code,'utf-8');

        return $country_code_len==self::$country_code_len;

    }

    /**
     * @param string|NULL $country_code_full
     * @return bool
     */
    public  static function is_valid_country_code_full(string $country_code_full=NULL){

        if(empty($country_code_full))
            return false;

        $country_code_full_len=mb_strlen($country_code_full,'utf-8');

        return $country_code_full_len==self::$country_code_full_len;

    }

    /**
     * @param string|NULL $country_code_iso
     * @return bool
     */
    public  static function is_valid_country_code_iso(string $country_code_iso=NULL){

        if(empty($country_code_iso))
            return false;

        $country_code_iso_len=mb_strlen($country_code_iso,'utf-8');

        return $country_code_iso_len==self::$country_code_iso_len;

    }

    /**
     * @param string|NULL $country_name
     * @return bool
     */
    public  static function is_valid_country_name(string $country_name=NULL){

        if(empty($country_name))
            return false;

        $country_name_len=mb_strlen($country_name,'utf-8');

        return $country_name_len==self::$country_name_len;

    }

    
}