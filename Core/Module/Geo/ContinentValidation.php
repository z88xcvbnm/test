<?php

namespace Core\Module\Geo;

use Core\Module\Exception\ParametersException;

class ContinentValidation{

    /** @var int */
    public  static $continent_code_len=2;

    /**
     * @param string|NULL $continent_code
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_valid_continent_code(string $continent_code=NULL){

        if(empty($continent_code)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Continent code is empty'
            );

            throw new ParametersException($error);

        }

        $continent_code_len=mb_strlen($continent_code,'utf-8');

        return $continent_code_len==self::$continent_code_len;

    }

}