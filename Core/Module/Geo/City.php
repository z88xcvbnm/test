<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Lang\LangConfig;

class City{

    /** @var string */
    public  static $table_name='_city';

    /** @var int */
    public  static $city_ID;
    
    /** @var string */
    public  static $city_name;
    
    /** @var string */
    public  static $city_postal_code;
    
    /** @var double */
    public  static $city_latitude;
    
    /** @var double */
    public  static $city_longitude;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){
        
        self::$city_ID              =NULL;
        self::$city_name            =NULL;
        self::$city_postal_code     =NULL;
        self::$city_latitude        =NULL;
        self::$city_longitude       =NULL;

        return true;
        
    }

    /**
     * @param int|NULL $city_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_city_ID(int $city_ID=NULL){

        if(empty($city_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'City ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($city_ID,'_city',0);

    }

    /**
     * @param int|NULL $city_ID
     * @return null|string
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_postal_code_from_city_ID(int $city_ID=NULL){

        if(empty(self::$city_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'City ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'postal_code'
            ),
            'table'=>'_city',
            'where'=>array(
                'id'    =>$city_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        self::$city_postal_code=empty($r[0]['postal_code'])?NULL:stripslashes($r[0]['postal_code']);

        return self::$city_postal_code;

    }

    /**
     * @param int|NULL $city_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_ID(int $city_ID=NULL){

        if(empty($city_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'City ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=[
            'select'=>[
                'country_id'
            ],
            'table'=>self::$table_name,
            'where'=>[
                'id'        =>$city_ID,
                'type'      =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['country_id'];

    }

    /**
     * @return null|string
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_postal_code_default(){

        if(!empty(self::$city_postal_code))
            return self::$city_postal_code;

        if(empty(self::$city_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default city ID is empty'
            );

            throw new ParametersException($error);

        }

        $city_postal_code=self::get_city_postal_code_from_city_ID(self::$city_ID);

        self::set_city_postal_code_default($city_postal_code);

        return $city_postal_code;

    }

    /**
     * @param int|NULL $city_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_coordinate_from_city_ID(int $city_ID=NULL){

        if(empty($city_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'City ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'latitude',
                'longitude'
            ),
            'table'=>'_city',
            'where'=>array(
                'id'        =>$city_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0];

    }

    /**
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_coordinate_default(){

        if(!empty(self::$city_latitude)&&!empty(self::$city_longitude))
            return array(
                'latitude'          =>self::$city_latitude,
                'longitude'         =>self::$city_longitude
            );

        if(empty(self::$city_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default city ID is empty'
            );

            throw new ParametersException($error);

        }

        $city_coordinate=self::get_city_coordinate_from_city_ID(self::$city_ID);

        self::set_city_latitude_default($city_coordinate['latitude']);
        self::set_city_longitude_default($city_coordinate['longitude']);

        return $city_coordinate;

    }

    /**
     * @param string|NULL $source
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_IDs_from_city(string $source=NULL){

        $country_ID     =NULL;
        $city_ID        =NULL;
        $country        =NULL;
        $city           =NULL;
        $result         =NULL;

        $list=mb_split(',',$source);

        if(count($list)==1){

            $city       =trim($list[0]);
            $city_ID    =CityLocalization::get_city_ID_from_name(mb_strtolower($city,'utf-8'));

            if(!empty($city_ID)){

                $city           =CityLocalization::get_city_name($city_ID,LangConfig::$lang_ID_default);
                $country_ID     =City::get_country_ID($city_ID);

                if(!empty($country_ID))
                    $country=CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);

                if(!empty($country_ID)&&!empty($city_ID))
                    $result=$country.', '.$city;
                else if(!empty($city_ID))
                    $result=$city;
                else
                    return NULL;

            }
            else
                return NULL;

        }
        else{

            $country        =trim($list[0]);
            $country_ID     =CountryLocalization::get_country_ID_from_name(mb_strtolower($country,'utf-8'));

            $city           =trim($list[1]);
            $city_ID        =CityLocalization::get_city_ID_from_name(mb_strtolower($city,'utf-8'));

            if(!empty($country_ID))
                $country=CountryLocalization::get_country_name($country_ID,LangConfig::$lang_ID_default);

            if(!empty($city_ID))
                $city=CityLocalization::get_city_name($city_ID,LangConfig::$lang_ID_default);

            if(!empty($country_ID)&&!empty($city_ID))
                $result=$country.', '.$city;
            else if(!empty($city_ID))
                $result=$city;
            else
                return NULL;

        }

        return[
            'country_ID'        =>$country_ID,
            'city_ID'           =>$city_ID,
            'country'           =>$country,
            'city'              =>$city,
            'result'            =>$result
        ];

    }

    /**
     * @param int|NULL $city_ID
     * @param bool $is_need_remove_city_localization
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_city_ID(int $city_ID=NULL,bool $is_need_remove_city_localization=true){

        if(empty($city_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'City ID is empty'
            );

            throw new ParametersException($error);

        }

        if($is_need_remove_city_localization)
            if(!CityLocalization::remove_city_localization($city_ID)){

                $error=array(
                    'title'     =>'DB query problem',
                    'info'      =>'City localization was not removew'
                );

                throw new DbQueryException($error);

            }

        if(!Db::delete_from_ID($city_ID,'_city',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'City was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $city_ID
     * @return bool
     */
    public  static function set_city_ID_default(int $city_ID=NULL){
        
        self::$city_ID=empty($city_ID)?NULL:$city_ID;

        return true;
        
    }

    /**
     * @param string|NULL $city_name
     * @return bool
     */
    public  static function set_city_name_default(string $city_name=NULL){
        
        self::$city_name=empty($city_name)?NULL:$city_name;

        return true;
        
    }

    /**
     * @param string|NULL $city_postal_code
     * @return bool
     */
    public  static function set_city_postal_code_default(string $city_postal_code=NULL){
        
        self::$city_postal_code=empty($city_postal_code)?NULL:$city_postal_code;

        return true;
        
    }

    /**
     * @param float|NULL $latitude
     * @return bool
     */
    public  static function set_city_latitude_default(float $latitude=NULL){
        
        self::$city_latitude=empty($latitude)?NULL:$latitude;

        return true;
        
    }

    /**
     * @param float|NULL $longitude
     * @return bool
     */
    public  static function set_city_longitude_default(float $longitude=NULL){
        
        self::$city_longitude=empty($longitude)?NULL:$longitude;

        return true;
        
    }

}