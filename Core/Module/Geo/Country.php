<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class Country{

    /** @var int */
    public  static $country_ID;

    /** @var string */
    public  static $country_code;

    /** @var string */
    public  static $country_code_full;

    /** @var string */
    public  static $country_code_iso;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$country_ID           =NULL;
        self::$country_code         =NULL;
        self::$country_code_full    =NULL;
        self::$country_code_iso     =NULL;

        return true;

    }

    /**
     * @param int|NULL $country_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_country_ID(int $country_ID=NULL){

        if(empty($country_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($country_ID,'_country',0);

    }

    /**
     * @param string|NULL $country_code
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_country_code(string $country_code=NULL){

        if(empty($country_code)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code is empty'
            );

            throw new ParametersException($error);

        }

        if(!ContinentValidation::is_valid_continent_code($country_code)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code is not valid'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'code'=>$country_code
        );

        return Db::isset_row('_country',0,$where_list);

    }

    /**
     * @param string|NULL $country_code_full
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_country_code_full(string $country_code_full=NULL){

        if(empty($country_code_full)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code full is empty'
            );

            throw new ParametersException($error);

        }

        if(!ContinentValidation::is_valid_continent_code($country_code_full)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code full is not valid'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'code_full'=>$country_code_full
        );

        return Db::isset_row('_country',0,$where_list);

    }

    /**
     * @param string|NULL $country_code
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_ID_from_country_code(string $country_code=NULL){

        if(empty($country_code)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code is empty'
            );

            throw new ParametersException($error);

        }

        if(!ContinentValidation::is_valid_continent_code($country_code)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code is not valid'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_country',
            'where'=>array(
                'code'  =>$country_code,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r))
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param string|NULL $country_code_full
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_ID_from_country_code_full(string $country_code_full=NULL){

        if(empty($country_code_full)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code full is empty'
            );

            throw new ParametersException($error);

        }

        if(!ContinentValidation::is_valid_continent_code_full($country_code_full)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code full is not valid'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_country',
            'where'=>array(
                'code_full'  =>$country_code_full,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r))
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param string|NULL $country_code_iso
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_ID_from_country_code_iso(string $country_code_iso=NULL){

        if(empty($country_code_iso)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code iso is empty'
            );

            throw new ParametersException($error);

        }

        if(!ContinentValidation::is_valid_continent_code_iso($country_code_iso)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code iso is not valid'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_country',
            'where'=>array(
                'code_iso'  =>$country_code_iso,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r))
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $country_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_code_from_country_ID(int $country_ID=NULL){

        if(empty($country_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country ID is empty'
            );

            throw new ParametersException($error);

        }
        
        $q=array(
            'select'=>array(
                'code'
            ),
            'table'=>'_country',
            'where'=>array(
                'id'    =>$country_ID,
                'type'  =>0
            ),
            'limit'=>1
        );
        
        $r=Db::select($q);
        
        if(empty($r))
            return NULL;
        
        return $r[0]['code'];        
        
    }

    /**
     * @param int|NULL $country_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_country_code_full_from_country_ID(int $country_ID=NULL){

        if(empty($country_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country ID is empty'
            );

            throw new ParametersException($error);

        }
        
        $q=array(
            'select'=>array(
                'code_full'
            ),
            'table'=>'_country',
            'where'=>array(
                'id'    =>$country_ID,
                'type'  =>0
            ),
            'limit'=>1
        );
        
        $r=Db::select($q);
        
        if(empty($r))
            return NULL;
        
        return $r[0]['code_full'];        
        
    }

    /**
     * @param string|NULL $country_code
     * @param string|NULL $country_code_full
     * @param string|NULL $country_code_iso
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_country(string $country_code=NULL,string $country_code_full=NULL,string $country_code_iso=NULL){

        if(empty($country_code)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code is empty'
            );

            throw new ParametersException($error);

        }

        if(!ContinentValidation::is_valid_continent_code($country_code)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code is not valid'
            );

            throw new ParametersException($error);

        }

        if(!ContinentValidation::is_valid_continent_code_full($country_code_full)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code full is not valid'
            );

            throw new ParametersException($error);

        }

        if(!ContinentValidation::is_valid_continent_code_iso($country_code_iso)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code iso is not valid'
            );

            throw new ParametersException($error);

        }

        $values_list=array(
            'code'  =>$country_code,
            'date'  =>'NOW()'
        );

        if(!empty($country_code_full))
            $values_list['code_full']=$country_code_full;

        if(!empty($country_code_iso))
            $values_list['code_iso']=$country_code_iso;

        $q=array(
            'table'     =>'_country',
            'values'    =>$values_list
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Country was not added',
                'data'      =>array(
                    'country_code'          =>$country_code,
                    'country_code_full'     =>$country_code_full,
                    'country_code_iso'      =>$country_code_iso
                )
            );

            throw new DbQueryException($error);

        }
        
        return $r[0]['id'];

    }

    /**
     * @param int|NULL $country_ID
     * @param bool $is_need_remove_country_localization
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_country_ID(int $country_ID=NULL,bool $is_need_remove_country_localization=true){

        if(empty($country_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country ID is empty'
            );

            throw new ParametersException($error);

        }
        
        if($is_need_remove_country_localization)
            if(CountryLocalization::remove_country_localization($country_ID)){
                
                $error=array(
                    'title'     =>'DB query problem',
                    'info'      =>'Country localization was not removed'
                );

                throw new DbQueryException($error);
                
            }
        
        if(!Db::delete_from_ID($country_ID,'_country',0)){
            
            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Country was not removed'
            );

            throw new DbQueryException($error);
            
        }
        
        return true;
        
    }

    /**
     * @param string|NULL $country_code
     * @param bool $is_need_remove_country_localization
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_country_code(string $country_code=NULL,bool $is_need_remove_country_localization=true){
        
        if(empty($country_code)){
            
            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code is empty'
            );

            throw new ParametersException($error);
            
        }
        
        if($is_need_remove_country_localization){
            
            $country_ID=self::get_country_ID_from_country_code($country_code);
            
            if(!empty($country_ID))
                if(CountryLocalization::remove_country_localization($country_ID)){
    
                    $error=array(
                        'title'     =>'DB query problem',
                        'info'      =>'Country localization was not removed'
                    );
    
                    throw new DbQueryException($error);
    
                }
            
        }
        
        $where_list=array(
            'code'=>$country_code
        );
        
        if(!Db::delete_from_where_list('_country',0,$where_list)){
            
            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Country was not removed'
            );

            throw new DbQueryException($error);
            
        }
        
        return true;
        
    }

    /**
     * @param string|NULL $country_code_full
     * @param bool $is_need_remove_country_localization
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_country_code_full(string $country_code_full=NULL,bool $is_need_remove_country_localization=true){
        
        if(empty($country_code_full)){
            
            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code full is empty'
            );

            throw new ParametersException($error);
            
        }
        
        if($is_need_remove_country_localization){
            
            $country_ID=self::get_country_ID_from_country_code_full($country_code_full);
            
            if(!empty($country_ID))
                if(CountryLocalization::remove_country_localization($country_ID)){
    
                    $error=array(
                        'title'     =>'DB query problem',
                        'info'      =>'Country localization was not removed'
                    );
    
                    throw new DbQueryException($error);
    
                }
            
        }
        
        $where_list=array(
            'code_full'=>$country_code_full
        );
        
        if(!Db::delete_from_where_list('_country',0,$where_list)){
            
            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Country was not removed'
            );

            throw new DbQueryException($error);
            
        }
        
        return true;
        
    }

    /**
     * @param string|NULL $country_code_iso
     * @param bool $is_need_remove_country_localization
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_country_code_iso(string $country_code_iso=NULL,bool $is_need_remove_country_localization=true){
        
        if(empty($country_code_iso)){
            
            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Country code full is empty'
            );

            throw new ParametersException($error);
            
        }
        
        if($is_need_remove_country_localization){
            
            $country_ID=self::get_country_ID_from_country_code_iso($country_code_iso);
            
            if(!empty($country_ID))
                if(CountryLocalization::remove_country_localization($country_ID)){
    
                    $error=array(
                        'title'     =>'DB query problem',
                        'info'      =>'Country localization was not removed'
                    );
    
                    throw new DbQueryException($error);
    
                }
            
        }
        
        $where_list=array(
            'code_iso'=>$country_code_iso
        );
        
        if(!Db::delete_from_where_list('_country',0,$where_list)){
            
            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Country was not removed'
            );

            throw new DbQueryException($error);
            
        }
        
        return true;
        
    }

    /**
     * @param int|NULL $country_ID
     * @return bool
     */
    public  static function set_country_ID_default(int $country_ID=NULL){

        self::$country_ID=empty($country_ID)?NULL:$country_ID;

        return true;

    }

    /**
     * @param string|NULL $country_code
     * @return bool
     */
    public  static function set_country_code_default(string $country_code=NULL){

        self::$country_code=empty($country_code)?NULL:$country_code;

        return true;

    }

    /**
     * @param string|NULL $country_code_full
     * @return bool
     */
    public  static function set_countr_code_full_default(string $country_code_full=NULL){

        self::$country_code_full=empty($country_code_full)?NULL:$country_code_full;

        return true;

    }

    /**
     * @param string|NULL $country_code_iso
     * @return bool
     */
    public  static function set_country_code_iso_default(string $country_code_iso=NULL){

        self::$country_code_iso=empty($country_code_iso)?NULL:$country_code_iso;

        return true;

    }

}