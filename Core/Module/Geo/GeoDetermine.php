<?php

namespace Core\Module\Geo;

use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Ip\Ip;
use Core\Module\Worktime\Worktime;

class GeoDetermine{

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_geo_ip_ID_to_default(){

        if(empty(Ip::$ip_address)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'IP address is empty'
            );

            throw new ParametersException($error);

        }

        $geo_ip_data=GeoIp::get_geo_ip_data_from_ip_address(Ip::$ip_address);

        if(empty($geo_ip_data))
            return false;

        if(count($geo_ip_data)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Geo IP data is empty'
            );

            throw new DbQueryException($error);

        }

        GeoIp::set_geo_ip_data_default($geo_ip_data);

        if(\Config::$is_geo_debug)
            print_r($geo_ip_data);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_geo_ID_to_default(){

        if(empty(Geo::$geo_ID))
            return true;

        $geo_data=Geo::get_geo_data(Geo::$geo_ID);

        if(count($geo_data)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Geo data is empty'
            );

            throw new DbQueryException($error);

        }

        if(\Config::$is_geo_debug)
            print_r($geo_data);

        Geo::set_geo_data_default($geo_data);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_geo_ip_ID_to_default();
        self::set_geo_ID_to_default();

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Worktime::set_timestamp_point('Geo Determine Start');

        self::set();

        Worktime::set_timestamp_point('Geo Determine Finish');

        return true;

    }

}