<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class RegionLocalization{

    /** @var int */
    public  static $region_localization_ID;

    /** @var int */
    public  static $lang_ID;

    /** @var string */
    public  static $region_name;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$region_localization_ID   =NULL;
        self::$lang_ID                  =NULL;
        self::$region_name              =NULL;

        return true;

    }

    /**
     * @param int|NULL $region_localization_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_region_localization_ID(int $region_localization_ID=NULL){

        if(empty($region_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Region localization ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($region_localization_ID,'_region_localization',0);

    }

    /**
     * @param int|NULL $region_ID
     * @param int|NULL $lang_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_region_localization(int $region_ID=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($region_ID))
            $error_info_list[]='Region ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'region_id'     =>$region_ID,
            'lang_id'       =>$lang_ID
        );

        return Db::isset_row('_region_localization',0,$where_list);

    }

    /**
     * @param int|NULL $region_ID
     * @param int|NULL $lang_ID
     * @return int|null
     * @throws ParametersException
     */
    public  static function get_region_localization_ID(int $region_ID=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($region_ID))
            $error_info_list[]='Region ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'region_id'     =>$region_ID,
            'lang_id'       =>$lang_ID
        );

        return Db::get_row_ID('_region_localization',0,$where_list);

    }

    /**
     * @param int|NULL $region_localization_ID
     * @return string|null
     * @throws ParametersException
     */
    public  static function get_region_name_from_region_localization_ID(int $region_localization_ID=NULL){

        if(empty($region_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Region localization ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_region_localization',
            'where'=>array(
                'id'    =>$region_localization_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param int|NULL $region_ID
     * @param int|NULL $lang_ID
     * @return string|null
     * @throws ParametersException
     */
    public  static function get_region_name(int $region_ID=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($region_ID))
            $error_info_list[]='Region ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_region_localization',
            'where'=>array(
                'region_id'     =>$region_ID,
                'lang_id'       =>$lang_ID,
                'type'          =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param int|NULL $region_ID
     * @param int|NULL $lang_ID
     * @param string|NULL $region_name
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function add_region_localization(int $region_ID=NULL,int $lang_ID=NULL,string $region_name=NULL){

        $error_info_list=[];

        if(empty($region_ID))
            $error_info_list[]='Region ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(empty($region_name))
            $error_info_list[]='Region name is empty';
        else if(!RegionValidation::is_valid_region_name($region_name))
            $error_info_list[]='Region name is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_region_localization',
            'values'    =>array(
                'region_id'     =>$region_ID,
                'lang_id'       =>$lang_ID,
                'name'          =>$region_name,
                'date'          =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Region localization was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $region_localization_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_region_localization_ID(int $region_localization_ID=NULL){

        if(empty($region_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Region localization ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($region_localization_ID,'_region_localization',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Region localization was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $region_ID
     * @param int|NULL $lang_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_region_localization(int $region_ID=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($region_ID))
            $error_info_list[]='Region ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'region_id'     =>$region_ID,
            'lang_id'       =>$lang_ID
        );

        if(!Db::delete_from_where_list('_region_localization',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Region localization was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $region_localization_ID
     */
    public  static function set_region_localization_ID_default(int $region_localization_ID=NULL){

        self::$region_localization_ID=empty($region_localization_ID)?NULL:$region_localization_ID;

    }

    /**
     * @param int|NULL $lang_ID
     */
    public  static function set_lang_ID(int $lang_ID=NULL){

        self::$lang_ID=empty($lang_ID)?NULL:$lang_ID;

    }

    /**
     * @param int|NULL $region_name
     */
    public  static function set_region_name(int $region_name=NULL){

        self::$region_name=empty($region_name)?NULL:$region_name;

    }

}