<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Lang\LangConfig;

class CityLocalization{

    /** @var int */
    public  static $city_localization_ID;

    /** @var int */
    public  static $lang_ID;

    /** @var string */
    public  static $city_name;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$city_localization_ID     =NULL;
        self::$lang_ID                  =NULL;
        self::$city_name                =NULL;

        return true;

    }

    /**
     * @param int|NULL $city_localization_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_city_localization_ID(int $city_localization_ID=NULL){

        if(empty($city_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'City localization ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($city_localization_ID,'_city_localization',0);

    }

    /**
     * @param int|NULL $city_ID
     * @param int|NULL $lang_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_city_localization(int $city_ID=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($city_ID))
            $error_info_list[]='City ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'city_id'   =>$city_ID,
            'lang_id'   =>$lang_ID
        );

        return Db::isset_row('_city_localization',0,$where_list);

    }

    /**
     * @param int|NULL $city_localization_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_name_from_city_localization_ID(int $city_localization_ID=NULL){

        if(empty($city_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'City localization ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table' =>'_city_localization',
            'where' =>array(
                'id'    =>$city_localization_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param int|NULL $city_ID
     * @param int|NULL $lang_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_name(int $city_ID=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($city_ID))
            $error_info_list[]='City ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table' =>'_city_localization',
            'where' =>array(
                'city_id'   =>$city_ID,
                'lang_id'   =>$lang_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param string|NULL $city_name
     * @param int|NULL $lang_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_ID(string $city_name=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($city_name))
            $error_info_list[]='City name is empty';

//        if(empty($lang_ID))
//            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table' =>'_city_localization',
            'where' =>array(
                'name'      =>$city_name,
                'type'      =>0
            ),
            'limit'=>1
        );

        if(!is_null($lang_ID))
            $q['where']['lang_id']=$lang_ID;

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param array $city_name_list
     * @param int|NULL $lang_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_ID_list_from_name_list(array $city_name_list=[],int $lang_ID=NULL){

        $error_info_list=[];

        if(count($city_name_list)==0)
            $error_info_list[]='City name is empty';

//        if(empty($lang_ID))
//            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'city_id',
                'name'
            ),
            'table' =>'_city_localization',
            'where' =>array(
                'name'      =>$city_name_list,
                'type'      =>0
            ),
            'limit'=>1
        );

        if(!is_null($lang_ID))
            $q['where']['lang_id']=$lang_ID;

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['city_id']]=$row['name'];

        return $list;

    }

    /**
     * @param array $city_ID_list
     * @param int|NULL $lang_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_ID_list_from_city_ID_list(array $city_ID_list=[],int $lang_ID=NULL){

        $error_info_list=[];

        if(count($city_ID_list)==0)
            $error_info_list[]='City ID list is empty';

//        if(empty($lang_ID))
//            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'city_id',
                'name'
            ),
            'table' =>'_city_localization',
            'where' =>array(
                'city_id'   =>$city_ID_list,
                'type'      =>0
            )
        );

        if(!is_null($lang_ID))
            $q['where']['lang_id']=$lang_ID;

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['city_id']]=$row['name'];

        return $list;

    }

    /**
     * @param string|NULL $city_name
     * @param int|NULL $lang_ID
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_ID_from_name(string $city_name=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($city_name))
            $error_info_list[]='City name is empty';

//        if(empty($lang_ID))
//            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'city_id'
            ),
            'table' =>'_city_localization',
            'where' =>array(
                'name'      =>[
                    'transform'     =>'lower',
                    'value'         =>$city_name
                ],
                'type'      =>0
            ),
            'limit'=>1
        );

        if(!is_null($lang_ID))
            $q['where']['lang_id']=$lang_ID;

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['city_id'];

    }

    /**
     * @param int|NULL $city_ID
     * @param int|NULL $lang_ID
     * @param string|NULL $city_name
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_city_name(int $city_ID=NULL,int $lang_ID=NULL,string $city_name=NULL){

        $error_info_list=[];

        if(empty($city_ID))
            $error_info_list[]='City ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(empty($city_name))
            $error_info_list[]='City name is empty';
        else if(!CityValidation::is_valid_city_name($city_name))
            $error_info_list[]='City name is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_city_localization',
            'values'    =>array(
                'city_id'   =>$city_ID,
                'lang_id'   =>$lang_ID,
                'name'      =>$city_name,
                'date'      =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'City was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_city_name_default(){

        if(empty(City::$city_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default city ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!empty(self::$city_localization_ID))
            return self::get_city_name_from_city_localization_ID(self::$city_localization_ID);
        else{

            if(empty(self::$lang_ID))
                self::set_lang_ID_default(LangConfig::$lang_ID_default);

            return self::get_city_name(City::$city_ID,self::$lang_ID);

        }

    }

    /**
     * @param int|NULL $city_localization_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_city_localization_ID(int $city_localization_ID=NULL){

        if(empty($city_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'City localization ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($city_localization_ID,'_city_localization',0)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'City was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $city_ID
     * @param int|NULL $lang_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_city_localization(int $city_ID=NULL,int $lang_ID=NULL){

        if(empty($city_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'City ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'city_id'   =>$city_ID
        );

        if(!empty($lang_ID))
            $where_list['lang_id']=$lang_ID;

        if(!Db::delete_from_where_list('_city_localization',0,$where_list)){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'City was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $city_localization_ID
     * @return bool
     */
    public  static function set_city_localization_ID_default(int $city_localization_ID=NULL){

        self::$city_localization_ID=empty($city_localization_ID)?NULL:$city_localization_ID;

        return true;

    }

    /**
     * @param int|NULL $lang_ID
     * @return bool
     */
    public  static function set_lang_ID_default(int $lang_ID=NULL){

        self::$lang_ID=empty($lang_ID)?NULL:$lang_ID;

        return true;

    }

    /**
     * @param string|NULL $city_name
     * @return bool
     */
    public  static function set_city_name_default(string $city_name=NULL){

        self::$city_name=empty($city_name)?NULL:$city_name;

        return true;

    }

}
