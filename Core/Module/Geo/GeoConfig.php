<?php

namespace Core\Module\Geo;

use Core\Module\Exception\ParametersException;

class GeoConfig{

    /** @var array */
    public  static $lang_key_list=array(
        'ru',
        'en',
        'de',
        'fr',
        'pt',
        'ja',
        'zh',
    );

    /**
     * @param string|NULL $lang_key
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_lang_key(string $lang_key=NULL){

        if(empty($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is empty'
            );

            throw new ParametersException($error);

        }

        return array_search($lang_key,self::$lang_key_list)!==false;

    }

    /**
     * @param string|NULL $lang_key
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_lang_key_in_lang_key_list(string $lang_key=NULL){

        if(empty($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is empty'
            );

            throw new ParametersException($error);

        }

        if(!self::isset_lang_key($lang_key))
            self::$lang_key_list[]=$lang_key;

        return true;

    }

    /**
     * @param string|NULL $lang_key
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_lang_key_from_lang_key_list(string $lang_key=NULL){

        if(empty($lang_key)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Lang key is empty'
            );

            throw new ParametersException($error);

        }

        if(!self::isset_lang_key($lang_key))
            return false;

        return true;

    }

}