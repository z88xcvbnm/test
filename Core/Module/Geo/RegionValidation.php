<?php

namespace Core\Module\Geo;

class RegionValidation{

    /** @var int */
    private static $region_code_len     =2;

    /** @var int */
    private static $region_name_len     =64;

    /**
     * @param string|NULL $region_code
     * @return bool
     */
    public  static function is_valid_region_code(string $region_code=NULL){

        if(empty($region_code))
            return false;

        $region_code_len=mb_strlen($region_code,'utf-8');

        return $region_code_len==self::$region_code_len;

    }
    
    /**
     * @param string|NULL $region_name
     * @return bool
     */
    public  static function is_valid_region_name(string $region_name=NULL){

        if(empty($region_name))
            return false;

        $region_name_len=mb_strlen($region_name,'utf-8');

        return $region_name_len==self::$region_name_len;

    }
    
}