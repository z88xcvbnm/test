<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Timezone\Timezone;

class Geo{

    /** @var int */
    public  static $geo_ID;

    /** @var int */
    public  static $country_geo_ID;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$geo_ID           =NULL;
        self::$country_geo_ID   =NULL;

        return true;

    }

    /**
     * @param int|NULL $geo_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_geo_ID(int $geo_ID=NULL){

        if(empty($geo_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Geo ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($geo_ID,'_geo',0);

    }

    /**
     * @param int|NULL $continent_ID
     * @param int|NULL $country_ID
     * @param int|NULL $region_ID
     * @param int|NULL $city_ID
     * @param int|NULL $timezone_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_geo_ID(int $continent_ID=NULL,int $country_ID=NULL,int $region_ID=NULL,int $city_ID=NULL,int $timezone_ID=NULL){

        if(empty($continent_ID)&&empty($country_ID)&&empty($region_ID)&&empty($city_ID)&&empty($timezone_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }
        
        $where_list=[];
        
        if(!empty($continent_ID))
            $where_list['continent_id']=$continent_ID;
        
        if(!empty($country_ID))
            $where_list['country_id']=$country_ID;
        
        if(!empty($region_ID))
            $where_list['region_id']=$region_ID;
        
        if(!empty($city_ID))
            $where_list['city_id']=$city_ID;
        
        if(!empty($timezone_ID))
            $where_list['timezone_id']=$timezone_ID;
        
        if(count($where_list)==0)
            return NULL;
        
        return Db::get_row_ID('_geo',0,$where_list);
        
    }

    /**
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_geo_ID_default(){

        if(empty(Continent::$continent_ID)&&empty(Country::$country_ID)&&empty(Region::$region_ID)&&empty(City::$city_ID)&&empty(Timezone::$timezone_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All default parameters are empty'
            );

            throw new ParametersException($error);

        }

        return self::get_geo_ID(Continent::$continent_ID,Country::$country_ID,Region::$region_ID,City::$city_ID,Timezone::$timezone_ID);

    }

    /**
     * @param int|NULL $geo_ID
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_geo_data(int $geo_ID=NULL){

        if(empty($geo_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Geo ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'continent_id',
                'country_id',
                'region_id',
                'city_id',
                'timezone_id'
            ),
            'table'=>'_geo',
            'where'=>array(
                'id'    =>$geo_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'continent_ID'      =>$r[0]['continent_id'],
            'country_ID'        =>$r[0]['country_id'],
            'region_ID'         =>$r[0]['region_id'],
            'city_ID'           =>$r[0]['city_id'],
            'timezone_ID'       =>$r[0]['timezone_id']
        );

    }

    /**
     * @param int|NULL $continent_ID
     * @param int|NULL $country_ID
     * @param int|NULL $region_ID
     * @param int|NULL $city_ID
     * @param int|NULL $timezone_ID
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_geo(int $continent_ID=NULL,int $country_ID=NULL,int $region_ID=NULL,int $city_ID=NULL,int $timezone_ID=NULL){

        if(empty($continent_ID)&&empty($country_ID)&&empty($region_ID)&&empty($city_ID)&&empty($timezone_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $value_list=[];

        if(!empty($continent_ID))
            $value_list['continent_id']=$continent_ID;

        if(!empty($country_ID))
            $value_list['country_id']=$country_ID;

        if(!empty($region_ID))
            $value_list['region_id']=$region_ID;

        if(!empty($city_ID))
            $value_list['city_id']=$city_ID;

        if(!empty($timezone_ID))
            $value_list['timezone_id']=$timezone_ID;

        $q=array(
            'table'     =>'_geo',
            'values'    =>$value_list
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Geo was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];
        
    }

    /**
     * @param int|NULL $geo_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_geo_ID(int $geo_ID=NULL){

        if(empty($geo_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Geo ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($geo_ID,'_geo',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Geo ID was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $geo_ID
     * @return bool
     */
    public  static function set_geo_ID_default(int $geo_ID=NULL){

        self::$geo_ID=empty($geo_ID)?NULL:$geo_ID;

        return true;

    }

    /**
     * @param int|NULL $country_geo_ID
     * @return bool
     */
    public  static function set_country_geo_ID_default(int $country_geo_ID=NULL){

        self::$country_geo_ID=empty($country_geo_ID)?NULL:$country_geo_ID;

        return true;

    }

    /**
     * @param array $geo_data
     * @return bool
     */
    public  static function set_geo_data_default(array $geo_data=[]){

        Continent::reset_data();
        Country::reset_data();
        Region::reset_data();
        City::reset_data();
        Timezone::reset_data();

        if(!empty($geo_data['continent_ID']))
            Continent::set_continent_ID_default($geo_data['continent_ID']);

        if(!empty($geo_data['country_ID']))
            Country::set_country_ID_default($geo_data['country_ID']);

        if(!empty($geo_data['region_ID']))
            Region::set_region_ID_default($geo_data['region_ID']);

        if(!empty($geo_data['city_ID']))
            City::set_city_ID_default($geo_data['city_ID']);

        if(!empty($geo_data['timezone_ID']))
            Timezone::set_timezone_ID_default($geo_data['timezone_ID']);

        return true;

    }

}