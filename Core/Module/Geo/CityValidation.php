<?php

namespace Core\Module\Geo;

class CityValidation{

    /** @var int */
    private static $city_name_len=128;

    /**
     * @param string|NULL $city_name
     * @return bool
     */
    public  static function is_valid_city_name(string $city_name=NULL){

        if(empty($city_name))
            return false;

        $city_name_len=mb_strlen($city_name,'utf-8');

        return $city_name_len==self::$city_name_len;

    }

}