<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Ip\Ip;

class GeoUpdate{

    /** @var string */
    private static $dir                     ='Core/Resources/Localization';

    /** @var string */
    private static $file_ip4_name           ='GeoLite2-City-Blocks-IPv4.csv';

    /** @var string */
    private static $file_ip6_name           ='GeoLite2-City-Blocks-IPv6.csv';

    /** @var array */
    private static $file_city_name_list     =array(
        'ru'        =>'GeoLite2-City-Locations-ru.csv',
        'de'        =>'GeoLite2-City-Locations-de.csv',
        'en'        =>'GeoLite2-City-Locations-en.csv',
        'es'        =>'GeoLite2-City-Locations-es.csv',
        'fr'        =>'GeoLite2-City-Locations-fr.csv',
        'ja'        =>'GeoLite2-City-Locations-ja.csv',
        'pt'        =>'GeoLite2-City-Locations-pt.csv',
        'zh'        =>'GeoLite2-City-Locations-zh.csv'
    );

    /**
     * @return bool
     */
    private static function isset_file_list(){

        if(!file_exists(self::$dir.'/'.self::$file_ip4_name))
            return false;

        if(!file_exists(self::$dir.'/'.self::$file_ip6_name))
            return false;

        foreach(self::$file_city_name_list as $file_name)
            if(!file_exists(self::$dir.'/'.$file_name))
                return false;

        return true;

    }

    /**
     * @param null $lang
     * @param null $code
     * @return array
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_continent($lang=NULL,$code=NULL){

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id'
                ),
                array(
                    'column'=>'name_'.$lang
                )
            ),
            'table'=>'_continent',
            'where'=>array(
                array(
                    'column'=>'code',
                    'value'=>$code
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return array(NULL,NULL);

        return array(
            $r[0]['id'],
            $r[0]['name_'.$lang]
        );

    }

    /**
     * @param null $lang
     * @param null $code
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_country($lang=NULL,$code=NULL){

        $error_info_list=[];

        if(empty($lang))
            $error_info_list[]='Lang is empty';

        if(empty($code))
            $error_info_list[]='Code is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id'
                ),
                array(
                    'column'=>'name_'.$lang
                )
            ),
            'table'=>'_country',
            'where'=>array(
                array(
                    'column'=>'code',
                    'value'=>$code
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return array(NULL,NULL);

        return array(
            $r[0]['id'],
            $r[0]['name_'.$lang]
        );

    }

    /**
     * @param null $lang
     * @param null $country_ID
     * @param null $name
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_city($lang=NULL,$country_ID=NULL,$name=NULL){

        $error_info_list=[];

        if(empty($lang))
            $error_info_list[]='Lang is empty';

        if(empty($country_ID))
            $error_info_list[]='Country ID is empty';

        if(empty($name))
            $error_info_list[]='Name is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id'
                ),
                array(
                    'column'=>'name_'.$lang
                )
            ),
            'table'=>'_city',
            'where'=>array(
                array(
                    'column'=>'country_id',
                    'value'=>$country_ID,
                ),
                array(
                    'column'=>'name_'.$lang,
                    'value'=>$name
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return array(NULL,NULL);

        return array(
            $r[0]['id'],
            $r[0]['name_'.$lang]
        );

    }

    /**
     * @param null $lang
     * @param null $country_ID
     * @param null $name
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_region($lang=NULL,$country_ID=NULL,$name=NULL){

        $error_info_list=[];

        if(empty($lang))
            $error_info_list[]='Lang is empty';

        if(empty($country_ID))
            $error_info_list[]='Country ID is empty';

        if(empty($name))
            $error_info_list[]='Name is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id'
                ),
                array(
                    'column'=>'name_'.$lang
                )
            ),
            'table'=>'_region',
            'where'=>array(
                array(
                    'column'=>'country_id',
                    'value'=>$country_ID,
                ),
                array(
                    'column'=>'name_'.$lang,
                    'value'=>$name
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return array(NULL,NULL);

        return array(
            $r[0]['id'],
            $r[0]['name_'.$lang]
        );

    }

    /**
     * @param null $name
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_timezone($name=NULL){

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id'
                )
            ),
            'table'=>'_timezone',
            'where'=>array(
                array(
                    'column'=>'name',
                    'value'=>$name,
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return count($r)==0?NULL:$r[0]['id'];

    }

    /**
     * @param null $geo_ID
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_geo($geo_ID=NULL){

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id'
                )
            ),
            'table'=>'_geo',
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$geo_ID,
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return count($r)!=0;

    }

    /**
     * @param null $network
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_geo_ip($network=NULL){

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id'
                )
            ),
            'table'=>'_geo_ip',
            'where'=>array(
                array(
                    'column'=>'network',
                    'value'=>$network,
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return count($r)==0?NULL:$r[0]['id'];

    }

    /**
     * @param null $geo_ID
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_geo_data($geo_ID=NULL){

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id',
                ),
                array(
                    'column'=>'continent_id',
                ),
                array(
                    'column'=>'country_id',
                ),
                array(
                    'column'=>'region_id',
                ),
                array(
                    'column'=>'city_id',
                ),
                array(
                    'column'=>'timezone_id',
                )
            ),
            'table'=>'_geo',
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$geo_ID,
                ),
                array(
                    'column'=>'type',
                    'value'=>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return array(
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL
            );

        return array(
            $r[0]['id'],
            $r[0]['continent_id'],
            $r[0]['country_id'],
            $r[0]['region_id'],
            $r[0]['city_id'],
            $r[0]['timezone_id']
        );

    }

    /**
     * @param null $lang
     * @param null $code
     * @param null $name
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_continent($lang=NULL,$code=NULL,$name=NULL){

        $q=array(
            'table'=>'_continent',
            'values'=>array(
                array(
                    'code'          =>$code,
                    'name_'.$lang   =>$name,
                    'type'          =>0
                )
            )
        );

        $r=Db::insert($q);

        return count($r)==0?NULL:$r[0]['id'];

    }

    /**
     * @param null $lang
     * @param null $continent_ID
     * @param null $code
     * @param null $name
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_country($lang=NULL,$continent_ID=NULL,$code=NULL,$name=NULL){

        $q=array(
            'table'=>'_country',
            'values'=>array(
                array(
                    'continent_id'      =>$continent_ID,
                    'code'              =>$code,
                    'name_'.$lang       =>$name,
                    'type'              =>0
                )
            )
        );

        $r=Db::insert($q);

        return count($r)==0?NULL:$r[0]['id'];

    }

    /**
     * @param null $lang
     * @param null $continent_ID
     * @param null $country_ID
     * @param null $code
     * @param null $name
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_region($lang=NULL,$continent_ID=NULL,$country_ID=NULL,$code=NULL,$name=NULL){

        $q=array(
            'table'=>'_region',
            'values'=>array(
                array(
                    'continent_id'      =>$continent_ID,
                    'country_id'        =>$country_ID,
                    'code'              =>$code,
                    'name_'.$lang       =>$name,
                    'type'              =>0
                )
            )
        );

        $r=Db::insert($q);

        return count($r)==0?NULL:$r[0]['id'];

    }

    /**
     * @param null $lang
     * @param null $continent_ID
     * @param null $country_ID
     * @param null $region_ID
     * @param null $timezone_ID
     * @param null $name
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_city($lang=NULL,$continent_ID=NULL,$country_ID=NULL,$region_ID=NULL,$timezone_ID=NULL,$name=NULL){

        $q=array(
            'table'=>'_city',
            'values'=>array(
                array(
                    'continent_id'      =>$continent_ID,
                    'country_id'        =>$country_ID,
                    'region_id'         =>$region_ID,
                    'timezone_id'       =>$timezone_ID,
                    'name_'.$lang       =>$name,
                    'type'              =>0
                )
            )
        );

        $r=Db::insert($q);

        return count($r)==0?NULL:$r[0]['id'];

    }

    /**
     * @param null $geo_ID
     * @param null $continent_ID
     * @param null $country_ID
     * @param null $region_ID
     * @param null $city_ID
     * @param null $timezone_ID
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_geo($geo_ID=NULL,$continent_ID=NULL,$country_ID=NULL,$region_ID=NULL,$city_ID=NULL,$timezone_ID=NULL){

        $q=array(
            'table'=>'_geo',
            'values'=>array(
                array(
                    'id'                =>$geo_ID,
                    'continent_id'      =>$continent_ID,
                    'country_id'        =>$country_ID,
                    'region_id'         =>$region_ID,
                    'city_id'           =>$city_ID,
                    'timezone_id'       =>$timezone_ID,
                    'type'              =>0
                )
            )
        );

        $r=Db::insert($q);

        return count($r)==0?NULL:$r[0]['id'];

    }

    /**
     * @param null $geo_ID
     * @param null $country_geo_ID
     * @param null $network
     * @param null $ip_start_long
     * @param null $ip_finish_long
     * @param null $postal_code
     * @param null $latitude
     * @param null $longitude
     * @param null $radius
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function add_geo_ip($geo_ID=NULL,$country_geo_ID=NULL,$network=NULL,$ip_start_long=NULL,$ip_finish_long=NULL,$postal_code=NULL,$latitude=NULL,$longitude=NULL,$radius=NULL){

        $q=array(
            'table'=>'_geo_ip',
            'values'=>array(
                array(
                    'geo_id'            =>$geo_ID,
                    'country_geo_id'    =>$country_geo_ID,
                    'network'           =>$network,
                    'ip_start_long'     =>$ip_start_long,
                    'ip_finish_long'    =>$ip_finish_long,
                    'postal_code'       =>$postal_code,
                    'latitude'          =>$latitude,
                    'longitude'         =>$longitude,
                    'radius'            =>$radius,
                    'date_create'       =>'NOW()',
                    'type'              =>0
                )
            )
        );

        $r=Db::insert($q);

        return count($r)==0?NULL:$r[0]['id'];

    }

    /**
     * @param int|NULL $continent_ID
     * @param string|NULL $lang
     * @param string|NULL $name
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_continent(int $continent_ID=NULL,string $lang=NULL,string $name=NULL){

        $q=array(
            'table'=>'_continent',
            'set'=>array(
                array(
                    'column'=>'name_'.$lang,
                    'value'=>$name
                )
            ),
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$continent_ID
                )
            )
        );

        return Db::update($q);

    }

    /**
     * @param int|NULL $country_ID
     * @param string|NULL $lang
     * @param string|NULL $name
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_country(int $country_ID=NULL,string $lang=NULL,string $name=NULL){

        $q=array(
            'table'=>'_country',
            'set'=>array(
                array(
                    'column'=>'name_'.$lang,
                    'value'=>$name
                )
            ),
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$country_ID
                )
            )
        );

        return Db::update($q);

    }

    /**
     * @param int|NULL $region_ID
     * @param string|NULL $lang
     * @param string|NULL $name
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_region(int $region_ID=NULL,string $lang=NULL,string $name=NULL){

        $q=array(
            'table'=>'_region',
            'set'=>array(
                array(
                    'column'=>'name_'.$lang,
                    'value'=>$name
                )
            ),
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$region_ID
                )
            )
        );

        return Db::update($q);

    }

    /**
     * @param int|NULL $city_ID
     * @param string|NULL $lang
     * @param string|NULL $name
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_city(int $city_ID=NULL,string $lang=NULL,string $name=NULL){

        $q=array(
            'table'=>'_city',
            'set'=>array(
                array(
                    'column'=>'name_'.$lang,
                    'value'=>$name
                )
            ),
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$city_ID
                )
            )
        );

        return Db::update($q);

    }

    /**
     * @param int|NULL $city_ID
     * @param null $postal_code
     * @param float|NULL $latitude
     * @param float|NULL $longitude
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_city_place(int $city_ID=NULL,$postal_code=NULL,float $latitude=NULL,float $longitude=NULL){

        $q=array(
            'table'=>'_city',
            'set'=>array(
                array(
                    'column'=>'postal_code',
                    'value'=>$postal_code
                ),
                array(
                    'column'=>'latitude',
                    'value'=>$latitude
                ),
                array(
                    'column'=>'longitude',
                    'value'=>$longitude
                ),
                array(
                    'column'=>'date_update',
                    'value'=>'NOW()'
                )
            ),
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$city_ID
                )
            )
        );

        return Db::update($q);

    }

    /**
     * @param int|NULL $geo_ID
     * @param int|NULL $continent_ID
     * @param int|NULL $country_ID
     * @param int|NULL $region_ID
     * @param int|NULL $city_ID
     * @param int|NULL $timezone_ID
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_geo(int $geo_ID=NULL,int $continent_ID=NULL,int $country_ID=NULL,int $region_ID=NULL,int $city_ID=NULL,int $timezone_ID=NULL){

        $q=array(
            'table'=>'_geo',
            'set'=>array(
                array(
                    'column'=>'continent_id',
                    'value'=>$continent_ID
                ),
                array(
                    'column'=>'country_id',
                    'value'=>$country_ID
                ),
                array(
                    'column'=>'region_id',
                    'value'=>$region_ID
                ),
                array(
                    'column'=>'city_id',
                    'value'=>$city_ID
                ),
                array(
                    'column'=>'timezone_id',
                    'value'=>$timezone_ID
                ),
                array(
                    'column'=>'date_update',
                    'value'=>'NOW()'
                ),
            ),
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$geo_ID
                )
            )
        );

        return Db::update($q);

    }

    /**
     * @param int|NULL $geo_ip_ID
     * @param int|NULL $geo_ID
     * @param int|NULL $country_geo_ID
     * @param null $network
     * @param null $ip_start_long
     * @param null $ip_finish_long
     * @param null $postal_code
     * @param float|NULL $latitude
     * @param float|NULL $longitude
     * @param int|NULL $radius
     * @return array|bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function update_geo_ip(int $geo_ip_ID=NULL,int $geo_ID=NULL,int $country_geo_ID=NULL,$network=NULL,$ip_start_long=NULL,$ip_finish_long=NULL,$postal_code=NULL,float $latitude=NULL,float $longitude=NULL,int $radius=NULL){

        $q=array(
            'table'=>'_geo_ip',
            'set'=>array(
                array(
                    'column'=>'geo_id',
                    'value'=>$geo_ID
                ),
                array(
                    'column'=>'country_geo_id',
                    'value'=>$country_geo_ID
                ),
                array(
                    'column'=>'network',
                    'value'=>$network
                ),
                array(
                    'column'=>'ip_start_long',
                    'value'=>$ip_start_long
                ),
                array(
                    'column'=>'ip_finish_long',
                    'value'=>$ip_finish_long
                ),
                array(
                    'column'=>'postal_code',
                    'value'=>$postal_code
                ),
                array(
                    'column'=>'latitude',
                    'value'=>$latitude
                ),
                array(
                    'column'=>'longitude',
                    'value'=>$longitude
                ),
                array(
                    'column'=>'radius',
                    'value'=>$radius
                ),
                array(
                    'column'=>'date_update',
                    'value'=>'NOW()'
                )
            ),
            'where'=>array(
                array(
                    'column'=>'id',
                    'value'=>$geo_ip_ID
                )
            )
        );

        return Db::update($q);

    }

    /**
     * @param string|NULL $lang
     * @param null $code
     * @param string|NULL $name
     * @return mixed|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_continent_ID(string $lang=NULL,$code=NULL,string $name=NULL){

        list($continent_ID,$isset_name)=self::isset_continent($lang,$code);

        if(empty($continent_ID))
            $continent_ID=self::add_continent($lang,$code,$name);
        else if(empty($isset_name))
            self::update_continent($continent_ID,$lang,$name);

        return $continent_ID;

    }

    /**
     * @param string|NULL $lang
     * @param int|NULL $continent_ID
     * @param null $code
     * @param string|NULL $name
     * @return mixed|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_country_ID(string $lang=NULL,int $continent_ID=NULL,$code=NULL,string $name=NULL){

        list($country_ID,$isset_name)=self::isset_country($lang,$code);

        if(empty($country_ID))
            $country_ID=self::add_country($lang,$continent_ID,$code,$name);
        else if(empty($isset_name))
            self::update_country($country_ID,$lang,$name);

        return $country_ID;

    }

    /**
     * @param string|NULL $lang
     * @param int|NULL $continent_ID
     * @param int|NULL $country_ID
     * @param null $code
     * @param string|NULL $name
     * @return mixed|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_region_ID(string $lang=NULL,int $continent_ID=NULL,int $country_ID=NULL,$code=NULL,string $name=NULL){

        list($region_ID,$isset_name)=self::isset_region($lang,$country_ID,$name);

        if(empty($region_ID))
            $region_ID=self::add_region($lang,$continent_ID,$country_ID,$code,$name);
        else if(empty($isset_name))
            self::update_region($region_ID,$lang,$name);

        return $region_ID;

    }

    /**
     * @param string|NULL $lang
     * @param int|NULL $continent_ID
     * @param int|NULL $country_ID
     * @param int|NULL $region_ID
     * @param int|NULL $timezone_ID
     * @param string|NULL $name
     * @return mixed|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_city_ID(string $lang=NULL,int $continent_ID=NULL,int $country_ID=NULL,int $region_ID=NULL,int $timezone_ID=NULL,string $name=NULL){

        list($city_ID,$isset_name)=self::isset_city($lang,$country_ID,$name);

        if(empty($city_ID))
            $city_ID=self::add_city($lang,$continent_ID,$country_ID,$region_ID,$timezone_ID,$name);
        else if(empty($isset_name))
            self::update_city($city_ID,$lang,$name);

        return $city_ID;

    }

    /**
     * @param string|NULL $name
     * @return null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_timezone_ID(string $name=NULL){

        return self::isset_timezone($name);

    }

    /**
     * @param int|NULL $geo_ID
     * @param int|NULL $continent_ID
     * @param int|NULL $country_ID
     * @param int|NULL $region_ID
     * @param int|NULL $city_ID
     * @param int|NULL $timezone_ID
     * @return int
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function get_geo_ID(int $geo_ID=NULL,int $continent_ID=NULL,int $country_ID=NULL,int $region_ID=NULL,int $city_ID=NULL,int $timezone_ID=NULL){
        
        if(self::isset_geo($geo_ID))
            self::update_geo($geo_ID,$continent_ID,$country_ID,$region_ID,$city_ID,$timezone_ID);
        else
            self::add_geo($geo_ID,$continent_ID,$country_ID,$region_ID,$city_ID,$timezone_ID);

        return $geo_ID;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_file_ip4(){

        $file_object=fopen(self::$dir.'/'.self::$file_ip4_name,"r");

        $cols_name_list=mb_split(",",trim(fgets($file_object)));

        while(!feof($file_object)){

            $value_list     =mb_split(",",trim(fgets($file_object)));
            $temp           =[];

            foreach($value_list as $index=>$value){

                $key        =trim($cols_name_list[$index]);
                $value      =trim($value);

                switch($key){

                    case'network':{

                        list($ip_start,$ip_finish)=Ip::get_cidr_to_ip_range($value);

                        $temp['ip_start']           =$ip_start;
                        $temp['ip_finish']          =$ip_finish;
                        $temp['ip_start_long']      =ip2long($ip_start);
                        $temp['ip_finish_long']     =ip2long($ip_finish);

                        $temp[$key]                 =$value;

                        break;

                    }

                    default:{

                        $temp[$key]=$value;

                        break;

                    }

                }

            }

            $network                =$temp['network'];
//            $geo_ID                 =empty($temp['geoname_id'])?NULL:(int)$temp['geoname_id'];
            $country_geo_ID         =empty($temp['registered_country_geoname_id'])?NULL:(int)$temp['registered_country_geoname_id'];
            $postal_code            =empty($temp['postal_code'])?NULL:$temp['postal_code'];
            $latitude               =empty($temp['latitude'])?NULL:(double)$temp['latitude'];
            $longitude              =empty($temp['longitude'])?NULL:(double)$temp['longitude'];
            $radius                 =empty($temp['radius'])?NULL:(int)$temp['radius'];

            list($geo_ID,$continent_ID,$country_ID,$region_ID,$city_ID,$timezone_ID)=self::get_geo_data((int)$temp['geoname_id']);

            if(!empty($geo_ID)){

                if(!empty($city_ID))
                    self::update_city_place($city_ID,$postal_code,$latitude,$longitude);

                $geo_ip_ID=self::isset_geo_ip($network);

                if(empty($geo_ip_ID))
                    self::add_geo_ip($geo_ID,$country_geo_ID,$network,$temp['ip_start_long'],$temp['ip_finish_long'],$postal_code,$latitude,$longitude,$radius);
                else
                    self::update_geo_ip($geo_ip_ID,$geo_ID,$country_geo_ID,$network,$temp['ip_start_long'],$temp['ip_finish_long'],$postal_code,$latitude,$longitude,$radius);

            }

        }

        fclose($file_object);

        return true;

    }

    /**
     * set file city list
     */
    private static function set_file_city_list(){

        foreach(self::$file_city_name_list as $lang=>$file_name){

            $file_object        =fopen(self::$dir.'/'.$file_name,"r");
            $cols_name_list     =mb_split(';',preg_replace('/,(?=(?:[^"]*$)|(?:[^"]*"[^"]*"[^"]*)*$)/x',';',trim(fgets($file_object))));

            $n=0;

            while(!feof($file_object)){

                $row=trim(fgets($file_object));

                if(empty($row))
                    break;

                $value_list     =mb_split(';',str_replace('"','',$row));
                $temp           =[];

                if(count($value_list)!=count($cols_name_list)){

                    $value_list=mb_split(';',preg_replace('/,(?=(?:[^"]*$)|(?:[^"]*"[^"]*"[^"]*)*$)/x',';',$row));

                    if(count($value_list)!=count($cols_name_list))
                        break;

                }

                foreach($value_list as $index=>$value){

                    $key            =trim($cols_name_list[$index]);
                    $value          =str_replace('"','',trim($value));

                    $temp[$key]     =$value;

                }

                $geo_ID=NULL;

                list($geo_ID,$continent_ID,$country_ID,$region_ID,$city_ID,$timezone_ID)=self::get_geo_data((int)$temp['geoname_id']);

                if(empty($geo_ID)){

                    $continent_ID   =empty($temp['continent_name'])?NULL:self::get_continent_ID($lang,strtolower($temp['continent_code']),$temp['continent_name']);
                    $country_ID     =empty($temp['country_name'])?NULL:self::get_country_ID($lang,$continent_ID,strtolower($temp['country_iso_code']),$temp['country_name']);
                    $region_ID      =empty($temp['subdivision_1_name'])?NULL:self::get_region_ID($lang,$continent_ID,$country_ID,strtolower($temp['subdivision_1_iso_code']),$temp['subdivision_1_name']);
                    $timezone_ID    =empty($temp['time_zone'])?NULL:self::get_timezone_ID($temp['time_zone']);
                    $city_ID        =empty($temp['city_name'])?NULL:self::get_city_ID($lang,$continent_ID,$country_ID,$region_ID,$timezone_ID,$temp['city_name']);

                    if(!empty($temp['geoname_id']))
                        self::get_geo_ID((int)$temp['geoname_id'],$continent_ID,$country_ID,$region_ID,$city_ID,$timezone_ID);

                }
                else{

                    if(!empty($continent_ID))
                        self::update_continent($continent_ID,$lang,$temp['continent_name']);

                    if(!empty($country_ID))
                        self::update_country($country_ID,$lang,$temp['country_name']);

                    if(!empty($region_ID))
                        self::update_region($region_ID,$lang,$temp['subdivision_1_name']);

                    if(!empty($city_ID))
                        self::update_city($city_ID,$lang,$temp['city_name']);

                }

                $n++;

            }

            fclose($file_object);

        }

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_file_list()){

            self::set_file_city_list();
            self::set_file_ip4();

            return true;

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}