<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class Region{

    /** @var int */
    public  static $region_ID;

    /** @var string */
    public  static $region_code;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$region_ID    =NULL;
        self::$region_code  =NULL;

        return true;

    }

    /**
     * @param int|NULL $region_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_region_ID(int $region_ID=NULL){

        if(empty($region_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Region ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($region_ID,'_region',0);

    }

    /**
     * @param string|NULL $region_code
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_region_code(string $region_code=NULL){

        if(empty($region_code)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Region code is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'code'=>$region_code
        );

        return Db::isset_row('_region',0,$where_list);

    }

    /**
     * @param int|NULL $continent_ID
     * @param int|NULL $country_ID
     * @param string|NULL $region_code
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_region_ID(int $continent_ID=NULL,int $country_ID=NULL,string $region_code=NULL){

        $error_info_list=[];

        if(empty($continent_ID))
            $error_info_list[]='Continent ID is empty';

        if(empty($country_ID))
            $error_info_list[]='Country ID is empty';

        if(empty($region_code))
            $error_info_list[]='Region code is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'continent_id'  =>$continent_ID,
            'country_id'    =>$country_ID,
            'code'          =>$region_code
        );

        return Db::get_row_ID('_region',0,$where_list);

    }

    /**
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_region_ID_default(){

        if(!empty(self::$region_ID))
            return self::$region_ID;

        $error_info_list=[];

        if(empty(Continent::$continent_ID))
            $error_info_list[]='Default continent ID is empty';

        if(empty(Country::$country_ID))
            $error_info_list[]='Default country ID is empty';

        if(empty(self::$region_code))
            $error_info_list[]='Defulat region code is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $region_ID=self::get_region_ID(Continent::$continent_ID,Country::$country_ID,self::$region_code);

        if(empty($region_ID))
            return NULL;

        self::set_region_ID_default($region_ID);

        return $region_ID;

    }

    /**
     * @param int|NULL $continent_ID
     * @param int|NULL $country_ID
     * @param string|NULL $region_code
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_region(int $continent_ID=NULL,int $country_ID=NULL,string $region_code=NULL){

        $error_info_list=[];

        if(empty($continent_ID))
            $error_info_list[]='Continent ID is empty';

        if(empty($country_ID))
            $error_info_list[]='Country ID is empty';

        if(empty($region_code))
            $error_info_list[]='Region code is empty';
        else if(!RegionValidation::is_valid_region_code($region_code))
            $error_info_list[]='Region code is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_region',
            'values'    =>array(
                'continent_id'  =>$continent_ID,
                'country_id'    =>$country_ID,
                'code'          =>$region_code,
                'date'          =>'NOW()'
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Region was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $region_ID
     * @param bool $is_need_remove_region_localization
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_region_ID(int $region_ID=NULL,bool $is_need_remove_region_localization=true){

        if(empty($region_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Region ID is empty'
            );

            throw new ParametersException($error);

        }

        if($is_need_remove_region_localization)
            if(!RegionLocalization::remove_region_localization($region_ID)){

                $error=array(
                    'title'     =>'DB query problem',
                    'info'      =>'Region localization was not removed'
                );

                throw new DbQueryException($error);

            }

        if(!Db::delete_from_ID($region_ID,'_region',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Region was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $continent_ID
     * @param int|NULL $country_ID
     * @param string|NULL $region_code
     * @param bool $is_need_remove_region_localization
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_region(int $continent_ID=NULL,int $country_ID=NULL,string $region_code=NULL,bool $is_need_remove_region_localization=true){

        $error_info_list=[];

        if(empty($continent_ID))
            $error_info_list[]='Continent ID is empty';

        if(empty($country_ID))
            $error_info_list[]='Country ID is empty';

        if(empty($region_code))
            $error_info_list[]='Region code is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

//        if($is_need_remove_region_localization){
//
//            $region_ID=self::get_region_ID($continent_ID,$country_ID,$region_code);
//
//            if(!empty($region_ID))
//
//        }

        $where_list=array(
            'continent_id'  =>$continent_ID,
            'country_id'    =>$country_ID,
            'code'          =>$region_code
        );

        if($is_need_remove_region_localization){

            $region_ID=self::get_region_ID($continent_ID,$country_ID,$region_code);

            if(!empty($region_ID))
                if(!RegionLocalization::remove_region_localization($region_ID)){

                    $error=array(
                        'title'     =>'DB query problem',
                        'info'      =>'Region localization was not removed'
                    );

                    throw new DbQueryException($error);

                }


        }

        if(!Db::delete_from_where_list('_region',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Region was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $region_ID
     * @return bool
     */
    public  static function set_region_ID_default(int $region_ID=NULL){

        self::$region_ID=empty($region_ID)?NULL:$region_ID;

        return true;

    }

    /**
     * @param string|NULL $region_code
     * @return bool
     */
    public  static function set_region_code_default(string $region_code=NULL){

        self::$region_code=empty($region_code)?NULL:$region_code;

        return true;

    }

}