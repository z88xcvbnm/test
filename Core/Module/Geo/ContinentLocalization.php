<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Lang\LangConfig;

class ContinentLocalization{

    /** @var int */
    public  static $continent_localization_ID;

    /** @var int */
    public  static $lang_ID;

    /** @var string */
    public  static $continent_name;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$continent_localization_ID    =NULL;
        self::$lang_ID                      =NULL;
        self::$continent_name               =NULL;

        return true;

    }

    /**
     * @param int|NULL $continent_localization_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_continent_localization_ID(int $continent_localization_ID=NULL){

        if(empty($continent_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Continent localization ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($continent_localization_ID,'_continent_localization',0);

    }

    /**
     * @param int|NULL $continent_ID
     * @param int|NULL $lang_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_continent_localization(int $continent_ID=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($continent_ID))
            $error_info_list[]='Continent ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'continent_id'  =>$continent_ID,
            'lang_id'       =>$lang_ID
        );

        return Db::isset_row('_continent_localization',0,$where_list);

    }

    /**
     * @param int|NULL $continent_ID
     * @param int|NULL $lang_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_continent_name(int $continent_ID=NULL,int $lang_ID=NULL){

        $error_info_list=[];

        if(empty($continent_ID))
            $error_info_list[]='Continent ID is empty';

        if(empty($lang_ID))
            $error_info_list[]='Lang ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_continent_localization',
            'where'=>array(
                'continent_id'      =>$continent_ID,
                'lang_id'           =>$lang_ID,
                'type'              =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param int|NULL $contient_localization_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_continent_name_from_continent_localization_ID(int $contient_localization_ID=NULL){

        if(empty($contient_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Continent localization ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_continent_localization',
            'where'=>array(
                'id'    =>$contient_localization_ID,
                'type'  =>0
            ),
            'type'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @return null|string
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_continent_name_default(){

        if(!empty(self::$continent_name))
            return self::$continent_name;

        if(empty(Continent::$continent_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default continent ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!empty(self::$continent_localization_ID))
            return self::get_continent_name_from_continent_localization_ID(self::$continent_localization_ID);
        else{

            if(empty(self::$lang_ID))
                self::set_lang_ID_default(LangConfig::$lang_ID_default);

            return self::get_continent_name(Continent::$continent_ID,self::$lang_ID);

        }

    }

    /**
     * @param int|NULL $continent_localization_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_continent_localization_ID(int $continent_localization_ID=NULL){

        if(empty($continent_localization_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Continent localization ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($continent_localization_ID,'_continent_localization',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Continent localization was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $continent_ID
     * @param int|NULL $lang_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_continent_localization(int $continent_ID=NULL,int $lang_ID=NULL){

        if(empty($continent_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Continent ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'continent_id'=>$continent_ID
        );

        if(!empty($lang_ID))
            $where_list['lang_id']=$lang_ID;

        if(!Db::delete_from_where_list('_continent_localization',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Continent localization was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $continent_localization_ID
     * @return bool
     */
    public  static function set_continent_localization_ID_default(int $continent_localization_ID=NULL){

        self::$continent_localization_ID=empty($continent_localization_ID)?NULL:$continent_localization_ID;

        return true;

    }

    /**
     * @param int|NULL $lang_ID
     * @return bool
     */
    public  static function set_lang_ID_default(int $lang_ID=NULL){

        self::$lang_ID=empty($lang_ID)?NULL:$lang_ID;

        return true;

    }

    /**
     * @param string|NULL $continent_name
     * @return bool
     */
    public  static function set_continent_name_default(string $continent_name=NULL){

        self::$continent_name=empty($continent_name)?NULL:$continent_name;

        return true;

    }

}