<?php

namespace Core\Module\Geo;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class Continent{

    /** @var int */
    public  static $continent_ID;

    /** @var string */
    public  static $continent_code;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$continent_ID     =NULL;
        self::$continent_code   =NULL;

        return true;

    }

    /**
     * @param int|NULL $continent_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_continent_ID(int $continent_ID=NULL){

        if(empty($continent_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Continent ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($continent_ID,'_continent',0);

    }

    /**
     * @param int|NULL $continent_ID
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_continent_code(int $continent_ID=NULL){

        if(empty($continent_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Continent ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'code'
            ),
            'table'=>'_continent',
            'where'=>array(
                'id'        =>$continent_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['code'];

    }

    /**
     * @param string|NULL $continent_code
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_continent_ID_from_continent_code(string $continent_code=NULL){

        if(empty($continent_code)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Continent code is empty'
            );

            throw new ParametersException($error);

        }

        if(!ContinentValidation::is_valid_continent_code($continent_code)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Continent code is not valid'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'code'=>strtolower($continent_code)
        );

        $continent_ID=Db::get_row_ID('_continent',0,$where_list);

        if(empty($continent_ID))
            return NULL;

        return $continent_ID;

    }

    /**
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_continent_ID_from_continent_code_default(){

        if(!empty(self::$continent_ID))
            self::$continent_ID;

        if(empty(self::$continent_code)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default continent code is empty'
            );

            throw new ParametersException($error);

        }

        $continent_ID=self::get_continent_ID_from_continent_code(self::$continent_code);

        self::set_continent_ID_default($continent_ID);

        return $continent_ID;

    }

    /**
     * @param int|NULL $continent_ID
     * @param bool $is_need_remove_continent_localization
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_continent_ID(int $continent_ID=NULL,bool $is_need_remove_continent_localization=true){

        if(empty($continent_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Continent ID is empty'
            );

            throw new ParametersException($error);

        }

        if($is_need_remove_continent_localization)
            if(!ContinentLocalization::remove_continent_localization($continent_ID)){

                $error=array(
                    'title'     =>'DB query problem',
                    'info'      =>'Continent localization was not removed'
                );

                throw new DbQueryException($error);

            }

        if(!Db::delete_from_ID($continent_ID,'_continent',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Continent ID was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $continent_ID
     * @return bool
     */
    public  static function set_continent_ID_default(int $continent_ID=NULL){

        self::$continent_ID=empty($continent_ID)?NULL:$continent_ID;

        return true;

    }

    /**
     * @param string|NULL $continent_code
     * @return bool
     */
    public  static function set_continent_code_default(string $continent_code=NULL){

        self::$continent_code=empty($continent_code)?NULL:$continent_code;

        return true;

    }

}