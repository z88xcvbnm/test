<?php

namespace Core\Module\Exception;

use Core\Module\Log\Log;
use Core\Module\Response\Response;
use Core\Module\Response\ResponseServerError;

class PathException extends \Exception{

    /** @var string */
    public  static $title='Path problem';

    /**
     * PathException constructor.
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     */
    public function __construct(array $data=[]){

        if(Response::$is_already_send)
            return false;

        Response::$is_already_send=true;

        $place=array(
            'place'     =>array(
                'file'      =>parent::getFile(),
                'line'      =>parent::getLine()
            )
        );

        $error=array_merge($data,$place);

        ResponseServerError::init();

        Log::init($error);

        exit;

    }

}

