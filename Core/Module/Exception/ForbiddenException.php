<?php

namespace Core\Module\Exception;

use Core\Module\Log\Log;
use Core\Module\Response\Response;
use Core\Module\Response\ResponseForbidden;

class ForbiddenException extends \Exception{

    /** @var string */
    public  static $title='Forbidden problem';

    /**
     * ForbiddenException constructor.
     * @param $data
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws SystemException
     */
    public function __construct($data){

        if(Response::$is_already_send)
            return false;

        Response::$is_already_send=true;

        $place=array(
            'place'     =>array(
                'file'      =>parent::getFile(),
                'line'      =>parent::getLine()
            )
        );

        $error=array_merge($data,$place);

        ResponseForbidden::init($data);

        Log::init($error);

        exit;

    }

}

