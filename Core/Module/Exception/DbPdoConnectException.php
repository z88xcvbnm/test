<?php

namespace Core\Module\Exception;

use Core\Module\Log\Log;
use Core\Module\Response\Response;
use Core\Module\Response\ResponseServerError;

class DbPdoConnectException extends \PDOException{

    /** @var string */
    public  static $title='DB PDO connect problem';

    /**
     * DbPdoConnectException constructor.
     * @param \PDOException $error
     * @param array $connect_data
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws SystemException
     */
    public function __construct(\PDOException $error,array $connect_data){

        if(Response::$is_already_send)
            return false;

        Response::$is_already_send=true;

        $error=array(
            'type'      =>'DbConnectException',
            'connect'   =>$connect_data,
            'code'      =>$error->getCode(),
            'line'      =>$error->getLine(),
            'file'      =>$error->getFile(),
            'message'   =>$error->getMessage()
        );

        ResponseServerError::init();

        Log::init($error);

        exit;

    }

}