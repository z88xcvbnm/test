<?php

namespace Core\Module\Exception;

use Core\Module\Log\Log;
use Core\Module\Response\Response;
use Core\Module\Response\ResponseUnknownError;

class UnknownException extends \Exception{

    /** @var string */
    public  static $title='Unknown problem';

    /**
     * UnknownException constructor.
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws SystemException
     */
    public function __construct(){

        if(Response::$is_already_send)
            return false;

        Response::$is_already_send=true;

        $error=array(
            'title'     =>'Unknown error',
            'info'      =>'Unknown error',
            'place'     =>array(
                'file'      =>parent::getFile(),
                'line'      =>parent::getLine()
            )
        );

        ResponseUnknownError::init();

        Log::init($error);

        exit;

    }

}

