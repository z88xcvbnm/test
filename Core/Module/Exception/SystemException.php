<?php

namespace Core\Module\Exception;

use Core\Module\Log\Log;
use Core\Module\Response\Response;
use Core\Module\Response\ResponseServerError;

class SystemException extends \Exception{

    /** @var string */
    public  static $title='System problem';

    /** @var bool */
    public static $is_exception=false;

    /**
     * SystemException constructor.
     * @param \Exception $error
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     */
    public function __construct(\Exception $error){

        if(Response::$is_already_send)
            return false;

        Response::$is_already_send=true;

        self::$is_exception=true;

        $error=array(
            'type'          =>'System Exception',
            'code'          =>$error->getCode(),
            'line'          =>$error->getLine(),
            'file'          =>$error->getFile(),
            'message'       =>$error->getMessage(),
            'previous'      =>$error->getPrevious()
        );

        ResponseServerError::init();

        Log::init($error);

        exit;

    }

}