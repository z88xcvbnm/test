<?php

namespace Core\Module\Exception;

use Core\Module\Log\Log;
use Core\Module\Response\Response;
use Core\Module\Response\ResponseUnprocessableEntity;

class ShluhamNetException extends \Exception{

    /** @var string */
    public  static $title='Shluham Net exception';

    /**
     * PhpException constructor.
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws SystemException
     */
    public function __construct(array $data=[]){

        if(Response::$is_already_send)
            return false;

        Response::$is_already_send=true;

        $place=array(
            'place'     =>array(
                'file'      =>parent::getFile(),
                'line'      =>parent::getLine()
            )
        );

        $error=array_merge($data,$place);

        ResponseUnprocessableEntity::init($data);

        Log::init($error);

        exit;

    }

}

