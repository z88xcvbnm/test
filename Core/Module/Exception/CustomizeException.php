<?php

namespace Core\Module\Exception;

use Core\Module\Log\Log;
use Core\Module\Response\Response;
use Core\Module\Response\ResponseServerError;
use Core\Module\Worktime\Worktime;

class CustomizeException{

    /** @var bool */
    public static $is_exception=false;

    /**
     * @return bool
     */
    public static function init(){

        Worktime::set_timestamp_point('Customize Exception Start');

        set_exception_handler(array('Core\Module\Exception\CustomizeException','exception_handler'));

        Worktime::set_timestamp_point('Customize Exception Start');

        return true;

    }

    /**
     * @param $error
     * @return bool
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     */
    public static function exception_handler($error){

        if(Response::$is_already_send)
            return false;

        Response::$is_already_send=true;

        self::$is_exception=true;

        $error=array(
            'type'          =>'Customize Exception',
            'code'          =>$error->getCode(),
            'line'          =>$error->getLine(),
            'file'          =>$error->getFile(),
            'message'       =>$error->getMessage(),
            'previous'      =>$error->getPrevious()
        );

        ResponseServerError::init();

        Log::init($error);

        exit;

    }

}