<?php

namespace Core\Module\Exception;

use Core\Module\Log\Log;
use Core\Module\Response\Response;
use Core\Module\Response\ResponseUnprocessableEntity;

class ParametersValidationException extends \Exception{

    /** @var string */
    public  static $title='Parameters validation problem';

    /**
     * ParametersValidationException constructor.
     * @param $data
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws SystemException
     */
    public function __construct($data){

        if(Response::$is_already_send)
            return false;

        Response::$is_already_send=true;

        $error=array(
            'title'     =>'Parameters validation problem',
            'place'     =>array(
                'file'      =>parent::getFile(),
                'line'      =>parent::getLine()
            ),
            'data'      =>$data
        );

        ResponseUnprocessableEntity::init($data);

        Log::init($error);

        exit;

    }

}