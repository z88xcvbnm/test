<?php

namespace Core\Module\Exception;

use Core\Module\Log\Log;
use Core\Module\Response\Response;
use Core\Module\Response\ResponseServerError;

class DbParametersException extends \Exception{

    /** @var string */
    public  static $title='DB parameters problem';

    /**
     * DbParametersException constructor.
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws PathException
     * @throws SystemException
     */
    public function __construct(array $data=[]){

        if(Response::$is_already_send)
            return false;

        Response::$is_already_send=true;

        $error=array(
            'title'     =>'DB name is not valid',
            'place'     =>array(
                'file'      =>parent::getFile(),
                'line'      =>parent::getLine()
            ),
            'data'      =>$data
        );

        ResponseServerError::init();

        Log::init($error);

        exit;

    }

}

