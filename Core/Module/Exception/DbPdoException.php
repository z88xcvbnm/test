<?php

namespace Core\Module\Exception;

use Core\Module\Log\Log;
use Core\Module\Response\Response;
use Core\Module\Response\ResponseServerError;

class DbPdoException extends \PDOException{

    /** @var string */
    public  static $title='DB PDO problem';

    /**
     * DbPdoException constructor.
     * @param \PDOException $error
     * @param array $data
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws DbQueryParametersException
     * @throws DbValidationValueException
     * @throws ParametersException
     * @throws SystemException
     */
    public function __construct(\PDOException $error,array $data=[]){

        if(Response::$is_already_send)
            return false;

        Response::$is_already_send=true;

        $error=array(
            'type'      =>'PDO Exception',
            'title'     =>'Db PDO Query Exception',
            'code'      =>$error->getCode(),
            'line'      =>$error->getLine(),
            'file'      =>$error->getFile(),
            'message'   =>$error->getMessage(),
            'previous'  =>$error->getPrevious(),
            'data'      =>$data
        );

        ResponseServerError::init();

        Log::init($error);

        exit;

    }

}