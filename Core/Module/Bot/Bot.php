<?php

namespace Core\Module\Bot;

class Bot{

    /** @var bool */
    public  static $is_facebook     =false;

    /** @var bool */
    public  static $is_vk           =false;

    /** @var bool */
    public  static $is_twitter      =false;

    /** @var bool */
    public  static $is_yandex       =false;

    /** @var bool */
    public  static $is_mail         =false;

    /** @var bool */
    public  static $is_google       =false;

    /** @var bool */
    public  static $is_yahoo        =false;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$is_facebook          =false;
        self::$is_vk                =false;
        self::$is_twitter           =false;
        self::$is_yandex            =false;
        self::$is_mail              =false;
        self::$is_google            =false;
        self::$is_yahoo             =false;

        return true;

    }

}