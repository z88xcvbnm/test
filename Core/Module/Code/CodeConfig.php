<?php

namespace Core\Module\Code;

class CodeConfig{

    /** @var int */
    public  static $code_len        =6;

    /** @var string */
    public  static $code_string     ='abcdefghijklmopqrstuvxwyz0123456789';

}