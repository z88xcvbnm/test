<?php

namespace Core\Module\Code;

use Core\Module\Encrypt\Hash;

class Code{

    /**
     * @param int|NULL $code_len
     * @return string
     */
    public  static function get_code_string(int $code_len=NULL){

        if(empty($code_len))
            $code_len=CodeConfig::$code_len;

        $code_line_len  =mb_strlen(CodeConfig::$code_string,'utf-8')-1;
        $code_return    ='';

        for($i=0;$i<$code_len;$i++){

            $code_index     =rand(0,$code_line_len);
            $code_return    .=substr(CodeConfig::$code_string,$code_index,1);

        }

        return $code_return;

    }

    /**
     * @param int|NULL $code_len
     * @return string
     */
    public  static function get_code_number(int $code_len=NULL){

        if(empty($code_len))
            $code_len=CodeConfig::$code_len;

        $min    =pow(10,$code_len-1);
        $max    =pow(10,$code_len)-1;

        return (string)rand($min,$max);

    }

    /**
     * @param string|NULL $code
     * @param bool|true $is_random
     * @return bool|string
     */
    public  static function get_code_hash(string $code=NULL,bool $is_random=true){

        if(empty($code))
            return false;

        if($is_random)
            $code=rand(0,time()).$code.rand(0,time());

        return Hash::get_sha1_encode($code);

    }

}
