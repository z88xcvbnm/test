<?php

namespace Core\Module\Token;

use Core\Module\Cookie\Cookie;
use Core\Module\Exception\ParametersException;
use Core\Module\Worktime\Worktime;

class TokenDetermine{

    /**
     * @throws ParametersException
     */
    private static function set_client_token_hash_to_default(){

        if(Cookie::isset_token())
            $token_hash=Cookie::get_token();

        if(isset($_POST['token']))
            $token_hash=$_POST['token'];

        if(empty($token_hash))
            $token_hash=Token::get_token_hash();

        Token::set_token_hash_default($token_hash);

    }

    /**
     * @return bool
     */
    private static function set(){

        return true;

    }

    /**
     * @return bool
     */
    public  static function init(){

        Worktime::set_timestamp_point('Token Start');

        self::set();

        Worktime::set_timestamp_point('Token Finish');

        return true;

    }

}