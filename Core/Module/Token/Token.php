<?php

namespace Core\Module\Token;

use Core\Module\App\App;
use Core\Module\App\AppVersion;
use Core\Module\Browser\Browser;
use Core\Module\Browser\BrowserVersion;
use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Device\DeviceFirm;
use Core\Module\Device\DeviceModel;
use Core\Module\Device\DeviceToken;
use Core\Module\Encrypt\Hash;
use Core\Module\Geo\City;
use Core\Module\Geo\Continent;
use Core\Module\Geo\Country;
use Core\Module\Geo\Region;
use Core\Module\Ip\Ip;
use Core\Module\Lang\Lang;
use Core\Module\Lang\LangKeyboard;
use Core\Module\Os\Os;
use Core\Module\Os\OsVersion;
use Core\Module\Route\RouteDetermine;
use Core\Module\Timezone\Timezone;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;
use Core\Module\User\UserDevice;
use Core\Module\Useragent\Useragent;

class Token{

    /** @var int */
    public  static $token_ID;

    /** @var string */
    public  static $token_hash;

    /** @var int */
    public  static $token_date_create;

    /** @var int */
    public  static $token_date_update;

    /** @var int */
    public  static $token_date_live;

    /** @var int */
    public  static $token_date_remove;

    /** @var int */
    public  static $token_date_recovery;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$token_ID                 =NULL;
        self::$token_hash               =NULL;
        self::$token_date_create        =NULL;
        self::$token_date_update        =NULL;
        self::$token_date_live          =NULL;
        self::$token_date_remove        =NULL;
        self::$token_date_recovery      =NULL;

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function reset_token_default(){

        if(!empty(self::$token_ID))
            self::remove_token_ID_default();

        self::add_token_default(false);

        return true;

    }

    /**
     * @param int|NULL $token_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_token_ID(int $token_ID=NULL){

        if(empty($token_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Token ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($token_ID,'_tokne',0);

    }

    /**
     * @param string|NULL $token_hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_token_from_token_hash(string $token_hash=NULL){

        if(empty($token_hash)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Token hash is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'hash'=>$token_hash
        );

        return Db::isset_row('_token',0,$where_list);

    }

    /**
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_token_from_user_ID(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'user_id'=>$user_ID
        );

        return Db::isset_row('_token',0,$where_list);

    }

    /**
     * @param bool $is_keep
     * @return false|string
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_token_date_live(bool $is_keep=true){

        $keep_delta=$is_keep?(12*31*24*3600):TokenConfig::$keep_delta;

        return Date::get_date_time_full(Date::get_timestamp()+$keep_delta);

    }

    /**
     * @return string
     */
    public  static function get_token_hash_default(){

        if(empty(self::$token_hash)){

            self::$token_hash=self::get_token_hash();

            return self::$token_hash;

        }

        return self::$token_hash;

    }

    /**
     * @return string
     */
    public  static function get_token_hash(){

        $hash_list      =[];
        $timestamp_ms   =Date::get_timestamp();

        $hash_list[]    =Useragent::$useragent_hash;
        $hash_list[]    =User::$user_ID;
        $hash_list[]    =Ip::$ip_ID;
        $hash_list[]    =Continent::$continent_ID;
        $hash_list[]    =Country::$country_ID;
        $hash_list[]    =Region::$region_ID;
        $hash_list[]    =City::$city_ID;
        $hash_list[]    =Timezone::$timezone_ID;
        $hash_list[]    =Os::$os_ID;
        $hash_list[]    =Browser::$browser_ID;
        $hash_list[]    =BrowserVersion::$browser_version_ID;
        $hash_list[]    =DeviceFirm::$device_firm_ID;
        $hash_list[]    =DeviceModel::$device_model_ID;
        $hash_list[]    =App::$app_ID;
        $hash_list[]    =AppVersion::$app_version_ID;
        $hash_list[]    =Lang::$lang_ID;
        $hash_list[]    =LangKeyboard::$lang_keyboard_ID;
        $hash_list[]    =$timestamp_ms;
        $hash_list[]    =rand(0,$timestamp_ms);

        return Hash::get_sha1_encode(implode(':',$hash_list));

    }

    /**
     * @param array $data
     * @return array
     */
    public  static function get_array_from_data(array $data=[]){

        if(count($data)==0)
            return[];

        return array(
            'token_ID'                  =>$data[0]['id'],
            'user_ID'                   =>$data[0]['user_id'],
            'ip_ID'                     =>$data[0]['ip_id'],
            'continent_ID'              =>$data[0]['continent_id'],
            'country_ID'                =>$data[0]['country_id'],
            'region_ID'                 =>$data[0]['region_id'],
            'city_ID'                   =>$data[0]['city_id'],
            'timezone_ID'               =>$data[0]['timezone_id'],
            'useragent_ID'              =>$data[0]['useragent_id'],
            'os_ID'                     =>$data[0]['os_id'],
            'os_version_ID'             =>$data[0]['os_version_id'],
            'browser_ID'                =>$data[0]['browser_id'],
            'browser_version_ID'        =>$data[0]['browser_version_id'],
            'device_firm_ID'            =>$data[0]['device_firm_id'],
            'device_model_ID'           =>$data[0]['device_model_id'],
            'device_token_ID'           =>$data[0]['device_token_id'],
            'user_device_token_ID'      =>$data[0]['user_device_token_id'],
            'app_ID'                    =>$data[0]['app_id'],
            'app_version_ID'            =>$data[0]['app_version_id'],
            'lang_ID'                   =>$data[0]['lang_id'],
            'lang_keyboard_ID'          =>$data[0]['lang_keyboard_id'],
            'is_keep'                   =>(bool)$data[0]['is_keep']
        );

    }

    /**
     * @param int|NULL $token_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_token_data_from_token_ID(int $token_ID=NULL){

        if(empty($token_ID)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Token ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'user_id',
                'ip_id',
                'continent_id',
                'country_id',
                'region_id',
                'city_id',
                'timezone_id',
                'useragent_id',
                'os_id',
                'os_version_id',
                'browser_id',
                'browser_version_id',
                'device_firm_id',
                'device_model_id',
                'device_token_id',
                'user_device_token_id',
                'app_id',
                'app_version_id',
                'lang_id',
                'lang_keyboard_id',
                'is_keep'
            ),
            'table'=>'_token',
            'where'=>array(
                'id'        =>$token_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        return self::get_array_from_data($r);

    }

    /**
     * @param string|NULL $hash
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_token_data_from_hash(string $hash=NULL){

        if(empty($hash)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Token hash is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'user_id',
                'ip_id',
                'continent_id',
                'country_id',
                'region_id',
                'city_id',
                'timezone_id',
                'useragent_id',
                'os_id',
                'os_version_id',
                'browser_id',
                'browser_version_id',
                'device_firm_id',
                'device_model_id',
                'device_token_id',
                'user_device_token_id',
                'app_id',
                'app_version_id',
                'lang_id',
                'lang_keyboard_id',
                'is_keep'
            ),
            'table'=>'_token',
            'where'=>array(
                array(
                    'column'    =>'hash',
                    'value'     =>$hash
                ),
                array(
                    'column'    =>'date_live',
                    'method'    =>'>',
                    'value'     =>Date::get_date_time_full()
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        return self::get_array_from_data($r);

    }

    /**
     * @param string|NULL $token_hash
     * @return int|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_token_ID_from_token_hash(string $token_hash=NULL){

        if(empty($token_hash)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Token hash is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'column'=>'id'
            ),
            'table'=>'_token',
            'where'=>array(
                'hash'  =>$token_hash,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return count($r)==0?NULL:$r[0]['id'];

    }

    /**
     * @param int|NULL $user_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_token_ID_from_user_ID(int $user_ID=NULL){

        if(empty($user_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_token',
            'where'=>array(
                'user_id'   =>$user_ID,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=$row['id'];

        return $list;

    }

    /**
     * @param string|NULL $token_hash
     * @param bool $is_keep
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_token(string $token_hash=NULL,bool $is_keep=true){

        if(empty($token_hash)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Default token hash is empty'
            );

            throw new ParametersException($error);

        }

        $token_date_live=self::get_token_date_live($is_keep);

        $q=array(
            'table'     =>'_token',
            'values'    =>array(
                array(
                    'user_id'               =>User::$user_ID,
                    'ip_id'                 =>Ip::$ip_ID,
                    'continent_id'          =>Continent::$continent_ID,
                    'country_id'            =>Country::$country_ID,
                    'region_id'             =>Region::$region_ID,
                    'city_id'               =>City::$city_ID,
                    'timezone_id'           =>Timezone::$timezone_ID,
                    'useragent_id'          =>Useragent::$useragent_ID,
                    'os_id'                 =>Os::$os_ID,
                    'os_version_id'         =>OsVersion::$os_version_ID,
                    'browser_id'            =>Browser::$browser_ID,
                    'browser_version_id'    =>BrowserVersion::$browser_version_ID,
                    'device_firm_id'        =>DeviceFirm::$device_firm_ID,
                    'device_model_id'       =>DeviceModel::$device_model_ID,
                    'device_token_id'       =>DeviceToken::$device_token_ID,
                    'user_device_token_id'  =>UserDevice::$user_device_ID,
                    'app_id'                =>App::$app_ID,
                    'app_version_id'        =>AppVersion::$app_version_ID,
                    'lang_id'               =>Lang::$lang_ID,
                    'lang_keyboard_id'      =>LangKeyboard::$lang_keyboard_ID,
                    'hash'                  =>$token_hash,
                    'is_keep'               =>(int)$is_keep,
                    'date_create'           =>'NOW()',
                    'date_update'           =>'NOW()',
                    'date_live'             =>$token_date_live,
                    'type'                  =>0
                )
            )
        );

        $conflict_list=[
            'column_list'=>[
                'hash'
            ],
            'update_list'=>[
                'set'=>[
                    'date_update'       =>'NOW()',
                    'date_recovery'     =>'NOW()',
                    'type'              =>0
                ]
            ],
            'return_list'=>[
                'id'
            ]
        ];

        $r=Db::insert($q,true,[],NULL,$conflict_list);

        if(count($r)==0){

            $error=array(
                'title'     =>'Token DB problem',
                'info'      =>'New token was not added',
                'data'      =>array(
                    'query'     =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param bool $is_old_hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_token_default(bool $is_old_hash=true){

        if($is_old_hash&&!empty(self::$token_hash))
            $token_hash=self::$token_hash;
        else
            $token_hash=self::get_token_hash_default();

        $token_ID=self::add_token($token_hash,TokenConfig::$is_keep);

        if(empty($token_ID)){

            $error=array(
                'title'     =>'Token DB problem',
                'info'      =>'New token was not added'
            );

            throw new DbQueryException($error);

        }

        $token_date_live=self::get_token_date_live(TokenConfig::$is_keep);

        self::set_token_ID_default($token_ID);
        self::set_token_date_create_default(Date::get_timestamp());
        self::set_token_date_live_default($token_date_live);

        return true;

    }

    /**
     * @param int|NULL $token_ID
     * @param string|NULL $token_date_live
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token_date_live_from_token_ID(int $token_ID=NULL,string $token_date_live=NULL){

        if(empty($token_ID)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Token ID is empty'
            );

            throw new ParametersException($error);

        }

        if(empty($token_date_live))
            $token_date_live=self::get_token_date_live();

        $q=array(
            'table'=>'_token',
            'set'=>array(
                array(
                    'column'    =>'date_update',
                    'value'     =>'now()'
                ),
                array(
                    'column'    =>'date_live',
                    'value'     =>$token_date_live
                )
            ),
            'where'=>array(
                array(
                    'column'    =>'id',
                    'value'     =>$token_ID
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        if(!Db::update($q)){

            $error=array(
                'title'         =>'Token remove problem',
                'info'          =>'Token date live fail',
                'data'          =>array(
                    'token_ID'      =>self::$token_ID
                )
            );

            throw new DbQueryException($error);
        }

        return true;

    }

    /**
     * @param int|NULL $token_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token_date_update_from_token_ID(int $token_ID=NULL){

        if(empty($token_ID)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Token ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_token',
            'set'=>array(
                array(
                    'column'    =>'date_update',
                    'value'     =>'now()'
                )
            ),
            'where'=>array(
                array(
                    'column'    =>'id',
                    'value'     =>$token_ID
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        if(!Db::update($q)){

            $error=array(
                'title'         =>'Token remove problem',
                'info'          =>'Default token date update fail',
                'data'          =>array(
                    'token_ID'      =>self::$token_ID
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token_date_live_default(){

        if(empty(self::$token_ID)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Default token ID is empty'
            );

            throw new ParametersException($error);

        }

        $token_date_live=self::get_token_date_live(TokenConfig::$is_keep);

        if(!self::update_token_date_live_from_token_ID(self::$token_ID,$token_date_live)){

            $error=array(
                'title'         =>'Token remove problem',
                'info'          =>'Default token date live fail',
                'data'          =>array(
                    'token_ID'      =>self::$token_ID,
                    'date_live'     =>$token_date_live
                )
            );

            throw new DbQueryException($error);

        }

        self::set_token_date_live_default($token_date_live);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token_date_update_default(){

        if(empty(self::$token_ID)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Default token ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!self::update_token_date_update_from_token_ID(self::$token_ID)){

            $error=array(
                'title'         =>'Token remove problem',
                'info'          =>'Default token date update fail',
                'data'          =>array(
                    'token_ID'      =>self::$token_ID
                )
            );

            throw new DbQueryException($error);

        }

        self::$token_date_update=Date::get_timestamp();

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token_useragent_ID(){

        if(empty(self::$token_ID)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Default token ID is empty'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $token_ID
     * @param int|NULL $user_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token_user_ID(int $token_ID=NULL,int $user_ID=NULL){

        $error_info_list=[];

        if(empty($token_ID))
            $error_info_list[]='Token ID is empty';

        if(empty($user_ID))
            $error_info_list[]='User ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'token_ID'      =>$token_ID,
                    'user_ID'       =>$user_ID
                )
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_token',
            'set'=>array(
                'user_id'       =>$user_ID,
                'date_update'   =>'NOW()'
            ),
            'where'=>array(
                'id'    =>$token_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'         =>'Token DB problem',
                'info'          =>'Token was not recovery',
                'data'          =>array(
                    'token_ID'      =>$token_ID,
                    'user_ID'       =>$user_ID,
                    'query'         =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $token_ID
     * @param int|NULL $orange_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token_orange_ID(int $token_ID=NULL,int $orange_ID=NULL){

        $error_info_list=[];

        if(empty($token_ID))
            $error_info_list[]='Token ID is empty';

        if(empty($orange_ID))
            $error_info_list[]='User ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'token_ID'      =>$token_ID,
                    'orange_ID'     =>$orange_ID
                )
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_token',
            'set'=>array(
                'user_id'       =>User::$user_ID,
                'orange_id'     =>$orange_ID,
                'date_update'   =>'NOW()'
            ),
            'where'=>array(
                'id'    =>$token_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'         =>'Token DB problem',
                'info'          =>'Token was not recovery',
                'data'          =>array(
                    'token_ID'      =>$token_ID,
                    'orange_ID'     =>$orange_ID,
                    'query'         =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $token_ID
     * @param int|NULL $user_device_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token_user_device_ID(int $token_ID=NULL,int $user_device_ID=NULL){

        $error_info_list=[];

        if(empty($token_ID))
            $error_info_list[]='Token ID is empty';

        if(empty($user_device_ID))
            $error_info_list[]='User device ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_token',
            'set'=>array(
                'user_device_token_id'      =>$user_device_ID,
                'date_update'               =>'NOW()'
            ),
            'where'=>array(
                'id'    =>$token_ID,
                'type'  =>0
            )
        );

        if(!Db::update($q)){

            $error=array(
                'title'         =>'Token DB problem',
                'info'          =>'Token was not recovery'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token_user_ID_default(){

        if(empty(User::$user_ID)){

            $error=array(
                'title' =>'Token parameter problem',
                'info'  =>'User ID is empty'
            );

            throw new ParametersException($error);

        }

        return self::update_token_user_ID(self::$token_ID,User::$user_ID);

    }

    /**
     * @param int|NULL $token_ID
     * @param int|NULL $os_ID
     * @param int|NULL $os_version_ID
     * @param int|NULL $device_firm_ID
     * @param int|NULL $device_model_ID
     * @param int|NULL $device_token_ID
     * @param int|NULL $user_device_token_ID
     * @param int|NULL $app_ID
     * @param int|NULL $app_version_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_token_device_data(int $token_ID=NULL,int $os_ID=NULL,int $os_version_ID=NULL,int $device_firm_ID=NULL,int $device_model_ID=NULL,int $device_token_ID=NULL,int $user_device_token_ID=NULL,int $app_ID=NULL,int $app_version_ID=NULL){

        if(empty($token_ID)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Token ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table' =>'_token',
            'set'   =>array(
                'os_id'                 =>$os_ID,
                'os_version_id'         =>$os_version_ID,
                'device_firm_id'        =>$device_firm_ID,
                'device_model_id'       =>$device_model_ID,
                'device_token_id'       =>$device_token_ID,
                'user_device_token_id'  =>$user_device_token_ID,
                'app_id'                =>$app_ID,
                'app_version_id'        =>$app_version_ID,
                'date_update'           =>'NOW()'
            ),
            'where'=>array(
                'id'    =>$token_ID,
                'type'  =>0
            )
        );

        $r=Db::update($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Token was not updated'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $token_hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_token_from_token_hash(string $token_hash=NULL){

        if(empty($token_hash)){

            $error=array(
                'title'=>'Token parameter problem',
                'info'=>'Token hash is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'hash'=>$token_hash
        );

        if(!Db::delete_from_where_list('_token',0,$where_list)){

            $error=array(
                'title'         =>'Token DB problem',
                'info'          =>'Token was not removed',
                'data'          =>array(
                    'hash'  =>$token_hash
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $token_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_token_from_token_ID(int $token_ID=NULL){

        if(empty($token_ID)){

            $error=array(
                'title'=>'Token parameter problem',
                'info'=>'Token ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($token_ID,'_token',0)){

            $error=array(
                'title'     =>'Token DB problem',
                'info'      =>'Token was not remove'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_token_ID_default(){

        if(empty(self::$token_ID)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Default token ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!self::remove_token_from_token_ID(self::$token_ID)){

            $error=array(
                'title'         =>'Token remove problem',
                'info'          =>'Default token ID was not removed'
            );

            throw new DbQueryException($error);

        }

        self::reset_data();

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_token_default_from_token_hash(){

        if(empty(self::$token_hash)){

            $error=array(
                'title'     =>'Token parameter problem',
                'info'      =>'Default token hash is empty'
            );

            throw new ParametersException($error);

        }

        if(!self::remove_token_from_token_hash(self::$token_hash)){

            $error=array(
                'title'         =>'Token remove problem',
                'info'          =>'Default token date update fail',
                'data'          =>array(
                    'token_ID'      =>self::$token_ID,
                    'token_hash'    =>self::$token_hash
                )
            );

            throw new DbQueryException($error);

        }

        self::reset_data();

        return true;

    }

    /**
     * @param int|NULL $token_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function recovery_token_from_token_ID(int $token_ID=NULL){

        if(empty($token_ID)){

            $error=array(
                'title'=>'Token parameter problem',
                'info'=>'Token ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_token',
            'set'=>array(
                array(
                    'column'    =>'date_update',
                    'value'     =>'now()'
                ),
                array(
                    'column'    =>'date_recovery',
                    'value'     =>'now()'
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'where'=>array(
                array(
                    'column'    =>'id',
                    'value'     =>$token_ID
                ),
                array(
                    'column'    =>'type',
                    'value'     =>1
                )
            ),
            'limit'=>1
        );

        if(!Db::update($q)){

            $error=array(
                'title'         =>'Token DB problem',
                'info'          =>'Token was not recovery',
                'data'          =>array(
                    'tokek_ID'      =>$token_ID
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $token_hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function recovery_token_from_token_hash(string $token_hash=NULL){

        if(empty($token_hash)){

            $error=array(
                'title'=>'Token parameter problem',
                'info'=>'Token hash is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>'_token',
            'set'=>array(
                array(
                    'column'    =>'date_update',
                    'value'     =>'now()'
                ),
                array(
                    'column'    =>'date_recovery',
                    'value'     =>'now()'
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'where'=>array(
                array(
                    'column'    =>'hash',
                    'value'     =>$token_hash
                ),
                array(
                    'column'    =>'type',
                    'value'     =>1
                )
            ),
            'limit'=>1
        );

        if(!Db::update($q)){

            $error=array(
                'title'         =>'Token DB problem',
                'info'          =>'Token was not recovery',
                'data'          =>array(
                    'hash'  =>$token_hash
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $token_ID
     */
    public  static function set_token_ID_default(int $token_ID=NULL){

        self::$token_ID=empty($token_ID)?NULL:$token_ID;

    }

    /**
     * @param string|NULL $token_hash
     */
    public  static function set_token_hash_default(string $token_hash=NULL){

        self::$token_hash=empty($token_hash)?NULL:$token_hash;

    }

    /**
     * @param string|NULL $token_date_create
     */
    public  static function set_token_date_create_default(string $token_date_create=NULL){
        
        self::$token_date_create=empty($token_date_create)?NULL:$token_date_create;
        
    }

    /**
     * @param string|NULL $token_date_update
     */
    public  static function set_token_date_update_default(string $token_date_update=NULL){
        
        self::$token_date_update=empty($token_date_update)?NULL:$token_date_update;
        
    }

    /**
     * @param string|NULL $token_date_live
     */
    public  static function set_token_date_live_default(string $token_date_live=NULL){

        self::$token_date_live=empty($token_date_live)?NULL:$token_date_live;

    }

    /**
     * @param string|NULL $token_date_remove
     */
    public  static function set_token_date_remove_default(string $token_date_remove=NULL){
        
        self::$token_date_remove=empty($token_date_remove)?NULL:$token_date_remove;
        
    }

    /**
     * @param string|NULL $token_date_recovery
     */
    public  static function set_token_date_recovery_default(string $token_date_recovery=NULL){
        
        self::$token_date_recovery=empty($token_date_recovery)?NULL:$token_date_recovery;
        
    }

}