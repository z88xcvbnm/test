<?php

namespace Core\Module\Token;

class TokenConfig{

    /** @var bool */
    public  static $is_keep                 =true;

    /** @var int */
    public  static $keep_delta              =3600;

    /**
     * @var int
     */
    public  static $keep_delta_default      =60;

    /**
     * @param bool|NULL $is_keep
     */
    public  static function set_token_is_keep_default(bool $is_keep=NULL){

        self::$is_keep=empty($is_keep)?true:$is_keep;

    }

    /**
     * @param int|NULL $token_keep_delta
     */
    public  static function set_token_keep_delta(int $token_keep_delta=NULL){

        self::$keep_delta=empty($token_keep_delta)?self::$keep_delta_default:$token_keep_delta;

    }

}