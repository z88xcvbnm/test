<?php

namespace Core\Module\Wallet;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;

class WalletAddress{

    /** @var string */
    public  static $table_name='_wallet_address';

    /**
     * @param string|NULL $address
     * @return int|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_wallet_address_ID_from_address(string $address=NULL){

        if(empty($address)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Address is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id'
            ],
            'where'=>[
                'address'   =>$address,
                'type'      =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param bool|NULL $is_hide
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_wallet_address_list(bool $is_hide=NULL){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id',
                'wallet_name_id',
                'address',
                'is_hide'
            ],
            'where'=>[
                'type'=>0
            ]
        ];

        if(!is_null($is_hide))
            $q['where']['is_hide']=(int)$is_hide;

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=[
                'ID'                =>$row['id'],
                'wallet_name_ID'    =>$row['wallet_name_id'],
                'address'           =>$row['address'],
                'is_hide'           =>(bool)$row['is_hide']
            ];

        return $list;

    }

}