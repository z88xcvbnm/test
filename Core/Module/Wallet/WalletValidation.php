<?php

namespace Core\Module\Wallet;

use Core\Module\Email\EmailValidation;
use Core\Module\Exception\ParametersException;

class WalletValidation{

    /**
     * @param string|NULL $address
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_valid_wallet_btc(string $address=NULL){

        if(empty($address)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Address is empty'
            ];

            throw new ParametersException($error);

        }

        return preg_match('/^(?=.*[0-9])(?=.*[a-zA-Z])[\da-zA-Z]{27,34}$/is',$address)!==false;

    }

    /**
     * @param string|NULL $address
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_valid_wallet_eth(string $address=NULL){

        if(empty($address)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Address is empty'
            ];

            throw new ParametersException($error);

        }

        return preg_match('/^0x[a-fA-F0-9]{40,44}$/is',$address)!==false;

    }

    /**
     * @param string|NULL $address
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_valid_wallet_qiwi(string $address=NULL){

        if(empty($address)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Address is empty'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $address
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_valid_wallet_paypal(string $address=NULL){

        if(empty($address)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Address is empty'
            ];

            throw new ParametersException($error);

        }

        return EmailValidation::is_valid_email($address);

    }

    /**
     * @param string|NULL $address
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_valid_wallet_credit_card(string $address=NULL){

        if(empty($address)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Address is empty'
            ];

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $name
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_valid_wallet_sberbank(string $name=NULL){

        if(empty($name)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Name is empty'
            ];

            throw new ParametersException($error);

        }

        return preg_match('/^[A-Za-zА-Яёа-я\-]+\s+[A-Za-zА-Яёа-я\-]$/is',$name)!==false;

    }

    /**
     * @param string|NULL $key
     * @param string|NULL $address
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function is_valid_wallet_address(string $key=NULL,string $address=NULL){

        $error_info_list=[];

        if(empty($key))
            $error_info_list[]='Key is empty';

        if(empty($address))
            $error_info_list[]='Address is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        switch($key){

            case'sberbank':
                return self::is_valid_wallet_sberbank($address);

            case'btc':
                return self::is_valid_wallet_btc($address);

            case'eth':
                return self::is_valid_wallet_eth($address);

            case'qiwi':
                return self::is_valid_wallet_qiwi($address);

            case'pp':
                return self::is_valid_wallet_paypal($address);

            case'cc':
                return self::is_valid_wallet_credit_card($address);

        }

        return false;

    }

}