<?php

namespace Core\Module\Wallet;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;

class WalletName{

    /** @var string */
    public  static $table_name='_wallet_name';

    /**
     * @param string|NULL $key
     * @return string|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_wallet_name_ID_from_key(string $key=NULL){

        if(empty($key)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Key is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id'
            ],
            'where'=>[
                'key'   =>$key,
                'type'  =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param bool|NULL $is_system
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_wallet_name_list(bool $is_system=NULL){

        $q=[
            'table'=>self::$table_name,
            'select'=>[
                'id',
                'key'
            ],
            'where'=>[
                'type'=>0
            ]
        ];

        if(!is_null($is_system))
            $q['where']['is_system']=(int)$is_system;

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[$row['id']]=$row['key'];

        return $list;

    }

}