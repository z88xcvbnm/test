<?php

namespace Core\Module\Ffmpeg;

use Core\Module\Audio\AudioConfig;
use Core\Module\Exception\FileException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Exception\ShellExecException;
use Core\Module\Time\Time;
use Core\Module\Video\VideoConfig;

class Ffmpeg{

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_info_list(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'file_info_list'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'file_info_list');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        if(empty(FfmpegConfig::$ffmpeg_path))
            FfmpegConfig::set_ffmpeg_command_path();

        $q=FfmpegConfig::$ffmpeg_path.' -i '.$_SERVER['DOCUMENT_ROOT'].'/'.$file_path.' 2>&1';

        exec($q,$r);

        if(count($r)==0){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File info is empty',
                'data'  =>array(
                    'file_path' =>$file_path,
                    'query'     =>$q
                )
            );

            throw new ShellExecException($error);

        }

        FfmpegCash::add_file_parameter_in_cash($file_path,'file_info_list',$r);

        return $r;

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_extension(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'file_extension'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'file_extension');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $data       =self::get_info_list($file_path);
        $index      =self::get_index('Duration:',$data);

        if($index==-1){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Bitrate not found',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        $r=preg_match_all('/Duration:\s*(\d*\S*),/is',$data[$index],$list);

        if(count($list[1])==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Duration is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        FfmpegCash::add_file_parameter_in_cash($file_path,'file_extension',$r[1][0]);

        return $r[1][0];

    }

    /**
     * @param string|NULL $search
     * @param array $list
     * @return int
     */
    public  static function get_index(string $search=NULL,array $list=[]){

        if(empty($search)||count($list)==0)
            return -1;

        for($i=count($list)-1;$i>=0;$i--)
            if((bool)preg_match('/'.$search.'/is',$list[$i],$r))
                return $i;

        return -1;

    }

    /**
     * @param string|NULL $file_path
     * @param string $stream_type
     * @return mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_bitrate_from_stream_type(string $file_path=NULL,string $stream_type='Audio'){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($stream_type))
            $error_info_list[]='Stream type is empty';

        if($stream_type!='Audio'&&$stream_type!='Video')
            $error_info_list[]='Stream type is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list,
                'data'  =>array(
                    'file_path'     =>$file_path,
                    'stream_type'   =>$stream_type
                )
            );

            throw new ParametersException($error);

        }

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $data=self::get_info_list($file_path);

        if(count($data)==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'File info is empty',
                'data'  =>array(
                    'file_path'     =>$file_path,
                    'stream_type'   =>$stream_type
                )
            );

            throw new FileException($error);

        }

        $index=self::get_index($stream_type.':',$data);

        if($index==-1){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Bitrate not found',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        if(preg_match_all('/\s*(\d*\S*)\s*kb\/s/is',$data[$index],$list)===false){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Bitrate not found',
                'data'  =>array(
                    'file_path'     =>$file_path,
                    'stream_type'   =>$stream_type,
                    'index'         =>$index,
                    'row'           =>$data[$index],
                    'data'          =>$data
                )
            );

            throw new FileException($error);

        }

        if(count($list[1])==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Bitrate is empty',
                'data'  =>array(
                    'file_path'     =>$file_path,
                    'stream_type'   =>$stream_type,
                    'index'         =>$index,
                    'row'           =>$data[$index],
                    'data'          =>$data
                )
            );

            throw new FileException($error);

        }

        return $list[1][0];

    }

    /**
     * @param string|NULL $file_path
     * @param string $stream_type
     * @return mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_codec_from_stream_type(string $file_path=NULL,string $stream_type='Audio'){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($stream_type))
            $error_info_list[]='Stream type is empty';

        if($stream_type!='Audio'&&$stream_type!='Video')
            $error_info_list[]='Stream type is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list,
                'data'  =>array(
                    'file_path'     =>$file_path,
                    'stream_type'   =>$stream_type
                )
            );

            throw new ParametersException($error);

        }

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $data=self::get_info_list($file_path);

        if(count($data)==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'File info is empty',
                'data'  =>array(
                    'file_path'     =>$file_path,
                    'stream_type'   =>$stream_type
                )
            );

            throw new FileException($error);

        }

        $index=self::get_index($stream_type.':',$data);

        if($index==-1){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Bitrate not found',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        if(preg_match_all('/'.$stream_type.':\s*(\w*\d*)\s*/is',$data[$index],$list)===false){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Bitrate not found',
                'data'  =>array(
                    'file_path'     =>$file_path,
                    'stream_type'   =>$stream_type,
                    'data'          =>$data
                )
            );

            throw new FileException($error);

        }

        if(count($list[1])==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Bitrate is empty',
                'data'  =>array(
                    'file_path'     =>$file_path,
                    'stream_type'   =>$stream_type,
                    'data'          =>$data
                )
            );

            throw new FileException($error);

        }

        return $list[1][0];

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_duration(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'duration'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'duration');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $data=self::get_info_list($file_path);

        if(count($data)==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'File info is empty',
                'data'  =>array(
                    'file_path'     =>$file_path,
                    'data'          =>$data
                )
            );

            throw new FileException($error);

        }

        $index=self::get_index('Duration:',$data);

        if($index==-1){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Bitrate not found',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        if(preg_match_all('/Duration:\s*(\d*\S*),/is',$data[$index],$list)===false){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Duration not found',
                'data'  =>array(
                    'file_path'     =>$file_path,
                    'data'          =>$data
                )
            );

            throw new FileException($error);

        }

        if(count($list[1])==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Duration not found',
                'data'  =>array(
                    'file_path' =>$file_path
                )
            );

            throw new FileException($error);

        }

        FfmpegCash::add_file_parameter_in_cash($file_path,'duration',$list[1][0]);

        return $list[1][0];

    }

    /**
     * @param string|NULL $file_path
     * @return float|int|mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_duration_in_seconds(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'duration_seconds'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'duration_seconds');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new PathException($error);

        }

        $duration           =self::get_duration($file_path);
        $duration_seconds   =Time::get_seconds_from_time_string($duration);

        FfmpegCash::add_file_parameter_in_cash($file_path,'duration_seconds',$duration_seconds);

        return $duration_seconds;

    }

    /**
     * @param string|NULL $file_path
     * @return float|int|mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_bitrate(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'audio_bitrate'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'audio_bitrate');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new ParametersException($error);

        }

        $bitrate=self::get_bitrate_from_stream_type($file_path,'Audio');

        if(empty($bitrate)){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Audio bitrate is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        FfmpegCash::add_file_parameter_in_cash($file_path,'audio_bitrate',$bitrate);

        return (int)$bitrate*1000;

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_bitrate(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'video_bitrate'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'video_bitrate');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new ParametersException($error);

        }

        $bitrate=self::get_bitrate_from_stream_type($file_path,'Video');

        if(empty($bitrate)){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Audio bitrate is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        FfmpegCash::add_file_parameter_in_cash($file_path,'video_bitrate',$bitrate);

        return $bitrate;

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_codec(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'video_codec'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'video_codec');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new ParametersException($error);

        }

        $codec=self::get_codec_from_stream_type($file_path,'Video');

        if(empty($codec)){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Video codec is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        FfmpegCash::add_file_parameter_in_cash($file_path,'video_codec',$codec);

        return $codec;

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_codec(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'audio_codec'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'audio_codec');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new ParametersException($error);

        }

        $codec=self::get_codec_from_stream_type($file_path,'Audio');

        if(empty($codec)){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Audio codec is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        FfmpegCash::add_file_parameter_in_cash($file_path,'audio_codec',$codec);

        return $codec;

    }

    /**
     * @param string|NULL $file_path
     * @return float|int|mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_audio_hz(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'audio_hz'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'audio_hz');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new ParametersException($error);

        }

        $data=self::get_info_list($file_path);

        if(count($data)==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'File info is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        $index=self::get_index('Audio:',$data);

        if($index==-1){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Bitrate not found',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        if(preg_match_all('/\s*(\d*)\s*Hz/is',$data[$index],$list)===false){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Hz is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        if(count($list[1])==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Hz is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        $audio_hz=(int)$list[1][0]*1000;

        FfmpegCash::add_file_parameter_in_cash($file_path,'audio_hz',$audio_hz);

        return $audio_hz;

    }

    /**
     * @param string|NULL $file_path
     * @return mixed|string[]
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_size(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'video_size'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'video_size');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new ParametersException($error);

        }

        $data=self::get_info_list($file_path);

        if(count($data)==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'File info is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        $index=self::get_index('Video:',$data);

        if($index==-1){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Bitrate not found',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        if(preg_match_all('/\s*(\d*x\d*),/is',$data[$index],$list)===false){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Video size is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        if(count($list[1])==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Hz is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        $list=mb_split('x',$list[1][0]);

        FfmpegCash::add_file_parameter_in_cash($file_path,'video_size',$list);

        return $list;

    }

    /**
     * @param string|NULL $file_path
     * @return float|mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_fps(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'video_fps'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'video_fps');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new ParametersException($error);

        }

        $data=self::get_info_list($file_path);

        if(count($data)==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'File info is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        $index=self::get_index('Video:',$data);

        if($index==-1){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Bitrate not found',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        if(preg_match_all('/\s*(\d*\D\d*)\s*fps,/is',$data[$index],$list)===false){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Video fps is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        if(count($list[1])==0){

            $error=array(
                'title' =>'File info problem',
                'info'  =>'Video fps is empty',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new FileException($error);

        }

        $video_fps=(double)$list[1][0];

        FfmpegCash::add_file_parameter_in_cash($file_path,'video_size',$video_fps);

        return $video_fps;

    }

    /**
     * @param string|NULL $file_path
     * @return int|mixed
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_duration_in_milliseconds(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(FfmpegCash::isset_file_parameter_in_cash($file_path,'duration_milliseconds'))
            return FfmpegCash::get_file_parameter_in_cash($file_path,'duration_milliseconds');

        if(!file_exists($file_path)){

            $error=array(
                'title' =>'Path problem',
                'info'  =>'File is not exists',
                'data'  =>array(
                    'file_path'=>$file_path
                )
            );

            throw new ParametersException($error);

        }

        $duration_string    =self::get_duration($file_path);
        $duration_ms        = Time::get_milliseconds_from_time_string($duration_string);

        FfmpegCash::add_file_parameter_in_cash($file_path,'duration_milliseconds',$duration_ms);

        return $duration_ms;

    }

    /**
     * @param string|NULL $codec
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function valid_audio_codec(string $codec=NULL){

        if(empty($codec)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio codec is empty'
            );

            throw new ParametersException($error);

        }

        return array_search($codec,AudioConfig::$audio_codec_list)!==false;

    }

    /**
     * @param int|NULL $width
     * @param int|NULL $height
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function valid_video_size(int $width=NULL,int $height=NULL){

        if(empty($width)||empty($height)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Video size is empty'
            );

            throw new ParametersException($error);

        }

        return array_search($width.'x'.$height,VideoConfig::$video_size_list)!==false;

    }

    /**
     * @param string|NULL $bitrate
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function valid_audio_bitrate(string $bitrate=NULL){

        if(empty($bitrate)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Bitrate is empty'
            );

            throw new ParametersException($error);

        }

        return array_search($bitrate,AudioConfig::$audio_bitrate_list)!==false;

    }

    /**
     * @param string|NULL $hz
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function valid_audio_hz(string $hz=NULL){

        if(empty($hz)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Audio HZ is empty'
            );

            throw new ParametersException($error);

        }

        return array_search($hz,AudioConfig::$audio_hz_list)!==false;

    }

    /**
     * @param string|NULL $file_path_from
     * @param string|NULL $file_path_to
     * @param string $codec
     * @param int $bitrate
     * @param int $hz
     * @return bool
     * @throws FileException
     * @throws ParametersException
     * @throws PathException
     * @throws ShellExecException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function convert_audio(string $file_path_from=NULL,string $file_path_to=NULL,string $codec='libmp3lame',int $bitrate=128000,int $hz=44100){

        $error_info_list=[];

        if(empty($file_path_from))
            $error_info_list[]='File path from is empty';

        if(empty($file_path_to))
            $error_info_list[]='File path to is empty';

        if(empty($codec))
            $error_info_list[]='Audio codec is empty';

        if(empty($bitrate))
            $error_info_list[]='Audio bitrate is empty';

        if(empty($hz))
            $error_info_list[]='Audio Hz is empty';

        if(!self::valid_audio_codec($codec))
            $error_info_list[]='Audio codec is not valid';

        if(!self::valid_audio_bitrate($bitrate))
            $error_info_list[]='Audio bitrate is not valid';

        if(!self::valid_audio_hz($hz))
            $error_info_list[]='Audio Hz is not valid';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'file_path_from'        =>$file_path_from,
                    'file_path_to'          =>$file_path_to,
                    'codec'                 =>$codec,
                    'bitrate'               =>$bitrate,
                    'hz'                    =>$hz
                )
            );

            throw new ParametersException($error);

        }

        if(!file_exists($file_path_from))
            $error_info_list[]='Audio path from is not exists';

        if(file_exists($file_path_to))
            $error_info_list[]='Audio path to already exists';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Path problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'file_path_from'        =>$file_path_from,
                    'file_path_to'          =>$file_path_to
                )
            );

            throw new ParametersException($error);

        }

        $bitrate_from   =self::get_audio_bitrate($file_path_from);
        $hz_from        =self::get_audio_hz($file_path_from);

        if($bitrate_from<$bitrate)
            $bitrate=$bitrate_from;

        if($hz_from<$hz)
            $hz=$hz_from;

        if(empty(FfmpegConfig::$ffmpeg_path))
            FfmpegConfig::set_ffmpeg_command_path();

        $dir_root   =$_SERVER['DOCUMENT_ROOT'];
        $q          =FfmpegConfig::$ffmpeg_path.' -i '.$dir_root.'/'.$file_path_from.' -c:a '.$codec.' -b:a '.$bitrate.' -ar '.$hz.' -y '.$dir_root.'/'.$file_path_to.' 2>&1';

        exec($q,$r);

        if(count($r)<20){

            $error=array(
                'title'     =>'Ffmepg problem',
                'info'      =>'Audio file was not converted',
                'data'      =>array(
                    'file_path_from'        =>$file_path_from,
                    'file_path_to'          =>$file_path_to,
                    'codec'                 =>$codec,
                    'bitrate'               =>$bitrate,
                    'hz'                    =>$hz,
                    'query'                 =>$q
                )
            );

            throw new ParametersException($error);

        }

        return file_exists($file_path_to);

    }

}