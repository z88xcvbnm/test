<?php

namespace Core\Module\Ffmpeg;

use Core\Module\Exception\PhpException;
use Core\Module\OsServer\OsServer;

class FfmpegConfig{

    /** @var string */
    public  static $ffmpeg_path;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$ffmpeg_path=NULL;

        return true;

    }

    /**
     * @return bool
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function set_ffmpeg_command_path(){

        if(OsServer::$is_linux)
            self::$ffmpeg_path='ffmpeg';
        else if(OsServer::$is_windows)
            self::$ffmpeg_path='C:\ffmpeg\bin\ffmpeg.exe';
        else if(OsServer::$is_freebsd)
            self::$ffmpeg_path='ffmpeg';
        else{

            $error=array(
                'title'     =>'Server problem',
                'info'      =>'FFMpeg command path is not valid'
            );

            throw new PhpException($error);

        }

        return true;

    }

}