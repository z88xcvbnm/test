<?php

namespace Core\Module\Ffmpeg;

use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class FfmpegCash{

    /** @var array */
    public  static $file_path_list=[];

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$file_path_list=[];

        return true;

    }

    /**
     * @param string|NULL $file_path
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_file_path_in_cash(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return isset(self::$file_path_list[$file_path]);

    }

    /**
     * @param string|NULL $file_path
     * @param string|NULL $param
     * @return bool
     */
    public  static function isset_file_parameter_in_cash(string $file_path=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(empty(self::$file_path_list[$file_path]))
            return false;

        return !empty(self::$file_path_list[$file_path][$param]);

    }

    /**
     * @param string|NULL $file_path
     * @param string|NULL $param
     * @return null
     */
    public  static function get_file_parameter_in_cash(string $file_path=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(empty(self::$file_path_list[$file_path]))
            return NULL;

        return self::$file_path_list[$file_path][$param];

    }

    /**
     * @param string|NULL $file_path
     * @param string|NULL $param
     * @param null $value
     * @param bool $is_rewrite
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_file_parameter_in_cash(string $file_path=NULL,string $param=NULL,$value=NULL,bool $is_rewrite=true){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($param))
            $error_info_list[]='Parameters is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'file_path'      =>$file_path,
                    'param'         =>$param,
                    'value'         =>$value,
                    'is_rewrite'    =>$is_rewrite
                )
            );

            throw new ParametersException($error);

        }

        if(empty(self::$file_path_list[$file_path]))
            self::$file_path_list[$file_path]=[];

        if($is_rewrite)
            self::$file_path_list[$file_path][$param]=$value;

        return true;

    }

    /**
     * @param string|NULL $file_path
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_file_from_cash(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        if(self::isset_file_path_in_cash($file_path))
            unset($file_path);

        return true;

    }

    /**
     * @param string|NULL $file_path
     * @param string|NULL $param
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_file_parameter_from_cash(string $file_path=NULL,string $param=NULL){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(empty($param))
            $error_info_list[]='Parameter is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list,
                'data'  =>array(
                    'file_path'      =>$file_path,
                    'param'         =>$param,
                )
            );

            throw new ParametersException($error);

        }

        if(self::isset_file_path_in_cash($file_path))
            if(self::isset_file_parameter_in_cash($file_path,$param))
                unset(self::$file_path_list[$file_path]);

        return true;

    }

    /**
     * @param string|NULL $file_path
     * @param array $param_list
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_file_parameters_list_form_cash(string $file_path=NULL,array $param_list=[]){

        $error_info_list=[];

        if(empty($file_path))
            $error_info_list[]='File path is empty';

        if(count($param_list)==0)
            $error_info_list[]='Parameters list is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list,
                'data'  =>array(
                    'file_path'       =>$file_path,
                    'param_list'    =>$param_list,
                )
            );

            throw new ParametersException($error);

        }

        if(self::isset_file_path_in_cash($file_path))
            foreach($param_list as $param)
                if(!self::remove_file_parameter_from_cash($file_path,$param)){

                    $error=[
                        'title'     =>DbQueryException::$title,
                        'info'      =>'File parameters was not remove'
                    ];

                    throw new DbQueryException($error);

                }

        return true;

    }

}