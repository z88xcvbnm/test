<?php

namespace Core\Module\Timezone;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Worktime\Worktime;

class TimezoneBackup{

    /** @var int */
    public  static $timezone_ID;

    /** @var string */
    public  static $timezone_name;

    /** @var int */
    public  static $offset_utc;

    /**
     * Reset default data
     */
    public  static function reset_data(){

        self::$timezone_ID      =NULL;
        self::$timezone_name    =NULL;
        self::$offset_utc       =NULL;

    }

    /**
     * @param string|NULL $timzone_name
     * @return null
     * @throws ParametersException
     */
    public  static function get_timezone_ID_from_timzone_name(string $timzone_name=NULL){

        if(empty($timzone_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Timezone name is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                array(
                    'column'=>'id'
                )
            ),
            'table'=>'_timezone',
            'where'=>array(
                array(
                    'column'    =>'name',
                    'value'     =>$timzone_name
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return count($r)==0?NULL:$r[0]['id'];

    }

    /**
     * @param int|NULL $timezone_ID
     * @return string|null
     * @throws ParametersException
     */
    public  static function get_timezone_name_from_timezone_ID(int $timezone_ID=NULL){

        if(empty($timezone_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Timezone ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                array(
                    'column'=>'name'
                )
            ),
            'table'=>'_timezone',
            'where'=>array(
                array(
                    'column'    =>'id',
                    'value'     =>$timezone_ID
                ),
                array(
                    'column'    =>'type',
                    'value'     =>0
                )
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        return count($r)==0?NULL:$r[0]['name'];

    }

    /**
     * @param string|NULL $timezone_name
     * @return mixed
     * @throws ParametersException
     */
    public  static function get_offset_utc_from_timzone_name(string $timezone_name=NULL){

        if(empty($timezone_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Timezone name is empty'
            );

            throw new ParametersException($error);

        }

        return Date::get_offset_utc_from_timzone_name($timezone_name);

    }

    public  static function set_timezone_name_default(){

        if(empty(self::$timezone_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default timezone ID is empty'
            );

            throw new ParametersException($error);

        }

        self::$timezone_name=self::get_timezone_name_from_timezone_ID(self::$timezone_ID);

    }
    public  static function set_offset_utc_default(){

        if(empty(self::$timezone_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default timezone name is empty'
            );

            throw new ParametersException($error);

        }

        self::$offset_utc=self::get_offset_utc_from_timzone_name(self::$timezone_name);

    }
    private static function set(){

    }

    public  static function init(){

        Worktime::set_timestamp_point('Timezone Start');

        self::set();

        Worktime::set_timestamp_point('Timezone Finish');

    }

}