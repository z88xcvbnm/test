<?php

namespace Core\Module\Timezone;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class Timezone{

    /** @var int */
    public  static $timezone_ID;

    /** @var string */
    public  static $timezone_name;

    /** @var int */
    public  static $offset_utc;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$timezone_ID      =NULL;
        self::$timezone_name    =NULL;
        self::$offset_utc       =NULL;

        return true;

    }

    /**
     * @param int|NULL $timezone_ID
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_timezone_ID(int $timezone_ID=NULL){

        if(empty($timezone_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Timezone ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($timezone_ID,'_timezone',0);

    }

    /**
     * @param string|NULL $timezone_name
     * @return bool
     * @throws ParametersException
     */
    public  static function isset_timezone(string $timezone_name=NULL){

        if(empty($timezone_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Timezone name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name'=>$timezone_name
        );

        return Db::isset_row('_timezone',0,$where_list);

    }

    /**
     * @param string|NULL $timezone_name
     * @return int|null
     * @throws ParametersException
     */
    public  static function get_timezone_ID(string $timezone_name=NULL){

        if(empty($timezone_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Timezone name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name'=>$timezone_name
        );

        return Db::get_row_ID('_timezone',0,$where_list);

    }

    /**
     * @param int|NULL $timezone_ID
     * @return string|null
     * @throws ParametersException
     */
    public  static function get_timezone_name(int $timezone_ID=NULL){

        if(empty($timezone_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Timezone name is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_timezone',
            'where'=>array(
                'id'    =>$timezone_ID,
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param string|NULL $timezone_name
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     */
    public  static function add_timezone(string $timezone_name=NULL){

        if(empty($timezone_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Timezone name is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_timezone',
            'values'    =>array(
                'name'      =>$timezone_name
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Timezone was not added'
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $timezone_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_timezone_ID(int $timezone_ID=NULL){

        if(empty($timezone_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Timezone ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($timezone_ID,'_timezone',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Timezone was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $timezone_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     */
    public  static function remove_timezone(string $timezone_name=NULL){

        if(empty($timezone_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Timezone name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name'=>$timezone_name
        );

        if(!Db::delete_from_where_list('_timezone',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Timezone was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $timezone_ID
     */
    public  static function set_timezone_ID_default(int $timezone_ID=NULL){

        self::$timezone_ID=empty($timezone_ID)?NULL:$timezone_ID;

    }

    /**
     * @param string|NULL $timezone_name
     */
    public  static function set_timezone_name(string $timezone_name=NULL){

        self::$timezone_name=empty($timezone_name)?NULL:$timezone_name;

    }

}