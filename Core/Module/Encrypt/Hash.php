<?php

namespace Core\Module\Encrypt;

use Core\Module\Json\Json;

class Hash{

    /**
     * @param $data
     * @param string $type
     * @return string
     */
    public  static function get_encode($data,string $type='sha1'){

        return hash($type,$data);

    }

    /**
     * @param $data
     * @return string // 40 symbols
     */
    public  static function get_sha1_encode($data){

        return self::get_encode($data);

    }

    /**
     * @param $data
     * @return string // 64 symbols
     */
    public  static function get_sha256_encode($data){

        return self::get_encode($data,'sha256');

    }

    /**
     * @param $data
     * @return string // 96 symbols
     */
    public  static function get_sha384_encode($data){

        return self::get_encode($data,'sha384');

    }

    /**
     * @param $data
     * @return string // 128 symbols
     */
    public  static function get_sha512_encode($data){

        return self::get_encode($data,'sha512');

    }

    /**
     * @param $data
     * @return string // 32 symbols
     */
    public  static function get_md2_encode($data){

        return self::get_encode($data,'md2');

    }

    /**
     * @param $data
     * @return string // 32 symbols
     */
    public  static function get_md4_encode($data){

        return self::get_encode($data,'md4');

    }

    /**
     * @param $data
     * @return string // 32 symbols
     */
    public  static function get_md5_encode($data){

        return self::get_encode($data,'md5');

    }

    /**
     * @param string $type
     * @return string
     */
    public  static function get_random_hash(string $type='sha1'){

        return self::get_encode(uniqid(rand(),true),$type);

    }

    /**
     * @param array $data
     * @param string $type
     * @return string
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_random_hash_from_data(array $data=[],string $type='sha1'){

        return self::get_encode(uniqid(Json::encode($data),true),$type);

    }

}