<?php

namespace Core\Module\Encrypt;

class HashFile{

    /**
     * @param string $file_path
     * @param string $type
     * @return string
     */
    public static function get_encode(string $file_path,string $type='sha1'){

        return hash_file($type,$file_path);

    }

    /**
     * @param $file_path
     * @return string // 40 symbols
     */
    public static function get_sha1_encode($file_path){

        return self::get_encode($file_path);

    }

    /**
     * @param $file_path
     * @return string // 64 symbols
     */
    public static function get_sha256_encode($file_path){

        return self::get_encode($file_path,'sha256');

    }

    /**
     * @param $file_path
     * @return string // 96 symbols
     */
    public static function get_sha384_encode($file_path){

        return self::get_encode($file_path,'sha384');

    }

    /**
     * @param $file_path
     * @return string // 128 symbols
     */
    public static function get_sha512_encode($file_path){

        return self::get_encode($file_path,'sha512');

    }

    /**
     * @param $file_path
     * @return string // 32 symbols
     */
    public static function get_md2_encode($file_path){

        return self::get_encode($file_path,'md2');

    }

    /**
     * @param $file_path
     * @return string // 32 symbols
     */
    public static function md4_encode($file_path){

        return self::get_encode($file_path,'md4');

    }

    /**
     * @param $file_path
     * @return string // 32 symbols
     */
    public static function get_md5_encode($file_path){

        return self::get_encode($file_path,'md5');

    }

}