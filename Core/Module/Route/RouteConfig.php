<?php

namespace Core\Module\Route;

class RouteConfig{

    /** @var array */
    public  static $domain_list=array(
        'whore'=>array(
            'api'       =>true,
            'page'      =>true,
            'system'    =>true
        ),
        'api.whore'=>array(
            'api'       =>true,
            'page'      =>false,
            'system'    =>true
        )
    );

}