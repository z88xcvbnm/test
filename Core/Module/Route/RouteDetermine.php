<?php

namespace Core\Module\Route;

use Core\Module\App\App;
use Core\Module\App\AppVersion;
use Core\Module\Browser\Browser;
use Core\Module\Browser\BrowserDetermine;
use Core\Module\Browser\BrowserVersion;
use Core\Module\Cookie\Cookie;
use Core\Module\Device\DeviceDetermine;
use Core\Module\Device\DeviceFirm;
use Core\Module\Device\DeviceModel;
use Core\Module\Device\DeviceToken;
use Core\Module\Geo\City;
use Core\Module\Geo\Continent;
use Core\Module\Geo\Country;
use Core\Module\Geo\Geo;
use Core\Module\Geo\GeoDetermine;
use Core\Module\Geo\GeoIp;
use Core\Module\Geo\Region;
use Core\Module\History\History;
use Core\Module\Ip\Ip;
use Core\Module\Ip\IpDetermine;
use Core\Module\Lang\Lang;
use Core\Module\Lang\LangDetermine;
use Core\Module\Lang\LangKeyboard;
use Core\Module\Os\Os;
use Core\Module\Os\OsDetermine;
use Core\Module\Os\OsVersion;
use Core\Module\OsServer\OsServerDetermine;
use Core\Module\Path\PathDetermine;
use Core\Module\Response\Response;
use Core\Module\Session\Session;
use Core\Module\Timezone\Timezone;
use Core\Module\Token\Token;
use Core\Module\Token\TokenConfig;
use Core\Module\Url\Url;
use Core\Module\Url\UrlDetermine;
use Core\Module\User\User;
use Core\Module\User\UserAccess;
use Core\Module\User\UserDevice;
use Core\Module\Useragent\Useragent;
use Core\Module\Useragent\UseragentDetermine;
use Core\Module\Worktime\Worktime;
use Project\Whore\All\Module\Lang\LangDetermineProject;

class RouteDetermine{

    /** @var int */
    private static $token_ID;

    /** @var string */
    public  static $token_hash;

    /** @var string */
    public  static $token_hash_first;

    /** @var array */
    public  static $token_data;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$token_ID             =NULL;
        self::$token_hash           =NULL;
        self::$token_hash_first     =NULL;
        self::$token_data           =NULL;

        return true;

    }

    /**
     * @return bool
     */
    private static function isset_token_hash_in_cookie(){

        if(Cookie::isset_token())
            self::$token_hash=Cookie::get_token();

        if(isset($_POST['token']))
            self::$token_hash=$_POST['token'];

        if(empty(self::$token_hash))
            return false;

        return true;

    }

    /**
     * @return array
     */
    private static function get_determine_data(){

        $list=array(
            'token_ID'              =>Token::$token_ID,
            'session_ID'            =>Session::$session_ID,
            'browser_ID'            =>Browser::$browser_ID,
            'browser_name'          =>Browser::$browser_name,
            'browser_version_ID'    =>BrowserVersion::$browser_version_ID,
            'browser_version'       =>BrowserVersion::$browser_version,
            'os_ID'                 =>Os::$os_ID,
            'os_name'               =>Os::$os_name,
            'os_version_ID'         =>OsVersion::$os_version_ID,
            'os_version'            =>OsVersion::$os_version,
            'device_firm_ID'        =>DeviceFirm::$device_firm_ID,
            'device_firm_name'      =>DeviceFirm::$device_firm_name,
            'device_model_ID'       =>DeviceModel::$device_model_ID,
            'device_model_name'     =>DeviceModel::$device_model_name,
            'geo_ID'                =>Geo::$geo_ID,
            'country_geo_ID'        =>Geo::$country_geo_ID,
            'geo_ip_ID'             =>GeoIp::$geo_ip_ID,
            'geo_ip_network'        =>GeoIp::$geo_ip_network,
            'geo_postal_code'       =>GeoIp::$postal_code,
            'geo_ip_latitude'       =>GeoIp::$latitude,
            'geo_ip_longitude'      =>GeoIp::$longitude,
            'geo_ip_radius'         =>GeoIp::$radius,
            'continent_ID'          =>Continent::$continent_ID,
            'country_ID'            =>Country::$country_ID,
            'region_ID'             =>Region::$region_ID,
            'city_ID'               =>City::$city_ID
        );

        return $list;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ForbiddenException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(Response::$type=='shell')
            return false;

        self::$token_hash_first=$_COOKIE;

        PathDetermine::init();
        History::init();
        UrlDetermine::init();

        $need_determine=true;

        if(count(Url::$list)>0)
            if(Url::$list[0]=='show')
                $need_determine=false;

        if($need_determine){

            IpDetermine::init();
            UseragentDetermine::init();
            LangDetermine::init();
            BrowserDetermine::init();
            OsServerDetermine::init();
            OsDetermine::init();
            DeviceDetermine::init();
            LangDetermineProject::init();

        }

        if(self::isset_token_hash_in_cookie()){

            self::$token_hash     =Cookie::get_token();
            self::$token_data     =Token::get_token_data_from_hash(self::$token_hash);

            if(!empty($_REQUEST['is_token_data'])){

                print_r(self::$token_data);
                print_r(self::$token_hash_first);

            }

            if(is_array(self::$token_data))
                if(count(self::$token_data)>0){

                    $isset_user=false;

                    if(!$need_determine){

                            if(!empty(self::$token_data['user_ID']))
//                                if(User::isset_user_ID(self::$token_data['user_ID']))
                                    $isset_user=true;

                            if($isset_user){

                                Token::set_token_ID_default(self::$token_data['token_ID']);
                                Token::set_token_hash_default(self::$token_hash);
                                TokenConfig::set_token_is_keep_default((bool)self::$token_data['is_keep']);
                                User::set_user_ID_default(self::$token_data['user_ID']);
                                UserDevice::set_user_device_ID_default(self::$token_data['user_device_token_ID']);
                                UserAccess::get_user_access_list_default();
                                Continent::set_continent_ID_default(self::$token_data['continent_ID']);
                                Country::set_country_ID_default(self::$token_data['country_ID']);
                                Region::set_region_ID_default(self::$token_data['region_ID']);
                                City::set_city_ID_default(self::$token_data['city_ID']);
                                Timezone::set_timezone_ID_default(self::$token_data['timezone_ID']);
                                Useragent::set_useragent_ID_default(self::$token_data['useragent_ID']);
                                Ip::set_ip_ID_default(self::$token_data['ip_ID']);

                                return true;

                            }

                    }

                    if(!empty(self::$token_data['user_ID']))
                        if(User::isset_user_ID(self::$token_data['user_ID']))
                            $isset_user=true;

                    if($isset_user){

                        $is_token_update=false;

                        Token::set_token_ID_default(self::$token_data['token_ID']);
                        Token::set_token_hash_default(self::$token_hash);
                        TokenConfig::set_token_is_keep_default((bool)self::$token_data['is_keep']);
                        User::set_user_ID_default(self::$token_data['user_ID']);
                        UserDevice::set_user_device_ID_default(self::$token_data['user_device_token_ID']);

                        if(!empty(User::$user_ID)){

                            User::update_user_date_online_default();
                            UserAccess::get_user_access_list_default();

                        }

                        if(!empty(self::$token_data['useragent_ID'])){

                            $useragent              =Useragent::get_useragent_from_useragent_ID(self::$token_data['useragent_ID']);
                            $useragent_default      =Useragent::get_useragent_default();

                            if($useragent==$useragent_default)
                                Useragent::set_useragent_ID_default(self::$token_data['useragent_ID']);
                            else
                                $is_token_update=true;

                        }
                        else{

                            UseragentDetermine::init();

                            $is_token_update=true;

                        }

                        if(!empty(self::$token_data['ip_ID'])){

                            $ip_ID          =self::$token_data['ip_ID'];
                            $ip_address     =Ip::get_ip_address($ip_ID);

                            if($ip_address==Ip::$ip_address){

                                Ip::set_ip_ID_default(self::$token_data['ip_ID']);
                                Continent::set_continent_ID_default(self::$token_data['continent_ID']);
                                Country::set_country_ID_default(self::$token_data['country_ID']);
                                Region::set_region_ID_default(self::$token_data['region_ID']);
                                City::set_city_ID_default(self::$token_data['city_ID']);
                                Timezone::set_timezone_ID_default(self::$token_data['timezone_ID']);

                            }
                            else{

                                if(\Config::$is_geo_determine)
                                    GeoDetermine::init();

                                $is_token_update=true;

                            }

                        }
                        else{

                            IpDetermine::init();

                            if(\Config::$is_geo_determine)
                                GeoDetermine::init();

                            $is_token_update=true;

                        }

                        if(Lang::$lang_ID!=self::$token_data['lang_ID'])
                            $is_token_update=true;

                        if(LangKeyboard::$lang_keyboard_ID!=self::$token_data['lang_keyboard_ID'])
                            $is_token_update=true;

                        if(Browser::$browser_ID!=self::$token_data['browser_ID'])
                            $is_token_update=true;

                        if(BrowserVersion::$browser_version_ID!=self::$token_data['browser_version_ID'])
                            $is_token_update=true;

                        if(Os::$os_ID!=self::$token_data['os_ID'])
                            $is_token_update=true;

                        if(OsVersion::$os_version_ID!=self::$token_data['os_version_ID'])
                            $is_token_update=true;

                        if(DeviceFirm::$device_firm_ID!=self::$token_data['device_firm_ID'])
                            $is_token_update=true;

                        if(DeviceModel::$device_model_ID!=self::$token_data['device_model_ID'])
                            $is_token_update=true;

                        if(DeviceToken::$device_token_ID!=self::$token_data['device_token_ID'])
                            $is_token_update=true;

                        if(App::$app_ID!=self::$token_data['app_ID'])
                            $is_token_update=true;

                        if(AppVersion::$app_version_ID!=self::$token_data['app_version_ID'])
                            $is_token_update=true;

                        if(Lang::$lang_ID!=self::$token_data['lang_ID'])
                            $is_token_update=true;

                        Session::set_session_ID_default(Session::get_session_ID(User::$user_ID,Token::$token_ID));

                        Worktime::set_timestamp_point('Start session');

                        Worktime::set_timestamp_point('Finish session');
                        Worktime::set_timestamp_point('Start reset token');

                        if($is_token_update){

                            Token::reset_token_default();

                            if(!empty(Session::$session_ID))
                                Session::remove_session_ID_default();

                        }
                        else
                            Token::update_token_date_live_default();

                        if(empty(Session::$session_ID))
                            Session::add_session_default();
                        else
                            Session::update_session_date_live_default();

                        Worktime::set_timestamp_point('Finish reset token');

                        Cookie::update_token();

                        Worktime::set_timestamp_point('Updated token');

                        if(\Config::$is_geo_debug)
                            print_r(self::get_determine_data());

                        Worktime::set_timestamp_point('Start update token in history');

                        History::update_token();

                        Worktime::set_timestamp_point('Finish update token in history');

                        return true;

                    }

                }

        }

//        echo"if empty token data\n";

        if(\Config::$is_geo_determine)
            GeoDetermine::init();

        if(empty(self::$token_hash)){

//            echo"if empty token hash\n";

            Token::add_token_default();

        }
        else{

            self::$token_ID=Token::get_token_ID_from_token_hash(self::$token_hash);

//            echo"token ID from hash: ".self::$token_ID."\n";

            if(empty(self::$token_ID)){

//                echo"token ID is empty -> reset\n";

                Token::add_token_default();

            }
            else{

//                echo"if token ID is not empty\n";

                Token::set_token_ID_default(self::$token_ID);
                Token::set_token_hash_default(self::$token_hash);
                Token::update_token_date_live_default();

            }

        }

        Session::add_session_default();
        Cookie::create_token();

        if(\Config::$is_geo_debug)
            print_r(self::get_determine_data());

        History::update_token();

        return true;

    }

    /**
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ForbiddenException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Worktime::set_timestamp_point('Token Start');

        self::set();

        Worktime::set_timestamp_point('Token Finish');

    }

}