<?php

namespace Core\Module\Route\RoutePage;

use Core\Module\Response\Response;
use Core\Module\Url\Url;
use Core\Module\Worktime\Worktime;
use Project\All\Page\NotFoundPage;
use Project\Whore\Admin\Route\RouteAdminWhorePage;

class RoutePage{

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        switch(Url::$host){

            case'whore':
            case'shluham.net':
                return RouteAdminWhorePage::init();

//            case'api.whore':
//            case'api.shluham.net':
//                return RouteApiWhorePage::init();

            default:
                return NotFoundPage::init();

        }

    }

    /**
     * @return bool
     */
    private static function set_worktime(){

        if(\Config::$is_debug){

            echo '<!--'.Worktime::get_delta().'-->';
            echo '<!--'.print_r(Worktime::$timestamp_list,true).'-->';

        }

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_url();
        self::set_worktime();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Response::$type='page';

        return self::set();

    }

}