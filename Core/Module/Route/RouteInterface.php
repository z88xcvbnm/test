<?php

namespace Core\Module\Route;

use Core\Module\Response\Response;
use Core\Module\Route\RouteApi\RouteApi;
use Core\Module\Route\RouteFileDownload\RouteFileDownload;
use Core\Module\Route\RoutePage\RoutePage;
use Core\Module\Url\Url;

class RouteInterface{

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShluhamNetException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        if(Response::$type=='shell')
            return false;

        if(count(Url::$list)>=3)
            switch(Url::$list[0]){

                case'api':
                    return RouteApi::init();

                case'download':
                case'show':
                    return RouteFileDownload::init();

            }

        return RoutePage::init();

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShluhamNetException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        return self::set();

    }

}