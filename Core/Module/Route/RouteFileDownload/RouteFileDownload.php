<?php

namespace Core\Module\Route\RouteFileDownload;

use Core\Module\Response\Response;
use Core\Module\Url\Url;
use Core\Module\Worktime\Worktime;
use Project\All\Page\NotFoundPage;
use Project\Whore\Admin\Route\FileDownload\RouteAdminWhoreFileDownload;

class RouteFileDownload{

    /**
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        switch(Url::$host){

            case'shluham.net':
            case'whore':{

                RouteAdminWhoreFileDownload::init();

                break;

            }

            default:{

                NotFoundPage::init();

                break;

            }

        }

    }

    /**
     * @return bool
     */
    private static function set_worktime(){

        echo '<!--'.Worktime::get_delta().'-->';

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_url();
        self::set_worktime();

        return true;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Response::$type='json';

        return self::set();

    }

}