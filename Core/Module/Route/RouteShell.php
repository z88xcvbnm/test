<?php

namespace Core\Module\Route;

use Core\Module\Exception\ParametersException;
use Core\Module\History\History;
use Core\Module\Ip\IpShellDetermine;
use Core\Module\Php\PhpValidation;
use Core\Module\Response\Response;
use Core\Module\Url\UrlShellDetermine;
use Project\Whore\Admin\Route\Shell\RouteAdminWhoreShell;

class RouteShell{

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        if(PhpValidation::is_console_running()){

            Response::$type='shell';

            UrlShellDetermine::init();
            IpShellDetermine::init();
            History::init();

            return RouteAdminWhoreShell::init();

        }

        return false;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        return self::set();

    }

}