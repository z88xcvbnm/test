<?php

namespace Core\Module\Route\RouteApi;

use Core\Module\Response\Response;
use Core\Module\Response\ResponseNotFound;
use Core\Module\Route\RouteApi\RouteApiJson\RouteApiJson;
use Core\Module\Url\Url;

class RouteApi{

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShluhamNetException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    private static function set(){

        if(count(Url::$list)>=3)
            if(strtolower(Url::$list[0])=='api')
                switch(Url::$list[1]){

                    case'json':{

                        Response::$type='json';

                        return RouteApiJson::init();

                    }

                    default:{

                        $data=array(
                            'title'     =>'Page not found',
                            'info'      =>'Json type is not exists'
                        );

                        return ResponseNotFound::init($data);

                    }

                }

        $data=array(
            'title'     =>'Page not found',
            'info'      =>'Host is not exists'
        );

        ResponseNotFound::init($data);

        return false;

    }

    /**
     * @return bool
     * @throws \Core\Module\Exception\AccessDeniedException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\ParametersException
     * @throws \Core\Module\Exception\ParametersValidationException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShluhamNetException
     * @throws \Core\Module\Exception\SystemException
     * @throws \ImagickException
     */
    public  static function init(){

        Response::$type='json';

        return self::set();

    }

}