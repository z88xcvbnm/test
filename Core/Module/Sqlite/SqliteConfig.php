<?php

namespace Core\Module\Sqlite;

class SqliteConfig{

    /** @var string */
    public  static $begin_transaction       ='BEGIN TRANSACTION;';

    /** @var string */
    public  static $commit                  ='COMMIT;';

    /** @var int */
    public  static $insert_limit            =500;

    /** @var int */
    public  static $transaction_limit       =5000;

}