<?php

namespace Core\Module\Sqlite;

use Core\Module\Exception\ParametersException;

class Sqlite{

    /**
     * @param string|NULL $sqlite_path
     * @return Sqlite
     * @throws ParametersException
     */
    public  static function open_sqlite(string $sqlite_path=NULL){

        if(empty($sqlite_path)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Sqlite path is empty'
            );

            throw new ParametersException($error);

        }

        return new \SQLite3($sqlite_path);

    }

    /**
     * @param Sqlite|NULL $sqlite_object
     * @return bool
     * @throws ParametersException
     */
    public  static function close_sqlite($sqlite_object=NULL){

        if(empty($sqlite_object)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Sqlite object is empty'
            );

            throw new ParametersException($error);

        }

        return $sqlite_object->close();

    }

    /**
     * @param string|NULL $sqlite_path
     * @param null $data
     * @return null|string|int|bool|double
     * @throws ParametersException
     */
    public  static function get_escape_string(string $sqlite_path=NULL,$data=NULL){

        if(empty($sqlite_path)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Sqlite object is empty'
            );

            throw new ParametersException($error);

        }

        $sqlite_object=self::open_sqlite($sqlite_path);

        $data=$sqlite_object->escapeString($data);

        self::close_sqlite($sqlite_object);

        return $data;

    }

    /**
     * @param string|NULL $sqlite_path
     * @param string|NULL $query
     * @return bool
     * @throws ParametersException
     */
    public  static function exec(string $sqlite_path=NULL,string $query=NULL){

        $error_info_list=[];

        if(empty($sqlite_path))
            $error_info_list['sqlite_path']='Sqlite path is empty';

        if(empty($query))
            $error_info_list['query']='Query is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $sqlite_object=self::open_sqlite($sqlite_path);

        $sqlite_object->exec(SqliteConfig::$begin_transaction);
        $sqlite_object->exec($query);
        $sqlite_object->exec(SqliteConfig::$commit);

        return self::close_sqlite($sqlite_object);

    }

}