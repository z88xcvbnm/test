<?php

namespace Core\Module\Sqlite;

use Core\Module\Exception\ParametersException;

class SqliteCreateTable{

    /** @var string */
    public  static $table_name;

    /** @var array */
    public  static $table_list;

    /**
     * @param array $data
     * @return array
     * @throws ParametersException
     */
    private static function get_foreign_key_list(){

        if(count(static::$table_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Column data is empty'
            );

            throw new ParametersException($error);

        }

        $list=[];

        foreach(static::$table_list as $column=>$row)
            if(isset($row['foreign'])){

                $error_info_list=[];

                if(empty($row['foreign']['table']))
                    $error_info_list['table']='Foreign table is empty';

                if(empty($row['foreign']['column']))
                    $error_info_list['column']='Foreign column is empty';

                if(count($error_info_list)>0){

                    $error=array(
                        'title'     =>'Parameters problem',
                        'info'      =>$error_info_list
                    );

                    throw new ParametersException($error);

                }

                $list[]="FOREIGN KEY('".$column."') REFERENCES '".$row['foreign']['table']."'('".$row['foreign']['column']."')";

            }

        return $list;

    }

    /**
     * @param array $data
     * @return array
     * @throws ParametersException
     */
    private static function get_column_list(){

        if(count(static::$table_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Column data is empty'
            );

            throw new ParametersException($error);

        }

        $list=[];

        foreach(static::$table_list as $column=>$row){

            $temp       =[];
            $temp[]     ="'".$column."'";

            $type           =self::get_column_type($row);
            $primary_key    =self::get_column_primary_key($row);
            $autoincrement  =self::get_column_autoincrement($row);
            $default        =self::get_column_default_value($row);

            if(!empty($type))
                $temp[]=$type;

            if(!empty($primary_key))
                $temp[]=$primary_key;

            if(!empty($autoincrement))
                $temp[]=$autoincrement;

            if(!empty($default))
                $temp[]=$default;

            $list[]=implode(' ',$temp);

        }

        return $list;

    }

    /**
     * @param array $data
     * @return string
     * @throws ParametersException
     */
    private static function get_column_primary_key(array $data=[]){

        if(count($data)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Column data is empty'
            );

            throw new ParametersException($error);

        }

        if(!isset($data['primary_key']))
            return '';

        if(!$data['primary_key'])
            return '';

        return 'PRIMARY KEY';

    }

    /**
     * @param array $data
     * @return string
     * @throws ParametersException
     */
    private static function get_column_autoincrement(array $data=[]){

        if(count($data)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Column data is empty'
            );

            throw new ParametersException($error);

        }

        if(!isset($data['autoincrement']))
            return '';

        if(!$data['autoincrement'])
            return '';

        return 'AUTOINCREMENT';

    }

    /**
     * @param array $data
     * @return string
     * @throws ParametersException
     */
    private static function get_column_type(array $data=[]){

        if(count($data)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Column data is empty'
            );

            throw new ParametersException($error);

        }

        if(empty($data['type']))
            return '';

        switch($data['type']){

            case'int':
            case'integer':
                return 'INTEGER';

            case'str':
            case'string':
                return 'TEXT';

            case'double':
                return 'DOUBLE';

            default:{

                $error=array(
                    'title'     =>'Parameters problem',
                    'info'      =>'Column type is not valid'
                );

                throw new ParametersException($error);

            }

        }

    }

    /**
     * @param array $data
     * @return string
     * @throws ParametersException
     */
    private static function get_column_default_value(array $data=[]){

        if(count($data)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Column data is empty'
            );

            throw new ParametersException($error);

        }

        if(!isset($data['default']))
            return '';

//        if(Data::is_string($data['default']))
//            $value="'".$data['default']."'";
//        else
//            $value=$data['default'];

        $value="'".$data['default']."'";

        return 'DEFAULT '.$value;

    }

    /**
     * @param array $list
     * @return string
     * @throws ParametersException
     */
    private static function get_query(){

        if(count(static::$table_list)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Table list is empty'
            );

            throw new ParametersException($error);

        }

        $column_list    =self::get_column_list();
        $foreign_list   =self::get_foreign_key_list();

        $query          ="CREATE TABLE IF NOT EXISTS '".static::$table_name."' (\n";
        $query          .=implode(', '."\n",array_merge($column_list,$foreign_list));
        $query          .=')';

        return $query;

    }

    /**
     * @param string|NULL $sqlite_path
     * @param array $data
     * @return bool
     * @throws ParametersException
     */
    public  static function create_table(string $sqlite_path=NULL){

        if(empty($sqlite_path)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Sqlite path is empty'
            );

            throw new ParametersException($error);

        }

        $query=self::get_query();

        return Sqlite::exec($sqlite_path,$query);

    }

}