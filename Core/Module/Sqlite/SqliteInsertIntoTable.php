<?php

namespace Core\Module\Sqlite;

use Core\Module\Data\Data;
use Core\Module\Exception\ParametersException;

class SqliteInsertIntoTable{

    /** @var array */
    private static $device_path_list=[];

    /** @var string */
    public  static $table_name;

    /** @var array */
    public  static $list;

    /**
     * @param string|NULL $table_name
     * @param array $list
     * @return string
     * @throws ParametersException
     */
    private static function get_insert_header(string $table_name=NULL,array $list=[]){

        $error_info_list=[];

        if(empty($table_name))
            $error_info_list['table_name']='Table name is empty';

        if(count($list)==0)
            $error_info_list['list']='ProfileList is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $list=array_values($list);

        $insert_header      ="INSERT OR IGNORE INTO `".$table_name."`(";
        $col_list           =[];

        foreach($list[0] as $col_key=>$row)
            $col_list[]="`".$col_key."`";

        $insert_header.=implode(',',$col_list);
        $insert_header.=")VALUES";

        return $insert_header;

    }

    /**
     * @param string|NULL $table_name
     * @param array $list
     * @return bool
     * @throws ParametersException
     */
    private static function insert(string $device_name=NULL,string $table_name=NULL,array $list=[]){

        $error_info_list=[];

        if(empty($table_name))
            $error_info_list['table_name']='Table name is empty';

        if(count($list)==0)
            return false;

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        if(count($list)==0)
            return false;

        $temp_list=[];

        foreach($list as $row)
            if(!empty($row['type'])){

                $temp=[];

                foreach($row as $column_name=>$value){

                    if($column_name=='_id'||$column_name=='type')
                        $temp[$column_name]=$value;
                    else if(Data::is_string($value))
                        $temp[$column_name]='';
                    else
                        $temp[$column_name]=0;

                }

                $temp_list[]=$temp;

            }
            else
                $temp_list[]=$row;

        $list=$temp_list;

        if(empty($device_name))
            $device_list=array(
                'google',
                'apple'
            );
        else
            $device_list=array(
                $device_name
            );


        $insert_header      =self::get_insert_header($table_name,$list);
        $list_len           =count($list);
        $current_len        =0;

        while($list_len>$current_len){

            $transaction_query      ='';
            $transaction_len        =0;

            while(SqliteConfig::$transaction_limit>$transaction_len&&$list_len>$current_len){

                $query_list=[];

                for($i=$current_len;$i<($current_len+SqliteConfig::$insert_limit);$i++)
                    if(isset($list[$i])){

                        $temp_list=[];

                        foreach($list[$i] as $key=>$row){

                            if(is_string($list[$i][$key]))
                                $temp_list[]="'".\SQLite3::escapeString($list[$i][$key])."'";
                            else if(is_integer($list[$i][$key]))
                                $temp_list[]="'".(int)$list[$i][$key]."'";
                            else if(is_float($list[$i][$key]))
                                $temp_list[]="'".(float)$list[$i][$key]."'";

                        }

                        $query_list[]="(".implode(',',$temp_list).")";

                    }
                    else
                        break;

                if(count($query_list)>0){

                    $current_len            +=SqliteConfig::$insert_limit;
                    $transaction_len        ++;

                    $query                  =$insert_header."\n".implode(','."\n",$query_list).';';
                    $transaction_query      .=(empty($transaction_query)?'':"\n").$query;

                }
                else
                    break;

            }

            if(!empty($transaction_query))
                foreach($device_list as $device_name){

                    if(!isset(self::$device_path_list[$device_name])){

                        $error=array(
                            'title'     =>'Parameters problem',
                            'info'      =>'Device path is not exists'
                        );

                        throw new ParametersException($error);

                    }

                    Sqlite::exec(self::$device_path_list[$device_name],$transaction_query);

                }
            else
                break;

        }

        return true;

    }

    /**
     * @return bool
     */
    private static function set_return(){

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     */
    private static function prepare_list(){

        foreach(self::$list as $table_name=>$table_list)
            if(isset($table_list['list']))
                if(isset($table_list['list']['google'])||isset($table_list['list']['apple'])){

                    foreach($table_list['list'] as $device_name=>$device_list)
                        self::insert($device_name,$table_name,$device_list);

                }
                else
                    self::insert(NULL,$table_name,$table_list['list']);

        return true;

    }

    /**
     * @return bool
     */
    private static function set(){

        self::prepare_list();

        return self::set_return();

    }

    /**
     * @param string|NULL $sqlite_path
     * @param array $data
     * @return bool
     * @throws ParametersException
     */
    public  static function insert_into_table(array $device_path_list=[],array $list=[]){

        $error_info_list=[];

        if(count($device_path_list)==0)
            $error_info_list['path']='Device sqlite path is empty';

        if(count($list)==0)
            $error_info_list['list']='ProfileList is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$device_path_list     =$device_path_list;
        self::$list                 =$list;

        return self::set();

    }

}