<?php

namespace Core\Module\Url;

class Url{

    /** @var string */
    public  static $url;

    /** @var string */
    public  static $host;

    /** @var string */
    public  static $query;

    /** @var string */
    public  static $shell_action;

    /** @var array */
    public  static $list=[];

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$url          =NULL;
        self::$host         =NULL;
        self::$query        =NULL;
        self::$list         =[];

        return true;

    }

    /**
     * @param string|NULL $link
     * @return string
     */
    public  static function get_url_without_query(string $link=NULL){

        return strtok($link,'?');

    }

    /**
     * @param string|NULL $link
     * @return array
     */
    public  static function get_url_list(string $link=NULL){

        if(empty($link))
            return[];

        $link=self::get_url_without_query($link);

        $list       =mb_split('\/',$link);
        $r_list     =[];

        foreach($list as $row)
            if(!empty($row))
                $r_list[]=strtolower(trim($row));

        return $r_list;

    }

}