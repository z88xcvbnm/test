<?php

namespace Core\Module\Url;

use Core\Module\Exception\ParametersException;

class UrlValidation{

    /**
     * @param string|NULL $link
     * @return int
     * @throws ParametersException
     */
    public  static function is_valid_url(string $link=NULL){

        if(empty($link)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'URL is empty'
            );

            throw new ParametersException($error);

        }

        return preg_match('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i',$link);

    }

}