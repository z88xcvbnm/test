<?php

namespace Core\Module\Url;

use Core\Module\Exception\ParametersException;
use Core\Module\Worktime\Worktime;

class UrlDetermine{

    /**
     * Set url list
     */
    private static function set_list(){

        Url::$list=Url::get_url_list(Url::$url);

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_host(){

        if(empty($_SERVER['HTTP_HOST'])){

            $error=array(
                'title'     =>'URL parameter problem',
                'info'      =>'HTTP_HOST is empty',
                'data'      =>$_SERVER
            );

            throw new ParametersException($error);

        }

        Url::$host=$_SERVER['HTTP_HOST'];

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_url(){

        if(empty($_SERVER['HTTP_HOST'])){

            $error=array(
                'title'     =>'URL parameter problem',
                'info'      =>'HTTP_HOST is empty',
                'data'      =>$_SERVER
            );

            throw new ParametersException($error);

        }

        Url::$url=str_replace('?'.$_SERVER['QUERY_STRING'],'',$_SERVER['REQUEST_URI']);

    }

    /**
     * Set get query string
     */
    private static function set_query(){

        Url::$query=$_SERVER['QUERY_STRING'];

    }

    /**
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_host();
        self::set_query();
        self::set_url();
        self::set_list();

    }

    /**
     * Determine url address
     */
    public  static function init(){

        Worktime::set_timestamp_point('URL Determine Start');

        self::set();

        Worktime::set_timestamp_point('URL Determine Finish');

    }

}