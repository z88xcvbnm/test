<?php

namespace Core\Module\Url;

use Core\Module\Exception\ParametersException;

class UrlShellDetermine{

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_action(){

        if(!isset($_POST['action'])){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Shell action is not exists'
            ];

            throw new ParametersException($error);

        }

        Url::$shell_action=$_POST['action'];

        return true;

    }

    /**
     * @return bool
     */
    private static function set_query(){

        global $argv;

        $_POST=[];

        if(count($argv)==0)
            return false;

        $list=[];

        for($i=1;$i<count($argv);$i++){

            $r=mb_split("=",$argv[$i]);

            if(count($r)==2)
                $list[$r[0]]=$r[1];

        }

        $_POST=$list;

        return true;

    }

    /**
     * @return bool
     */
    private static function set_host(){

        Url::$host=gethostname();

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_host();
        self::set_query();
        self::set_action();

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        return self::set();

    }

}