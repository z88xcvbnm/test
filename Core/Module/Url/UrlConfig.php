<?php

namespace Core\Module\Url;

class UrlConfig{

    /**
     * @var array
     */
    public  static $url_reserve_name_list=array(
        'settings',
        'api',
        'json',
        'xml'
    );

}