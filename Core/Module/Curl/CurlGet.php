<?php

namespace Core\Module\Curl;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Tor\Tor;

class CurlGet{

    /** @var string */
    private static $url;

    /** @var array */
    private static $get;

    /** @var array */
    private static $post;

    /** @var string */
    private static $data;

    /** @var int */
    private static $status;

    /** @var string */
    public  static $header          ='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36';

    /** @var array */
    private static $header_list     =[];

    /** @var string */
    private static $cookie          ='';

    /** @var string */
    private static $url_referer;

    /** @var string */
    private static $cookie_path     ='Temp/Parsing/Lovesss/reverse_ip.txt';

    /** @var array */
    private static $cookie_list     =[];

    /** @var bool */
    private static $need_tor        =false;

    /** @var string */
    private static $proxy;

    /** @var int */
    private static $proxy_port;

    /** @var bool */
    private static $need_simple_redirect;

    /** @var int */
    private static $ssl_version;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$url                      =NULL;
        self::$get                      =NULL;
        self::$post                     =[];
        self::$data                     =NULL;
        self::$status                   =NULL;
        self::$need_tor                 =false;
        self::$url_referer              =NULL;
        self::$proxy                    =NULL;
        self::$proxy_port               =NULL;
        self::$cookie                   ='';
        self::$cookie_list              =[];
        self::$need_simple_redirect     =false;
        self::$header_list              =[];
        self::$ssl_version              =NULL;
//        self::$repeat_index     =0;

        return true;

    }

    /**
     * @return array|null
     */
    public  static function get_cookie(){

        if(empty(self::$data))
            return NULL;

        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi',self::$data, $matches);

        $cookies=[];

        foreach($matches[1] as $item){

            parse_str($item,$cookie);

            $cookies=array_merge($cookies,$cookie);

        }

        return $cookies;

    }

    /**
     * @param $ch
     * @param null $maxredirect
     * @return array|bool
     */
    private static function curl_exec_follow($ch, &$maxredirect = null) {

          $user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5)".
                        " Gecko/20041107 Firefox/1.0";

          curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
//          curl_setopt($ch, CURLINFO_HEADER_OUT, true);

          if(!empty(self::$cookie_path))
              if(!file_exists(self::$cookie_path))
                  file_put_contents(self::$cookie_path,'');

          if(count(self::$header_list)>0)
            curl_setopt($ch,CURLOPT_HTTPHEADER,self::$header_list);

          if(!empty(self::$url_referer))
            curl_setopt($ch,CURLOPT_REFERER,self::$url_referer);

          if(!empty(self::$proxy))
            curl_setopt($ch, CURLOPT_PROXY, self::$proxy);

          if(!empty(self::$ssl_version)){

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSLVERSION , self::$ssl_version); //NEW ADDITION

          }

          if(!empty(self::$proxy_port))
                curl_setopt($ch, CURLOPT_PROXYPORT, self::$proxy_port);

//            curl_setopt($ch, CURLOPT_PROXYTYPE, 'HTTP');
//            curl_setopt($ch, CURLOPT_PROXYUSERPWD, 'vpn:vpn');

//                curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, 0);
//                curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 0);
//                curl_setopt($ch,CURLOPT_PROXY, self::$proxy);
            // curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);


//          curl_setopt($ch, CURLOPT_HEADER, true);

          if(count(self::$cookie_list)==0){

                curl_setopt($ch,CURLOPT_COOKIESESSION,true);
                curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true );
                curl_setopt($ch,CURLOPT_COOKIEJAR,realpath(self::$cookie_path));
                curl_setopt($ch,CURLOPT_COOKIEFILE,realpath(self::$cookie_path));

          }
          else{

            $cookie_list=[];

            foreach(self::$cookie_list as $row)
                $cookie_list[]=$row['name'].'='.$row['value'];

            curl_setopt($ch, CURLOPT_COOKIESESSION,true);
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION,true);
            curl_setopt($ch,CURLOPT_COOKIEJAR,realpath(self::$cookie_path));
            curl_setopt($ch,CURLOPT_COOKIEFILE,realpath(self::$cookie_path));
            curl_setopt( $ch, CURLOPT_COOKIE,implode(';',$cookie_list));

          }

            //Post Activate
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, self::$post); //Наши данные POST
            //        curl_setopt($ch, CURLOPT_POSTFIELDS,); //Наши данные POST

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));

            //<-- для работы с https
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);

            $mr = $maxredirect === null ? 5 : intval($maxredirect);

          if (ini_get('open_basedir') == '' && ini_get('safe_mode') == 'Off') {

//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $mr > 0);
            curl_setopt($ch, CURLOPT_MAXREDIRS, $mr);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

          }
          else {

//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);

            if ($mr > 0)
            {
              $original_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
              $newurl = $original_url;

              $rch = curl_copy_handle($ch);

//              curl_setopt($rch, CURLOPT_HEADER, true);
              curl_setopt($rch, CURLOPT_NOBODY, true);
              curl_setopt($rch, CURLOPT_FORBID_REUSE, false);

              if(count(self::$header_list)>0)
                curl_setopt($rch,CURLOPT_HTTPHEADER,self::$header_list);

              do
              {
                curl_setopt($rch, CURLOPT_URL, $newurl);
                $header = curl_exec($rch);

                if (curl_errno($rch)) {
                  $code = 0;
                } else {

                  $code = curl_getinfo($rch, CURLINFO_HTTP_CODE);

                  if ($code == 301 || $code == 302|| $code == 100) {

                    preg_match('/Location:(.*?)\n/', $header, $matches);
                    $newurl = trim(array_pop($matches));

                    // if no scheme is present then the new url is a
                    // relative path and thus needs some extra care
                    if(!preg_match("/^https?:/i", $newurl)){
                      $newurl = $original_url . $newurl;
                    }

//                    if(\Config::$is_debug){
//
//                        echo 'Isset redirect: '.$code.' '.$newurl."\n->>>>>>>>>>>>>>>>>>>>>>>\n\n";
//
//                    }


                  } else {
                    $code = 0;
                  }
                }
              } while ($code && --$mr);

              self::$status=curl_getinfo($rch, CURLINFO_HTTP_CODE);

              curl_close($rch);

              if (!$mr)
              {
                if ($maxredirect === null)
                trigger_error('Too many redirects.', E_USER_WARNING);
                else
                $maxredirect = 0;

                  curl_close($ch);

                return false;
              }
              
              curl_setopt($ch, CURLOPT_URL, $newurl);
            }
          }

          self::$data=curl_exec($ch);

          curl_close($ch);

          return true;

        }

    /**
     * @return bool
     */
    private static function set_data(){

        $curl=curl_init();

        $url=self::$url.'?'.http_build_query(self::$get);

        curl_setopt($curl,CURLOPT_URL,$url);

        if(self::$need_tor){

            Tor::change_proxy_identity();

            self::$proxy        ='socks5://'.Tor::$ip_address;
            self::$proxy_port   =Tor::$port;

        }

        self::curl_exec_follow($curl);

        return true;

    }

    /**
     * @return array
     */
    private static function set(){

        self::set_data();

//        self::$data=utf8_decode(self::$data);

        return[
            'status'        =>self::$status,
            'cookie'        =>self::get_cookie(),
            'data'          =>self::$data
        ];

    }

    /**
     * @param string|NULL $url
     * @param array $get
     * @param array|NULL $header
     * @param bool $need_tor
     * @param array $post
     * @param array $cookie_list
     * @param string|NULL $cookie_path
     * @param string|NULL $url_referer
     * @param string|NULL $proxy
     * @param int|NULL $proxy_port
     * @param array $header_list
     * @param int|NULL $ssl_version
     * @return array
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $url=NULL,array $get=[],array $header=NULL,bool $need_tor=false,array $post=[],array $cookie_list=[],string $cookie_path=NULL,string $url_referer=NULL,string $proxy=NULL,int $proxy_port=NULL,array $header_list=[],int $ssl_version=NULL){

        $error_info_list=[];

        if(empty($url))
            $error_info_list[]='URL is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$url              =$url;
        self::$get              =$get;
        self::$post             =$post;
        self::$cookie_list      =$cookie_list;
        self::$cookie_path      =$cookie_path;
        self::$need_tor         =$need_tor;
        self::$proxy            =$proxy;
        self::$proxy_port       =$proxy_port;
        self::$header_list      =$header_list;

        if(!empty(self::$ssl_version))
            self::$ssl_version=$ssl_version;

        if(!empty($header))
            self::$header=$header;

        return self::set();

    }

}