<?php

namespace Core\Module\Curl;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;
use Core\Module\Tor\Tor;

class CurlPostParsing{

    /** @var string */
    private static $url;

    /** @var string */
    private static $get;

    /** @var array */
    private static $post;

    /** @var string */
    private static $data;

    /** @var bool */
    private static $is_multipart;

    /** @var int */
    private static $status;

    /** @var string */
    private static $cookie_path;

    /** @var bool */
    private static $need_tor        =false;

    /** @var string */
    private static $proxy;

    /** @var int */
    private static $proxy_port;

    /** @var array */
    private static $header_list;

    /** @var bool */
    private static $isset_redirect=false;

    /** @var string */
    private static $url_redirect;

    /** @var string */
    private static $url_referer;

    /** @var array */
    private static $header_info=[];

    /** @var array */
    private static $cookie_list=[];

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$url                  =NULL;
        self::$get                  =NULL;
        self::$post                 =NULL;
        self::$data                 =NULL;
        self::$status               =NULL;
        self::$cookie_path          =NULL;
        self::$need_tor             =false;
        self::$header_list          =NULL;
        self::$isset_redirect       =false;
        self::$url_redirect         =NULL;
        self::$url_referer          =NULL;
        self::$cookie_list          =[];
//        self::$header_info          =[];

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_data(){

        $curl=curl_init();

        if($curl===false){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'CURL was not start'
            );

            throw new PhpException($error);

        }

//        $user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041107 Firefox/1.0";
        $user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36";

        curl_setopt($curl,CURLOPT_USERAGENT, $user_agent);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_VERBOSE, true);

        if(self::$is_multipart)
            curl_setopt($curl,CURLOPT_HTTPHEADER,'Content-Type: multipart/form-data');

//        if(!empty(self::$cookie_path))
//            if(!file_exists(self::$cookie_path))
//                file_put_contents(self::$cookie_path,'');

        if(!empty(self::$proxy))
            curl_setopt($curl,CURLOPT_PROXY, self::$proxy);

        if(!empty(self::$proxy_port))
            curl_setopt($curl, CURLOPT_PROXYPORT, self::$proxy_port);

        if(!empty(self::$url_referer))
            curl_setopt($curl, CURLOPT_REFERER, self::$url_referer);

          if(!empty(self::$header_list))
            curl_setopt($curl,CURLOPT_HTTPHEADER,self::$header_list);
          else
            curl_setopt($curl, CURLOPT_HTTPHEADER,[
                'Accept-Language: en,ru;q=0.9'
            ]);

//        curl_setopt($curl, CURLOPT_HEADER,true);
////        curl_setopt($curl, CURLOPT_VERBOSE, false);
//        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_COOKIESESSION,true);
        curl_setopt($curl, CURLOPT_COOKIEJAR,realpath(self::$cookie_path));
        curl_setopt($curl, CURLOPT_COOKIEFILE,realpath(self::$cookie_path));

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        if(!empty(self::$cookie_list))
            curl_setopt($curl, CURLOPT_COOKIE,implode('; ',self::$cookie_list));

//
//        curl_setopt($curl,CURLOPT_URL,self::$url.(empty(self::$get)?'':('?'.http_build_query(self::$get))));
//        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
//        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, true);

        curl_setopt($curl,CURLOPT_URL,self::$url.(empty(self::$get)?'':('?'.http_build_query(self::$get))));
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        if(!empty(self::$post)){

            curl_setopt($curl,CURLOPT_POST,true);
            curl_setopt($curl,CURLOPT_POSTFIELDS,http_build_query(self::$post));

        }

        self::$data             =curl_exec($curl);
        self::$status           =curl_getinfo($curl,CURLINFO_HTTP_CODE);
        self::$header_info[]    =self::$url;
        self::$header_info[]    =curl_getinfo($curl);

//        if (self::$status == 301 || self::$status == 302) {
//
//            preg_match('/Location:(.*?)\n/', self::$data, $matches);
//
//            $newurl=trim(array_pop($matches));
//
//            curl_close($curl);
//
//            self::$isset_redirect   =true;
//            self::$url_redirect     =$newurl;
//
////            echo 'New URL: '.$newurl."\n";
//
//            self::$url=$newurl;
//
//            return self::set_data();
//
//        }

        if(self::$data===false){

            curl_close($curl);

            sleep(1);

            return self::set_data();

        }

        curl_close($curl);

        return true;

    }

    /**
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::$need_tor){

            Tor::change_proxy_identity();

            self::$proxy        ='socks5://'.Tor::$ip_address;
            self::$proxy_port   =Tor::$port;

        }

        self::set_data();

        return[
            'status'            =>self::$status,
            'isset_redirect'    =>self::$isset_redirect,
            'url_redirect'      =>self::$url_redirect,
            'header_info'       =>self::$header_info,
            'data'              =>self::$data
        ];

    }

    /**
     * @param string|NULL $url
     * @param array $get
     * @param array $post
     * @param bool $is_multipart
     * @param string|NULL $cookie_path
     * @param bool $need_tor
     * @param string|NULL $proxy
     * @param int|NULL $proxy_port
     * @param string|NULL $url_referer
     * @param array|NULL $header_list
     * @param array $cookie_list
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $url=NULL,array $get=[],array $post=[],bool $is_multipart=false,string $cookie_path=NULL,bool $need_tor=false,string $proxy=NULL,int $proxy_port=NULL,string $url_referer=NULL,array $header_list=NULL,array $cookie_list=[]){

        $error_info_list=[];

        if(empty($url))
            $error_info_list[]='URL is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$url                      =$url;
        self::$get                      =empty($get)?NULL:$get;
        self::$post                     =$post;
        self::$need_tor                 =$need_tor;
        self::$proxy                    =$proxy;
        self::$proxy_port               =$proxy_port;
        self::$is_multipart             =$is_multipart;
        self::$cookie_path              =$cookie_path;
        self::$header_list              =$header_list;
        self::$cookie_list              =$cookie_list;

        return self::set();

    }

}