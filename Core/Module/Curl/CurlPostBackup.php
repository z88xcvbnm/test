<?php

namespace Core\Module\Curl;

use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PhpException;

class CurlPostBackup{

    /** @var string */
    private static $url;

    /** @var string */
    private static $get;

    /** @var array */
    private static $post;

    /** @var string */
    private static $data;

    /** @var bool */
    private static $is_multipart;

    /** @var int */
    private static $status;

    /**
     * @return bool
     */
    private static function reset_data(){

        self::$url          =NULL;
        self::$get          =NULL;
        self::$post         =NULL;
        self::$data         =NULL;
        self::$status       =NULL;

        return true;

    }

    /**
     * @return bool
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_data(){

        $curl=curl_init();

        if($curl===false){

            $error=array(
                'title'     =>'System problem',
                'info'      =>'CURL was not start'
            );

            throw new PhpException($error);

        }

        if(self::$is_multipart)
            curl_setopt($curl,CURLOPT_HTTPHEADER ,'Content-Type:multipart/form-data');

        curl_setopt($curl,CURLOPT_URL,self::$url.(empty(self::$get)?'':('?'.http_build_query(self::$get))));
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_POST,1);
        curl_setopt($curl,CURLOPT_POSTFIELDS,http_build_query(self::$post));
        curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        self::$data         =curl_exec($curl);
        self::$status       =curl_getinfo($curl,CURLINFO_HTTP_CODE);

        if(self::$data===false){

//            $error=array(
//                'title'     =>'System problem',
//                'info'      =>'Curl error: '.curl_error($curl)
//            );
//
//            throw new PhpException($error);

            curl_reset($curl);
            curl_close($curl);

            sleep(1);

            return self::set_data();

        }

        curl_reset($curl);
        curl_close($curl);

        return true;

    }

    /**
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_data();

        return[
            'status'        =>self::$status,
            'data'          =>self::$data
        ];

    }

    /**
     * @param string|NULL $url
     * @param array $get
     * @param array $post
     * @param bool $is_multipart
     * @return array
     * @throws ParametersException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(string $url=NULL,array $get=[],array $post=[],bool $is_multipart=false){

        $error_info_list=[];

        if(empty($url))
            $error_info_list[]='URL is empty';

        if(count($post)==0)
            $error_info_list[]='POST is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::reset_data();

        self::$url              =$url;
        self::$get              =empty($get)?NULL:$get;
        self::$post             =$post;
        self::$is_multipart     =$is_multipart;

        return self::set();

    }

}