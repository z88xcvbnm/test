<?php

namespace Core\Module\Feedback;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\User\User;

class Feedback{

    /** @var string */
    public  static $table_name='_feedback';

    /**
     * @param string|NULL $email
     * @param string|NULL $content
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_feedback(string $email=NULL,string $content=NULL){

        $error_info_list=[];

        if(empty($email))
            $error_info_list[]='Email is empty';

        if(empty($content))
            $error_info_list[]='Content is empty';

        if(count($error_info_list)>0){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>$error_info_list
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'values'    =>[
                'user_id'       =>User::$user_ID,
                'email'         =>$email,
                'content'       =>$content,
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()',
                'type'          =>0
            ]
        ];

        $r=Db::insert($q);

        if(count($r)==0){

            $error=[
                'title'     =>DbQueryException::$title,
                'info'      =>'FeedbackApi was not add'
            ];

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

}