<?php

namespace Core\Module\Device;

use Core\Module\Db\Db;
use Core\Module\Exception\ParametersException;
use Core\Module\Worktime\Worktime;

class Device{

    /**
     * @return array
     */
    public  static function get_full_info(){

        return array(
            'firm_ID'       =>DeviceFirm::$device_firm_ID,
            'model_ID'      =>DeviceModel::$device_model_ID,
            'firm_name'     =>DeviceFirm::$device_firm_name,
            'model_name'    =>DeviceModel::$device_model_name,
            'device_type'   =>DeviceType::$device_type,
            'is_mobile'     =>DeviceType::$is_mobile,
            'is_tablet'     =>DeviceType::$is_tablet
        );

    }

    /**
     * @param int|NULL $device_model_ID
     * @return int|null
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_firm_ID_from_model_ID(int $device_model_ID=NULL){

        if(empty($device_model_ID)){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>'Device model ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'device_firm_id'
            ),
            'table'=>'_device_model',
            'where'=>array(
                'id'=>$device_model_ID,
                'type'=>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return empty($r[0]['device_firm_id'])?NULL:$r[0]['device_firm_id'];

    }

    /**
     * @return bool
     */
    public  static function init(){

        Worktime::set_timestamp_point('Device Determine Start');

        self::set();

        Worktime::set_timestamp_point('Device Determine Finish');

        return true;

    }

}