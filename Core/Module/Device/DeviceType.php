<?php

namespace Core\Module\Device;

class DeviceType{

    /** @var string */
    public static $device_type;

    /** $var bool */
    public static $is_mobile        =false;

    /** $var bool */
    public static $is_tablet        =false;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$device_type      =NULL;
        self::$is_mobile        =false;
        self::$is_tablet        =false;

        return true;

    }

    /**
     * @return mixed
     */
    public  static function is_tablet(){

        return self::$is_mobile;

    }

    /**
     * @return bool
     */
    public  static function is_mobile(){

        return self::$is_mobile;

    }

    /**
     * @param string|NULL $device_type
     * @return bool
     */
    public  static function set_device_type(string $device_type=NULL){

        self::$device_type=empty($device_type)?NULL:strtolower($device_type);

        return true;

    }

    /**
     * @param bool|NULL $is_mobile
     * @return bool
     */
    public  static function set_is_mobile(bool $is_mobile=NULL){

        self::$is_mobile=empty($is_mobile)?NULL:$is_mobile;

        return true;

    }

    /**
     * @param bool|NULL $is_tablet
     * @return bool
     */
    public  static function set_is_tablet(bool $is_tablet=NULL){

        self::$is_tablet=empty($is_tablet)?NULL:$is_tablet;

        return true;

    }


}