<?php

namespace Core\Module\Device;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class DeviceFirm{

    /** @var int */
    public  static $device_firm_ID;

    /** @var string */
    public  static $device_firm_name;

    /**
     * Reset default data
     * @return bool
     */
    public  static function reset_data(){

        self::$device_firm_ID       =NULL;
        self::$device_firm_name     =NULL;

        return true;

    }

    /**
     * @param string|NULL $device_firm_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_device_firm(string $device_firm_name=NULL){

        if(empty($device_firm_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device firm name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name_lower'=>strtolower($device_firm_name)
        );

        return Db::isset_row('_device_firm',0,$where_list);

    }

    /**
     * @param int|NULL $device_firm_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_device_firm_ID(int $device_firm_ID=NULL){

        if(empty($device_firm_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device firm ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($device_firm_ID,'_device_firm',0);

    }

    /**
     * @param string|NULL $device_firm_name
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_firm_ID(string $device_firm_name=NULL){

        if(empty($device_firm_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device firm name is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_device_firm',
            'where'=>array(
                'name_lower'    =>strtolower(trim($device_firm_name)),
                'type'          =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $device_firm_ID
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_firm_name(int $device_firm_ID=NULL){

        if(empty($device_firm_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device firm ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_device_firm',
            'where'=>array(
                'id'        =>$device_firm_ID,
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param string|NULL $device_firm_name
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_device_firm_name(string $device_firm_name=NULL){

        if(empty(self::$device_firm_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Default device firm name is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_device_firm',
            'values'    =>array(
                'name'          =>$device_firm_name,
                'name_lower'    =>strtolower($device_firm_name),
                'date_create'   =>'NOW()',
                'date_update'   =>'NOW()'
            )
        );

        $conflict_list=[
            'column_list'=>[
                'name_lower'
            ],
            'return_list'=>[
                'id'
            ]
        ];

        $r=Db::insert($q,true,[],NULL,$conflict_list);

        if(count($r)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device firm name was not added'
            );

            throw new ParametersException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $device_firm_ID
     * @param bool $need_remove_device_model
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_device_firm_ID(int $device_firm_ID=NULL,bool $need_remove_device_model=true){

        if(empty($device_firm_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device firm ID is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_device_model)
            if(!DeviceModel::remove_device_model($device_firm_ID)){

                $error=array(
                    'title'     =>'DB query problem',
                    'info'      =>'Device model was not removed'
                );

                throw new DbQueryException($error);

            }

        if(!Db::delete_from_ID($device_firm_ID,'_device_firm')){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device firm ID was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $device_firm_name
     * @param bool $need_remove_device_model
     * @return array|bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_device_firm_name(string $device_firm_name=NULL,bool $need_remove_device_model=true){

        if(empty($device_firm_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device firm name is empty'
            );

            throw new ParametersException($error);

        }

        if($need_remove_device_model){

            $device_firm_ID=self::get_device_firm_ID($device_firm_name);

            if(!empty($device_firm_ID))
                if(!DeviceModel::remove_device_model($device_firm_ID)){

                    $error=array(
                        'title'     =>'DB query problem',
                        'info'      =>'Device model was not removed'
                    );

                    throw new DbQueryException($error);

                }

        }

        $where_list=array(
            'name_lower'=>strtolower($device_firm_name)
        );

        return Db::delete_from_where_list('_device_name',0,$where_list);

    }

    /**
     * @param int|NULL $device_firm_ID
     * @return bool
     */
    public  static function set_device_firm_ID_default(int $device_firm_ID=NULL){

        self::$device_firm_ID=empty($device_firm_ID)?NULL:$device_firm_ID;

        return true;

    }

    /**
     * @param string|NULL $device_firm_name
     * @return bool
     */
    public  static function set_device_firm_name_default(string $device_firm_name=NULL){

        self::$device_firm_name=empty($device_firm_name)?NULL:$device_firm_name;

        return true;

    }

}