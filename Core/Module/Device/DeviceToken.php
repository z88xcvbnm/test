<?php

namespace Core\Module\Device;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class DeviceToken{

    /** @var int */
    public  static $device_token_ID;

    /** @var string */
    public  static $device_hash;

    /** @var string */
    public  static $device_token;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$device_token_ID      =NULL;
        self::$device_hash          =NULL;
        self::$device_token         =NULL;

        return true;

    }

    /**
     * @param int|NULL $device_token_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_device_token_ID(int $device_token_ID=NULL){

        if(empty($device_token_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device token ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($device_token_ID,'_device_token',0);

    }

    /**
     * @param string|NULL $device_hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_device_hash(string $device_hash=NULL){

        if(empty($device_hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device hash is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'hash'=>$device_hash
        );

        return Db::isset_row('_device_token',0,$where_list);

    }

    /**
     * @param string|NULL $device_token
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_device_token(string $device_token=NULL){

        if(empty($device_token)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device token is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'token'=>$device_token
        );

        return Db::isset_row('_device_token',0,$where_list);

    }

    /**
     * @param string|NULL $device_hash
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_token_ID_from_device_hash(string $device_hash=NULL){

        if(empty($device_hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device hash is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'hash'=>$device_hash
        );

        $device_token_ID=Db::get_row_ID('_device_token',0,$where_list);

        if(empty($device_token_ID))
            return NULL;

        return $device_token_ID;

    }

    /**
     * @param string|NULL $device_hash
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_token_data_from_hash(string $device_hash=NULL){

        if(empty($device_hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device hash is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'device_firm_id',
                'device_model_id',
                'os_id',
                'os_version_id',
                'hash',
                'token'
            ),
            'table'=>'_device_token',
            'where'=>array(
                'hash'      =>$device_hash,
                'type'      =>0
            ),
            'type'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return array(
            'ID'                =>$r[0]['id'],
            'device_firm_ID'    =>$r[0]['device_firm_id'],
            'device_model_ID'   =>$r[0]['device_model_id'],
            'os_ID'             =>$r[0]['os_id'],
            'os_version_ID'     =>$r[0]['os_version_id'],
            'hash'              =>$r[0]['hash'],
            'token'             =>$r[0]['token']
        );

    }

    /**
     * @param string|NULL $device_hash
     * @param string|NULL $device_token
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_token_ID_from_device_hash_and_token(string $device_hash=NULL,string $device_token=NULL){

        $error_info_list=[];

        if(empty($device_hash))
            $error_info_list[]='Device hash is empty';

        if(empty($device_token))
            $error_info_list[]='Device token is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'device_hash'   =>$device_hash,
                    'device_token'  =>$device_token
                )
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'hash'      =>$device_hash,
            'token'     =>$device_token
        );

        $device_token_ID=Db::get_row_ID('_device_token',0,$where_list);

        if(empty($device_token_ID))
            return NULL;

        return $device_token_ID;

    }

    /**
     * @param string|NULL $device_token
     * @return null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_token_ID_from_device_token(string $device_token=NULL){

        if(empty($device_token)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device token is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'token'=>$device_token
        );

        $device_token_ID=Db::get_row_ID('_device_token',0,$where_list);

        if(empty($device_token_ID))
            return NULL;

        return $device_token_ID;

    }

    /**
     * @param int|NULL $device_firm_ID
     * @param int|NULL $device_model_ID
     * @param int|NULL $os_ID
     * @param int|NULL $os_version_ID
     * @param string|NULL $device_hash
     * @param string|NULL $device_token
     * @param bool $is_google
     * @param bool $is_apple
     * @return int
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_device_token(int $device_firm_ID=NULL,int $device_model_ID=NULL,int $os_ID=NULL,int $os_version_ID=NULL,string $device_hash=NULL,string $device_token=NULL,bool $is_google=false,bool $is_apple=false){

        $error_info_list=[];

        if(empty($device_firm_ID))
            $error_info_list[]='Device firm ID is empty';

        if(empty($device_model_ID))
            $error_info_list[]='Device model ID is empty';

        if(empty($os_ID))
            $error_info_list[]='OS ID is empty';

        if(empty($os_version_ID))
            $error_info_list[]='OS version ID is empty';

        if(empty($device_hash))
            $error_info_list[]='Device hash is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $value_list=array(
            'device_firm_id'    =>$device_firm_ID,
            'device_model_id'   =>$device_model_ID,
            'os_id'             =>$os_ID,
            'os_version_id'     =>$os_version_ID,
            'hash'              =>$device_hash,
            'google'            =>(int)$is_google,
            'apple'             =>(int)$is_apple,
            'date_create'       =>'NOW()',
            'date_update'       =>'NOW()'
        );

        if(!empty($device_token))
            $value_list['token']=$device_token;

        $q=array(
            'table'=>'_device_token',
            'values'=>array(
                $value_list
            )
        );

        $r=Db::insert($q);

        if(count($r)==0){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'Device token was not added',
                'data'  =>array(
                    'device_firm_ID'        =>$device_firm_ID,
                    'device_model_ID'       =>$device_model_ID,
                    'os_ID'                 =>$os_ID,
                    'os_version_ID'         =>$os_version_ID,
                    'hash'                  =>$device_hash,
                    'token'                 =>$device_token,
                    'query'                 =>$q
                )
            );

            throw new DbQueryException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $device_token_ID
     * @param int|NULL $device_firm_ID
     * @param int|NULL $device_model_ID
     * @param int|NULL $os_ID
     * @param int|NULL $os_version_ID
     * @param string|NULL $device_hash
     * @param string|NULL $device_token
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_device_token(int $device_token_ID=NULL,int $device_firm_ID=NULL,int $device_model_ID=NULL,int $os_ID=NULL,int $os_version_ID=NULL,string $device_hash=NULL,string $device_token=NULL){

        if(empty($device_token_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device token ID is empty'
            );

            throw new ParametersException($error);
            
        }
        
        if(
              empty($device_firm_ID)
            &&empty($device_model_ID)
            &&empty($os_ID)
            &&empty($os_version_ID)
            &&empty($device_hash)
            &&empty($device_token)
        ){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'All parameters are empty'
            );

            throw new ParametersException($error);

        }

        $set_list=[];

        if(!empty($device_firm_ID))
            $set_list['device_firm_id']=$device_firm_ID;

        if(!empty($device_model_ID))
            $set_list['device_model_id']=$device_model_ID;

        if(!empty($os_ID))
            $set_list['os_id']=$os_ID;

        if(!empty($os_version_ID))
            $set_list['os_version_id']=$os_version_ID;

        if(!empty($device_hash))
            $set_list['hash']=$device_hash;
        
        if(!empty($device_token))
            $set_list['token']=$device_token;
        
        if(count($set_list)==0)
            return false;
        
        $set_list['date_update']='NOW()';
        
        if(!Db::update_data_from_ID($device_token_ID,'_device_token',$set_list,0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device token was not updated',
                'data'      =>array(
                    'set_list'          =>$set_list,
                    'device_token_ID'   =>$device_token_ID
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $device_token_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_device_token_ID(int $device_token_ID=NULL){

        if(empty($device_token_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device token ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($device_token_ID,'_device_token',0)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device token was not removed',
                'data'      =>array(
                    'device_token_ID'=>$device_token_ID
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $device_token
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_device_token_from_device_token(string $device_token=NULL){

        if(empty($device_token)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device token is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'token'=>$device_token
        );

        if(!Db::delete_from_where_list('_device_token',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device token was not removed',
                'data'      =>array(
                    'device_token'=>$device_token
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $device_hash
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_device_token_from_device_hash(string $device_hash=NULL){

        if(empty($device_hash)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device hash is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'hash'=>$device_hash
        );

        if(!Db::delete_from_where_list('_device_token',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device token was not removed',
                'data'      =>array(
                    'device_hash'=>$device_hash
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param string|NULL $device_hash
     * @param string|NULL $device_token
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_device_token_from_device_hash_and_device_token(string $device_hash=NULL,string $device_token=NULL){

        $error_info_list=[];

        if(empty($device_hash))
            $error_info_list[]='Device hash is empty';

        if(empty($device_token))
            $error_info_list[]='Device token is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list,
                'data'      =>array(
                    'device_hash'   =>$device_hash,
                    'device_token'  =>$device_token
                )
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'hash'      =>$device_hash,
            'token'     =>$device_token
        );

        if(!Db::delete_from_where_list('_device_token',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device token was not removed',
                'data'      =>array(
                    'device_hash'   =>$device_hash,
                    'device_token'  =>$device_token
                )
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $device_token_ID
     * @return bool
     */
    public  static function set_device_token_ID_default(int $device_token_ID=NULL){

        self::$device_token_ID=empty($device_token_ID)?NULL:$device_token_ID;

        return true;

    }

    /**
     * @param string|NULL $device_token
     * @return bool
     */
    public  static function set_device_token_default(string $device_token=NULL){

        self::$device_token=empty($device_token)?NULL:$device_token;

        return true;

    }

    /**
     * @param string|NULL $device_hash
     * @return bool
     */
    public  static function set_device_hash_default(string $device_hash=NULL){

        self::$device_hash=empty($device_hash)?NULL:$device_hash;

        return true;

    }

}