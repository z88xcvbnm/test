<?php

namespace Core\Module\Device;

use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Os\Os;
use Core\Module\Os\OsVersion;
use Core\Module\Worktime\Worktime;

class DeviceDetermine{

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_device_firm_ID_to_defualt(){

        if(empty(DeviceFirm::$device_firm_name))
            return false;

        $device_firm_ID=DeviceFirm::get_device_firm_ID(DeviceFirm::$device_firm_name);

        if(empty($device_firm_ID))
            $device_firm_ID=DeviceFirm::add_device_firm_name(DeviceFirm::$device_firm_name);

        if(empty($device_firm_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device firm was not added'
            );

            throw new DbQueryException($error);

        }

        DeviceFirm::set_device_firm_ID_default($device_firm_ID);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_device_model_ID_to_default(){

        if(empty(DeviceModel::$device_model_name))
            return false;

        $device_model_ID=DeviceModel::get_device_model_ID(DeviceFirm::$device_firm_ID,DeviceModel::$device_model_name);

        if(empty($device_model_ID))
            $device_model_ID=DeviceModel::add_device_model(DeviceFirm::$device_firm_ID,DeviceModel::$device_model_name);

        if(empty($device_model_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device model was not added'
            );

            throw new DbQueryException($error);

        }

        DeviceModel::set_device_model_ID_default($device_model_ID);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set_client_device_token_to_default(){

        if(empty(DeviceToken::$device_hash)&&empty(DeviceToken::$device_token))
            return false;

        if(!empty(DeviceToken::$device_hash)&&!empty(DeviceToken::$device_token))
            $device_token_ID=DeviceToken::get_device_token_ID_from_device_hash_and_token(DeviceToken::$device_hash,DeviceToken::$device_token);
        else if(!empty(DeviceToken::$device_hash))
            $device_token_ID=DeviceToken::get_device_token_ID_from_device_hash(DeviceToken::$device_hash);
        else if(!empty(DeviceToken::$device_hash))
            $device_token_ID=DeviceToken::get_device_token_ID_from_device_token(DeviceToken::$device_token);
        else
            return false;

        $error_info_list=[];

        if(empty(DeviceFirm::$device_firm_ID))
            $error_info_list[]='Default device firm ID is empty';

        if(empty(DeviceModel::$device_model_ID))
            $error_info_list[]='Default device model ID is empty';

        if(empty(Os::$os_ID))
            $error_info_list[]='Default OS ID is empty';

        if(empty(OsVersion::$os_version_ID))
            $error_info_list[]='Default OS version ID is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        Os::set_device_type_default();

        if(empty($device_token_ID))
            $device_token_ID=DeviceToken::add_device_token(DeviceFirm::$device_firm_ID,DeviceModel::$device_model_ID,Os::$os_ID,OsVersion::$os_version_ID,DeviceToken::$device_hash,DeviceToken::$device_token,Os::$is_google,Os::$is_apple);

        if(empty($device_token_ID)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device token was not added'
            );

            throw new DbQueryException($error);

        }

        DeviceToken::set_device_token_ID_default($device_token_ID);

        return true;

    }

    /**
     * @return bool
     */
    private static function set_client_device_data_from_post_to_default(){

        if(!empty($_POST['device_firm_name']))
            DeviceFirm::set_device_firm_name_default($_POST['device_firm_name']);

        if(!empty($_POST['device_model_name']))
            DeviceModel::set_device_model_name_default($_POST['device_model_name']);

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        self::set_client_device_data_from_post_to_default();
        self::set_client_device_firm_ID_to_defualt();
        self::set_client_device_model_ID_to_default();

        return true;

    }

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(){

        Worktime::set_timestamp_point('Device Determine Start');

        self::set();

        Worktime::set_timestamp_point('Device Determine Finish');

        return true;

    }

}