<?php

namespace Core\Module\Device;

use Core\Module\Db\Db;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;

class DeviceModel{

    /** @var int */
    public  static $device_model_ID;

    /** @var string */
    public  static $device_model_name;

    /**
     * @return bool
     */
    public  static function reset_data(){

        self::$device_model_ID       =NULL;
        self::$device_model_name     =NULL;

        return true;

    }

    /**
     * @param string|NULL $device_model_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_device_model(string $device_model_name=NULL){

        if(empty($device_model_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device model name is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'name_lower'=>strtolower($device_model_name)
        );

        return Db::isset_row('_device_model',0,$where_list);

    }

    /**
     * @param int|NULL $device_model_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_device_model_ID(int $device_model_ID=NULL){

        if(empty($device_model_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device model ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($device_model_ID,'_device_model',0);

    }

    /**
     * @param int|NULL $device_firm_ID
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_model_list(int $device_firm_ID=NULL){

        if(empty($device_firm_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device firm ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'name'
            ),
            'table'=>'_device_model',
            'where'=>array(
                'device_firm_id'    =>$device_firm_ID,
                'type'              =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row)
            $list[]=array(
                'ID'    =>$row['id'],
                'name'  =>$row['name']
            );

        return $list;

    }

    /**
     * @param int|NULL $device_firm_ID
     * @param string|NULL $device_model_name
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_model_ID(int $device_firm_ID=NULL,string $device_model_name=NULL){

        $error_info_list=[];

        if(empty($device_firm_ID))
            $error_info_list[]='Device firm ID is empty';

        if(empty($device_model_name))
            $error_info_list[]='Device model name is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_device_model',
            'where'=>array(
                'device_firm_id'    =>$device_firm_ID,
                'name_lower'        =>strtolower(trim($device_model_name)),
                'type'              =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param string|NULL $device_model_name
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_model_ID_from_name(string $device_model_name=NULL){

        if(empty($device_model_name)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device model name is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id'
            ),
            'table'=>'_device_model',
            'where'=>array(
                'name_lower'        =>strtolower(trim($device_model_name)),
                'type'              =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $device_model_ID
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_model_name(int $device_model_ID=NULL){

        if(empty($device_model_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device model ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'name'
            ),
            'table'=>'_device_model',
            'where'=>array(
                'id'      =>strtolower($device_model_ID),
                'type'      =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['name'];

    }

    /**
     * @param int|NULL $device_model_ID
     * @return |null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_device_firm_ID(int $device_model_ID=NULL){

        if(empty($device_model_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device model ID is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'device_firm_id'
            ),
            'table'=>'_device_model',
            'where'=>array(
                'id'    =>strtolower($device_model_ID),
                'type'  =>0
            ),
            'limit'=>1
        );

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return $r[0]['device_firm_id'];

    }

    /**
     * @param int|NULL $device_firm_ID
     * @param string|NULL $device_model_name
     * @return mixed
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_device_model(int $device_firm_ID=NULL,string $device_model_name=NULL){

        $error_info_list=[];

        if(empty($device_firm_ID))
            $error_info_list[]='Device firm ID is empty';

        if(empty($device_model_name))
            $error_info_list[]='Device model name is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'     =>'_device_model',
            'values'    =>array(
                'device_firm_id'    =>$device_firm_ID,
                'name'              =>$device_model_name,
                'name_lower'        =>strtolower($device_model_name),
                'date_create'       =>'NOW()',
                'date_update'       =>'NOW()'
            )
        );

        $conflict_list=[
            'column_list'=>[
                'name_lower'
            ],
            'return_list'=>[
                'id'
            ]
        ];

        $r=Db::insert($q,true,[],NULL,$conflict_list);

        if(count($r)==0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device model name was not added'
            );

            throw new ParametersException($error);

        }

        return $r[0]['id'];

    }

    /**
     * @param int|NULL $device_model_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_device_model_ID(int $device_model_ID=NULL){

        if(empty($device_model_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device model ID is empty'
            );

            throw new ParametersException($error);

        }

        if(!Db::delete_from_ID($device_model_ID,'_device_model')){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device model ID was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $device_firm_ID
     * @param string|NULL $device_model_name
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_device_model(int $device_firm_ID=NULL,string $device_model_name=NULL){

        if(empty($device_firm_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Device firm ID is empty'
            );

            throw new ParametersException($error);

        }

        $where_list=array(
            'device_firm_id'=>$device_firm_ID
        );

        if(!empty($device_model_name))
            $where_list['name_lower']=strtolower($device_model_name);

        if(!Db::delete_from_where_list('_device_model',0,$where_list)){

            $error=array(
                'title'     =>'DB query problem',
                'info'      =>'Device model ID was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $device_model_ID
     * @return bool
     */
    public  static function set_device_model_ID_default(int $device_model_ID=NULL){

        self::$device_model_ID=empty($device_model_ID)?NULL:$device_model_ID;

        return true;

    }

    /**
     * @param string|NULL $device_model_name
     * @return bool
     */
    public  static function set_device_model_name_default(string $device_model_name=NULL){

        self::$device_model_name=empty($device_model_name)?NULL:$device_model_name;

        return true;

    }

}