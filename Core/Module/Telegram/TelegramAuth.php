<?php

namespace Core\Module\Telegram;

use Core\Module\MadelineProto\API;
use Core\Module\MadelineProto\Exception;

class TelegramAuth{

    private static function get_posts(){

        $MadelineProto = new API('session.madeline');

        $val=[
            'url'       =>'modelzonechannel'
        ];

        $settings = array(
                'peer' => '@'.$val['url'], //название_канала, должно начинаться с @, например @breakingmash, все остальные параметры, кроме limit, можно оставить равными 0
                'offset_id' => $val['offset_id']?:0,
                'offset_date' => $val['offset_date']?:0,
                'add_offset' => $val['add_offset']?:0,
                'limit' => $val['limit']?:10, //Количество постов, которые вернет клиент
                'max_id' => $val['max_id']?:0, //Максимальный id поста
                'min_id' => $val['min_id']?:0, //Минимальный id поста - использую для пагинации, при  0 возвращаются последние посты.
                'hash' => 0
         );

         $data = $MadelineProto->messages->getHistory($settings);

         print_r($data);

    }

    private static function auth(){

        $settings = [
            'authorization' => [
                'default_temp_auth_key_expires_in' => 315576000, // я установил 10 лет, что бы не авторизовывать приложение повторно.
            ],
            'app_info' => [ // Эти данные мы получили после регистрации приложения на https://my.telegram.org
                'api_id'          => TelegramConfig::$api_ID,
                'api_hash'        => TelegramConfig::$api_hash
            ],
            'logger' => [ // Вывод сообщений и ошибок
                'logger' => 3, // выводим сообещения через echo
                'logger_level' => 'FATAL ERROR', // выводим только критические ошибки.
            ],
            'max_tries' => [ // Количество попыток установить соединения на различных этапах работы. Лучше не уменьшать, так как телеграм не всегда отвечает с первого раза
                'query' => 5,
                'authorization' => 5,
                'response' => 5,
            ],
            'updates' => [ // Я обновляю данные прямыми запросами, поэтому обновления с каналов и чатов мне не требуются.
                'handle_updates' => false,
                'handle_old_updates' => false,
            ],
        ];

        $MadelineProto = new API($settings);

        $MadelineProto->phone_login(readline('Enter your phone number: ')); //вводим в консоли свой номер телефона
        $authorization = $MadelineProto->complete_phone_login(readline('Enter the code you received: ')); // вводим в консоли код авторизации, который придет в телеграм
        if ($authorization['_'] === 'account.noPassword') {
            throw new Exception('2FA is enabled but no password is set!');
        }
        if ($authorization['_'] === 'account.password') {
            $authorization = $MadelineProto->complete_2fa_login(readline('Please enter your password (hint '.$authorization['hint'].'): ')); //если включена двухфакторная авторизация, то вводим в консоли пароль.
        }
        if ($authorization['_'] === 'account.needSignup') {
            $authorization = $MadelineProto->complete_signup(readline('Please enter your first name: '), readline('Please enter your last name (can be empty): '));
        }
        $MadelineProto->session = 'session.madeline';
        $MadelineProto->serialize();


    }

    private static function set(){

        self::auth();
        self::get_posts();


    }

    public  static function init(){

        self::set();

    }

}