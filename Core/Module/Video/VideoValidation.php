<?php

namespace Core\Module\Video;

use Core\Module\Exception\ParametersException;
use Core\Module\Ffmpeg\Ffmpeg;

class VideoValidation{

    /**
     * @param int|NULL $width
     * @param int|NULL $height
     * @return mixed
     * @throws ParametersException
     */
    public  static function valid_video_size(int $width=NULL,int $height=NULL){

        if(empty($width)||empty($height)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'Video size is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::valid_video_size($width,$height);

    }

}