<?php

namespace Core\Module\Video;

class VideoConfig{

    /** @var array */
    public  static $video_codec_list    =array(
        'h264',
        'mpeg'
    );

    /** @var array */
    public  static $video_size_list     =array(
        '640x480',
        '1280x720',
        '1280x800',
        '1920x1080'
    );

    /** @var array */
    public  static $video_convert_type_list=array(
        'video'=>[]
    );

}