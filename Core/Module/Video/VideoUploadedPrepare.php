<?php

namespace Core\Module\Video;

use Core\Module\Encrypt\Hash;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Exception\PhpException;
use Core\Module\File\File;
use Core\Module\User\User;

class VideoUploadedPrepare{

    /** @var int */
    private static $file_ID;

    /** @var int */
    private static $video_ID;

    /** @var string */
    private static $file_content_type;

    /** @var string */
    private static $file_extension;

    /**
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function isset_convert_type(){

        if(!isset(VideoConfig::$video_convert_type_list[self::$file_content_type])){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'File type is not exists'
            );

            throw new ParametersException($error);

        }

        return true;

    }

    /**
     * @return array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function prepare_video(){

        $file_path          =File::get_file_path_from_file_ID(self::$file_ID);

        $file_mime_type     =File::get_file_mime_type_from_path($file_path);
        $mime_type_list     =mb_split('\/',$file_mime_type);
        $file_type          =end($mime_type_list);
        $file_size          =filesize($file_path);
        $file_extension     =File::get_file_extension_from_file_name($file_path);

        self::$video_ID     =Video::add_video(self::$file_ID,$file_type,$file_size,$file_mime_type,mb_strtolower($file_extension,'utf-8'),true,false,true,false,false,NULL);

        $video_path         =Video::get_file_path_from_video_ID(self::$video_ID,true);

        if(!copy($file_path,$video_path)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'File was not copy to video dir'
            ];

            throw new PhpException($error);

        }

        $hash_list=[
            User::$user_ID,
            self::$file_ID,
            self::$video_ID,
            time(),
            rand(0,time())
        ];
        $hash=Hash::get_sha1_encode(implode(':',$hash_list));

        if(!Video::update_video_prepared(self::$video_ID,$hash,NULL,NULL,$file_size,$file_mime_type,$file_extension,NULL)){

            $error=[
                'title'     =>PhpException::$title,
                'info'      =>'Video was not update to prepared'
            ];

            throw new PhpException($error);

        }

        $video_hash_path=Video::get_file_path_from_video_ID(self::$video_ID,true,true);

        return[
            'file_ID'           =>self::$file_ID,
            'video_ID'          =>self::$video_ID,
            'video_path'        =>$video_path,
            'video_hash_path'   =>$video_hash_path
        ];


//        ///
//
//        $video_data=Video::add_video_preparing(self::$file_ID,self::$file_extension,true);
//
//        if(empty($video_data)){
//
//            $error=array(
//                'title' =>'DB query problem',
//                'info'  =>'Image was not added'
//            );
//
//            throw new DbQueryException($error);
//
//        }
//
//        self::$video_ID             =$video_data['ID'];
//        self::$video_date_create    =$video_data['date_create'];
//
//        $file_result_path           =self::get_file_source_path();
//        $dir_date_video_result      =Dir::get_dir_from_date(DirConfig::$dir_video,self::$video_date_create);
//        $dir_video_result           =$dir_date_video_result.'/'.self::$video_ID;
//        $video_source_path          =$dir_video_result.'/source.'.self::$file_extension;
//
//        Dir::create_dir($dir_video_result);
//
//        if(!copy($file_result_path,$video_source_path)){
//
//            $error=array(
//                'title' =>'File problem',
//                'info'  =>'File was not copied'
//            );
//
//            throw new PathException($error);
//
//        }
//
//        foreach(VideoConfig::$video_bitrate_list as $bitrate)
//            foreach(VideoConfig::$video_hz_list as $hz){
//
//                    $video_item_ID                      =VideoItem::add_video_item_preparing(self::$file_ID,self::$video_ID,$bitrate,$hz);
//                    $file_video_result_path             =$dir_video_result.'/'.$video_item_ID.'.mp3';
//
//                    self::$video_item_ID_list[]         =$video_item_ID;
//
//                    if(!VideoConvert::convert($file_result_path,$file_video_result_path,'libmp3lame',$bitrate,$hz)){
//
//                        $error=array(
//                            'title'     =>'System problem',
//                            'info'      =>'Video was not converted'
//                        );
//
//                        throw new PhpException($error);
//
//                    }
//
//                    $video_hash         =HashFile::get_sha1_encode($file_video_result_path);
//                    $video_mime_type    =FileType::get_file_mime_type($file_video_result_path);
//                    $video_extension    =VideoConfig::$video_extension_default;
//                    $video_file_size    =filesize($file_video_result_path);
//                    $video_duration     =Video::get_video_duration_in_seconds_from_path($file_video_result_path);
//
//                    VideoItem::update_video_item_ID_prepared($video_item_ID,$video_hash,$video_duration,$bitrate,$hz,$video_file_size,$video_mime_type,$video_extension);
//
//                    if($bitrate==VideoConfig::$video_bitrate_default&&$hz==VideoConfig::$video_hz_default)
//                        Video::update_video_preparing(self::$video_ID,$video_hash,$video_duration,$bitrate,$hz,$video_file_size,$video_mime_type,$video_extension);
//
//                }
//
//        Video::update_video_ID_prepared(self::$video_ID);
//
//        return array(
//            'video_ID'              =>self::$video_ID,
//            'video_item_ID_list'    =>self::$video_item_ID_list,
//            'video_dir'             =>$dir_video_result
//        );

    }

    /**
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\SystemException
     */
    private static function set(){

        if(self::isset_convert_type())
            return self::prepare_video();

        return NULL;

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $file_content_type
     * @param string|NULL $file_extension
     * @return array|null
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws PhpException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function init(int $file_ID=NULL,string $file_content_type=NULL,string $file_extension=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list['file_ID']='File ID is empty';

        if(empty($file_content_type))
            $error_info_list['file_content_type']='File content type is empty';

        if(empty($file_extension))
            $error_info_list['file_extension']='File extension is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        self::$file_ID              =$file_ID;
        self::$file_content_type    =$file_content_type;
        self::$file_extension       =$file_extension;

        return self::set();

    }

}