<?php

namespace Core\Module\Video;

use Core\Module\Date\Date;
use Core\Module\Db\Db;
use Core\Module\Dir\Dir;
use Core\Module\Dir\DirConfig;
use Core\Module\Exception\DbParametersException;
use Core\Module\Exception\DbQueryException;
use Core\Module\Exception\ParametersException;
use Core\Module\Exception\PathException;
use Core\Module\Ffmpeg\Ffmpeg;
use Core\Module\Php\PhpValidation;
use Core\Module\User\User;

class Video{
    
    /** @var string */
    public  static $table_name='_video';

    /**
     * @param int|NULL $video_ID
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function isset_video_ID(int $video_ID=NULL){

        if(empty($video_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Video ID is empty'
            );

            throw new ParametersException($error);

        }

        return Db::isset_row_ID($video_ID,self::$table_name,0);

    }

    /**
     * @param array $video_ID_list
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_dir_list(array $video_ID_list=[]){

        if(count($video_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Video ID list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'hash',
                'file_extension',
                'date_create'
            ),
            'table'=>self::$table_name,
            'where'=>array(
                'id'        =>$video_ID_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row){

            $video_file_path=self::get_video_dir($row['id'],$row['file_extension'],$row['date_create']);

            if(file_exists($video_file_path))
                $list[$row['id']]=$video_file_path;

        }

        return $list;

    }

    /**
     * @param string|NULL $hash_link
     * @return array|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_item_data_from_hash_link(string $hash_link=NULL){

        if(empty($hash_link)){

            $error=[
                'title'     =>ParametersException::$title,
                'info'      =>'Hash link is empty'
            ];

            throw new ParametersException($error);

        }

        $q=[
            'table'     =>self::$table_name,
            'select'    =>[
                'id',
                'user_id',
                'file_id',
                'video_id',
                'file_extension',
                'date_create'
            ],
            'where'     =>[
                'hash'  =>$hash_link,
                'type'  =>0
            ],
            'limit'=>1
        ];

        $r=Db::select($q);

        if(count($r)==0)
            return NULL;

        return[
            'ID'                    =>$r[0]['id'],
            'user_ID'               =>$r[0]['user_id'],
            'file_ID'               =>$r[0]['file_id'],
            'video_ID'              =>$r[0]['video_id'],
            'file_extension'        =>$r[0]['file_extension'],
            'date_create'           =>$r[0]['date_create']
        ];

    }

    /**
     * @param int|NULL $video_ID
     * @param string|NULL $file_extension
     * @param string|NULL $date_create
     * @return string|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_dir(int $video_ID=NULL,string $file_extension=NULL,string $date_create=NULL){

        $error_info_list=[];

        if(empty($video_ID))
            $error_info_list[]='Video ID is empty';

        if(empty($file_extension))
            $error_info_list[]='File type is empty';

        if(empty($date_create))
            $error_info_list[]='Date create is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'video_ID'      =>$video_ID,
                    'file_extension'     =>$file_extension,
                    'date_create'   =>$date_create
                )
            );

            throw new ParametersException($error);

        }

        $date_path      =Date::get_date_path($date_create,DirConfig::$dir_video);
        $file_path      =$date_path.'/'.$video_ID;

        return $file_path;

    }

    /**
     * @param array $video_ID_list
     * @param bool $need_server_link
     * @return array
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_path_list(array $video_ID_list=[],bool $need_server_link=false){

        if(count($video_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Video ID list is empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'select'=>array(
                'id',
                'hash',
                'file_extension',
                'date_create'
            ),
            'table'=>self::$table_name,
            'where'=>array(
                'id'        =>$video_ID_list,
                'type'      =>0
            )
        );

        $r=Db::select($q);

        if(count($r)==0)
            return[];

        $list=[];

        foreach($r as $row){

            $video_file_path=self::get_video_dir($row['id'],$row['file_extension'],$row['date_create']);

            if(file_exists($video_file_path)){

                if($need_server_link)
                    $list[$row['id']]=$video_file_path.'/'.$row['id'].'.'.$row['file_extension'];
                else
                    $list[$row['id']]='show/video/content/'.$row['hash'].'.'.$row['file_extension'];

            }

        }

        return $list;

    }

    /**
     * @param int|NULL $video_ID
     * @param string|NULL $file_extension
     * @param string|NULL $date_create
     * @return string|null
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_dir(int $video_ID=NULL,string $file_extension=NULL,string $date_create=NULL){

        $error_info_list=[];

        if(empty($video_ID))
            $error_info_list[]='Video ID is empty';

        if(empty($date_create))
            $error_info_list[]='Date create is empty';

        if(count($error_info_list)>0){

            $error=array(
                'title'=>'Parameters problem',
                'info'=>$error_info_list,
                'data'=>array(
                    'file_ID'           =>$video_ID,
                    'file_extension'    =>$file_extension,
                    'date_create'       =>$date_create
                )
            );

            throw new ParametersException($error);

        }

        $date_path      =Date::get_date_path($date_create,DirConfig::$dir_video);
        $file_path      =$date_path.'/'.$video_ID;

        if(PhpValidation::is_console_running())
            $file_path=DIR_ROOT.'/'.$file_path;

        if(!file_exists($file_path))
            Dir::create_dir($file_path);

        return $file_path;

    }

    /**
     * @param int|NULL $video_ID
     * @param bool $need_path
     * @param bool $need_hash_path
     * @return string
     * @throws DbParametersException
     * @throws DbQueryException
     * @throws ParametersException
     * @throws PathException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_file_path_from_video_ID(int $video_ID=NULL,bool $need_path=false,bool $need_hash_path=false){

        if(empty($video_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File ID is empty'
            );

            throw new ParametersException($error);

        }

        $column_list        =[];
        $file_extension     =NULL;
        $date_create        =NULL;
        $hash               =NULL;
        $column_list[]      ='file_extension';
        $column_list[]      ='date_create';
        $column_list[]      ='hash';

        if(count($column_list)>0){

            $r=Db::get_data_from_ID($video_ID,self::$table_name,$column_list,0);

            if(!empty($r['hash']))
                $hash=$r['hash'];

            if(!empty($r['file_extension']))
                $file_extension=$r['file_extension'];

            if(!empty($r['date_create']))
                $date_create=$r['date_create'];

        }

        if(
              empty($date_create)
            &&empty($file_extension)
        ){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'System cannot get file date create',
                'data'  =>array(
                    'file_ID'           =>$video_ID,
                    'file_extension'    =>$file_extension,
                    'date_create'       =>$date_create
                )
            );

            throw new DbParametersException($error);

        }

        if($need_hash_path)
            $file_path='show/video/content/'.$hash;
        else{

            $file_dir   =self::get_file_dir($video_ID,$file_extension,$date_create);
            $file_path  =$file_dir.'/'.$video_ID;

        }

        if(!empty($file_extension))
            $file_path.='.'.$file_extension;

        if(!$need_path)
            if(!file_exists($file_path)){

                $error=array(
                    'title' =>'File path problem',
                    'info'  =>'File file is not exists',
                    'data'  =>array(
                        'file_ID'       =>$video_ID,
                        'file_path'     =>$file_path
                    )
                );

                throw new PathException($error);

            }

        return $file_path;

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_bitrate_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_video_bitrate($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_codec_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_video_codec($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return mixed|string[]
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_size_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_video_size($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return float|mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_fps_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_video_fps($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_duration_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_duration($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return float|int|mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_videoo_duration_in_seconds_from_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_duration_in_seconds($file_path);

    }

    /**
     * @param string|NULL $file_path
     * @return int|mixed
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\FileException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\ShellExecException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function get_video_duration_in_milliseconds_from_file_path(string $file_path=NULL){

        if(empty($file_path)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'File path is empty'
            );

            throw new ParametersException($error);

        }

        return Ffmpeg::get_duration_in_milliseconds($file_path);

    }

    /**
     * @param int|NULL $file_ID
     * @param string|NULL $video_type
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param bool $preparing
     * @param bool $prepared
     * @param bool $public
     * @param bool $hide
     * @param bool $need_return_date_create
     * @param string|NULL $source_link
     * @return int|array
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function add_video(int $file_ID=NULL,string $video_type=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL,bool $preparing=false,bool $prepared=false,bool $public=false,bool $hide=false,bool $need_return_date_create=false,string $source_link=NULL){

        $error_info_list=[];

        if(empty($file_ID))
            $error_info_list[]='File ID is empty';

        if(empty($video_type))
            $error_info_list[]='Video type is empty';;

        if(count($error_info_list)>0){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>$error_info_list
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>self::$table_name,
            'values'=>array(
                'user_id'           =>User::$user_ID,
                'file_id'           =>$file_ID,
                'video_type'        =>$video_type,
                'file_size'         =>$file_size,
                'file_mime_type'    =>$file_mime_type,
                'file_extension'    =>$file_extension,
                'source_link'       =>empty($source_link)?NULL:$source_link,
                'preparing'         =>(int)$preparing,
                'prepared'          =>(int)$prepared,
                'public'            =>(int)$public,
                'hide'              =>(int)$hide,
                'date_create'       =>'NOW()',
                'date_update'       =>'NOW()',
                'type'              =>0
            )
        );

        $return_list=[];

        if($need_return_date_create)
            $return_list[]='date_create';

        $r=Db::insert($q,true,$return_list);

        if(count($r)==0){

            $error=array(
                'title' =>'DB query problem',
                'info'  =>'video was not added'
            );

            throw new DbQueryException($error);

        }

        if(count($return_list)==0)
            return $r[0]['id'];
        else
            return array(
                'ID'            =>$r[0]['id'],
                'date_create'   =>$r[0]['date_create']
            );

    }

    /**
     * @param int|NULL $video_ID
     * @param string|NULL $hash
     * @param int|NULL $width
     * @param int|NULL $height
     * @param int|NULL $file_size
     * @param string|NULL $file_mime_type
     * @param string|NULL $file_extension
     * @param string|NULL $source_link
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function update_video_prepared(int $video_ID=NULL,string $hash=NULL,int $width=NULL,int $height=NULL,int $file_size=NULL,string $file_mime_type=NULL,string $file_extension=NULL,string $source_link=NULL){

        if(empty($video_ID)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'video_ID si empty'
            );

            throw new ParametersException($error);

        }

        $q=array(
            'table'=>self::$table_name,
            'set'=>array(
                'hash'                  =>$hash,
                'width'                 =>$width,
                'height'                =>$height,
                'file_size'             =>$file_size,
                'file_mime_type'        =>$file_mime_type,
                'file_extension'        =>$file_extension,
                'preparing'             =>0,
                'prepared'              =>1,
                'public'                =>1,
                'hide'                  =>0,
                'date_update'           =>'NOW()',
                'type'                  =>0
            ),
            'where'=>array(
                'id'    =>$video_ID,
                'type'  =>0
            )
        );

        if(!empty($source_link))
            $q['set']['source_link']=$source_link;

        if(!Db::update($q)){

            $error=array(
                'title'     =>'Parameters problem',
                'info'      =>'video ID is empty'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param int|NULL $video_ID
     * @param bool $is_need_remove_video_item
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_video_ID(int $video_ID=NULL,bool $is_need_remove_video_item=true){

        if(empty($video_ID)){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Video ID is empty'
            );

            throw new ParametersException($error);

        }

//        if($is_need_remove_video_item)
//            VideoItem::remove_video_item($video_ID);

        if(!Db::delete_from_ID($video_ID,self::$table_name,0)){

            $error=array(
                'title' =>'Db query problem',
                'info'  =>'Video ID was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

    /**
     * @param array $video_ID_list
     * @param bool $is_need_remove_video_item
     * @return bool
     * @throws DbQueryException
     * @throws ParametersException
     * @throws \Core\Module\Exception\DbParametersException
     * @throws \Core\Module\Exception\DbQueryParametersException
     * @throws \Core\Module\Exception\DbValidationValueException
     * @throws \Core\Module\Exception\PathException
     * @throws \Core\Module\Exception\PhpException
     * @throws \Core\Module\Exception\SystemException
     */
    public  static function remove_video_ID_list(array $video_ID_list=[],bool $is_need_remove_video_item=true){

        if(count($video_ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Video ID list is empty'
            );

            throw new ParametersException($error);

        }

        $ID_list=[];

        foreach($video_ID_list as $video_ID)
            if(!empty($video_ID))
                $ID_list[]=$video_ID;

        if(count($ID_list)==0){

            $error=array(
                'title' =>'Parameters problem',
                'info'  =>'Video ID list is empty'
            );

            throw new ParametersException($error);

        }

//        if($is_need_remove_video_item)
//            VideoItem::remove_video_item_list($ID_list);

        if(!Db::delete_from_ID_list($ID_list,self::$table_name,0)){

            $error=array(
                'title' =>'Db query problem',
                'info'  =>'Video ID was not removed'
            );

            throw new DbQueryException($error);

        }

        return true;

    }

}